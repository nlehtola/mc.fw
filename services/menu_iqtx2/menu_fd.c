#define _GNU_SOURCE
#include <encoder.h>
#include <lcp.h>
#include <stdio.h>
#include <libio.h>
#include <string.h>
#include "menu_fd.h"
#include <pthread.h>
#include <errno.h>


enum write_state
{
	WRITE_STATE_NORMAL,
	WRITE_STATE_ESCAPE1
};


#define MAX_EVENTS (32)
static struct mfpio_t
{
	struct encoder_handle *h_encoder;
	enum write_state state;

	bool verbose;
	int button0;
	int last_button0;
	int button1;
	int last_button1;
	int button2;
	int last_button2;

	pthread_t poll_thread;
	pthread_mutex_t event_mutex;
	pthread_cond_t event_sig;
	enum menu_read_result event_queue[MAX_EVENTS];
	int eq_head, eq_tail;
} mfpio;


void eq_push(struct mfpio_t *h, enum menu_read_result r)
{
	h->event_queue[h->eq_head++] = r;
	if (h->eq_head == MAX_EVENTS)
		h->eq_head = 0;

	// if the head overruns the tail then push the tail
	// along so we discard the oldest sample
	if (h->eq_head == h->eq_tail)
	{
		h->eq_tail++;
		if (h->eq_tail == MAX_EVENTS)
			h->eq_tail = 0;
	}
}


int eq_pop(struct mfpio_t *h, enum menu_read_result *r)
{
	if (h->eq_tail == h->eq_head)
		return 0;

	if (r != NULL)
		memcpy(r, &h->event_queue[h->eq_tail], sizeof(*r));
	if (h->eq_tail++ == MAX_EVENTS)
		h->eq_tail = 0;
	return 1;
}


#define POLL_TIMEOUT (200000) // 200ms timeout
#define ENCODER_SPEED_THRESH0 30000
#define ENCODER_SPEED_THRESH1 115000
void * menu_poll(void *ptr)
{
	int r;
	struct mfpio_t *h = ptr;

	while (1)
	{
		int event = 0;
		int dtick;
		r = encoder_poll(h->h_encoder, POLL_TIMEOUT);
		if (r == -1)
			// error occurred (try again)
			continue;
		
		pthread_mutex_lock(&h->event_mutex);

		// update the encoder button state (this may have trigger this event)
		h->last_button0 = h->button0;
		h->button0 = encoder_button(h->h_encoder);
		if (h->button0 != h->last_button0)
		{
			event = 1;
			if (h->button0)
				eq_push(h, menu_button0_push);
			else
				eq_push(h, menu_button0_release);
		}

		// update laser control panel state
		h->last_button1 = h->button1;
		h->button1 = lcp_get_enabled(0);
		if (h->button1 != h->last_button1)
		{
			event = 1;
			if (h->button1)
				eq_push(h, menu_button1_push);
			else
				eq_push(h, menu_button1_release);
		}
		h->last_button2 = h->button2;
		h->button2 = lcp_get_enabled(1);
		if (h->button2 != h->last_button2)
		{
			event = 1;
			if (h->button2)
				eq_push(h, menu_button2_push);
			else
				eq_push(h, menu_button2_release);
		}

		// update rotary movements
		dtick = encoder_tick(h->h_encoder, 1);
		if (dtick != 0)
		{
			int dt = encoder_dt(h->h_encoder) / dtick; // average dt per tick (1/w)
			event = 1;
			if (dt < -ENCODER_SPEED_THRESH1)
				eq_push(h, menu_rot_down_slow);
			else if (dt < -ENCODER_SPEED_THRESH0)
				eq_push(h, menu_rot_down);
			else if (dt < 0)
				eq_push(h, menu_rot_down_fast);
			else if (dt < ENCODER_SPEED_THRESH0)
				eq_push(h, menu_rot_up_fast);
			else if (dt < ENCODER_SPEED_THRESH1)
				eq_push(h, menu_rot_up);
			else
				eq_push(h, menu_rot_up_slow);
		}

		if (event)
			// event occurred so signal the read thread to wakeup
			pthread_cond_signal(&h->event_sig);

		pthread_mutex_unlock(&h->event_mutex);
	}
}

enum menu_read_result menu_read(struct mfpio_t *h, int timeout)
{
	enum menu_read_result result;
	int r = 0;
	int dtick;
	int dt;
	int button;
	struct timespec now;
	struct timespec _timeout;

	// figure out when timeout should expire
	gettimeofday(&now, NULL);
	_timeout.tv_sec = now.tv_sec + timeout / 1000;
	_timeout.tv_nsec = now.tv_nsec + (timeout % 1000) * 1e6;

	// wait for a signal from the poll thread
	while (1)
	{
		pthread_mutex_lock(&h->event_mutex);

		if (eq_pop(h, &result))
		{
			// return next pending event
			pthread_mutex_unlock(&h->event_mutex);
			return result;
		}

		if (timeout >= 0)
			r = pthread_cond_timedwait(&h->event_sig, &h->event_mutex, &_timeout);
		else
			r = pthread_cond_wait(&h->event_sig, &h->event_mutex);
		if (r == ETIMEDOUT)
		{
			pthread_mutex_unlock(&h->event_mutex);
			return menu_timeout;
		}

		// no signal or event found so try again
		pthread_mutex_unlock(&h->event_mutex);
	}
}


int menu_close(void *param)
{
	///@todo sig term and join poll thread
}


struct mfpio_t * menu_open(FILE **fin, FILE **fout)
{
	struct mfpio_t *h = &mfpio;
	cookie_io_functions_t fops;
	memset(&mfpio, 0, sizeof(mfpio));
	h->verbose = 0;

	// init the front panel submodules
	h->h_encoder = encoder_open();
	if (fout != NULL)
	{
		*fout = fopen("/dev/lcd", "r+");
		if (*fout == NULL)
			goto fail1;
	}
	// we do not support input fd atm
	if (fin != NULL)
		*fin = NULL;

	// start poll thread
	if (pthread_mutex_init(&h->event_mutex, NULL))
		goto fail1;
	if (pthread_cond_init(&h->event_sig, NULL))
		goto fail2;
	if (pthread_create(&h->poll_thread, NULL, menu_poll, h))
		goto fail3;

	// success
	return h;

	// badnesses
fail3:
	pthread_cond_destroy(&h->event_sig);
fail2:
	pthread_mutex_destroy(&h->event_mutex);
fail1:
	return NULL;
}

