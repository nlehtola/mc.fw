#ifndef __LCD_FD_H__
#define __LCD_FD_H__

#include <stdint.h>
#include <stdio.h>

struct mfpio_t;

enum menu_read_result
{
	menu_error = -1,
	menu_timeout = 0,
	menu_rot_up_slow,
	menu_rot_up,
	menu_rot_up_fast,
	menu_rot_down_slow,
	menu_rot_down,
	menu_rot_down_fast,
	menu_button0_push,
	menu_button0_release,
	menu_button1_push,
	menu_button1_release,
	menu_button2_push,
	menu_button2_release,
};
enum menu_read_result menu_read(struct mfpio_t *h, int timeout);

struct mfpio_t * menu_open(FILE **fin, FILE **fout);

#endif

