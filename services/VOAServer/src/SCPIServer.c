/**
 * @file VOAServer.c
 * @brief 
**/

#include "libSCPI.h"
#include "common.h"
#include "logging.h"
#include "singleton.h"
#include "backendCallbacks.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <dlfcn.h>
#include <stdbool.h>

#define SCPI_DAEMON_NAME "TLSX_VOAServer"
#define SCPI_PID_FILE "/tmp/VOAServer.pid"

void usage(void)
{
	fprintf(stderr,"SCPIServer [-h][-m <mask>][-v <verbosity>]\n");
	fprintf(stderr,"\t-m\t\t<mask> must be a number in hexidecimal, up to 16 bits. Each bit masks a blade address to search for\n");
	fprintf(stderr,"\t-v\t\tSet the logging verbosity to <verbosity>\n");
	//fprintf(stderr,"\t-f\t\tLoad the libfakeblade.so library instead of libkblade.so\n");
	fprintf(stderr,"\t-h\t\tshow this message\n");
}

/** @brief Safe shutdown handler
 *  @return void
 */
void shutdown_handler(int signum)
{
	deinitBackend();
	// clean up any open tcp streams
	// removeAllConnections();
	// socketList
	exit(128+signum);
}

int main(int argc, char **argv)
{
	// process command line options
	int verbosity = 0;
	unsigned int CLImask = 0;
	int opt;
	bool fakekblade = false;
	char configFile[1000];
	configFile[0] = '\0';
	while ((opt = getopt(argc, argv, "c:m:v:fh")) != -1)
	{
		switch (opt)
		{
			case 'c':
				strncpy(configFile, optarg, 1000);
				break;
			case 'v':
				verbosity = atoi(optarg);
				if(verbosity<0) {
					verbosity = 0;
				}
				break;
			case 'f':
				fakekblade = true;
				break;
			case 'm':
				if(sscanf(optarg, "%x", &CLImask)==1)
					break;
			case 'h':
				usage();
				exit(0);
			default:
				usage();
				exit(1);
		}
	}
	
	spOpenLog(SCPI_DAEMON_NAME, verbosity);
	if (getSingletonLock(SCPI_PID_FILE)<0) {
		spLogError("Another instance of %s is already running, aborting\n", SCPI_DAEMON_NAME);
		fprintf(stdout, "Another instance of %s is already running, aborting\n", SCPI_DAEMON_NAME);
		exit(2);
	}

	// TODO, get mask from config file
	uint16_t mask = 0x8000;		// Benchtop = 15  1000 0000 0000 0000
	//uint16_t mask = 0x03FE;	// IQS = 1-9      0000 0011 1111 1110
	if (CLImask!=0) {
		mask = CLImask;
	}
	
	loadConfig(configFile);
	
	// initialize the connection with the firmware
	// If either the command line flags or config file say so then load fakekblade
	if(initBackend(mask, fakekblade||getIsFakekblade())<0) {
		spLogError("Could not initialize connection to blade firmware");
		exit(3);
	}
	
	// Register a signal handler to ensure safe shutdown of sockets
	signal(SIGINT, shutdown_handler);
	signal(SIGTERM, shutdown_handler);
	
	// starts the RPC server to recieve SCPI commands over VXI11
	startSCPI();
	// Only returns on fatal errors	
	exit(4);
}
