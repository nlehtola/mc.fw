#include "fake_kblade.h"
#include "logging.h"
#include <pthread.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <math.h>
#include <string.h>

// In the worst case this will be approximately the time to do 1 round trip
// write then read to the blade using kowhai
#define HARDWARE_DELAY (30000)	//us
// This is how often actual values are updated
#define UPDATE_DELAY (1000)		//us

#define POWER_NOISE (0.01)	//dBm
#define ATTEN_NOISE (0.01)	//dB

#define INPUT_POWER (10.0)	//dBm

struct hardware_state_t
{
	double min_attenuation;
	double max_attenuation;
	double default_attenuation;
	double set_attenuation;
	double attenuation;
	double attenuation_offset;
	double insertion_loss;

	double atten_velocity_set;
	double atten_velocity;
	double atten_velocity_default;
	double atten_velocity_min;
	double atten_velocity_max;

	double min_power;
	double max_power;
	double default_power;
	double set_power;
	double power;

	double averaging_time_min;
	double averaging_time_max;
	double averaging_time_default;
	double averaging_time;

	float dark_power_min;
	float dark_power_max;
	float dark_power_raw;
	float dark_power;

	float nulling_time_remaining;

	float wave_list_len;
	float wave_list[10];
	float wave_min;
	float wave_max;
	int wave_set;

	enum kblade_ctrl_mode_t control_mode;		// 0=attenuation 1=tracking
	bool power_locked;
	bool attenuation_locked;
} hardware_state[4];

// background thread to simulate real life processes
static void* update_values(void* param)
{
	while(1) {
		int i;
		for (i=0; i<4; ++i) {
			if (KBLADE_CTRL_MODE_ATTENUATION==hardware_state[i].control_mode) {
				// fake a gradual move to the set point
				hardware_state[i].attenuation = 99.0 / 100.0 * (hardware_state[i].set_attenuation/99.0
						+ hardware_state[i].attenuation);
				hardware_state[i].power = INPUT_POWER - hardware_state[i].attenuation - hardware_state[i].insertion_loss;

				if (fabs(hardware_state[i].attenuation-hardware_state[i].set_attenuation)<0.01)
					hardware_state[i].attenuation_locked = true;
			} else {
				double target = fmin(hardware_state[i].set_power,INPUT_POWER);
				hardware_state[i].power = 99.0 / 100.0 * (target/99.0
						+ hardware_state[i].power);
				hardware_state[i].attenuation = INPUT_POWER - hardware_state[i].power - hardware_state[i].insertion_loss;

				if (fabs(hardware_state[i].power-hardware_state[i].set_power)<0.01)
					hardware_state[i].power_locked = true;
			}
			float nulling_time_remaining = hardware_state[i].nulling_time_remaining;
			if (nulling_time_remaining!=0.0) {
				nulling_time_remaining -= ((float)UPDATE_DELAY)*1e-6;
				if (nulling_time_remaining<0.0)
					nulling_time_remaining = 0.0;
				hardware_state[i].nulling_time_remaining = nulling_time_remaining;
			}
		}
		usleep(UPDATE_DELAY);
	}
	return 0;
}

// #################### kblade ####################
int kblade_get_fw_version(int blade_id, uint16_t *major, uint16_t *minor)
{
	*major = 0;
	*minor = 0;
	// delay to simulate hardware delay
	usleep(HARDWARE_DELAY);
	return 0;
}

int kblade_get_power_raw(int blade_id, int channel, float *act, float *min, float *max)
{
	if(min!=NULL)
		*min = 0;
	if(max!=NULL)
		*max = 1;
	if(act!=NULL)
		*act = 0;
	usleep(HARDWARE_DELAY);
	return 0;
}

int kblade_get_power(int blade_id, int channel, float* set, float* act, float* def, float* min, float* max)
{
	double noise = POWER_NOISE*(1.0-(2.0*(random()/(float)RAND_MAX)));
	if(min!=NULL)
		*min = hardware_state[channel].min_power;
	if(max!=NULL)
		*max = hardware_state[channel].max_power;
	if(def!=NULL)
		*def = hardware_state[channel].default_power;
	if(set!=NULL)
		*set = hardware_state[channel].set_power;
	if(act!=NULL)
		*act = hardware_state[channel].power + noise;
	// delay to simulate hardware delay
	usleep(HARDWARE_DELAY);
	return 0;
}

int kblade_set_power(int blade_id, int channel, float value)
{
	//hardware_state[channel].control_mode = 1;		// tracking power
	hardware_state[channel].set_power = value;

	if (hardware_state[channel].control_mode == KBLADE_CTRL_MODE_POWER) {
		hardware_state[channel].power_locked = false;
		hardware_state[channel].attenuation_locked = false;
	}

	// delay to simulate hardware delay
	usleep(HARDWARE_DELAY);
	return 0;
}

int kblade_get_dark_power(int blade_id, int channel, float *set, float *min, float *max, float *raw)
{
	if(min!=NULL)
		*min = hardware_state[channel].dark_power_min;
	if(max!=NULL)
		*max = hardware_state[channel].dark_power_max;
	if(raw!=NULL)
		*raw = hardware_state[channel].dark_power_raw;
	if(set!=NULL)
		*set = hardware_state[channel].dark_power;

	usleep(HARDWARE_DELAY);
	return 0;
}

int kblade_set_dark_power(int blade_id, int channel, float pdark)
{
	hardware_state[channel].dark_power = pdark;
	usleep(HARDWARE_DELAY);
	return 0;
}

int kblade_get_dark_power_time_remaining(int blade_id, int channel, float *time)
{
	if(time!=NULL)
		*time = hardware_state[channel].nulling_time_remaining;
	usleep(HARDWARE_DELAY);
	return 0;
}

int kblade_start_dark_power_measurement(int blade_id, int channel)
{
	hardware_state[channel].nulling_time_remaining = 10.0;
	usleep(HARDWARE_DELAY);
	return 0;
}

int kblade_get_power_averaging_time(int blade_id, int channel, float *set, float *def, float *min, float *max)
{
	if(min!=NULL)
		*min = hardware_state[channel].averaging_time_min;
	if(max!=NULL)
		*max = hardware_state[channel].averaging_time_max;
	if(def!=NULL)
		*def = hardware_state[channel].averaging_time_default;
	if(set!=NULL)
		*set = hardware_state[channel].averaging_time;
	usleep(HARDWARE_DELAY);
	return 0;
}

int kblade_set_power_averaging_time(int blade_id, int channel, float time)
{
	hardware_state[channel].averaging_time = time;
	usleep(HARDWARE_DELAY);
	return 0;
}

int kblade_get_attenuation_raw(int blade_id, int channel, float *set, float *act, float *min, float *max)
{
	if(min!=NULL)
		*min = 0;
	if(max!=NULL)
		*max = 65535;
	if(set!=NULL)
		*set = 0;
	if(act!=NULL)
		*act = 0;
	usleep(HARDWARE_DELAY);
	return 0;
}

int kblade_get_attenuation(int blade_id, int channel, float* set, float* act, float* def, float* min, float* max)
{
	double noise = ATTEN_NOISE*(1.0-(2.0*(random()/(float)RAND_MAX)));
	double offset = hardware_state[channel].insertion_loss + hardware_state[channel].attenuation_offset;

	if(min!=NULL)
		*min = hardware_state[channel].min_attenuation + offset;
	if(max!=NULL)
		*max = hardware_state[channel].max_attenuation + offset;
	if(def!=NULL)
		*def = hardware_state[channel].default_attenuation + offset;
	if(set!=NULL)
		*set = hardware_state[channel].set_attenuation + offset;
	if(act!=NULL) {
		if (KBLADE_CTRL_MODE_ATTENUATION==hardware_state[channel].control_mode) {
			*act = hardware_state[channel].set_attenuation + offset;
		} else {
			*act = hardware_state[channel].attenuation + noise + offset;
			if (*act<0) {
				*act = 0;
			}
		}
	}
	// delay to simulate hardware delay
	usleep(HARDWARE_DELAY);
	return 0;
}

int kblade_set_attenuation_raw(int blade_id, int channel, float attenuation)
{
	// do nothing
	// delay to simulate hardware delay
	usleep(HARDWARE_DELAY);
	return 0;
}

int kblade_set_attenuation(int blade_id, int channel, float value)
{
	//hardware_state[channel].control_mode = 0;		// attenuation
	double offset = hardware_state[channel].insertion_loss + hardware_state[channel].attenuation_offset;

	hardware_state[channel].set_attenuation = value - offset;

	if (hardware_state[channel].control_mode == KBLADE_CTRL_MODE_ATTENUATION) {
		hardware_state[channel].power_locked = false;
		hardware_state[channel].attenuation_locked = false;
	}

	// delay to simulate hardware delay
	usleep(HARDWARE_DELAY);
	return 0;
}

int kblade_get_attenuation_offset(int blade_id, int channel, float *offset)
{
	if(offset!=NULL)
		*offset = hardware_state[channel].attenuation_offset;
	usleep(HARDWARE_DELAY);
	return 0;
}

int kblade_set_attenuation_offset(int blade_id, int channel, float offset)
{
	hardware_state[channel].attenuation_offset = offset;
	usleep(HARDWARE_DELAY);
	return 0;
}

int kblade_get_attenuation_velocity(int blade_id, int channel, float *set, float *actual, float *def, float *min, float *max)
{
	if(set!=NULL)
		*set = hardware_state[channel].atten_velocity_set;
	if(actual!=NULL)
		*actual = hardware_state[channel].atten_velocity;
	if(def!=NULL)
		*def = hardware_state[channel].atten_velocity_default;
	if(min!=NULL)
		*min = hardware_state[channel].atten_velocity_min;
	if(max!=NULL)
		*max = hardware_state[channel].atten_velocity_max;
	usleep(HARDWARE_DELAY);
	return 0;
}

int kblade_set_attenuation_velocity(int blade_id, int channel, float velocity)
{
	hardware_state[channel].atten_velocity_set = velocity;
	hardware_state[channel].atten_velocity = velocity;
	usleep(HARDWARE_DELAY);
	return 0;
}

int kblade_get_attenuator_temperature(int blade_id, int channel, float *actual, float *min, float *max)
{
	if(min!=NULL)
		*min = -20;
	if(max!=NULL)
		*max = 100;
	if(actual!=NULL)
		*actual = 23.46;
	usleep(HARDWARE_DELAY);
	return 0;
}

int kblade_get_wavelength(int blade_id, int channel, float *set, float *def, float *min, float *max)
{
	if (set!=NULL)
		*set = hardware_state[channel].wave_list[hardware_state[channel].wave_set];
	if (def!=NULL)
		*def = hardware_state[channel].wave_max;
	if (min!=NULL)
		*min = hardware_state[channel].wave_min;
	if (max!=NULL)
		*max = hardware_state[channel].wave_max;

	// delay to simulate hardware delay
	usleep(HARDWARE_DELAY);
	return 0;
}

int kblade_set_wavelength(int blade_id, int channel, float wavelength)
{
	int i;
	for (i=0; i<hardware_state[channel].wave_list_len; ++i) {
		// if the value being set is closer to list[i] than the next highest,
		// then set the internal value to i, otherwise go higher
		if (wavelength<((hardware_state[channel].wave_list[i]+hardware_state[channel].wave_list[i+1])/2)) {
			break;
		}
	}
	hardware_state[channel].wave_set = i;
	// delay to simulate hardware delay
	usleep(HARDWARE_DELAY);
	return 0;
}

int kblade_get_ctrl_mode(int blade_id, int channel, enum kblade_ctrl_mode_t *mode)
{
	if (mode!=NULL)
		*mode = hardware_state[channel].control_mode;
	// delay to simulate hardware delay
	usleep(HARDWARE_DELAY);
	return 0;
}

int kblade_set_ctrl_mode(int blade_id, int channel, enum kblade_ctrl_mode_t mode)	// 0=attenuation 1=tracking
{
	if (mode==KBLADE_CTRL_MODE_ATTENUATION) {
		hardware_state[channel].set_attenuation = hardware_state[channel].attenuation;
	} else {
		hardware_state[channel].set_power = hardware_state[channel].power;
	}

	hardware_state[channel].control_mode = mode;

	hardware_state[channel].power_locked = false;
	hardware_state[channel].attenuation_locked = false;

	// delay to simulate hardware delay
	usleep(HARDWARE_DELAY);
	return 0;
}

int kblade_get_locked(int blade_id, int channel, uint8_t *attenuation_locked, uint8_t *power_locked)
{
	if (attenuation_locked!=NULL)
		*attenuation_locked = hardware_state[channel].attenuation_locked;
	if (power_locked!=NULL)
		*power_locked = hardware_state[channel].power_locked;
	// delay to simulate hardware delay
	usleep(HARDWARE_DELAY);
	return 0;
}

int kblade_init(uint16_t *mask)
{
	if (mask!=NULL){
		*mask &= (1<<15);
	}
	spLogWarning("Warning: current libkblade.so is fake kblade");

	int i;
	for (i=0; i<4; ++i) {
		hardware_state[i].min_attenuation = 0.0;
		hardware_state[i].max_attenuation = 45.0;
		hardware_state[i].default_attenuation = 0.0;
		hardware_state[i].set_attenuation = 0.0;
		hardware_state[i].attenuation = 0.0;
		hardware_state[i].attenuation_offset = 0.0;
		hardware_state[i].insertion_loss = 0.53;

		hardware_state[i].atten_velocity_set = 20.0;
		hardware_state[i].atten_velocity = 20.0;
		hardware_state[i].atten_velocity_default = 20.0;
		hardware_state[i].atten_velocity_min = 0.0;
		hardware_state[i].atten_velocity_max = 100.0;

		hardware_state[i].min_power = -45;
		hardware_state[i].max_power = 20;
		hardware_state[i].default_power = 10.0;
		hardware_state[i].set_power = 10.0;
		hardware_state[i].power = 10.0;

		hardware_state[i].averaging_time_min = 0.0;
		hardware_state[i].averaging_time_max = 100.0;
		hardware_state[i].averaging_time_default = 2.0;
		hardware_state[i].averaging_time = 2.0;

		hardware_state[i].dark_power_min = -100;
		hardware_state[i].dark_power_max = -30;
		hardware_state[i].dark_power_raw = 0.012;
		hardware_state[i].dark_power = -70.234;

		hardware_state[i].nulling_time_remaining = 0.0;

		hardware_state[i].wave_list_len = 3;
		hardware_state[i].wave_list[0] = 1310e-9;
		hardware_state[i].wave_list[1] = 1490e-9;
		hardware_state[i].wave_list[2] = 1550e-9;
		hardware_state[i].wave_list[3] = 1550e-9;
		hardware_state[i].wave_min = hardware_state[i].wave_list[0];
		hardware_state[i].wave_max = hardware_state[i].wave_list[3];
		hardware_state[i].wave_set = 2;

		hardware_state[i].control_mode = KBLADE_CTRL_MODE_ATTENUATION;
		hardware_state[i].power_locked = false;
		hardware_state[i].attenuation_locked = true;
	}

	pthread_t background_thread;
	if (pthread_create(&background_thread, NULL, update_values, NULL)) {
		spLogError("Error: could not create background thread for fake_kblade");
	}

	return 0;
}

void kblade_deinit(uint16_t mask)
{
}

int kblade_get_setting(int blade_id, const char* path, int path_len, void *dst, int dst_len, int pull)
{
	int len = (path_len<100)?path_len:100;
	if (strncmp(path, "settings.wavelengths", len)==0) {
		if (dst_len!=3*sizeof(float)) {
			return -3;
		}
		if (dst) {
			((float*)dst)[0] = 1310e-9;
			((float*)dst)[1] = 1490e-9;
			((float*)dst)[2] = 1550e-9;
		}
	} else if (strncmp(path, "settings.channel_mask", len)==0) {
		if (dst_len!=1) {
			return -6;
		}
		if (dst) {
			*(uint8_t *)dst = 15;
		}
	} else if (len == strlen("settings.channel[x].attenuation.wavelength[x].insertion_loss")) {
		int channel = (path[17]-'0');
		//int wavelength = (path[43]-'0');
		*((float*)dst) = hardware_state[channel].insertion_loss;
	} else {
		return -5;
	}
	return 0;
}

// run nm on this .so file to see this function. This is to distinguish it
// from the real libkblade.so
void zFAKE_KBLADE()
{
}

