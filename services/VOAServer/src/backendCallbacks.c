#include "backendCallbacks.h"
#include "libSCPI.h"
#include "kblade.h"
#include "version.h"
#include "error_codes.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <dlfcn.h>

#define FLOAT_PRECISION_ATTEN		(4)
#define FLOAT_PRECISION_POWER		(4)
#define FLOAT_PRECISION_TEMP		(2)

static struct {
	// functions
	int (*kblade_get_fw_version)(int blade_id, uint16_t *major, uint16_t *minor);
	int (*kblade_get_power_raw)(int blade_id, int channel, float *actual, float *min, float *max);
	int (*kblade_get_power)(int blade_id, int channel, float *set, float *actual, float *def, float *min, float *max);
	int (*kblade_set_power)(int blade_id, int channel, float power);
	int (*kblade_get_dark_power)(int blade_id, int channel, float *set, float *min, float *max, float *raw);
	int (*kblade_set_dark_power)(int blade_id, int channel, float pdark);
	int (*kblade_get_dark_power_time_remaining)(int blade_id, int channel, float *time);
	int (*kblade_start_dark_power_measurement)(int blade_id, int channel);
	int (*kblade_get_power_averaging_time)(int blade_id, int channel, float *set, float *def, float *min, float *max);
	int (*kblade_set_power_averaging_time)(int blade_id, int channel, float time);
	int (*kblade_get_attenuation_raw)(int blade_id, int channel, float *set, float *actual, float *min, float *max);
	int (*kblade_get_attenuation)(int blade_id, int channel, float *set, float *actual, float *def, float *min, float *max);
	int (*kblade_set_attenuation_raw)(int blade_id, int channel, float attenuation);
	int (*kblade_set_attenuation)(int blade_id, int channel, float attenuation);
	int (*kblade_get_attenuation_offset)(int blade_id, int channel, float *offset);
	int (*kblade_set_attenuation_offset)(int blade_id, int channel, float offset);
	int (*kblade_get_attenuation_velocity)(int blade_id, int channel, float *set, float *actual, float *def, float *min, float *max);
	int (*kblade_set_attenuation_velocity)(int blade_id, int channel, float velocity);
	int (*kblade_get_attenuator_temperature)(int blade_id, int channel, float *actual, float *min, float *max);
	int (*kblade_get_wavelength)(int blade_id, int channel, float *set, float *def, float *min, float *max);
	int (*kblade_set_wavelength)(int blade_id, int channel, float wavelength);
	int (*kblade_get_ctrl_mode)(int blade_id, int channel, enum kblade_ctrl_mode_t *mode);
	int (*kblade_set_ctrl_mode)(int blade_id, int channel, enum kblade_ctrl_mode_t mode);
	int (*kblade_get_locked)(int blade_id, int channel, uint8_t *attenuation_locked, uint8_t *power_locked);
	int (*kblade_init)(uint16_t *mask);
	void (*kblade_deinit)(uint16_t mask);
	int (*kblade_get_setting)(int blade_id, const char * path, int path_len, void *dst, int dst_len, int pull);

	// params
	bool init;
	int initializedBladeMask;
} kblade = {.init=false, .initializedBladeMask=0};

int loadKbladeFunctions(bool fakekblade)
{
	void *kh = NULL;
	if (fakekblade) {
		kh = dlopen("libfakekblade.so", RTLD_NOW);
		if (!kh) {
			spLogError("Unable to load library libfakekblade.so");
			return -1;
		}
	} else {
		kh = dlopen("libkblade.so", RTLD_NOW);
		if (!kh) {
			spLogError("Unable to load library libkblade.so");
			return -1;
		}
	}
	kblade.kblade_get_fw_version = dlsym(kh, "kblade_get_fw_version");
	kblade.kblade_get_power_raw = dlsym(kh, "kblade_get_power_raw");
	kblade.kblade_get_power = dlsym(kh, "kblade_get_power");
	kblade.kblade_set_power = dlsym(kh, "kblade_set_power");
	kblade.kblade_get_dark_power = dlsym(kh, "kblade_get_dark_power");
	kblade.kblade_set_dark_power = dlsym(kh, "kblade_set_dark_power");
	kblade.kblade_get_dark_power_time_remaining = dlsym(kh, "kblade_get_dark_power_time_remaining");
	kblade.kblade_start_dark_power_measurement = dlsym(kh, "kblade_start_dark_power_measurement");
	kblade.kblade_get_power_averaging_time = dlsym(kh, "kblade_get_power_averaging_time");
	kblade.kblade_set_power_averaging_time = dlsym(kh, "kblade_set_power_averaging_time");
	kblade.kblade_get_attenuation_raw = dlsym(kh, "kblade_get_attenuation_raw");
	kblade.kblade_get_attenuation = dlsym(kh, "kblade_get_attenuation");
	kblade.kblade_set_attenuation_raw = dlsym(kh, "kblade_set_attenuation_raw");
	kblade.kblade_set_attenuation = dlsym(kh, "kblade_set_attenuation");
	kblade.kblade_get_attenuation_offset = dlsym(kh, "kblade_get_attenuation_offset");
	kblade.kblade_set_attenuation_offset = dlsym(kh, "kblade_set_attenuation_offset");
	kblade.kblade_get_attenuation_velocity = dlsym(kh, "kblade_get_attenuation_velocity");
	kblade.kblade_set_attenuation_velocity = dlsym(kh, "kblade_set_attenuation_velocity");
	kblade.kblade_get_attenuator_temperature = dlsym(kh, "kblade_get_attenuator_temperature");
	kblade.kblade_get_wavelength = dlsym(kh, "kblade_get_wavelength");
	kblade.kblade_set_wavelength = dlsym(kh, "kblade_set_wavelength");
	kblade.kblade_get_ctrl_mode = dlsym(kh, "kblade_get_ctrl_mode");
	kblade.kblade_set_ctrl_mode = dlsym(kh, "kblade_set_ctrl_mode");
	kblade.kblade_get_locked = dlsym(kh, "kblade_get_locked");
	kblade.kblade_init = dlsym(kh, "kblade_init");
	kblade.kblade_deinit = dlsym(kh, "kblade_deinit");
	kblade.kblade_get_setting = dlsym(kh, "kblade_get_setting");

	kblade.init = true;
	return 0;
}

// dummy callback which responds to any call by printing the data it received
// and returning "5" for any query
int execDummy(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)
{
	spLogDebug(1, "%s callback with TaskID: %d, blade: %u, channel: %u", __func__, task, blade, channel);
	size_t n = 0;
	for (;numValues>0;numValues--) {
		spLogDebug(1, "value%d: %f", numValues-1, values[numValues-1]);
	}
	if (task>=TASK_QUERY) {
		n += snprintf(response+n, max-n, "*********");
	}
	return n;
}

enum attenuation_mode_t
{
	mode_offset = 0,	// attenuation = relative_atten + insertion_loss + attenuation_offset 	(actual attenuation of user system)
	mode_absolute,		// attenuation = relative_atten + insertion_loss 						(actual attenuation of unit)
	mode_relative		// attenuation = relative_atten											(min attenuation = 0)
};

int kblade_get_wavelength_wrapper(int blade_id, int channel, unsigned int* set, unsigned int* def, unsigned int* list, unsigned int* list_len)
{
	int i;
	int e = 0;
	if (list_len!=NULL&&list!=NULL) {
		// get list from kblade settings
		float _list[100];
		char* wavelengths_path = "settings.wavelengths";
		int e = kblade.kblade_get_setting(blade_id, wavelengths_path, strnlen(wavelengths_path, 1000), _list, 3*sizeof(float), false);
		if (e<0) {
			spLogError("blade: %d, channel %d, Error while reading 'settings.wavelengths'. Unexpected number of wavelengths, not 3", blade_id, channel);
			return e;
		}
		list[0] = (unsigned int)(1e9*_list[0]+0.5);
		list[1] = (unsigned int)(1e9*_list[1]+0.5);
		list[2] = (unsigned int)(1e9*_list[2]+0.5);
		*list_len = 3;
	}

	float _set, _def, _min, _max;
	e = kblade.kblade_get_wavelength(blade_id, channel, &_set, &_def, &_min, &_max);
	if (e<0) {
		return e;
	}
	if (set!=NULL)
		*set = (unsigned int)(1e9*_set+0.5);	// adding 0.5 and truncating is equivalent to rounding to the nearest integer
	if (def!=NULL)
		*def = (unsigned int)(1e9*_def+0.5);
	return 0;
}

int kblade_set_wavelength_wrapper(int blade_id, int channel_id, unsigned int wavelength)
{
	return kblade.kblade_set_wavelength(blade_id, channel_id, ((float)wavelength)*1e-9);
}

// if there are multiple blades then we must keep a set for each blade
// for now we are dealing with benchtop units only
static struct channel_state_t
{
	float attenuation_offset;
	enum attenuation_mode_t attenuation_mode;
} channel_state[4] = {
	 {.attenuation_mode = mode_absolute},
	 {.attenuation_mode = mode_absolute},
	 {.attenuation_mode = mode_absolute},
	 {.attenuation_mode = mode_absolute}
};

int kblade_get_insertion_loss_wrapper(int blade_id, int channel, float* insertion_loss)
{
	unsigned int _set;
	unsigned int list[100];
	unsigned int list_len;
	int e = kblade_get_wavelength_wrapper(blade_id, channel, &_set, NULL, list, &list_len);
	if (e<0) {
		return e;
	}
	int i;
	for (i=0; i<list_len; i++) {
		if (list[i]==_set) {
			break;
		}
	}
	if (i==list_len) {
		return SCPISERVER_ERR_INVALID_WAVELENGTH_SET;
	}
	char path[100];
	int len = snprintf(path, 100, "settings.channel[%d].attenuation.wavelength[%d].insertion_loss", channel, i);
	float _insertion_loss;
	e = kblade.kblade_get_setting(blade_id, path, len, &_insertion_loss, sizeof(float), false);
	if (e<0) {
		return e;
	}
	if (insertion_loss) {
		*insertion_loss = _insertion_loss;
	}
	return 0;
}

int execInsertionLoss_dB(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)
{
	spLogDebug(1, "%s callback with TaskID: %d, blade: %u, channel: %u", __func__, task, blade, channel);
	size_t n = 0;
	float _insertion_loss;

	if (((1<<blade)&(kblade.initializedBladeMask))==0) {
		return SCPISERVER_ERR_INVALID_BLADE_ID;
	}
	int e = kblade_get_insertion_loss_wrapper(blade, channel-1, &_insertion_loss);
	if (e<0) {
		spLogError("Error, blade: %u, channel: %u, Blade returned error code while getting insertion loss: %d", blade, channel, e);
		return e;
	}

	switch(task) {
	case TASK_QUERY:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_ATTEN, _insertion_loss);
		break;
	default:
		return SCPISERVER_ERR_TASK_UNDEFINED;
	}

	return n;
}

int execAttenuation_raw(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)
{
	spLogDebug(1, "%s callback with TaskID: %d, blade: %u, channel: %u", __func__, task, blade, channel);
	size_t n = 0;
	float _min, _max, _set, _act, _value;

	if (((1<<blade)&(kblade.initializedBladeMask))==0) {
		return SCPISERVER_ERR_INVALID_BLADE_ID;
	}
	int e = kblade.kblade_get_attenuation_raw(blade, channel-1, &_set, &_act, &_min, &_max);
	if (e<0) {
		spLogError("Error, blade: %u, channel: %u, Blade returned error code while getting attenuation raw: %d", blade, channel, e);
		return e;
	}
	// ####### Hard coded until blade knows about actual attenuation ########
	//_act = _set;
	// ######################################################################

	switch(task) {
	case SET_MIN:
		e = kblade.kblade_set_attenuation_raw(blade, channel-1, _min);
		break;
	case SET_MAX:
		e = kblade.kblade_set_attenuation_raw(blade, channel-1, _max);
		break;
	case TASK_SET:
		if (numValues!=1) {
			return COMMAND_ERROR;
		}
		e = kblade.kblade_set_attenuation_raw(blade, channel-1, values[0]);
		break;
	case TASK_MIN:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_ATTEN, _min);
		break;
	case TASK_MAX:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_ATTEN, _max);
		break;
	case TASK_SET_POINT:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_ATTEN, _set);
		break;
	case TASK_QUERY:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_ATTEN, _act);
		break;
	case TASK_ALL:
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_ATTEN, _min);
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_ATTEN, _max);
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_ATTEN, _set);
		n += snprintf(response+n, max-n, "%.*f" , FLOAT_PRECISION_ATTEN, _act);
		break;
	default:
		e = SCPISERVER_ERR_TASK_UNDEFINED;
	}

	if (e<0) {
		spLogError("Error, blade: %u, channel: %u, Blade returned error code while setting attenuation raw: %d", blade, channel, e);
		return e;
	}
	return n;
}

int execAttenuation_dB(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)
{
	spLogDebug(1, "%s callback with TaskID: %d, blade: %u, channel: %u", __func__, task, blade, channel);
	size_t n = 0;
	float _min, _max, _def, _set, _act, _value;

	if (((1<<blade)&(kblade.initializedBladeMask))==0) {
		return SCPISERVER_ERR_INVALID_BLADE_ID;
	}
	int e = kblade.kblade_get_attenuation(blade, channel-1, &_set, &_act, &_def, &_min, &_max);
	if (e<0) {
		spLogError("Error, blade: %u, channel: %u, Blade returned error code while getting attenuation: %d", blade, channel, e);
		return e;
	}

	// ####### Hard coded until blade knows about actual attenuation ########
	//_act = _set;
	// ######################################################################

	switch(task) {
	case SET_MIN:
		e = kblade.kblade_set_attenuation(blade, channel-1, _min);
		break;
	case SET_MAX:
		e = kblade.kblade_set_attenuation(blade, channel-1, _max);
		break;
	case SET_DEFAULT:
		e = kblade.kblade_set_attenuation(blade, channel-1, _def);
		break;
	case TASK_SET:
		if (numValues!=1) {
			return COMMAND_ERROR;
		}
		e = kblade.kblade_set_attenuation(blade, channel-1, values[0]);
		break;
	case TASK_MIN:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_ATTEN, _min);
		break;
	case TASK_MAX:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_ATTEN, _max);
		break;
	case TASK_DEFAULT:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_ATTEN, _def);
		break;
	case TASK_SET_POINT:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_ATTEN, _set);
		break;
	case TASK_QUERY:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_ATTEN, _act);
		break;
	case TASK_ALL:
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_ATTEN, _min);
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_ATTEN, _max);
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_ATTEN, _def);
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_ATTEN, _set);
		n += snprintf(response+n, max-n, "%.*f" , FLOAT_PRECISION_ATTEN, _act);
		break;
	default:
		e = SCPISERVER_ERR_TASK_UNDEFINED;
	}

	if (e<0) {
		spLogError("Error, blade: %u, channel: %u, Blade returned error code while setting attenuation: %d", blade, channel, e);
		return e;
	}
	return n;
}

int execAttenuatorTemp_C(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)
{
	spLogDebug(1, "%s callback with TaskID: %d, blade: %u, channel: %u", __func__, task, blade, channel);
	size_t n = 0;
	float _min, _max, _act;

	if (((1<<blade)&(kblade.initializedBladeMask))==0) {
		return SCPISERVER_ERR_INVALID_BLADE_ID;
	}
	int e = kblade.kblade_get_attenuator_temperature(blade, channel-1, &_act, &_min, &_max);
	if (e<0) {
		spLogError("Error, blade: %u, channel: %u, Blade returned error code while getting Attenuator temp: %d", blade, channel, e);
		return e;
	}

	switch(task) {
	case TASK_MIN:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_TEMP, _min);
		break;
	case TASK_MAX:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_TEMP, _max);
		break;
	case TASK_QUERY:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_TEMP, _act);
		break;
	case TASK_ALL:
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_TEMP, _min);
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_TEMP, _max);
		n += snprintf(response+n, max-n, "%.*f" , FLOAT_PRECISION_TEMP, _act);
		break;
	default:
		e = SCPISERVER_ERR_TASK_UNDEFINED;
	}
	return n;
}

int execPower_raw(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)
{
	spLogDebug(1, "%s callback with TaskID: %d, blade: %u, channel: %u", __func__, task, blade, channel);
	size_t n = 0;
	float _min, _max, _act, _value;

	if (((1<<blade)&(kblade.initializedBladeMask))==0) {
		return SCPISERVER_ERR_INVALID_BLADE_ID;
	}
	int e = kblade.kblade_get_power_raw(blade, channel-1, &_act, &_min, &_max);
	if (e<0) {
		spLogError("Error, blade: %u, channel: %u, Blade returned error code while getting power raw: %d", blade, channel, e);
		return e;
	}

	switch(task) {
	case TASK_MIN:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_POWER, _min);
		break;
	case TASK_MAX:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_POWER, _max);
		break;
	case TASK_QUERY:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_POWER, _act);
		break;
	case TASK_ALL:
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_POWER, _min);
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_POWER, _max);
		n += snprintf(response+n, max-n, "%.*f" , FLOAT_PRECISION_POWER, _act);
		break;
	default:
		e = SCPISERVER_ERR_TASK_UNDEFINED;
	}
	return n;
}

int execPower_dBm(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)
{
	spLogDebug(1, "%s callback with TaskID: %d, blade: %u, channel: %u", __func__, task, blade, channel);
	size_t n = 0;
	float _min, _max, _def, _set, _act, _value;

	if (((1<<blade)&(kblade.initializedBladeMask))==0) {
		return SCPISERVER_ERR_INVALID_BLADE_ID;
	}
	int e = kblade.kblade_get_power(blade, channel-1, &_set, &_act, &_def, &_min, &_max);
	if (e<0) {
		spLogError("Error, blade: %u, channel: %u, Blade returned error code while getting power: %d", blade, channel, e);
		return e;
	}
	if (_act<_min) {		// when no light is on the tap we should show the user -99.99, which is code for "off"
		_act = -99.99;
	}

	switch(task) {
	case SET_MIN:
		e = kblade.kblade_set_power(blade, channel-1, _min);
		break;
	case SET_MAX:
		e = kblade.kblade_set_power(blade, channel-1, _max);
		break;
	case SET_DEFAULT:
		e = kblade.kblade_set_power(blade, channel-1, _def);
		break;
	case TASK_SET:
		if (numValues!=1) {
			return COMMAND_ERROR;
		}
		e = kblade.kblade_set_power(blade, channel-1, values[0]);
		break;
	case TASK_MIN:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_POWER, _min);
		break;
	case TASK_MAX:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_POWER, _max);
		break;
	case TASK_DEFAULT:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_POWER, _def);
		break;
	case TASK_SET_POINT:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_POWER, _set);
		break;
	case TASK_QUERY:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_POWER, _act);
		break;
	case TASK_ALL:
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_POWER, _min);
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_POWER, _max);
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_POWER, _def);
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_POWER, _set);
		n += snprintf(response+n, max-n, "%.*f" , FLOAT_PRECISION_POWER, _act);
		break;
	default:
		e = SCPISERVER_ERR_TASK_UNDEFINED;
	}

	if (e<0) {
		spLogError("Error, blade: %u, channel: %u, Blade returned error code while setting power: %d", blade, channel, e);
		return e;
	}
	return n;
}

int execWavelength_nm(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)
{
	spLogDebug(1, "%s callback with TaskID: %d, blade: %u, channel: %u", __func__, task, blade, channel);
	size_t n = 0;
	unsigned int _set, _def;
	unsigned int _list[100];
	unsigned int _list_len = 0;
	int i;

	if (((1<<blade)&(kblade.initializedBladeMask))==0) {
		return SCPISERVER_ERR_INVALID_BLADE_ID;
	}
	int e = kblade_get_wavelength_wrapper(blade, channel-1, &_set, &_def, _list, &_list_len);
	if (e<0) {
		spLogError("Error, blade: %u, channel: %u, Blade returned error code while getting wavelength: %d", blade, channel, e);
		return e;
	}

	switch(task) {
	case TASK_SET:
		if (numValues!=1) {
			return COMMAND_ERROR;
		}
		e = kblade_set_wavelength_wrapper(blade, channel-1, (unsigned int)(values[0]));
		break;
	case TASK_QUERY:
		n += snprintf(response+n, max-n, "%u", _set);
		break;
	case TASK_LIST:
		n += snprintf(response+n, max-n, "%u", _list[0]);
		for (i=1; i<_list_len; ++i) {
			n += snprintf(response+n, max-n, ",%u", _list[i]);
		}
		break;
	default:
		e = SCPISERVER_ERR_TASK_UNDEFINED;
	}

	if (e<0) {
		spLogError("Error, blade: %u, channel: %u, Blade returned error code while setting wavelength: %d", blade, channel, e);
		return e;
	}
	return n;
}

int execControlMode(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)
{
	spLogDebug(1, "%s callback with TaskID: %d, blade: %u, channel: %u", __func__, task, blade, channel);
	size_t n = 0;
	int e, _mode;

	if (((1<<blade)&(kblade.initializedBladeMask))==0) {
		return SCPISERVER_ERR_INVALID_BLADE_ID;
	}
	switch(task) {
	case TASK_QUERY:
		e = kblade.kblade_get_ctrl_mode(blade, channel-1, (enum kblade_ctrl_mode_t*)&_mode);
		if (e<0) {
			spLogError("Error, blade: %u, channel: %u, Blade returned error code while getting control mode: %d", blade, channel, e);
			return e;
		}
		if (_mode)
			n += snprintf(response+n, max-n, "POWER");			// 1 = power tracking
		else
			n += snprintf(response+n, max-n, "ATTENUATION");	// 0 = attenuation
		break;
	case SET_ATT:
		e = kblade.kblade_set_ctrl_mode(blade, channel-1, KBLADE_CTRL_MODE_ATTENUATION);	// attenuation
		break;
	case SET_POW:
		e = kblade.kblade_set_ctrl_mode(blade, channel-1, KBLADE_CTRL_MODE_POWER);	// power (tracking)
		break;
	default:
		e = SCPISERVER_ERR_TASK_UNDEFINED;
	}

	if (e<0) {
		spLogError("Error, blade: %u, channel: %u, Blade returned error code while setting control mode: %d", blade, channel, e);
		return e;
	}
	return n;
}

// When run this will lock the unit and perform the dark current nulling routine
int execDarkNulling(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)
{
	spLogDebug(1, "%s callback with TaskID: %d, blade: %u, channel: %u", __func__, task, blade, channel);
	size_t n = 0;
	int e;

	if (((1<<blade)&(kblade.initializedBladeMask))==0) {
		return SCPISERVER_ERR_INVALID_BLADE_ID;
	}
	// task==TASK_COMMAND only

	spLogNotice("Dark current nulling in progress...");
	e = kblade.kblade_start_dark_power_measurement(blade, channel-1);
	if (e<0) {
		spLogError("Error, blade: %u, channel: %u, Blade returned error code while running Dark Current Nulling: %d", blade, channel, e);
		return e;
	}

	return n;
}

int execDarkPower_dBm(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)
{
	spLogDebug(1, "%s callback with TaskID: %d, blade: %u, channel: %u", __func__, task, blade, channel);
	size_t n = 0;
	float _set, _min, _max, _raw;

	if (((1<<blade)&(kblade.initializedBladeMask))==0) {
		return SCPISERVER_ERR_INVALID_BLADE_ID;
	}
	int e = kblade.kblade_get_dark_power(blade, channel-1, &_set, &_min, &_max, &_raw);
	if (e<0) {
		spLogError("Error, blade: %u, channel: %u, Blade returned error code while getting Dark Power raw: %d", blade, channel, e);
		return e;
	}

	switch(task) {
	case SET_MIN:
		e = kblade.kblade_set_dark_power(blade, channel-1, _min);
		break;
	case SET_MAX:
		e = kblade.kblade_set_dark_power(blade, channel-1, _max);
		break;
	case TASK_SET:
		if (numValues!=1) {
			return COMMAND_ERROR;
		}
		e = kblade.kblade_set_dark_power(blade, channel-1, values[0]);
		break;
	case TASK_MIN:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_POWER, _min);
		break;
	case TASK_MAX:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_POWER, _max);
		break;
	case TASK_SET_POINT:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_POWER, _set);
		break;
	case TASK_QUERY:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_POWER, _raw);
		break;
	case TASK_ALL:
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_POWER, _min);
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_POWER, _max);
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_POWER, _set);
		n += snprintf(response+n, max-n, "%.*f" , FLOAT_PRECISION_POWER, _raw);
		break;
	default:
		e = SCPISERVER_ERR_TASK_UNDEFINED;
	}
	if (e<0) {
		spLogError("Error, blade: %u, channel: %u, Blade returned error code while setting Dark Power raw: %d", blade, channel, e);
		return e;
	}
	return n;
}

int execAveragingTime_s(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)
{
	spLogDebug(1, "%s callback with TaskID: %d, blade: %u, channel: %u", __func__, task, blade, channel);
	size_t n = 0;
	float _min, _max, _def, _set;

	if (((1<<blade)&(kblade.initializedBladeMask))==0) {
		return SCPISERVER_ERR_INVALID_BLADE_ID;
	}
	int e = kblade.kblade_get_power_averaging_time(blade, channel-1, &_set, &_def, &_min, &_max);
	if (e<0) {
		spLogError("Error, blade: %u, channel: %u, Blade returned error code while getting power averaging time: %d", blade, channel, e);
		return e;
	}

	switch(task) {
	case SET_MIN:
		e = kblade.kblade_set_power_averaging_time(blade, channel-1, _min);
		break;
	case SET_MAX:
		e = kblade.kblade_set_power_averaging_time(blade, channel-1, _max);
		break;
	case SET_DEFAULT:
		e = kblade.kblade_set_power_averaging_time(blade, channel-1, _def);
		break;
	case TASK_SET:
		if (numValues!=1) {
			return COMMAND_ERROR;
		}
		e = kblade.kblade_set_power_averaging_time(blade, channel-1, values[0]);
		break;
	case TASK_QUERY:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_POWER, _set);
		break;
	case TASK_DEFAULT:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_POWER, _def);
		break;
	case TASK_MIN:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_POWER, _min);
		break;
	case TASK_MAX:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_POWER, _max);
		break;
	case TASK_ALL:
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_POWER, _min);
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_POWER, _max);
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_POWER, _def);
		n += snprintf(response+n, max-n, "%.*f" , FLOAT_PRECISION_POWER, _set);
		break;
	default:
		e = SCPISERVER_ERR_TASK_UNDEFINED;
	}

	if (e<0) {
		spLogError("Error, blade: %u, channel: %u, Blade returned error code while setting power averaging time: %d", blade, channel, e);
		return e;
	}
	return n;
}

int execAttenMode(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)
{
	spLogDebug(1, "%s callback with TaskID: %d, blade: %u, channel: %u", __func__, task, blade, channel);
	size_t n = 0;
	enum attenuation_mode_t _mode;
	int e = 0;
	float insertion_loss;

	if (((1<<blade)&(kblade.initializedBladeMask))==0) {
		return SCPISERVER_ERR_INVALID_BLADE_ID;
	}
	switch(task) {
	case TASK_QUERY:
		_mode = channel_state[channel-1].attenuation_mode;
		if (mode_offset==_mode)
			n += snprintf(response+n, max-n, "OFFSET");
		else if (mode_absolute==_mode)
			n += snprintf(response+n, max-n, "ABSOLUTE");
		else
			n += snprintf(response+n, max-n, "RELATIVE");
		break;
	case SET_OFFSET:
		channel_state[channel-1].attenuation_mode = mode_offset;
		break;
	case SET_ABS:
		channel_state[channel-1].attenuation_mode = mode_absolute;
		e = kblade.kblade_set_attenuation_offset(blade, channel-1, 0);
		break;
	case SET_REL:
		e = kblade_get_insertion_loss_wrapper(blade, channel-1, &insertion_loss);
		if (e<0) {
			spLogError("Error, blade: %u, channel: %u, Blade returned error code while getting insertion loss: %d", blade, channel, e);
			return e;
		}
		channel_state[channel-1].attenuation_mode = mode_relative;
		e = kblade.kblade_set_attenuation_offset(blade, channel-1, -insertion_loss);
		break;
	default:
		e = SCPISERVER_ERR_TASK_UNDEFINED;
	}

	if (e<0) {
		spLogError("Error, blade: %u, channel: %u, Blade returned error code while setting Attenuation Mode: %d", blade, channel, e);
		return e;
	}
	return n;
}

int execAttenOffset_dB(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)
{
	spLogDebug(1, "%s callback with TaskID: %d, blade: %u, channel: %u", __func__, task, blade, channel);
	size_t n = 0;
	float _set;

	if (((1<<blade)&(kblade.initializedBladeMask))==0) {
		return SCPISERVER_ERR_INVALID_BLADE_ID;
	}
	int e = kblade.kblade_get_attenuation_offset(blade, channel-1, &_set);
	if (e<0) {
		spLogError("Error, blade: %u, channel: %u, Blade returned error code while getting attenuation offset: %d", blade, channel, e);
		return e;
	}

	switch(task) {
	case TASK_SET:
		if (numValues!=1) {
			return COMMAND_ERROR;
		}
		e = kblade.kblade_set_attenuation_offset(blade, channel-1, values[0]);
		channel_state[channel-1].attenuation_mode = mode_offset;
		break;
	case TASK_QUERY:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_ATTEN, _set);
		break;
	default:
		e = SCPISERVER_ERR_TASK_UNDEFINED;
	}

	if (e<0) {
		spLogError("Error, blade: %u, channel: %u, Blade returned error code while setting attenuation offset: %d", blade, channel, e);
		return e;
	}
	return n;
}

int execAttenVelocity_dBps(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)
{
	spLogDebug(1, "%s callback with TaskID: %d, blade: %u, channel: %u", __func__, task, blade, channel);
	size_t n = 0;
	float _set, _act, _def, _min, _max;

	if (((1<<blade)&(kblade.initializedBladeMask))==0) {
		return SCPISERVER_ERR_INVALID_BLADE_ID;
	}
	int e = kblade.kblade_get_attenuation_velocity(blade, channel-1, &_set, &_act, &_def, &_min, &_max);
	if (e<0) {
		spLogError("Error, blade: %u, channel: %u, Blade returned error code while getting attenuation velocity: %d", blade, channel, e);
		return e;
	}

	switch(task) {
	case TASK_SET:
		if (numValues!=1) {
			return COMMAND_ERROR;
		}
		e = kblade.kblade_set_attenuation_velocity(blade, channel-1, values[0]);
		break;
	case TASK_SET_POINT:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_ATTEN, _set);
		break;
	case TASK_QUERY:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_ATTEN, _act);
		break;
	case TASK_DEFAULT:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_ATTEN, _def);
		break;
	case TASK_MIN:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_ATTEN, _min);
		break;
	case TASK_MAX:
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_ATTEN, _max);
		break;
	case TASK_ALL:
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_ATTEN, _min);
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_ATTEN, _max);
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_ATTEN, _def);
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_ATTEN, _set);
		n += snprintf(response+n, max-n, "%.*f" , FLOAT_PRECISION_ATTEN, _act);
		break;
	default:
		e = SCPISERVER_ERR_TASK_UNDEFINED;
	}

	if (e<0) {
		spLogError("Error, blade: %u, channel: %u, Blade returned error code while setting attenuation velocity: %d", blade, channel, e);
		return e;
	}
	return n;
}


// GPIB:
int placeholder_get_blademodelnumber(int blade_id, char* result)
{
	snprintf(result, 128, "FVA-3800-B");
	return 0;
}

int placeholder_get_bladeserialnumber(int blade_id, char* result)
{
	snprintf(result, 128, "CS123-B");
	return 0;
}

int execIDNSlot(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)
{
	spLogDebug(1, "executing IDN Slot");
	// companyName,ModelNumber,SerialNumber,1.6
	size_t n=0;
	int e;
	char result[128];
	uint16_t major, minor;
	if (((1<<blade)&(kblade.initializedBladeMask))==0) {
		return SCPISERVER_ERR_INVALID_BLADE_ID;
	}
	n += snprintf(response+n, max-n, "%s,", getCompanyNameString());
	if ((e=placeholder_get_blademodelnumber(blade, result))<0)
		return e;
	n += snprintf(response+n, max-n, "%.128s,", result);
	if ((e=placeholder_get_bladeserialnumber(blade, result))<0)
		return e;
	n += snprintf(response+n, max-n, "%.128s,", result);
	if ((e=kblade.kblade_get_fw_version(blade, &major, &minor))<0)
		return e;
	n += snprintf(response+n, max-n, "%u", major);
	n += snprintf(response+n, max-n, ".%u", minor);
	return n;
}

int execOPC(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)
{
	spLogDebug(1, "executing OPC");
	int e = 0;
	size_t n = 0;
	bool _opc = true;
	
	int c;
	for (c=0; c<4; ++c) {
		float _time;
		uint8_t _alock;
		uint8_t _plock;
		int _mode;
		e = kblade.kblade_get_dark_power_time_remaining(15, c, &_time);
		if (e<0)
			break;
		spLogDebug(1, "Dark current nulling time remaining: %f", _time);

		e = kblade.kblade_get_locked(15, c, &_alock, &_plock);
		if (e<0)
			break;
		spLogDebug(1, "attenuation locked: %d power locked: %d", _alock, _plock);

		e = kblade.kblade_get_ctrl_mode(15, c, (enum kblade_ctrl_mode_t*)&_mode);
		if (e<0)
			break;

		if ((_time!=0)||!((_mode&&_plock)||((!_mode)&&_alock))) {
			_opc = false;
			//break;
		}
	}	
	if (e<0) {
		spLogError("Error, blade: %u, channel: %u, Blade returned error code while getting ocp: %d", blade, channel, e);
		return e;
	}

	if (_opc) {
		n += snprintf(response+n, max-n, "1");
	} else {
		n += snprintf(response+n, max-n, "0");
	}
	return n;
}

int execOPCSlot(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)
{
	spLogDebug(1, "executing OPC Slot");
	int e = 0;
	size_t n = 0;
	if (((1<<blade)&(kblade.initializedBladeMask))==0) {
		return SCPISERVER_ERR_INVALID_BLADE_ID;
	}

	n += snprintf(response+n, max-n, "1");
	return n;
}

int execOPT(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)
{
	spLogDebug(1, "executing OPT");
	int e = 0;
	size_t n = 0;
	// this is where we expect the blade to be
	if (kblade.initializedBladeMask==(1<<15)){
		char result[128];
		if ((e=placeholder_get_blademodelnumber(15, result))<0)
			return e;
		n += snprintf(response+n, max-n, "%.128s", result);
	} else if (kblade.initializedBladeMask!=0) {
		n += snprintf(response+n, max-n, "InvalidSlot");
	} else {
		n += snprintf(response+n, max-n, "ERR");
	}
	return n;
}

int execOPTSlot(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)
{
	spLogDebug(1, "executing OPT Slot");
	size_t n = 0;
	uint8_t _mask;

	if (((1<<blade)&(kblade.initializedBladeMask))==0) {
		return SCPISERVER_ERR_INVALID_BLADE_ID;
	}

	char* channel_mask_path = "settings.channel_mask";
	int e = kblade.kblade_get_setting(blade, channel_mask_path, strnlen(channel_mask_path, 1000), &_mask, 1, false);

	int i;
	for(i=0;; i++) {
		if (_mask&(1<<i)) {
			n += snprintf(response+n, max-n, "1");
		}
		if (i>=3)
			break;
		n += snprintf(response+n, max-n, ",");
	}

	return n;
}

int execRST(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)
{
	spLogDebug(1, "executing *RST");

	unsigned int _wav;
	float _def;
	unsigned int _list[100];
	unsigned int _list_len = 0;

	int mask = kblade.initializedBladeMask;

	// Reset things
	int e, b, c;
	for(b=0; mask!=0; mask=mask>>1, ++b) {
		if (mask&1) {
			spLogDebug(1, "Blade %d is being reset", b);
			for (c = 0; c<4; ++c) {
				e = kblade.kblade_set_ctrl_mode(b, c, 0);	// default is attenuation mode
				if (e<0)
					return e;
				e = kblade_get_wavelength_wrapper(b, c, NULL, &_wav, NULL, NULL);
				if (e<0)
					return e;
				if (_list_len!=0)
					e = kblade_set_wavelength_wrapper(b, c, _wav);
				if (e<0)
					return e;
				e = kblade.kblade_get_power(b, c, NULL, NULL, &_def, NULL, NULL);
				if (e<0)
					return e;
				e = kblade.kblade_set_power(b, c, _def);
				if (e<0)
					return e;
				e = kblade.kblade_get_attenuation(b, c, NULL, NULL, &_def, NULL, NULL);
				if (e<0)
					return e;
				e = kblade.kblade_set_attenuation(b, c, _def);
				if (e<0)
					return e;
			}
			spLogDebug(1, "Blade %d has been reset", b);
		}
	}
	return 0;
}

int execTST(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)
{
	spLogDebug(1, "executing *TST?");
	size_t n = 0;
	int status;

	if (kblade.initializedBladeMask==0) {
		status = TST_ERR_COULD_NOT_INITIALIZE_HARDWARE;
	} else {
		// success
		status = 0;
	}

	n += snprintf(response+n, max-n, "%d", status);
	return n;
}

// registers all the callbacks that are implemented by this file
void registerAllCallbacks()
{
	//registerCallback(ID_READDC, execDummy);
	//registerCallback(ID_COLZERO, execDummy);
	//registerCallback(ID_ACLSTATE, execDummy);
	//registerCallback(ID_LOCK, execDummy);
	//registerCallback(ID_CALZERO, execDummy);
	registerCallback(ID_ATTEN_MODE, execAttenMode);
	registerCallback(ID_CONTMODE, execControlMode); // execDummyMode);
	//registerCallback(ID_ARESOLUTION, execDummy);
	registerCallback(ID_INSERTION_LOSS_DB, execInsertionLoss_dB);
	registerCallback(ID_ATTENUATION_DB, execAttenuation_dB);
	registerCallback(ID_ATTENUATION_RAW, execAttenuation_raw);
	registerCallback(ID_ATTENUATOR_TEMP_C, execAttenuatorTemp_C);
	registerCallback(ID_INOFFSET, execAttenOffset_dB);
	registerCallback(ID_ATTEN_VEL_DBPS, execAttenVelocity_dBps);
	//registerCallback(ID_RATTENUATION, execDummy);
	//registerCallback(ID_INREFERENCE, execDummy);
	registerCallback(ID_WAVELENGTH_NM, execWavelength_nm);
	//registerCallback(ID_APMODE, execDummy);
	//registerCallback(ID_DTOLERANCE, execDummy);
	//registerCallback(ID_OUTOFFSET, execDummy);
	registerCallback(ID_POWER_DBM, execPower_dBm);
	registerCallback(ID_POWER_RAW, execPower_raw);
	registerCallback(ID_DARK_POWER_DBM,		execDarkPower_dBm);
	registerCallback(ID_START_DARK_NULLING,	execDarkNulling);
	registerCallback(ID_AVERAGING_TIME_S, execAveragingTime_s);
	//registerCallback(ID_OUTREFERENCE, execDummy);
	//registerCallback(ID_RPOWER, execDummy);
	//registerCallback(ID_STATE, execDummy);

	registerCallback(ID_IDN_SLOT, execIDNSlot);
	registerCallback(ID_OPC_SLOT, execOPCSlot);
	registerCallback(ID_OPT_SLOT, execOPTSlot);

	registerCallback(ID_OPC, execOPC);
	registerCallback(ID_OPT, execOPT);
	registerCallback(ID_TST, execTST);
	registerCallback(ID_RST, execRST);
	registerCallback(ID_PRES, execRST);
}

int initBackend(uint16_t mask, bool fakekblade)
{
	if (loadKbladeFunctions(fakekblade)<0) {
		return -2;
	}

	int e;
	if ((e=kblade.kblade_init(&mask))<0) {
		spLogError("Error: Could not init blades. kblade_init() returned error code %d", e);
		return -1;
	}
	spLogDebug(1, "kblade_init successfully initialized blades, mask: %X", mask);
	kblade.initializedBladeMask = mask;
	registerAllCallbacks();
	return 0;
}

int deinitBackend()
{
	kblade.kblade_deinit(kblade.initializedBladeMask);
	return 0;
}


