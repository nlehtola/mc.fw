#ifndef BACKENDCALLBACKS_H
#define BACKENDCALLBACKS_H

#include <stdint.h>
#include <stdbool.h>

int deinitBackend();
int initBackend(uint16_t mask, bool fakekblade);

#endif //BACKENDCALLBACKS_H
