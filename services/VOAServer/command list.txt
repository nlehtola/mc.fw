// The GPIB commands are described in detail in the SCPI/488.2 doc
// I have tried to be as close to that as possible
// TST, OPC and WAI currently do the bare minimum, which is effectively nothing
*CLS
*ESE <bitmask>|?
*ESR?
*IDN?
*OPC|?
*RST
*WAI
*STB?
*SRE <bitmask>|?
*TST?
*OPT?
	returns "FVA-3800-B"

// remember, everything is case insensitive (including units)
// UPPERlower case describes SHORTlong form
// A new feature which the FLS didn't have is being able to set to
// MIN/MAX/DEFAULT by saying just the word e.g.:
// ":INPUT1:ATT MIN" is now actually a valid command
// Another change is that the leading : actually does what it's supposed to
// If a command has a leading : it means its absolute, otherwise it is relative
// to the previous command. "Previous command" only applies when you do this:
// <command>;<command>;<command>
// Also units are supported when setting a value now, which is handy when
// changing between nm, m etc instead of saying 1550e-9
// the default unit is the first one in the lists below (can be ommitted)

:SYSTem:VERSion?
		returns "1990.0", SCPI compliance version number

:SLOT<slot>:OPC|?
	returns "1"
:SLOT<slot>:OPTions?
	returns "1,1,1,1", channel mask for that slot
:SLOT<slot>:IDN?

:CONTrol<chan>:MODE ATTenuation|POWer
:CONTrol<chan>:MODE?
	returns "ATTENUATION" or "POWER"

:INPup<chan>:ATTenuation MINimum|MAXimum|DEFault|<value>[DB|MDB]
:INPup<chan>:ATTenuation? [MINimum|MAXimum|DEFault|SET|ALL]
	all returns <min>,<max>,<def>,<set>,<act>
:INPup<chan>:ATTenuation:VELocity MINimum|MAXimum|DEFault|<value>[DBPS|MDBPS]
:INPup<chan>:ATTenuation:VELocity? [MINimum|MAXimum|DEFault|SET|ALL]
	all returns <min>,<max>,<def>,<set>,<act>
:INPut<chan>:OFFSet <value>[DB|MDB]
:INPut<chan>:OFFSet?
	returns <set point>
:INPut<chan>:AMODE ABSolute|RELative|OFFSET
:INPut<chan>:AMODE?
	returns "ABSOLUTE" or "RELATIVE" or "OFFSET"
:INPut<chan>:WAVElength <wavelength>[NM|M|MM|UM|PM]
:INPut<chan>:WAVElength? [LIST]
	returns <wavelength>
	list returns <w1>,<w2>,<w3>,etc.	//which is currently 1310,1490,1550

:OUTPut<chan>:POWer MINimum|MAXimum|DEFault|<value>[DBM|MDBM]
:OUTPut<chan>:POWer? [MINimum|MAXimum|DEFault|SET|ALL]
	all returns <min>,<max>,<def>,<set>,<act>
:OUTPut<chan>:POWer:AVERagingtime MINimum|MAXimum|DEFault|<value>[S|MS|US|NS]
:OUTPut<chan>:POWer:AVERagingtime? [MINimum|MAXimum|DEFault|ALL]
	all returns <min>,<max>,<def>,<set>
:OUTPut<chan>:POWer:NULLing		//this is just a command which starts dark power nulling, use *OPC? to see when it has completed

// secret commands (don't put in manual)
:RAW<chan>:ATTenuation (set raw dac value from 0-65535)
:RAW<chan>:POWer (get raw adc value for 0.0-1.0)
:RAW<chan>:TEMPerature? (get temperature of voa)
:RAW<chan>:LOSS (insertion loss)
:RAW<chan>:DARKpower (get/set dark power)
:RAW<chan>:NULLing (alias for :OUTPut<chan>:POWer:NULLing)
