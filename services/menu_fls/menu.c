#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <ncurses.h>
#include <menu.h>
#include <math.h>
#include <sched.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <syslog.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <iface.h>

// from vxi_local
#include <fls_commands.h>

#include "menu_fd.h"

#define ONE_MS (1)
#define ONE_SEC (1000*ONE_MS)

#define xFAKE_VXI
#define BLADE_ID (15)

#define NUM_LASERS (2)

#define DEFAULT_IDLE_TIMEOUT (10.0)
#define NET_IDLE_TIMEOUT (30.0)

static char mwd[256];

enum freq_units_t
{
	freq_units_thz,
	freq_units_nm
};


enum power_units_t
{
	power_units_dbm,
	power_units_mw,
};


struct tlsx_info_t
{
	struct laser_t
	{
		bool enabled;
		bool show_power_set_point;
		double power_set_point;
		double power_actual;
		double power_step_size;
		double power_min, power_max;
		bool show_freq_set_point;
		double freq_set_point;
		double freq_actual;
		double freq_min, freq_max;
		double freq_step_size;
		double freq_step_size_min, freq_step_size_max;
	} laser[NUM_LASERS];

	int laser_mask;

	enum freq_units_t freq_units;
	enum power_units_t power_units;

	pthread_t vxi_thread;
	pthread_mutex_t tlsx_info_mutex;
	pthread_mutex_t vxi_mutex;
	bool vxi_event;
} tlsx_info;


struct menu_info_t;


struct event_ret_t
{
	int code; // return code
	unsigned int flags;
};


// if ret.code < 0 this is how far down the menu stack to escape, ie
// to escape to the previous menu code times, to  escape to the start
// return ret.code=-BIG_NUMBER, where BIG_NUMBER >= menu stack depth;
// else return 0 to have the menu process the signal normally, or > 0
// to indicate the callback handled this.
// Also the flag field is for meta flags ie MENU_REPOST_HANDLED which
// tells the menu not to redraw itself as we have already done it in a 
// special way.
#define BIG_NUMBER (100)
#define MENU_REPOST_HANDLED (1)
typedef struct event_ret_t (*menu_event_cb_t)(int param, struct menu_info_t *info, enum menu_read_result event);


struct menu_item_t
{
	char name[32];
	char desc[32];
	bool skip;
	menu_event_cb_t cb;
	int cb_param;
};


static struct mfpio_t *mfpio;


static bool button_push(int button)
{
	#ifndef FAKE_VXI
	pthread_mutex_lock(&tlsx_info.vxi_mutex);

	// turn the safety off and enable the laser
	// this is kind of annoying since to release safety 
	// you must turn the laser on, but there isnt any 
	// better option
	vxi_set_safety(BLADE_ID, button, 0);
	vxi_set_state(BLADE_ID, button, 1);
	pthread_mutex_unlock(&tlsx_info.vxi_mutex);
	#endif
}


static void button_release(int button)
{
	#ifndef FAKE_VXI
	pthread_mutex_lock(&tlsx_info.vxi_mutex);

	// turn the laser off and lock it off via safety command 
	// (also turn off manually after for older blade firmware)
	vxi_set_safety(BLADE_ID, button, 1);
	vxi_set_state(BLADE_ID, button, 0);
	pthread_mutex_unlock(&tlsx_info.vxi_mutex);
	#endif
}


#define VXI_POLL_TIME (500000) // in us
void * vxi_poll(void *ptr)
{
	int l;
	struct vxi_info_t
	{
		struct vxi_laser_t
		{
			int state;
			double power_min, power_max;
			double power_set_point;
			double power_actual;
			double freq_min, freq_max;
			double freq_set_point;
			double freq_step_size;
			double freq_step_size_min, freq_step_size_max;
			int lock;
		} laser[NUM_LASERS];

		uint8_t laser_mask;
	} vxi_info;

	srand(time(NULL));
	
	// init the laser state based on the button position
	if (lcp_get_enabled(0))
		button_push(1);
	else
		button_release(1);
	if (lcp_get_enabled(1))
		button_push(2);
	else
		button_release(2);

	while (1)
	{
		vxi_stat r;
		bool vxi_scan_success = false;

		#ifdef FAKE_VXI
		vxi_scan_success = true;
		#else
		// populate the vxi_info 
		pthread_mutex_lock(&tlsx_info.vxi_mutex);

		r = vxi_get_laser_positions(BLADE_ID, &vxi_info.laser_mask);
		if(r != VXI_SUCCESS)
			goto vxi_scan_done;
		for (l = 0; l < NUM_LASERS; l++)
		{
			if ((vxi_info.laser_mask >> l) & 0x01)
			{
				struct vxi_laser_t *laser = &vxi_info.laser[l];
				r = vxi_get_power(BLADE_ID, l + 1, &laser->power_min, &laser->power_max, &laser->power_set_point, &laser->power_actual);
				if (r != VXI_SUCCESS)
					goto vxi_scan_done;
				r = vxi_get_frequency(BLADE_ID, l + 1, &laser->freq_min, &laser->freq_max, &laser->freq_set_point, &laser->lock);
				if (r != VXI_SUCCESS)
					goto vxi_scan_done;
				r = vxi_get_grid(BLADE_ID, l + 1, &laser->freq_step_size_min, &laser->freq_step_size_max, &laser->freq_step_size);
				if (r != VXI_SUCCESS)
					goto vxi_scan_done;
				r = vxi_get_state(BLADE_ID, l + 1, &laser->state);
				if(r != VXI_SUCCESS)
					goto vxi_scan_done;
			}
		}
		vxi_scan_success = true;

vxi_scan_done:
		pthread_mutex_unlock(&tlsx_info.vxi_mutex);
		#endif


		if (vxi_scan_success)
		{
			pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);

			#ifdef FAKE_VXI
			if (tlsx_info.laser_mask == 0) // only update this on init
				tlsx_info.laser_mask = 3;

			for (l = 0; l < NUM_LASERS; l++)
			{
				if ((tlsx_info.laser_mask >> l) & 0x01)
				{
					tlsx_info.laser[l].freq_min = 0;
					tlsx_info.laser[l].freq_max = 9e12;
					tlsx_info.laser[l].power_min = 5.5;
					tlsx_info.laser[l].power_max = 16.5;
					if (!tlsx_info.laser[l].show_freq_set_point)
						tlsx_info.laser[l].freq_actual = 10.0 / 11.0 * (tlsx_info.laser[l].freq_set_point/10.0 + tlsx_info.laser[l].freq_actual);
					if (!tlsx_info.laser[l].show_power_set_point)
						tlsx_info.laser[l].power_actual = 10.0 / 11.0 * (tlsx_info.laser[l].power_set_point/10.0 + tlsx_info.laser[l].power_actual);
				}
			}
			#else
			// populate the tlsx_info from the vxi_info 
			if (tlsx_info.laser_mask == 0) // only update this on init
				tlsx_info.laser_mask = vxi_info.laser_mask;

			for (l = 0; l < NUM_LASERS; l++)
			{
				if ((tlsx_info.laser_mask >> l) & 0x01)
				{
					tlsx_info.laser[l].enabled = vxi_info.laser[l].state;
					lcp_set_status(l, tlsx_info.laser[l].enabled);
					tlsx_info.laser[l].freq_actual = vxi_info.laser[l].freq_set_point * 1e-12;
					tlsx_info.laser[l].freq_min = vxi_info.laser[l].freq_min * 1e-12;
					tlsx_info.laser[l].freq_max = vxi_info.laser[l].freq_max * 1e-12;
					tlsx_info.laser[l].freq_step_size = vxi_info.laser[l].freq_step_size * 1e-9;
					tlsx_info.laser[l].freq_step_size_min = vxi_info.laser[l].freq_step_size_min * 1e-9;
					tlsx_info.laser[l].freq_step_size_max = vxi_info.laser[l].freq_step_size_max * 1e-9;
					tlsx_info.laser[l].power_actual = vxi_info.laser[l].power_actual;
					tlsx_info.laser[l].power_min = vxi_info.laser[l].power_min;
					tlsx_info.laser[l].power_max = vxi_info.laser[l].power_max;
					if (!tlsx_info.laser[l].show_freq_set_point)
						tlsx_info.laser[l].freq_set_point = vxi_info.laser[l].freq_set_point * 1e-12;
					if (!tlsx_info.laser[l].show_power_set_point)
						tlsx_info.laser[l].power_set_point = vxi_info.laser[l].power_set_point;
				}
			}
			#endif

			tlsx_info.vxi_event = true;
			pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
		}

		usleep(VXI_POLL_TIME);
	}
}


#define C (299792458) // m/s
static char * laser_freq_str(int l)
{
	static char s[16];
	double f;
	enum freq_units_t freq_units;

	///@todo this will eventually come from tlsx_service
	pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
	if (tlsx_info.laser[l].show_freq_set_point)
		f = tlsx_info.laser[l].freq_set_point;
	else
		f = tlsx_info.laser[l].freq_actual;
	freq_units = tlsx_info.freq_units; 
	pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
	
	switch (freq_units)
	{
		case freq_units_thz:
			snprintf(s, sizeof(s), "L%d:%08.4f THz", l + 1, f);
			break;
		case freq_units_nm:
			snprintf(s, sizeof(s), "L%d:%08.3f nm ", l + 1, C * 1e-3 / f);
			break;
	}

	return s;
}


static char * laser_power_str(int l)
{
	static char s[16];
	double power;
	bool enabled;
	bool show_power_set_point;
	enum power_units_t power_units;
	
	///@todo this will eventually come from tlsx_service
	pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
	show_power_set_point = tlsx_info.laser[l].show_power_set_point;
	if (tlsx_info.laser[l].show_power_set_point)
		power = tlsx_info.laser[l].power_set_point;
	else
		power = tlsx_info.laser[l].power_actual;
	enabled = tlsx_info.laser[l].enabled;
	power_units = tlsx_info.power_units;
	pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);

	if (enabled || show_power_set_point)
	{
		switch (power_units)
		{
			case power_units_dbm:
				if (power < -99.99)
					power = -99.99;
				else if (power > 99.99)
					power = 99.99;
				snprintf(s, sizeof(s), "     %+06.2f dBm", power);
				break;
			case power_units_mw:
				power = pow(10, power / 10.0); // dbm to mW
				if (power < -99.99)
					power = -99.99;
				else if (power > 99.99)
					power = 99.99;
				snprintf(s, sizeof(s), "     %+06.2f mW ", power);
				break;
		}
	}
	else
		snprintf(s, sizeof(s), "   LASER OFF   ");

	return s;
}


static char * get_freq_step_size_str(int l, double step_size)
{
	static char s[32];
	snprintf(s, sizeof(s), "L%d: %05.1f GHz", l + 1, step_size);
	return s;
}


static char * get_freq_units_str(enum freq_units_t units)
{
	static char s[32];

	switch (units)
	{
		case freq_units_thz:
			snprintf(s, sizeof(s), "THz");
			return s;
		case freq_units_nm:
			snprintf(s, sizeof(s), "nm ");
			return s;
		default:
			snprintf(s, sizeof(s), "err");
			return s;
	}
}


static char * get_power_units_str(enum power_units_t units)
{
	static char s[32];

	switch (units)
	{
		case power_units_dbm:
			snprintf(s, sizeof(s), "dBm");
			return s;
		case power_units_mw:
			snprintf(s, sizeof(s), "mW ");
			return s;
		default:
			snprintf(s, sizeof(s), "err");
			return s;
	}
}


typedef struct event_ret_t (*timeout_cb_t)(struct menu_info_t *info);


enum menu_mode_t
{
	menu_mode_normal,
	menu_mode_set_value,
};


struct menu_info_t
{
	// settings
	struct menu_item_t *mitems;
	int nitems;
	int timeout;
	timeout_cb_t timeout_cb;
	double idle_timeout;
	timeout_cb_t idle_timeout_cb;

	// private
	MENU *menu;
	ITEM **items;
};


static enum menu_read_result _menu_read(int timeout)
{
	enum menu_read_result menu_res = menu_read(mfpio, timeout);

	// process priority interrupts
	switch (menu_res)
	{
		case menu_button1_push:
			button_push(1);
			break;
		case menu_button1_release:
			button_release(1);
			break;
		case menu_button2_push:
			button_push(2);
			break;
		case menu_button2_release:
			button_release(2);
			break;
		default:
			break;
	}

	return menu_res;
}


static struct event_ret_t _make_menu(struct menu_info_t *info)
{
	int i;
	int run = 1;
	int start = -1;
	struct event_ret_t ret = {0,};
	double dt;
	struct timeval now, last_input_time;

	// we touch some control to get in here to record this as the last input time
	gettimeofday(&last_input_time, NULL);

	// load mitems in to items and make the menu
	info->items = (ITEM **)calloc(info->nitems + 1, sizeof(ITEM *));
	for (i = 0 ; i < info->nitems; i++)
	{
		info->items[i] = new_item(info->mitems[i].name, info->mitems[i].desc);
		if (!info->mitems[i].skip && start == -1)
			start = i;
	}
	info->items[info->nitems] = NULL;
	info->menu = new_menu(info->items);

	// setup the menu to have our style
	set_menu_format(info->menu, 4, 1);
	set_menu_spacing(info->menu, 1, 1, 1);
	set_menu_mark(info->menu, (start == -1)? " ": "~"); // note "~" turns out to be a single char arrow like this '->' on the lcd char set
	
	// before we start move to first non skipped item
	if (start > 0 && start < info->nitems)
		set_current_item(info->menu, info->items[start]);

	// show the new menu
	post_menu(info->menu);
	wrefresh(stdscr);

	// menu loop, handle events
	while (ret.code >= 0)
	{
		ITEM *cur_item = current_item(info->menu);
		int cur_item_index = item_index(cur_item);
		enum menu_read_result r = _menu_read(info->timeout);
		int k;
		ret.code = 0;
	
		// handle signal
		switch (r)
		{
			case menu_timeout:
				gettimeofday(&now, NULL);
				dt = now.tv_sec - last_input_time.tv_sec;
				dt += (now.tv_usec - last_input_time.tv_usec) * 1e-6;
				if (info->idle_timeout != 0 && dt > info->idle_timeout)
				{
					if (info->idle_timeout_cb != NULL)
						ret = info->idle_timeout_cb(info);
					gettimeofday(&last_input_time, NULL);
				}
				
				if (info->timeout_cb != NULL)
					info->timeout_cb(info);
				break;

			default:
				// run the event callback
				if (info->mitems[cur_item_index].cb != NULL)
				{
					ret = info->mitems[cur_item_index].cb(info->mitems[cur_item_index].cb_param, info, r);
					
					if (!(ret.flags & MENU_REPOST_HANDLED))
					{
						// repost menu so it will refresh screen when popping back to this menu
						unpost_menu(info->menu);
						post_menu(info->menu);
						wrefresh(stdscr);
					}
				}
				
				// if the event callback did not handle this sig we will
				if (ret.code == 0)
				{
					switch(r)
					{
						case menu_rot_up_slow:
						case menu_rot_up:
						case menu_rot_up_fast:
							for (k = cur_item_index - 1; k >= 0; k--)
							{
								if (info->mitems[k].skip == 0)
								{
									while (k++ != cur_item_index)
										menu_driver(info->menu, REQ_UP_ITEM);
									break;
								}
							}
							break;
						case menu_rot_down_slow:
						case menu_rot_down:
						case menu_rot_down_fast:
							for (k = cur_item_index + 1; k < info->nitems; k++)
							{
								if (info->mitems[k].skip == 0)
								{
									while (k-- != cur_item_index)
										menu_driver(info->menu, REQ_DOWN_ITEM);
									break;
								}
							}
							break;
					}
				}
				
				// update the time we last touched so idle timeout will restart
				// note we do this after the callback as we could jump into submenu's for a long time
				switch (r)
				{
					case menu_button0_push:
					case menu_button0_release:
					case menu_rot_up_slow:
					case menu_rot_up:
					case menu_rot_up_fast:
					case menu_rot_down_slow:
					case menu_rot_down:
					case menu_rot_down_fast:
						gettimeofday(&last_input_time, NULL);
						break;
				}
				break;
		}
		wrefresh(stdscr);
	}

	// clean up
	unpost_menu(info->menu);
	for(i = 0; i < info->nitems; i++)
		free_item(info->items[i]);
	free_menu(info->menu);

	ret.code++;
	return ret;
}


// if param < 0 pop down that many submenu's otherwise just back 1 submenu
struct event_ret_t done(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};

	switch (event)
	{
		case menu_button0_release:
			if (param < 0)
				ret.code = param;
			else
				ret.code = -1;
			break;
		default:
			break;
	}

	return ret;
}

static const double menu_freq_step_points[] = {0.1, 25.0, 50.0, 100.0}; // note list must remain sorted in ascending order
static double inc_freq_step_size(double freq_step_size, double min, double max)
{
	int p;
	for (p = 0; p < sizeof(menu_freq_step_points)/sizeof(menu_freq_step_points[0]); p++)
	{
		// bound checks
		if (menu_freq_step_points[p] > max)
			break;	// we cannot choose above the max, list is sorted so we just get bigger so give up
		if (menu_freq_step_points[p] < min)
			continue;	// we cannot choose below the min !

		// if this step size is bigger it is the next biggest within the min/max bounds so
		// choose it
		if (freq_step_size < menu_freq_step_points[p])
			return menu_freq_step_points[p];
	}

	// f*%k it, there is nothing bigger than this we want so just stay here
	return freq_step_size;
}


static double dec_freq_step_size(double freq_step_size, double min, double max)
{
	int p;
	for (p = sizeof(menu_freq_step_points)/sizeof(menu_freq_step_points[0]) - 1; p >= 0; p--)
	{
		// bound checks
		if (menu_freq_step_points[p] > max)
			continue;	// we cannot choose above the max !
		if (menu_freq_step_points[p] < min)
			break;	// we cannot choose below the min, list is sorted so we just get smaller so give up

		// if this step size is smaller it is the next smallest within the min/max bounds so
		// choose it
		if (freq_step_size > menu_freq_step_points[p])
			return menu_freq_step_points[p];
	}

	// f*%k it, there is nothing smaller than this we want so just stay here
	return freq_step_size;
}


struct event_ret_t set_freq_step_size(int l, struct menu_info_t *info, enum menu_read_result event)
{
	static double freq_step_size; // shadow
	struct laser_t *laser = &tlsx_info.laser[l];
	struct event_ret_t ret = {0,};

	switch (event)
	{
		case menu_button0_release:
			// toggle between set point and actual
			if (strncmp(menu_mark(info->menu), "~", 1) == 0)
			{
				// entry
				pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
				freq_step_size = tlsx_info.laser[l].freq_step_size; // load shadow frequency step size to adjust
				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);

				set_menu_mark(info->menu, "*");
			}
			else
			{
				// exit
				pthread_mutex_lock(&tlsx_info.vxi_mutex);
				vxi_set_grid(BLADE_ID, l + 1, freq_step_size * 1e9); // commit frequency step size via vxi
				pthread_mutex_unlock(&tlsx_info.vxi_mutex);

				set_menu_mark(info->menu, "~");
			}
			wrefresh(stdscr);
			ret.code = 1;
			return ret;

		default:
			// other signals are only processed when changing
			if (strncmp(menu_mark(info->menu), "*", 1) == 0)
			{
				int i;
				ITEM *cur_item = current_item(info->menu);

				switch (event)
				{
					case menu_rot_up_slow:
					case menu_rot_up:
					case menu_rot_up_fast:
						pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
						freq_step_size = inc_freq_step_size(freq_step_size, laser->freq_step_size_min, laser->freq_step_size_max);
						pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
						break;
					case menu_rot_down_slow:
					case menu_rot_down:
					case menu_rot_down_fast:
						pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
						freq_step_size = dec_freq_step_size(freq_step_size, laser->freq_step_size_min, laser->freq_step_size_max);
						pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
						break;
					default:
						ret.code = 0;
						return ret;
				}

				// update the menu with the new value
				unpost_menu(info->menu);
				snprintf((char *)cur_item->name.str, 32, "%s", get_freq_step_size_str(l, freq_step_size));
				post_menu(info->menu);
				wrefresh(stdscr);
				ret.code = 1;
				return ret;
			}
			ret.code = 0;
			return ret;
	}
}


struct event_ret_t set_freq_units(int param, struct menu_info_t *info, enum menu_read_result event)
{
	static enum freq_units_t freq_units; // shadow
	struct event_ret_t ret = {0,};

	switch (event)
	{
		case menu_button0_release:
			// toggle between set point and actual
			if (strncmp(menu_mark(info->menu), "~", 1) == 0)
			{
				// entry
				pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
				freq_units = tlsx_info.freq_units; // load shadow frequency units
				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);

				set_menu_mark(info->menu, "*");
			}
			else
			{
				// exit
				pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
				tlsx_info.freq_units = freq_units; // commit frequency units
				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);

				set_menu_mark(info->menu, "~");
			}
			wrefresh(stdscr);
			ret.code = 1;
			return ret;

		default:
			// other signals are only processed when changing
			if (strncmp(menu_mark(info->menu), "*", 1) == 0)
			{
				int i;
				ITEM *cur_item = current_item(info->menu);

				switch (event)
				{
					case menu_rot_up_slow:
					case menu_rot_up:
					case menu_rot_up_fast:
					case menu_rot_down_slow:
					case menu_rot_down:
					case menu_rot_down_fast:
						if (freq_units == freq_units_thz)
							freq_units = freq_units_nm;
						else
							freq_units = freq_units_thz;
						break;
					default:
						ret.code = 0;
						return ret;
				}

				// update the menu with the new value
				unpost_menu(info->menu);
				snprintf((char *)cur_item->name.str, 32, "%s", get_freq_units_str(freq_units));
				post_menu(info->menu);
				wrefresh(stdscr);
				ret.code = 1;
				return ret;
			}
			ret.code = 0;
			return ret;
	}
}


struct event_ret_t set_power_units(int param, struct menu_info_t *info, enum menu_read_result event)
{
	static enum power_units_t power_units; // shadow
	struct event_ret_t ret = {0,};

	switch (event)
	{
		case menu_button0_release:
			// toggle between set point and actual
			if (strncmp(menu_mark(info->menu), "~", 1) == 0)
			{
				// entry
				pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
				power_units = tlsx_info.power_units; // load shadow power units
				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);

				set_menu_mark(info->menu, "*");
			}
			else
			{
				// exit
				pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
				tlsx_info.power_units = power_units; // commit power units
				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);

				set_menu_mark(info->menu, "~");
			}
			wrefresh(stdscr);
			ret.code = 1;
			return ret;

		default:
			// other signals are only processed when changing
			if (strncmp(menu_mark(info->menu), "*", 1) == 0)
			{
				int i;
				ITEM *cur_item = current_item(info->menu);

				switch (event)
				{
					case menu_rot_up_slow:
					case menu_rot_up:
					case menu_rot_up_fast:
					case menu_rot_down_slow:
					case menu_rot_down:
					case menu_rot_down_fast:
						if (power_units == power_units_dbm)
							power_units = power_units_mw;
						else
							power_units = power_units_dbm;
						break;
					default:
						ret.code = 0;
						return ret;
				}

				// update the menu with the new value
				unpost_menu(info->menu);
				snprintf((char *)cur_item->name.str, 32, "%s", get_power_units_str(power_units));
				post_menu(info->menu);
				wrefresh(stdscr);
				ret.code = 1;
				return ret;
			}
			ret.code = 0;
			return ret;
	}
}

struct event_ret_t main_menu_timeout(struct menu_info_t *info)
{
	struct event_ret_t ret = {0,};
	ITEM **items = menu_items(info->menu);
	int l, i=0;
	int laser_mask;

	unpost_menu(info->menu);
	pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
	laser_mask = tlsx_info.laser_mask;
	pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
	for (l = 0; l < NUM_LASERS; l++)
	{
		if ((laser_mask >> l) & 0x01)
		{
			snprintf((char *)items[i++]->name.str, 32, "%s", laser_freq_str(l));
			snprintf((char *)items[i++]->name.str, 32, "%s", laser_power_str(l));
		}
	}
	post_menu(info->menu);
	wrefresh(stdscr);
	return ret;
}


struct event_ret_t idle_timeout(struct menu_info_t *info)
{
	struct event_ret_t ret = {.code=-BIG_NUMBER, 0,};
	int l;
	pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
	for (l = 0; l < NUM_LASERS; l++)
	{
		if ((tlsx_info.laser_mask >> l) & 0x01)
		{
			// bail all set point adjustments
			tlsx_info.laser[l].show_freq_set_point = false;
			tlsx_info.laser[l].show_power_set_point = false;
		}
	}
	pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
	curs_set(0); // in case we timeout with the cursor set to underline or blinking etc
	return ret;
}


struct event_ret_t freq_step_size_menu(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};

	switch (event)
	{
		case menu_button0_release:
			{
				int i = 0;
				struct menu_item_t freq_step_size_menu_items[4];
				struct menu_info_t freq_step_size_menu_info =
				{
					freq_step_size_menu_items,
					0,
					2 * ONE_SEC,
					NULL,
					DEFAULT_IDLE_TIMEOUT,	// idle timeout
					idle_timeout,
				};
				
				memset(freq_step_size_menu_items, 0, sizeof(freq_step_size_menu_items));
				snprintf(freq_step_size_menu_items[i].name, 32, "STEP SIZE");
				freq_step_size_menu_items[i++].skip= 1;
				pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
				snprintf(freq_step_size_menu_items[i].name, 32, "%s", get_freq_step_size_str(0, tlsx_info.laser[0].freq_step_size));
				freq_step_size_menu_items[i].cb_param = 0;
				freq_step_size_menu_items[i++].cb = set_freq_step_size;
				snprintf(freq_step_size_menu_items[i].name, 32, "%s", get_freq_step_size_str(1, tlsx_info.laser[1].freq_step_size));
				freq_step_size_menu_items[i].cb_param = 1;
				freq_step_size_menu_items[i++].cb = set_freq_step_size;
				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
				snprintf(freq_step_size_menu_items[i].name, 32, "DONE");
				freq_step_size_menu_items[i].cb_param = -1;
				freq_step_size_menu_items[i++].cb = done;
				freq_step_size_menu_info.nitems = i;

				wclear(stdscr);
				ret = _make_menu(&freq_step_size_menu_info);
				wclear(stdscr);

				return ret;
			}
		default:
			return ret;
	}
}


struct event_ret_t units_menu(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};

	switch (event)
	{
		case menu_button0_release:
			{
				int i = 0;
				struct menu_item_t units_menu_items[7];
				struct menu_info_t units_menu_info =
				{
					units_menu_items,
					0,
					2 * ONE_SEC,
					NULL,
					DEFAULT_IDLE_TIMEOUT,	// idle timeout
					idle_timeout,
				};
				
				memset(units_menu_items, 0, sizeof(units_menu_items));
				snprintf(units_menu_items[i].name, 32, "UNITS");
				units_menu_items[i++].skip= 1;
				pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
				snprintf(units_menu_items[i].name, 32, "%s", get_freq_units_str(tlsx_info.freq_units));
				units_menu_items[i].cb_param = 0;
				units_menu_items[i++].cb = set_freq_units;
				snprintf(units_menu_items[i].name, 32, "%s", get_power_units_str(tlsx_info.power_units));
				units_menu_items[i].cb_param = 0;
				units_menu_items[i++].cb = set_power_units;
				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
				snprintf(units_menu_items[i].name, 32, "DONE");
				units_menu_items[i].cb_param = -1;
				units_menu_items[i++].cb = done;
				units_menu_info.nitems = i;

				wclear(stdscr);
				ret = _make_menu(&units_menu_info);
				wclear(stdscr);

				return ret;
			}
		default:
			return ret;
	}
}


static bool iface_changed;
static int iface_menu_method_index;
static int iface_menu_ipaddress_index;
static int iface_menu_netmask_index;


static void on_iface_changed_event(struct iface_t *iface, struct menu_info_t *info)
{
	ITEM *method_item = info->menu->items[iface_menu_method_index];
	ITEM *ipaddress_item = info->menu->items[iface_menu_ipaddress_index];
	ITEM *netmask_item = info->menu->items[iface_menu_netmask_index];
	
	iface_changed = true;
	snprintf((char *)method_item->name.str, 32, "METHOD: %-7s", iface_method_to_str(iface, NULL, 0));
	if (iface->method == IFACE_METHOD_DHCP)
	{
		snprintf((char *)ipaddress_item->name.str, 32, "---.---.---.---");
		snprintf((char *)netmask_item->name.str, 32, "---.---.---.---");
	}
	else
	{
		snprintf((char *)ipaddress_item->name.str, 32, "%.3d.%.3d.%.3d.%.3d", iface->addr[0], iface->addr[1], iface->addr[2], iface->addr[3]);
		snprintf((char *)netmask_item->name.str, 32, "%.3d.%.3d.%.3d.%.3d", iface->netmask[0], iface->netmask[1], iface->netmask[2], iface->netmask[3]);
	}
}


struct event_ret_t iface_menu_change_method(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	struct iface_t *iface = (struct iface_t *)param;
	ITEM *cur_item = current_item(info->menu);
	
	switch (event)
	{
		case menu_button0_release:
			if (strncmp(menu_mark(info->menu), "~", 1) == 0)
				set_menu_mark(info->menu, "*");
			else if (strncmp(menu_mark(info->menu), "*", 1) == 0)
				set_menu_mark(info->menu, "~");
			break;
		
		default:
			// other signals are only processed when changing
			if (strncmp(menu_mark(info->menu), "*", 1) == 0)
			{
				switch (event)
				{
					case menu_rot_up_slow:
					case menu_rot_up:
					case menu_rot_up_fast:
					case menu_rot_down_slow:
					case menu_rot_down:
					case menu_rot_down_fast:
						// just toggle through our supported methods
						if (iface->method == IFACE_METHOD_STATIC)
							iface->method = IFACE_METHOD_DHCP;
						else
							iface->method = IFACE_METHOD_STATIC;
						on_iface_changed_event(iface, info);
						ret.code = 1;
						break;
					default:
						break;
				}
			}
			break;
	}

	snprintf((char *)cur_item->name.str, 32, "METHOD: %-7s", iface_method_to_str(iface, NULL, 0));
	return ret;
}


struct event_ret_t iface_menu_change_ipaddr(struct iface_t *iface, int ipaddr_off, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	static int o = 0;
	static int addr[4];
	ITEM *cur_item = current_item(info->menu);
	int y, cur_mode = 0;

	switch (event)
	{
		case menu_button0_release:
			if (strncmp(menu_mark(info->menu), "~", 1) == 0)
			{
				// entry
				o = 0;
				cur_mode = 1;
				set_menu_mark(info->menu, "*");
				memcpy(addr, (uint8_t *)iface + ipaddr_off, sizeof(addr));
			}
			else if (strncmp(menu_mark(info->menu), "*", 1) == 0)
			{
				struct iface_t iface_cur = *iface;
				
				switch (o)
				{
					case 0:
					case 1:
					case 2:
						// just move through all the octect's until the 4th
						o++;
						cur_mode = 1;
						break;

					default:
						syslog(LOG_ERR, "invalid ipaddr state !\n");
					case 3:
						// exit
						o = 0;
						cur_mode = 0;
						set_menu_mark(info->menu, "~");
						memcpy((uint8_t *)iface + ipaddr_off, addr, sizeof(addr));
						break;
				}
			}
			break;

		default:
			// other signals are only processed when changing
			if (strncmp(menu_mark(info->menu), "*", 1) == 0)
			{
				cur_mode = 1;
				switch (event)
				{
					case menu_rot_up_slow:
					case menu_rot_up:
						if (addr[o] < 255)
							addr[o]++;
						break;
					case menu_rot_up_fast:
						if (addr[o] <= 255 - 16)
							addr[o] += 16;
						break;
					case menu_rot_down_slow:
					case menu_rot_down:
						if (addr[o] > 0)
							addr[o]--;
						break;
					case menu_rot_down_fast:
						if (addr[o] >= 16)
							addr[o] -= 16;
						break;
					default:
						ret.flags = MENU_REPOST_HANDLED;
						return ret;
				}
				on_iface_changed_event(iface, info);
			}
			else
				return ret;
			break;
	}

	// display the new address and 
	unpost_menu(info->menu);
	snprintf((char *)cur_item->name.str, 32, "%.3d.%.3d.%.3d.%.3d", addr[0], addr[1], addr[2], addr[3]);
	post_menu(info->menu);
	y = getcury(stdscr);
	move(y, 3 + 4*o);
	curs_set(cur_mode);
	wrefresh(stdscr);
	ret.code = 1;
	ret.flags = MENU_REPOST_HANDLED;
	return ret;
}


struct event_ret_t iface_menu_change_addr(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	struct iface_t *iface = (struct iface_t *)param;

	if (iface->method == IFACE_METHOD_STATIC)
		return iface_menu_change_ipaddr(iface, offsetof(struct iface_t, addr), info, event);
	return ret;
}


struct event_ret_t iface_menu_change_netmask(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	struct iface_t *iface = (struct iface_t *)param;

	if (iface->method == IFACE_METHOD_STATIC)
		return iface_menu_change_ipaddr(iface, offsetof(struct iface_t, netmask), info, event);
	return ret;
}


struct event_ret_t iface_save(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	struct iface_t *iface = (struct iface_t *)param;
	char cmd[512];
	
	switch (event)
	{
		case menu_button0_release:
			// save changes was selected so write new network setup to disk
			// and resetup the network
			
			wclear(stdscr);
			mvwprintw(stdscr, 1, 0, "UPDATING NETWORK");
			mvwprintw(stdscr, 2, 0, "  PLEASE  WAIT  ");
			wrefresh(stdscr);
			sprintf(cmd, "%s/ifchange %s %s", mwd, "pre", iface->iface_name);
			system(cmd);
			iface_set_cfg(iface, NULL);
			sprintf(cmd, "%s/ifchange %s %s", mwd, "post", iface->iface_name);
			system(cmd);
			ret.code = -2;
			break;

		default:
			break;
	}

	return ret;
}


struct event_ret_t iface_done_menu(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};

	switch (event)
	{
		case menu_button0_release:
			ret.code = -1;

			// if something has changed in the network setup offer to save new cfg
			if (iface_changed)
			{
				int i = 0;
				int l;
				struct menu_item_t iface_done_menu_items[10];
				struct menu_info_t iface_done_menu_info =
				{
					iface_done_menu_items,
					0,
					2 * ONE_SEC,
					NULL,
					NET_IDLE_TIMEOUT,	// idle timeout
					idle_timeout,
				};
				memset(iface_done_menu_items, 0, sizeof(iface_done_menu_items));

				snprintf(iface_done_menu_items[i].name, 32, "SAVE CHANGES:  ");
				iface_done_menu_items[i].skip = 1;
				iface_done_menu_items[i++].cb = 0;
				snprintf(iface_done_menu_items[i].name, 32, "YES");
				iface_done_menu_items[i].cb_param = param;
				iface_done_menu_items[i++].cb = iface_save;
				snprintf(iface_done_menu_items[i].name, 32, "NO");
				iface_done_menu_items[i].cb_param = -2;
				iface_done_menu_items[i++].cb = done;
				iface_done_menu_info.nitems = i;
				
				wclear(stdscr);
				ret = _make_menu(&iface_done_menu_info);
				wclear(stdscr);
			}
			break;

		default:
			break;
	}

	return ret;
}


struct event_ret_t iface_menu(struct iface_t *iface_cur, bool ro, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	struct iface_t iface_saved;

	iface_changed = false;

	switch (event)
	{
		case menu_button0_release:
			{
				int i = 0;
				int l;
				struct menu_item_t network_menu_items[10];
				struct menu_info_t iface_menu_info =
				{
					network_menu_items,
					0,
					2 * ONE_SEC,
					NULL,
					NET_IDLE_TIMEOUT,	// idle timeout
					idle_timeout,
				};
			
				snprintf(iface_saved.iface_name, IFACE_STR_LEN, "%s", iface_cur->iface_name);
				iface_get_cfg(&iface_saved, NULL);
				iface_get_active_cfg(iface_cur);
				iface_cur->method = iface_saved.method; // we dont know how iface_cur got setup so assume it is from what was saved

				memset(network_menu_items, 0, sizeof(network_menu_items));

				snprintf(network_menu_items[i].name, 32, "ADDRESS:");
				network_menu_items[i++].cb = 0;
				snprintf(network_menu_items[i].name, 32, "%-15s", iface_addr_to_str(iface_cur, NULL, 0));
				iface_menu_ipaddress_index = i;
				network_menu_items[i].cb = ro? NULL: iface_menu_change_addr;
				network_menu_items[i++].cb_param = (int)&iface_saved;

				snprintf(network_menu_items[i].name, 32, "NETMASK:");
				network_menu_items[i++].cb = 0;
				snprintf(network_menu_items[i].name, 32, "%-15s", iface_netmask_to_str(iface_cur, NULL, 0));
				iface_menu_netmask_index = i;
				network_menu_items[i].cb = ro? NULL: iface_menu_change_netmask;
				network_menu_items[i++].cb_param = (int)&iface_saved;

				snprintf(network_menu_items[i].name, 32, "METHOD: %-7s", iface_method_to_str(iface_cur, NULL, 0));
				iface_menu_method_index = i;
				network_menu_items[i].cb = ro? NULL: iface_menu_change_method;
				network_menu_items[i++].cb_param = (int)&iface_saved;

				snprintf(network_menu_items[i].name, 32, "DONE");
				network_menu_items[i].cb_param = (int)&iface_saved;
				network_menu_items[i++].cb = iface_done_menu;

				iface_menu_info.nitems = i;

				wclear(stdscr);
				ret = _make_menu(&iface_menu_info);
				wclear(stdscr);

				return ret;
			}
		default:
			return ret;
	}
}


struct event_ret_t iface_rw_menu(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct iface_t *iface = (struct iface_t *)param;
	return iface_menu(iface, false, info, event);
}


struct event_ret_t iface_ro_menu(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct iface_t *iface = (struct iface_t *)param;
	return iface_menu(iface, true, info, event);
}


struct event_ret_t network_menu(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	struct iface_t eth0;
	struct iface_t usb0;

	switch (event)
	{
		case menu_button0_release:
			{
				int i = 0;
				int l;
				struct menu_item_t network_menu_items[3];
				struct menu_info_t network_menu_info =
				{
					network_menu_items,
					0,
					2 * ONE_SEC,
					NULL,
					NET_IDLE_TIMEOUT,	// idle timeout
					idle_timeout,
				};

				snprintf(eth0.iface_name, IFACE_STR_LEN, "eth0");
				snprintf(usb0.iface_name, IFACE_STR_LEN, "usb0");
				
				memset(network_menu_items, 0, sizeof(network_menu_items));
				snprintf(network_menu_items[i].name, 32, "ETHERNET");
				network_menu_items[i].cb = iface_rw_menu;
				network_menu_items[i++].cb_param = (int)&eth0;
				snprintf(network_menu_items[i].name, 32, "USB");
				network_menu_items[i].cb = iface_ro_menu;
				network_menu_items[i++].cb_param = (int)&usb0;
				snprintf(network_menu_items[i].name, 32, "DONE");
				network_menu_items[i].cb_param = -1;
				network_menu_items[i++].cb = done;
				network_menu_info.nitems = i;

				wclear(stdscr);
				ret = _make_menu(&network_menu_info);
				wclear(stdscr);

				return ret;
			}
		default:
			return ret;
	}
}


struct event_ret_t settings_menu(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};

	switch (event)
	{
		case menu_button0_release:
			{
				int i = 0;
				struct menu_item_t settings_menu_items[4];
				struct menu_info_t settings_menu_info =
				{
					settings_menu_items,
					0,
					2 * ONE_SEC,
					NULL,
					DEFAULT_IDLE_TIMEOUT,	// idle timeout
					idle_timeout,
				};
				
				memset(settings_menu_items, 0, sizeof(settings_menu_items));

				snprintf(settings_menu_items[i].name, 32, "UNITS");
				settings_menu_items[i].cb_param = 0;
				settings_menu_items[i++].cb = units_menu;
				snprintf(settings_menu_items[i].name, 32, "STEP SIZE");
				settings_menu_items[i].cb_param = 0;
				settings_menu_items[i++].cb = freq_step_size_menu;
				snprintf(settings_menu_items[i].name, 32, "NETWORKING");
				settings_menu_items[i].cb_param = 0;
				settings_menu_items[i++].cb = network_menu;
				snprintf(settings_menu_items[i].name, 32, "DONE");
				settings_menu_items[i].cb_param = -1;
				settings_menu_items[i++].cb = done;
				settings_menu_info.nitems = i;

				wclear(stdscr);
				ret = _make_menu(&settings_menu_info);
				wclear(stdscr);
				main_menu_timeout(info);

				return ret;
			}
		default:
			return ret;
	}
}


#define SIGN(x) ( (x < 0)? -1: (x > 0)? 1: 0 )
struct event_ret_t laser_set_freq(int l, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	struct laser_t *laser;
	double freq_set_point;
	bool set_freq_setpoint = false;
	pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
	laser = &tlsx_info.laser[l];

	switch (event)
	{
		case menu_button0_release:
			// toggle between set point and actual
			///@todo when transitioning back to show actual mode, send the new set point to tlsx_service
			laser->show_freq_set_point = !laser->show_freq_set_point;
			if (laser->show_freq_set_point)
			{
				set_menu_mark(info->menu, "*");
			}
			else
			{
				freq_set_point = laser->freq_set_point;
				set_freq_setpoint = true;
				set_menu_mark(info->menu, "~");
			}
			pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
			wrefresh(stdscr);
			
			if (set_freq_setpoint)
			{
				pthread_mutex_lock(&tlsx_info.vxi_mutex);
				vxi_set_frequency(BLADE_ID, l + 1, freq_set_point * 1e12);
				pthread_mutex_unlock(&tlsx_info.vxi_mutex);
			}

			main_menu_timeout(info);
			ret.code = 1;
			return ret;

		default:
			// other signals are only processed when changing setpoint
			if (laser->show_freq_set_point)
			{
				double df = 0;
				int speed = 0;

				switch (event)
				{
					case menu_rot_up_slow:
						speed = 1;
						break;
					case menu_rot_up:
						speed = 10;
						break;
					case menu_rot_up_fast:
						speed = 1000;
						break;
					case menu_rot_down_slow:
						speed = -1;
						break;
					case menu_rot_down:
						speed = -10;
						break;
					case menu_rot_down_fast:
						speed = -1000;
						break;
					default:
						break;
				}
				
				// we only step by speed for 0.1 step size, the others are too large to do usefully
				if (laser->freq_step_size == 0.1)
					df = speed * laser->freq_step_size * 1e-3;
				else
					df = SIGN(speed) * laser->freq_step_size * 1e-3;
				
				// for wavelength we need to go the other way, ie dlamda = C / df
				if (tlsx_info.freq_units == freq_units_nm)
					df = -df; 
				laser->freq_set_point += df;

				// snap to min/max boundaries
				if (laser->freq_set_point < laser->freq_min)
					laser->freq_set_point = laser->freq_min;
				else if (laser->freq_set_point > laser->freq_max)
					laser->freq_set_point = laser->freq_max;

				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
				main_menu_timeout(info);
				ret.code = 1;
				return ret;
			}
			pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
			ret.code = 0;
			return ret;
	}

	// do this just to be safe (we should never ever ever get here)
	pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
	return ret;
}


struct event_ret_t laser_set_power(int l, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	struct laser_t *laser;
	double power_set_point;
	bool set_power_setpoint = false;
	pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
	laser = &tlsx_info.laser[l];

	switch (event)
	{
		case menu_button0_release:
			// toggle between set point and actual
			///@todo when transitioning back to show actual mode, send the new set point to tlsx_service
			laser->show_power_set_point = !laser->show_power_set_point;
			if (laser->show_power_set_point)
			{
				set_menu_mark(info->menu, "*");
			}
			else
			{
				power_set_point = laser->power_set_point;
				set_power_setpoint = true;
				set_menu_mark(info->menu, "~");
			}
			pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
			wrefresh(stdscr);
			
			if (set_power_setpoint)
			{
				pthread_mutex_lock(&tlsx_info.vxi_mutex);
				vxi_set_power(BLADE_ID, l + 1, power_set_point);
				pthread_mutex_unlock(&tlsx_info.vxi_mutex);
			}

			main_menu_timeout(info);
			ret.code = 1;
			return ret;

		default:
			// other signals are only processed when changing setpoint
			if (tlsx_info.laser[l].show_power_set_point)
			{
				double dp = 0;
				switch (event)
				{
					case menu_rot_up_slow:
						dp = 1 * laser->power_step_size;
						break;
					case menu_rot_up:
						dp = 10 * laser->power_step_size;
						break;
					case menu_rot_up_fast:
						dp = 50 * laser->power_step_size;
						break;
					case menu_rot_down_slow:
						dp = -1 * laser->power_step_size;
						break;
					case menu_rot_down:
						dp = -10 * laser->power_step_size;
						break;
					case menu_rot_down_fast:
						dp = -50 * laser->power_step_size;
						break;
					default:
						break;
				}
				laser->power_set_point += dp;

				// snap to min/max boundaries
				if (laser->power_set_point < laser->power_min)
					laser->power_set_point = laser->power_min;
				else if (laser->power_set_point > laser->power_max)
					laser->power_set_point = laser->power_max;

				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
				main_menu_timeout(info);
				ret.code = 1;
				return ret;
			}
			pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
			ret.code = 0;
			return ret;
	}

	// do this just to be safe (we should never ever ever get here)
	pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
}


struct event_ret_t main_menu(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	int laser_mask;

	switch (event)
	{
		case menu_rot_up_slow:
		case menu_rot_up:
		case menu_rot_up_fast:
		case menu_rot_down_slow:
		case menu_rot_down:
		case menu_rot_down_fast:
		case menu_button0_release:
			{
				int i = 0;
				int l;
				struct menu_item_t main_menu_items[6];
				struct menu_info_t main_menu_info =
				{
					main_menu_items,
					0,
					2 * ONE_SEC,
					main_menu_timeout,
					DEFAULT_IDLE_TIMEOUT,	// idle timeout
					idle_timeout,
				};

				memset(main_menu_items, 0, sizeof(main_menu_items));
				pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
				laser_mask = tlsx_info.laser_mask;
				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
				for (l = 0; l < NUM_LASERS; l++)
				{
					if ((laser_mask >> l) & 0x01)
					{
						snprintf(main_menu_items[i].name, 32, "%s", laser_freq_str(l));
						main_menu_items[i].cb_param = l;
						main_menu_items[i++].cb = laser_set_freq;

						snprintf(main_menu_items[i].name, 32, "%s", laser_power_str(l));
						main_menu_items[i].cb_param = l;
						main_menu_items[i++].cb = laser_set_power;
					}
				}
				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
				snprintf(main_menu_items[i].name, 32, "SETTINGS");
				main_menu_items[i].cb_param = 0;
				main_menu_items[i++].cb = settings_menu;

				main_menu_info.nitems = i;
				wclear(stdscr);
				_make_menu(&main_menu_info);
				wclear(stdscr);
			}
			return ret;

		default:
			return ret;
	}
}


int screen_saver(void)
{
	int laser_mask;
	wclear(stdscr);
	while (1)
	{
		int l;
		int y = 0;

		pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
		laser_mask = tlsx_info.laser_mask;
		pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
		for (l = 0; l < NUM_LASERS; l++)
		{
			if ((laser_mask >> l) & 0x01)
			{
				wmove(stdscr, y++, 0);
				printw(" %s", laser_freq_str(l));
				wmove(stdscr, y++, 0);
				printw(" %s", laser_power_str(l));
			}
		}
		wrefresh(stdscr);
		
		enum menu_read_result r = _menu_read(1*ONE_SEC);
		main_menu(0, NULL, r);
	}
}


int pid_fd = 0;
char pid_filename[64];
void sig_handler(int sig)
{
	switch (sig)
	{
		case SIGINT:
		case SIGTERM:
			///@todo if anyone feels like being tidy this could shutdown all the threads / sockets
			/// etc nicely
			if (pid_fd != 0)
				close(pid_fd);
			remove(pid_filename);
			exit(EXIT_SUCCESS);
		default:
			// unhandled sig
			break;
	}
}


int daemonize(char *name)
{
	char pid_str[64];

	// reg signal handlers
	signal(SIGINT, sig_handler);
	signal(SIGTERM, sig_handler);
	
	// use unistd daemon maker for all the boring work
	if (daemon(0, 0) == -1)
	{
		syslog(LOG_INFO, "failboat cannot make %s a daemon\n", name);
		return -1;
	}

	// make the pid file
	snprintf(pid_filename, sizeof(pid_filename), "/tmp/%s.pid", name);
	pid_fd = open(pid_filename, O_RDWR|O_CREAT, 0600);
	if (pid_fd == -1)
	{
		syslog(LOG_INFO, "failboat cannot open pid file %s\n", pid_filename);
		return -1;
	}
	if (lockf(pid_fd, F_TLOCK, 0) == -1)
	{
		syslog(LOG_INFO, "failboat cannot lock pid file %s\n", pid_filename);
		return -1;
	}
	sprintf(pid_str, "%d\n", getpid());
	write(pid_fd, pid_str, strlen(pid_str));

	return 0;	
}


void usage(void)
{
	printf("menu [OPTION]\n");
	printf("\t-f\t\tby default menu will daemonise, if -f is present stay in the foreground\n");
	printf("\t-h\t\tshow this message\n");
}


#define MY_TERM
#ifdef MY_TERM
#define TERM "vtlcd"
#define OUT (fout)
#define IN (NULL)
#else
#define TERM (NULL)
#define OUT (stdout)
#define IN (stdin)
#endif

#define fail(r,f) {ret = r; goto fail##f;}
int main(int argc, char **argv)
{
	// init structs
	int l;
	int ret;
	FILE *fin, *fout;
	struct sched_param sp;
	int opt;
	bool become_daemon = true;

	// process command line options
	while ((opt = getopt(argc, argv, "hf")) != -1)
	{
		switch (opt)
		{
			case 'f':
				become_daemon = false;
				break;
			case 'h':
			default:
				usage();
				return 0;
		}
	}
	

	// daemonize
	getcwd(mwd, sizeof(mwd));
	syslog(LOG_INFO, "mwd is %s\n", mwd);
	if (become_daemon)
	{
		if (daemonize("menu") == -1)
			exit(EXIT_FAILURE);
	}

	// set this program up to run really fast so the menu seems smoother :)
	sp.sched_priority = sched_get_priority_max(SCHED_FIFO);
	sched_setscheduler(0, SCHED_FIFO, &sp);

	// this module is shared between menu_fd and this file so call it here
	lcp_init();

	memset(&tlsx_info, 0, sizeof(tlsx_info));
	// start vxi thread to poll tlx info and wait for it to start
	if (pthread_mutex_init(&tlsx_info.tlsx_info_mutex, NULL))
		fail(-1, 1);
	if (pthread_mutex_init(&tlsx_info.vxi_mutex, NULL))
		fail(-1, 2);
	if (pthread_create(&tlsx_info.vxi_thread, NULL, vxi_poll, NULL))
		fail(-1, 3);
	while (1)
	{
		pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
		if (tlsx_info.vxi_event)
			break;
		pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
		usleep(100000);
	}
	tlsx_info.freq_units = freq_units_thz;
	tlsx_info.power_units = power_units_dbm;
	for (l = 0; l < NUM_LASERS; l++)
	{
		if ((tlsx_info.laser_mask >> l) & 0x01)
		{
			#ifdef FAKE_VXI
			tlsx_info.laser[l].freq_set_point = 192.5;
			tlsx_info.laser[l].power_set_point = 6.0;
			tlsx_info.laser[l].freq_step_size = 0.1;
			#endif
			tlsx_info.laser[l].power_step_size = 0.01;
		}
	}
	pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);

	// setup io
	if ((mfpio = menu_open(&fin, &fout)) == NULL)
		fail(-2, 2);

	// setup ncurses
	SCREEN *s = newterm(TERM, OUT, IN);
	if (s == NULL)
		fail(-3, 2);
	set_term(s);
	cbreak();
	noecho();
	curs_set(0);
	keypad(stdscr, TRUE);

	// start our menu interface via the screen saver
	screen_saver();
	wclear(stdscr);
	wrefresh(stdscr);

	endwin();
	delscreen(s);
	return 0;

fail3:
	pthread_mutex_destroy(&tlsx_info.tlsx_info_mutex);
fail2:
	pthread_mutex_destroy(&tlsx_info.vxi_mutex);
fail1:
	return ret;
}

