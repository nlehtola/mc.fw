ifndef INSTALL_PATH
INSTALL_PATH = ./bin
endif

ifdef TOOL_PREFIX
CC = $(TOOL_PREFIX)gcc
AR = $(TOOL_PREFIX)ar
LD = $(TOOL_PREFIX)ld
ARCH ?= noncavium
CFLAGS += -march=armv4 -ffunction-sections -fdata-sections -D$(ARCH)=1
endif

CFLAGS += -g
LDFLAGS += -Wl,-gc-sections
LDFLAGS += -Wl,-rpath,.

COMPONENTS_DIR = ../../components
LIBCOMMON_DIR = ../../common
LIBTSCTL_DIR = $(COMPONENTS_DIR)/libtsctl
LIBENCODER_DIR = $(COMPONENTS_DIR)/encoder
LIBLCP_DIR = $(COMPONENTS_DIR)/lcp
LIBVXILOCAL_DIR = $(COMPONENTS_DIR)/vxi_local
LINUX_DIR=$(COMPONENTS_DIR)/linux
LCDDRIVER_DIR=$(COMPONENTS_DIR)/lcd_driver
LIBIFACE_DIR= $(COMPONENTS_DIR)/iface

LIBCOMMON = $(LIBCOMMON_DIR)/libcommon.so
LIBTSCTL = $(LIBTSCTL_DIR)/$(ARCH)/libtsctl.so
LIBENCODER = $(LIBENCODER_DIR)/libencoder.so
LIBLCP = $(LIBLCP_DIR)/liblcp.so
LIBVXILOCAL = $(LIBVXILOCAL_DIR)/libvxilocal.so
LCDDRIVER = $(LCDDRIVER_DIR)/fls_lcd.ko
LIBIFACE = $(LIBIFACE_DIR)/libiface.so

ifdef TOOL_PREFIX
LIB += $(LIBCOMMON)
LIB += $(LIBTSCTL)
LIB += $(LIBENCODER)
LIB += $(LIBLCP)
LIB += $(LIBVXILOCAL)
LIB += $(LIBIFACE)
endif

LDFLAGS += -L$(COMPONENTS_DIR) -lncurses -lmenu
LDFLAGS += -L$(COMPONENTS_DIR) -lpthread
LDFLAGS += -lm
ifdef TOOL_PREFIX
LDFLAGS += -L$(LIBCOMMON_DIR) -lcommon
LDFLAGS += -L$(LIBTSCTL_DIR)/$(ARCH) -ltsctl
LDFLAGS += -L$(LIBENCODER_DIR) -lencoder
LDFLAGS += -L$(LIBLCP_DIR) -llcp
LDFLAGS += -L$(LIBVXILOCAL_DIR) -lvxilocal
LDFLAGS += -L$(LIBIFACE_DIR) -liface
endif

INC += ./
INC += $(COMPONENTS_DIR)/include
ifdef TOOL_PREFIX
INC += $(LIBCOMMON_DIR)/include
INC += $(LIBTSCTL_DIR)/ts
INC += $(LIBENCODER_DIR)/
INC += $(LIBLCP_DIR)/
INC += $(LIBVXILOCAL_DIR)/include
INC += $(LIBIFACE_DIR)
endif
INCDIR = $(patsubst %,-I%,$(INC))

SRC_DIR = ./
SRC += $(SRC_DIR)/menu.c
SRC += $(SRC_DIR)/menu_fd.c
OBJ_DIR = ./obj
OBJ = $(patsubst %.c, $(OBJ_DIR)/%.o,$(notdir $(SRC)))

MENU = menu

.PHONY: clean all $(LIB) $(OBJ_DIR) $(LCDDRIVER)

all: $(MENU)
	@echo done

$(MENU): $(LIB) $(LCDDRIVER) $(OBJ_DIR) $(OBJ) 
	tic vtlcd
	$(CC) $(CFLAGS) -I. $(INCDIR) $(OBJ) $(LDFLAGS) -o $@

$(LIBCOMMON):
	make -C $(LIBCOMMON_DIR) $(notdir $(LIBCOMMON)) CC="$(CC)" CFLAGS="$(CFLAGS)" LD="$(LD)"

$(LIBTSCTL):
	make -C $(LIBTSCTL_DIR) $(notdir $(LIBTSCTL)) TOOL_PREFIX="$(TOOL_PREFIX)"

$(LCDDRIVER):
	make -C $(LINUX_DIR) CROSS_COMPILE=$(TOOL_PREFIX) ARCH=arm KBUILD_NOPEDANTIC=1
	@/bin/bash -c "pushd $(LCDDRIVER_DIR); make CROSS_COMPILE=$(TOOL_PREFIX) ARCH=arm KPATH=$(LINUX_DIR) KBUILD_NOPEDANTIC=1; popd"

$(LIBENCODER):
	make -C $(LIBENCODER_DIR) $(notdir $(LIBENCODER)) TOOL_PREFIX="$(TOOL_PREFIX)"

$(LIBLCP):
	make -C $(LIBLCP_DIR) $(notdir $(LIBLCP)) TOOL_PREFIX="$(TOOL_PREFIX)"

$(LIBVXILOCAL):
	make -C $(LIBVXILOCAL_DIR) $(notdir $(LIBVXILOCAL)) TOOL_PREFIX="$(TOOL_PREFIX)"

$(LIBIFACE):
	make -C $(LIBIFACE_DIR) $(notdir $(LIBIFACE)) CC="$(TOOL_PREFIX)gcc"

$(OBJ_DIR):
	mkdir -p $(OBJ_DIR)

$(OBJ_DIR)/%.o : $(SRC_DIR)/%.c
	$(CC) -c $(CFLAGS) -I. $(INCDIR) $< -o $@

clean:
	-rm -rf $(OBJ_DIR)
	-rm -f $(MENU)
	make -C $(LIBCOMMON_DIR) clean
	make -C $(LIBTSCTL_DIR) clean
	make -C $(LIBENCODER_DIR) clean
	make -C $(LIBLCP_DIR) clean
	make -C $(LIBVXILOCAL_DIR) clean
	make -C $(LIBIFACE_DIR) clean
	@/bin/bash -c "pushd $(LCDDRIVER_DIR); make KPATH=$(LINUX_DIR) clean; popd"
	# purposely don't clean up the Linux kernel as this takes ages to rebuild

install:
	@echo -e "\t\texport INSTALL_PATH=/mnt/tlsx_dev/root/dev\n"
	@mkdir -p $(INSTALL_PATH)
	cp $(LIB) $(INSTALL_PATH)
	cp $(MENU) $(INSTALL_PATH)
	cp $(LCDDRIVER_DIR)/fls_lcd.ko $(INSTALL_PATH)
