
#include "deviceConnection.hpp"
#include <iostream>
#include <sstream>
using namespace std;

//global state of exisiting connection objects
map<long, Connection*> connections;

// ***************************** utility functions ****************************
int connectToRemoteSocket(const char* host, int port)
{
	int sockfd;
	struct sockaddr_in serv_addr;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
		spLogError("Error, unable to create new socket: %s", strerror(errno));
		return -2;
	}
	bzero((char*) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = inet_addr(host);
	serv_addr.sin_port = htons(port);
	
	if (connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
	 	spLogError("Error, Could not connect to %s on port %d: %s", host, port, strerror(errno));
		return -1;
	}
	if (sockfd<0) {
		spLogError("Error, new socket is invalid");
	}
	return sockfd;
}

/*
int connectToRemoteSocket(const char* host, const char* port)
{
	int sockfd;  
	struct addrinfo hints, *servinfo, *p;
	int e;

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	if ((e = getaddrinfo(host, port, &hints, &servinfo)) != 0) {
		spLogError("Error, Could not resolve hostname: \"%s\", %s", host, gai_strerror(e));
		return -1;
	}

	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype,
							p->ai_protocol)) == -1) {
			spLogError("Error, unable to create new socket: %s", strerror(errno));
			continue;
		}

		if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			spLogError("Error, Could not connect to %s on port %s: %s", host, port, strerror(errno));
			continue;
		}
		break;
	}
	freeaddrinfo(servinfo);
	if (p==NULL) {
		spLogError("Error, Could not connect to %s on port %s: %s", host, port, "Reason Unknown");
		return -2;
	}
	return sockfd;
}*/

// This is a function that is called like read, but reports errors
// TODO, timeouts?
int wrappedWrite(int fd, void* buf, size_t count, long &linkid) {
	if (count>TCP_MAX_BUFF_SIZE) {
		spLogError("Error, linkid: %ld, data being written exceeds maximum allowed buffer size", linkid);
		return CONNECTION_PACKET_ERROR;
	}
	int e = write(fd, buf, count);	
	if (e < 0) {
		spLogError("Error, linkid: %ld, could not write to socket: %s", linkid, strerror(errno));
		return CONNECTION_SOCKET_ERROR;
	} else if (e==0) {
		spLogError("Error, linkid: %ld, write packet attempted, but SCPI server disconnected", linkid);		// TODO, does this apply to write? I wouldn't expect to see this error message
		return CONNECTION_SOCKET_ERROR;
	} else if (e<(int)count) {
		spLogError("Error, linkid: %ld, only %d out of %d bytes sent", linkid, e, count);
		return CONNECTION_SOCKET_ERROR;
	}
	return CONNECTION_SUCCESS;
}
	
// TODO, timeouts?
// This is a function that is called like read, but reports errors
// returns negative on error. The number of characters read on success
int wrappedRead(int &fd, void* buf, size_t count, long &linkid) {
	char* bufPtr = (char*)buf;
	unsigned int left = count;
	if (count>TCP_MAX_BUFF_SIZE) {
		spLogError("Error, linkid: %ld, data being read exceeds maximum allowed buffer size", linkid);
		return CONNECTION_PACKET_ERROR;
	}
	int done = 0;
	while(!done) {
		int e = read(fd, bufPtr, left);
		if (e<0) {
			spLogError("Error, linkid: %ld, could not read from socket: %s", linkid, strerror(errno));
			return CONNECTION_SOCKET_ERROR;
		} else if (e==0) {		//TODO is this a valid assumption?
			spLogError("Error, linkid: %ld, SCPI server disconnected", linkid);
			return CONNECTION_SOCKET_ERROR;
		} else if (e>TCP_MAX_BUFF_SIZE) {
			// read should stop this from happening
			// seeing this error is a bad sign
			spLogError("Error, linkid: %ld, read returned more data than asked for", linkid);
			return CONNECTION_PACKET_ERROR;
		}
		bufPtr+=e;
		left-=e;
		if (left==0) {
			break;
		}
	}
	return count-left;	//should always just be count, should never be negative
}

// ***************************** packet class *****************************
// A packet is a wrapper for reading and writing from a stream, and keeping the memory allocated to a packet from leaking
class Packet
{
private:
	// sanity checks data and len to ensure they are valid
	// if either is empty, both become empty
	void check() {
		if (data==NULL) {
			len = 0;
		} else if (len==0) {
			delete[](data);
			data = NULL;
		}
	}

	char* data;				// NULL data indicates empty
	unsigned int len;
public:
	SCPIPurpose purpose;	// explained in the purpose section of the comments at the top of the tcpServer file in SCPI parser
//public:
	// ownership of data is relinquished to Packet, call detach() to regain ownership
	Packet(SCPIPurpose purpose, char* data, unsigned int len) {
		this->purpose = purpose;
		this->data = data;
		this->len = len;
		check();
	}
	Packet(SCPIPurpose purpose) {
		this->purpose = purpose;
		data = NULL;
		len = 0;
	}
	Packet() {
		this->purpose = SDCPP_NONE;
		data = NULL;
		len = 0;
	}
	
	~Packet() {
		if (data!=NULL) {
			delete[](data);
		}
	}
	
	// sets length to 0 and frees memory associated with data stored
	// packet becomes a SDCPP_NONE packet
	void clear() {
		purpose = SDCPP_NONE;
		if (data!=NULL) {
			delete[](data);
			data=NULL;
		}
		len = 0;
	}
	
	// calling detach prevents Packet from freeing the data when it is freed
	// This causes the internal data pointer to be NULL, and the packet will be empty
	// the caller assumes responsibility for freeing the memory pointed to by* data
	char* detach(unsigned int &len) {
		char* data;
		check();
		
		data = this->data;
		len = this->len;
		
		this->data = NULL;
		this->len = 0;
		return data;
	}
	
	// similar in purpose to detach, but does not transfer ownership of data
	// You MAY edit this data, but do not change its size
	// pointer will become invalid after a call to assign, clear, detach or readFromSocket
	char* borrow(unsigned int &len) {
		check();
		
		len = this->len;
		return data;
	}
	char* borrow() {
		check();
		return data;
	}
	
	// this assigns ownership of the memory pointed to by data to the Packet
	// this will be freed when it is no longer needed. The caller should no longer
	// use this memory
	void assign(char* data, unsigned int len) {
		clear();
		this->data = data;
		this->len = len;
		check();
	}
	
	unsigned int length() {
		check();
		return len;
	}
	
	// sends this packet to the socket in the passed connection object
	// TODO, wrapped write
	int sendToSocket(long linkid, int sockfd) {
		int n;
		check();

		if (purpose!=SDCPP_NONE) {
			if ((n=wrappedWrite(sockfd, (void*)&purpose, sizeof(SCPIPurpose), linkid))!=CONNECTION_SUCCESS) {
				return n;
			}
			if ((n=wrappedWrite(sockfd, (void*)&len, sizeof(unsigned int), linkid))!=CONNECTION_SUCCESS) {
				return n;
			}
			if (len==0) {
				return CONNECTION_SUCCESS;
			}
			if ((n=wrappedWrite(sockfd, (void*)data, len, linkid))!=CONNECTION_SUCCESS) {
				return n;
			}
		} else {
			// sending an SDCPP_NONE is a programming error in higher levels
			spLogWarning("Warning, linkid: %ld, Attempted to send empty packet object to server", linkid);
			return CONNECTION_PACKET_ERROR;	//because a programming error should be fatal
		}
		return CONNECTION_SUCCESS;
	}
	
	// returns an error code on failure
	// CONNECTION_SUCCESS on success
	int readFromSocket(long linkid, int sockfd) {
		clear();

		int n;
		// read in the purpose and length fields
		if ((n=wrappedRead(sockfd, (void*)&purpose, sizeof(SCPIPurpose), linkid))<=CONNECTION_SUCCESS) {
			return n;
		}
		if ((n=wrappedRead(sockfd, (void*)&len, sizeof(int), linkid))<=CONNECTION_SUCCESS) {
			return n;
		}
		if (len>0) {
			// safety check length
			if (len>TCP_MAX_BUFF_SIZE) {
				spLogError("Error, linkid: %ld, nominal data packet length exceeds maximum allowed buffer size", linkid);
				return CONNECTION_PACKET_ERROR;
			}
			//  allocate a new buffer
			data = new char[len];

			// Read in the data contents the data field into data
			if ((n=wrappedRead(sockfd, data, len, linkid))<=CONNECTION_SUCCESS) {
				clear();
				return n;
			}
		}		
		return CONNECTION_SUCCESS;
	}
};

// ***************************** connection implimentation *****************************
Connection::Connection()
{
	spLogDebug(2, "Connection()");
	linkid = -1;	// -1 is an invalid linkid
	sockfd = -1;
}

Connection::~Connection()
{
	spLogDebug(2, "~Connection(): linkid: %ld, sockfd: %d", linkid, sockfd);
	disconnect(CONNECTION_SUCCESS);
}

// opens the tcp stream for the connection and begins communication with the server
int Connection::start(string host, int port)
{
	if (linkid!=-1) {
		disconnect(CONNECTION_SOCKET_ERROR);
	}

	sockfd = connectToRemoteSocket(host.c_str(), port);
	
	if (sockfd < 0) {
		return CONNECTION_SOCKET_ERROR;		// unable to connect
	}
	Packet response;
	int e;
	if ((e=response.readFromSocket(-1, sockfd))!=CONNECTION_SUCCESS) {
		// readFromSocket will log an error message
		close(sockfd);
		return e;
	}
	// This should be the implicit initial packet sent to us when we connect
	if (response.purpose!=SDCPP_CONNECT) {
		spLogError("Error, Initial packet sent from server was of invalid purpose");
		close(sockfd);
		return CONNECTION_PACKET_ERROR;
	}
	if (response.length() != sizeof(long)) {
		spLogError("Error, Initial packet sent from server was of invalid length");
		close(sockfd);
		return CONNECTION_PACKET_ERROR;
	}
	linkid = *((long*)response.borrow());
	return CONNECTION_SUCCESS;
}

// reads a block of data from the server
// returns CONNECTION_SUCCESS on success
// you must free data with delete[]
// will read at most, maxLength bytes
// TODO, return CONNECTION_ENDCHAR when SDCPP_READ_END
// and return CONNECTION_IO_TIMEOUT when  SDCPP_IO_TIMEOUT
int Connection::readBlock(char*& data, unsigned int &length, unsigned int maxLength, char termchar)
{
	data = NULL;
	length = 0;
	if (linkid==-1) {
		spLogError("Error, connection is not open, could not read block");
		return CONNECTION_SOCKET_ERROR;
	}
	int e;
	
	/*Packet packet(SDCPP_READY);

	// send a SDCPP_READY packet
	if ((e=packet.sendToSocket(linkid, sockfd))!=CONNECTION_SUCCESS) {
		disconnect(e);
		return e;
	}
	// receive a SDCPP_READY_RESPONSE packet
	if ((e=packet.readFromSocket(linkid, sockfd))!=CONNECTION_SUCCESS) {
		disconnect(e);
		return e;
	}
	if (packet.purpose!=SDCPP_READY_RESPONSE) {
		spLogError("Error, linkid: %ld, unexpected packet from server of type %d, expected SDCPP_READY_RESPONSE", linkid, packet.purpose);
		disconnect(CONNECTION_PACKET_ERROR);
		return CONNECTION_PACKET_ERROR;
	}
	if (packet.length()!=sizeof(unsigned int)) {
		spLogError("Error, linkid: %ld, malformed SDCPP_READY_RESPONSE packet from server", linkid);
		disconnect(CONNECTION_PACKET_ERROR);
		return CONNECTION_PACKET_ERROR;
	}
	// sanity check the length in the packet
	unsigned int expected = *((long*)packet.borrow());
	if (expected > TCP_MAX_BUFF_SIZE) {
		expected = TCP_MAX_BUFF_SIZE;
	}
	if (expected > maxLength) {
		expected = maxLength;
	}
	*((long*)packet.borrow()) = expected;*/

	typedef struct {
		unsigned int maxLength; 	// number of bytes to return in SDCPP_READ_RESPONSE packet
		char termchar;				// if not -1, then terminate if this character is read
	} ReadData;
	
	// send a SDCPP_READ packet
	ReadData* readData = new ReadData;	// we allocate it with "new", rather than malloc, so it can be freed by packet destructor
	readData->maxLength = maxLength;
	readData->termchar = termchar;
	Packet packet(SDCPP_READ, (char*)readData, sizeof(ReadData));
	if ((e=packet.sendToSocket(linkid, sockfd))!=CONNECTION_SUCCESS) {
		disconnect(CONNECTION_PACKET_ERROR);
		return e;
	}
	
	// receive a SDCPP_READ response packet
	if ((e=packet.readFromSocket(linkid, sockfd))!=CONNECTION_SUCCESS) {
		disconnect(CONNECTION_PACKET_ERROR);
		return e;
	}
	bool endchar = false;
	if (packet.purpose==SDCPP_READ_END) {
		endchar = true;
	} else if (packet.purpose==SDCPP_IO_TIMEOUT) {
		spLogDebug(1, "Warning, linkid: %ld, Read timeout occured", linkid);
		return CONNECTION_IO_TIMEOUT;
	} else if (packet.purpose!=SDCPP_READ) {
		spLogError("Error, linkid: %ld, unexpected packet from server of type %d, expected SDCPP_READ", linkid, packet.purpose);
		disconnect(CONNECTION_PACKET_ERROR);
		return CONNECTION_PACKET_ERROR;
	}
	if (packet.length()>maxLength) {
		spLogError("Error, linkid: %ld, SDCPP_READ was longer than requested size", linkid);
		disconnect(CONNECTION_PACKET_ERROR);
		return CONNECTION_PACKET_ERROR;
	}
	data = packet.detach(length);
	if (endchar) {
		return CONNECTION_ENDCHAR;
	} else {
		return CONNECTION_SUCCESS;
	}
}

// sends a block of data to the server
// returns length on success
// if disown is true, data will be freed
// data must be NULL or allocated
// if disown is true data must be allocated with new[]
// if endchar is true the END flag shall be ascociated with the last byte of data
int Connection::writeBlock(char* data, unsigned int length, bool disown, bool endchar)
{
	if (linkid==-1) {
		spLogError("Error, connection is not open, could not write block");
		return CONNECTION_SOCKET_ERROR;
	}
	
	SCPIPurpose purpose;
	if (endchar) {
		purpose = SDCPP_WRITE_END;
	} else {
		purpose = SDCPP_WRITE;
	}

	Packet packet(purpose, data, length);
	
	int e;
	
	
	e=packet.sendToSocket(linkid, sockfd);
	if (!disown) {	// always detach the data again after the call, because we don't want Packet to free it
		// this is mearly a formality, if the *data or length we started with is not what we get from detach, then :/
		// they should be assigned the values they already had
		// this tells packet not to delete *data
		data = packet.detach(length);
	}
	// send SDCPP_WRITE
	if (e!=CONNECTION_SUCCESS) {	//TODO, e should be number of succesfully written bytes, which if wrong is link fatal
		disconnect(e);
		return e;
	}
	
	// receive a SDCPP_WRITE packet
	if ((e=packet.readFromSocket(linkid, sockfd))!=CONNECTION_SUCCESS) {
		disconnect(e);
		return e;
	}
	if (packet.purpose==SDCPP_PARSE_ERROR) {
		//return CONNECTION_PARSE_ERROR;
		// we don't care
	} else if (packet.purpose!=SDCPP_WRITE) {
		spLogError("Error, linkid: %ld, unexpected packet from server of type %d, expected SDCPP_WRITE", linkid, packet.purpose);
		disconnect(CONNECTION_PACKET_ERROR);
		return CONNECTION_PACKET_ERROR;
	}
	if (packet.length()==0) {
		spLogError("Error, linkid: %ld, SDCPP_WRITE SCPI server responded with IO error", linkid);
		return CONNECTION_IO_ERROR;
	}
	if (packet.length()!=sizeof(unsigned int)) {
		spLogError("Error, linkid: %ld, SDCPP_WRITE response packet was of incorrect length", linkid);
		disconnect(CONNECTION_PACKET_ERROR);
		return CONNECTION_PACKET_ERROR;
	}
	return *(unsigned int*)packet.borrow();
	// data gets freed after packet goes out of scope
}

int Connection::lockDevice(unsigned int timeOut)
{
	/*if (linkid==-1) {
		spLogError("Error, connection is not open, could not lock device");
		return CONNECTION_SOCKET_ERROR;
	}

	Packet packet(SDCPP_LOCK);
	int e;
	// send SDCPP_LOCK
	if ((e=packet.sendToSocket(linkid, sockfd))!=CONNECTION_SUCCESS) {
		disconnect(e);
		return e;
	}
	
	// receive a SDCPP_LOCK packet
	if ((e=packet.readFromSocket(linkid, sockfd))!=CONNECTION_SUCCESS) {
		disconnect(e);
		return e;
	}
	if (packet.purpose!=SDCPP_LOCK) {
		spLogError("Error, linkid: %ld, unexpected packet from server of type %d, expected SDCPP_LOCK", linkid, packet.purpose);
		disconnect(CONNECTION_PACKET_ERROR);
		return CONNECTION_PACKET_ERROR;
	}
	if (packet.length()!=0) {
		spLogError("Error, linkid: %ld, SDCPP_LOCK response packet was of incorrect length", linkid);
		disconnect(CONNECTION_PACKET_ERROR);
		return CONNECTION_PACKET_ERROR;
	}*/
	return CONNECTION_SUCCESS;
}

int Connection::unlockDevice()
{
	/*if (linkid==-1) {
		spLogError("Error, connection is not open, could not lock device");
		return CONNECTION_SOCKET_ERROR;
	}

	Packet packet(SDCPP_UNLOCK);
	int e;
	// send SDCPP_UNLOCK
	if ((e=packet.sendToSocket(linkid, sockfd))!=CONNECTION_SUCCESS) {
		disconnect(e);
		return e;
	}
	
	// receive a SDCPP_UNLOCK packet
	if ((e=packet.readFromSocket(linkid, sockfd))!=CONNECTION_SUCCESS) {
		disconnect(e);
		return e;
	}
	if (packet.purpose!=SDCPP_UNLOCK) {
		spLogError("Error, linkid: %ld, unexpected packet from server of type %d, expected SDCPP_UNLOCK", linkid, packet.purpose);
		disconnect(CONNECTION_PACKET_ERROR);
		return CONNECTION_PACKET_ERROR;
	}
	if (packet.length()!=0) {
		spLogError("Error, linkid: %ld, SDCPP_UNLOCK response packet was of incorrect length", linkid);
		disconnect(CONNECTION_PACKET_ERROR);
		return CONNECTION_PACKET_ERROR;
	}*/
	return CONNECTION_SUCCESS;
}

int Connection::getStatusByte()
{
	return '\0';
	
	
	/*data = NULL;
	length = 0;
	if (linkid==-1) {
		spLogError("Error, connection is not open, could not read block");
		return CONNECTION_SOCKET_ERROR;
	}
	int e;

	typedef struct {
		unsigned int maxLength; 	// number of bytes to return in SDCPP_READ_RESPONSE packet
		char termchar;				// if not -1, then terminate if this character is read
	} ReadData;
	
	// send a SDCPP_READ packet
	ReadData* readData = new ReadData;	// we allocate it with "new", rather than malloc, so it can be freed by packet destructor
	readData->maxLength = maxLength;
	readData->termchar = termchar;
	Packet packet(SDCPP_READ, (char*)readData, sizeof(ReadData));
	if ((e=packet.sendToSocket(linkid, sockfd))!=CONNECTION_SUCCESS) {
		disconnect(CONNECTION_PACKET_ERROR);
		return e;
	}
	
	// receive a SDCPP_READ response packet
	if ((e=packet.readFromSocket(linkid, sockfd))!=CONNECTION_SUCCESS) {
		disconnect(CONNECTION_PACKET_ERROR);
		return e;
	}
	bool endchar = false;
	if (packet.purpose==SDCPP_READ_END) {
		endchar = true;
	} else if (packet.purpose==SDCPP_IO_TIMEOUT) {
		spLogDebug(1, "Warning, linkid: %ld, Read timeout occured", linkid);
		return CONNECTION_IO_TIMEOUT;
	} else if (packet.purpose!=SDCPP_READ) {
		spLogError("Error, linkid: %ld, unexpected packet from server of type %d, expected SDCPP_READ", linkid, packet.purpose);
		disconnect(CONNECTION_PACKET_ERROR);
		return CONNECTION_PACKET_ERROR;
	}
	if (packet.length()>maxLength) {
		spLogError("Error, linkid: %ld, SDCPP_READ was longer than requested size", linkid);
		disconnect(CONNECTION_PACKET_ERROR);
		return CONNECTION_PACKET_ERROR;
	}
	data = packet.detach(length);
	if (endchar) {
		return CONNECTION_ENDCHAR;
	} else {
		return CONNECTION_SUCCESS;
	}*/
}

void Connection::disconnect()
{
	disconnect(CONNECTION_SUCCESS);
}

// if error!=CONNECTION_SUCCESS then an error message is sent
void Connection::disconnect(int error)
{
	if (error!=CONNECTION_SUCCESS) {
		spLogError("Error, linkid: %ld, aborting connection", linkid);
	}
	
	if (linkid==-1) {
		sockfd = -1;
		return;
	}

	// We say goodbye to the server, there is no error for us if this does not arrive
	// this prevents error mesages on the log from the other daemon if we break uncleanly
	Packet packet(SDCPP_CLOSE);
	if (error!=CONNECTION_SOCKET_ERROR) {
		// we do not want to send this if the socket is closed due to a 
		// CONNECTION_SOCKET_ERROR, otherwise a SIGPIPE is raised on write(2)
		packet.sendToSocket(linkid, sockfd);
	}
	close(sockfd);
	linkid = -1;
	sockfd = -1;
}

/*// if error!=CONNECTION_SUCCESS then an error message is sent
void Connection::reconnect(int error)
{
	if (error!=CONNECTION_SUCCESS) {
		spLogError("Error, linkid: %ld, restarting SCPI connection", linkid);
	}
	disconnect();
	
	start("127.0.0.1", PORT);
}*/

long Connection::getLinkid()
{
	return linkid;
}

// will attempt to verify whether the link still exists from the 
// perspective of the SCPIServer
bool Connection::checkIfValid()
{
	if (linkid==-1) {
		// this connection is already in a closed state
		spLogDebug(2, "linkid: %ld, Connection is not valid, connection already closed", linkid);
		return false;
	} else {
		spLogDebug(3, "linkid: %ld, Sending ping packet to SCPIServer", linkid);
		
		Packet packet(SDCPP_PING, NULL, 0);
		int e;
		e=packet.sendToSocket(linkid, sockfd);
		
		// send SDCPP_PING
		if (e!=CONNECTION_SUCCESS) {
			disconnect(e);
			spLogDebug(2, "linkid: %ld, Connection is not valid, ping failed on write", linkid);
			return false;
		}
	
		// receive a SDCPP_PING packet
		if ((e=packet.readFromSocket(linkid, sockfd))!=CONNECTION_SUCCESS) {
			disconnect(e);
			spLogDebug(2, "linkid: %ld, Connection is not valid, ping failed on read", linkid);
			return false;
		}
	}
	return true;
}

// ***************************** static exposed functions *****************************
Connection* findConnection(long linkid)
{
	// looks up in the connections map object to find the link id
	// checks that the linkid matches the object
	// returns the connection if it is found, NULL otherwise
	if (linkid==-1) {
		spLogError("Error, VXI11 server tried to find connection with invalid linkid -1");
		return NULL;
	}
	map<long, Connection*>::iterator i = connections.find(linkid);
	if (i==connections.end()) {
		spLogDebug(2, "Connection for requested linkid %ld could not be found", linkid);
		return NULL;	// connection not found
	}
	if (i->second->getLinkid()==-1) {
		spLogError("Connection to SCPI server for linkid %ld has been closed, connection removed", linkid);
		spLogDebug(1, "Removing connection #2, linkid: %d, connections.size(): %d", i->second->getLinkid(), connections.size()-1);
		delete(i->second);
		connections.erase(i);
		return NULL;
	}
	if (linkid!=i->second->getLinkid()) {
		spLogError("Error, linkid in map and linkid of Connection differ, connection terminated");
		spLogDebug(1, "Removing connection #3, linkid: %d, connections.size(): %d", i->second->getLinkid(), connections.size()-1);
		delete(i->second);
		connections.erase(i);
		return NULL;
	}
	return i->second;
}

// same as findConnection except connection is removed from the map
// 0 on failure
// 1 on success
int removeConnection(long linkid)
{
	// ensure that all open DeviceConnection objects still have a TCP stream 
	// open to the SCPIServer. Close any that do not
	garbageCollect();
	
	// looks up in the connections map object to find the link id
	// checks that the linkid matches the object
	// returns the connection if it is found, NULL otherwise
	if (linkid==-1) {
		spLogError("Warning, VXI11 server tried to remove connection with invalid linkid -1");
		return 0;
	}
	map<long, Connection*>::iterator i = connections.find(linkid);
	if (i==connections.end()) {
		spLogWarning("Warning, linkid: %ld not found, VXI11 server could not remove connection", linkid);
		return 0;
	}
	
	spLogDebug(1, "Removing connection #1, linkid: %d, connections.size(): %d", i->second->getLinkid(), connections.size()-1);
	delete(i->second);
	connections.erase(i);

	return 1;		//TODO, this doesn't follow return value convention
}

//remove all closed or very old connections
void garbageCollect()
{
	map<long, Connection*>::iterator i = connections.begin();
	while(i != connections.end())
	{
		if (!(i->second->checkIfValid())) {
			spLogNotice("garbage collection removing connection, linkid: %d, "
				"connections.size(): %d", i->second->getLinkid(), 
				connections.size()-1);
			delete(i->second);
			connections.erase(i++);
		} else {
			++i;
		}
	}
}

void removeAllConnections()
{
	spLogNotice("Closing all active connections to device");
	for (map<long, Connection*>::iterator i=connections.begin(); i!=connections.end(); ++i)
	{
		delete(i->second);
	}
	connections.clear();
}

Connection* getNewConnection(string device) {
	// in theory each device has its own map of connections
	// with unique linkid's or something
	// since we are only dealing with 1 we ignore it
	if (device!="inst0") {
		spLogError("Error, a VXI11 client tried to create new connection to device %s, only inst0 supported", device.c_str());
		return NULL;
	}

	// ensure that all open DeviceConnection objects still have a TCP stream 
	// open to the SCPIServer. Close any that do not
	garbageCollect();
	Connection *temp = new Connection();
	// TODO, error check start() for different types of errors
	temp->start("127.0.0.1", PORT);	// if there are multiple devices then PORT is looked up in a table coresponding to device number inst#
	if (temp->getLinkid()==-1) {
		delete(temp);
		spLogError("Error, Could not establish new connection to device: %s", device.c_str());
		return NULL;
	}

	map<long, Connection*>::iterator i = connections.find(temp->getLinkid());
	if (i!=connections.end()) {
		spLogWarning("Warning, SCPIServer has reused a previously assigned "
			"linkid: %ld for a new link, the old link has been freed", 
			temp->getLinkid());
		// This means that the SCPIServer has given us a new linkid
		// that already has a connection object associated with it.
		// If you see this you have a problem in the SCPIServer
		// The SCPIServer is not assigning unique linkid's properly
		// This may also be an indication that the SCPIServer has 
		// been restarted
		
		// we need to close the old connection object associated with the
		// reused linkid which will close the TCP stream connecting to the
		// SCPIServer
		delete(i->second);
		connections.erase(i);
	}

	pair<map<long, Connection*>::iterator,bool> p = connections.insert(make_pair(temp->getLinkid(), temp));
	if (p.second==false) {
		spLogError("Error, SCPIServer has reused a previously assigned "
			"linkid for a new link, the original connection object has "
			"memory leaked");
		// we shouldn't ever get here. If we do then the connection object will memoryleak
	}
	spLogDebug(1, "New connection, linkid: %d, connections.size(): %d", temp->getLinkid(), connections.size());

	return temp;
}

