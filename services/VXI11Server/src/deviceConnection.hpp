#ifndef DEVICECONNECTIONS_HPP
#define DEVICECONNECTIONS_HPP

#define PORT 10010

#define TCP_MAX_BUFF_SIZE 1024


#define CONNECTION_IO_ERROR 3		// non fatal
#define CONNECTION_IO_TIMEOUT 2		// non fatal
#define CONNECTION_ENDCHAR 1		// expected behaviour
#define CONNECTION_SUCCESS 0		// expected behaviour
#define CONNECTION_SOCKET_ERROR -1	// link fatal
#define CONNECTION_PACKET_ERROR -2	// link fatal
#define CONNECTION_PARSE_ERROR -3	// non fatal
#define CONNECTION_MISC_ERROR -4	// link fatal
// c++
#include <string>
#include <map>
using namespace std;

// c
#include <cstdlib>
#include <errno.h>

// TODO remove
#include <cstdio>

// network
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/stat.h>

// dependancies
#include "logging.h"

typedef enum {
	SDCPP_NONE			= 0,
	SDCPP_CLOSE			= 1,
	SDCPP_CONNECT	 	= 2,
	SDCPP_WRITE			= 3,
	SDCPP_READ			= 4,
	SDCPP_STATUS	 	= 5,
	SDCPP_END			= 6,
	SDCPP_PARSE_ERROR	= 7,
	SDCPP_IO_TIMEOUT	= 8,
	SDCPP_READ_END		= 9,
	SDCPP_WRITE_END		= 10,
	SDCPP_IO_ERROR		= 11,
	SDCPP_PING			= 12
} SCPIPurpose;

// ***************************** connection class *****************************

// the connection class represents a link to a device
// links have linkid's, which are assigned by the device.
// each link should correspond to a single user or stream of
// commands. This is a called a link in VXI11, and the SCPI 
// parser, a session in VISA and corresponds to a single TCP
// stream in this module
// The "device" should be a SCPI parser daemon. Each device should
// be bound to a different port on the local host. Translation 
// device number to port number should be known by our parent (findConnection)
class Connection
{
private:
	long linkid;	// linkid -1 indicates an inactive Connection, will have negative sockfd
	int sockfd;
	//TODO: connection should have a notion of timeouts so these do not have to be specified on each call
public:
	Connection();
	~Connection();
	
	int start(string host, int port);
	int setEndchar(char endchar);
	int readBlock(char*& data, unsigned int &length, unsigned int maxLength, char termchar);
	
	// returns CONNECTION_PARSE_ERROR on a non fatal parse error
	// a negative error code on a link fatal error
	// otherwise the number of bytes written to the device
	// if endchar is true the END flag shall be ascociated with the last byte of data
	int writeBlock(char* data, unsigned int length, bool disown, bool endchar);
	int lockDevice(unsigned int timeOut);
	int unlockDevice();
	int getStatusByte();
	
	long getLinkid();
	
	// will attempt to verify whether the link still exists from the 
	// perspective of the SCPIServer
	bool checkIfValid();
	
	void disconnect(int error);
	void disconnect();
	void reconnect(int error);
};

// ***************************** static exposed functions *****************************
Connection* findConnection(long linkid);
int removeConnection(long linkid);
void garbageCollect();	//remove all closed or very old connections
void removeAllConnections();
Connection* getNewConnection(string device);

//TODO, add a call that will check all connections to ensure they are still valid/open

#endif //DEVICECONNECTIONS_HPP
