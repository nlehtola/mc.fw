#include <svc_modded.h>

void
vxi_svc_getreq_poll (struct pollfd *pfdp, int pollretval)
{
  if (pollretval == 0)
    return;

  register int fds_found;
  for (int i = fds_found = 0; i < svc_max_pollfd; ++i)
    {
      register struct pollfd *p = &pfdp[i];

      if (p->fd != -1 && p->revents)
	{
	  /* fd has input waiting */
	  if (p->revents & POLLNVAL)
	    xprt_unregister (xports[p->fd]);
	  else
	    svc_getreq_common (p->fd);

	  if (++fds_found >= pollretval)
	    break;
	}
    }
}

void
vxi_svc_run (void)
{
  int i;
  struct pollfd *my_pollfd = NULL;
  int last_max_pollfd = 0;

  for (;;)
    {
      int max_pollfd = svc_max_pollfd;
      if (max_pollfd == 0 && svc_pollfd == NULL)
	break;

      if (last_max_pollfd != max_pollfd)
	{
	  struct pollfd *new_pollfd
	    = realloc (my_pollfd, sizeof (struct pollfd) * max_pollfd);

	  if (new_pollfd == NULL)
	    {
	      perror (_("svc_run: - out of memory"));
	      break;
	    }

	  my_pollfd = new_pollfd;
	  last_max_pollfd = max_pollfd;
	}

      for (i = 0; i < max_pollfd; ++i)
	{
	  my_pollfd[i].fd = svc_pollfd[i].fd;
	  my_pollfd[i].events = svc_pollfd[i].events;
	  my_pollfd[i].revents = 0;
	}

      switch (i = __poll (my_pollfd, max_pollfd, -1))
	{
	case -1:
	  if (errno == EINTR)
	    continue;
	  perror (_("svc_run: - poll failed"));
	  break;
	case 0:
	  continue;
	default:
	  svc_getreq_poll (my_pollfd, i);
	  continue;
	}
      break;
    }

  free (my_pollfd);
}
