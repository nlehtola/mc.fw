#include "ExcecutionQueue.hpp"
#include "SCPIServerState.hpp"

#define LOCK_QUEUE pthread_mutex_lock(&queueMutex);
#define UNLOCK_QUEUE pthread_mutex_unlock(&queueMutex);

ExcecutionQueue::ExcecutionQueue(long linkid)
{
	pthread_mutex_init(&queueMutex, NULL);
	this->linkid = linkid;
	parseError = false;
}

ExcecutionQueue::~ExcecutionQueue()
{
	LOCK_QUEUE	
	while (!decodedMessages.empty())
	{
		DecodedMessageList *decodedMessageList = decodedMessages.front();
		decodedMessages.pop();
		for (DecodedMessageList::iterator i=decodedMessageList->begin(); i!=decodedMessageList->end(); ++i) {
			delete(*i);
		}
		delete(decodedMessageList);
	}
	UNLOCK_QUEUE
	pthread_mutex_destroy(&queueMutex);
}

void ExcecutionQueue::pushDecodedMessages(DecodedMessageList*& x)
{
	LOCK_QUEUE
	decodedMessages.push(x);
	UNLOCK_QUEUE
	
	while(1) {
		LOCK_QUEUE
		bool isEmpty = decodedMessages.empty();
		UNLOCK_QUEUE
		if (isEmpty) {
			break;
		}	
	}
}

int ExcecutionQueue::excecuteNext(SCPIServerState& instrumentState)
{
	int executeStatus = EXECUTE_NOCOMMAND;
	LOCK_QUEUE
	if (!decodedMessages.empty()) {
		string response = "";
		DecodedMessageList *decodedMessageList = decodedMessages.front();
		
		// if we get to here then a command has been sent by the user, we do 
		// not know yet if it has succeded, but if there is an error this flag
		// will be set to something other than success
		executeStatus = EXECUTE_SUCCESS;
		if (decodedMessageList==NULL) {
			parseError = true;
		} else {
			for (DecodedMessageList::iterator i=decodedMessageList->begin(); i!=decodedMessageList->end(); ++i) {
				if (i!=decodedMessageList->begin()) {
					response += ";";
				}
				int e;
				if ((*i)->isCLS()) {
					// *CLS is a special command which clears the reponse buffer of the current stream

					//  clear the responseMessages queue
					while(!responseMessages.empty()) {
						responseMessages.pop();
					}
					// TODO, clear the status byte and error queue
					//instrumentState.clear();
					response = "";
					spLogDebug(1, "executing *CLS: Clearing output buffer");
				} else {
					e = (*i)->execute(response, instrumentState);	//TODO, do not excecute under the mutex so the parser thread won't stall, (this messes with !decodedMessages.empty() logic in getNextResponseMessage)
				}
				delete(*i);
				if (e!=COMMAND_SUCCESS) {
					//TODO, add something to the status byte
					// TODO: distinguish between timeout error and other errors
					executeStatus = EXECUTE_ERROR;
					instrumentState.recordError(e);	//since this has a mutex, on it, best not to impliment it while under our own mutex (potential deadlock)
				}
			}
			delete(decodedMessageList);
			if (response!="") {
				response+="\n";
				// This shouldn't really be a queue
				while(!responseMessages.empty()) {
					responseMessages.pop();
				}
				responseMessages.push(response);
			}
		}
		// wait until command excecutes, then pop from command queue
		decodedMessages.pop();
	}
	UNLOCK_QUEUE
	return executeStatus;
}

//TODO, mutex timeout
uint8_t ExcecutionQueue::getStatusByte(SCPIServerState& instrumentState)
{
	//TODO, get device status byte info (instrument level)
	uint8_t SESRbyte = 0;
	uint8_t statusbyte = 0;
	LOCK_QUEUE
	if (parseError) {
		SESRbyte |= SCPI_SESR_CME;
	}
	if(!responseMessages.empty()) {
		statusbyte |= SCPI_STB_MAV;
	}
	if (SESRbyte!=0) {
		statusbyte |= SCPI_STB_SESR;
	}
	UNLOCK_QUEUE
	return statusbyte;
}
//TODO, mutex timeout
string ExcecutionQueue::getNextResponseMessage()
{
	string response;
	LOCK_QUEUE
	while (!decodedMessages.empty()) {
		UNLOCK_QUEUE
		// TODO timeout
		usleep(1000);	// sleep for 1ms to prevent starvation
		LOCK_QUEUE
	}
	// if we know that no responses will ever be generated then there is no point waiting for them
	if (!responseMessages.empty()) {
		response = responseMessages.front();
		responseMessages.pop();
	} else {
		response = "";
	}
	UNLOCK_QUEUE
	return response;
}
