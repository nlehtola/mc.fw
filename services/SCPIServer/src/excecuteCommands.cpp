#include "excecuteCommands.hpp"
#include "SCPIServerState.hpp"
#include "Units.hpp"
#include "version.h"

// Dither
int execSBSDitherState(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values)
{	
	spLogDebug(1, "executing SOUR%d:CHAN%d:SBS:STATE", blade, laser);
	bool e = false;
	stringstream ss;
	if (task==STATE_ON) {
		spLogDebug(1, "Turning SBS %d %d ON", blade, laser);
		setBoolState(e, blade, laser, true, S2T_SET_STATE_SBS, instrumentState);
	} else if (task==STATE_OFF) {
		spLogDebug(1, "Turning SBS %d %d OFF", blade, laser);
		setBoolState(e, blade, laser, false, S2T_SET_STATE_SBS, instrumentState);
	} else if (task==TASK_QUERY) {
		if (blade==0) {
			spLogDebug(1, "Excecute error, blade id invalid in OUTP[n]:CHAN[m]:SBS:STATE");
	 		return COMMAND_ERROR;
		}
		if (laser==0) {
			spLogDebug(1, "Excecute error, laser id invalid in OUTP[n]:CHAN[m]:SBS:STATE");
			return COMMAND_ERROR;
		}
		
		if (getBoolState(e, blade, laser, S2T_GET_STATE_SBS, instrumentState)) {
			ss << "ON";
			
		} else {
			ss << "OFF";
		}
	} else {
		spLogError("Command Error, TaskID for OUTP[n]:CHAN[m]:SBS:STATE was incorrect: %d", task);
		// If you see this error you must look in 
		// commandTable.cpp -> ArgumentNode stateArgs
		return COMMAND_ERROR;
	}
	if (e) {
		return COMMAND_ERROR;
	} else {
		response += ss.str();
	}
	return COMMAND_SUCCESS;
}

int execSBSDitherRate(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values)
{
	spLogDebug(1, "executing SOUR%d:CHAN%d:SBS:RATE", blade, laser);
	double temp;
	bool e = false;
	stringstream ss;
	ss.precision(FLOAT_PRECISION_SBSRATE);
	ss << std::scientific;
	switch (task)
	{
		case MIN:
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_MINSBSRATE, instrumentState), UNIT_KHZ, defaultUnit);
			ss << temp;
			break;
		case MAX:
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_MAXSBSRATE, instrumentState), UNIT_KHZ, defaultUnit);
			ss << temp;
			break;
		case TASK_SET:
			if (values.size()!=1) {
				return COMMAND_ERROR;
			}
			setDoubleValue(e, blade, laser, values[0]->get(UNIT_KHZ), S2T_SET_SBSRATE, instrumentState);
			break;
		case ACTUAL:
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_ACTSBSRATE, instrumentState), UNIT_KHZ, defaultUnit);
			ss << temp;
			break;
		case ALL:
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_MINSBSRATE, instrumentState), UNIT_KHZ, defaultUnit);
			ss << temp << ",";
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_MAXSBSRATE, instrumentState), UNIT_KHZ, defaultUnit);
			ss << temp << ",";
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_ACTSBSRATE, instrumentState), UNIT_KHZ, defaultUnit);
			ss << temp;
			break;
		default:
			spLogError("Command Error, TaskID for SOUR[n]:CHAN[m]:SBS:RATE was incorrect: %d", task);
			// If you see this error you must look in 
			// commandTable.cpp -> ArgumentNode ditherArgs
			return COMMAND_ERROR;
	}
	if (e) {
		return COMMAND_ERROR;
	} else {
		response += ss.str();
	}
	return COMMAND_SUCCESS;
}
int execSBSDitherFreq(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values)
{
	spLogDebug(1, "executing SOUR%d:CHAN%d:SBS:FREQ", blade, laser);
	double temp;
	bool e = false;
	stringstream ss;
	ss.precision(FLOAT_PRECISION_FREQ);
	ss << std::scientific;
	switch (task)
	{
		case MIN:
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_MINSBSFREQ, instrumentState), UNIT_MHZ, defaultUnit);
			ss << temp;
			break;
		case MAX:
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_MAXSBSFREQ, instrumentState), UNIT_MHZ, defaultUnit);
			ss << temp;
			break;
		case TASK_SET:
			if (values.size()!=1) {
				return COMMAND_ERROR;
			}
			setDoubleValue(e, blade, laser, values[0]->get(UNIT_MHZ), S2T_SET_SBSFREQ, instrumentState);
			break;
		case ACTUAL:
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_ACTSBSFREQ, instrumentState), UNIT_MHZ, defaultUnit);
			ss << temp;
			break;
		case ALL:
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_MINSBSFREQ, instrumentState), UNIT_MHZ, defaultUnit);
			ss << temp << ",";
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_MAXSBSFREQ, instrumentState), UNIT_MHZ, defaultUnit);
			ss << temp << ",";
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_ACTSBSFREQ, instrumentState), UNIT_MHZ, defaultUnit);
			ss << temp;
			break;
		default:
			spLogError("Command Error, TaskID for SOUR[n]:CHAN[m]:SBS:FREQ was incorrect: %d", task);
			// If you see this error you must look in 
			// commandTable.cpp -> ArgumentNode ditherArgs
			return COMMAND_ERROR;
	}
	if (e) {
		return COMMAND_ERROR;
	} else {
		response += ss.str();
	}
	return COMMAND_SUCCESS;
}

int execControlledDitherState(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values)
{	
	spLogDebug(1, "executing SOUR%d:CHAN%d:DITHER:STATE", blade, laser);
	bool e = false;
	stringstream ss;
	if (task==STATE_ON) {
		spLogDebug(1, "Turning controlled dither  %d %d ON", blade, laser);
		setBoolState(e, blade, laser, true, S2T_SET_STATE_CONTROL_DITHER, instrumentState);
	} else if (task==STATE_OFF) {
		spLogDebug(1, "Turning controlled dither %d %d OFF", blade, laser);
		setBoolState(e, blade, laser, false, S2T_SET_STATE_CONTROL_DITHER, instrumentState);
	} else if (task==TASK_QUERY) {
		if (blade==0) {
			spLogDebug(1, "Excecute error, blade id invalid in OUTP[n]:CHAN[m]:DITHER:STATE");
	 		return COMMAND_ERROR;
		}
		if (laser==0) {
			spLogDebug(1, "Excecute error, laser id invalid in OUTP[n]:CHAN[m]:DITHER:STATE");
			return COMMAND_ERROR;
		}
		
		if (getBoolState(e, blade, laser, S2T_GET_STATE_CONTROL_DITHER, instrumentState)) {
			ss << "ON";
		} else {
			ss << "OFF";
		}
	} else {
		spLogError("Command Error, TaskID for OUTP[n]:CHAN[m]:DITHER:STATE was incorrect: %d", task);
		// If you see this error you must look in 
		// commandTable.cpp -> ArgumentNode stateArgs
		return COMMAND_ERROR;
	}
	if (e) {
		return COMMAND_ERROR;
	} else {
		response += ss.str();
	}
	return COMMAND_SUCCESS;
}

// Source:Frequency
int execFreqFine(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values)
{
	spLogDebug(1, "executing SOUR%d:CHAN%d:FREQ:FINE", blade, laser);
	double temp;
	bool e = false;
	stringstream ss;
	ss.precision(FLOAT_PRECISION_FREQ);
	ss << std::scientific;
	switch (task)
	{
		case MIN:
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_MINFINE, instrumentState), UNIT_MHZ, defaultUnit);
			ss << temp;
			break;
		case MAX:
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_MAXFINE, instrumentState), UNIT_MHZ, defaultUnit);
			ss << temp;
			break;
		case TASK_SET:
			if (values.size()!=1) {
				return COMMAND_ERROR;
			}
			setDoubleValue(e, blade, laser, values[0]->get(UNIT_MHZ), S2T_SET_FINE, instrumentState);
			break;
		case ACTUAL:
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_ACTFINE, instrumentState), UNIT_MHZ, defaultUnit);
			ss << temp;
			break;
		case ALL:
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_MINFINE, instrumentState), UNIT_MHZ, defaultUnit);
			ss << temp << ",";
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_MAXFINE, instrumentState), UNIT_MHZ, defaultUnit);
			ss << temp << ",";
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_ACTFINE, instrumentState), UNIT_MHZ, defaultUnit);
			ss << temp;
			break;
		default:
			spLogError("Command Error, TaskID for SOUR[n]:CHAN[m]:FREQ:FINE was incorrect: %d", task);
			// If you see this error you must look in 
			// commandTable.cpp -> ArgumentNode fineArgs
			return COMMAND_ERROR;
	}
	if (e) {
		return COMMAND_ERROR;
	} else {
		response += ss.str();
	}
	return COMMAND_SUCCESS;
}

// Operation

// Questionable

// Output:Power
int execLaserPowerUnit(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values)
{
	spLogDebug(1, "executing OUTP%d:CHAN%d:POW:UNIT?", blade, laser);

	response += "dBm";

	return COMMAND_SUCCESS;
}

// Output
int execLaserState(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values)
{
	spLogDebug(1, "executing OUTP%d:CHAN%d:STATE", blade, laser);
	bool e = false;
	stringstream ss;
	if (task==STATE_ON) {
		spLogDebug(1, "Turning laser %d %d ON", blade, laser);
		setBoolState(e, blade, laser, true, S2T_SET_STATE_LASER, instrumentState);
	} else if (task==STATE_OFF) {
		spLogDebug(1, "Turning laser %d %d OFF", blade, laser);
		setBoolState(e, blade, laser, false, S2T_SET_STATE_LASER, instrumentState);
	} else if (task==TASK_QUERY) {
		if (blade==0) {
			spLogDebug(1, "Excecute error, blade id invalid in OUTP[n]:CHAN[m]:STATE");
	 		return COMMAND_ERROR;
		}
		if (laser==0) {
			spLogDebug(1, "Excecute error, laser id invalid in OUTP[n]:CHAN[m]:STATE");
			return COMMAND_ERROR;
		}
		
		if (getBoolState(e, blade, laser, S2T_GET_STATE_LASER, instrumentState)) {
			ss << "ON";
			
		} else {
			ss << "OFF";
		}
	} else {
		spLogError("Command Error, TaskID for OUTP[n]:CHAN[m]:STATE was incorrect: %d", task);
		// If you see this error you must look in 
		// commandTable.cpp -> ArgumentNode stateArgs
		return COMMAND_ERROR;
	}
	if (e) {
		return COMMAND_ERROR;
	} else {
		response += ss.str();
	}
	return COMMAND_SUCCESS;
}

int execLaserSafety(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values)
{
	spLogDebug(1, "executing OUTP%d:CHAN%d:SAFETY", blade, laser);
	bool e = false;
	stringstream ss;
	if (task==STATE_ON) 
	{
		spLogDebug(1, "Laser %d %d Safety mode is set to ON", blade, laser);
		setBoolState(e, blade, laser, true, S2T_SET_SAFETY, instrumentState);
	}
	else if (task==STATE_OFF)
	{
		spLogDebug(1, "Laser %d %d Safty mode is set to OFF", blade, laser);
		setBoolState(e, blade, laser, false, S2T_SET_SAFETY, instrumentState);
	}
	else if (task==TASK_QUERY)
	{
		if (blade==0)
		{
			spLogDebug(1, "Excecute error, blade id invalid in OUTP[n]:CHAN[m]:SAFETY");
	 		return COMMAND_ERROR;
		}
		if (laser==0)
		{
			spLogDebug(1, "Excecute error, laser id invalid in OUTP[n]:CHAN[m]:SAFETY");
			return COMMAND_ERROR;
		}
		
		if (getBoolState(e, blade, laser, S2T_GET_SAFETY, instrumentState))
		{
			ss << "ON";
			
		}
		else
		{
			ss << "OFF";
		}
	} 
	else 
	{
		spLogError("Command Error, TaskID for OUTP[n]:CHAN[m]:SAFETY was incorrect: %d", task);
		// If you see this error you must look in 
		// commandTable.cpp -> ArgumentNode stateArgs
		return COMMAND_ERROR;
	}
	if (e)
	{
		return COMMAND_ERROR;
	}
	else 
	{
		response += ss.str();
	}
	return COMMAND_SUCCESS;
}

// Source
int execReset(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values)	//testing only: resets a laser
{
	spLogDebug(1, "executing SOUR%d:CHAN%d:RESET", blade, laser);
	spLogDebug(1, "Warning: SOUR[n]:CHAN[m]:RESET is a testing only command");
	spLogWarning("Warning: SOUR[n]:CHAN[m]:RESET is a testing only command");
	bool e = false;
	if (task==TASK_COMMAND) {
	 	if (blade==0) {
	 		// must specify slot number
	 		return COMMAND_ERROR;
	 	} else if (laser==0) {
			for (int i=1; i<MAX_NUM_LASERS; ++i) {
				laserReset(e, blade, i, instrumentState);
			}
		} else {
			laserReset(e, blade, laser, instrumentState);
		}
	} else {
		spLogError("Command Error, TaskID for SOUR[n]:CHAN[m]:RESET was incorrect: %d", task);
		// If you see this error you must look in 
		// commandTable.cpp -> ArgumentNode noCommandArgs
		return COMMAND_ERROR;
	}
	if (e) {
		return COMMAND_ERROR;
	}
	return COMMAND_SUCCESS;
}
int execPower(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values)
{
	spLogDebug(1, "executing SOUR%d:CHAN%d:POW", blade, laser);
	double temp;
	bool e = false;
	stringstream ss;
	ss.precision(FLOAT_PRECISION_POW);
	switch (task)
	{
		case MIN:
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_MINPOWER, instrumentState), UNIT_MDBM, defaultUnit);
			ss << temp;
			break;
		case MAX:
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_MAXPOWER,instrumentState), UNIT_MDBM, defaultUnit);
			ss << temp;
			break;
		case TASK_SET:
			if (values.size()!=1) {
				return COMMAND_ERROR;
			}
			setDoubleValue(e, blade, laser, values[0]->get(UNIT_MDBM), S2T_SET_POWER, instrumentState);
			break;
		case DESIRED:
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_DESPOWER, instrumentState), UNIT_MDBM, defaultUnit);
			ss << temp;
			break;
		case ACTUAL:
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_ACTPOWER, instrumentState), UNIT_MDBM, defaultUnit);
			ss << temp;
			break;
		case ALL:
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_MINPOWER, instrumentState), UNIT_MDBM, defaultUnit);
			ss << temp << ",";
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_MAXPOWER, instrumentState), UNIT_MDBM, defaultUnit);
			ss << temp << ",";
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_DESPOWER, instrumentState), UNIT_MDBM, defaultUnit);
			ss << temp << ",";
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_ACTPOWER, instrumentState), UNIT_MDBM, defaultUnit);
			ss << temp;
			break;
		default:
			spLogError("Command Error, TaskID for SOUR[n]:CHAN[m]:POW was incorrect: %d", task);
			// If you see this error you must look in 
			// commandTable.cpp -> ArgumentNode powerArgs
			return COMMAND_ERROR;
	}
	if (e) {
		return COMMAND_ERROR;
	} else {
		response += ss.str();
	}
	return COMMAND_SUCCESS;
}
int execWavelength(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values)
{
	spLogDebug(1, "executing SOUR%d:CHAN%d:WAVE", blade, laser);
	double temp;
	bool e = false;
	stringstream ss;
	ss.precision(FLOAT_PRECISION_WAV);
	ss << std::scientific;
	switch (task)
	{
		case MIN:	//MIN wavelength is max frequency
			temp = doUnitConvertion(MHzTom(getDoubleValue(e, blade, laser, S2T_GET_MAXFREQ, instrumentState)), UNIT_M, defaultUnit);
			ss << temp;
			break;
		case MAX:	//MAX wavelength is min frequency
			temp = doUnitConvertion(MHzTom(getDoubleValue(e, blade, laser, S2T_GET_MINFREQ, instrumentState)), UNIT_M, defaultUnit);
			ss << temp;
			break;
		case TASK_SET:
			if (values.size()!=1) {
				return COMMAND_ERROR;
			}
			setDoubleValue(e, blade, laser, mToMHzRoundToHundredmHz(values[0]->get(UNIT_M)), S2T_SET_FREQ, instrumentState);
			break;
		case DESIRED:
			temp = doUnitConvertion(MHzTom(getDoubleValue(e, blade, laser, S2T_GET_DESFREQ, instrumentState)), UNIT_M, defaultUnit);
			ss << temp;
			break;
		case TASK_LOCK:	//TODO this should be lock
			if (getBoolState(e, blade, laser, S2T_GET_LOCKFREQ, instrumentState)) {
				ss << "TRUE";
			} else {
				ss << "FALSE";
			}
			break;
		case ALL:
			temp = doUnitConvertion(MHzTom(getDoubleValue(e, blade, laser, S2T_GET_MAXFREQ, instrumentState)), UNIT_M, defaultUnit);
			ss << temp << ",";
			temp = doUnitConvertion(MHzTom(getDoubleValue(e, blade, laser, S2T_GET_MINFREQ, instrumentState)), UNIT_M, defaultUnit);
			ss << temp << ",";
			temp = doUnitConvertion(MHzTom(getDoubleValue(e, blade, laser, S2T_GET_DESFREQ, instrumentState)), UNIT_M, defaultUnit);
			ss << temp << ",";			
			if (getBoolState(e, blade, laser, S2T_GET_LOCKFREQ, instrumentState)) {
				ss << "TRUE";
			} else {
				ss << "FALSE";
			}
			break;
		default:
			spLogError("Command Error, TaskID for SOUR[n]:CHAN[m]:WAVE was incorrect: %d", task);
			// If you see this error you must look in 
			// commandTable.cpp -> ArgumentNode wavelengthArgs
			return COMMAND_ERROR;
	}
	if (e) {
		return COMMAND_ERROR;
	} else {
		response += ss.str();
	}
	return COMMAND_SUCCESS;
}
int execFrequency(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values)
{
	spLogDebug(1, "executing SOUR%d:CHAN%d:FREQ", blade, laser);
	double temp;
	bool e = false;
	stringstream ss;
	ss.precision(FLOAT_PRECISION_FREQ);
	ss << std::scientific;
	switch (task)
	{
		case MIN:
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_MINFREQ, instrumentState), UNIT_MHZ, defaultUnit);
			ss << temp;
			break;
		case MAX:
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_MAXFREQ, instrumentState), UNIT_MHZ, defaultUnit);
			ss << temp;
			break;
		case TASK_SET:
			if (values.size()!=1) {
				return COMMAND_ERROR;
			}
			setDoubleValue(e, blade, laser, values[0]->get(UNIT_MHZ), S2T_SET_FREQ, instrumentState);
			break;
		case DESIRED:
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_DESFREQ, instrumentState), UNIT_MHZ, defaultUnit);
			ss << temp;
			break;
		case TASK_LOCK:	//TODO this should be lock
			if (getBoolState(e, blade, laser, S2T_GET_LOCKFREQ, instrumentState)) {
				ss << "TRUE";
			} else {
				ss << "FALSE";
			}
			break;
		case ALL:
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_MINFREQ, instrumentState), UNIT_MHZ, defaultUnit);
			ss << temp << ",";
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_MAXFREQ, instrumentState), UNIT_MHZ, defaultUnit);
			ss << temp << ",";
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_DESFREQ, instrumentState), UNIT_MHZ, defaultUnit);
			ss << temp << ",";			
			if (getBoolState(e, blade, laser, S2T_GET_LOCKFREQ, instrumentState)) {
				ss << "TRUE";
			} else {
				ss << "FALSE";
			}
			break;
		default:
			spLogError("Command Error, TaskID for SOUR[n]:CHAN[m]:FREQ was incorrect: %d", task);
			// If you see this error you must look in 
			// commandTable.cpp -> ArgumentNode frequencyArgs
			return COMMAND_ERROR;
	}
	if (e) {
		return COMMAND_ERROR;
	} else {
		response += ss.str();
	}
	return COMMAND_SUCCESS;
}
int execGrid(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values)
{
	spLogDebug(1, "executing SOUR%d:CHAN%d:GRID", blade, laser);
	double temp;
	bool e = false;
	stringstream ss;
	ss.precision(FLOAT_PRECISION_GRID);
	ss << std::scientific;
	switch (task)
	{
		case MIN:
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_MINGRID, instrumentState), UNIT_MHZ, defaultUnit);
			ss << temp;
			break;
		case MAX:
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_MAXGRID, instrumentState), UNIT_MHZ, defaultUnit);
			ss << temp;
			break;
		case TASK_SET:
			if (values.size()!=1) {
				return COMMAND_ERROR;
			}
			setDoubleValue(e, blade, laser, values[0]->get(UNIT_MHZ), S2T_SET_GRID, instrumentState);
			break;
		case ACTUAL:
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_ACTGRID, instrumentState), UNIT_MHZ, defaultUnit);
			ss << temp;
			break;
		case ALL:
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_MINGRID, instrumentState), UNIT_MHZ, defaultUnit);
			ss << temp << ",";
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_MAXGRID, instrumentState), UNIT_MHZ, defaultUnit);
			ss << temp << ",";
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_ACTGRID, instrumentState), UNIT_MHZ, defaultUnit);
			ss << temp;
			break;
		default:
			spLogError("Command Error, TaskID for SOUR[n]:CHAN[m]:GRID was incorrect: %d", task);
			// If you see this error you must look in 
			// commandTable.cpp -> ArgumentNode gridArgs
			return COMMAND_ERROR;
	}
	if (e) {
		return COMMAND_ERROR;
	} else {
		response += ss.str();
	}
	return COMMAND_SUCCESS;
}
int execTemp(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values)
{
	spLogDebug(1, "executing SOUR[n]:CHAN[m]:TEMP");
	double temp;
	bool e = false;
	stringstream ss;
	if (task==TASK_QUERY) {
	 	double temp;
	 	if (blade==0) {
	 		return COMMAND_ERROR;
	 	} else if (laser==0) {
			for (int i=1;; ++i) {
				temp = doUnitConvertion(getDoubleValue(e, blade, i, S2T_GET_TEMP, instrumentState), UNIT_CC, defaultUnit);
				ss << temp;
				if (i>=MAX_NUM_LASERS) {
					break;
				}
				ss << ",";
			}
		} else {
			temp = doUnitConvertion(getDoubleValue(e, blade, laser, S2T_GET_TEMP, instrumentState), UNIT_CC, defaultUnit);
			ss << temp;
		}
	} else {
		spLogError("Command Error, TaskID for SOUR[n]:CHAN[m]:TEMP was incorrect: %d", task);
		// If you see this error you must look in 
		// commandTable.cpp -> ArgumentNode noQueryArgs
		return COMMAND_ERROR;
	}
	if (e) {
		return COMMAND_ERROR;
	} else {
		response += ss.str();
	}
 	return COMMAND_SUCCESS;
 }

// System
//int execError(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values)
int execVersion(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values)
{
	spLogDebug(1, "executing Version");

	response += TLSX_VERSION;

	return COMMAND_SUCCESS;
}


// Status

// GPIB:
//int execCLS(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values)
//int execESE(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values)
//int execESR(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values)
int execIDN(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values)
{
	spLogDebug(1, "executing IDN");
	bool e = false;
	string stringout;
	if (task==TASK_QUERY) {
		if (blade==0) {
			stringout = getIDN(e, instrumentState);
		} else {
			stringout = getIDNSlot(e, blade, instrumentState);
		}
	} else {
		spLogError("Command Error, TaskID for *IDN or SLOT[n]:IDN was incorrect: %d", task);
		// If you see this error you must look in 
		// commandTable.cpp -> ArgumentNode noQueryArgs
		return COMMAND_ERROR;
	}
	if (e) {
		return COMMAND_ERROR;
	} else {
		response += stringout;
	}
	return COMMAND_SUCCESS;
}

int execOPC(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values)
{
	spLogDebug(1, "executing OPC");
	string stringout;
	bool e = false;
	
	if (task==TASK_QUERY) 
	{
		// a bladeid of 0 returns opc for the instrument
		if (blade==0) 
		{
			stringout = getOPC(e, instrumentState);
		} 
		else 
		{
			stringout = getOPCSlot(e, blade, instrumentState);
		}
	} else {
		spLogError("Command Error, TaskID for *OPC or SLOT[n]:OPC was incorrect: %d", task);
		// If you see this error you must look in 
		// commandTable.cpp -> ArgumentNode noQueryArgs
		return COMMAND_ERROR;
	}
	if (e) {
		return COMMAND_ERROR;
	} else {
		response += stringout;
	}
	return COMMAND_SUCCESS;
}

int execOPT(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values)
{
	spLogDebug(1, "executing OPT");
	string stringout;
	bool e = false;
	
	if (task==TASK_QUERY) {
		// a bladeid of 0 returns opt for the instrument
		if (blade==0) {
			stringout = getOPT(e, instrumentState);
		} else {
			stringout = getOPTSlot(e, blade, instrumentState);
		}
	} else {
		spLogError("Command Error, TaskID for *OPT or SLOT[n]:OPT was incorrect: %d", task);
		// If you see this error you must look in 
		// commandTable.cpp -> ArgumentNode noQueryArgs
		return COMMAND_ERROR;
	}
	if (e) {
		return COMMAND_ERROR;
	} else {
		response += stringout;
	}
	return COMMAND_SUCCESS;
}
//int execRST(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values)
//int execSTB(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values)
//int execTST(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values)

