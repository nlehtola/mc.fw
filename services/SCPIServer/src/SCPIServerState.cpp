#include "SCPIServerState.hpp"

#include <iostream>
#include <fstream>
#include <unistd.h>
using namespace std;

SCPIServerState::SCPIServerState()
{
	TLSXConnectionSockfd = -1;	// This gets opened the first time it is used by sendToTCP in TLSXServerTCP
	ifstream cfgFile;
	cfgFile.open(CHASSIS_CONFIG_FILE);

	companyName = "";
	chassisModelNumber = "";
	chassisSerialNumber = "";
	SWVersion = "";
	HWVersion = "";
	isBenchtop = false;
	maxNumBlades = 9;
	maxNumLasers = 4;
	
	if (!cfgFile.is_open()) {
		cfgFile.open(BACKUP_CONFIG_FILE);
		if (!cfgFile.is_open()) {
			spLogError("Could not open a chassis config file for reading");
			return;
		}
	}
	while(cfgFile.good()) {
		string header;
		string data;

		getline (cfgFile, header);
		getline (cfgFile, data);
		
		if (header=="companyName") {
			companyName = data;
		} else if (header=="chassisModelNumber") {
			chassisModelNumber = data;
		} else if (header=="chassisSerialNumber") {
			chassisSerialNumber = data;
		} else if (header=="SWVersion") {
			SWVersion = data;
		} else if (header=="HWVersion") {
			HWVersion = data;
		} else if (header=="maxNumBlades") {
			maxNumBlades = atoi(data.c_str());
		} else if (header=="maxNumLasers") {
			maxNumLasers = atoi(data.c_str());
		} else if (header=="isBenchtop") {
			if (data=="true") {
				isBenchtop = true;
			}
		}
	}
	cfgFile.close();
}

SCPIServerState::~SCPIServerState()
{
	if(TLSXConnectionSockfd >= 0)
	{
		close(TLSXConnectionSockfd);
	}
}


void SCPIServerState::recordError(int e)
{
	//TODO, do something with status byte
}

string SCPIServerState::getCompanyNameString()
{
	return companyName;
}
string SCPIServerState::getInstrumentmodelNumber()
{
	return chassisModelNumber;
}
string SCPIServerState::getInstrumentSerialNumber()
{
	return chassisSerialNumber;
}
string SCPIServerState::getHWVersionNumber()
{
	return HWVersion;
}
string SCPIServerState::getSWVersionNumber()
{
	return string(TLSX_VERSION);
}
bool SCPIServerState::getIsBenchtop()
{
	return isBenchtop;
}
		
unsigned int SCPIServerState::getMaxNumberOfBlades()
{
	return maxNumBlades;
}
	
unsigned int SCPIServerState::getMaxNumberOfLasers()
{
	return maxNumLasers;
}

