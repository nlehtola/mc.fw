#include "Units.hpp"
#include <algorithm>

struct UnitTableEntry {
	Unit unit;
	double factor;
	UnitType type;
	string name;		//upper case
};
const UnitTableEntry unitsTable[] = {
	{UNIT_C,1,UNIT_TEMP,"C"},
	{UNIT_CC,1e-2,UNIT_TEMP,""},	// centi celcius is 1/100 of a degee celcius (this is what the blade returns)
	//frequency
	{UNIT_HZ,1,UNIT_FREQ,"Hz"},
	{UNIT_KHZ,1e3,UNIT_FREQ,"kHz"},
	{UNIT_MHZ,1e6,UNIT_FREQ,"MHz"},
	{UNIT_GHZ,1e9,UNIT_FREQ,"GHz"},
	{UNIT_THZ,1e12,UNIT_FREQ,"THz"},
	//wavelength
	{UNIT_PM,1,UNIT_WAVE,"m"},
	{UNIT_PM,1e-3,UNIT_WAVE,"mm"},
	{UNIT_PM,1e-6,UNIT_WAVE,"um"},
	{UNIT_PM,1e-9,UNIT_WAVE,"pm"},
	{UNIT_NM,1e-12,UNIT_WAVE,"nm"},
	//power
	{UNIT_DBM,1,UNIT_DBMPOWER,"dBm"},
	{UNIT_MDBM,1e-3,UNIT_DBMPOWER,"mdBm"},
	{UNIT_W,1,UNIT_POWER,"W"},
	{UNIT_MW,1e-3,UNIT_POWER,"mW"},
	
	{UNIT_NONE,1,UNIT_TYPE_NONE,""}
};

/*
const Unit validFreq[] = {UNIT_HZ, UNIT_KHZ, UNIT_MHZ, UNIT_GHZ, UNIT_THZ, UNIT_NONE};
const Unit validPower[] = {UNIT_DBM, UNIT_MDBM, UNIT_W, UNIT_MW, UNIT_NONE};
const Unit validWave[] = {UNIT_M, UNIT_MM, UNIT_UM, UNIT_NM, UNIT_PM, UNIT_NONE};
const Unit validTemp[] = {UNIT_C, UNIT_NONE};

struct UnitTypeTableEntry {
	UnitType type;
	const Unit* units;
};

const (Unit*)[] unitFamilies = {
	{UNIT_FREQ,validFreq},
	{UNIT_POWER,validPower},
	{UNIT_DBMPOWER,validPower},
	{UNIT_WAVE,validWave},
	{UNIT_TEMP,validTemp},
	{UNIT_TYPE_NONE,NULL}
};*/

// converts wavelength to frequency
double mToMHzRoundToHundredmHz(double wavelength)
{
	return round((SPEED_OF_LIGHT)/wavelength/((double)100e6))*100;
}

// converts frequency to wavelength
double MHzTom(double frequency)
{
	return (SPEED_OF_LIGHT)/(frequency*((double)1e6));
}

// converts between dBm and 
inline double dBmtoW(double power)
{
	return pow(10, power/20.0)/1000.0;
}
inline double WtodBm(double power)
{
	return 20*log10(power*1000.0);
}


// ############################### Static functions ###############################
const UnitTableEntry* findUnit(Unit unit) {
	int i=0; 
	for (; unitsTable[i].unit!=UNIT_NONE; ++i) {
		if (unitsTable[i].unit==unit) {
			break;
		}
	}
	return &(unitsTable[i]);
}

string convertToUpper(string in)
{
	std::transform(in.begin(), in.end(), in.begin(), ::toupper);
	return in;
}

// is that unit string valid for that type
bool isSameType(string name, Unit unit)
{
	UnitType type = findUnit(unit)->type;
	for (int i=0; unitsTable[i].unit!=UNIT_NONE; ++i) {
		if (convertToUpper(unitsTable[i].name)==convertToUpper(name)) {
			if (unitsTable[i].type==type) {
				return true;
			}
			return false;
		}
	}
	return false;
}

Unit unitFromString(string name)
{
	int i=0;
	for (; unitsTable[i].unit!=UNIT_NONE; ++i) {
		if (convertToUpper(unitsTable[i].name)==convertToUpper(name)) {
			break;
		}
	}
	return unitsTable[i].unit;
}

// for printing the name
string getUnitName(Unit unit)
{
	const UnitTableEntry* unitPtr = findUnit(unit);
	return unitPtr->name; // this will be "" if we can't find the unit
}

// convert input from units from into units to
// returns the converted value
double doUnitConvertion(double value, Unit from, Unit to)
{
	if (from==to) {
		return value;
	}
	
	double output = nan("");
	
	const UnitTableEntry* inUnit = findUnit(from);
	const UnitTableEntry* outUnit = findUnit(to);
	spLogDebug(3, "doUnitConvertion converting value: %f, "
				"from units: %s into units: %s", value, 
				inUnit->name.c_str(), outUnit->name.c_str());
	
	if (inUnit->type==outUnit->type) {
		// This is the easy case where there is an easy conversion
		// the rest require more complicated conversions
		output = (value*inUnit->factor)/outUnit->factor;
		spLogDebug(4, "doUnitConvertion converting %f to %f", value, output);
	} else if ((inUnit->type==UNIT_DBMPOWER)&&(outUnit->type==UNIT_POWER)) {
		output = dBmtoW((value*inUnit->factor))/outUnit->factor;
		spLogDebug(4, "doUnitConvertion converting dBm to W");
	} else if ((inUnit->type==UNIT_POWER)&&(outUnit->type==UNIT_DBMPOWER)) {
		output = WtodBm((value*inUnit->factor))/outUnit->factor;
		spLogDebug(4, "doUnitConvertion converting W to dBm");
	} else {
		spLogError("Error, doUnitConvertion converting between invalid units: \"%s\" to \"%s\"", inUnit->name.c_str(), outUnit->name.c_str());
	}
	spLogDebug(4, "doUnitConvertion outputing %f, in units: %d", output, to);
	return output;
}

/*
// ############################### UnitFamily ###############################
UnitFamily::UnitFamily(const Unit* validUnits, Unit defaultUnit)
{
	this->validUnits = validUnits;
	this->defaultUnit = defaultUnit;
}
// set the default unit for this bucket
UnitResponse UnitFamily::setDefault(Unit unit)
{
	// search through the validUnits and check if unit is valid
	for(int i=0; validUnits[i]!=UNIT_NONE; ++i) {
		if (unit==validUnits[i]) {
			defaultUnit = unit;
			return UNIT_SUCCESS;
		}
	}
	return UNIT_ERROR;
}
// get the current default unit for this bucket
Unit UnitFamily::getDefault()
{
	return defaultUnit;
}
// get the default unit for this bucket as a string
string UnitFamily::getDefaultString()
{
	int i=0;
	for (; unitsTable[i].unit!=UNIT_NONE; ++i) {
		if (unitsTable[i].unit==defaultUnit) {
			break;
		}
	}
	return unitsTable[i].name;	// this will be "" if we can't find the unit
}
// is the unit represented by string provided valid for this bucket
// returns the Unit that it represents
Unit UnitFamily::isValid(string name)
{
	int i=0;
	for (; unitsTable[i].unit!=UNIT_NONE; ++i) {
		if (convertToUpper(unitsTable[i].name)==convertToUpper(name)) {
			break;
		}
	}
	for(int j=0; validUnits[j]!=UNIT_NONE; ++j) {
		if (unitsTable[i].unit==validUnits[j]) {
			return unitsTable[i].unit;
		}
	}
	return UNIT_NONE;
}

bool UnitFamily::isValid(Unit unit)
{
	int i=0;
	for (; unitsTable[i].unit!=UNIT_NONE; ++i) {
		if (unitsTable[i].unit==unit) {
			break;
		}
	}
	for(int j=0; validUnits[j]!=UNIT_NONE; ++j) {
		if (unitsTable[i].unit==validUnits[j]) {
			return true;
		}
	}
	return false;
}

double UnitFamily::convertToDefault(Unit from, double input)
{
	if (from==defaultUnit&&from!=UNIT_NONE) {
		return input; 
	}
	
	const UnitTableEntry* inUnit = findUnit(from);
	const UnitTableEntry* outUnit = findUnit(defaultUnit);
	if (inUnit==NULL||outUnit==NULL) {
		return nan("");
	}
	if (inUnit->type==outUnit->type) {
		// This is the easy case where there is an easy conversion
		// the rest require more complicated conversions
		return (input*inUnit->factor)/outUnit->factor;
	} else if ((inUnit->type==UNIT_DBMPOWER)&&(outUnit->type==UNIT_POWER)) {
		return dBmtoW(input*inUnit->factor)/outUnit->factor;
	} else if ((inUnit->type==UNIT_POWER)&&(outUnit->type==UNIT_DBMPOWER)) {
		return WtodBm(input*inUnit->factor)/outUnit->factor;
	}
	return nan("");
}*/

// ############################ UnitValue ############################

// create a new quantity with a value in the unit unit
UnitValue::UnitValue(double value, Unit from, Unit to)
{
	this->unit = to;
	const UnitTableEntry* inUnit = findUnit(from);
	const UnitTableEntry* outUnit = findUnit(to);
	spLogDebug(3, "UnitValue() on UnitValue with internal value: %f, "
				"from units: %s into units: %s", value, 
				inUnit->name.c_str(), outUnit->name.c_str());
	
	if (from==to) {
		this->value = value;
	} else if (inUnit->type==outUnit->type) {
		// This is the easy case where there is an easy conversion
		// the rest require more complicated conversions
		this->value = (value*inUnit->factor)/outUnit->factor;
		spLogDebug(4, "UnitValue() on UnitValue converting %f to %f", value, this->value);
	} else if ((inUnit->type==UNIT_DBMPOWER)&&(outUnit->type==UNIT_POWER)) {
		this->value = dBmtoW((value*inUnit->factor))/outUnit->factor;
		spLogDebug(4, "UnitValue() on UnitValue converting dBm to W");
	} else if ((inUnit->type==UNIT_POWER)&&(outUnit->type==UNIT_DBMPOWER)) {
		this->value = WtodBm((value*inUnit->factor))/outUnit->factor;
		spLogDebug(4, "UnitValue() on UnitValue converting W to dBm");
	} else {
		this->value = value;
		spLogError("Error, UnitValue::UnitValue() converting between invalid units: \"%s\" to \"%s\"", inUnit->name.c_str(), outUnit->name.c_str());
	}
}

// get the quantity in a certain unit
double UnitValue::get(Unit unit)
{
	double output = nan("");
	
	const UnitTableEntry* inUnit = findUnit(this->unit);
	const UnitTableEntry* outUnit = findUnit(unit);
	spLogDebug(3, "get on UnitValue with internal value: %f, "
				"from units: %s into units: %s", value, 
				inUnit->name.c_str(), outUnit->name.c_str());
	
	if (inUnit->type==outUnit->type) {
		// This is the easy case where there is an easy conversion
		// the rest require more complicated conversions
		output = (value*inUnit->factor)/outUnit->factor;
		spLogDebug(4, "get on UnitValue converting %f to %f", value, output);
	} else if ((inUnit->type==UNIT_DBMPOWER)&&(outUnit->type==UNIT_POWER)) {
		output = dBmtoW((value*inUnit->factor))/outUnit->factor;
		spLogDebug(4, "get on UnitValue converting dBm to W");
	} else if ((inUnit->type==UNIT_POWER)&&(outUnit->type==UNIT_DBMPOWER)) {
		output = WtodBm((value*inUnit->factor))/outUnit->factor;
		spLogDebug(4, "get on UnitValue converting W to dBm");
	} else {
		spLogError("Error, UnitValue::get converting between invalid units: \"%s\" to \"%s\"", inUnit->name.c_str(), outUnit->name.c_str());
	}
	spLogDebug(4, "get on UnitValue outputing %f, in units: %d", output, unit);
	return output;
}

// get the quantity in default units
double UnitValue::get()
{
	return value;
}

// get the default unit string
string UnitValue::getUnitString()
{
	return getUnitName(unit);
}

