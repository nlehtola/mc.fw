/**
 * @file tcpListener.cpp
 * @brief This is the central thread and object manger in the SCPI server.
 * The main thread runs from main() and is responsible for listening on the
 * a TCP port for connections from the VXI11 server. New threads are then
 * spawned to service each Link. A master thread is also spawned, and runs
 * master(). This monitors the excecution queues and excecutes pending tasks
 * @author Ian Paterson
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <signal.h>

#include "singleton.h"
#include "logging.h"

#include "tcpWorker.hpp"
#include "commandTable.hpp"

#include "ExcecutionQueue.hpp"

#include <pthread.h>

#define PORT 10010
#define SCPI_DAEMON_NAME "TLSX_SCPIServer"
#define SCPI_PID_FILE "/tmp/SCPIServer.pid"

#define MAX_OPEN_LINKS 32

// The main thread accepts TCP connections from client programs, then spawns threads for each TCP connection
// each TCP connecton represents a "link" as defined in VXI-11. Each link has a unique link ID
// error state, and read and write buffers. Information written to the write buffers of the link
// will be passed through a SCPI parser.

// A master thread will dispatch the requested task from a queue associated with each worker thread.
// A dispatched task may write data into the read stream of the link that dispatched the task.
// A single link may request a lock on the entire device. This may be rejected by the server if a lock
// is already held. A request may be made to wait for the lock to be released or until a timeout occurs

// There are 3 types of errors in this daemon, non fatal, link fatal and daemon fatal
// most errors should be link fatal, this indicates that the link in which the error occured should be torn down
// if possible this should be communicated to the VXI11 server by sending a SDCPP_CLOSE packet
// non fatal errors are recoverable, such as a parse error
// daemon fatal errors are generally hypothetical and should never happen in a production system
// they should be clearly indicate in comments why they would be expected to happen and why they could
// not be handled. daemon fatal errors are handled by sending SIGTERM to the process, this calls the signal 
// handler and frees all open sockets //TODO, standardised highly visible daemon exit syslog message

// This is a queue of workers created by the main thread to add to the master threads list of workers
typedef struct {
	queue<TCPWorker*> workers;
	pthread_mutex_t mutex;
} WorkerQueue;

/** @brief Creates a new socket and binds it to a port
 *
 *  @param port The port number to bind to
 *  @return On success: an open socket file descriptor ready for listening
 *  @return On Error: -1
 */
int createListenSocket(int port) {
	int listenSocket, on, error;
	struct sockaddr_in serv_addr;
	
	listenSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (listenSocket < 0) {
		spLogError("Error, call to socket(2) failed, could not create listen socket: %s", strerror(errno));
		return -1;	// unable to create new socket
	}
	
	on=1;
	error = setsockopt(listenSocket, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));

	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(port);
	if (bind(listenSocket, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		spLogError("Error, call to bind(2) failed, could not bind to listen socket: %s", strerror(errno));
		return -1;
	}
	if (listen(listenSocket, 10)<0) {
		spLogError("Error, call to listen(2) failed: %s", strerror(errno));
		return -1;
	}
	return listenSocket;
}

/** @brief Listens on a port until a client connects
 *  @param port socket to listen on, bind this socket to a port before calling this function
 *  @return On success: an open socket file descriptor ready for IO
 *  @return On Error: -1
 */
int waitAndAccept(int listenSocket)
{
	int sockfd;
	socklen_t clilen;
	struct sockaddr_in cli_addr;
	clilen = sizeof(cli_addr);
	sockfd = accept(listenSocket, (struct sockaddr*)&cli_addr, &clilen);
	if (sockfd<0) {
		spLogError("Error, call to accept(2) failed: %s", strerror(errno));
		return sockfd;
	}
	
	//TODO ensure that cli_addr corresponds to localhost
	/*char remoteHostName[10];
	getnameinfo(&cli_addr, sizeof(cli_addr), remoteHostName, sizeof(remoteHostName), NULL, 0, 0);
	if (strncmp(remoteHostName, "localhost", sizeof(remoteHostName))!=0) {	
		spLogWarning("Warning, blocked attempt from a remote host to connect "
					"(only connections from local VXI11Server and USBServer "
					"are allowable)");
		close(sockfd);
		return -1;
	}*/
	return sockfd;
}

/** @brief Signal handler to perform a clean shutdown of the program when the process is terminated
 *  @param signum The signal that was sent to the proccess
 *  @return Does not return
 */
void signal_callback_handler(int signum)
{
	// clean up any open tcp streams
	// TODO
	// we can't do pthread_mutex_lock(&socketListMutex);, because if this was
	// the thread that held the mutex then it will deadlock
	// if we are really worried then we can kill the other threads
	// aquire the mutex first, so they will hang or something...
	
	if (signum==SIGSEGV) {
		spLogError("Server terminated due to segfault");
		//	exit(128+signum);
	}
	
	//TODO:
	//iterate over and close socket
	for (set<int>::iterator i=socketList.begin(); i!=socketList.end(); ++i) {
		close(*i);
	}
	
	spLogError("SCPI Server terminated by signal number: %d", signum);
	
	exit(128+signum);
}

/**	@brief This is the master excecution thread. It is responsible for 
 *	excecuting tasks queued by all of the clients
 *	@param pass this a void* to a WorkerQueue*. This is used as in input 
 *	queue of new workers produced by main()
 *	@return Does not return
 */
void* master(void* pass)
{
	WorkerQueue* pushQueue = (WorkerQueue*)pass;
	
	// master list of TCPWorkers
	vector<TCPWorker*> workers;
	// MRU list of open linkid's
	// when a linkid is used it is touched in this list by removing it's entry
	// then readding it at the back of the list
	// thus the top of this list is always the least recently used
	vector<long> linkidMRU;
	
	SCPIServerState instrumentState; //TODO, this should be in main
	while (1) {
		// receive new links from the main thread
		spLogDebug(5, "master thread: acquiring mutex: pushQueue->mutex #1");
		pthread_mutex_lock(&pushQueue->mutex);
		spLogDebug(5, "master thread: mutex acquired: pushQueue->mutex #1");
		while(!pushQueue->workers.empty()) {
			long newlinkd = pushQueue->workers.front()->getLinkid();
			if (linkidMRU.size()>=MAX_OPEN_LINKS) {
				// remove the linkid of the TCPWorker from the MRU list
				long culledLinkid = linkidMRU.front();
				linkidMRU.erase(linkidMRU.begin());
						
				// iterate over each worker to find the worker with the corresponding linkid
				for (vector<TCPWorker*>::iterator w=workers.begin(); w!=workers.end(); ++w)
				{
					if ((*w)->getLinkid()==culledLinkid) {
						// terminate the thread associated with the TCPWorker
						pthread_cancel(*((*w)->getThread()));
						pthread_join(*((*w)->getThread()), NULL);
						
						// call the destructor of the TCPWorker to be removed
						delete(*w);
						// remove the TCPWorker from master list
						workers.erase(w);
						--w;
					}
				}
				
				spLogDebug(1, "While adding new link: %ld, number of open"
					"links exceeds %d, Culling stale link: %ld", newlinkd, 
					MAX_OPEN_LINKS, culledLinkid);
			}
			// add the newly added linkid to the back of the MRU
			linkidMRU.push_back(newlinkd);
			// add the new TCPWorker to the master list
			workers.push_back(pushQueue->workers.front());
			// remove the TCPWorker from the input queue
			pushQueue->workers.pop();
			usleep(5);
		}
		pthread_mutex_unlock(&pushQueue->mutex);
		spLogDebug(5, "master thread: mutex released: pushQueue->mutex #1");
	
		// iterate over each worker and try to execute a program message
		for (vector<TCPWorker*>::iterator w=workers.begin(); w!=workers.end(); ++w)
		{
			// get a handle of the linkid of this TCPWorker in the MRU list
			vector<long>::iterator i=linkidMRU.begin();
			for (; i!=linkidMRU.end(); ++i) {
				if ((*w)->getLinkid()==*i) {
					break;
				}
			}
		
			// trim finished links
			if (!(*w)->isInit()) {
				// remove the linkid of the TCPWorker from the MRU list
				linkidMRU.erase(i);
				// call the destructor of the TCPWorker
				delete(*w);
				// remove the TCPWorker from master list
				workers.erase(w);
				--w;
				continue;
			}
			
			// process one program message from the worker's queue
			int e = (*w)->getQueue()->excecuteNext(instrumentState);
			// if a command was found on the input queue then touch the entry in the MRU list for this linkid
			if (e!=EXECUTE_NOCOMMAND) {
				long linkid = *i;
				linkidMRU.erase(i);
				linkidMRU.push_back(linkid);
			}
		}
		usleep(100);
	}
	return NULL;
}

/**	@brief Entry point to the SCPI Server proccess
 *	@param argc The number of elements in argv
 *	@param argv An array of c strings which are the command line 
 *	arguments of the program
 *	@return Does not return
 */
int main(int argc, char** argv)
{
	int verbosity = 0;
	if ((argc>=3)&&(strcmp("-v", argv[1])==0)) {
		verbosity = atoi(argv[2]);
		if(verbosity<0) {
			verbosity = 0;
		}
	}
	spOpenLog(SCPI_DAEMON_NAME, verbosity);
		
	if (getSingletonLock(SCPI_PID_FILE)<0) {
		spLogNotice("Another instance of %s is already running, aborting", SCPI_DAEMON_NAME);
		fprintf(stdout, "Another instance of %s is already running, aborting\n", SCPI_DAEMON_NAME);
		exit(1);
	}

	// Register a signal handler to ensure safe shutdown of sockets
	signal(SIGINT, signal_callback_handler);
	signal(SIGTERM, signal_callback_handler);
	signal(SIGSEGV, signal_callback_handler);
	
	// if SIGINT is not ignored, then calling write on a broken socket will 
	// result in proccess termination due to unhandled signal
	signal(SIGPIPE, SIG_IGN);
	//should we possibly handle other signals? In case they happen

	// this is the global thread number/link number, this will roll over at some point, TODO figure out what happens then, is it safe or do we need a better solution
	long t = 0;
	
	// TODO add a retry mechanism here
	int listenSocket = createListenSocket(PORT);
	if (listenSocket<0) {
		spLogNotice("Could not bind to socket, retry server start after socket timeout has passed");
		fprintf(stdout, "Could not bind to socket, retry server start after socket timeout has passed\n");
		exit(1);
	}
	WorkerQueue workerQueue;
	if (pthread_mutex_init(&workerQueue.mutex, NULL) < 0) {
		spLogError("Error, initialisation of mutex workerQueue.mutex failed: %s\n", strerror(errno));
		exit(1);
	}
	
	pthread_t masterThread;
	if (pthread_create(&masterThread, NULL, master, (void*)&workerQueue)!=0) {
		spLogError("Error, could not create new thread: %s\n", strerror(errno));
		close(listenSocket);
		exit(1);
	}
	
	while (1) {
		int sockfd = waitAndAccept(listenSocket);
		if (sockfd<0) {
			// waitAndAccept records error message
			continue;
		}
		spLogDebug(5, "main thread: acquiring mutex: socketListMutex #1");
		pthread_mutex_lock(&socketListMutex);
		spLogDebug(5, "main thread: mutex acquired: socketListMutex #1");
		socketList.insert(sockfd);
		pthread_mutex_unlock(&socketListMutex);
		spLogDebug(5, "main thread: mutex released: socketListMutex #1");
		spLogDebug(2, "made new socket: %d", sockfd);

		TCPWorker* temp = new TCPWorker((long)t, sockfd);
		if (temp==NULL) {
			spLogError("Error, could not allocate new TCPWorker, out of memory?\n");
			continue;
		}
		if (!temp->isInit()) {
			spLogError("Error, could not create new TCP Worker, this is could be caused by too many active connections\n");
			delete(temp);
			continue;
		}

		if (pthread_create(temp->getThread(), NULL, spawnWorker, (void *)temp) !=0) {
			spLogError("Error, could not create new thread #2: %s\n", strerror(errno));
			delete(temp);
			continue;
		}
		// pass the new worker to the master thread
		spLogDebug(5, "main thread: acquiring mutex: workerQueue.mutex #1");
		pthread_mutex_lock(&workerQueue.mutex);
		spLogDebug(5, "main thread: mutex acquired: workerQueue.mutex #1");
		workerQueue.workers.push(temp);
		pthread_mutex_unlock(&workerQueue.mutex);
		spLogDebug(5, "main thread: mutex released: workerQueue.mutex #1");
		++t;
	}
	
	close(listenSocket);
	
#ifdef SINGLETON
	removeSingletonLock();
#endif
	return 0;
}
