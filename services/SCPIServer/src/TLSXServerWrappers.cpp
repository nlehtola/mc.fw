//returns the temperature of the laser in degrees C
// returns negative numbers on error
// 0.0 is invalid

#include "TLSXServerWrappers.hpp"
#include <stdio.h>
#include <math.h>
#include <sstream>
using namespace std;

// ############################## Real Backend ################################
// These functions send commands to the TLSX Server backend

// takes a double and sends a value in device units for the command
void setDoubleValue(bool& e, uint8_t blade, uint8_t laser, double value, S2TCommand command, SCPIServerState& instrumentState)
{
	spLogDebug(1, "Sending double numeric value to TLSXServer with value: %f, uint32_t: %d, command: %d", value, (uint32_t)value, command);
	#ifndef TLSXSERVER_BACKEND_DISABLED
	int r;
	if((r=sendToTLSXServer(NULL, blade, laser, command, value, instrumentState.TLSXConnectionSockfd))!=TLSXSERVER_SUCCESS) {
		e = true;
	}
	#endif
}

// gives back a double value in the device units for that command
double getDoubleValue(bool& e, uint8_t blade, uint8_t laser, S2TCommand command, SCPIServerState& instrumentState)
{
	int32_t temp;
	#ifndef TLSXSERVER_BACKEND_DISABLED
	int r;
	if ((r=sendToTLSXServer((uint32_t*)&temp, blade, laser, command, 0, instrumentState.TLSXConnectionSockfd))!=TLSXSERVER_SUCCESS) {
		e = true;
		return 0;
	}
	#else
	switch (command) {
	// power units are in mdBm
		case S2T_GET_MINPOWER:
			temp = 7;
			break;
		case S2T_GET_MAXPOWER:
			temp = 13500;
			break;
		case S2T_GET_DESPOWER:
			temp = 11500;
			break;
		case S2T_GET_ACTPOWER:
			temp = 11500;
			// this command is for testing the sendToTLSXServer while in backend disabled
			int r;
			if ((r=sendToTLSXServer((uint32_t*)&temp, blade, laser, command, 0, instrumentState.TLSXConnectionSockfd))!=TLSXSERVER_SUCCESS) {
				e = true;
				return 0;
			}
			break;
	
	// freq units are in MHz
		case S2T_GET_MINFREQ:
			temp = 191500000;
			break;
		case S2T_GET_MAXFREQ:
			temp = 196250000;
			break;
		case S2T_GET_DESFREQ:
			temp = 195840000;
			break;

	// grid units are in MHz	
		case S2T_GET_MINGRID:
			temp = 100;
			break;
		case S2T_GET_MAXGRID:
			temp = 25000;
			break;
		case S2T_GET_ACTGRID:
			temp = 20000;
			break;
	
	// fine units are in MHz
		case S2T_GET_MINFINE:
			temp = -6000;
			break;
		case S2T_GET_MAXFINE:
			temp = 6000;
			break;
		case S2T_GET_ACTFINE:
			temp = 20;
			break;
			
	// temp units are in degrees C
		case S2T_GET_TEMP:
			temp = 230;
			break;
			
	// SBS Rate units are kHz
		case S2T_GET_MINSBSRATE:
			temp = 10;
			break;
		case S2T_GET_MAXSBSRATE:
			temp = 200;
			break;
		case S2T_GET_ACTSBSRATE:
			temp = 150;
			break;
			
	// SBS freq untis are in MHz
		case S2T_GET_MINSBSFREQ:
			temp = 1000;
			break;
		case S2T_GET_MAXSBSFREQ:
			temp = 20000;
			break;
		case S2T_GET_ACTSBSFREQ:
			temp = 15000;
			break;
			
		default:
			temp = -1.0;
	}
	#endif
	return (double)temp;
}

// Get/Set state
void setBoolState(bool& e, uint8_t blade, uint8_t laser, bool on, S2TCommand command, SCPIServerState& instrumentState)
{
	int r;
	int32_t temp;
	if (on) {
		spLogDebug(1, "Sending bool state value \"ON\" to TLSXServer, command: %d", command);
		temp = STATE_ON_IN;
	} else {
		spLogDebug(1, "Sending bool state value \"OFF\" to TLSXServer, command: %d", command);
		temp = STATE_OFF_IN;
	}
	#ifndef TLSXSERVER_BACKEND_DISABLED
	if ((r = sendToTLSXServer(NULL, blade, laser, command, temp, instrumentState.TLSXConnectionSockfd))!=TLSXSERVER_SUCCESS) {
		e = true;
	}
	#endif
}
bool getBoolState(bool& e, uint8_t blade, uint8_t laser, S2TCommand command, SCPIServerState& instrumentState)
{
	#ifndef TLSXSERVER_BACKEND_DISABLED
	uint32_t temp;
	int r;
	r = sendToTLSXServer(&temp, blade, laser, command, 0, instrumentState.TLSXConnectionSockfd);
	if (r==TLSXSERVER_SUCCESS) {
		if (temp==STATE_ON_OUT) {
			return true;
		} else if (temp==STATE_OFF_OUT) {
			return false;
		} else {
			spLogError("Invalid Response for getBoolState from TLSX Server: %X", temp);
			e = true;
		}
	}
	e = true;
	return false;
	#else
	return true;
	#endif
}

void laserReset(bool& e, uint8_t blade, uint8_t laser, SCPIServerState& instrumentState)	//testing only, resets a laser
{
	spLogDebug(1, "Sending reset laser command to TLSXServer");
	#ifndef TLSXSERVER_BACKEND_DISABLED
	int r;
	if ((r=sendToTLSXServer(NULL, blade, laser, S2T_RESET, 0, instrumentState.TLSXConnectionSockfd))!=TLSXSERVER_SUCCESS) {
		e = true;
	}
	#endif
}

uint32_t getuint32(bool& e, uint8_t blade, S2TCommand command, SCPIServerState& instrumentState)
{
	spLogDebug(1, "Getting uint32 from TLSXServer");
	#ifndef TLSXSERVER_BACKEND_DISABLED
	int r;
	uint32_t temp;
	if ((r=sendToTLSXServer(&temp, blade, 0, command, 0, instrumentState.TLSXConnectionSockfd))!=TLSXSERVER_SUCCESS) {
		e = true;
	}
	return temp;
	#else
	return 0;
	#endif
}

string get4ByteString(bool& e, uint8_t blade, S2TCommand command, SCPIServerState& instrumentState)
{
	spLogDebug(1, "Quering 4 ByteString from TLSXServer");
	#ifndef TLSXSERVER_BACKEND_DISABLED
	uint32_t temp;
	int r;
	if ((r=sendToTLSXServer(&temp, blade, 0, command, 0, instrumentState.TLSXConnectionSockfd))!=TLSXSERVER_SUCCESS) {
		e = true;
		return "****";
	}
	string stingout;
	for (int i=0; i<4; ++i) {
		char c = ((char*)(&temp))[i];
		if (c=='\0') {
			break;
		}
		if (c>127) {
			return "XXXX";
		}
		stingout += c;
	}
	return stingout;
	#else
	return "????";
	#endif
}

string get32ByteString(bool& e, uint8_t blade, S2TCommand command, SCPIServerState& instrumentState)
{
	spLogDebug(1, "Quering 4 ByteString from TLSXServer");
	#ifndef TLSXSERVER_BACKEND_DISABLED
	uint32_t temp;
	int r;
	string stingout;
	for (int j=0; j<8; ++j) {
		if ((r=sendToTLSXServer(&temp, blade, j, command, 0, instrumentState.TLSXConnectionSockfd))!=TLSXSERVER_SUCCESS) {
			return "****";
		}
		for (int i=0; i<4; ++i) {
			char c = ((char*)(&temp))[i];
			if (c=='\0') {
				j = 8;
				break;
			}
			if (c>127) {
				return "XXXX";
			}
			stingout += c;
		}
	}
	return stingout;
	#else
	return "????";
	#endif
}

// GPIB:
string getIDN(bool& e, SCPIServerState& instrumentState)
{
	string stringout;
	stringout += instrumentState.getCompanyNameString() + ",";
	stringout += instrumentState.getInstrumentmodelNumber() + ",";
	stringout += instrumentState.getInstrumentSerialNumber() + ",HW";
	stringout += instrumentState.getHWVersionNumber() + "FW";
	stringout += instrumentState.getSWVersionNumber();
	return stringout;
}
string getIDNSlot(bool& e, uint8_t blade, SCPIServerState& instrumentState)
{
	stringstream ss;
	string companyName = instrumentState.getCompanyNameString();
	string modelNumber = get32ByteString(e, blade, S2T_GET_MODEL_NUMBER, instrumentState);
	string serialNumber = get32ByteString(e, blade, S2T_GET_SERIAL_NUMBER, instrumentState);
	string hwVersion = get4ByteString(e, blade, S2T_GET_HW_VERSION, instrumentState);
	uint32_t fwMajor = getuint32(e, blade, S2T_GET_FW_MAJOR_VERSION, instrumentState);
	uint32_t fwMinor = getuint32(e, blade, S2T_GET_FW_MINOR_VERSION, instrumentState);
	ss << companyName << "," << modelNumber << "," << serialNumber << 
					",HW" << hwVersion << "FW" << fwMajor << "." << fwMinor;
	return ss.str();
}
string getOPT(bool& e, SCPIServerState& instrumentState)
{
	#ifndef TLSXSERVER_BACKEND_DISABLED
	uint32_t temp;
	int r;
	stringstream ss;
	if ((r=sendToTLSXServer(&temp, 0, 0, S2T_GET_OPT_BLADES, 0, instrumentState.TLSXConnectionSockfd))!=TLSXSERVER_SUCCESS)
	{
		e = true;
		return "";
	}
	
	if(instrumentState.getIsBenchtop())
	{
		if (1&(temp>>15))
		{
			ss << get32ByteString(e, 15, S2T_GET_MODEL_NUMBER, instrumentState);
		}
	}
	else
	{
		for (int i=1;; ++i) 
		{
			if (1&(temp>>i)) {
				ss << get32ByteString(e, i, S2T_GET_MODEL_NUMBER, instrumentState);
			}

			if(i>=instrumentState.getMaxNumberOfBlades())
			{
					break;
			}
			ss << ",";
		}
	}
	return ss.str();
	#else
	return ",,,,,,CSB0001,,";
	#endif
}
string getOPTSlot(bool& e, uint8_t blade, SCPIServerState& instrumentState)
{
	#ifndef TLSXSERVER_BACKEND_DISABLED
	uint32_t temp;
	int r;
	stringstream ss;
	if ((r=sendToTLSXServer(&temp, blade, 0, S2T_GET_OPT_LASERS, 0, instrumentState.TLSXConnectionSockfd))!=TLSXSERVER_SUCCESS) {
		e = true;
		return "";
	}
	for (int i=1;; ++i) {
		if (1&(temp>>i)) {
			ss << "1";
		}
		if(i>=instrumentState.getMaxNumberOfLasers()) {
			break;
		}
		ss << ",";
	}
	return ss.str();
	#else
	return "LAS01,LAS01,LAS01,LAS01";
	#endif
}
string getOPC(bool& e, SCPIServerState& instrumentState)
{
	#ifndef TLSXSERVER_BACKEND_DISABLED
	uint32_t temp;
	int r;
	if ((r=sendToTLSXServer(&temp, 0, 0, S2T_GET_OPC_BLADES, 0, instrumentState.TLSXConnectionSockfd))!=TLSXSERVER_SUCCESS) {
		e = true;
		return "";
	}

	stringstream ss;
	ss << temp;
	return ss.str();
	#else
	return "";
	#endif
}
string getOPCSlot(bool& e, uint8_t blade, SCPIServerState& instrumentState)
{
	#ifndef TLSXSERVER_BACKEND_DISABLED
	uint32_t temp;
	int r;
	if ((r=sendToTLSXServer(&temp, blade, 0, S2T_GET_OPC_LASERS, 0, instrumentState.TLSXConnectionSockfd))!=TLSXSERVER_SUCCESS) {
		e = true;
		return "";
	}

	stringstream ss;
	ss << temp;
	return ss.str();
	#else
	return "";
	#endif
}
string getOPCLaser(bool& e, uint8_t blade, uint8_t laser, SCPIServerState& instrumentState)
{
	#ifndef TLSXSERVER_BACKEND_DISABLED
	uint32_t temp;
	int r;
	if ((r=sendToTLSXServer(&temp, blade, laser, S2T_GET_OPC_LASER, 0, instrumentState.TLSXConnectionSockfd))!=TLSXSERVER_SUCCESS) {
		e = true;
		return "";
	}

	stringstream ss;
	ss << temp;
	return ss.str();
	#else
	return "";
	#endif
}

