#include "TLSXServerTCP.hpp"

#ifndef TLSXSERVER_BACKEND_DISABLED
extern "C" {
#include "b2s.h"
}

#define TLSX_TCP_ERROR 140
#define TLSX_PORT 10001
#define TLS_SERVER_FRAME_SIZE 16

int connectToRemoteSocket(const char* host, int port)
{
	int sockfd;
	struct sockaddr_in serv_addr;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
		spLogError("Error, unable to create new socket: %s", strerror(errno));
		return -2;
	}
	bzero((char*) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = inet_addr(host);
	serv_addr.sin_port = htons(port);
	
	if (connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
	 	spLogError("Error, Could not connect to %s on port %d: %s", host, port, strerror(errno));
		return -1;
	}
	if (sockfd<0) {
		spLogError("Error, new socket is invalid");
	}
	return sockfd;
}

//inData and outData are 16 bytes long each
int sendToTCP(uint8_t* outData, uint8_t* inData, int& sockfd)
{
	int i;
	if (sockfd==-1) {
		if ((sockfd=connectToRemoteSocket("127.0.0.1", TLSX_PORT))<0) {
			return -1;
		}
	}
	
	//printf("Sending to TLSX Server:\n");
	//for(i=0;i<16;i++)
	//	printf("%u ", inData[i]);
	//printf("\n");

	if(send(sockfd, inData, TLS_SERVER_FRAME_SIZE, 0) < 0) {
		spLogError("Error, Message transmission to TLSX Server failed");
		close(sockfd);
		sockfd = -1;
		return -1;
	}
	spLogDebug(3, "To TLSX: Message sent");
	
	if(recv(sockfd, outData, TLS_SERVER_FRAME_SIZE, 0) < 0)
	{
		spLogError("Error, Recieving message from TLSX Server failed");
		close(sockfd);
		sockfd = -1;
		return -1;
	}

	//printf("Received from TLSX Server:\n");
	//for(i=0;i<16;i++)
	//	printf("%u ", outData[i]);
	//printf("\n");

	spLogDebug(3, "To TLSX: Message recieved");

	return 0;
}

TLSXServerReturn sendToTLSXServer(uint32_t* dataOut, unsigned char blade, unsigned char laser, uint8_t command, int32_t dataIn, int& sockfd)
{
	TLSXSendData input;
	TLSXReturnData output;
	
	input.blade_id = (uint8_t)blade;
	input.laser_address = (uint8_t)laser;
	input.sequence = 0;
	input.command = command;	//TEMP
	int32_to_uint8a(&dataIn, input.data, 0); //endianess is important here
	*(uint32_t*)input.reserved = 0;		// zero reserved data
	*(uint32_t*)(input.reserved+4) = 0;	// zero reserved data

	stringstream ss;
	ss << "TLSX send data: ";
	for (unsigned int i=0; i<16; ++i) {
		ss << (int)(((unsigned char*)&input)[i]) << " ";
	}
	spLogDebug(4, ss.str().c_str());
	
	//######## send to TCP:
	if (sendToTCP((uint8_t*)&output, (uint8_t*)&input, sockfd) < 0) {
		return TLSXSERVER_TCP_ERROR;
	}

	//######## finished sending to TCP
	
	stringstream ss2;
	ss2 << "TLSX return data: ";
	for (unsigned int i=0; i<16; ++i) {
		ss2 << (int)(((unsigned char*)&output)[i]) << " ";
	}
	spLogDebug(4, ss2.str().c_str());

	if (dataOut!=NULL) {
		uint8a_to_uint32(output.data, dataOut, 0); //endianess is important here
	}
	return (TLSXServerReturn)output.status;
}

#else
TLSXServerReturn sendToTLSXServer(uint32_t* dataOut, unsigned char blade, unsigned char laser, uint8_t command, int32_t dataIn, int& sockfd)
{
	TLSXReturnData output;
	if (dataOut!=NULL) {
		int32_t a = -50;
		uint32_t dataIn = (int32_t)a;
		uint32_to_uint8a(&dataIn, output.data, 0);
		
		stringstream ss;
		ss << "TLSX return dataOut: ";
		for (unsigned int i=0; i<4; ++i) {
			ss << (int)(((unsigned char*)&output.data)[i]) << " ";
		}
		spLogDebug(1, ss.str().c_str());
			
		uint8a_to_uint32(output.data, dataOut, 0);
	}
	return (TLSXServerReturn)128;
}
#endif
