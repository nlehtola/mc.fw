#include "commandTable.hpp"
#include "Units.hpp"

// checks if a SCPI mnemonic matches its long or short form, then returns the
// trailing digit if it is present. Returns -1 if there is no match, returns 0
// if there is no number, otherwise returns the trailing digit. A trailing 0 is
// illegal and will return -1
// longForm and shortForm are compared to the mnemonic after any trailing digits
// are removed, they must be upper case
int SCPIMnemonicDecode(string& mnemonic, string longForm, string shortForm)
{
	int number;
	string name;
	
	// look for numbers
	unsigned found = mnemonic.find_last_not_of("0123456789");
	if (found==string::npos) {
		// the mnemonic was all number
		return -1;
	}
	if (found+1==mnemonic.length()) {
		// there is no trailing number
		name = mnemonic;
		number = 0;
	} else {
		// there is a trailing number
		name = mnemonic.substr(0, found+1);
		number = atoi(mnemonic.substr(found+1).c_str());
		if (number==0) {
			// the trailing number is a 0
			return -1;
		}
	}
	
	//compare the name part of the header to the candidate and see if it matches
	name = convertToUpper(name);
	if (name==longForm) {
		return number;
	}
	if (name==shortForm) {
		return number;
	}
	return -1;
}

// ############################### new ###############################

// ############################## ArgumentNodes ##############################

// allowable tasks are MIN, MAX, SET, ACTUAL, ALL
const ArgumentNode minMaxActSetableArgs[] = {
{NODE_QUERY,	TASK_NONE,	1, {{false, MIN, "MIN"}}		},
{NODE_QUERY,	TASK_NONE,	1, {{false, MAX, "MAX"}}		},
{NODE_CMD,		TASK_SET,	1, {{false, TASK_NONE, ""}} 	},
{NODE_QUERY,	TASK_NONE,	1, {{true, ACTUAL, "ACT"}}		},	// default task
{NODE_QUERY,	TASK_NONE,	1, {{false, ALL, "ALL"}}		},
{NODE_QUERY,	TASK_NONE,	0, {} } };

// allowable tasks are MIN, MAX, SET, DESIRED, ACTUAL, ALL
const ArgumentNode minMaxDesiredActSetableArgs[] = {
{NODE_QUERY,	TASK_NONE,	1, {{false, MIN, "MIN"}}		},
{NODE_QUERY,	TASK_NONE,	1, {{false, MAX, "MAX"}}		},
{NODE_CMD,		TASK_SET,	1, {{false, TASK_NONE, ""}} 	},
{NODE_QUERY,	TASK_NONE,	1, {{false, DESIRED, "SET"}}	},
{NODE_QUERY,	TASK_NONE,	1, {{true, ACTUAL, "ACT"}}		},	// default task
{NODE_QUERY,	TASK_NONE,	1, {{false, ALL, "ALL"}}		},
{NODE_QUERY,	TASK_NONE,	0, {} } };

// allowable tasks are MIN, MAX, SET, DESIRED, LOCK, ALL
const ArgumentNode minMaxDesiredLockSetableArgs[] = {
{NODE_QUERY,	TASK_NONE,	1, {{false, MIN, "MIN"}}		},
{NODE_QUERY,	TASK_NONE,	1, {{false, MAX, "MAX"}}		},
{NODE_CMD,		TASK_SET,	1, {{false, TASK_NONE, ""}} 	},
{NODE_QUERY,	TASK_NONE,	1, {{true, DESIRED, "SET"}}		},	// default task
{NODE_QUERY,	TASK_NONE,	1, {{false, TASK_LOCK, "LOCK"}}	},	// returns TRUE/FALSE
{NODE_QUERY,	TASK_NONE,	1, {{false, ALL, "ALL"}}		},
{NODE_QUERY,	TASK_NONE,	0, {} } };

// allowable tasks are MIN, MAX, ACTUAL, ALL
const ArgumentNode minMaxActArgs[] = {
{NODE_QUERY,	TASK_NONE,	1, {{false, MIN, "MIN"}}		},
{NODE_QUERY,	TASK_NONE,	1, {{false, MAX, "MAX"}}		},
{NODE_QUERY,	TASK_NONE,	1, {{true, ACTUAL, "ACT"}}		},	// default task
{NODE_QUERY,	TASK_NONE,	1, {{false, ALL, "ALL"}}		},
{NODE_QUERY,	TASK_NONE,	0, {} } };

const ArgumentNode stateArgs[] = {
{NODE_CMD,		TASK_NONE,	1, {{false, STATE_ON, "ON"}}	},
{NODE_CMD,		TASK_NONE,	1, {{false, STATE_OFF, "OFF"}}	},
{NODE_QUERY,	TASK_QUERY,	0, {} },
{NODE_QUERY,	TASK_NONE,	0, {} } };

// aliases:
const ArgumentNode* powerArgs = minMaxDesiredActSetableArgs;
const ArgumentNode* wavelengthArgs = minMaxDesiredLockSetableArgs;
const ArgumentNode* frequencyArgs = minMaxDesiredLockSetableArgs;
const ArgumentNode* fineArgs = minMaxActSetableArgs;
const ArgumentNode* gridArgs = minMaxActSetableArgs;
const ArgumentNode* ditherArgs = minMaxActSetableArgs;

// no arguments just command:
const ArgumentNode noCommandArgs[] = {
{NODE_CMD,		TASK_COMMAND,	0, {}	},
{NODE_QUERY,	TASK_NONE,		0, {} 	} };

// no arguments just query:
const ArgumentNode noQueryArgs[] = {
{NODE_QUERY,	TASK_QUERY,	0, {}	},
{NODE_QUERY,	TASK_NONE,	0, {}	} };

// something that has both command and query forms with no arguments
const ArgumentNode noCMDQueryArgs[] = {
{NODE_CMD,		TASK_COMMAND,	0, {}	},
{NODE_QUERY,	TASK_QUERY,		0, {}	},
{NODE_QUERY,	TASK_NONE,		0, {}	} };

const ArgumentNode dateArgs[] = {
{NODE_CMD,		TASK_COMMAND,	3, {{false, TASK_NONE, ""},{false, TASK_NONE, ""},{false, TASK_NONE, ""}}	},
{NODE_QUERY,	TASK_QUERY,		0, {}	},
{NODE_QUERY,	TASK_NONE,		0, {}	} };

const ArgumentNode* timeArgs = dateArgs;

// ############################### HeaderNodes ###############################

// level 5

const HeaderNode sbsChilds[] = {
{ID_SBSDITHER_STATE,	NODE_PLAIN, "STATE", "STATE", NULL, stateArgs, UNIT_NONE},
{ID_SBSDITHER_RATE, NODE_PLAIN, "RATE", "RATE", NULL, minMaxActArgs, UNIT_HZ},
{ID_SBSDITHER_FREQ, NODE_PLAIN, "FREQ", "FREQUENCY", NULL, ditherArgs, UNIT_HZ},
{ID_NONE,	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

const HeaderNode controlledDitherChilds[] = {
//{ID_CONTROLLEDDITHER_STATE,	NODE_PLAIN, "STATE", "STATE", NULL, stateArgs, UNIT_NONE},
{ID_NONE,	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

// level 4 
const HeaderNode frequencyChilds[] = {
{ID_FINE,	NODE_PLAIN, "FINE", "FINE", NULL, fineArgs, UNIT_HZ},
{ID_NONE,	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

// level 3
const HeaderNode operationChilds[] = {
//{ID_OP_EVENT,	NODE_PLAIN, "EVEN", "EVENT", NULL, noQueryArgs, UNIT_NONE},
//{ID_OP_COND,		NODE_PLAIN, "COND", "CONDITION", NULL, noQueryArgs, UNIT_NONE},
//{ID_OP_ENABLE,	NODE_PLAIN, "ENAB", "ENABLE", NULL, noCMDQueryArgs, UNIT_NONE},
{ID_NONE,	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

const HeaderNode questionableChilds[] = {
//{ID_QUE_EVENT,	NODE_PLAIN, "EVEN", "EVENT", NULL, noQueryArgs, UNIT_NONE},
//{ID_QUE_COND,		NODE_PLAIN, "COND", "CONDITION", NULL, noQueryArgs, UNIT_NONE},
//{ID_QUE_ENABLE,	NODE_PLAIN, "ENAB", "ENABLE", NULL, noCMDQueryArgs, UNIT_NONE},
{ID_NONE,	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

HeaderNode outputPowerChilds[] = {
{ID_POWERUNIT,	NODE_PLAIN, "UNIT", "UNIT", NULL, noQueryArgs, UNIT_NONE},
{ID_NONE,	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

HeaderNode outputChannelChilds[] = {
{ID_NONE,	NODE_PLAIN, "POW", "POWER", outputPowerChilds, NULL, UNIT_NONE},
{ID_STATE,	NODE_PLAIN, "STATE", "STATE", NULL, stateArgs, UNIT_NONE},
{ID_SAFETY,	NODE_PLAIN, "SAFETY", "SAFETY", NULL, stateArgs, UNIT_NONE},
{ID_NONE,	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

const HeaderNode sourceChannelChilds[] = {
{ID_RESET,	NODE_PLAIN, "RESET", "RESET", NULL, noCommandArgs, UNIT_NONE},	//TODO: testing only, resets a laser
{ID_POWER,	NODE_PLAIN, "POW", "POWER", NULL, powerArgs, UNIT_DBM},
{ID_WAVE,	NODE_PLAIN, "WAV", "WAVELENGTH", NULL, wavelengthArgs, UNIT_M},
{ID_FREQ,	NODE_PLAIN, "FREQ", "FREQUENCY", frequencyChilds, frequencyArgs, UNIT_HZ},
{ID_GRID,	NODE_PLAIN, "GRID", "GRID", NULL, gridArgs, UNIT_HZ},
{ID_SBSDITHER_STATE, 			NODE_PLAIN, "SBS", "SBS", sbsChilds, stateArgs, UNIT_NONE},
{ID_CONTROLLEDDITHER_STATE, 	NODE_PLAIN, "DITH", "DITHER", controlledDitherChilds, stateArgs, UNIT_NONE},
{ID_TEMP,	NODE_PLAIN, "TEMP", "TEMPERATURE", NULL, noQueryArgs, UNIT_C},
{ID_NONE,	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

// level 2
const HeaderNode systemChilds[] = {
//{ID_ERROR,	NODE_PLAIN, "ERR", "ERROR", NULL, noQueryArgs, UNIT_NONE},
{ID_VERS,	NODE_PLAIN, "VERS", "VERSION", NULL, noQueryArgs, UNIT_NONE},
/*{ID_DATE,	NODE_PLAIN, "DATE", "DATE", NULL, dateArgs, UNIT_NONE},
{ID_TIME,	NODE_PLAIN, "TIME", "TIME", NULL, timeArgs, UNIT_NONE},*/
{ID_NONE,	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

const HeaderNode slotChilds[] = {
//{ID_OPC,	NODE_PLAIN, "OPC", "OPC", NULL, noCMDQueryArgs, UNIT_NONE},
{ID_OPC,	NODE_PLAIN, "OPC", "OPC", NULL, noQueryArgs, UNIT_NONE},
{ID_OPT,	NODE_PLAIN, "OPT", "OPTIONS", NULL, noQueryArgs, UNIT_NONE},
//{ID_TST,	NODE_PLAIN, "TST", "TST", NULL, noQueryArgs, UNIT_NONE},
{ID_IDN,	NODE_PLAIN, "IDN", "IDN", NULL, noQueryArgs, UNIT_NONE},
{ID_NONE,	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

const HeaderNode statusChilds[] = {
//{ID_PRES,	NODE_PLAIN, "PRES", "PRESET", NULL, noCommandArgs, UNIT_NONE},
{ID_NONE,	NODE_PLAIN, "OPER", "OPERATION", operationChilds, NULL, UNIT_NONE},
{ID_NONE,	NODE_PLAIN, "QUES", "QUESTIONABLE", questionableChilds, NULL, UNIT_NONE},
{ID_NONE,	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

const HeaderNode outputChilds[] = {
{ID_NONE,	NODE_DIGIT, "CHAN", "CHANNEL", outputChannelChilds, NULL, UNIT_NONE},
{ID_NONE,	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

const HeaderNode sourceChilds[] = {
{ID_NONE,	NODE_DIGIT, "CHAN", "CHANNEL", sourceChannelChilds, NULL, UNIT_NONE},
{ID_NONE, 	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

// level 1
const HeaderNode root[] = {
// GPIB:
{ID_CLS,	NODE_PLAIN, "*CLS", "*CLS", NULL, noCommandArgs, UNIT_NONE},
//{ID_ESE,	NODE_PLAIN, "*ESE", "*ESE", NULL, esrArgs, UNIT_NONE},
//{ID_ESR,	NODE_PLAIN, "*ESR", "*ESR", NULL, noQueryArgs, UNIT_NONE},
{ID_IDN,	NODE_PLAIN, "*IDN", "*IDN", NULL, noQueryArgs, UNIT_NONE},
//{ID_OPC,	NODE_PLAIN, "*OPC", "*OPC", NULL, opc args, UNIT_NONE},
{ID_OPC,	NODE_PLAIN, "*OPC", "*OPC", NULL, noQueryArgs, UNIT_NONE},
{ID_OPT,	NODE_PLAIN, "*OPT", "*OPT", NULL, noQueryArgs, UNIT_NONE},
//{ID_RST,	NODE_PLAIN, "*RST", "*RST", NULL, noCommandArgs, UNIT_NONE},
//{ID_STB,	NODE_PLAIN, "*STB", "*STB", NULL, noQueryArgs, UNIT_NONE},
//{ID_SRE,	NODE_PLAIN, "*SRE", "*SRE", NULL, noQueryArgs, UNIT_NONE},
//{ID_TST,	NODE_PLAIN, "*TST", "*TST", NULL, noQueryArgs, UNIT_NONE},
// SCPI:
{ID_NONE,	NODE_PLAIN, "SYST", "SYSTEM", systemChilds, NULL, UNIT_NONE},
{ID_NONE,	NODE_DIGIT, "SLOT", "SLOT", slotChilds, NULL, UNIT_NONE},
{ID_NONE,	NODE_DIGIT, "STAT", "STATUS", statusChilds, NULL, UNIT_NONE},
{ID_NONE,	NODE_DIGIT, "OUTP", "OUTPUT", outputChilds, NULL, UNIT_NONE}, 
{ID_NONE,	NODE_DIGIT, "SOUR", "SOURCE", sourceChilds, NULL, UNIT_NONE},
{ID_NONE,	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

// matches dataList, which is the user input, against a list of candidate 
// arangments for arguments provided by args (hard coded constants)
// defaultUnit is the default Units for the header
DecodedMessage* decodeArgs(Unit* defaultUnit, bool query, vector<ProgramDataValue*>& dataList, DecodedMessageID messageid, const ArgumentNode* args, uint8_t n, uint8_t m)
{
	spLogDebug(1, "decodeArgs: DecodedMessageID: %d query?: %d", messageid, query);
	
	for (int i=0;; ++i) {
		// this is the marker for the end of the table
		if ((args[i].argsLength==0)&&(args[i].defaultTask==TASK_NONE)) {
			spLogDebug(4, "parse error: argument arangment did not match any in table");
			return NULL;
		}
		
		// if the candidate's query-ness isn't the same as the input, 
		// try the next candidate
		if (args[i].query != query) {
			spLogDebug(4, "parse error: arguments candidate failed, query-ness differs");
			continue;
		}
		
		// try each of the arguments in turn, 
		// if all match then combine them to make the command
		vector<UnitValue*> values;
		TaskID task = args[i].defaultTask;
		unsigned int j=0, k=0;
		for (; (j<args[i].argsLength); ++j) {
			if (k<dataList.size()) {
				if ((args[i].args[j].defaultTask==TASK_NONE) && dataList[k]->isDecimal()) {
					// this is a decimal number
					string unitString;
					double value = dataList[k]->retrieveDecimal(unitString);
					Unit from;
					if (unitString=="") {
						from = *defaultUnit;
					} else if (isSameType(unitString, *defaultUnit)) {
						from = unitFromString(unitString);
					} else {
						spLogDebug(4, "parse error: arguments candidate failed, invalid units");
						break;
					}
					values.push_back(new UnitValue(value, from, *defaultUnit));
					++k;
					continue;
				} else if (dataList[k]->retrieveCharacter()!=NULL&& convertToUpper(*(dataList[k]->retrieveCharacter()))==args[i].args[j].taskString) {		
					// this is a task string
					task = args[i].args[j].defaultTask;
					++k;
					continue;
				}
			}
			if (!args[i].args[j].optional) {
				spLogDebug(4, "parse error: arguments candidate failed, non optional argument ommitted or of incorrect type");
				break;	// try the next candidate (k will be wrong, so this won't match)
			}
			// optional argument
			if (args[i].args[j].defaultTask!=TASK_NONE) {
				task = args[i].args[j].defaultTask;
				continue;
			} else {
				spLogDebug(4, "parse error: arguments candidate failed, non optional argument ommitted or of incorrect type");
				spLogWarning("Warning, an entry in arguments table has optional argument with TASK_NONE");
				break; // This shouldn't really happen, it means the table is wrong
			}
		}
		if ((j!=args[i].argsLength)||(k!=dataList.size())) {
			// this candidate did not match
			spLogDebug(4, "parse error: arguments candidate failed, incorrect number of arguments");
			continue;
		}
		if (task==TASK_NONE) {
			spLogDebug(4, "parse error: arguments candidate failed, task not set");
			// this canidate did not match
			continue;
		}
		// this candidate matches
		return new DecodedMessage(*defaultUnit, messageid, task, n, m, values);
	}

	return NULL;
}

DecodedMessage* decodeHeader(Unit* powerUnit, bool query, vector<string>& headerPath, vector<ProgramDataValue*>& dataList)
{
	// this prevents matches when nothing is provided
	if (headerPath.size()<1) {
		return NULL;	
	}
	
	unsigned int depth=0;
	int digit[5] = {0};
	const HeaderNode* node = root;
	while(depth<5) {		// this is while 1, but with a sanity check. loop should break before depth = 5
		spLogDebug(2, "header path depth: %d out of %d", 
					depth, headerPath.size());
		for (int i=0;; ++i) {
			// this is the marker for the end of the table
			if (node[i].shortForm==NULL) {
				spLogDebug(4, "parse error: header did not match any in table");
				return NULL;
			}
			// for commands allowing trailing digits explicit 0 is invalid,
			// but no digits decodes to 0
			digit[depth] = SCPIMnemonicDecode(headerPath[depth], 
									node[i].shortForm, node[i].longForm);
			// if the header doesn't match then move to the next candidate 
			if (digit[depth]<0) {
				continue; // move to node[i+1]
			}
			if ((node[i].digit==NODE_PLAIN) && (digit[depth]!=0)) {
				spLogDebug(4, "parse error: plain node has digit supplied");
				return NULL;
			}
			if ((node[i].digit==NODE_DIGIT) && (digit[depth]==0)) {
				// ######### change made for FLS-2800  which has only 1 blade in slot 15 #########
				// if no blade number is suplied, and depth = 0 (slot, source, output commands)
				// then set blade number to 15
				if (depth==0) {	// TODO &&instrument.isBenchtop()
					digit[0] = 15;
				// ###############################################################################	
				} else {
					spLogDebug(4, "parse error: required trailing digit ommited");
					return NULL;
				}
			}

			if (headerPath.size()==depth+1) {
				if (node[i].messageid!=ID_NONE) {
					if (node[i].args==NULL) {
						// not a terminal node, but no more header to parse
						spLogDebug(4, "parse error: not a terminal node");
						return NULL;
					}
					else {
						//terminal node
						if (node[i].defaultUnit==UNIT_DBM) {
							return decodeArgs(powerUnit, query, dataList, node[i].messageid, node[i].args, digit[0], digit[1]);
						} else {
							Unit unit = node[i].defaultUnit;
							return decodeArgs(&unit, query, dataList, node[i].messageid, node[i].args, digit[0], digit[1]);
						}
					}
				}
				return NULL;
			} else {
				if (node[i].children==NULL) {
					if (node[i].args==NULL) {
						spLogWarning("Warning, Internal error, node[i].args and "
									"node[i].children are both NULL, command is "
									"not implimented: %s", node[i].longForm);
					}
					return NULL;
				}
				node = node[i].children;
				++depth;
				break;	// This is a matching non terminal node, increase depth
			}
		}
	}
	return NULL;
}
