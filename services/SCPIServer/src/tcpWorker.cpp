/**
 * @file tcpWorker.cpp
 * @brief This is the central thread and object manger in the SCPI server.
 * The main thread runs from main() and is responsible for listening on the
 * a TCP port for connections from the VXI11 server. New threads are then
 * spawned to service each Link. A master thread is also spawned, and runs
 * master(). This monitors the excecution queues and excecutes pending tasks
 * @author Ian Paterson
 */
 
#include "tcpWorker.hpp"

// includes for select(2)
//#include <sys/types.h>
//#include <sys/stat.h>
//#include <fcntl.h>
#include <sys/select.h>
	
// This is a list of open sockets, so that the signal handler can close them
// any worker can close it's own socket, the signal handler can close them
// when a worker closes a socket it removes it from this list
// the signal handler only reads from this list
// the opener of sockets adds to this list
// These are global static variables
pthread_mutex_t socketListMutex = PTHREAD_MUTEX_INITIALIZER;
set<int> socketList;

// This is a function that is called like read, but reports errors and blocks 
// until all expected data is recived or timeout occurs (TODO)
int wrappedRead(int &fd, void *buf, size_t count, long &linkid) {
	char* bufPtr = (char*)buf;
	unsigned int left = count;
	if (count>TCP_MAX_BUFF_SIZE) {
		spLogError("Error, linkid: %ld, data being read exceeds maximum "
					"allowed buffer size", linkid);
		return CONNECTION_PACKET_ERROR;
	}
	int done = 0;
	while(!done) {
		int e = read(fd, bufPtr, left);
		if (e<0) {
			spLogError("Error, linkid: %ld, could not read from socket: %s",
						linkid, strerror(errno));
			return CONNECTION_SOCKET_ERROR;
		} else if (e==0) {
			spLogError("Error, linkid: %ld, could not read from socket, remote host disconnected: %s",
						linkid, strerror(errno));
			return CONNECTION_SOCKET_ERROR;
		} else if (e>TCP_MAX_BUFF_SIZE) {
			// read should stop this from happening
			// seeing this error is a really bad sign
			spLogError("Error, linkid: %ld, read() returned more data than "
						"asked for: %s", linkid, strerror(errno));
			return CONNECTION_PACKET_ERROR;
		}
		bufPtr+=e;
		left-=e;
		if (left==0) {
			break;
		}
	}
	return count-left;	//should always just be count, should never be negative
}

// worker is a void pointer to TCPWorker
void *spawnWorker(void *worker)
{
	((TCPWorker*)worker)->run();
	pthread_exit(NULL);
}


TCPWorker::TCPWorker(long linkid, int sockfd)
{
	init = false;
	
	this->linkid = linkid;
	this->sockfd = sockfd;
	
	excecutionQueue = new ExcecutionQueue(linkid);
	
	parser = new SCPIParser(excecutionQueue, linkid);
	init = true;
}
TCPWorker::~TCPWorker()
{
	delete(excecutionQueue);
	delete(parser);
	close(sockfd);
	pthread_mutex_lock(&socketListMutex);
	socketList.erase(sockfd);
	pthread_mutex_unlock(&socketListMutex);
}

pthread_t* TCPWorker::getThread()
{
	return &thread;
}

ExcecutionQueue* TCPWorker::getQueue()
{
	//this should be threadsafe without a mutex, hopefully
	return excecutionQueue;
}

long TCPWorker::getLinkid()
{
	return linkid;
}

bool TCPWorker::isInit()
{
	//this should be threadsafe without a mutex, hopefully
	return init;
}


int TCPWorker::sdcppRead(SCPIDaemonCommandPacket *packet, 
		SCPIDaemonCommandPacket *response)
{
	typedef struct {
		unsigned int maxLength; 	// max number of bytes to return in response packet
		char termchar;				// if not -1, then terminate if this character is read
	} ReadData;
	int c;
	
	if (packet->purpose!=SDCPP_READ) {
		return CONNECTION_PACKET_ERROR;
	}
	if (packet->length!=sizeof(ReadData)) {
		return CONNECTION_PACKET_ERROR;
	}
	
	ReadData* readData = (ReadData*)packet->data;
	unsigned int maxlength = readData->maxLength;
	char termchar = readData->termchar;

	// length is the max number of bytes we will return, make sure that
	// it does not exceed our own maximum
	if (maxlength>TCP_MAX_BUFF_SIZE) {
		maxlength = TCP_MAX_BUFF_SIZE;
	}

	//read at most maxlength bytes from the stream, if there are not that 
	//many bytes on the stream then return as many as possible
	// we allocate the maximum number that may be needed, even though
	// in the average case they will not be
	response->data = new char[maxlength];

	spLogDebug(1, "starting read...");
	// read 1 byte at a time from the device buffer
	unsigned int transfered = 0;
	// if no other termination conditions are met then this will occur
	response->purpose = SDCPP_IO_TIMEOUT;
	string responseData;
	while ((responseData=excecutionQueue->getNextResponseMessage())!="") {
		spLogDebug(2, "reading: %s", responseData.c_str());
		//add to output buffer
		if (responseData.size()<(maxlength-transfered)) {
			strncpy(&(response->data[transfered]), responseData.c_str(), responseData.size());
			transfered += responseData.size();
		} else {
			strncpy(&(response->data[transfered]), responseData.c_str(), maxlength-transfered);
			transfered = maxlength;
			// TODO, store the remainder of the response in a buffer to send on the next read
		}

		//TODO, do not ignore termchar (this means searching through the string for termchar)
		// this would set response->purpose = SDCPP_READ;
	}
	if (transfered!=0) {
		response->purpose = SDCPP_READ_END;
	}

	// TODO, find out how the VXI11Server responds to this empty packet
	if (transfered==0) {
		response->length = 0;
		delete[](response->data);
		response->data = NULL;
	}
	response->length = transfered;
	return CONNECTION_SUCCESS;
	
}
	
// response.purpose = SDCPP_NONE if there is to be no response
// response.purpose should be the same as packet.purpose
// returns 0 on success
// returns -1 on error
// response->data needs to be freed by caller if not NULL
int TCPWorker::decodeDaemonPacket(SCPIDaemonCommandPacket *packet, 
									SCPIDaemonCommandPacket *response)
{
	bool endchar = false;
	if (response!=NULL) {
		response->purpose = packet->purpose;
		response->length = 0;
		response->data = NULL;
	} else {
		spLogDebug(1, "NULL response sent to decodeDaemonPacket");
		return CONNECTION_MISC_ERROR;
	}
	
	switch (packet->purpose) {
		case SDCPP_NONE:
			// this is the implicit initial packet, sends the linkid
			response->purpose = SDCPP_CONNECT;
			response->length = sizeof(long);
			response->data = new char[sizeof(long)];
			*((long *)response->data) = linkid;
			break;
		case SDCPP_WRITE_END:
			endchar = true;
			response->purpose = SDCPP_WRITE;
		case SDCPP_WRITE:
			// sanity check packet
			if ((packet->length==0)||(packet->data==NULL)) {
				return CONNECTION_PACKET_ERROR;
			}
			
			spLogDebug(3, "pushing to parser, ^END = %d", endchar);
			int e;
			if ((e = parser->push(packet->data, packet->length, endchar))!=PARSER_SUCCESS) {
				// if there is a failure, this means we wrote no data, 
				// TODO, on parse error find out what character parse errored
				if (e==PARSER_PARSE_ERROR) {
					spLogDebug(2, "Write: Parse error");
					response->purpose = SDCPP_PARSE_ERROR;	// we return a special ack for parse errors
				} else {
					response->length = 0;
					response->data = NULL;
					spLogError("Error, linkid: %ld, could not push data to parser, restarting parser, buffer has been cleared", linkid);
					break;
				}
			}
			
			// write response, data is the number of bytes 
			// written to parser (all of them)
			response->length = sizeof(unsigned int);
			response->data = (char*)new unsigned int(packet->length);
			break;
		case SDCPP_READ:
			return sdcppRead(packet, response);
		case SDCPP_STATUS:
			// TODO: get the instrument status byte
			// response->purpose = SDCPP_STATUS_RESPONSE
			break;
		case SDCPP_PING:
			// doing nothing will echo the packet purpose
			break;
		case SDCPP_CLOSE:
			//this should be take care of by the wrapper function
			// it is an error if it is passed here
		default:
			return CONNECTION_MISC_ERROR;
	}
	return CONNECTION_SUCCESS;	
}

void TCPWorker::run()
{
	if (init==false) {
		spLogError("Error, linkid: %ld, TCPWorker instances "
					"can only be run once", linkid);
		return;
	}
	
	int first = 1;
	char data[TCP_BUFF_SIZE];	// TODO, make dynamic
	SCPIDaemonCommandPacket packet;
	SCPIDaemonCommandPacket response;
	packet.data = data;
	int n;
	
	while (1) {
		if (first) {
			first = 0;
			// implicit first packet
			packet.purpose = SDCPP_NONE;
			packet.length = 0;
			spLogDebug(1, "Connection from VXI11Server established linkid: %ld, sockfd: %d", linkid, sockfd);
		} else {
			// for all other packets
			// read in the purpose and length fields in the 
			// SCPIDaemonCommandPacket packet
			n = wrappedRead(sockfd, &packet, 
							sizeof(SCPIDaemonCommandPacket)-sizeof(char *),
							linkid);
			if (n<0) {
				// this is a fatal error for the stream
				// wrappedRead reports errors
				spLogError("Error, linkid: %ld, TCP stream terminated",
							linkid);
				break;
			}
		}
		spLogDebug(3, "SCPI to VXI11 packet.purpose: %d", packet.purpose);
		if (packet.length>TCP_BUFF_SIZE) {
			spLogError("Error, linkid: %ld, nominal data packet length "
						"exceeds buffer size, ignoring packet, aborting "
						"connection", linkid);
			break;
		}
		
		if (packet.length>0) {
			// Read in the contents of the data field into
			// memory and link with packet.packet
			n = wrappedRead(sockfd, data, packet.length, linkid);
			if (n < 0) {
				// this is a fatal error for the stream
				// wrappedRead reports errors
				spLogError("Error, linkid: %ld, TCP stream terminated #2",
							linkid);
				break;
			}
		}
		
		// end the session if it is requested
		if (packet.purpose == SDCPP_CLOSE) {
			spLogDebug(1, "Connection from VXI11Server performing graceful shutdown");
			break;
		}
		if ((n=decodeDaemonPacket(&packet, &response))<0) {
			if (n==CONNECTION_PACKET_ERROR) {
				spLogError("Error, linkid: %ld, malformed packet received "
							"from client, aborting connection", linkid);
				spLogDebug(1, "malformed packet received from client, decodeDaemonPacket returned: %d, linkid: %ld, ", n, linkid);
			} else {
				spLogError("Error, linkid: %ld, internal error in SCPI server, aborting connection", linkid);
			}
			break;
		}
		
		spLogDebug(3, "SCPI to VXI11 response.purpose: %d", response.purpose);
		
		if (response.purpose!=SDCPP_NONE) {
			n = write(sockfd, &response,
						sizeof(SCPIDaemonCommandPacket)-sizeof(char *));
			if (n < 0) {
				spLogError("Error, linkid: %ld, could not write to socket, "
						"TCP stream terminated: %s", linkid, strerror(errno));
				break;
			}
			n = write(sockfd, response.data, response.length);
			if (n < 0) {
				spLogError("Error, linkid: %ld, could not write to socket, "
					"TCP stream terminated #2: %s", linkid, strerror(errno));
				break;
			}
		}
		
		if (response.data!=NULL) {
			delete[](response.data);
			response.data = NULL;
		}
	}
	
	spLogDebug(2, "Connection from VXI11Server shutting down: linkid: %ld, sockfd: %d", linkid, sockfd);
	
	if (response.data!=NULL) {
		delete[](response.data);
		response.data = NULL;
	}
	close(sockfd);
	pthread_mutex_lock(&socketListMutex);
	socketList.erase(sockfd);
	pthread_mutex_unlock(&socketListMutex);
	
	// the master thread might call our destructor as soon as we set this
	init = false;	
}

