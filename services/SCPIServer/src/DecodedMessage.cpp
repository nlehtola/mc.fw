#include "DecodedMessage.hpp"
#include "excecuteCommands.hpp"

typedef struct {
	DecodedMessageID id;
	int (*funcPtr)(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);
} TableEntry;

const static TableEntry dispatchTable[] = {
// Dither
	{ID_SBSDITHER_STATE, execSBSDitherState},
	{ID_SBSDITHER_RATE, execSBSDitherRate},
	{ID_SBSDITHER_FREQ, execSBSDitherFreq},
	{ID_CONTROLLEDDITHER_STATE, execControlledDitherState},
// Source:Frequency
	{ID_FINE, execFreqFine},
// Operation
	{ID_OP_EVENT, NULL},
	{ID_OP_COND, NULL},
	{ID_OP_ENABLE, NULL},
// Questionable
	{ID_QUE_EVENT, NULL},
	{ID_QUE_COND, NULL},
	{ID_QUE_ENABLE, NULL},
// Output:Power
	{ID_POWERUNIT, execLaserPowerUnit},
// Output
	{ID_STATE, execLaserState},
	{ID_SAFETY, execLaserSafety},
// Source
	{ID_RESET, execReset},	//testing only, resets a laser
	{ID_POWER, execPower},
	{ID_WAVE, execWavelength},
	{ID_FREQ, execFrequency},
	{ID_GRID, execGrid},
	{ID_TEMP, execTemp},
// System
	{ID_ERROR, NULL},	//	execError},
	{ID_VERS, execVersion},
	{ID_DATE, NULL},
	{ID_TIME, NULL},
// Status
	{ID_PRES, NULL},
// GPIB:
	{ID_CLS, NULL},	//	execCLS},
	{ID_ESE, NULL},	//	execESE},
	{ID_ESR, NULL},	//	execESR},
	{ID_IDN, execIDN},
	{ID_OPC, execOPC},
	{ID_OPT, execOPT},
	{ID_RST, NULL},	//	execRST},
	{ID_STB, NULL},	//	execSTB},
	{ID_TST, NULL},	//	execTST},
	{ID_NONE, NULL}
};

/*DecodedMessage::DecodedMessage(Unit defaultUnit, DecodedMessageID commandid, TaskID taskid, uint8_t bladeid, uint8_t laserid, double value)
{
	this->id = commandid;
	this->task = taskid;
	this->blade = bladeid;
	this->laser = laserid;
	this->values.push_back(value);
}*/

DecodedMessage::DecodedMessage(Unit defaultUnit, DecodedMessageID commandid, TaskID taskid, uint8_t bladeid, uint8_t laserid, vector<UnitValue*> values)
{
	this->id = commandid;
	this->task = taskid;
	this->blade = bladeid;
	this->laser = laserid;
	this->values = values;
	this->defaultUnit = defaultUnit;
}

DecodedMessage::~DecodedMessage()
{	
	for (vector<UnitValue*>::iterator i=values.begin(); i!=values.end(); ++i)
	{
		delete(*i);
	}
}

int DecodedMessage::execute(string& response, SCPIServerState& instrumentState)
{
	int (*funcPtr)(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values) = NULL;
	
	//lookup command
	for (int i=0; dispatchTable[i].id!=ID_NONE; ++i) {
		if (dispatchTable[i].id==id) {
			funcPtr = dispatchTable[i].funcPtr;
			break;
		}
	}
	if (funcPtr==NULL) {
		spLogError("DecodedMessage::execute: message id not found: commandID %d has not been implimented", id);
		return COMMAND_ERROR;
	}
	
	//run function
	return funcPtr(defaultUnit, instrumentState, response, task, blade, laser, values);
}

bool DecodedMessage::isCLS()
{
	if (id==ID_CLS) {
		return true;
	}
	return false;
}
