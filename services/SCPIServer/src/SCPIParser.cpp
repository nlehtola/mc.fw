#include "SCPIParser.hpp"


void SCPIParser::initPipes()
{
	if (inputfdWrite==-1) {
		int pipes[2];

		if (pipe(pipes)!=0) {
			// This means that we have too many streams open currently, maximum was 1024 when I tried it
			// this should indicate to the other daemon that we cannot accept new connections right now, 
			// which will in turn pass this on to the user
			// this is link fatal
			spLogError("Error, could not create internal pipe for parser: %s", strerror(errno));
			inputfdWrite = -1;
			inputfdRead = NULL;
			return;
		}
	
		inputfdWrite = pipes[1];
		inputfdRead = fdopen(pipes[0], "r");
		if (inputfdRead==NULL) {
			// not sure why this would happen, assumption is that it is temporary and only link fatal
			spLogError("Error, could not create internal pipe for parser #2: %s", strerror(errno));
			close(pipes[0]);
			close(pipes[1]);	// is this correct procedure for closing the failed pipe?
			inputfdWrite = -1;
			inputfdRead = NULL;
			return;
		}
	}
}

// if the scanner and bison instances have not been set up, set them up
void SCPIParser::initScanner()
{
	if (!scannerInit) {
		scannerInit = true;
		ps = yypstate_new();
		yylex_init(&scanner);
		yyset_in(inputfdRead, scanner);
	}
}

// from any state, clean all resources
void SCPIParser::cleanup()
{
	if (scannerInit) {
		scannerInit = false;
		// shut down the parser/scanner	
		yypstate_delete(ps);
		yylex_destroy(scanner);
	}
	
	if (inputfdWrite!=-1) {
		close(inputfdWrite);
		inputfdWrite = -1;
	}
	if (inputfdRead!=NULL) {
		fclose(inputfdRead);
		inputfdRead = NULL;
	}
}

SCPIParser::SCPIParser(ExcecutionQueue* excecutionQueue, long linkid)
{
	scannerInit = false;
	inputfdWrite = -1;
	inputfdRead = NULL;
	this->excecutionQueue = excecutionQueue;
	this->linkid = linkid;
	initPipes();
	initScanner();
	powerUnits = UNIT_DBM;
}

SCPIParser::~SCPIParser()
{
	cleanup();
}

// returns the parser to a working state ready for another command to be sent
void SCPIParser::reset()
{
	// clean up all old resources if there are any
	cleanup();
	
	// reinit all of the new resources
	initPipes();
	initScanner();
}

int SCPIParser::push(char* data, unsigned int length, bool end)
{
	initPipes();
	initScanner();

	stringstream ss;
	string message;
	for(unsigned int i=0; i<length; ++i) {
		ss << (int)(data[i]) << " ";
		message += data[i];
	}
	spLogDebug(1, "parsing: ascii: %s", message.c_str());
	spLogDebug(3, "parsing: raw: %s", ss.str().c_str());

	unsigned int written = 0;
	
	// variables for select(2) call, and timeouts
	fd_set fdset;
	struct timeval timeoutStruct;
	long timeout = 100;	// ms
	timeoutStruct.tv_sec = (long)((timeout*1000)/1000000);
	timeoutStruct.tv_usec = (long)((timeout*1000)%1000000);
	FD_ZERO(&fdset); // clear the set
	FD_SET(inputfdWrite, &fdset); // add our file descriptor to the set
	
	while(written<length) {
		int e;
		e = select(inputfdWrite+1, NULL, &fdset, NULL, &timeoutStruct);
		if (e<0) {
			spLogError("Error, linkid: %ld, inputfdWrite: select(2) errored: %s", linkid, strerror(errno));
			return PARSER_FATAL_ERROR;
		}
		if (e==0) {
			//IO timeout
			spLogDebug(1, "Error, linkid: %ld, SCPIParser push: write to flex scanner inputfdWrite: IO Timeout occured", linkid);
			return PARSER_FATAL_ERROR;	//TODO write timeout
		}
		if ((e = write(inputfdWrite, &(data[written]), length-written))<0) {
			spLogError("Error, linkid: %ld, SCPIParser, write to flex scanner inputfdWrite failed: %s", linkid, strerror(errno));
			return PARSER_FATAL_ERROR;
		}
		written += e;
	}

	if (end) {
		// send an EOF to the parser, this seems to be the only way to trigger
		// it to halt at the end of the input
		fsync(inputfdWrite);	//is fsync necessary?
		close(inputfdWrite);
		inputfdWrite = -1;
	
		int status;
		do {
			status = yypush_parse(ps, yylex(&type, scanner), &type, excecutionQueue, &powerUnits);
		} while ((status==YYPUSH_MORE));
		
		//reset the parser
		reset();
		
		if (status==1) {
			spLogDebug(1, "######################### Parse Error #########################");
			return PARSER_PARSE_ERROR;
		}
		spLogDebug(1, "Done parsing, bison status: %d", status);
	}
	
	return PARSER_SUCCESS;
}
