#ifndef TLSX_SERVER_TCP_HPP
#define TLSX_SERVER_TCP_HPP

// disable this flag for production
// if this flag is set hardware communication is disabled, use for testing only
//#define TLSXSERVER_BACKEND_DISABLED

#include <errno.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "logging.h"

extern "C" {
#include "conversions.h"
}

#include <sstream>
using namespace std;

#include <stdint.h>

enum S2TCommand {
	S2T_ECHO = 0,
	S2T_GET_MODEL = 1,
	S2T_GET_FW_VERSION = 2,
	S2T_GET_BLADE_STATUS = 3,
	
	// power units are in mdBm
	S2T_GET_MINPOWER = 4,	//getDoubleValue
	S2T_GET_MAXPOWER,		//getDoubleValue
	S2T_SET_POWER,			//setDoubleValue
	S2T_GET_DESPOWER,		//getDoubleValue
	S2T_GET_ACTPOWER,		//getDoubleValue
	
	// freq units are in MHz
	S2T_GET_MINFREQ = 9,	//getDoubleValue
	S2T_GET_MAXFREQ,		//getDoubleValue
	S2T_SET_FREQ,			//setDoubleValue
	S2T_GET_DESFREQ,		//getDoubleValue
	S2T_GET_LOCKFREQ = 13,	//getDoubleValue

	// grid units are in MHz	
	S2T_GET_MINGRID = 14,	//getDoubleValue
	S2T_GET_MAXGRID,		//getDoubleValue
	S2T_SET_GRID,			//setDoubleValue
	S2T_GET_ACTGRID,		//getDoubleValue
	
	// fine units are in MHz
	S2T_GET_MINFINE = 18,	//getDoubleValue
	S2T_GET_MAXFINE,		//getDoubleValue
	S2T_SET_FINE,			//setDoubleValue
	S2T_GET_ACTFINE,		//getDoubleValue
	
	// on/off
	S2T_SET_STATE_CONTROL_DITHER = 22,
	S2T_GET_STATE_CONTROL_DITHER,
	
	// temp units are in degrees CC
	S2T_GET_TEMP = 24,
	
	// SBS Rate units are kHz
	S2T_GET_MINSBSRATE = 25,	//getDoubleValue
	S2T_GET_MAXSBSRATE,		//getDoubleValue
	S2T_SET_SBSRATE,		//setDoubleValue
	S2T_GET_ACTSBSRATE,		//getDoubleValue
	
	// SBS freq untis are in MHz
	S2T_GET_MINSBSFREQ = 29,	//getDoubleValue
	S2T_GET_MAXSBSFREQ,		//getDoubleValue
	S2T_SET_SBSFREQ,		//setDoubleValue
	S2T_GET_ACTSBSFREQ,		//getDoubleValue
	
	// SBS ampl units are percent (unitless)
	S2T_GET_MINSBSAMP = 33,	//getDoubleValue
	S2T_GET_MAXSBSAMP,		//getDoubleValue
	S2T_SET_SBSAMP,			//setDoubleValue
	S2T_GET_ACTSBSAMP,		//getDoubleValue
	
	//on/off
	S2T_SET_STATE_LASER = 37,
	S2T_GET_STATE_LASER = 38,
	
	// data = 0
	S2T_RESET = 39,
	
	// number, see doc
	S2T_GET_LASERSTATE = 40,
	
	// on/off
	S2T_SET_STATE_SBS = 41,
	S2T_GET_STATE_SBS,

	S2T_GET_FLASH_VERSION = 45,		//get4ByteString
	S2T_SET_FLASH_VERSION = 46,
	S2T_GET_SERIAL_NUMBER = 47,		//get32ByteString
	S2T_SET_SERIAL_NUMBER = 48,
	S2T_GET_MODEL_NUMBER = 49,		//get32ByteString
	S2T_SET_MODEL_NUMBER = 50,
	S2T_GET_FW_MAJOR_VERSION = 51,	//get4ByteString
	S2T_GET_FW_MINOR_VERSION = 52,	//get4ByteString
	S2T_GET_HW_VERSION = 53,		//get4ByteString
	S2T_SET_HW_VERSION = 54,
	S2T_GET_NUM_LASERS = 55,		//getInteger
	S2T_SET_NUM_LASERS = 56,
	S2T_COMMIT_ST = 57,

	S2T_SET_SAFETY = 110,			//execLaserSafety
	S2T_GET_SAFETY = 111,

	S2T_GET_OPC_BLADES = 128,		//getOPC
	S2T_GET_OPC_LASERS = 129,		//getOPCSlot
	S2T_GET_OPC_LASER = 130,		//getOPCLaser
	S2T_GET_OPT_BLADES = 131,		//getOPT
	S2T_GET_OPT_LASERS = 132		//getOPTSlot
};

/*#define STATE_ON_IN		(0x01000000)
#define STATE_OFF_IN	(0x00010000)*/

#define STATE_ON_IN		(0x00000001)
#define STATE_OFF_IN	(0x00000100)
#define STATE_OFF_OUT	(0x00000100)
#define STATE_ON_OUT	(0x00000001)

/*#define STATE_ON_IN		(0x1)
#define STATE_OFF_IN	(0x0)
#define STATE_OFF_OUT	(0x0)
#define STATE_ON_OUT	(0x1)*/

#define MAXBLADES 16
#define MAXLASERS 4

typedef struct {
	uint8_t blade_id;
	uint8_t laser_address;
	uint8_t sequence;
	uint8_t command;
	uint8_t data[4];
	uint8_t reserved[8];	//64 bits reserved for later use
} TLSXSendData;

typedef struct {
	uint8_t blade_id;
	uint8_t laser_address;
	uint8_t sequence;
	uint8_t status;
	uint8_t data[4];
	uint8_t reserved[8];	//64 bits reserved for later use
} TLSXReturnData;

// error type for TLSXServer return values
enum TLSXServerReturn {
	TLSXSERVER_SUCCESS = 128,
	TLSXSERVER_ERROR = 127,
	TLSXSERVER_TCP_ERROR = 126
};

//TODO, errors
// returns TLSX_SUCCESS on success, otherwise error
TLSXServerReturn sendToTLSXServer(uint32_t* dataOut, unsigned char blade, unsigned char laser, uint8_t command, int32_t dataIn, int& sockfd);

#endif //TLSX_SERVER_TCP_HPP
