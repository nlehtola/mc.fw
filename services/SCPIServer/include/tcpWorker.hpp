/**
 * @file tcpWorker.hpp
 * @brief defines the TCPWorker class
 * @author Ian Paterson
 */

#ifndef TCP_WORKER_HPP
#define TCP_WORKER_HPP

// Data sent to or recieved from the TCP server will be in packets sent of 
// the form described by this struct:
// struct {
// 		int purpose;
//		unsigned int length;
//		char data[length];		// if length==0 then this will be missing
// };
// endianness isn't portable, but the server and client are running on the 
// same system, so it is ok to send things in host byte order

// for each packet sent to the server a response packet will be sent within 
// PACKET_TIMEOUT seconds, this should be near instant for all packets except 
// SDCPP_READ. The response packet will have the same purpose number as the
// packet, except on error, when a SDCPP_CLOSE will be sent. An initial 
// SDCPP_CONNECT packet will be sent at the begining of the connection to 
// indicate the linkid of the new connection. If the server sends a 
// SDCPP_CLOSE this indicates a connection breakdown, all link fatal errors
// should attempt to send this. If client sends SDCPP_CLOSE this indicates 
// clean shutdown.

// Purpose:
// the puspose number of the SCPIDaemonCommandPacket determines what action
// should be performed on the server
// The following packets can be sent to the server:
// -Close link
//		purpose = SDCPP_CLOSE
//		length = 0
// -Write Block
//		purpose = SDCPP_WRITE
//		length = length of data
//		data = raw data to send to device
// -Read Block		-causes server to return data
//		purpose = SDCPP_READ
//		length = sizeof(unsigned int)+sizeof(char)
//		data = struct {
//	unsigned int maxLength;  //max number of bytes to return in response packet
//	char termChar;  // if not -1, then terminate if this character is read
//	};
// -Query Device Status		-causes server to return the instrument status byte
//		purpose = SDCPP_STATUS
//		length = 0
// -Set END char			-sets the endchar termination condition on reads
//		purpose = SDCPP_END
//		length = sizeof(char)
//		data = (char) endchar
// -ping	-allows client to ping the server
//		purpose = SDCPP_PING
//		length = 0
//
//
// The following are responses from the server:
// -Connection established:		-implicit initial packet from server
// 		purpose = SDCPP_CONNECT
//		length = sizeof(long)
//		data = (long) linkid of connection
// -Close link		-this isn't a response, this is a server initiated packet to indicate connection failure
//		purpose = SDCPP_CLOSE
//		length = 0
// -Write response		-acknowledge write
//		purpose = SDCPP_WRITE
//		length = sizeof(unsigned int)
//		data = (unsigned int) number of bytes written
// -Read response
//		purpose = SDCPP_READ
//		length = length of data
//		data = raw data returned by device
// -Status response
//		purpose = SDCPP_STATUS
//		length = 1
//		data = (char) global status byte of device
// -Set END char response	-acknowledge set
//		purpose = SDCPP_END
//		length = 0
// -parse error, response from SDCPP_WRITE
//	 	purpose = SDCPP_PARSE_ERROR
//		length = 0
// -ping, response from SDCPP_PING
//		purpose = SDCPP_PING
//		length = 0

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

#include "singleton.h"
#include "logging.h"

#include "ExcecutionQueue.hpp"
#include "SCPIParser.hpp"

#include <pthread.h>

#include <set>
using namespace std;

#define PORT 10010

#define TCP_BUFF_SIZE 1024		//TODO remove
#define TCP_MAX_BUFF_SIZE 1024

#define CONNECTION_SUCCESS 0
#define CONNECTION_SOCKET_ERROR -1
#define CONNECTION_PACKET_ERROR -2
#define CONNECTION_MISC_ERROR -3

typedef enum {
	SDCPP_NONE			= 0,
	SDCPP_CLOSE			= 1,
	SDCPP_CONNECT	 	= 2,
	SDCPP_WRITE			= 3,
	SDCPP_READ			= 4,
	SDCPP_STATUS	 	= 5,
	SDCPP_END			= 6,
	SDCPP_PARSE_ERROR	= 7,
	SDCPP_IO_TIMEOUT	= 8,
	SDCPP_READ_END		= 9,
	SDCPP_WRITE_END		= 10,
	SDCPP_IO_ERROR		= 11,
	SDCPP_PING			= 12
} SCPIPurpose;

typedef struct {
	SCPIPurpose purpose;	// explained in the purpose section of the comments at the top of this file
	unsigned int length;	// the length of the data in *data, may be 0
	char *data;				// if length = 0 implies *data is NULL (ie, will segfault)
} SCPIDaemonCommandPacket;

//worker is a void pointer to TCPWorker
void *spawnWorker(void *worker);

// This is a list of open sockets, so that the signal handler can close them
// any worker can close it's own socket, the signal handler can close them
// when a worker closes a socket it removes it from this list
// the signal handler only reads from this list
// the opener of sockets adds to this list
// These are global static variables
extern pthread_mutex_t socketListMutex;
extern set<int> socketList;

/**
 * @brief Is responsible for providing the server interface of the SCPI 
 * server to the VXI11 server. Each instance of this class is reading from a
 * an open TCP stream which is connected to the VXI11 Server. Each instance
 * of this class represents a single client Link. Data is exchanged over the
 * TCP connection via variable sized packets. These take the form of the 
 * following struct:
 * struct {
 *		int purpose;
 * 		unsigned int length;
 * 		char data[length];		// if length==0 then this will be missing
 * };
 *
*/
class TCPWorker
{
private:
	int sockfd;
	long linkid;
	int sdcppRead(SCPIDaemonCommandPacket *packet, SCPIDaemonCommandPacket *response);
	int decodeDaemonPacket(SCPIDaemonCommandPacket *packet, SCPIDaemonCommandPacket *response);
	
	ExcecutionQueue* excecutionQueue;
	SCPIParser *parser;
	
	// have we been initialised properly, a link fatal error will set this to false
	// the master thread will kill us if this is set to false, we must not run
	bool init;				
	
	pthread_t thread;
public:
	
	// have we been initialised properly, a link fatal error will set this to false
	// link is only usable if true
	bool isInit();
	
	TCPWorker(long linkid, int listenSocket);
	~TCPWorker();
	
	// not thread safe, this should be called before this object is ever used in another thread
	// this should be treated as a return from the constructor
	ExcecutionQueue* getQueue();
	
	pthread_t* getThread();
	long getLinkid();
	void run();
};

#endif //TCP_WORKER_HPP
