#ifndef SCPISSERVERSTATE_HPP
#define SCPISSERVERSTATE_HPP

#define CHASSIS_CONFIG_FILE "/cs/releases/TLSX_chassis_config.cfg"
#define BACKUP_CONFIG_FILE "TLSX_chassis_config.cfg"

// standard event status register
#define SCPI_SESR_OPC	(1)		// operation complete
#define SCPI_SESR_QUERY	(1<<2)	// query error
#define SCPI_SESR_DDE	(1<<3)	// device dependant
#define SCPI_SESR_EXEC	(1<<4)	// excecution error
#define SCPI_SESR_CME	(1<<5)	// command error (parse error)

// status byte
#define SCPI_STB_MAV	(1<<4)
#define SCPI_STB_SESR	(1<<5)

#include <stdint.h>
#include <stdlib.h>
#include <string>
#include <version.h>
#include <vector>
#include "logging.h"
using namespace std;

class SCPIServerState
{
private:
	vector<bool> opc;
	
	// errors:
	bool executionError;
	bool deviceDependentError;
	
	string companyName;
	string chassisModelNumber;
	string chassisSerialNumber;
	string SWVersion;
	string HWVersion;
	bool isBenchtop;
	unsigned int maxNumBlades;
	unsigned int maxNumLasers;
public:
	SCPIServerState();
	~SCPIServerState();
	int TLSXConnectionSockfd;		

	void recordError(int e);
	
	// gets the status byte from the chassis level perspective (no parse errors or MAV (queue) bit, or MSS)
	// Status Byte Register
	uint8_t getInstrumentStatusByte();
	// Standard Event Status Register
	uint8_t getInstrumentErrorByte();
	
	string getCompanyNameString();
	string getInstrumentmodelNumber();
	string getInstrumentSerialNumber();
	string getHWVersionNumber();		// get hardware version number
	string getSWVersionNumber();		// get software version number
	
	bool getIsBenchtop();				// is this a benchtop unit with a single blade

	unsigned int getMaxNumberOfBlades();	// get the number of blades expected per chassis
	unsigned int getMaxNumberOfLasers();	// get the number of lasers expected per blade
};


#endif //SCPISSERVERSTATE_HPP
