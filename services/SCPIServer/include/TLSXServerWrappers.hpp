#ifndef TLSX_SERVER_WRAPPERS_HPP
#define TLSX_SERVER_WRAPPERS_HPP

#include "TLSXServerTCP.hpp"
#include "Units.hpp"
#include "SCPIServerState.hpp"

// any errors are taken care of inside these functions
// This includes setting the error and status bytes, and adding errors to the error queue
// the error bool passed to these functions will be set if the command failed
// otherwise the value is untouched

// sanity check values:
#define MAX_TEMP 80
#define MIN_TEMP 5

// takes a double and sends a value in device units for the command
void setDoubleValue(bool& e, uint8_t blade, uint8_t laser, double value, S2TCommand command, SCPIServerState& instrumentState);

// gives back a double value in the device units for that command
double getDoubleValue(bool& e, uint8_t blade, uint8_t laser, S2TCommand command, SCPIServerState& instrumentState);

// Get/Set state
void setBoolState(bool& e, uint8_t blade, uint8_t laser, bool on, S2TCommand command, SCPIServerState& instrumentState);
bool getBoolState(bool& e, uint8_t blade, uint8_t laser, S2TCommand command, SCPIServerState& instrumentState);

// laserReset
void laserReset(bool& e, uint8_t blade, uint8_t laser, SCPIServerState& instrumentState);

// GPIB:
string getIDNSlot(bool& e, uint8_t blade, SCPIServerState& instrumentState);
string getIDN(bool& e, SCPIServerState& instrumentState);
string getOPTSlot(bool& e, uint8_t blade, SCPIServerState& instrumentState);
string getOPT(bool& e, SCPIServerState& instrumentState);
string getOPC(bool& e, SCPIServerState& instrumentState);
string getOPCSlot(bool& e, uint8_t blade, SCPIServerState& instrumentState);
string getOPCLaser(bool& e, uint8_t blade, uint8_t laser, SCPIServerState& instrumentState);

// generic commands, set or get a double from the blade using the TLSX server, 
// send the S2TCommand command to the TLSXServer
// Returns TLSXSERVER_SUCCESS on success, TLSXSERVER_ERROR on error with 
// TLSX server, otherwise, errors defined in error_codes.h for SCPI to 
// TLSX protocol (numbers above 128)
/*TLSXServerReturn setDoubleValue(uint8_t blade, uint8_t laser, double value, S2TCommand command);
TLSXServerReturn getDoubleValue(uint8_t blade, uint8_t laser, double& value, S2TCommand command);

// takes a unitless value and sends a MHz value to the blade
void setFrequencyMHz(bool& e, uint8_t blade, uint8_t laser, UnitlessQuantity& value, S2TCommand command);
// returns a value in the default frequency units, gets a MHz value from the blade
double getFrequencyMHz(bool& e, uint8_t blade, uint8_t laser, S2TCommand command);

// takes a unitless value and sends a KHz value to the blade
void setFrequencyKHz(bool& e, uint8_t blade, uint8_t laser, UnitlessQuantity& value, S2TCommand command);
// returns a value in the default frequency units, gets a KHz value from the blade
double getFrequencyKHz(bool& e, uint8_t blade, uint8_t laser, S2TCommand command);

// takes a unitless value and sends a mdBm value to the blade
void setPowermdBm(bool& e, uint8_t blade, uint8_t laser, UnitlessQuantity& value, S2TCommand command);
// returns a value in the default power units, gets a mdBm value from the blade
double getPowermdBm(bool& e, uint8_t blade, uint8_t laser, S2TCommand command);

// Dither

TLSXServerReturn setSBSDitherState(bool& e, uint8_t blade, uint8_t laser, bool on);
TLSXServerReturn getSBSDitherState(bool& e, uint8_t blade, uint8_t laser, bool& on);

TLSXServerReturn setSBSDitherRate_Hz(bool& e, uint8_t blade, uint8_t laser, double value);
TLSXServerReturn getSBSDitherRate_Hz(bool& e, uint8_t blade, uint8_t laser, double& value);

TLSXServerReturn setSBSDitherFreq_Hz(bool& e, uint8_t blade, uint8_t laser, double value);
TLSXServerReturn getSBSDitherFreq_Hz(bool& e, uint8_t blade, uint8_t laser, double& value);

TLSXServerReturn setSBSDitherAmpl_mdBm(bool& e, uint8_t blade, uint8_t laser, double value);
TLSXServerReturn getSBSDitherAmpl_mdBm(bool& e, uint8_t blade, uint8_t laser, double& value);

TLSXServerReturn setContDitherState(bool& e, uint8_t blade, uint8_t laser, bool on);
TLSXServerReturn getContDitherState(bool& e, uint8_t blade, uint8_t laser, bool& on);

// Output
TLSXServerReturn setLaserState(bool& e, uint8_t blade, uint8_t laser, bool on);
TLSXServerReturn getLaserState(bool& e, uint8_t blade, uint8_t laser, bool& on);

// Source:Frequency
TLSXServerReturn setFreqFine_Hz(bool& e, uint8_t blade, uint8_t laser, double value);
TLSXServerReturn getFreqFine_Hz(bool& e, uint8_t blade, uint8_t laser, double& value);

// Source
TLSXServerReturn laserReset(bool& e, uint8_t blade, uint8_t laser);	//testing only, resets a laser
	// power
TLSXServerReturn getPowerMin_mdBm(bool& e, uint8_t blade, uint8_t laser, double& value);
TLSXServerReturn getPowerMax_mdBm(bool& e, uint8_t blade, uint8_t laser, double& value);
TLSXServerReturn setPower_mdBm(bool& e, uint8_t blade, uint8_t laser, double value);
TLSXServerReturn getPowerDesired_mdBm(bool& e, uint8_t blade, uint8_t laser, double& value);
TLSXServerReturn getPowerActual_mdBm(bool& e, uint8_t blade, uint8_t laser, double& value);
	// wavelength
TLSXServerReturn getWavelengthMin_m(uint8_t blade, uint8_t laser, double& value);
TLSXServerReturn getWavelengthMax_m(uint8_t blade, uint8_t laser, double& value);
TLSXServerReturn setWavelength_m(uint8_t blade, uint8_t laser, double value);
TLSXServerReturn getWavelengthlock_m(uint8_t blade, uint8_t laser, bool& on);
	// frequency
TLSXServerReturn getFrequencyMin_Hz(bool& e, uint8_t blade, uint8_t laser, double& value);
TLSXServerReturn getFrequencyMax_Hz(bool& e, uint8_t blade, uint8_t laser, double& value);
TLSXServerReturn setFrequency_Hz(bool& e, uint8_t blade, uint8_t laser, double value);
TLSXServerReturn getFrequencylock_Hz(bool& e, uint8_t blade, uint8_t laser, bool& on);

// TODO
//TLSXServerReturn execGrid

TLSXServerReturn getTemp(bool& e, uint8_t blade, uint8_t laser, double& value);

// GPIB:
TLSXServerReturn getIDN(bool& e, uint8_t blade, string& ret);

TLSXServerReturn getOPT(bool& e, uint8_t blade, string& ret);


//returns the temperature of the laser in degrees C
// returns negative numbers on error
// 0.0 is invalid
double getLaserTemperature(unsigned char blade, unsigned char laser);

// set laser state
// if state is true laser is turned on (if it isn't already)
// if state is false laser is turned off (if it isn't already)
// the effect of this function is never immediate, call does not block
// you must query the state of the laser periodically using getLaserState
void setLaserState(unsigned char blade, unsigned char laser, bool state);

// get the current state of the laser
// returns true if the laser reports that it is on
// returns false if the laser reports that it is off
bool getLaserState(unsigned char blade, unsigned char laser);

// double values are in dBm
double getMinPower_dBm(unsigned char blade, unsigned char laser);
double getMaxPower_dBm(unsigned char blade, unsigned char laser);
void setPower_dBm(unsigned char blade, unsigned char laser, double power);
double getDesiredPower_dBm(unsigned char blade, unsigned char laser);
double getActualPower_dBm(unsigned char blade, unsigned char laser);

// double values are in Hz
double getMinFrequency_Hz(unsigned char blade, unsigned char laser);
double getMaxFrequency_Hz(unsigned char blade, unsigned char laser);
void setFrequency_Hz(unsigned char blade, unsigned char laser, double frequency);
double getDesiredFrequency_Hz(unsigned char blade, unsigned char laser);
double getActualFrequency_Hz(unsigned char blade, unsigned char laser);

// convert power between mW and dBm (dBmW)
double dBmtomW(double power);
double mWtodBm(double power);

// double values are in mW
double getMinPower_mW(unsigned char blade, unsigned char laser);
double getMaxPower_mW(unsigned char blade, unsigned char laser);
void setPower_mW(unsigned char blade, unsigned char laser, double power);
double getDesiredPower_mW(unsigned char blade, unsigned char laser);
double getActualPower_mW(unsigned char blade, unsigned char laser);

// hardware resets the laser
void resetLaser(unsigned char blade, unsigned char laser);
*/
#endif //TLSX_SERVER_WRAPPERS_HPP
