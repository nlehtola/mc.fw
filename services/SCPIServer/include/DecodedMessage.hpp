#ifndef DECODEDMESSAGE_HPP
#define DECODEDMESSAGE_HPP

#define COMMAND_SUCCESS 0
#define COMMAND_ERROR   -1

#define MAX_NUM_BLADES 16
#define MAX_NUM_LASERS 4

#include "logging.h"
#include "TLSXServerWrappers.hpp"
#include "SCPIServerState.hpp"
#include "Units.hpp"

#include <stdio.h>
#include <iostream>
#include <stdint.h>
#include <vector>
using namespace std;

// this is a character which will be pushed onto the output stream of the
// device to indicate an imediate timeout should be initiated (command failed, thus no data to read)
// It should not be sent to the 
#define TLSX_ERROR_CHAR 129

// each of these id's corresponds to a function call to perform the command
typedef enum {
	ID_NONE = 0, // encodes end of command table
// Dither
	ID_SBSDITHER_STATE,
	ID_SBSDITHER_RATE,
	ID_SBSDITHER_FREQ,
	ID_CONTROLLEDDITHER_STATE,
// Source:Frequency
	ID_FINE,
// Operation
	ID_OP_EVENT,
	ID_OP_COND,
	ID_OP_ENABLE,
// Questionable
	ID_QUE_EVENT,
	ID_QUE_COND,
	ID_QUE_ENABLE,
// Output:Power
	ID_POWERUNIT,
// Output
	ID_STATE,
	ID_SAFETY,
// Source
	ID_RESET,	//testing only, resets a laser
	ID_POWER,
	ID_WAVE,
	ID_FREQ,
	ID_GRID,
	ID_TEMP,
// System
	ID_ERROR,
	ID_VERS,
	ID_DATE,
	ID_TIME,
// Status
	ID_PRES,
// GPIB:
	ID_CLS,
	ID_ESE,
	ID_ESR,
	ID_IDN,
	ID_OPC,
	ID_OPT,
	ID_RST,
	ID_STB,
	ID_TST
} DecodedMessageID;

// the meaning of TaskID varies depending on the command id
// each command function interprets this differently, some commands support
// multiple tasks, some support only the default
typedef enum {
//Commands:
	TASK_NONE = 0,		// encodes unset task in arguments
	TASK_COMMAND,
	STATE_ON,
	STATE_OFF,
	TASK_SET,
//Queries:
	TASK_QUERY,
	MIN,
	MAX,
	DESIRED,
	TASK_LOCK,
	ACTUAL,
	ALL
} TaskID;

// This class defines a deferred call to a command function, 
// Each call takes 2 arguments, task and value
// task is optional, and may be NULL or unnecessary for some commands
// value is a 64 bit double for a value which may be sent to the device
class DecodedMessage
{
private:
	DecodedMessageID id;
	TaskID task;
	uint8_t blade;
	uint8_t laser;
	vector<UnitValue*> values;
	Unit defaultUnit;
public:
	DecodedMessage(Unit defaultUnit, DecodedMessageID commandid, TaskID taskid, uint8_t bladeid, uint8_t laserid, vector<UnitValue*> values);

//	DecodedMessage(Unit defaultUnit, DecodedMessageID commandid, TaskID taskid, uint8_t bladeid, uint8_t laserid, double value);
	~DecodedMessage();
	int execute(string& response, SCPIServerState& instrumentState);
	// returns true if the message is a CLS command
	bool isCLS();
};

#endif //DECODEDMESSAGE_HPP

