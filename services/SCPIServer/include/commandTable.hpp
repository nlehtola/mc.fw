#ifndef COMMANDTABLE_H
#define COMMANDTABLE_H

#include "DecodedMessage.hpp"
#include "SCPItypes.hpp"

#include <stdio.h>

#include <string>
#include <sstream>
using namespace std;

// decodeCommand is responsible for checking if a command exists, and that it
// has the correct arguments. Thus it is part of parsing. It does not excecute
// the command. It returns a DecodedCommand object which may be queued for
// excecution, returns NULL on parser error
// passing NULL for dataList indicates no arguments	//TODO
// powerUnit is a pointer to the default unit for power
// if the user inputs the command which changes the default the this will be changed
// otherwise, this is used as the units for any power's input or output on this link
DecodedMessage* decodeHeader(Unit* powerUnit, bool query, vector<string>& headerPath, vector<ProgramDataValue*>& dataList);

//DecodedMessage* decodeMessage(bool query, vector<string>& commandPath, vector<ProgramDataValue*>* dataList);

// ################### new ########################

// decodeHeader:
// To find put if a combination of header, query and data list are valid you 
// must walk the tree and check each of the header nodes match. The 
// HeaderNode's form a tree of header names and child nodes, If the child 
// nodes member of an entry in the table is NULL then it shall be 
// considered to not match unless the header path depth matches the depth 
// into the tree. If the header path matches a set of nodes in the tree
// then the ArgumentNode of that node is passed to decodeArgs, if args
// is NULL then the node is non terminal and will parse error
// for commands allowing trailing digits explicit 0 is invalid, but no digits
// decodes to 0

// decodeArgs:
// takes a list of ArgumentNodes from decodeHeader, which correspond to the
// possible argument arrangments for the command. Each possible arangment
// is either a query or a command. The default TaskID may be specified by 
// some commands that do not determine it from the argments. 
// Each ArgumentNode has a list of Argument's which make up the arguments for
// that arangment. Each of these can either be optional or required. If a user
// does not specify an optional argument then it's default value will be sent
// to DecodedMessage. If the defaultTask argument is not TASK_NONE then
// the taskString is compared to the string inputed by the user. If it does not
// match then a parse error occurs. If defaultTask is TASK_NONE then the user
// must supply a decimal numeric number. Each of these will be appended to the 
// list of values in DecodedMessage, in order

#define NODE_CMD false		// not a query
#define NODE_QUERY true		// query

#define NODE_PLAIN false	// the header node does not support digits
#define NODE_DIGIT true		// the header node supports optional digits

struct Argument
{
	bool optional;			// can this argument be ommited
	TaskID defaultTask;		// task id to use if taskString matches
	string taskString;
};

struct ArgumentNode
{
	bool query;

	// TASK_NONE indicates that arguments specify task
	TaskID defaultTask;		// task id if an argument doesn't set one
	unsigned int argsLength; // how long is args, args[argsLength] will segfault
	const Argument args[3];
};

struct HeaderNode
{
	DecodedMessageID messageid;
	bool digit; // is a trailing digit allowed? either NODE_DIGIT or NODE_PLAIN
	const char* shortForm;
	const char* longForm;
	const HeaderNode* children;
	const ArgumentNode* args;
	Unit defaultUnit;
};

#endif //COMMANDTABLE_H
