#ifndef PROGRAM_MESSAGE_QUEUE_HPP
#define PROGRAM_MESSAGE_QUEUE_HPP

#include "SCPItypes.hpp"
#include "SCPIServerState.hpp"
#include <pthread.h>
#include <queue>
#include <stdio.h>
#include <string>
using namespace std;

#define EXECUTE_SUCCESS		0
#define EXECUTE_NOCOMMAND	-1
#define EXECUTE_ERROR		-2
#define EXECUTE_TIMEOUT		-3

// This is a thread safe queue
// behaves exactly as a queue<ProgramMessage*> would in a single threaded environment
class ExcecutionQueue
{
private:
	queue<DecodedMessageList*> decodedMessages;
	queue<string> responseMessages;
	
	pthread_mutex_t queueMutex;
	long linkid;
	bool parseError;	//TODO *CLS
public:
	
	ExcecutionQueue(long linkid);
	~ExcecutionQueue();
	
	// queue related stuff
	int excecuteNext(SCPIServerState& instrumentState);
	void pushDecodedMessages(DecodedMessageList*& x);
	
	uint8_t getStatusByte(SCPIServerState& instrumentState);
	string getNextResponseMessage();
};

#endif //PROGRAM_MESSAGE_QUEUE_HPP
