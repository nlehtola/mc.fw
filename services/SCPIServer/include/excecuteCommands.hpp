#ifndef EXCECUTECOMMANDS_HPP
#define EXCECUTECOMMANDS_HPP

#include <stdio.h>
#include "Units.hpp"
#include "stdint.h"
#include "TLSXServerWrappers.hpp"
#include "SCPIServerState.hpp"
#include "DecodedMessage.hpp"
#include <sstream>
using namespace std;

#define FLOAT_PRECISION_FREQ	8
#define FLOAT_PRECISION_POW		6
#define FLOAT_PRECISION_GRID	6
#define FLOAT_PRECISION_WAV		6
#define FLOAT_PRECISION_SBSRATE 3

// Dither
int execSBSDitherState(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);
int execSBSDitherRate(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);
int execSBSDitherFreq(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);
int execControlledDitherState(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);

// Source:Frequency
int execFreqFine(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);

// Operation

// Questionable

// Output:Power
int execLaserPowerUnit(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);

// Output
int execLaserState(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);
int execLaserSafety(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);

// Source
int execReset(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);	//testing only, resets a laser
int execPower(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);
int execWavelength(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);
int execFrequency(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);
int execGrid(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);
int execTemp(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);

// System
//int execError(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);
int execVersion(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);

// Status

// GPIB:
//int execCLS(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);
//int execESE(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);
//int execESR(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);
int execIDN(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);
int execOPC(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);
int execOPT(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);
//int execRST(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);
//int execSTB(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);
//int execTST(Unit defaultUnit, SCPIServerState& instrumentState, string& response, TaskID& task, uint8_t blade, uint8_t laser, vector<UnitValue*>& values);

#endif //EXCECUTECOMMANDS_HPP
