#ifndef UNITS_HPP
#define UNITS_HPP

#include "math.h"
#include <string>
#include "logging.h"
using namespace std;

// This is used for conversions from wavelength to frequency
// TODO: set correctly
#define SPEED_OF_LIGHT (299792458.0)	//m/s

enum Unit {
	UNIT_NONE,
	UNIT_C,
	UNIT_CC,
	//frequency
	UNIT_HZ,
	UNIT_KHZ,
	UNIT_MHZ,
	UNIT_GHZ,
	UNIT_THZ,
	//wavelength
	UNIT_M,
	UNIT_MM,
	UNIT_UM,
	UNIT_PM,
	UNIT_NM,
	//power
	UNIT_DBM,
	UNIT_MDBM,
	UNIT_W,
	UNIT_MW
};

enum UnitType {
	UNIT_FREQ,
	UNIT_POWER,
	UNIT_DBMPOWER,
	UNIT_WAVE,
	UNIT_TEMP,
	UNIT_TYPE_NONE
};

/*// A unit bucket is a place where the default units for a certain command type
// (eg. laser frequency) are storred. This also stores a list of valid units 
// for the command type
class UnitFamily {
private:
	const Unit* validUnits;
	Unit defaultUnit;
public:
	// validUnits must be an array of units ending in UNIT_NONE
	// UNIT_NONE indicates the end of the array
	UnitFamily(const Unit* validUnits, Unit defaultUnit);
	// set the default unit for this bucket
	UnitResponse setDefault(Unit unit);
	// get the current default unit for this bucket
	Unit getDefault();
	// get the default unit for this bucket as a string
	string getDefaultString();
	// is the unit represented by string provided valid for this bucket
	// returns the Unit that it represents
	Unit isValid(string name);
	bool isValid(Unit unit);
	// do a conversion from 1 unit in this family to the default
	double convertToDefault(Unit from, double input);
};*/

// converts wavelength to frequency
double mToMHzRoundToHundredmHz(double wavelength);
// converts frequency to wavelength
double MHzTom(double frequency);

string convertToUpper(string in);
bool isSameType(string name, Unit unit);
Unit unitFromString(string name);
string getUnitName(Unit unit);
// convert input from units from into units to
// returns the converted value
double doUnitConvertion(double input, Unit from, Unit to);

// Stores a number quantity and allows it to be outputted in
// any units that the quantity supports
class UnitValue {
private:
	double value;
	Unit unit;
	
public:
	// create a new quantity with a value in the unit
	UnitValue(double value, Unit from, Unit to);
	
	// get the quantity in a certain unit
	// returns NaN if not compatable units
	// Logs an error message
	double get(Unit unit);
	double get();
	string getUnitString();
};


#endif //UNITS_HPP
