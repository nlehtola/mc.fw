#ifndef SCPIPARSER_HPP
#define SCPIPARSER_HPP

//#include <unistd.h>
#include "parser/SCPI.tab.hpp"
#include "parser/SCPI.yy.hpp"
#include "SCPItypes.hpp"
//#include "commandTable.hpp"
#include "ExcecutionQueue.hpp"
#include "logging.h"

#include <iostream>
#include <string>
using namespace std;

#define PARSER_SUCCESS		0
#define PARSER_PARSE_ERROR	-1
#define PARSER_FATAL_ERROR	-2 		//link fatal

class SCPIParser
{
private:
	// The state of the lexer and parser
	YYSTYPE type;
	yypstate *ps;
	yyscan_t scanner;
	
	// a pair of connected FILE descriptors
	int inputfdWrite;		//input (we push into here)
	FILE *inputfdRead;		//output (lexer pulls out of here)
	
	ExcecutionQueue* excecutionQueue;
	Unit powerUnits;		// this is a user setable value for the units that power has
	
	long linkid;
	void initPipes();
	void initScanner();

	// has the flex scanner and bison parser instance been
	// initialized, indicates to the destructor that it must be freed
	bool scannerInit;
	
	// from any state, clean all resources
	void cleanup();
	
	// resets the parser state to send a new command
	void reset();
public:
	SCPIParser(ExcecutionQueue* excecutionQueue, long linkid);
	~SCPIParser();

	// 0 on success, -1 on parse error
	// if end is true, then an ^END indicator will be associated with the last byte in data
	int push(char* data, unsigned int length, bool end);
};

#endif //SCPIPARSER_HPP
