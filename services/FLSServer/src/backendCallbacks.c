#include "backendCallbacks.h"
#include <string.h>

// Dither
int execSBSDitherState(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values)
{
	spLogDebug(1, "executing SOUR%d:CHAN%d:SBS:STATE", blade, laser);
	int e = 0;
	size_t n = 0;
	if (task==STATE_ON) {
		spLogDebug(1, "Turning SBS %d %d ON", blade, laser);
		setBoolState(&e, blade, laser, 1, S2T_SET_STATE_SBS);
	} else if (task==STATE_OFF) {
		spLogDebug(1, "Turning SBS %d %d OFF", blade, laser);
		setBoolState(&e, blade, laser, 0, S2T_SET_STATE_SBS);
	} else if (task==TASK_QUERY) {
		if (blade==0) {
			spLogDebug(1, "Excecute error, blade id invalid in OUTP[n]:CHAN[m]:SBS:STATE");
	 		e = 1;
		} else if (laser==0) {
			spLogDebug(1, "Excecute error, laser id invalid in OUTP[n]:CHAN[m]:SBS:STATE");
			e = 1;
		} else {
			if (getBoolState(&e, blade, laser, S2T_GET_STATE_SBS)) {
				n += snprintf(response+n, max-n, "ON");
			} else {
				n += snprintf(response+n, max-n, "OFF");
			}
		}
	} else {
		spLogError("Command Error, TaskID for OUTP[n]:CHAN[m]:SBS:STATE was incorrect: %d", task);
		// If you see this error you must look in 
		// commandTable.cpp -> ArgumentNode stateArgs
		e = 1;
	}
	if (e) {
		*response = '\0';	//erases any response written in this function
		return COMMAND_ERROR;
	}
	return n;
}

int execSBSDitherRate_Hz(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values)
{
	spLogDebug(1, "executing SOUR%d:CHAN%d:SBS:RATE", blade, laser);
	double temp;
	int e = 0;
	size_t n = 0;
	switch (task)
	{
	case MIN:
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_MINSBSRATE), UNIT_HZ, UNIT_KHZ);
		n += snprintf(response+n, max-n, "%.*e", FLOAT_PRECISION_SBSRATE, temp);
		break;
	case MAX:
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_MAXSBSRATE), UNIT_HZ, UNIT_KHZ);
		n += snprintf(response+n, max-n, "%.*e", FLOAT_PRECISION_SBSRATE, temp);
		break;
	case TASK_SET:
		if (numValues!=1) {
			return COMMAND_ERROR;
		}
		doUnitConvertion(&temp, values[0], UNIT_KHZ, UNIT_HZ);
		setDoubleValue(&e, blade, laser, temp, S2T_SET_SBSRATE);
		break;
	case ACTUAL:
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_ACTSBSRATE), UNIT_HZ, UNIT_KHZ);
		n += snprintf(response+n, max-n, "%.*e", FLOAT_PRECISION_SBSRATE, temp);
		break;
	case ALL:
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_MINSBSRATE), UNIT_HZ, UNIT_KHZ);
		n += snprintf(response+n, max-n, "%.*e,", FLOAT_PRECISION_SBSRATE, temp);
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_MAXSBSRATE), UNIT_HZ, UNIT_KHZ);
		n += snprintf(response+n, max-n, "%.*e,", FLOAT_PRECISION_SBSRATE, temp);
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_ACTSBSRATE), UNIT_HZ, UNIT_KHZ);
		n += snprintf(response+n, max-n, "%.*e", FLOAT_PRECISION_SBSRATE, temp);
		break;
	default:
		spLogError("Command Error, TaskID for SOUR[n]:CHAN[m]:SBS:RATE was incorrect: %d", task);
		// If you see this error you must look in 
		// commandTable.cpp -> ArgumentNode ditherArgs
		e = 1;
	}
	if (e) {
		*response = '\0';	//erases any response written in this function
		return COMMAND_ERROR;
	}
	return n;
}

int execSBSDitherFreq_Hz(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values)
{
	spLogDebug(1, "executing SOUR%d:CHAN%d:SBS:RATE", blade, laser);
	double temp;
	int e = 0;
	size_t n = 0;
	switch (task)
	{
	case MIN:
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_MINSBSFREQ), UNIT_HZ, UNIT_MHZ);
		n += snprintf(response+n, max-n, "%.*e", FLOAT_PRECISION_FREQ, temp);
		break;
	case MAX:
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_MAXSBSFREQ), UNIT_HZ, UNIT_MHZ);
		n += snprintf(response+n, max-n, "%.*e", FLOAT_PRECISION_FREQ, temp);
		break;
	case TASK_SET:
		if (numValues!=1) {
			return COMMAND_ERROR;
		}
		doUnitConvertion(&temp, values[0], UNIT_MHZ, UNIT_HZ);
		setDoubleValue(&e, blade, laser, temp, S2T_SET_SBSFREQ);
		break;
	case ACTUAL:
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_ACTSBSFREQ), UNIT_HZ, UNIT_MHZ);
		n += snprintf(response+n, max-n, "%.*e", FLOAT_PRECISION_FREQ, temp);
		break;
	case ALL:
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_MINSBSFREQ), UNIT_HZ, UNIT_MHZ);
		n += snprintf(response+n, max-n, "%.*e,", FLOAT_PRECISION_FREQ, temp);
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_MAXSBSFREQ), UNIT_HZ, UNIT_MHZ);
		n += snprintf(response+n, max-n, "%.*e,", FLOAT_PRECISION_FREQ, temp);
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_ACTSBSFREQ), UNIT_HZ, UNIT_MHZ);
		n += snprintf(response+n, max-n, "%.*e", FLOAT_PRECISION_FREQ, temp);
		break;
	default:
		spLogError("Command Error, TaskID for SOUR[n]:CHAN[m]:SBS:FREQ was incorrect: %d", task);
		// If you see this error you must look in 
		// commandTable.cpp -> ArgumentNode ditherArgs
		e = 1;
	}
	if (e) {
		*response = '\0';	//erases any response written in this function
		return COMMAND_ERROR;
	}
	return n;
}

int execControlledDitherState(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values)
{
	spLogDebug(1, "executing SOUR%d:CHAN%d:DITHER:STATE", blade, laser);
	int e = 0;
	size_t n = 0;
	if (task==STATE_ON) {
		spLogDebug(1, "Turning controlled dither  %d %d ON", blade, laser);
		setBoolState(&e, blade, laser, 1, S2T_SET_STATE_CONTROL_DITHER);
	} else if (task==STATE_OFF) {		
		spLogDebug(1, "Turning controlled dither  %d %d OFF", blade, laser);
		setBoolState(&e, blade, laser, 0, S2T_SET_STATE_CONTROL_DITHER);
	} else if (task==TASK_QUERY) {
		if (blade==0) {
			spLogDebug(1, "Excecute error, blade id invalid in OUTP[n]:CHAN[m]:DITHER:STATE");
	 		e = 1;
		} else if (laser==0) {
			spLogDebug(1, "Excecute error, laser id invalid in OUTP[n]:CHAN[m]:DITHER:STATE");
			e = 1;
		} else {
			if (getBoolState(&e, blade, laser, S2T_GET_STATE_CONTROL_DITHER)) {
				n += snprintf(response+n, max-n, "ON");
			} else {
				n += snprintf(response+n, max-n, "OFF");
			}
		}
	} else {
		spLogError("Command Error, TaskID for OUTP[n]:CHAN[m]:DITHER:STATE was incorrect: %d", task);
		// If you see this error you must look in 
		// commandTable.cpp -> ArgumentNode stateArgs
		e = 1;
	}
	if (e) {
		*response = '\0';	//erases any response written in this function
		return COMMAND_ERROR;
	}
	return n;
}

// Source:Frequency:Fine
int execFreqFine_Hz(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values)
{
	spLogDebug(1, "executing SOUR%d:CHAN%d:FREQ:FINE", blade, laser);
	double temp;
	int e = 0;
	size_t n = 0;
	switch (task)
	{
	case MIN:
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_MINFINE), UNIT_HZ, UNIT_MHZ);
		n += snprintf(response+n, max-n, "%.*e", FLOAT_PRECISION_FREQ, temp);
		break;
	case MAX:
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_MAXFINE), UNIT_HZ, UNIT_MHZ);
		n += snprintf(response+n, max-n, "%.*e", FLOAT_PRECISION_FREQ, temp);
		break;
	case TASK_SET:
		if (numValues!=1) {
			return COMMAND_ERROR;
		}
		doUnitConvertion(&temp, values[0], UNIT_MHZ, UNIT_HZ);
		setDoubleValue(&e, blade, laser, temp, S2T_SET_FINE);
		break;
	case ACTUAL:
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_ACTFINE), UNIT_HZ, UNIT_MHZ);
		n += snprintf(response+n, max-n, "%.*e", FLOAT_PRECISION_FREQ, temp);
		break;
	case ALL:
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_MINFINE), UNIT_HZ, UNIT_MHZ);
		n += snprintf(response+n, max-n, "%.*e,", FLOAT_PRECISION_FREQ, temp);
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_MAXFINE), UNIT_HZ, UNIT_MHZ);
		n += snprintf(response+n, max-n, "%.*e,", FLOAT_PRECISION_FREQ, temp);
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_ACTFINE), UNIT_HZ, UNIT_MHZ);
		n += snprintf(response+n, max-n, "%.*e", FLOAT_PRECISION_FREQ, temp);
		break;
	default:
		spLogError("Command Error, TaskID for SOUR[n]:CHAN[m]:FREQ:FINE was incorrect: %d", task);
		// If you see this error you must look in 
		// commandTable.cpp -> ArgumentNode fineArgs
		e = 1;
	}
	if (e) {
		*response = '\0';	//erases any response written in this function
		return COMMAND_ERROR;
	}
	return n;
}

// Output:Power
int execLaserPowerUnit(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values)
{
	size_t n = 0;
	spLogDebug(1, "executing OUTP%d:CHAN%d:POW:UNIT?", blade, laser);
	n += snprintf(response+n, max-n, "dBm");
	return n;
}

// Output
int execLaserState(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values)
{
	spLogDebug(1, "executing OUTP%d:CHAN%d:STATE", blade, laser);
	int e = 0;
	size_t n = 0;
	if (task==STATE_ON) {
		spLogDebug(1, "Turning laser %d %d ON", blade, laser);
		setBoolState(&e, blade, laser, 1, S2T_SET_STATE_LASER);
	} else if (task==STATE_OFF) {
		spLogDebug(1, "Turning laser %d %d OFF", blade, laser);
		setBoolState(&e, blade, laser, 0, S2T_SET_STATE_LASER);
	} else if (task==TASK_QUERY) {
		if (blade==0) {
			spLogDebug(1, "Excecute error, blade id invalid in OUTP[n]:CHAN[m]:STATE");
	 		e = 1;
		} else if (laser==0) {
			spLogDebug(1, "Excecute error, laser id invalid in OUTP[n]:CHAN[m]:STATE");
			e = 1;
		} else {
			if (getBoolState(&e, blade, laser, S2T_GET_STATE_LASER)) {
				n += snprintf(response+n, max-n, "ON");
			} else {
				n += snprintf(response+n, max-n, "OFF");
			}
		}
	} else {
		spLogError("Command Error, TaskID for OUTP[n]:CHAN[m]:STATE was incorrect: %d", task);
		// If you see this error you must look in 
		// commandTable.cpp -> ArgumentNode stateArgs
		e = 1;
	}
	if (e) {
		*response = '\0';	//erases any response written in this function
		return COMMAND_ERROR;
	}
	return n;
}

int execLaserSafety(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values)
{
	spLogDebug(1, "executing OUTP%d:CHAN%d:SAFETY", blade, laser);
	int e = 0;
	size_t n = 0;
	if (task==STATE_ON) {
		spLogDebug(1, "Turning laser %d %d safety mode to ON", blade, laser);
		setBoolState(&e, blade, laser, 1, S2T_SET_SAFETY);
	} else if (task==STATE_OFF) {
		spLogDebug(1, "Turning laser %d %d safety mode to OFF", blade, laser);
		setBoolState(&e, blade, laser, 0, S2T_SET_SAFETY);
	} else if (task==TASK_QUERY) {
		if (blade==0) {
			spLogDebug(1, "Excecute error, blade id invalid in OUTP[n]:CHAN[m]:SAFETY");
	 		e = 1;
		} else if (laser==0) {
			spLogDebug(1, "Excecute error, laser id invalid in OUTP[n]:CHAN[m]:SAFETY");
			e = 1;
		} else {
			if (getBoolState(&e, blade, laser, S2T_GET_SAFETY)) {
				n += snprintf(response+n, max-n, "ON");
			} else {
				n += snprintf(response+n, max-n, "OFF");
			}
		}
	} else {
		spLogError("Command Error, TaskID for OUTP[n]:CHAN[m]:SAFETY was incorrect: %d", task);
		// If you see this error you must look in 
		// commandTable.cpp -> ArgumentNode stateArgs
		e = 1;
	}
	if (e) {
		*response = '\0';	//erases any response written in this function
		return COMMAND_ERROR;
	}
	return n;
}

// Source
int execReset(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values)
{
	spLogDebug(1, "executing SOUR%d:CHAN%d:RESET", blade, laser);
	spLogWarning("Warning: executing SOUR%d:CHAN%d:RESET, this is a testing command ONLY", blade, laser);
	return 0;
}

int execPower_dBm(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values)
{
	spLogDebug(1, "executing SOUR%d:CHAN%d:POW", blade, laser);
	double temp;
	int e = 0;
	size_t n = 0;
	switch (task)
	{
	case MIN:
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_MINPOWER), UNIT_DBM, UNIT_MDBM);
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_POW, temp);
		break;
	case MAX:
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_MAXPOWER), UNIT_DBM, UNIT_MDBM);
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_POW, temp);
		break;
	case TASK_SET:
		if (numValues!=1) {
			return COMMAND_ERROR;
		}
		doUnitConvertion(&temp, values[0], UNIT_MDBM, UNIT_DBM);
		setDoubleValue(&e, blade, laser, temp, S2T_SET_POWER);
		break;
	case DESIRED:
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_DESPOWER), UNIT_DBM, UNIT_MDBM);
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_POW, temp);
		break;
	case ACTUAL:
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_ACTPOWER), UNIT_DBM, UNIT_MDBM);
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_POW, temp);
		break;
	case ALL:
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_MINPOWER), UNIT_DBM, UNIT_MDBM);
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_POW, temp);
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_MAXPOWER), UNIT_DBM, UNIT_MDBM);
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_POW, temp);
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_DESPOWER), UNIT_DBM, UNIT_MDBM);
		n += snprintf(response+n, max-n, "%.*f,", FLOAT_PRECISION_POW, temp);
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_ACTPOWER), UNIT_DBM, UNIT_MDBM);
		n += snprintf(response+n, max-n, "%.*f", FLOAT_PRECISION_POW, temp);
		break;
	default:
		spLogError("Command Error, TaskID for SOUR[n]:CHAN[m]:POW was incorrect: %d", task);
		// If you see this error you must look in 
		// commandTable.cpp -> ArgumentNode powerArgs
		e = 1;
	}
	if (e) {
		*response = '\0';	//erases any response written in this function
		return COMMAND_ERROR;
	}
	return n;
}
int execWavelength_m(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values)
{
	spLogDebug(1, "executing SOUR%d:CHAN%d:WAVE", blade, laser);
	double temp;
	int e = 0;
	size_t n = 0;
	switch (task)
	{
	case MIN:	//MIN wavelength is max frequency
		doUnitConvertion(&temp, MHzTom(getDoubleValue(&e, blade, laser, S2T_GET_MAXFREQ)), UNIT_M, UNIT_M);
		n += snprintf(response+n, max-n, "%.*e", FLOAT_PRECISION_WAV, temp);
		break;
	case MAX:	//MAX wavelength is min frequency
		doUnitConvertion(&temp, MHzTom(getDoubleValue(&e, blade, laser, S2T_GET_MINFREQ)), UNIT_M, UNIT_M);
		n += snprintf(response+n, max-n, "%.*e", FLOAT_PRECISION_WAV, temp);
		break;
	case TASK_SET:
		if (numValues!=1) {
			return COMMAND_ERROR;
		}
		setDoubleValue(&e, blade, laser, mToMHzRoundToHundredMHz(values[0]), S2T_SET_FREQ);
		break;
	case DESIRED:
		doUnitConvertion(&temp, MHzTom(getDoubleValue(&e, blade, laser, S2T_GET_DESFREQ)), UNIT_M, UNIT_M);
		n += snprintf(response+n, max-n, "%.*e", FLOAT_PRECISION_WAV, temp);
		break;
	case LOCK:
		if (getBoolState(&e, blade, laser, S2T_GET_LOCKFREQ)) {
			n += snprintf(response+n, max-n, "TRUE");
		} else {
			n += snprintf(response+n, max-n, "FALSE");
		}
		break;
	case ALL:
		doUnitConvertion(&temp, MHzTom(getDoubleValue(&e, blade, laser, S2T_GET_MAXFREQ)), UNIT_M, UNIT_M);
		n += snprintf(response+n, max-n, "%.*e,", FLOAT_PRECISION_WAV, temp);
		doUnitConvertion(&temp, MHzTom(getDoubleValue(&e, blade, laser, S2T_GET_MINFREQ)), UNIT_M, UNIT_M);
		n += snprintf(response+n, max-n, "%.*e,", FLOAT_PRECISION_WAV, temp);
		doUnitConvertion(&temp, MHzTom(getDoubleValue(&e, blade, laser, S2T_GET_DESFREQ)), UNIT_M, UNIT_M);
		n += snprintf(response+n, max-n, "%.*e,", FLOAT_PRECISION_WAV, temp);
		if (getBoolState(&e, blade, laser, S2T_GET_LOCKFREQ)) {
			n += snprintf(response+n, max-n, "TRUE");
		} else {
			n += snprintf(response+n, max-n, "FALSE");
		}
		break;
	default:
		spLogError("Command Error, TaskID for SOUR[n]:CHAN[m]:WAVE was incorrect: %d", task);
		// If you see this error you must look in 
		// commandTable.cpp -> ArgumentNode wavelengthArgs
		e = 1;
	}
	if (e) {
		*response = '\0';	//erases any response written in this function
		return COMMAND_ERROR;
	}
	return n;
}

int execFrequency_Hz(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values)
{
	spLogDebug(1, "executing SOUR%d:CHAN%d:FREQ", blade, laser);
	double temp;
	int e = 0;
	size_t n = 0;
	switch (task)
	{
	case MIN:
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_MINFREQ), UNIT_HZ, UNIT_MHZ);
		n += snprintf(response+n, max-n, "%.*e", FLOAT_PRECISION_FREQ, temp);
		break;
	case MAX:
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_MAXFREQ), UNIT_HZ, UNIT_MHZ);
		n += snprintf(response+n, max-n, "%.*e", FLOAT_PRECISION_FREQ, temp);
		break;
	case TASK_SET:
		if (numValues!=1) {
			return COMMAND_ERROR;
		}
		doUnitConvertion(&temp, values[0], UNIT_MHZ, UNIT_HZ);
		setDoubleValue(&e, blade, laser, temp, S2T_SET_FREQ);
		break;
	case DESIRED:
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_DESFREQ), UNIT_HZ, UNIT_MHZ);
		n += snprintf(response+n, max-n, "%.*e", FLOAT_PRECISION_FREQ, temp);
		break;
	case LOCK:
		if (getBoolState(&e, blade, laser, S2T_GET_LOCKFREQ)) {
			n += snprintf(response+n, max-n, "TRUE");
		} else {
			n += snprintf(response+n, max-n, "FALSE");
		}
		break;
	case ALL:
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_MINFREQ), UNIT_HZ, UNIT_MHZ);
		n += snprintf(response+n, max-n, "%.*e,", FLOAT_PRECISION_FREQ, temp);
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_MAXFREQ), UNIT_HZ, UNIT_MHZ);
		n += snprintf(response+n, max-n, "%.*e,", FLOAT_PRECISION_FREQ, temp);
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_DESFREQ), UNIT_HZ, UNIT_MHZ);
		n += snprintf(response+n, max-n, "%.*e,", FLOAT_PRECISION_FREQ, temp);
		if (getBoolState(&e, blade, laser, S2T_GET_LOCKFREQ)) {
			n += snprintf(response+n, max-n, "TRUE");
		} else {
			n += snprintf(response+n, max-n, "FALSE");
		}
		break;
	default:
		spLogError("Command Error, TaskID for SOUR[n]:CHAN[m]:FREQ was incorrect: %d", task);
		// If you see this error you must look in 
		// commandTable.cpp -> ArgumentNode frequencyArgs
		e = 1;
	}
	if (e) {
		*response = '\0';	//erases any response written in this function
		return COMMAND_ERROR;
	}
	return n;
}

int execGrid_Hz(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values)
{
	spLogDebug(1, "executing SOUR%d:CHAN%d:GRID", blade, laser);
	double temp;
	int e = 0;
	size_t n = 0;
	switch (task)
	{
	case MIN:
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_MINGRID), UNIT_HZ, UNIT_MHZ);
		n += snprintf(response+n, max-n, "%.*e", FLOAT_PRECISION_GRID, temp);
		break;
	case MAX:
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_MAXGRID), UNIT_HZ, UNIT_MHZ);
		n += snprintf(response+n, max-n, "%.*e", FLOAT_PRECISION_GRID, temp);
		break;
	case TASK_SET:
		if (numValues!=1) {
			return COMMAND_ERROR;
		}
		doUnitConvertion(&temp, values[0], UNIT_MHZ, UNIT_HZ);
		setDoubleValue(&e, blade, laser, temp, S2T_SET_GRID);
		break;
	case ACTUAL:
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_ACTGRID), UNIT_HZ, UNIT_MHZ);
		n += snprintf(response+n, max-n, "%.*e", FLOAT_PRECISION_GRID, temp);
		break;
	case ALL:
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_MINGRID), UNIT_HZ, UNIT_MHZ);
		n += snprintf(response+n, max-n, "%.*e,", FLOAT_PRECISION_GRID, temp);
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_MAXGRID), UNIT_HZ, UNIT_MHZ);
		n += snprintf(response+n, max-n, "%.*e,", FLOAT_PRECISION_GRID, temp);
		doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_ACTGRID), UNIT_HZ, UNIT_MHZ);
		n += snprintf(response+n, max-n, "%.*e", FLOAT_PRECISION_GRID, temp);
		break;
	default:
		spLogError("Command Error, TaskID for SOUR[n]:CHAN[m]:FREQ:GRID was incorrect: %d", task);
		// If you see this error you must look in 
		// commandTable.cpp -> ArgumentNode gridArgs
		e = 1;
	}
	if (e) {
		*response = '\0';	//erases any response written in this function
		return COMMAND_ERROR;
	}
	return n;
}

int execTemp_C(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values)
{
	spLogDebug(1, "executing SOUR[n]:CHAN[m]:TEMP");
	int e = 0;
	size_t n = 0;
	if (task==TASK_QUERY) {
	 	double temp;
	 	if (blade==0) {
	 		return COMMAND_ERROR;
	 	} else if (laser==0) {
	 		int i;
			for (i=1;; ++i) {
				doUnitConvertion(&temp, getDoubleValue(&e, blade, i, S2T_GET_TEMP), UNIT_C, UNIT_CC);
				n += snprintf(response+n, max-n, "%.2f", temp);
				if (i>=MAX_NUM_LASERS) {
					break;
				}
				n += snprintf(response+n, max-n, ",");
			}
		} else {
			doUnitConvertion(&temp, getDoubleValue(&e, blade, laser, S2T_GET_TEMP), UNIT_C, UNIT_CC);
			n += snprintf(response+n, max-n, "%.2f", temp);
		}
	} else {
		spLogError("Command Error, TaskID for SOUR[n]:CHAN[m]:TEMP was incorrect: %d", task);
		// If you see this error you must look in 
		// commandTable.cpp -> ArgumentNode noQueryArgs
		e = 1;
	}
	if (e) {
		*response = '\0';	//erases any response written in this function
		return COMMAND_ERROR;
	}
	return n;
}

// GPIB:
int execIDN(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values)
{
	spLogDebug(1, "executing IDN");
	int e = 0;
	size_t n = 0;
	if (task==TASK_QUERY) {
		if (blade==0) {
			n += getIDN(&e, max, response);
		} else {
			n += getIDNSlot(&e, max, response, blade);
		}
	} else {
		spLogError("Command Error, TaskID for *IDN or SLOT[n]:IDN was incorrect: %d", task);
		// If you see this error you must look in 
		// commandTable.cpp -> ArgumentNode noQueryArgs
		e = 1;
	}
	if (e) {
		*response = '\0';	//erases any response written in this function
		return COMMAND_ERROR;
	}
	return n;
}

int execOPC(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values)
{
	spLogDebug(1, "executing OPC");
	int e = 0;
	size_t n = 0;
	if (task==TASK_QUERY) {
		// a bladeid of 0 returns opc for the instrument
		if (blade==0) {
			// Note that SCPI says that OPC is sequential command. My 
			// interpretation is that this means OPC commands block until they
			// return 1, which makes sense. Except it's tricky to impliment
			// Also this is not how the TLS-X works, so for now I am returning
			// 0 when OPC is false instead of blocking
			if(getOPC(&e)) {
				n += snprintf(response+n, max-n, "1");
			} else {
				n += snprintf(response+n, max-n, "0");
			}
		} else {
			if(getOPCSlot(&e, blade)) {
				n += snprintf(response+n, max-n, "1");
			} else {
				n += snprintf(response+n, max-n, "0");
			}
		}
	} else {
		spLogError("Command Error, TaskID for *OPC or SLOT[n]:OPC was incorrect: %d", task);
		// If you see this error you must look in 
		// commandTable.cpp -> ArgumentNode noQueryArgs
		e = 1;
	}
	if (e) {
		*response = '\0';	//erases any response written in this function
		return COMMAND_ERROR;
	}
	return n;
}

int execOPT(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values)
{
	spLogDebug(1, "executing OPT");
	int e = 0;
	size_t n = 0;
	if (task==TASK_QUERY) {
		if (blade==0) {
			n += getOPT(&e, max, response);
		} else {
			n += getOPTSlot(&e, max, response, blade);
		}
	} else {
		spLogError("Command Error, TaskID for *OPT or SLOT[n]:OPT was incorrect: %d", task);
		// If you see this error you must look in 
		// commandTable.cpp -> ArgumentNode noQueryArgs
		e = 1;
	}
	if (e) {
		*response = '\0';	//erases any response written in this function
		return COMMAND_ERROR;
	}
	return n;
}
//int execRST(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values)
//int execTST(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values)

void registerAllCallbacks()
{
	registerCallback(ID_SBSDITHER_STATE, execSBSDitherState);
	registerCallback(ID_SBSDITHER_RATE_HZ, execSBSDitherRate_Hz);
	registerCallback(ID_SBSDITHER_FREQ_HZ, execSBSDitherFreq_Hz);
	registerCallback(ID_CONTROLLEDDITHER_STATE, execControlledDitherState);
	registerCallback(ID_FINE_HZ, execFreqFine_Hz);
	registerCallback(ID_POWERUNIT, execLaserPowerUnit);
	registerCallback(ID_STATE, execLaserState);
	registerCallback(ID_SAFETY, execLaserSafety);
	//registerCallback(ID_RESET, execReset);
	registerCallback(ID_POWER_DBM, execPower_dBm);
	registerCallback(ID_WAVE_M, execWavelength_m);
	registerCallback(ID_FREQ_HZ, execFrequency_Hz);
	registerCallback(ID_GRID_HZ, execGrid_Hz);
	registerCallback(ID_TEMP_C, execTemp_C);
	registerCallback(ID_IDN, execIDN);
	registerCallback(ID_OPC, execOPC);
	registerCallback(ID_OPT, execOPT);
}

