//returns the temperature of the laser in degrees C
// returns negative numbers on error
// 0.0 is invalid

#include "TLSXServerWrappers.h"
#include <stdio.h>
#include <math.h>
#include <string.h>

// These functions send commands to the TLSX Server backend

// takes a double and sends a value in device units for the command
void setDoubleValue(int* e, uint8_t blade, uint8_t laser, double value, S2TCommand command)
{
	spLogDebug(1, "Sending double numeric value to TLSXServer with value: %f, uint32_t: %d, command: %d", value, (uint32_t)value, command);
	#ifndef TLSXSERVER_BACKEND_DISABLED
	int r;
	if((r=sendToTLSXServer(NULL, blade, laser, command, value, instrumentConfig.TLSXConnectionSockfd))!=TLSXSERVER_SUCCESS) {
		e = true;
	}
	#endif
}

// gives back a double value in the device units for that command
double getDoubleValue(int* e, uint8_t blade, uint8_t laser, S2TCommand command)
{
	int32_t temp;
	#ifndef TLSXSERVER_BACKEND_DISABLED
	int r;
	if ((r=sendToTLSXServer((uint32_t*)&temp, blade, laser, command, 0, instrumentConfig.TLSXConnectionSockfd))!=TLSXSERVER_SUCCESS) {
		e = true;
		return 0;
	}
	#else
	switch (command) {
	// power units are in mdBm
		case S2T_GET_MINPOWER:
			temp = 7;		//0.007
			break;
		case S2T_GET_MAXPOWER:
			temp = 13500;	//13.500
			break;
		case S2T_GET_DESPOWER:
			temp = 11500;	//11.500
			break;
		case S2T_GET_ACTPOWER:
			temp = 11500;	//11.500
			break;
	
	// freq units are in MHz
		case S2T_GET_MINFREQ:
			temp = 191500000;	//1.91500000e+14 (191 THz)
			break;
		case S2T_GET_MAXFREQ:
			temp = 196250000;	//1.96250000e+14
			break;
		case S2T_GET_DESFREQ:
			temp = 195840000;	//1.95840000e+14
			break;

	// grid units are in MHz	
		case S2T_GET_MINGRID:
			temp = 100;
			break;
		case S2T_GET_MAXGRID:
			temp = 25000;
			break;
		case S2T_GET_ACTGRID:
			temp = 20000;
			break;
	
	// fine units are in MHz
		case S2T_GET_MINFINE:
			temp = -6000;
			break;
		case S2T_GET_MAXFINE:
			temp = 6000;
			break;
		case S2T_GET_ACTFINE:
			temp = 20;
			break;
			
	// temp units are in centi degrees celsius
		case S2T_GET_TEMP:
			temp = 2312;	// 23.12
			break;
			
	// SBS Rate units are kHz
		case S2T_GET_MINSBSRATE:
			temp = 10;
			break;
		case S2T_GET_MAXSBSRATE:
			temp = 200;
			break;
		case S2T_GET_ACTSBSRATE:
			temp = 150;
			break;
			
	// SBS freq units are in MHz
		case S2T_GET_MINSBSFREQ:
			temp = 1000;
			break;
		case S2T_GET_MAXSBSFREQ:
			temp = 20000;
			break;
		case S2T_GET_ACTSBSFREQ:
			temp = 15000;
			break;
			
		default:
			temp = -1.0;
	}
	#endif
	return (double)temp;
}

// Get/Set state
void setBoolState(int* e, uint8_t blade, uint8_t laser, int on, S2TCommand command)
{
	//int r;
	//int32_t temp;
	if (on) {
		spLogDebug(1, "Sending bool state value \"ON\" to TLSXServer, command: %d", command);
		//temp = STATE_ON_IN;
	} else {
		spLogDebug(1, "Sending bool state value \"OFF\" to TLSXServer, command: %d", command);
		//temp = STATE_OFF_IN;
	}
	#ifndef TLSXSERVER_BACKEND_DISABLED
	if ((r = sendToTLSXServer(NULL, blade, laser, command, temp, instrumentConfig.TLSXConnectionSockfd))!=TLSXSERVER_SUCCESS) {
		e = true;
	}
	#endif
}

int getBoolState(int* e, uint8_t blade, uint8_t laser, S2TCommand command)
{
	#ifndef TLSXSERVER_BACKEND_DISABLED
	uint32_t temp;
	int r;
	r = sendToTLSXServer(&temp, blade, laser, command, 0, instrumentConfig.TLSXConnectionSockfd);
	if (r==TLSXSERVER_SUCCESS) {
		if (temp==STATE_ON_OUT) {
			return true;
		} else if (temp==STATE_OFF_OUT) {
			return false;
		} else {
			spLogError("Invalid Response for getBoolState from TLSX Server: %X", temp);
			e = true;
		}
	}
	e = true;
	return 0;
	#else
	return 1;
	#endif
}

void laserReset(int* e, uint8_t blade, uint8_t laser)	//testing only, resets a laser
{
	spLogDebug(1, "Sending reset laser command to TLSXServer");
	#ifndef TLSXSERVER_BACKEND_DISABLED
	int r;
	if ((r=sendToTLSXServer(NULL, blade, laser, S2T_RESET, 0, instrumentConfig.TLSXConnectionSockfd))!=TLSXSERVER_SUCCESS) {
		e = true;
	}
	#endif
}

uint32_t getuint32(int* e, uint8_t blade, S2TCommand command)
{
	spLogDebug(1, "Getting uint32 from TLSXServer");
	#ifndef TLSXSERVER_BACKEND_DISABLED
	int r;
	uint32_t temp;
	if ((r=sendToTLSXServer(&temp, blade, 0, command, 0, instrumentConfig.TLSXConnectionSockfd))!=TLSXSERVER_SUCCESS) {
		e = true;
	}
	return temp;
	#else
	return 4;
	#endif
}

void get4ByteString(int* e, char* output, uint8_t blade, S2TCommand command)
{
	spLogDebug(1, "Quering 4 byte string from backend");
	strncpy(output, "????", 4);
}

void get32ByteString(int* e, char* output, uint8_t blade, S2TCommand command)
{
	spLogDebug(1, "Quering 32 byte string from backend");
	strncpy(output, "?????????", 32);
}

// GPIB:
int getIDN(int *e, size_t max, char* response)
{
	// companyName,ModelNumber,SerialNumber,HW1.0FW3.5
	size_t n=0;
	n += snprintf(response+n, max-n, "%s,", getCompanyNameString());
	n += snprintf(response+n, max-n, "%s,", getInstrumentModelNumber());
	n += snprintf(response+n, max-n, "%s,", getInstrumentSerialNumber());
	n += snprintf(response+n, max-n, "HW%s", getHWVersionNumber());
	n += snprintf(response+n, max-n, "FW%s", TLSX_VERSION);
	return n;
}

int getIDNSlot(int *e, size_t max, char* response, uint8_t blade)
{
	// companyName,ModelNumber,SerialNumber,HW1.8FW1.6
	size_t n=0;
	char result[32];
	n += snprintf(response+n, max-n, "%s,", getCompanyNameString());
	get32ByteString(e, result, blade, S2T_GET_MODEL_NUMBER);
	n += snprintf(response+n, max-n, "%.32s,", result);
	get32ByteString(e, result, blade, S2T_GET_SERIAL_NUMBER);
	n += snprintf(response+n, max-n, "%.32s,", result);
	get4ByteString(e, result, blade, S2T_GET_HW_VERSION);
	n += snprintf(response+n, max-n, "HW%.4s", result);
	n += snprintf(response+n, max-n, "FW%u", getuint32(e, blade, S2T_GET_FW_MAJOR_VERSION));
	n += snprintf(response+n, max-n, ".%u", getuint32(e, blade, S2T_GET_FW_MINOR_VERSION));
	return n;
}

int getOPC(int* e)
{
	return 1;
}

int getOPCSlot(int* e, uint8_t blade)
{
	return 1;
}

int getOPCLaser(int* e, uint8_t blade, uint8_t laser)
{
	return 1;
}

int getOPT(int *e, size_t max, char* response)
{
	size_t n=0;
	n += snprintf(response+n, max-n, ",,,,,,CSB0001,,");
	return n;
}

int getOPTSlot(int *e, size_t max, char* response, uint8_t blade)
{
	size_t n=0;
	n += snprintf(response+n, max-n, "LAS01,LAS01,LAS01,LAS01");
	return n;
}

