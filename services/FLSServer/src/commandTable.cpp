// This is a temporary home for the most up to date version of the FLS/TLSX 
// command table. At the start of the VOA project (Feb 2014) SCPI has moved 
// to libSCPI and is being used for the VOA. The original SCPIServer and 
// VXI11Server compatible with the FLS/TLSX command set are still in services/
// however they will be ultimately replaced by a combined server which 
// impliments both command sets simultaneously. In the mean time however the 
// FLS/TLSX command set must temporarily be removed from libSCPI to make space 
// for the VOA commands set
// -Ian

// ############################## ArgumentNodes ##############################

// allowable tasks are MIN, MAX, SET, ACTUAL, ALL
const ArgumentNode minMaxActSetableArgs[] = {
{NODE_QUERY,	TASK_NONE,	1, {{false, MIN, "MIN"}}		},
{NODE_QUERY,	TASK_NONE,	1, {{false, MAX, "MAX"}}		},
{NODE_CMD,		TASK_SET,	1, {{false, TASK_NONE, ""}} 	},
{NODE_QUERY,	TASK_NONE,	1, {{true, ACTUAL, "ACT"}}		},	// default task
{NODE_QUERY,	TASK_NONE,	1, {{false, ALL, "ALL"}}		},
{NODE_QUERY,	TASK_NONE,	0, {} } };

// allowable tasks are MIN, MAX, SET, DESIRED, ACTUAL, ALL
const ArgumentNode minMaxDesiredActSetableArgs[] = {
{NODE_QUERY,	TASK_NONE,	1, {{false, MIN, "MIN"}}		},
{NODE_QUERY,	TASK_NONE,	1, {{false, MAX, "MAX"}}		},
{NODE_CMD,		TASK_SET,	1, {{false, TASK_NONE, ""}} 	},
{NODE_QUERY,	TASK_NONE,	1, {{false, DESIRED, "SET"}}	},
{NODE_QUERY,	TASK_NONE,	1, {{true, ACTUAL, "ACT"}}		},	// default task
{NODE_QUERY,	TASK_NONE,	1, {{false, ALL, "ALL"}}		},
{NODE_QUERY,	TASK_NONE,	0, {} } };

// allowable tasks are MIN, MAX, SET, DESIRED, LOCK, ALL
const ArgumentNode minMaxDesiredLockSetableArgs[] = {
{NODE_QUERY,	TASK_NONE,	1, {{false, MIN, "MIN"}}		},
{NODE_QUERY,	TASK_NONE,	1, {{false, MAX, "MAX"}}		},
{NODE_CMD,		TASK_SET,	1, {{false, TASK_NONE, ""}} 	},
{NODE_QUERY,	TASK_NONE,	1, {{true, DESIRED, "SET"}}		},	// default task
{NODE_QUERY,	TASK_NONE,	1, {{false, LOCK, "LOCK"}}		},	// returns TRUE/FALSE
{NODE_QUERY,	TASK_NONE,	1, {{false, ALL, "ALL"}}		},
{NODE_QUERY,	TASK_NONE,	0, {} } };

// allowable tasks are MIN, MAX, ACTUAL, ALL
const ArgumentNode minMaxActArgs[] = {
{NODE_QUERY,	TASK_NONE,	1, {{false, MIN, "MIN"}}		},
{NODE_QUERY,	TASK_NONE,	1, {{false, MAX, "MAX"}}		},
{NODE_QUERY,	TASK_NONE,	1, {{true, ACTUAL, "ACT"}}		},	// default task
{NODE_QUERY,	TASK_NONE,	1, {{false, ALL, "ALL"}}		},
{NODE_QUERY,	TASK_NONE,	0, {} } };

const ArgumentNode stateArgs[] = {
{NODE_CMD,		TASK_NONE,	1, {{false, STATE_ON, "ON"}}	},
{NODE_CMD,		TASK_NONE,	1, {{false, STATE_OFF, "OFF"}}	},
{NODE_QUERY,	TASK_QUERY,	0, {} },
{NODE_QUERY,	TASK_NONE,	0, {} } };

// aliases:
const ArgumentNode* powerArgs = minMaxDesiredActSetableArgs;
const ArgumentNode* wavelengthArgs = minMaxDesiredLockSetableArgs;
const ArgumentNode* frequencyArgs = minMaxDesiredLockSetableArgs;
const ArgumentNode* fineArgs = minMaxActSetableArgs;
const ArgumentNode* gridArgs = minMaxActSetableArgs;
const ArgumentNode* ditherArgs = minMaxActSetableArgs;

// no arguments just command:
const ArgumentNode noCommandArgs[] = {
{NODE_CMD,		TASK_COMMAND,	0, {}	},
{NODE_QUERY,	TASK_NONE,		0, {} 	} };

// no arguments just query:
const ArgumentNode noQueryArgs[] = {
{NODE_QUERY,	TASK_QUERY,	0, {}	},
{NODE_QUERY,	TASK_NONE,	0, {}	} };

// something that has both command and query forms with no arguments
const ArgumentNode noCMDQueryArgs[] = {
{NODE_CMD,		TASK_COMMAND,	0, {}	},
{NODE_QUERY,	TASK_QUERY,		0, {}	},
{NODE_QUERY,	TASK_NONE,		0, {}	} };

const ArgumentNode dateArgs[] = {
{NODE_CMD,		TASK_COMMAND,	3, {{false, TASK_NONE, ""},{false, TASK_NONE, ""},{false, TASK_NONE, ""}}	},
{NODE_QUERY,	TASK_QUERY,		0, {}	},
{NODE_QUERY,	TASK_NONE,		0, {}	} };

const ArgumentNode* timeArgs = dateArgs;

// ############################### HeaderNodes ###############################

// level 5

const HeaderNode sbsChilds[] = {
{ID_SBSDITHER_STATE,	NODE_PLAIN, "STATE", "STATE", NULL, stateArgs, UNIT_NONE},
{ID_SBSDITHER_RATE_HZ, NODE_PLAIN, "RATE", "RATE", NULL, minMaxActArgs, UNIT_HZ},
{ID_SBSDITHER_FREQ_HZ, NODE_PLAIN, "FREQ", "FREQUENCY", NULL, ditherArgs, UNIT_HZ},
{ID_NONE,	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

const HeaderNode controlledDitherChilds[] = {
//{ID_CONTROLLEDDITHER_STATE,	NODE_PLAIN, "STATE", "STATE", NULL, stateArgs, UNIT_NONE},
{ID_NONE,	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

// level 4 
const HeaderNode frequencyChilds[] = {
{ID_FINE_HZ,	NODE_PLAIN, "FINE", "FINE", NULL, fineArgs, UNIT_HZ},
{ID_NONE,		NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

// level 3
const HeaderNode operationChilds[] = {
//{ID_OP_EVENT,	NODE_PLAIN, "EVEN", "EVENT", NULL, noQueryArgs, UNIT_NONE},
//{ID_OP_COND,		NODE_PLAIN, "COND", "CONDITION", NULL, noQueryArgs, UNIT_NONE},
//{ID_OP_ENABLE,	NODE_PLAIN, "ENAB", "ENABLE", NULL, noCMDQueryArgs, UNIT_NONE},
{ID_NONE,	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

const HeaderNode questionableChilds[] = {
//{ID_QUE_EVENT,	NODE_PLAIN, "EVEN", "EVENT", NULL, noQueryArgs, UNIT_NONE},
//{ID_QUE_COND,		NODE_PLAIN, "COND", "CONDITION", NULL, noQueryArgs, UNIT_NONE},
//{ID_QUE_ENABLE,	NODE_PLAIN, "ENAB", "ENABLE", NULL, noCMDQueryArgs, UNIT_NONE},
{ID_NONE,	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

HeaderNode outputPowerChilds[] = {
{ID_POWERUNIT,	NODE_PLAIN, "UNIT", "UNIT", NULL, noQueryArgs, UNIT_NONE},
{ID_NONE,	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

HeaderNode outputChannelChilds[] = {
{ID_NONE,	NODE_PLAIN, "POW", "POWER", outputPowerChilds, NULL, UNIT_NONE},
{ID_STATE,	NODE_PLAIN, "STATE", "STATE", NULL, stateArgs, UNIT_NONE},
{ID_SAFETY,	NODE_PLAIN, "SAFETY", "SAFETY", NULL, stateArgs, UNIT_NONE},
{ID_NONE,	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

const HeaderNode sourceChannelChilds[] = {
{ID_RESET,		NODE_PLAIN, "RESET", "RESET", NULL, noCommandArgs, UNIT_NONE},	//TODO: testing only, resets a laser
{ID_POWER_DBM,	NODE_PLAIN, "POW", "POWER", NULL, powerArgs, UNIT_DBM},
{ID_WAVE_M,		NODE_PLAIN, "WAV", "WAVELENGTH", NULL, wavelengthArgs, UNIT_M},
{ID_FREQ_HZ,	NODE_PLAIN, "FREQ", "FREQUENCY", frequencyChilds, frequencyArgs, UNIT_HZ},
{ID_GRID_HZ,	NODE_PLAIN, "GRID", "GRID", NULL, gridArgs, UNIT_HZ},
{ID_SBSDITHER_STATE, 			NODE_PLAIN, "SBS", "SBS", sbsChilds, stateArgs, UNIT_NONE},
{ID_CONTROLLEDDITHER_STATE, 	NODE_PLAIN, "DITH", "DITHER", controlledDitherChilds, stateArgs, UNIT_NONE},
{ID_TEMP_C,		NODE_PLAIN, "TEMP", "TEMPERATURE", NULL, noQueryArgs, UNIT_C},
{ID_NONE,		NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

// level 2
const HeaderNode systemChilds[] = {
//{ID_ERROR,	NODE_PLAIN, "ERR", "ERROR", NULL, noQueryArgs, UNIT_NONE},
{ID_VERS,	NODE_PLAIN, "VERS", "VERSION", NULL, noQueryArgs, UNIT_NONE},
/*{ID_DATE,	NODE_PLAIN, "DATE", "DATE", NULL, dateArgs, UNIT_NONE},
{ID_TIME,	NODE_PLAIN, "TIME", "TIME", NULL, timeArgs, UNIT_NONE},*/
{ID_NONE,	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

const HeaderNode slotChilds[] = {
//{ID_OPC,	NODE_PLAIN, "OPC", "OPC", NULL, noCMDQueryArgs, UNIT_NONE},
{ID_OPC,	NODE_PLAIN, "OPC", "OPC", NULL, noQueryArgs, UNIT_NONE},
{ID_OPT,	NODE_PLAIN, "OPT", "OPTIONS", NULL, noQueryArgs, UNIT_NONE},
//{ID_TST,	NODE_PLAIN, "TST", "TST", NULL, noQueryArgs, UNIT_NONE},
{ID_IDN,	NODE_PLAIN, "IDN", "IDN", NULL, noQueryArgs, UNIT_NONE},
{ID_NONE,	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

const HeaderNode statusChilds[] = {
//{ID_PRES,	NODE_PLAIN, "PRES", "PRESET", NULL, noCommandArgs, UNIT_NONE},
{ID_NONE,	NODE_PLAIN, "OPER", "OPERATION", operationChilds, NULL, UNIT_NONE},
{ID_NONE,	NODE_PLAIN, "QUES", "QUESTIONABLE", questionableChilds, NULL, UNIT_NONE},
{ID_NONE,	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

const HeaderNode outputChilds[] = {
{ID_NONE,	NODE_DIGIT, "CHAN", "CHANNEL", outputChannelChilds, NULL, UNIT_NONE},
{ID_NONE,	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

const HeaderNode sourceChilds[] = {
{ID_NONE,	NODE_DIGIT, "CHAN", "CHANNEL", sourceChannelChilds, NULL, UNIT_NONE},
{ID_NONE, 	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

// level 1
const HeaderNode root[] = {
// GPIB:
{ID_CLS,	NODE_PLAIN, "*CLS", "*CLS", NULL, noCommandArgs, UNIT_NONE},
//{ID_ESE,	NODE_PLAIN, "*ESE", "*ESE", NULL, esrArgs, UNIT_NONE},
{ID_ESR,	NODE_PLAIN, "*ESR", "*ESR", NULL, noQueryArgs, UNIT_NONE},
{ID_IDN,	NODE_PLAIN, "*IDN", "*IDN", NULL, noQueryArgs, UNIT_NONE},
//{ID_OPC,	NODE_PLAIN, "*OPC", "*OPC", NULL, opc args, UNIT_NONE},
{ID_OPC,	NODE_PLAIN, "*OPC", "*OPC", NULL, noQueryArgs, UNIT_NONE},
{ID_OPT,	NODE_PLAIN, "*OPT", "*OPT", NULL, noQueryArgs, UNIT_NONE},
//{ID_RST,	NODE_PLAIN, "*RST", "*RST", NULL, noCommandArgs, UNIT_NONE},
{ID_STB,	NODE_PLAIN, "*STB", "*STB", NULL, noQueryArgs, UNIT_NONE},
//{ID_SRE,	NODE_PLAIN, "*SRE", "*SRE", NULL, noQueryArgs, UNIT_NONE},
//{ID_TST,	NODE_PLAIN, "*TST", "*TST", NULL, noQueryArgs, UNIT_NONE},
// SCPI:
{ID_NONE,	NODE_PLAIN, "SYST", "SYSTEM", systemChilds, NULL, UNIT_NONE},
{ID_NONE,	NODE_DIGIT, "SLOT", "SLOT", slotChilds, NULL, UNIT_NONE},
{ID_NONE,	NODE_DIGIT, "STAT", "STATUS", statusChilds, NULL, UNIT_NONE},
{ID_NONE,	NODE_DIGIT, "OUTP", "OUTPUT", outputChilds, NULL, UNIT_NONE}, 
{ID_NONE,	NODE_DIGIT, "SOUR", "SOURCE", sourceChilds, NULL, UNIT_NONE},
{ID_NONE,	NODE_PLAIN, NULL, NULL, NULL, NULL, UNIT_NONE} };

// ############################### Hardcoded config from backendCallbackTable.h ###############################

typedef enum {
//Commands:
	TASK_NONE = 0,		// encodes unset task in arguments
	TASK_COMMAND,
	STATE_ON,
	STATE_OFF,
	TASK_SET,
//Queries:
	TASK_QUERY,
	MIN,
	MAX,
	DESIRED,
	LOCK,
	ACTUAL,
	ALL
} TaskID;

typedef enum {
	ID_NONE = 0, // encodes end of command table
// Dither
	ID_SBSDITHER_STATE,
	ID_SBSDITHER_RATE_HZ,
	ID_SBSDITHER_FREQ_HZ,
	ID_CONTROLLEDDITHER_STATE,
// Source:Frequency
	ID_FINE_HZ,
// Operation
	ID_OP_EVENT,
	ID_OP_COND,
	ID_OP_ENABLE,
// Questionable
	ID_QUE_EVENT,
	ID_QUE_COND,
	ID_QUE_ENABLE,
// Output:Power
	ID_POWERUNIT,
// Output
	ID_STATE,
	ID_SAFETY,
// Source
	ID_RESET,	//testing only, resets a laser
	ID_POWER_DBM,
	ID_WAVE_M,
	ID_FREQ_HZ,
	ID_GRID_HZ,
	ID_TEMP_C,
// System
	ID_ERROR,
	ID_VERS,
	ID_DATE,
	ID_TIME,
// Status
	ID_PRES,
// GPIB:
	ID_CLS,
	ID_ESE,
	ID_ESR,
	ID_IDN,
	ID_OPC,
	ID_OPT,
	ID_RST,
	ID_STB,
	ID_TST
} BackendCallbackID;


static TableEntry callbackTable[] = {
// Dither
	{ID_SBSDITHER_STATE, execSBSDitherState},
	{ID_SBSDITHER_RATE_HZ, execSBSDitherRate_Hz},
	{ID_SBSDITHER_FREQ_HZ, execSBSDitherFreq_Hz},
	{ID_CONTROLLEDDITHER_STATE, execControlledDitherState},
// Source:Frequency
	{ID_FINE_HZ, execFreqFine_Hz},
// Operation
	{ID_OP_EVENT,	NULL},
	{ID_OP_COND,	NULL},
	{ID_OP_ENABLE,	NULL},
// Questionable
	{ID_QUE_EVENT,	NULL},
	{ID_QUE_COND,	NULL},
	{ID_QUE_ENABLE,	NULL},
// Output:Power
	{ID_POWERUNIT, execLaserPowerUnit},
// Output
	{ID_STATE, execLaserState},
	{ID_SAFETY, execLaserSafety},
// Source
	{ID_RESET, NULL},	//execReset},	//testing only, resets a laser
	{ID_POWER_DBM, execPower_dBm},
	{ID_WAVE_M, execWavelength_m},
	{ID_FREQ_HZ, execFrequency_Hz},
	{ID_GRID_HZ, execGrid_Hz},
	{ID_TEMP_C, execTemp_C},
// System
	{ID_ERROR, NULL},	//	execError},
	{ID_VERS, execVersion},
	{ID_DATE, NULL},
	{ID_TIME, NULL},
// Status
	{ID_PRES, NULL},
// GPIB:
	{ID_CLS, NULL},	// handled as special case
	{ID_ESE, NULL},	// handled as special case
	{ID_ESR, NULL},	// handled as special case
	{ID_IDN, execIDN},
	{ID_OPC, execOPC},
	{ID_OPT, execOPT},
	{ID_RST, NULL},	//	execRST}, // TODO, handled as special case but also called here
	{ID_STB, NULL},	// handled as special case
	{ID_TST, NULL},	//	execTST},
	{ID_NONE, NULL}
}
