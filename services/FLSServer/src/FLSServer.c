/**
 * @file FLSServer.c
 * @brief Test usage of the libSCPI library to impliment the FLS/TLSX command set
 * Note that this will only work with a certain version of libSCPI, before it is
 * modified to work with the VOA command set. Certain parts of libSCPI are 
 * currently hardcoded for FLS and will be hardcoded for VOA for a short time.
 * Ultimately this will be changable at runtime based on the type of blade
 * Things which are currently hardcoded:
 * backendServerTable.h:
 *		enum TaskID
 *		enum BackendCallbackID
 * commandTable.cpp:
 *		all the HeaderNode arrays
 *		all the ArgumentNode arrays
**/

#include "libSCPI.h"
#include "common.h"
#include "logging.h"
#include "singleton.h"
#include "backendCallbacks.h"
#include <string.h>
#include <stdlib.h>

#define SCPI_DAEMON_NAME "TLSX_FLSServer"
#define SCPI_PID_FILE "/tmp/FLSServer.pid"

int main(int argc, char **argv)
{
	int verbosity = 0;
	if ((argc>=3)&&(strcmp("-v", argv[1])==0)) {
		verbosity = atoi(argv[2]);
		if(verbosity<0) {
			verbosity = 0;
		}
	}
	spOpenLog(SCPI_DAEMON_NAME, verbosity);
	if (getSingletonLock(SCPI_PID_FILE)<0) {
		spLogError("Another instance of %s is already running, aborting\n", SCPI_DAEMON_NAME);
		fprintf(stdout, "Another instance of %s is already running, aborting\n", SCPI_DAEMON_NAME);
		exit(1);
	}
	
	// register callback functions
	registerAllCallbacks();
	
	// starts the RPC server to recieve SCPI commands over VXI11
	startSCPI();
	// Only returns on fatal errors	
	return 1;
}
