#ifndef BACKENDCALLBACKS_H
#define BACKENDCALLBACKS_H

#include "TLSXServerWrappers.h"
#include "libSCPI.h"
#include "stdint.h"
#include <stdio.h>

#define MAX_NUM_BLADES 16
#define MAX_NUM_LASERS 4

#define FLOAT_PRECISION_FREQ	8
#define FLOAT_PRECISION_POW		3
#define FLOAT_PRECISION_GRID	6
#define FLOAT_PRECISION_WAV		6
#define FLOAT_PRECISION_SBSRATE 3

// Dither
int execSBSDitherState(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values);
int execSBSDitherRate_Hz(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values);
int execSBSDitherFreq_Hz(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values);
int execControlledDitherState(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values);

// Source:Frequency
int execFreqFine_Hz(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values);

// Output:Power
int execLaserPowerUnit(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values);

// Output
int execLaserState(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values);
int execLaserSafety(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values);

// Source
//testing only, resets a laser
//int execReset(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values);
int execPower_dBm(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values);
int execWavelength_m(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values);
int execFrequency_Hz(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values);
int execGrid_Hz(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values);
int execTemp_C(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values);

// GPIB:
int execIDN(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values);
int execOPC(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values);
int execOPT(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values);
//int execRST(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values);
//int execTST(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values);

// registers all the callbacks that are implimented by this file
void registerAllCallbacks();

#endif //BACKENDCALLBACKS_H
