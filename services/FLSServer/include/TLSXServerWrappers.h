#ifndef TLSX_SERVER_WRAPPERS_H
#define TLSX_SERVER_WRAPPERS_H

#define TLSXSERVER_BACKEND_DISABLED

#include "libSCPI.h"
#include "version.h"
#include <stdint.h>
#include <stdlib.h>

// any errors are taken care of inside these functions
// This includes setting the error and status bytes, and adding errors to the error queue
// the error bool passed to these functions will be set if the command failed
// otherwise the value is untouched

// sanity check values:
#define MAX_TEMP 80
#define MIN_TEMP 5

// temporary to make the compiler stop complaining about undefined names
// Was originally in TLSXServerTCP.hpp
typedef enum {
	S2T_ECHO = 0,
	S2T_GET_MODEL = 1,
	S2T_GET_FW_VERSION = 2,
	S2T_GET_BLADE_STATUS = 3,
	
	// power units are in mdBm
	S2T_GET_MINPOWER = 4,	//getDoubleValue
	S2T_GET_MAXPOWER,		//getDoubleValue
	S2T_SET_POWER,			//setDoubleValue
	S2T_GET_DESPOWER,		//getDoubleValue
	S2T_GET_ACTPOWER,		//getDoubleValue
	
	// freq units are in MHz
	S2T_GET_MINFREQ = 9,	//getDoubleValue
	S2T_GET_MAXFREQ,		//getDoubleValue
	S2T_SET_FREQ,			//setDoubleValue
	S2T_GET_DESFREQ,		//getDoubleValue
	S2T_GET_LOCKFREQ = 13,	//getDoubleValue

	// grid units are in MHz	
	S2T_GET_MINGRID = 14,	//getDoubleValue
	S2T_GET_MAXGRID,		//getDoubleValue
	S2T_SET_GRID,			//setDoubleValue
	S2T_GET_ACTGRID,		//getDoubleValue
	
	// fine units are in MHz
	S2T_GET_MINFINE = 18,	//getDoubleValue
	S2T_GET_MAXFINE,		//getDoubleValue
	S2T_SET_FINE,			//setDoubleValue
	S2T_GET_ACTFINE,		//getDoubleValue
	
	// on/off
	S2T_SET_STATE_CONTROL_DITHER = 22,
	S2T_GET_STATE_CONTROL_DITHER,
	
	// temp units are in degrees CC
	S2T_GET_TEMP = 24,
	
	// SBS Rate units are kHz
	S2T_GET_MINSBSRATE = 25,	//getDoubleValue
	S2T_GET_MAXSBSRATE,		//getDoubleValue
	S2T_SET_SBSRATE,		//setDoubleValue
	S2T_GET_ACTSBSRATE,		//getDoubleValue
	
	// SBS freq untis are in MHz
	S2T_GET_MINSBSFREQ = 29,	//getDoubleValue
	S2T_GET_MAXSBSFREQ,		//getDoubleValue
	S2T_SET_SBSFREQ,		//setDoubleValue
	S2T_GET_ACTSBSFREQ,		//getDoubleValue
	
	// SBS ampl units are percent (unitless)
	S2T_GET_MINSBSAMP = 33,	//getDoubleValue
	S2T_GET_MAXSBSAMP,		//getDoubleValue
	S2T_SET_SBSAMP,			//setDoubleValue
	S2T_GET_ACTSBSAMP,		//getDoubleValue
	
	//on/off
	S2T_SET_STATE_LASER = 37,
	S2T_GET_STATE_LASER = 38,
	
	// data = 0
	S2T_RESET = 39,
	
	// number, see doc
	S2T_GET_LASERSTATE = 40,
	
	// on/off
	S2T_SET_STATE_SBS = 41,
	S2T_GET_STATE_SBS,

	S2T_GET_FLASH_VERSION = 45,		//get4ByteString
	S2T_SET_FLASH_VERSION = 46,
	S2T_GET_SERIAL_NUMBER = 47,		//get32ByteString
	S2T_SET_SERIAL_NUMBER = 48,
	S2T_GET_MODEL_NUMBER = 49,		//get32ByteString
	S2T_SET_MODEL_NUMBER = 50,
	S2T_GET_FW_MAJOR_VERSION = 51,	//get4ByteString
	S2T_GET_FW_MINOR_VERSION = 52,	//get4ByteString
	S2T_GET_HW_VERSION = 53,		//get4ByteString
	S2T_SET_HW_VERSION = 54,
	S2T_GET_NUM_LASERS = 55,		//getInteger
	S2T_SET_NUM_LASERS = 56,
	S2T_COMMIT_ST = 57,

	S2T_SET_SAFETY = 110,			//execLaserSafety
	S2T_GET_SAFETY = 111,

	S2T_GET_OPC_BLADES = 128,		//getOPC
	S2T_GET_OPC_LASERS = 129,		//getOPCSlot
	S2T_GET_OPC_LASER = 130,		//getOPCLaser
	S2T_GET_OPT_BLADES = 131,		//getOPT
	S2T_GET_OPT_LASERS = 132		//getOPTSlot
} S2TCommand;

#define STATE_ON_IN		(0x00000001)
#define STATE_OFF_IN	(0x00000100)
#define STATE_OFF_OUT	(0x00000100)
#define STATE_ON_OUT	(0x00000001)

// takes a double and sends a value in device units for the command
void setDoubleValue(int* e, uint8_t blade, uint8_t laser, double value, S2TCommand command);

// gives back a double value in the device units for that command
double getDoubleValue(int* e, uint8_t blade, uint8_t laser, S2TCommand command);

// Get/Set state
void setBoolState(int* e, uint8_t blade, uint8_t laser, int on, S2TCommand command);
int getBoolState(int* e, uint8_t blade, uint8_t laser, S2TCommand command);

// laserReset
void laserReset(int* e, uint8_t blade, uint8_t laser);

// GPIB:
int getIDN(int *e, size_t max, char* response);
int getIDNSlot(int *e, size_t max, char* response, uint8_t blade);

int getOPC(int* e);
int getOPCSlot(int* e, uint8_t blade);
int getOPCLaser(int* e, uint8_t blade, uint8_t laser);

int getOPT(int *e, size_t max, char* response);
int getOPTSlot(int *e, size_t max, char* response, uint8_t blade);

#endif //TLSX_SERVER_WRAPPERS_H
