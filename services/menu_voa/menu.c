#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <ncurses.h>
#include <menu.h>
#include <math.h>
#include <sched.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <syslog.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <iface.h>

// from vix_local
#include <voa_commands.h>

#include "menu_fd.h"

#define ONE_MS (1)
#define ONE_SEC (1000*ONE_MS)

#define xFAKE_VXI
#define BLADE_ID (15)

#define NUM_CHANNELS (4)	//maximum number

#define DEFAULT_IDLE_TIMEOUT (10.0)
#define NET_IDLE_TIMEOUT (30.0)

// this applies to mW or dBm mode, depending on which mode is in use
// remember that power is set as an integer in mdBm in hardware
// This means that the actual value in mdBm will be less precise than the
// displayed value for values over about 40mW
#define POWER_STEP_SIZE (0.01)
// wavelength step size in nm
#define WAVE_STEP_SIZE (1)

static char mwd[256];

enum power_units_t
{
	power_units_dbm,
	power_units_mw,
};

enum control_mode_t
{
	mode_atten = 0,	// fixed attenuation
	mode_power 		// output power tracking
};

enum attenuation_mode_t
{
	mode_offset = 0,	// attenuation = relative_atten + insertion_loss + attenuation_offset	(actual attenuation of user system)
	mode_absolute = 1,	// attenuation = relative_atten + insertion_loss						(actual attenuation of unit)
	mode_relative = 2	// attenuation = relative_atten											(min attenuation = 0)
};

enum atten_display_mode_t
{
	disp_atten = 0,	// display actual attenuation when in attenuation mode
	disp_power		// display actual power when in attenuation mode
};

enum chan_display_mode_t
{
	disp_combined = 0,	// display all channels at the same time
	disp_individual
};

struct tlsx_info_t
{
	struct channel_t
	{
		double insertion_loss;
		double atten_min, atten_max, atten_default, atten_actual;
		double atten_set_point;
		bool atten_set_point_dirty;
		bool show_atten_set_point;
		bool atten_busy;

		double power_min, power_max, power_default, power_actual;
		double power_set_point;
		bool power_set_point_dirty;
		bool show_power_set_point;
		bool power_busy;

		double attenuation_offset;
		bool attenuation_offset_dirty;
		bool show_attenuation_offset;

		unsigned int wave_list_len;
		unsigned int wave_list[50];
		unsigned int wave_set_point;
		bool wave_set_point_dirty;
		bool show_wave_set_point;

		enum attenuation_mode_t attenuation_mode;
		bool attenuation_mode_dirty;
		bool setting_attenuation_mode;

		enum control_mode_t control_mode;
		bool control_mode_dirty;
		bool setting_control_mode;

		bool busy;
	} channel[NUM_CHANNELS];

	int channel_mask;

	enum power_units_t power_units;
	enum atten_display_mode_t atten_display_mode;
	enum chan_display_mode_t chan_display_mode;
	int selected_channel;

	pthread_t vxi_thread;
	pthread_mutex_t tlsx_info_mutex;
	bool vxi_event;
	bool vxi_error;
} tlsx_info;

struct menu_info_t;

struct event_ret_t
{
	int code; // return code
	unsigned int flags;
};


// if ret.code < 0 this is how far down the menu stack to escape, ie
// to escape to the previous menu code times, to  escape to the start
// return ret.code=-BIG_NUMBER, where BIG_NUMBER >= menu stack depth;
// else return 0 to have the menu process the signal normally, or > 0
// to indicate the callback handled this.
// Also the flag field is for meta flags ie MENU_REPOST_HANDLED which
// tells the menu not to redraw itself as we have already done it in a
// special way.
#define BIG_NUMBER (100)
#define MENU_REPOST_HANDLED (1)
typedef struct event_ret_t (*menu_event_cb_t)(int param, struct menu_info_t *info, enum menu_read_result event);

struct event_ret_t main_menu_timeout(struct menu_info_t *info);

struct menu_item_t
{
	char name[32];
	char desc[32];
	bool skip;
	menu_event_cb_t cb;
	int cb_param;
};

static struct mfpio_t *mfpio;

#define VXI_POLL_TIME (50000) // in us
void * vxi_poll(void *ptr)
{
	int l;
	struct vxi_info_t
	{
		struct channel_t
		{
			double insertion_loss;
			double atten_min, atten_max, atten_default, atten_actual;
			double atten_set_point;
			bool atten_set_point_dirty;

			double power_min, power_max, power_default, power_actual;
			double power_set_point;
			bool power_set_point_dirty;

			double attenuation_offset;
			bool attenuation_offset_dirty;

			unsigned int wave_list_len;
			unsigned int wave_list[50];
			unsigned int wave_set_point;
			bool wave_set_point_dirty;

			enum attenuation_mode_t attenuation_mode;
			bool attenuation_mode_dirty;

			enum control_mode_t control_mode;
			bool control_mode_dirty;

		} channel[NUM_CHANNELS];

		uint8_t channel_mask;
		bool vxi_error;
	} vxi_info;

	memset(&vxi_info, 0, sizeof(vxi_info));

	#ifdef FAKE_VXI
	vxi_info.channel_mask = 0xF;
	for (l = 0; l < NUM_CHANNELS; l++)
	{
		vxi_info.channel[l].insertion_loss = 0.51;
		vxi_info.channel[l].atten_min = 0.2;
		vxi_info.channel[l].atten_max = 25.5;
		vxi_info.channel[l].atten_default = 3.6;
		vxi_info.channel[l].atten_set_point = 10.3;
		vxi_info.channel[l].atten_actual = 10.3;

		vxi_info.channel[l].power_min = -5.8;
		vxi_info.channel[l].power_max = 13.6;
		vxi_info.channel[l].power_default = 10.2;
		vxi_info.channel[l].power_set_point = 12.5;
		vxi_info.channel[l].power_actual = 12.5;

		vxi_info.channel[l].attenuation_offset = 0.0;

		vxi_info.channel[l].wave_list_len = 0;
		vxi_info.channel[l].wave_set_point = 150.2;

		vxi_info.channel[l].control_mode = mode_atten;
		vxi_info.channel[l].attenuation_mode = mode_absolute;
	}
	vxi_info.channel[0].control_mode = mode_power;
	vxi_info.channel[2].control_mode = mode_power;
	#endif

	while (1)
	{
		vxi_stat r;
		bool vxi_scan_success = false;

		#ifdef FAKE_VXI
		for (l = 0; l < NUM_CHANNELS; l++)
		{
			if ((vxi_info.channel_mask >> l) & 0x01)
			{
				struct channel_t *channel = &vxi_info.channel[l];

				if (channel->power_set_point_dirty) {
					usleep(100000.0);
					channel->power_set_point_dirty = 0;
				}

				if (channel->atten_set_point_dirty) {
					usleep(100000.0);
					channel->atten_set_point_dirty = 0;
				}

				if (channel->attenuation_offset_dirty) {
					usleep(100000.0);
					vxi_info.channel[l].attenuation_offset_dirty = 0;
				}

				if (channel->wave_set_point_dirty) {
					usleep(100000.0);
					channel->wave_set_point_dirty = 0;
				}

				if (channel->control_mode_dirty) {
					usleep(100000.0);
					channel->control_mode_dirty = 0;
				}

				if (channel->attenuation_mode_dirty) {
					usleep(100000.0);
					channel->attenuation_mode_dirty = 0;
				}

				// fake a gradual move to the set point
				channel->power_actual =
						10.0 / 11.0 * (channel->power_set_point/10.0
						+ channel->power_actual);
				channel->atten_actual =
						10.0 / 11.0 * (channel->atten_set_point/10.0
						+ channel->atten_actual);
			}
		}
		vxi_scan_success = true;
		#else
		// populate the vxi_info

		if (vxi_info.channel_mask == 0) {  // only update this on init
			//check that there is something to talk to
			char modelNumber[128];
			r = vxi_get_opt(modelNumber);
			if(r != VXI_SUCCESS)
				goto vxi_scan_done;
			if (strncmp("ERR", modelNumber, 3)==0) {
				vxi_info.vxi_error = true;
				goto vxi_scan_done;
			}

			r = vxi_get_channel_mask(BLADE_ID, &vxi_info.channel_mask);
			if(r != VXI_SUCCESS)
				goto vxi_scan_done;

			for (l = 0; l < NUM_CHANNELS; l++) {
				if ((vxi_info.channel_mask >> l) & 0x01) {
					r = vxi_get_voa_insertion_loss(BLADE_ID, l+1, &vxi_info.channel[l].insertion_loss);
					if(r != VXI_SUCCESS) {
						vxi_info.channel_mask = 0;	// reset and start again
						goto vxi_scan_done;
					}
				}
			}
		}
		for (l = 0; l < NUM_CHANNELS; l++) {
			if ((vxi_info.channel_mask >> l) & 0x01) {
				struct channel_t *channel = &vxi_info.channel[l];

				// set attenuation_mode
				if (channel->attenuation_mode_dirty) {
						r = vxi_set_voa_attenuation_mode(BLADE_ID, l+1, (int)channel->attenuation_mode);
						if (r != VXI_SUCCESS)
							goto vxi_scan_done;
						channel->attenuation_mode_dirty = 0;
				}
				// get attenuation_mode
				r = vxi_get_voa_attenuation_mode(BLADE_ID, l+1, (int*)&channel->attenuation_mode);
				if (r != VXI_SUCCESS)
					goto vxi_scan_done;

				// set attenuation offset
				if (channel->attenuation_offset_dirty) {
					r = vxi_set_voa_attenuation_offset(BLADE_ID, l+1, channel->attenuation_offset);
					if (r != VXI_SUCCESS)
						goto vxi_scan_done;
					channel->attenuation_offset_dirty = 0;
				}
				// get attenuation offset
				r = vxi_get_voa_attenuation_offset(BLADE_ID, l+1, &channel->attenuation_offset);
				if (r != VXI_SUCCESS)
					goto vxi_scan_done;

				// set control_mode
				if (channel->control_mode_dirty) {
						r = vxi_set_voa_control_mode(BLADE_ID, l+1, (int)channel->control_mode);
						if (r != VXI_SUCCESS)
							goto vxi_scan_done;
						channel->control_mode_dirty = 0;
				}
				// get control_mode
				r = vxi_get_voa_control_mode(BLADE_ID, l+1, (int*)&channel->control_mode);
				if (r != VXI_SUCCESS)
					goto vxi_scan_done;

				// set attenuation
				if (channel->atten_set_point_dirty) {
					r = vxi_set_voa_attenuation(BLADE_ID, l+1, channel->atten_set_point);
					if (r != VXI_SUCCESS)
						goto vxi_scan_done;
					channel->atten_set_point_dirty = 0;
				}
				// get attenuation
				r = vxi_get_voa_attenuation(BLADE_ID, l+1, &channel->atten_min,
							&channel->atten_max, &channel->atten_default,
							&channel->atten_set_point, &channel->atten_actual);
				if (r != VXI_SUCCESS)
					goto vxi_scan_done;

				// set power
				if (channel->power_set_point_dirty) {
					r = vxi_set_voa_power(BLADE_ID, l+1, channel->power_set_point);
					if (r != VXI_SUCCESS)
						goto vxi_scan_done;
					channel->power_set_point_dirty = 0;
				}
				// get power
				r = vxi_get_voa_power(BLADE_ID, l+1, &channel->power_min,
							&channel->power_max, &channel->power_default,
							&channel->power_set_point, &channel->power_actual);
				if (r != VXI_SUCCESS)
					goto vxi_scan_done;

				// set wavelength
				if (channel->wave_set_point_dirty) {
					r = vxi_set_voa_wavelength(BLADE_ID, l+1, channel->wave_set_point);
					if (r != VXI_SUCCESS)
						goto vxi_scan_done;
					channel->wave_set_point_dirty = 0;
				}
				// get wavelength
				r = vxi_get_voa_wavelength(BLADE_ID, l+1, &channel->wave_set_point,
							channel->wave_list, &channel->wave_list_len);
				if (r != VXI_SUCCESS)
					goto vxi_scan_done;
			}
		}
		vxi_scan_success = true;

vxi_scan_done:
		#endif

		if (vxi_scan_success)
		{
			pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);

			// populate the tlsx_info from the vxi_info
			tlsx_info.channel_mask = vxi_info.channel_mask;

			for (l = 0; l < NUM_CHANNELS; l++)
			{
				if ((tlsx_info.channel_mask >> l) & 0x01)
				{
					// if the user just set this, then send it to the device
					if ((!tlsx_info.channel[l].setting_control_mode)&&tlsx_info.channel[l].control_mode_dirty) {
						vxi_info.channel[l].control_mode_dirty = 1;
						tlsx_info.channel[l].control_mode_dirty = 0;
						vxi_info.channel[l].control_mode = tlsx_info.channel[l].control_mode;
					} else if (!tlsx_info.channel[l].setting_control_mode) {
						// otherwise respect any changes from the device, as long as they aren't editing it
						tlsx_info.channel[l].control_mode = vxi_info.channel[l].control_mode;
					}

					tlsx_info.channel[l].insertion_loss = vxi_info.channel[l].insertion_loss;
					if (!tlsx_info.channel[l].setting_attenuation_mode && !tlsx_info.channel[l].attenuation_mode_dirty) {
						// if someone is editing the attenuation_mode, then hold off on updating these
						tlsx_info.channel[l].atten_min = vxi_info.channel[l].atten_min;
						tlsx_info.channel[l].atten_max = vxi_info.channel[l].atten_max;
						tlsx_info.channel[l].atten_default = vxi_info.channel[l].atten_default;
						tlsx_info.channel[l].atten_actual = vxi_info.channel[l].atten_actual;
					}
					tlsx_info.channel[l].power_min = vxi_info.channel[l].power_min;
					tlsx_info.channel[l].power_max = vxi_info.channel[l].power_max;
					tlsx_info.channel[l].power_default = vxi_info.channel[l].power_default;
					tlsx_info.channel[l].power_actual = vxi_info.channel[l].power_actual;

					int w;
					tlsx_info.channel[l].wave_list_len = vxi_info.channel[l].wave_list_len;
					for (w = 0; w<vxi_info.channel[l].wave_list_len; ++w) {
						tlsx_info.channel[l].wave_list[w] = vxi_info.channel[l].wave_list[w];
					}

					// as long as the set point isn't being adjusted,
					// then we can send it to the hardware
					if (!tlsx_info.channel[l].setting_attenuation_mode && !tlsx_info.channel[l].attenuation_mode_dirty) {
						// if someone is editing the attenuation_mode, then hold off on updating these
						if ((!tlsx_info.channel[l].show_atten_set_point)&&tlsx_info.channel[l].atten_set_point_dirty) {
							vxi_info.channel[l].atten_set_point_dirty = 1;
							tlsx_info.channel[l].atten_set_point_dirty = 0;
							vxi_info.channel[l].atten_set_point = tlsx_info.channel[l].atten_set_point;
						} else if (!tlsx_info.channel[l].show_atten_set_point) {
							// or if we aren't sending it to the hardware, then update our setpoint
							tlsx_info.channel[l].atten_set_point = vxi_info.channel[l].atten_set_point;
						}
					}
					if ((!tlsx_info.channel[l].show_power_set_point)&&tlsx_info.channel[l].power_set_point_dirty) {
						vxi_info.channel[l].power_set_point_dirty = 1;
						tlsx_info.channel[l].power_set_point_dirty = 0;
						vxi_info.channel[l].power_set_point = tlsx_info.channel[l].power_set_point;
					} else if (!tlsx_info.channel[l].show_power_set_point) {
						tlsx_info.channel[l].power_set_point = vxi_info.channel[l].power_set_point;
					}

					if ((!tlsx_info.channel[l].show_attenuation_offset)&&tlsx_info.channel[l].attenuation_offset_dirty) {
						vxi_info.channel[l].attenuation_offset_dirty = 1;
						tlsx_info.channel[l].attenuation_offset_dirty = 0;
						vxi_info.channel[l].attenuation_offset = tlsx_info.channel[l].attenuation_offset;
					} else if (!tlsx_info.channel[l].show_attenuation_offset) {
						tlsx_info.channel[l].attenuation_offset = vxi_info.channel[l].attenuation_offset;
					}

					if ((!tlsx_info.channel[l].show_wave_set_point)&&tlsx_info.channel[l].wave_set_point_dirty) {
						vxi_info.channel[l].wave_set_point_dirty = 1;
						tlsx_info.channel[l].wave_set_point_dirty = 0;
						vxi_info.channel[l].wave_set_point = tlsx_info.channel[l].wave_set_point;
					} else if (!tlsx_info.channel[l].show_wave_set_point) {
						tlsx_info.channel[l].wave_set_point = vxi_info.channel[l].wave_set_point;
					}

					// if the user just set this, then send it to the device
					if ((!tlsx_info.channel[l].setting_attenuation_mode)&&tlsx_info.channel[l].attenuation_mode_dirty) {
						vxi_info.channel[l].attenuation_mode_dirty = 1;
						tlsx_info.channel[l].attenuation_mode_dirty = 0;
						vxi_info.channel[l].attenuation_mode = tlsx_info.channel[l].attenuation_mode;
					} else if (!tlsx_info.channel[l].setting_attenuation_mode) {
						// otherwise respect any changes from the device, as long as they aren't editing it
						tlsx_info.channel[l].attenuation_mode = vxi_info.channel[l].attenuation_mode;
					}

					// If there are no pending actions, then clear the
					// busy mark next to this channel. The busy mark is
					// set to true whenever a parameter is set to dirty in
					// the menu thread. Then cleared here when the action
					// is completed
					/*if (!vxi_info.channel[l].atten_set_point_dirty)
						tlsx_info.channel[l].atten_busy = 0;
					if (!vxi_info.channel[l].power_set_point_dirty)
						tlsx_info.channel[l].power_busy = 0;*/
					if (	!vxi_info.channel[l].atten_set_point_dirty &&
							!vxi_info.channel[l].power_set_point_dirty &&
							!vxi_info.channel[l].attenuation_offset_dirty &&
							!vxi_info.channel[l].wave_set_point_dirty &&
							!vxi_info.channel[l].attenuation_mode_dirty &&
							!vxi_info.channel[l].control_mode_dirty) {
						tlsx_info.channel[l].busy = 0;
					}
				}
			}

			tlsx_info.vxi_event = true;
			pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
		} else {
			pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
			tlsx_info.vxi_error = vxi_info.vxi_error;
			pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
		}

		//usleep(VXI_POLL_TIME);
	}
}

#define C (299792458) // m/s

static char* individual_channel_header_str(int l)
{
	static char s[16];
	pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);

	char busy_char = ' ';
	if (tlsx_info.channel[l].busy)
		busy_char = '*';
	if (mode_atten==tlsx_info.channel[l].control_mode)
		snprintf(s, sizeof(s), "CHANNEL %d: ATT%c", l+1, busy_char);
	else
		snprintf(s, sizeof(s), "CHANNEL %d: POW%c", l+1, busy_char);

	pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
	return s;
}

static char* individual_atten_str(int l)
{
	static char s[16];

	pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
	struct channel_t *channel = &tlsx_info.channel[l];

	double atten;
	if (channel->show_atten_set_point)
		atten = channel->atten_set_point;
	else
		atten = channel->atten_actual;

	// when values are being changed we should display a *
	char busy_char = ' ';
	if (channel->atten_busy&&!channel->show_atten_set_point) {
		busy_char = '*';
	}

	if (atten > 999.99) {
		snprintf(s, sizeof(s), "ATTEN%c######dB ", busy_char);
	} else {
		snprintf(s, sizeof(s), "ATTEN%c%6.2fdB ", busy_char, atten);
	}

	pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
	return s;
}

static char* individual_power_str(int l)
{
	static char s[16];

	pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
	struct channel_t *channel = &tlsx_info.channel[l];

	double power;
	if (channel->show_power_set_point)
		power = channel->power_set_point;
	else
		power = channel->power_actual;

	// when values are being changed we should display a *
	char busy_char = ' ';
	if (channel->power_busy&&!channel->show_power_set_point) {
		busy_char = '*';
	}

	switch (tlsx_info.power_units)
	{
	case power_units_dbm:
		if (power < -99.99) {
			snprintf(s, sizeof(s), "POWER%c-#####dBm", busy_char);
		} else if (power > 99.99) {
			snprintf(s, sizeof(s), "POWER%c+#####dBm", busy_char);
		} else {
			snprintf(s, sizeof(s), "POWER%c%+6.2fdBm", busy_char, power);
		}
		break;
	case power_units_mw:
		power = pow(10, power / 10.0); // dBm to mW
		if (power < 1.0) {
			// display in uW
			snprintf(s, sizeof(s), "POWER%c%6.2fuW ", busy_char, power*1000.0);
		} else if (power > 999.99) {
			snprintf(s, sizeof(s), "POWER%c+#####mW ", busy_char);
		} else {
			snprintf(s, sizeof(s), "POWER%c%6.2fmW ", busy_char, power);
		}
		break;
	}

	pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
	return s;
}

static char* individual_wave_str(int l)
{
	static char s[16];

	pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
	struct channel_t *channel = &tlsx_info.channel[l];

	snprintf(s, sizeof(s), "WAVE    %4dnm ", channel->wave_set_point);

	pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
	return s;
}

static char* individual_attenuation_offset_str(int l)
{
	static char s[16];

	pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
	struct channel_t *channel = &tlsx_info.channel[l];

	if (mode_offset == channel->attenuation_mode) {
		float ao = channel->attenuation_offset;

		if (ao < -99.99) {
			snprintf(s, sizeof(s), "^^^^^^-#####dB ");
		} else if (ao > 99.99) {
			snprintf(s, sizeof(s), "^^^^^^+#####dB ");
		} else {
			snprintf(s, sizeof(s), "^^^^^^%6.2fdB ", ao);
		}
	} else {
		snprintf(s, sizeof(s), " ------------- ");
	}

	pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
	return s;
}

// prints either output power or attenuation depending on the control mode
static char* channel_value_str(int l)
{
	static char s[16];

	pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);

	struct channel_t *channel = &tlsx_info.channel[l];

	double power;
	if (channel->show_power_set_point)
		power = channel->power_set_point;
	else
		power = channel->power_actual;

	double atten;
	if (channel->show_atten_set_point)
		atten = channel->atten_set_point;
	else
		atten = channel->atten_actual;

	// when values are being changed we should display a *
	char busy_char = ':';
	if ((channel->atten_busy||channel->power_busy)
				&&!channel->show_power_set_point
				&&!channel->show_atten_set_point) {
		busy_char = '*';
	}

	// these strings must be padded with spaces up to 15 chars, otherwise
	// the m in dBm gets truncated off until the screensaver runs again
	char* power_str = NULL;
	if (channel->control_mode==mode_atten) {
		power_str = "ATT";
	} else {
		power_str = "POW";
	}

	if (channel->control_mode==mode_atten && (channel->show_atten_set_point || tlsx_info.atten_display_mode==disp_atten)) {
		if (atten > 999.99) {
			snprintf(s, sizeof(s), "%d%cATT ######dB ", l + 1, busy_char, atten);
		} else {
			snprintf(s, sizeof(s), "%d%cATT %6.2fdB ", l + 1, busy_char, atten);
		}
	} else {
		switch (tlsx_info.power_units)
		{
		case power_units_dbm:
			if (power < -99.99) {
				snprintf(s, sizeof(s), "%d%c%s -#####dBm", l + 1, busy_char, power_str);
			} else if (power > 99.99) {
				snprintf(s, sizeof(s), "%d%c%s +#####dBm", l + 1, busy_char, power_str);
			} else {
				snprintf(s, sizeof(s), "%d%c%s %+6.2fdBm", l + 1, busy_char, power_str, power);
			}
			break;
		case power_units_mw:
			power = pow(10, power / 10.0); // dBm to mW
			if (power < 1.0) {
				// display in uW
				snprintf(s, sizeof(s), "%d%c%s %6.2fuW ", l + 1, busy_char, power_str, power*1000.0);
			} else if (power > 999.99) {
				snprintf(s, sizeof(s), "%d%c%s +#####mW ", l + 1, busy_char, power_str);
			} else {
				snprintf(s, sizeof(s), "%d%c%s %6.2fmW ", l + 1, busy_char, power_str, power);
			}
			break;
		}
	}

	pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);

	return s;
}

static char* get_attenuation_offset_combined_str(double attenuation_offset)
{
	static char s[16];
	snprintf(s, sizeof(s), "      %6.2fdB ", attenuation_offset);
	return s;
}

static char* get_wavelength_str(unsigned int wavelength)
{
	static char s[16];
	snprintf(s, sizeof(s), "     %unm ", wavelength);
	return s;
}

static char* get_power_units_str(enum power_units_t units)
{
	static char s[32];

	switch (units)
	{
		case power_units_dbm:
			snprintf(s, sizeof(s), "dBm  ");
			return s;
		case power_units_mw:
			snprintf(s, sizeof(s), "mW/uW");
			return s;
		default:
			snprintf(s, sizeof(s), "err  ");
			return s;
	}
}


typedef struct event_ret_t (*timeout_cb_t)(struct menu_info_t *info);


enum menu_mode_t
{
	menu_mode_normal,
	menu_mode_set_value,
};


struct menu_info_t
{
	// settings
	struct menu_item_t *mitems;
	int nitems;
	int timeout;
	timeout_cb_t timeout_cb;
	double idle_timeout;
	timeout_cb_t idle_timeout_cb;

	// private
	MENU *menu;
	ITEM **items;
};


static enum menu_read_result _menu_read(int timeout)
{
	enum menu_read_result menu_res = menu_read(mfpio, timeout);

	/*// process priority interrupts
	switch (menu_res)
	{
		case menu_button1_push:
			button_push(1);
			break;
		case menu_button1_release:
			button_release(1);
			break;
		case menu_button2_push:
			button_push(2);
			break;
		case menu_button2_release:
			button_release(2);
			break;
		default:
			break;
	}*/

	return menu_res;
}


static struct event_ret_t _make_menu(struct menu_info_t *info)
{
	int i;
	int run = 1;
	int start = -1;
	struct event_ret_t ret = {0,};
	double dt;
	struct timeval now, last_input_time;

	// we touch some control to get in here to record this as the last input time
	gettimeofday(&last_input_time, NULL);

	// load mitems in to items and make the menu
	info->items = (ITEM **)calloc(info->nitems + 1, sizeof(ITEM *));
	for (i = 0 ; i < info->nitems; i++)
	{
		info->items[i] = new_item(info->mitems[i].name, info->mitems[i].desc);
		if (!info->mitems[i].skip && start == -1)
			start = i;
	}
	info->items[info->nitems] = NULL;
	info->menu = new_menu(info->items);

	// setup the menu to have our style
	set_menu_format(info->menu, 4, 1);
	set_menu_spacing(info->menu, 1, 1, 1);
	set_menu_mark(info->menu, (start == -1)? " ": "~"); // note "~" turns out to be a single char arrow like this '->' on the lcd char set

	// before we start move to first non skipped item
	if (start > 0 && start < info->nitems)
		set_current_item(info->menu, info->items[start]);

	// show the new menu
	post_menu(info->menu);
	wrefresh(stdscr);

	// menu loop, handle events
	while (ret.code >= 0)
	{
		ITEM *cur_item = current_item(info->menu);
		int cur_item_index = item_index(cur_item);
		enum menu_read_result r = _menu_read(info->timeout);
		int k;
		ret.code = 0;

		// handle signal
		switch (r)
		{
			case menu_timeout:
				gettimeofday(&now, NULL);
				dt = now.tv_sec - last_input_time.tv_sec;
				dt += (now.tv_usec - last_input_time.tv_usec) * 1e-6;
				if (info->idle_timeout != 0 && dt > info->idle_timeout)
				{
					if (info->idle_timeout_cb != NULL)
						ret = info->idle_timeout_cb(info);
					gettimeofday(&last_input_time, NULL);
				}

				if (info->timeout_cb != NULL)
					info->timeout_cb(info);
				break;

			default:
				// run the event callback
				if (info->mitems[cur_item_index].cb != NULL)
				{
					ret = info->mitems[cur_item_index].cb(info->mitems[cur_item_index].cb_param, info, r);

					if (!(ret.flags & MENU_REPOST_HANDLED))
					{
						// repost menu so it will refresh screen when popping back to this menu
						unpost_menu(info->menu);
						post_menu(info->menu);
						wrefresh(stdscr);
					}
				}

				// if the event callback did not handle this sig we will
				if (ret.code == 0)
				{
					switch(r)
					{
						case menu_rot_up_slow:
						case menu_rot_up:
						case menu_rot_up_fast:
							for (k = cur_item_index - 1; k >= 0; k--)
							{
								if (info->mitems[k].skip == 0)
								{
									while (k++ != cur_item_index)
										menu_driver(info->menu, REQ_UP_ITEM);
									break;
								}
							}
							break;
						case menu_rot_down_slow:
						case menu_rot_down:
						case menu_rot_down_fast:
							for (k = cur_item_index + 1; k < info->nitems; k++)
							{
								if (info->mitems[k].skip == 0)
								{
									while (k-- != cur_item_index)
										menu_driver(info->menu, REQ_DOWN_ITEM);
									break;
								}
							}
							break;
					}
				}

				// update the time we last touched so idle timeout will restart
				// note we do this after the callback as we could jump into submenu's for a long time
				switch (r)
				{
					case menu_button0_push:
					case menu_button0_release:
					case menu_rot_up_slow:
					case menu_rot_up:
					case menu_rot_up_fast:
					case menu_rot_down_slow:
					case menu_rot_down:
					case menu_rot_down_fast:
						gettimeofday(&last_input_time, NULL);
						break;
				}
				break;
		}
		wrefresh(stdscr);
	}

	// clean up
	unpost_menu(info->menu);
	for(i = 0; i < info->nitems; i++)
		free_item(info->items[i]);
	free_menu(info->menu);

	ret.code++;
	return ret;
}


// if param < 0 pop down that many submenu's otherwise just back 1 submenu
struct event_ret_t done(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};

	switch (event)
	{
		case menu_button0_release:
			if (param < 0)
				ret.code = param;
			else
				ret.code = -1;
			break;
		default:
			break;
	}

	return ret;
}

struct event_ret_t set_power_units(int param, struct menu_info_t *info, enum menu_read_result event)
{
	static enum power_units_t power_units; // shadow
	struct event_ret_t ret = {0,};

	switch (event)
	{
		case menu_button0_release:
			// toggle between set point and actual
			if (strncmp(menu_mark(info->menu), "~", 1) == 0)
			{
				// entry
				pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
				power_units = tlsx_info.power_units; // load shadow power units
				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);

				set_menu_mark(info->menu, "*");
			}
			else
			{
				// exit
				pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
				tlsx_info.power_units = power_units; // commit power units
				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);

				set_menu_mark(info->menu, "~");
			}
			wrefresh(stdscr);
			ret.code = 1;
			return ret;

		default:
			// other signals are only processed when changing
			if (strncmp(menu_mark(info->menu), "*", 1) == 0)
			{
				int i;
				ITEM *cur_item = current_item(info->menu);

				switch (event)
				{
					case menu_rot_up_slow:
					case menu_rot_up:
					case menu_rot_up_fast:
					case menu_rot_down_slow:
					case menu_rot_down:
					case menu_rot_down_fast:
						if (power_units == power_units_dbm)
							power_units = power_units_mw;
						else
							power_units = power_units_dbm;
						break;
					default:
						ret.code = 0;
						return ret;
				}

				// update the menu with the new value
				unpost_menu(info->menu);
				snprintf((char *)cur_item->name.str, 32, "%s", get_power_units_str(power_units));
				post_menu(info->menu);
				wrefresh(stdscr);
				ret.code = 1;
				return ret;
			}
			ret.code = 0;
			return ret;
	}
}

struct event_ret_t idle_timeout(struct menu_info_t *info)
{
	struct event_ret_t ret = {.code=-BIG_NUMBER, 0,};
	int l;
	pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
	for (l = 0; l < NUM_CHANNELS; l++)
	{
		if ((tlsx_info.channel_mask >> l) & 0x01)
		{
			// bail all set point adjustments
			tlsx_info.channel[l].show_atten_set_point = false;
			tlsx_info.channel[l].show_power_set_point = false;
			tlsx_info.channel[l].show_wave_set_point = false;
			tlsx_info.channel[l].setting_control_mode = false;
			tlsx_info.channel[l].setting_attenuation_mode = false;
		}
	}
	pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
	curs_set(0); // in case we timeout with the cursor set to underline or blinking etc
	return ret;
}

struct event_ret_t units_menu(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};

	switch (event)
	{
		case menu_button0_release:
			{
				int i = 0;
				struct menu_item_t units_menu_items[7];
				struct menu_info_t units_menu_info =
				{
					units_menu_items,
					0,
					2 * ONE_SEC,
					NULL,
					DEFAULT_IDLE_TIMEOUT,	// idle timeout
					idle_timeout,
				};

				memset(units_menu_items, 0, sizeof(units_menu_items));
				snprintf(units_menu_items[i].name, 32, "UNITS");
				units_menu_items[i++].skip= 1;
				pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
				snprintf(units_menu_items[i].name, 32, "%s", get_power_units_str(tlsx_info.power_units));
				units_menu_items[i].cb_param = 0;
				units_menu_items[i++].cb = set_power_units;
				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
				snprintf(units_menu_items[i].name, 32, "DONE");
				units_menu_items[i].cb_param = -1;
				units_menu_items[i++].cb = done;
				units_menu_info.nitems = i;

				wclear(stdscr);
				ret = _make_menu(&units_menu_info);
				wclear(stdscr);

				return ret;
			}
		default:
			return ret;
	}
}


static bool iface_changed;
static int iface_menu_method_index;
static int iface_menu_ipaddress_index;
static int iface_menu_netmask_index;


static void on_iface_changed_event(struct iface_t *iface, struct menu_info_t *info)
{
	ITEM *method_item = info->menu->items[iface_menu_method_index];
	ITEM *ipaddress_item = info->menu->items[iface_menu_ipaddress_index];
	ITEM *netmask_item = info->menu->items[iface_menu_netmask_index];

	iface_changed = true;
	snprintf((char *)method_item->name.str, 32, "METHOD: %-7s", iface_method_to_str(iface, NULL, 0));
	if (iface->method == IFACE_METHOD_DHCP)
	{
		snprintf((char *)ipaddress_item->name.str, 32, "---.---.---.---");
		snprintf((char *)netmask_item->name.str, 32, "---.---.---.---");
	}
	else
	{
		snprintf((char *)ipaddress_item->name.str, 32, "%.3d.%.3d.%.3d.%.3d", iface->addr[0], iface->addr[1], iface->addr[2], iface->addr[3]);
		snprintf((char *)netmask_item->name.str, 32, "%.3d.%.3d.%.3d.%.3d", iface->netmask[0], iface->netmask[1], iface->netmask[2], iface->netmask[3]);
	}
}


struct event_ret_t iface_menu_change_method(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	struct iface_t *iface = (struct iface_t *)param;
	ITEM *cur_item = current_item(info->menu);

	switch (event)
	{
		case menu_button0_release:
			if (strncmp(menu_mark(info->menu), "~", 1) == 0)
				set_menu_mark(info->menu, "*");
			else if (strncmp(menu_mark(info->menu), "*", 1) == 0)
				set_menu_mark(info->menu, "~");
			break;

		default:
			// other signals are only processed when changing
			if (strncmp(menu_mark(info->menu), "*", 1) == 0)
			{
				switch (event)
				{
					case menu_rot_up_slow:
					case menu_rot_up:
					case menu_rot_up_fast:
					case menu_rot_down_slow:
					case menu_rot_down:
					case menu_rot_down_fast:
						// just toggle through our supported methods
						if (iface->method == IFACE_METHOD_STATIC)
							iface->method = IFACE_METHOD_DHCP;
						else
							iface->method = IFACE_METHOD_STATIC;
						on_iface_changed_event(iface, info);
						ret.code = 1;
						break;
					default:
						break;
				}
			}
			break;
	}

	snprintf((char *)cur_item->name.str, 32, "METHOD: %-7s", iface_method_to_str(iface, NULL, 0));
	return ret;
}


struct event_ret_t iface_menu_change_ipaddr(struct iface_t *iface, int ipaddr_off, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	static int o = 0;
	static int addr[4];
	ITEM *cur_item = current_item(info->menu);
	int y, cur_mode = 0;

	switch (event)
	{
		case menu_button0_release:
			if (strncmp(menu_mark(info->menu), "~", 1) == 0)
			{
				// entry
				o = 0;
				cur_mode = 1;
				set_menu_mark(info->menu, "*");
				memcpy(addr, (uint8_t *)iface + ipaddr_off, sizeof(addr));
			}
			else if (strncmp(menu_mark(info->menu), "*", 1) == 0)
			{
				struct iface_t iface_cur = *iface;

				switch (o)
				{
					case 0:
					case 1:
					case 2:
						// just move through all the octect's until the 4th
						o++;
						cur_mode = 1;
						break;

					default:
						syslog(LOG_ERR, "invalid ipaddr state !\n");
					case 3:
						// exit
						o = 0;
						cur_mode = 0;
						set_menu_mark(info->menu, "~");
						memcpy((uint8_t *)iface + ipaddr_off, addr, sizeof(addr));
						break;
				}
			}
			break;

		default:
			// other signals are only processed when changing
			if (strncmp(menu_mark(info->menu), "*", 1) == 0)
			{
				cur_mode = 1;
				switch (event)
				{
					case menu_rot_up_slow:
					case menu_rot_up:
						if (addr[o] < 255)
							addr[o]++;
						break;
					case menu_rot_up_fast:
						if (addr[o] <= 255 - 16)
							addr[o] += 16;
						break;
					case menu_rot_down_slow:
					case menu_rot_down:
						if (addr[o] > 0)
							addr[o]--;
						break;
					case menu_rot_down_fast:
						if (addr[o] >= 16)
							addr[o] -= 16;
						break;
					default:
						ret.flags = MENU_REPOST_HANDLED;
						return ret;
				}
				on_iface_changed_event(iface, info);
			}
			else
				return ret;
			break;
	}

	// display the new address and
	unpost_menu(info->menu);
	snprintf((char *)cur_item->name.str, 32, "%.3d.%.3d.%.3d.%.3d", addr[0], addr[1], addr[2], addr[3]);
	post_menu(info->menu);
	y = getcury(stdscr);
	move(y, 3 + 4*o);
	curs_set(cur_mode);
	wrefresh(stdscr);
	ret.code = 1;
	ret.flags = MENU_REPOST_HANDLED;
	return ret;
}


struct event_ret_t iface_menu_change_addr(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	struct iface_t *iface = (struct iface_t *)param;

	if (iface->method == IFACE_METHOD_STATIC)
		return iface_menu_change_ipaddr(iface, offsetof(struct iface_t, addr), info, event);
	return ret;
}


struct event_ret_t iface_menu_change_netmask(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	struct iface_t *iface = (struct iface_t *)param;

	if (iface->method == IFACE_METHOD_STATIC)
		return iface_menu_change_ipaddr(iface, offsetof(struct iface_t, netmask), info, event);
	return ret;
}


struct event_ret_t iface_save(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	struct iface_t *iface = (struct iface_t *)param;
	char cmd[512];

	switch (event)
	{
		case menu_button0_release:
			// save changes was selected so write new network setup to disk
			// and resetup the network

			wclear(stdscr);
			mvwprintw(stdscr, 1, 0, "UPDATING NETWORK");
			mvwprintw(stdscr, 2, 0, "  PLEASE  WAIT  ");
			wrefresh(stdscr);
			sprintf(cmd, "%s/ifchange %s %s", mwd, "pre", iface->iface_name);
			system(cmd);
			iface_set_cfg(iface, NULL);
			sprintf(cmd, "%s/ifchange %s %s", mwd, "post", iface->iface_name);
			system(cmd);
			ret.code = -2;
			break;

		default:
			break;
	}

	return ret;
}


struct event_ret_t iface_done_menu(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};

	switch (event)
	{
		case menu_button0_release:
			ret.code = -1;

			// if something has changed in the network setup offer to save new cfg
			if (iface_changed)
			{
				int i = 0;
				int l;
				struct menu_item_t iface_done_menu_items[10];
				struct menu_info_t iface_done_menu_info =
				{
					iface_done_menu_items,
					0,
					2 * ONE_SEC,
					NULL,
					NET_IDLE_TIMEOUT,	// idle timeout
					idle_timeout,
				};
				memset(iface_done_menu_items, 0, sizeof(iface_done_menu_items));

				snprintf(iface_done_menu_items[i].name, 32, "SAVE CHANGES:  ");
				iface_done_menu_items[i].skip = 1;
				iface_done_menu_items[i++].cb = 0;
				snprintf(iface_done_menu_items[i].name, 32, "YES");
				iface_done_menu_items[i].cb_param = param;
				iface_done_menu_items[i++].cb = iface_save;
				snprintf(iface_done_menu_items[i].name, 32, "NO");
				iface_done_menu_items[i].cb_param = -2;
				iface_done_menu_items[i++].cb = done;
				iface_done_menu_info.nitems = i;

				wclear(stdscr);
				ret = _make_menu(&iface_done_menu_info);
				wclear(stdscr);
			}
			break;

		default:
			break;
	}

	return ret;
}


struct event_ret_t iface_menu(struct iface_t *iface_cur, bool ro, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	struct iface_t iface_saved;

	iface_changed = false;

	switch (event)
	{
		case menu_button0_release:
			{
				int i = 0;
				int l;
				struct menu_item_t network_menu_items[10];
				struct menu_info_t iface_menu_info =
				{
					network_menu_items,
					0,
					2 * ONE_SEC,
					NULL,
					NET_IDLE_TIMEOUT,	// idle timeout
					idle_timeout,
				};

				snprintf(iface_saved.iface_name, IFACE_STR_LEN, "%s", iface_cur->iface_name);
				iface_get_cfg(&iface_saved, NULL);
				iface_get_active_cfg(iface_cur);
				iface_cur->method = iface_saved.method; // we dont know how iface_cur got setup so assume it is from what was saved

				memset(network_menu_items, 0, sizeof(network_menu_items));

				snprintf(network_menu_items[i].name, 32, "ADDRESS:");
				network_menu_items[i++].cb = 0;
				snprintf(network_menu_items[i].name, 32, "%-15s", iface_addr_to_str(iface_cur, NULL, 0));
				iface_menu_ipaddress_index = i;
				network_menu_items[i].cb = ro? NULL: iface_menu_change_addr;
				network_menu_items[i++].cb_param = (int)&iface_saved;

				snprintf(network_menu_items[i].name, 32, "NETMASK:");
				network_menu_items[i++].cb = 0;
				snprintf(network_menu_items[i].name, 32, "%-15s", iface_netmask_to_str(iface_cur, NULL, 0));
				iface_menu_netmask_index = i;
				network_menu_items[i].cb = ro? NULL: iface_menu_change_netmask;
				network_menu_items[i++].cb_param = (int)&iface_saved;

				snprintf(network_menu_items[i].name, 32, "METHOD: %-7s", iface_method_to_str(iface_cur, NULL, 0));
				iface_menu_method_index = i;
				network_menu_items[i].cb = ro? NULL: iface_menu_change_method;
				network_menu_items[i++].cb_param = (int)&iface_saved;

				snprintf(network_menu_items[i].name, 32, "DONE");
				network_menu_items[i].cb_param = (int)&iface_saved;
				network_menu_items[i++].cb = iface_done_menu;

				iface_menu_info.nitems = i;

				wclear(stdscr);
				ret = _make_menu(&iface_menu_info);
				wclear(stdscr);

				return ret;
			}
		default:
			return ret;
	}
}


struct event_ret_t iface_rw_menu(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct iface_t *iface = (struct iface_t *)param;
	return iface_menu(iface, false, info, event);
}


struct event_ret_t iface_ro_menu(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct iface_t *iface = (struct iface_t *)param;
	return iface_menu(iface, true, info, event);
}


struct event_ret_t network_menu(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	struct iface_t eth0;
	struct iface_t usb0;

	switch (event)
	{
		case menu_button0_release:
			{
				int i = 0;
				int l;
				struct menu_item_t network_menu_items[3];
				struct menu_info_t network_menu_info =
				{
					network_menu_items,
					0,
					2 * ONE_SEC,
					NULL,
					NET_IDLE_TIMEOUT,	// idle timeout
					idle_timeout,
				};

				snprintf(eth0.iface_name, IFACE_STR_LEN, "eth0");
				snprintf(usb0.iface_name, IFACE_STR_LEN, "usb0");

				memset(network_menu_items, 0, sizeof(network_menu_items));
				snprintf(network_menu_items[i].name, 32, "ETHERNET");
				network_menu_items[i].cb = iface_rw_menu;
				network_menu_items[i++].cb_param = (int)&eth0;
				snprintf(network_menu_items[i].name, 32, "USB");
				network_menu_items[i].cb = iface_ro_menu;
				network_menu_items[i++].cb_param = (int)&usb0;
				snprintf(network_menu_items[i].name, 32, "DONE");
				network_menu_items[i].cb_param = -1;
				network_menu_items[i++].cb = done;
				network_menu_info.nitems = i;

				wclear(stdscr);
				ret = _make_menu(&network_menu_info);
				wclear(stdscr);

				return ret;
			}
		default:
			return ret;
	}
}

struct event_ret_t set_value_atten(int l, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	struct channel_t *channel;
	double atten_set_point;
	bool set_atten_setpoint = false;
	pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
	channel = &tlsx_info.channel[l];

	switch (event)
	{
		case menu_button0_release:
			// toggle between set point and actual
			///@todo when transitioning back to show actual mode, send the new set point to tlsx_service
			channel->show_atten_set_point = !channel->show_atten_set_point;
			if (channel->show_atten_set_point)
			{
				set_menu_mark(info->menu, "*");
			}
			else
			{
				atten_set_point = channel->atten_set_point;
				set_atten_setpoint = true;
				set_menu_mark(info->menu, "~");
			}

			if (set_atten_setpoint) {
				tlsx_info.channel[l].atten_set_point = atten_set_point;
				tlsx_info.channel[l].atten_set_point_dirty = 1;
				//tlsx_info.channel[l].atten_busy = 1;
				tlsx_info.channel[l].busy = 1;
			}

			pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
			wrefresh(stdscr);

			main_menu_timeout(info);
			ret.code = 1;
			return ret;
		default:
			// other signals are only processed when changing setpoint
			if (tlsx_info.channel[l].show_atten_set_point)
			{
				double dp = 0;
				switch (event)
				{
					case menu_rot_up_slow:
						dp = 1 * POWER_STEP_SIZE;
						break;
					case menu_rot_up:
						dp = 10 * POWER_STEP_SIZE;
						break;
					case menu_rot_up_fast:
						dp = 50 * POWER_STEP_SIZE;
						break;
					case menu_rot_down_slow:
						dp = -1 * POWER_STEP_SIZE;
						break;
					case menu_rot_down:
						dp = -10 * POWER_STEP_SIZE;
						break;
					case menu_rot_down_fast:
						dp = -50 * POWER_STEP_SIZE;
						break;
					default:
						break;
				}
				channel->atten_set_point += dp;

				// snap to min/max boundaries
				if (channel->atten_set_point < channel->atten_min)
					channel->atten_set_point = channel->atten_min;
				else if (channel->atten_set_point > channel->atten_max)
					channel->atten_set_point = channel->atten_max;

				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
				main_menu_timeout(info);
				ret.code = 1;
				return ret;
			}
			pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
			ret.code = 0;
			return ret;
	}

	// do this just to be safe (we should never ever ever get here)
	pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
}

struct event_ret_t set_value_attenuation_offset(int l, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	struct channel_t *channel;
	double attenuation_offset;
	pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
	channel = &tlsx_info.channel[l];

	switch (event)
	{
		case menu_button0_release:
			// toggle between setting and actual
			channel->show_attenuation_offset = !channel->show_attenuation_offset;
			if (channel->show_attenuation_offset)
			{
				set_menu_mark(info->menu, "*");
			}
			else
			{
				attenuation_offset = channel->attenuation_offset;
				set_menu_mark(info->menu, "~");
				tlsx_info.channel[l].attenuation_offset = attenuation_offset;
				tlsx_info.channel[l].attenuation_offset_dirty = 1;
				tlsx_info.channel[l].busy = 1;
			}
			enum chan_display_mode_t chan_display_mode = tlsx_info.chan_display_mode;
			pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);

			wrefresh(stdscr);
			if (disp_individual==chan_display_mode) {
				// if we are on the main menu the it handles the repaint
				main_menu_timeout(info);
			}
			main_menu_timeout(info);
			ret.code = 1;
			return ret;
		default:
			// other signals are only processed when changing setpoint
			if (tlsx_info.channel[l].show_attenuation_offset)
			{
				double dp = 0;
				switch (event)
				{
					case menu_rot_up_slow:
						dp = 1 * POWER_STEP_SIZE;
						break;
					case menu_rot_up:
						dp = 10 * POWER_STEP_SIZE;
						break;
					case menu_rot_up_fast:
						dp = 50 * POWER_STEP_SIZE;
						break;
					case menu_rot_down_slow:
						dp = -1 * POWER_STEP_SIZE;
						break;
					case menu_rot_down:
						dp = -10 * POWER_STEP_SIZE;
						break;
					case menu_rot_down_fast:
						dp = -50 * POWER_STEP_SIZE;
						break;
					default:
						break;
				}
				channel->attenuation_offset += dp;

				// snap to min/max boundaries
				if (channel->attenuation_offset < -100)
					channel->attenuation_offset = -100;
				else if (channel->attenuation_offset > 100)
					channel->attenuation_offset = 100;

				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
				main_menu_timeout(info);
				ret.code = 1;
				return ret;
			}
			pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
			ret.code = 0;
			return ret;
	}

	// do this just to be safe (we should never ever ever get here)
	pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
}

struct event_ret_t set_value_power(int l, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	struct channel_t *channel;
	double power_set_point;
	pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
	channel = &tlsx_info.channel[l];

	switch (event)
	{
		case menu_button0_release:
			// toggle between set point and actual
			///@todo when transitioning back to show actual mode, send the new set point to tlsx_service
			channel->show_power_set_point = !channel->show_power_set_point;
			if (channel->show_power_set_point)
			{
				set_menu_mark(info->menu, "*");
			}
			else
			{
				power_set_point = channel->power_set_point;
				tlsx_info.channel[l].power_set_point = power_set_point;
				tlsx_info.channel[l].power_set_point_dirty = 1;
				//tlsx_info.channel[l].power_busy = 1;
				tlsx_info.channel[l].busy = 1;
				set_menu_mark(info->menu, "~");
			}

			pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
			wrefresh(stdscr);

			main_menu_timeout(info);
			ret.code = 1;
			return ret;
		default:
			// other signals are only processed when changing setpoint
			if (tlsx_info.channel[l].show_power_set_point)
			{
				double dp = 0;
				switch (event)
				{
					case menu_rot_up_slow:
						dp = 1 * POWER_STEP_SIZE;
						break;
					case menu_rot_up:
						dp = 10 * POWER_STEP_SIZE;
						break;
					case menu_rot_up_fast:
						dp = 50 * POWER_STEP_SIZE;
						break;
					case menu_rot_down_slow:
						dp = -1 * POWER_STEP_SIZE;
						break;
					case menu_rot_down:
						dp = -10 * POWER_STEP_SIZE;
						break;
					case menu_rot_down_fast:
						dp = -50 * POWER_STEP_SIZE;
						break;
					default:
						break;
				}
				// always adjust with 1 tick as 0.01 dBm, even when the display units are in mW/uW
				channel->power_set_point += dp;

				// snap to min/max boundaries
				if (channel->power_set_point < channel->power_min)
					channel->power_set_point = channel->power_min;
				else if (channel->power_set_point > channel->power_max)
					channel->power_set_point = channel->power_max;

				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
				main_menu_timeout(info);
				ret.code = 1;
				return ret;
			}
			pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
			ret.code = 0;
			return ret;
	}

	// do this just to be safe (we should never ever ever get here)
	pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
}

struct event_ret_t set_value_channel(int l, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	enum control_mode_t control_mode;

	pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
	control_mode = tlsx_info.channel[l].control_mode;
	pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);

	if (control_mode==mode_atten) {
		return set_value_atten(l, info, event);
	} else if (control_mode==mode_power) {
		return set_value_power(l, info, event);
	} else {
		main_menu_timeout(info);
		ret.code = 0;
		return ret;
	}
}

struct event_ret_t channel_set_wavelength(int l, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	struct channel_t *channel;
	static unsigned int wave_set_point;
	pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);

	switch (event)
	{
		case menu_button0_release:
			// toggle between set point and actual
			///@todo when transitioning back to show actual mode, send the new set point to tlsx_service
			tlsx_info.channel[l].show_wave_set_point = !tlsx_info.channel[l].show_wave_set_point;
			if (tlsx_info.channel[l].show_wave_set_point)
			{
				wave_set_point = tlsx_info.channel[l].wave_set_point;
				set_menu_mark(info->menu, "*");
			}
			else
			{
				tlsx_info.channel[l].wave_set_point = wave_set_point;
				tlsx_info.channel[l].wave_set_point_dirty = 1;
				tlsx_info.channel[l].busy = 1;
				set_menu_mark(info->menu, "~");
			}
			enum chan_display_mode_t chan_display_mode = tlsx_info.chan_display_mode;
			pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);

			wrefresh(stdscr);
			if (disp_individual==chan_display_mode) {
				// if we are on the main menu the it handles the repaint
				main_menu_timeout(info);
			}
			ret.code = 1;
			return ret;
		default:
			// other signals are only processed when changing setpoint
			if (tlsx_info.channel[l].show_wave_set_point)
			{
				int dp = 0;
				int i;
				ITEM *cur_item = current_item(info->menu);
				switch (event)
				{
					case menu_rot_up_slow:
					case menu_rot_up:
					case menu_rot_up_fast:
						++dp;
						break;
					case menu_rot_down_slow:
					case menu_rot_down:
					case menu_rot_down_fast:
						--dp;
						break;
					default:
						break;
				}
				// find the current item, if the set point was somehow not in the list, we get set to the last item
				for (i=0; i<tlsx_info.channel[l].wave_list_len; ++i) {
					if (wave_set_point==tlsx_info.channel[l].wave_list[i])
						break;
				}
				// move relative to the current index
				i += dp;
				// snap the new index to the bounds
				if (i<0)
					i = 0;
				if (i>=tlsx_info.channel[l].wave_list_len)
					i = tlsx_info.channel[l].wave_list_len-1;
				// set the set point to the value of the new item*/
				wave_set_point = tlsx_info.channel[l].wave_list[i];
				// we never need to remember the index itself

				tlsx_info.channel[l].wave_set_point = wave_set_point;
				if (disp_combined==tlsx_info.chan_display_mode) {
					pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
					// update the menu with the new value, unless we are on the main menu
					unpost_menu(info->menu);
					snprintf((char *)cur_item->name.str, 32, "%d: %s", l+1, get_wavelength_str(wave_set_point));
					post_menu(info->menu);
					wrefresh(stdscr);
				} else {
					pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
					// if we are on the main menu the it handles the repaint
					main_menu_timeout(info);
				}
				ret.code = 1;
				return ret;
			}
			pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
			ret.code = 0;
			return ret;
	}

	// do this just to be safe (we should never ever ever get here)
	pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
}

struct event_ret_t set_selected_channel(int l, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);

	switch (event)
	{
		case menu_button0_release:
			// toggle between set point and actual
			if (strncmp(menu_mark(info->menu), "~", 1) == 0)
			{
				set_menu_mark(info->menu, "*");
			}
			else
			{
				set_menu_mark(info->menu, "~");
			}
			pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
			wrefresh(stdscr);
			ret.code = 1;
			return ret;

		default:
			// other signals are only processed when changing
			if (strncmp(menu_mark(info->menu), "*", 1) == 0)
			{
				int dp = 0;
				ITEM *cur_item = current_item(info->menu);
				switch (event)
				{
					case menu_rot_up_slow:
					case menu_rot_up:
					case menu_rot_up_fast:
						dp = 1;
						break;
					case menu_rot_down_slow:
					case menu_rot_down:
					case menu_rot_down_fast:
						dp = -1;
						break;
					default:
						break;
				}
				tlsx_info.selected_channel += dp;
				// test that we will actually find a channel that is less than 4
				// otherwise we would deadlock in the while loop
				if (tlsx_info.channel_mask&0xF) {
					while (!((1<<tlsx_info.selected_channel)&tlsx_info.channel_mask)) {
						if (tlsx_info.selected_channel>=NUM_CHANNELS)
							dp = -1;
						if (tlsx_info.selected_channel<=0)
							dp = 1;

						tlsx_info.selected_channel += dp;
					}
				} else {
					// if there are no channels, bail to avoid a deadlock
					tlsx_info.selected_channel = 0;
				}

				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
				main_menu_timeout(info);
				ret.code = 1;
				return ret;
			}
			pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
			ret.code = 0;
			return ret;
	}

	// do this just to be safe (we should never ever ever get here)
	pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
}

struct event_ret_t set_test_char(int param, struct menu_info_t *info, enum menu_read_result event)
{
	static char test_char = 'A';
	struct event_ret_t ret = {0,};
	int y;

	switch (event)
	{
		case menu_button0_release:
			// toggle between set point and actual
			if (strncmp(menu_mark(info->menu), "~", 1) == 0)
			{
				set_menu_mark(info->menu, "*");
				y = getcury(stdscr);
				move(y, 5);
				curs_set(2);
			}
			else
			{
				set_menu_mark(info->menu, "~");
				y = getcury(stdscr);
				move(y, 5);
				curs_set(0);
			}
			wrefresh(stdscr);
			ret.code = 1;
			return ret;

		default:
			// other signals are only processed when changing
			if (strncmp(menu_mark(info->menu), "*", 1) == 0)
			{
				int i;
				ITEM *cur_item = current_item(info->menu);

				switch (event)
				{
					case menu_rot_up_slow:
					case menu_rot_up:
					case menu_rot_up_fast:
						test_char++;
						break;
					case menu_rot_down_slow:
					case menu_rot_down:
					case menu_rot_down_fast:
						test_char--;
						break;
					default:
						ret.code = 0;
						return ret;
				}

				// update the menu with the new value
				unpost_menu(info->menu);
				snprintf((char *)cur_item->name.str, 32, "12%c34 %X", test_char, (unsigned int)test_char);
				post_menu(info->menu);
				wrefresh(stdscr);
				ret.code = 1;
				return ret;
			}
			ret.code = 0;
			return ret;
	}
}


struct event_ret_t wavelength_menu(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	int channel_mask = 0;

	switch (event)
	{
		case menu_button0_release:
			{
				int i = 0;
				int l;
				struct menu_item_t wave_menu_items[5];
				struct menu_info_t wave_menu_info =
				{
					wave_menu_items,
					0,
					2 * ONE_SEC,
					NULL,
					DEFAULT_IDLE_TIMEOUT,	// idle timeout
					idle_timeout,
				};
				memset(wave_menu_items, 0, sizeof(wave_menu_items));

				pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
				channel_mask = tlsx_info.channel_mask;
				for (l = 0; l < NUM_CHANNELS; l++)
				{
					if ((channel_mask >> l) & 0x01)
					{
						snprintf(wave_menu_items[i].name, 32, "%d: %s", l+1, get_wavelength_str(tlsx_info.channel[l].wave_set_point));
						wave_menu_items[i].cb_param = l;
						wave_menu_items[i++].cb = channel_set_wavelength;
					}
				}
				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
				snprintf(wave_menu_items[i].name, 32, "DONE");
				wave_menu_items[i].cb_param = -1;
				wave_menu_items[i++].cb = done;
				wave_menu_info.nitems = i;

				wclear(stdscr);
				ret = _make_menu(&wave_menu_info);
				wclear(stdscr);

				return ret;
			}
		default:
			return ret;
	}
}

static char* get_control_mode_str(enum control_mode_t control_mode)
{
	static char s[32];

	switch (control_mode)
	{
		case mode_atten:
			snprintf(s, sizeof(s), "ATTENUATION");
			break;
		case mode_power:
			snprintf(s, sizeof(s), "POWER      ");
			break;
		default:
			snprintf(s, sizeof(s), "err %d      ", control_mode);
			break;
	}

	return s;
}

struct event_ret_t set_attenuation_mode(int l, struct menu_info_t *info, enum menu_read_result event)
{
	static int attenuation_mode; // shadow
	struct event_ret_t ret = {0,};

	pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
	switch (event)
	{
		case menu_button0_release:
			// toggle between set point and actual
			if (tlsx_info.channel[l].setting_attenuation_mode == 0) {
				// entry
				attenuation_mode = tlsx_info.channel[l].attenuation_mode; // load shadow attenuation_mode
				tlsx_info.channel[l].setting_attenuation_mode = 1;

				set_menu_mark(info->menu, "*");
			} else {
				// exit
				tlsx_info.channel[l].attenuation_mode = attenuation_mode;
				tlsx_info.channel[l].attenuation_mode_dirty = 1;
				tlsx_info.channel[l].setting_attenuation_mode = 0;
				tlsx_info.channel[l].busy = 1;

				if(mode_offset==attenuation_mode) {
					// we also need to update the user offset to 0 and set it
					tlsx_info.channel[l].attenuation_offset = 0.0;
					tlsx_info.channel[l].attenuation_offset_dirty = true;
				}
				set_menu_mark(info->menu, "~");
			}
			enum chan_display_mode_t chan_display_mode = tlsx_info.chan_display_mode;
			pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);

			wrefresh(stdscr);
			if (disp_individual==chan_display_mode) {
				// if we are on the main menu the it handles the repaint
				main_menu_timeout(info);
			}
			ret.code = 1;
			return ret;

		default:
			// other signals are only processed when changing
			if (tlsx_info.channel[l].setting_attenuation_mode == 1) {
				int i;
				ITEM *cur_item = current_item(info->menu);

				// when we change mode we expect to see certain changes in
				// the attenuation numbers in tlsx_info (min, max, set, actual)
				// This will be done by the blade and will be updated on the next poll
				// However the current values are stale and must be adjusted
				// whenever we change the mode
				double adjustment = 0.0;
				switch (event) {
					case menu_rot_up_slow:
					case menu_rot_up:
					case menu_rot_up_fast:
						switch (attenuation_mode) {
							case mode_offset:
								attenuation_mode = mode_absolute;
								break;
							case mode_absolute:
								adjustment = -tlsx_info.channel[l].insertion_loss;
								attenuation_mode = mode_relative;
								break;
							case mode_relative:
								adjustment = tlsx_info.channel[l].insertion_loss;
								attenuation_mode = mode_offset;
								break;
							default:
								attenuation_mode = mode_offset;
						}
						break;
					case menu_rot_down_slow:
					case menu_rot_down:
					case menu_rot_down_fast:
						switch (attenuation_mode) {
							case mode_offset:
								adjustment = -tlsx_info.channel[l].insertion_loss;
								attenuation_mode = mode_relative;
								break;
							case mode_absolute:
								attenuation_mode = mode_offset;
								break;
							case mode_relative:
								adjustment = tlsx_info.channel[l].insertion_loss;
								attenuation_mode = mode_absolute;
								break;
							default:
								attenuation_mode = mode_offset;
						}
						break;
					default:
						pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
						ret.code = 0;
						return ret;
				}

				// make the adjustments (for display purposes, see above)
				tlsx_info.channel[l].atten_min += adjustment;
				tlsx_info.channel[l].atten_max += adjustment;
				tlsx_info.channel[l].atten_default += adjustment;
				tlsx_info.channel[l].atten_actual += adjustment;
				tlsx_info.channel[l].atten_set_point += adjustment;

				// we do this for display purposes, while the user is scrolling
				// through the options we want user offset to show 0
				// note this doesn't actually effect the actual user offset
				// because the dirty flag isn't set, this is only done if the
				// user actually selects mode_offset
				tlsx_info.channel[l].attenuation_offset = 0.0;

				// change the mode
				tlsx_info.channel[l].attenuation_mode = attenuation_mode;
				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
				// if we are on the main menu the it handles the repaint
				main_menu_timeout(info);

				ret.code = 1;
				return ret;
			}
			pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
			ret.code = 0;
			return ret;
	}
	// just in case
	pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
	ret.code = 0;
	return ret;
}

struct event_ret_t set_control_mode(int l, struct menu_info_t *info, enum menu_read_result event)
{
	static int control_mode; // shadow
	struct event_ret_t ret = {0,};

	pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
	switch (event)
	{
		case menu_button0_release:
			// toggle between set point and actual
			if (strncmp(menu_mark(info->menu), "~", 1) == 0) {
				// entry
				control_mode = tlsx_info.channel[l].control_mode; // load shadow control mode
				tlsx_info.channel[l].setting_control_mode = 1;

				set_menu_mark(info->menu, "*");
			} else {
				// exit
				tlsx_info.channel[l].control_mode = control_mode;
				tlsx_info.channel[l].control_mode_dirty = 1;
				tlsx_info.channel[l].setting_control_mode = 0;
				tlsx_info.channel[l].busy = 1;

				set_menu_mark(info->menu, "~");
			}
			enum chan_display_mode_t chan_display_mode = tlsx_info.chan_display_mode;
			pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);

			wrefresh(stdscr);
			if (disp_individual==chan_display_mode) {
				// if we are on the main menu the it handles the repaint
				main_menu_timeout(info);
			}
			ret.code = 1;
			return ret;

		default:
			// other signals are only processed when changing
			if (strncmp(menu_mark(info->menu), "*", 1) == 0) {
				int i;
				ITEM *cur_item = current_item(info->menu);

				switch (event) {
					case menu_rot_up_slow:
					case menu_rot_up:
					case menu_rot_up_fast:
					case menu_rot_down_slow:
					case menu_rot_down:
					case menu_rot_down_fast:
						if (control_mode == mode_atten)
							control_mode = mode_power;
						else
							control_mode = mode_atten;
						break;
					default:
						pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
						ret.code = 0;
						return ret;
				}

				tlsx_info.channel[l].control_mode = control_mode;
				if (disp_combined==tlsx_info.chan_display_mode) {
					pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
					// update the menu with the new value, unless we are on the main menu
					unpost_menu(info->menu);
					snprintf((char *)cur_item->name.str, 32, "%u: %s", l, get_control_mode_str(control_mode));
					post_menu(info->menu);
					wrefresh(stdscr);
				} else {
					pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
					// if we are on the main menu the it handles the repaint
					main_menu_timeout(info);
				}

				ret.code = 1;
				return ret;
			}
			pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
			ret.code = 0;
			return ret;
	}
	// just in case
	pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
	ret.code = 0;
	return ret;
}

struct event_ret_t control_mode_menu(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	int channel_mask = 0;

	switch (event)
	{
		case menu_button0_release:
			{
				int i = 0;
				int l;
				struct menu_item_t menu_items[7];
				struct menu_info_t menu_info =
				{
					menu_items,
					0,
					2 * ONE_SEC,
					NULL,
					DEFAULT_IDLE_TIMEOUT,	// idle timeout
					idle_timeout,
				};
				memset(menu_items, 0, sizeof(menu_items));

				pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
				channel_mask = tlsx_info.channel_mask;
				for (l = 0; l < NUM_CHANNELS; l++)
				{
					if ((channel_mask >> l) & 0x01)
					{
						snprintf(menu_items[i].name, 32, "%u: %s", l, get_control_mode_str(tlsx_info.channel[l].control_mode));
						menu_items[i].cb_param = l;
						menu_items[i++].cb = set_control_mode;
					}
				}
				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
				snprintf(menu_items[i].name, 32, "DONE");
				menu_items[i].cb_param = -1;
				menu_items[i++].cb = done;
				menu_info.nitems = i;

				wclear(stdscr);
				ret = _make_menu(&menu_info);
				wclear(stdscr);

				return ret;
			}
		default:
			return ret;
	}
}

static char* atten_display_mode_str(enum atten_display_mode_t atten_display_mode)
{
	static char s[32];

	switch (atten_display_mode)
	{
		case disp_atten:
			snprintf(s, sizeof(s), "ATTENUATION");
			break;
		case disp_power:
			snprintf(s, sizeof(s), "POWER      ");
			break;
		default:
			snprintf(s, sizeof(s), "err %d      ", atten_display_mode);
			break;
	}

	return s;
}

static char* chan_display_mode_str(enum chan_display_mode_t chan_display_mode)
{
	static char s[32];

	switch (chan_display_mode)
	{
		case disp_combined:
			snprintf(s, sizeof(s), "COMBINED  ");
			break;
		case disp_individual:
			snprintf(s, sizeof(s), "INDIVIDUAL");
			break;
		default:
			snprintf(s, sizeof(s), "err %d     ", chan_display_mode);
			break;
	}

	return s;
}

struct event_ret_t set_atten_display_mode(int param, struct menu_info_t *info, enum menu_read_result event)
{
	static int atten_display_mode; // shadow
	struct event_ret_t ret = {0,};

	switch (event)
	{
		case menu_button0_release:
			// toggle between set point and actual
			if (strncmp(menu_mark(info->menu), "~", 1) == 0)
			{
				// entry
				pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
				atten_display_mode = tlsx_info.atten_display_mode; // load shadow atten_display_mode
				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);

				set_menu_mark(info->menu, "*");
			}
			else
			{
				// exit
				pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
				tlsx_info.atten_display_mode = atten_display_mode;
				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);

				set_menu_mark(info->menu, "~");
			}
			wrefresh(stdscr);
			ret.code = 1;
			return ret;

		default:
			// other signals are only processed when changing
			if (strncmp(menu_mark(info->menu), "*", 1) == 0)
			{
				int i;
				ITEM *cur_item = current_item(info->menu);

				switch (event)
				{
					case menu_rot_up_slow:
					case menu_rot_up:
					case menu_rot_up_fast:
					case menu_rot_down_slow:
					case menu_rot_down:
					case menu_rot_down_fast:
						if (atten_display_mode == disp_atten)
							atten_display_mode = disp_power;
						else
							atten_display_mode = disp_atten;
						break;
					default:
						ret.code = 0;
						return ret;
				}

				// update the menu with the new value
				unpost_menu(info->menu);
				snprintf((char *)cur_item->name.str, 32, "%s", atten_display_mode_str(atten_display_mode));
				post_menu(info->menu);
				wrefresh(stdscr);
				ret.code = 1;
				return ret;
			}
			ret.code = 0;
			return ret;
	}
}

struct event_ret_t set_chan_display_mode(int param, struct menu_info_t *info, enum menu_read_result event)
{
	static int chan_display_mode; // shadow
	struct event_ret_t ret = {0,};

	switch (event)
	{
		case menu_button0_release:
			// toggle between set point and actual
			if (strncmp(menu_mark(info->menu), "~", 1) == 0)
			{
				// entry
				pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
				chan_display_mode = tlsx_info.chan_display_mode; // load shadow chan_display_mode
				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);

				set_menu_mark(info->menu, "*");
			}
			else
			{
				// exit
				pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
				tlsx_info.chan_display_mode = chan_display_mode;
				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);

				set_menu_mark(info->menu, "~");
			}
			wrefresh(stdscr);
			ret.code = 1;
			return ret;

		default:
			// other signals are only processed when changing
			if (strncmp(menu_mark(info->menu), "*", 1) == 0)
			{
				int i;
				ITEM *cur_item = current_item(info->menu);

				switch (event)
				{
					case menu_rot_up_slow:
					case menu_rot_up:
					case menu_rot_up_fast:
					case menu_rot_down_slow:
					case menu_rot_down:
					case menu_rot_down_fast:
						if (chan_display_mode == disp_combined)
							chan_display_mode = disp_individual;
						else
							chan_display_mode = disp_combined;
						break;
					default:
						ret.code = 0;
						return ret;
				}

				// update the menu with the new value
				unpost_menu(info->menu);
				snprintf((char *)cur_item->name.str, 32, "%s", chan_display_mode_str(chan_display_mode));
				post_menu(info->menu);
				wrefresh(stdscr);
				ret.code = 1;
				return ret;
			}
			ret.code = 0;
			return ret;
	}
}

struct event_ret_t display_mode_menu(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	int channel_mask = 0;

	switch (event)
	{
		case menu_button0_release:
			{
				int i = 0;
				int l;
				struct menu_item_t menu_items[7];
				struct menu_info_t menu_info =
				{
					menu_items,
					0,
					2 * ONE_SEC,
					NULL,
					DEFAULT_IDLE_TIMEOUT,	// idle timeout
					idle_timeout,
				};
				memset(menu_items, 0, sizeof(menu_items));

				snprintf(menu_items[i].name, 32, "DISPLAY MODES:");
				menu_items[i++].skip = 1;

				pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);

				snprintf(menu_items[i].name, 32, "%s", atten_display_mode_str(tlsx_info.atten_display_mode));
				menu_items[i].cb_param = 0;
				menu_items[i++].cb = set_atten_display_mode;

				snprintf(menu_items[i].name, 32, "%s", chan_display_mode_str(tlsx_info.chan_display_mode));
				menu_items[i].cb_param = 0;
				menu_items[i++].cb = set_chan_display_mode;

				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);

				snprintf(menu_items[i].name, 32, "DONE");
				menu_items[i].cb_param = -BIG_NUMBER;
				menu_items[i++].cb = done;
				menu_info.nitems = i;

				wclear(stdscr);
				ret = _make_menu(&menu_info);
				wclear(stdscr);

				return ret;
			}
		default:
			return ret;
	}
}

struct event_ret_t char_test_menu(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};

	switch (event)
	{
		case menu_button0_release:
			{
				int i = 0;
				struct menu_item_t temp_menu_items[5];
				struct menu_info_t temp_menu_info =
				{
					temp_menu_items,
					0,
					2 * ONE_SEC,
					NULL,
					DEFAULT_IDLE_TIMEOUT,	// idle timeout
					idle_timeout,
				};

				memset(temp_menu_items, 0, sizeof(temp_menu_items));

				snprintf(temp_menu_items[i].name, 32, "LINE1");
				temp_menu_items[i].cb_param = 0;
				temp_menu_items[i++].cb = NULL;
				snprintf(temp_menu_items[i].name, 32, "12 34       ");
				temp_menu_items[i].cb_param = 0;
				temp_menu_items[i++].cb = set_test_char;
				snprintf(temp_menu_items[i].name, 32, "LINE3");
				temp_menu_items[i].cb_param = 0;
				temp_menu_items[i++].cb = NULL;
				snprintf(temp_menu_items[i].name, 32, "DONE");
				temp_menu_items[i].cb_param = -1;
				temp_menu_items[i++].cb = done;
				temp_menu_info.nitems = i;

				wclear(stdscr);
				ret = _make_menu(&temp_menu_info);
				wclear(stdscr);

				return ret;
			}
		default:
			return ret;
	}
}

struct event_ret_t settings_menu(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};

	switch (event)
	{
		case menu_button0_release:
			{
				int i = 0;
				struct menu_item_t settings_menu_items[7];
				struct menu_info_t settings_menu_info =
				{
					settings_menu_items,
					0,
					2 * ONE_SEC,
					NULL,
					DEFAULT_IDLE_TIMEOUT,	// idle timeout
					idle_timeout,
				};

				memset(settings_menu_items, 0, sizeof(settings_menu_items));

				pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);

				snprintf(settings_menu_items[i].name, 32, "DISPLAY");
				settings_menu_items[i].cb_param = 0;
				settings_menu_items[i++].cb = display_mode_menu;
				snprintf(settings_menu_items[i].name, 32, "UNITS");
				settings_menu_items[i].cb_param = 0;
				settings_menu_items[i++].cb = units_menu;
				snprintf(settings_menu_items[i].name, 32, "NETWORKING");
				settings_menu_items[i].cb_param = 0;
				settings_menu_items[i++].cb = network_menu;
				if (disp_combined == tlsx_info.chan_display_mode) {
					snprintf(settings_menu_items[i].name, 32, "WAVELENGTH");
					settings_menu_items[i].cb_param = 0;
					settings_menu_items[i++].cb = wavelength_menu;
					snprintf(settings_menu_items[i].name, 32, "CONTROL MODE");
					settings_menu_items[i].cb_param = 0;
					settings_menu_items[i++].cb = control_mode_menu;
				}
				snprintf(settings_menu_items[i].name, 32, "DONE");
				settings_menu_items[i].cb_param = -1;
				settings_menu_items[i++].cb = done;
				settings_menu_info.nitems = i;

				pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);

				wclear(stdscr);
				ret = _make_menu(&settings_menu_info);
				wclear(stdscr);
				main_menu_timeout(info);

				return ret;
			}
		default:
			return ret;
	}
}

struct event_ret_t main_menu_timeout(struct menu_info_t *info)
{
	struct event_ret_t ret = {0,};
	ITEM **items = menu_items(info->menu);
	struct menu_item_t* mitems = info->mitems;
	int l, i=0;
	int channel_mask;

	unpost_menu(info->menu);

	pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
	if (tlsx_info.vxi_error) {
		// just display settings
		pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
	} else if (tlsx_info.chan_display_mode==disp_combined) {
		channel_mask = tlsx_info.channel_mask;
		pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);

		for (l = 0; l < NUM_CHANNELS; l++)
		{
			if ((channel_mask >> l) & 0x01)
			{
				if (i<info->nitems) {
					snprintf((char *)items[i]->name.str, 32, "%s", channel_value_str(l));
					mitems[i].skip = 0;
					mitems[i].cb_param = l;
					mitems[i++].cb = set_value_channel;
				}
			}
		}
	} else {
		l = tlsx_info.selected_channel;
		enum control_mode_t control_mode = tlsx_info.channel[l].control_mode;
		enum attenuation_mode_t attenuation_mode = tlsx_info.channel[l].attenuation_mode;
		pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
		// CHANNEL 1: ATT
		if (i<info->nitems) {
			snprintf((char *)items[i]->name.str, 32, individual_channel_header_str(l));
			mitems[i].cb_param = l;
			mitems[i++].cb = set_selected_channel;
		}
		// ATTEN  10.00dB
		if (i<info->nitems) {
			snprintf((char *)items[i]->name.str, 32, "%s", individual_atten_str(l));
			mitems[i].skip = 0;
			if (mode_power==control_mode)
				mitems[i].skip = 1;
			mitems[i].cb_param = l;
			mitems[i++].cb = set_value_atten;
		}
		// POWER +10.00dBm
		if (i<info->nitems) {
			snprintf((char *)items[i]->name.str, 32, "%s", individual_power_str(l));
			mitems[i].skip = 0;
			if (mode_atten==control_mode)
				mitems[i].skip = 1;
			mitems[i].cb_param = l;
			mitems[i++].cb = set_value_power;
		}
		// WAVE    1550nm
		if (i<info->nitems) {
			snprintf((char *)items[i]->name.str, 32, "%s", individual_wave_str(l));
			mitems[i].cb_param = l;
			mitems[i++].cb = channel_set_wavelength;
		}
		// CTRL-MODE ATTEN
		if (i<info->nitems) {
			if (mode_atten==control_mode)
				snprintf((char *)items[i]->name.str, 32, "CTRL-MODE ATTEN");
			else
				snprintf((char *)items[i]->name.str, 32, "CTRL-MODE POWER");
			mitems[i].cb_param = l;
			mitems[i++].cb = set_control_mode;
		}
		// MODE-O RELATIVE
		if (i<info->nitems) {
			if (mode_offset==attenuation_mode)
				snprintf((char *)items[i]->name.str, 32, "OFFSET     USER");
			else if (mode_absolute==attenuation_mode)
				snprintf((char *)items[i]->name.str, 32, "OFFSET ABSOLUTE");
			else
				snprintf((char *)items[i]->name.str, 32, "OFFSET RELATIVE");
			mitems[i].cb_param = l;
			mitems[i++].cb = set_attenuation_mode;
		}
		//   ^->  10.00dBm
		if (i<info->nitems) {
			snprintf((char *)items[i]->name.str, 32, "%s", individual_attenuation_offset_str(l));
			mitems[i].cb_param = l;
			if (mode_offset==attenuation_mode) {
				mitems[i].skip = 0;
				mitems[i++].cb = set_value_attenuation_offset;
			} else {
				mitems[i].skip = 1;
				mitems[i++].cb = NULL;
			}
		}
	}
	// SETTINGS
	if (i<info->nitems) {
		snprintf((char *)items[i]->name.str, 32, "SETTINGS");
		mitems[i].cb_param = 0;
		mitems[i++].cb = settings_menu;
	}
	// we don't update info->nitems because nitems lets _make_menu free some things
	// when it unwinds the stack, messing with this anywhere other than main_menu
	// when it is called by screen_saver can cause memory leaks and segfaults
	// in fact, editing mitems at all is a bit dodgy. This is why there is
	// (i<info->nitems) gates around each line. There is no good way to
	// generalise what nitems should be other than this method since nitems can
	// be different depending on the channel mask

	post_menu(info->menu);
	wrefresh(stdscr);
	return ret;
}

struct event_ret_t main_menu(int param, struct menu_info_t *info, enum menu_read_result event)
{
	struct event_ret_t ret = {0,};
	int channel_mask;

	switch (event)
	{
		case menu_rot_up_slow:
		case menu_rot_up:
		case menu_rot_up_fast:
		case menu_rot_down_slow:
		case menu_rot_down:
		case menu_rot_down_fast:
		case menu_button0_release:
			{
				int i = 0;
				int l;
				struct menu_item_t main_menu_items[10];
				struct menu_info_t main_menu_info =
				{
					main_menu_items,
					0,
					2 * ONE_SEC,
					main_menu_timeout,
					DEFAULT_IDLE_TIMEOUT,	// idle timeout
					idle_timeout,
				};
				memset(main_menu_items, 0, sizeof(main_menu_items));

				pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
				if (tlsx_info.vxi_error) {
					// just display settings
					pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
				} else if (tlsx_info.chan_display_mode==disp_combined) {
					channel_mask = tlsx_info.channel_mask;
					pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
					for (l = 0; l < NUM_CHANNELS; l++)
					{
						if ((channel_mask >> l) & 0x01)
						{
							snprintf(main_menu_items[i].name, 32, "%s", channel_value_str(l));
							main_menu_items[i].skip = 0;
							main_menu_items[i].cb_param = l;
							main_menu_items[i++].cb = set_value_channel;
						}
					}
				} else {
					l = tlsx_info.selected_channel;
					enum control_mode_t control_mode = tlsx_info.channel[l].control_mode;
					enum attenuation_mode_t attenuation_mode = tlsx_info.channel[l].attenuation_mode;
					pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);

					snprintf(main_menu_items[i].name, 32, individual_channel_header_str(l));
					main_menu_items[i].cb_param = l;
					main_menu_items[i++].cb = set_selected_channel;
					snprintf(main_menu_items[i].name, 32, "%s", individual_atten_str(l));
					main_menu_items[i].skip = 0;
					if (mode_power==control_mode)
						main_menu_items[i].skip = 1;
					main_menu_items[i].cb_param = l;
					main_menu_items[i++].cb = set_value_atten;
					snprintf(main_menu_items[i].name, 32, "%s", individual_power_str(l));
					main_menu_items[i].skip = 0;
					if (mode_atten==control_mode)
						main_menu_items[i].skip = 1;
					main_menu_items[i].cb_param = l;
					main_menu_items[i++].cb = set_value_power;
					snprintf(main_menu_items[i].name, 32, "%s", individual_wave_str(l));
					main_menu_items[i].cb_param = l;
					main_menu_items[i++].cb = channel_set_wavelength;
					if (mode_atten==control_mode)
						snprintf(main_menu_items[i].name, 32, "CTRL-MODE ATTEN");
					else
						snprintf(main_menu_items[i].name, 32, "CTRL-MODE POWER");
					main_menu_items[i].cb_param = l;
					main_menu_items[i++].cb = set_control_mode;

					if (mode_offset==attenuation_mode)
						snprintf(main_menu_items[i].name, 32, "OFFSET     USER");
					else if (mode_absolute==attenuation_mode)
						snprintf(main_menu_items[i].name, 32, "OFFSET ABSOLUTE");
					else
						snprintf(main_menu_items[i].name, 32, "OFFSET RELATIVE");
					main_menu_items[i].cb_param = l;
					main_menu_items[i++].cb = set_attenuation_mode;
					snprintf(main_menu_items[i].name, 32, "%s", individual_attenuation_offset_str(l));
					main_menu_items[i].cb_param = l;
					if (mode_offset==attenuation_mode) {
						main_menu_items[i].skip = 0;
						main_menu_items[i++].cb = set_value_attenuation_offset;
					} else {
						main_menu_items[i].skip = 1;
						main_menu_items[i++].cb = NULL;
					}
				}
				snprintf(main_menu_items[i].name, 32, "SETTINGS");
				main_menu_items[i].cb_param = 0;
				main_menu_items[i++].cb = settings_menu;

				main_menu_info.nitems = i;
				wclear(stdscr);
				_make_menu(&main_menu_info);
				wclear(stdscr);
			}
			return ret;

		default:
			return ret;
	}
}

int screen_saver(void)
{
	int channel_mask;
	wclear(stdscr);
	while (1)
	{
		int l;
		int y = 0;

		pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
		if (tlsx_info.vxi_error) {
			pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
			wmove(stdscr, y++, 0);
			wmove(stdscr, y++, 0);
			printw("   COULD  NOT");
			wmove(stdscr, y++, 0);
			printw("   INITIALIZE");
			wmove(stdscr, y++, 0);
			printw("    HARDWARE");
		}else if (tlsx_info.chan_display_mode==disp_combined) {
			channel_mask = tlsx_info.channel_mask;
			char busy_char = ' ';
			if (tlsx_info.channel[l].busy)
				busy_char = '*';
			pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
			for (l = 0; l < NUM_CHANNELS; l++)
			{
				if ((channel_mask >> l) & 0x01)
				{
					wmove(stdscr, y++, 0);
					printw(" %s", channel_value_str(l));
				}
			}
		} else {
			l = tlsx_info.selected_channel;
			pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);

			wmove(stdscr, y++, 0);
			printw(" %s", individual_channel_header_str(l));
			wmove(stdscr, y++, 0);
			printw(" %s", individual_atten_str(l));
			wmove(stdscr, y++, 0);
			printw(" %s", individual_power_str(l));
			wmove(stdscr, y++, 0);
			printw(" %s", individual_wave_str(l));
		}
		wrefresh(stdscr);

		enum menu_read_result r = _menu_read(1*ONE_SEC);
		main_menu(0, NULL, r);
	}
}


int pid_fd = 0;
char pid_filename[64];
void sig_handler(int sig)
{
	switch (sig)
	{
		case SIGINT:
		case SIGTERM:
			///@todo if anyone feels like being tidy this could shutdown all the threads / sockets
			/// etc nicely
			if (pid_fd != 0)
				close(pid_fd);
			remove(pid_filename);
			exit(EXIT_SUCCESS);
		default:
			// unhandled sig
			break;
	}
}


int daemonize(char *name)
{
	char pid_str[64];

	// reg signal handlers
	signal(SIGINT, sig_handler);
	signal(SIGTERM, sig_handler);

	// use unistd daemon maker for all the boring work
	if (daemon(0, 0) == -1)
	{
		syslog(LOG_INFO, "failboat cannot make %s a daemon\n", name);
		return -1;
	}

	// make the pid file
	snprintf(pid_filename, sizeof(pid_filename), "/tmp/%s.pid", name);
	pid_fd = open(pid_filename, O_RDWR|O_CREAT, 0600);
	if (pid_fd == -1)
	{
		syslog(LOG_INFO, "failboat cannot open pid file %s\n", pid_filename);
		return -1;
	}
	if (lockf(pid_fd, F_TLOCK, 0) == -1)
	{
		syslog(LOG_INFO, "failboat cannot lock pid file %s\n", pid_filename);
		return -1;
	}
	sprintf(pid_str, "%d\n", getpid());
	write(pid_fd, pid_str, strlen(pid_str));

	return 0;
}


void usage(void)
{
	printf("menu [OPTION]\n");
	printf("\t-f\t\tby default menu will daemonise, if -f is present stay in the foreground\n");
	printf("\t-h\t\tshow this message\n");
}

const char* couldNotInitializeHardwareMessage =
"\eJ\n\r   COULD  NOT\n\r   INITIALIZE\n\r    HARDWARE";

void display_error(const char* message) {
	FILE *fout;
	fout = fopen("/dev/lcd", "r+");
	if (fout == NULL)
		return;
	if (message==NULL)
		return;
	//int length = strlen(message);
	fprintf(fout, "%s", message);
	fclose(fout);
}

#define MY_TERM
#ifdef MY_TERM
#define TERM "vtlcd"
#define OUT (fout)
#define IN (NULL)
#else
#define TERM (NULL)
#define OUT (stdout)
#define IN (stdin)
#endif

#define fail(r,f) {ret = r; goto fail##f;}
int main(int argc, char **argv)
{
	// init structs
	int l;
	int ret;
	FILE *fin, *fout;
	struct sched_param sp;
	int opt;
	bool become_daemon = true;

	// process command line options
	while ((opt = getopt(argc, argv, "hf")) != -1)
	{
		switch (opt)
		{
			case 'f':
				become_daemon = false;
				break;
			case 'h':
			default:
				usage();
				return 0;
		}
	}


	// daemonize
	getcwd(mwd, sizeof(mwd));
	syslog(LOG_INFO, "mwd is %s\n", mwd);
	if (become_daemon)
	{
		if (daemonize("menu") == -1)
			exit(EXIT_FAILURE);
	}


	// set this program up to run really fast so the menu seems smoother :)
	sp.sched_priority = sched_get_priority_max(SCHED_FIFO);
	sched_setscheduler(0, SCHED_FIFO, &sp);

	// this module is shared between menu_fd and this file so call it here
	lcp_init();

	memset(&tlsx_info, 0, sizeof(tlsx_info));
	// start vxi thread to poll tlx info and wait for it to start
	if (pthread_mutex_init(&tlsx_info.tlsx_info_mutex, NULL))
		fail(-1, 1);
	if (pthread_create(&tlsx_info.vxi_thread, NULL, vxi_poll, NULL))
		fail(-1, 2);
	while (1)
	{
		pthread_mutex_lock(&tlsx_info.tlsx_info_mutex);
		if (tlsx_info.vxi_error)
			break;
		/*if (tlsx_info.vxi_error) {
			display_error(couldNotInitializeHardwareMessage);
			exit(EXIT_FAILURE);
		}*/
		if (tlsx_info.vxi_event)
			break;
		pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);
		usleep(100000);
	}
	tlsx_info.power_units = power_units_dbm;
	tlsx_info.atten_display_mode = disp_power;
	tlsx_info.chan_display_mode = disp_individual;
	tlsx_info.selected_channel = 0;

	for (l = 0; l < NUM_CHANNELS; l++)
	{
		if ((tlsx_info.channel_mask >> l) & 0x01)
		{
			tlsx_info.channel[l].atten_busy = 0;
			tlsx_info.channel[l].power_busy = 0;
			tlsx_info.channel[l].busy = 0;
		}
	}
	pthread_mutex_unlock(&tlsx_info.tlsx_info_mutex);

	// setup io
	if ((mfpio = menu_open(&fin, &fout)) == NULL)
		fail(-2, 2);

	// setup ncurses
	SCREEN *s = newterm(TERM, OUT, IN);
	if (s == NULL)
		fail(-3, 2);
	set_term(s);
	cbreak();
	noecho();
	curs_set(0);
	keypad(stdscr, TRUE);

	// start our menu interface via the screen saver
	screen_saver();
	wclear(stdscr);
	wrefresh(stdscr);

	endwin();
	delscreen(s);
	return 0;

fail2:
	pthread_mutex_destroy(&tlsx_info.tlsx_info_mutex);
fail1:
	return ret;
}

