/***********************************************************************************
* Project    : TLSX
************************************************************************************
* File       : 
* Author     : u.malik@coherent-solutions.com
* Company    : Coherent Solutions
* Created    : 
************************************************************************************
* Description: 
* Basic data structure to store blade state.
************************************************************************************
* Copyright 2013, Coherent Solutions www.coherent-solutions.com
* All rights reserved.
************************************************************************************/

#ifndef BLADE_H
#define BLADE_H

#include "laser.h"

#define STATIC_PARAMETER_SIZE 4
#define MAX_NUMBER_OF_LASERS 5 //Laser 0 is considered a blade level command. We align laser index with laser
//number for code simplicity.

#define NOT_PRESENT 0 
#define PRESENT     1
#define BOOTMODE 2

/**This structure is used to store the current state of a blade.**/
typedef struct
{
  
  /**Global details about the target device.**/
	uint8_t present;
	uint32_t fw_rev_major;
	uint32_t fw_rev_minor;
	uint8_t status;
	uint8_t model[32];
	uint8_t hw_version[32];
	laser_t lasers[MAX_NUMBER_OF_LASERS]; 
	
} blade_t;


#endif
