/**********************************************************************************
* Project    : TLSX
***********************************************************************************
* File       : 
* Author     : u.malik@coherent-solutions.com
* Company    : Coherent Solutions
* Created    : 
***********************************************************************************
* Description: 
* 
***********************************************************************************
* Copyright 2013, Coherent Solutions www.coherent-solutions.com
* All rights reserved.
***********************************************************************************/

#ifndef CLIENT_MANAGER_H
#define CLIENT_MANAGER_H

#include "TLSXServer.h"

//All functions defined in this header are private to client_manager. Globally accesible 
//functions are declarfed in TLSXServer.h

void handle_client_transaction(uint8_t* to, uint8_t* from);
uint8_t check_data_ranges(uint8_t* from_user, uint8_t* to_user);
void signal_handler(int signal_no);



#endif
