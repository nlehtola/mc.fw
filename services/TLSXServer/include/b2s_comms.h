/**********************************************************************************
* Project    : TLSX
***********************************************************************************
* File       : 
* Author     : u.malik@coherent-solutions.com
* Company    : Coherent Solutions
* Created    : 
***********************************************************************************
* Description: 
* 
***********************************************************************************
* Copyright 2013, Coherent Solutions www.coherent-solutions.com
* All rights reserved.
***********************************************************************************/

#ifndef B2S_COMMS
#define B2S_COMMS


/***********************************************************************************/
//Global defs. 
#define PAYLOAD_SIZE 16

#define B2SSERVER_COMMS_CONNECTED    0
#define B2SSERVER_COMMS_UNCONNECTED  1
#define B2SSERVER_SOCKET_ERROR       2
#define B2SSERVER_CONNECT_ERROR      3
#define B2SSERVER_SEND_ERROR         4 
#define B2SSERVER_RECEIVE_ERROR      5 

#define ERROR                        1


//These commands are for B2S Server itself are not used by B2S. These commands 
//essentially call different functions in b2s wrapper.
#define B2SS_ECHO                    128
#define B2SS_TXRX                    129
#define B2SS_COMMAND                 130
#define B2SS_QUERY                   131
#define B2SS_BLADE_PRESENT           132 
#define B2SS_LASER_PRESENT           133 


void gen_random_bytes(int length, uint8_t* o_data);
int  check_bytes(int length, uint8_t* a, uint8_t* b);

int  frame_txrx(uint8_t* i_b2s, uint8_t* o_b2s,int mode);


#endif
