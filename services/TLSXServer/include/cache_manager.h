/***********************************************************************************
* Project    : TLSX
************************************************************************************
* File       : 
* Author     : u.malik@coherent-solutions.com
* Company    : Coherent Solutions
* Created    : 
************************************************************************************
* Description: 
* All functions that are globally accessible are declared in TLSXServer.h. 
* This file declares functions for several files as listed in the comments below. 
************************************************************************************
* Copyright 2013, Coherent Solutions www.coherent-solutions.com
* All rights reserved.
************************************************************************************/


#ifndef CACHE_MANAGER_H
#define CACHE_MANAGER_H

#include "TLSXServer.h"
#include "blade.h" //This declares blade data structure which is only accessed by the 
//cache manager

//Note: All functions in cache_manager.c are declared in TLSXServer.h so that they can 
//be used by other modules. The functions declared here are meant to be used by cache
//manager itself and not any outside module. 

/***********************************************************************************/
//blade_discovery.c
int init_blade(uint8_t blade_id, blade_t* blade);
void find_blade(uint8_t slot_id, uint8_t* present);
void find_laser(uint8_t blade_id, uint8_t laser_id, uint8_t* present);
void turn_safety_on(uint8_t blade_id, uint8_t laser_id);
int get_blade_fw_rev(uint8_t slot_id, uint32_t* major, uint32_t* minor);
void get_rom_parameter(uint8_t blade_id, uint8_t command, uint8_t* rom_data);
void check_bootmode();
void print_blade_versions();
/***********************************************************************************/
//cache_init.c
void get_static_parameters();
void check_static_parameters();
void check_static_parameter(laser_parameter_t*);
void opt_unit_command();
void opt_blade_command(uint8_t i_blade_id);
void retrieve_parameter_pipelined(uint8_t command);
/***********************************************************************************/
//util.c
void print_laser(laser_t*);
void print_parameter(laser_parameter_t*, char*,uint8_t);
void print_value(uint8_t*,uint8_t);
/***********************************************************************************/
//cache_bypass_mode.c
void handle_bypass_mode(uint8_t* from_client, uint8_t* to_client);
void unit_opc(uint8_t* opc_status);
void blade_opc(uint8_t blade_id,uint8_t* opc_status);
void laser_opc(uint8_t blade_id, uint8_t laser_id, uint8_t* opc_status);
void map_status_code(uint8_t* status);
/***********************************************************************************/
//cache_normal_mode.c
void handle_cached_mode(uint8_t* from_client, uint8_t* to_client);
void handle_get_parameter_if_valid(uint8_t* from_client, uint8_t* to_client);
void handle_get_parameter(uint8_t* from_client, uint8_t* to_client);
void handle_set_parameter(uint8_t* from_client, uint8_t* to_client);
void handle_set_frequency(uint8_t* from_client, uint8_t* to_client);
void handle_set_power(uint8_t* from_client, uint8_t* to_client);
void handle_enable(uint8_t* from_client, uint8_t* to_client);
/***********************************************************************************/
//command_to_data_mapper.c
int set_parameter_valid(uint8_t blade_id, uint8_t laser_idx, uint8_t command, uint8_t data);
int get_parameter_valid(uint8_t blade_id, uint8_t laser_idx, uint8_t command, uint8_t *data);
int set_parameter(uint8_t blade_id, uint8_t laser_idx, uint8_t command, uint8_t* data);
int get_parameter(uint8_t blade_id, uint8_t laser_idx, uint8_t command, uint8_t* data);
uint8_t classify_command(uint8_t command);
uint8_t check_dynamic_parameter_in_range(uint8_t blade_id, uint8_t laser_idx, uint8_t command, uint8_t* value);

#define GET_PARAMETER_IF_VALID 1
#define GET_PARAMETER 2
#define ENABLE_COMMAND 3
#define SET_PARAMETER 4
#define OPC_COMMAND 5
#define SET_FREQ_COMMAND 6
#define SET_POWER_COMMAND 7 
#define CACHE_BYPASS 8
/***********************************************************************************/
//cache_state_monitor.c
void state_update_thread();
void update_state_parameters();
/***********************************************************************************/


#endif
