/***********************************************************************************
* Project    : TLSX
************************************************************************************
* File       : 
* Author     : u.malik@coherent-solutions.com
* Company    : Coherent Solutions
* Created    : 
************************************************************************************
* Description: 
* Basic data structure for strong laser state.
************************************************************************************
* Copyright 2013, Coherent Solutions www.coherent-solutions.com
* All rights reserved.
************************************************************************************/

#ifndef LASER_H
#define LASER_H

#include <stdint.h>

#define STATIC_PARAMETER_SIZE 4

/**We always store raw bytes and convert to an appropriate format only when required.**/
typedef struct
{
	uint8_t min[4];
	uint8_t max[4];
	uint8_t static_data_valid;
	uint8_t requested[4];
	uint8_t actual[4];
	uint8_t dynamic_data_valid;
 
} laser_parameter_t;


/**This structure is used to store the current state of a laser.**/
typedef struct
{
	//local parameters
	uint8_t present; 
	
	/**State storing variables.**/
	uint8_t status;
	uint8_t enable[4];
	uint8_t safety[4];
	uint8_t control_dither[4];
	uint8_t sbs_dither_enable[4]; 
	laser_parameter_t temperature;
	laser_parameter_t power;
	laser_parameter_t frequency;
	laser_parameter_t grid;
	laser_parameter_t fine_frequency;
	laser_parameter_t sbs_dither_rate;
	laser_parameter_t sbs_dither_frequency;


} laser_t;

#define TEMPERATURE_MIN 100 //1 degree C is considered min
#define TEMPERATURE_MAX 8500 //85 degree C is considered max. 






#endif
