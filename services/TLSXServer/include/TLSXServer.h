/**********************************************************************************
* Project    : TLSX
***********************************************************************************
* File       : 
* Author     : u.malik@coherent-solutions.com
* Company    : Coherent Solutions
* Created    : 
***********************************************************************************
* Description: 
* 
***********************************************************************************
* Copyright 2013, Coherent Solutions www.coherent-solutions.com
* All rights reserved.
***********************************************************************************/

#ifndef MINI_TLSX
#define MINI_TLSX


#include "common.h"
#include <string.h>
#include "error_codes.h"
#include "time.h"
#include "b2s.h"
#include "b2s_comms.h"

/***********************************************************************************/
//Global defs. 
#define MAX_NUMBER_OF_BLADES 16
#define MAX_NUMBER_OF_LASERS 5 //Laser 0 considered a blade level command
#define PAYLOAD_SIZE 16
#define MAX_NUM_COMMANDS 2 

#define SOCKET_RETRIES  50
#define SLEEP_TIME      5

#define B2S_SERVER_PORT 10017
#define TLSX_SERVER_PORT 10001


//Additional commands between SCPI server and TLSX
#define GET_UNIT_OPC 128
#define GET_BLADE_OPC 129
#define GET_LASER_OPC 130
#define CHASSIS_OPT 131
#define BLADE_OPT 132


#define B2SSERVER_COMMS_CONNECTED    0
#define B2SSERVER_COMMS_UNCONNECTED  1
#define B2SSERVER_SOCKET_ERROR       2
#define B2SSERVER_CONNECT_ERROR      3
#define B2SSERVER_SEND_ERROR         4 
#define B2SSERVER_RECEIVE_ERROR      5 

#define STATUS_QUERY_OK              5

//These commands are for B2S Server itself are not used by B2S. These commands 
//essentially call different functions in b2s wrapper.
#define B2SS_COMMAND_INDEX           15 //We steal byte[15] of the outgoing frame to 
                                        //send instructions for B2SS.

#define B2SS_ECHO                    128
#define B2SS_TXRX                    129
#define B2SS_COMMAND                 130
#define B2SS_QUERY                   131
#define B2SS_BLADE_PRESENT           132 
#define B2SS_LASER_PRESENT           133 

//On return one of the following status can be present
#define B2SS_NO_ERROR                140
#define B2SS_UNKNOWN_COMMAND         141
#define B2SS_ERROR                   142
#define B2SS_COMMAND_CRC_ERROR       143
#define B2SS_QUERY_CRC_ERROR         144
#define B2SS_BLADE_IS_PRESENT           145 
#define B2SS_LASER_IS_PRESENT           146 
#define B2SS_BLADE_IS_NOT_PRESENT           147 
#define B2SS_LASER_IS_NOT_PRESENT           148 


/***********************************************************************************/
//Functions implemented in TLSXServer.c
void tlsx_init();

/***********************************************************************************/
//Commands to be run by client manager
uint8_t cm_comms_handler();

/***********************************************************************************/
//Cache manager
void cache_init(); 
void cache_handle_client(uint8_t* from_client, uint8_t* to_client);
void cache_print();
void cache_start_monitoring();

/***********************************************************************************/
//B2SCommunication module. 

int b2ss_comms_init();
int b2ss_comms_open();
int b2ss_comms_close();

//Different functions for calling B2S services. 
int b2ss_command(uint8_t i_blade_id,
		uint8_t i_laser_id, 
                uint8_t i_sequence,
		uint8_t i_command,
		uint8_t* i_data, //4 bytes of data to B2S
		uint8_t* o_status); //command status

int b2ss_query(uint8_t i_blade_id,
	      uint8_t i_sequence,
	      uint8_t* o_data,
	      uint8_t* o_status);

//A single function to encompase both command and query phase of B2S
int b2ss_command_query(uint8_t i_blade_id,
		      uint8_t i_laser_id, 
		      uint8_t i_sequence,
		      uint8_t i_command,
		      uint8_t* i_data, //4 bytes of data to B2S
	      	      uint8_t* o_data,//4 bytes of data from B2S
		      uint8_t* o_status); //command status
int  b2ss_echo();
/***********************************************************************************/





#endif
