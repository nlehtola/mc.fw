/***********************************************************************************
* Project    : TLSX
************************************************************************************
* File       : 
* Author     : u.malik@coherent-solutions.com
* Company    : Coherent Solutions
* Created    : 
************************************************************************************
* Description: 
* The functions in this file allow the TLSX server to be run in B2SServe (no cache)
* mode. This can be useful for debugging purposes. 
************************************************************************************
* Copyright 2013, Coherent Solutions www.coherent-solutions.com
* All rights reserved.
************************************************************************************/
#include "cache_manager.h"

extern blade_t blades[MAX_NUMBER_OF_BLADES];
extern int verbose;

/***********************************************************************************/

/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * This is added mainly as a debug aid. 
 * @assumptions
 * In bypass mode we assume that there is no update thread running
 **/

void handle_bypass_mode(uint8_t* from_client, uint8_t* to_client){

//Four commands need special attention, OPC (unit, blade and laser) and temperature. 
	uint8_t blade; 
	uint8_t laser;
	uint8_t sequence;
	uint8_t command;
	uint8_t to_b2s[4];
	uint8_t from_b2s[4];
	uint8_t status;
	int j;
	uint8_t zeros[3] = {0};

	blade = from_client[0];
	laser = from_client[1];
	sequence = 0;
	command = from_client[3];
	
	to_client[0]  = blade;
	to_client[1]  = laser;
	to_client[2]  = from_client[2];
	
 
  switch (command)
           {
	   case GET_UNIT_OPC:
	     spLogDebug(3,"Unit OPC called.\n");
	     unit_opc(&to_client[4]);
	     to_client[3] = B2S_NO_ERROR;
	     uint8a_to_uint8a(zeros,&to_client[5],3);
	     return;
	     
	   case GET_BLADE_OPC:
	     spLogDebug(3,"Blade OPC called.\n");
	     blade_opc(blade,&to_client[4]);
	     uint8a_to_uint8a(zeros,&to_client[5],3);
	     to_client[3] = B2S_NO_ERROR;
	     return;
	   case GET_LASER_OPC:
	     spLogDebug(3,"Laser OPC called.\n");
	     laser_opc(blade,laser,&to_client[4]);
	     uint8a_to_uint8a(zeros,&to_client[5],3);
	     to_client[3] = B2S_NO_ERROR;
	     return;

           default:
	     break;
           }


//No we need to perform a b2ss transaction	
  
  uint8a_to_uint8a(&from_client[4], to_b2s,4);
  b2ss_command_query(blade, laser, sequence, command, to_b2s, from_b2s, &status);

  //Sometimes temperature command send back all zeros. Add a check..
  if (command==GET_TEMPERATURE){
	  for(j=0;j<8;j++){
		  if(status==STATUS_QUERY_OK && 1==is_zero(&from_b2s,4))
			  break;; //We have good data
		  spLogDebug(3,"Temperature. Status: %u, j:%u\n", status,j);
//else we retry
		  b2ss_command_query(blade, laser, sequence, command, to_b2s, from_b2s, &status);
				  
	  }//for loop
  }//Check for temperature
  
  //Now update the frame sent back to the client. 

  to_client[3]  = status;
  map_status_code(&to_client[3]);//The status codes between TLSX and Client are different
  spLogDebug(3,"Final status: %u\n", status);
  uint8a_to_uint8a(from_b2s, &to_client[4],4);

  return; 
}

/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/


void unit_opc(uint8_t* opc_status){


  uint8_t tx_data[4];
  uint8_t rx_data[4];
  uint8_t status; 
  uint8_t laser = 0;
  uint8_t sequence = 1;
  int i;
  uint8_t blade_opc_status = 0;

  *opc_status = 1;


  for(i=1;i<16;i++){
    if(blades[i].present == 0)
      continue;
    blade_opc(i, &blade_opc_status);
    if(blade_opc_status==0){
      *opc_status = 0;
      return;
    }
  }

    return;

}

/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/


void blade_opc(uint8_t blade_id,uint8_t* opc_status){

	uint8_t tx_data[4];
	uint8_t rx_data[4];
	uint8_t status; 
	uint8_t laser = 0;
	uint8_t sequence = 1;
	int i;
	uint8_t laser_opc_status = 0;
	
	*opc_status = 1;
	
	//first check if this blade and laser are even present
	if (blades[blade_id].present == 0){
		spLogDebug(3,"OPC: blade/laser do not exist or do not responde. \n");
		return;
	} 
	
	for(i=1;i<5;i++){
		spLogDebug(3,"OPC laser: %u ", i);
		//check if the laser is present;
		if (blades[blade_id].lasers[i].present==0){
			spLogDebug(3,"Not present.\n");
			continue;
		}
		laser_opc(blade_id, i, &laser_opc_status);
		if(laser_opc_status==0){
			*opc_status = 0;
			return;
		}
	}
	
	return;
	
}//blade_opc command

/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/


void laser_opc(uint8_t blade_id, uint8_t laser_id, uint8_t* opc_status){

	uint8_t tx_data[4];
	uint8_t rx_data[4];
	uint8_t status; 
	uint8_t laser = 0;
	uint8_t sequence = 1;
	uint8_t laser_pending = 0 ;
	uint8_t laser_on = 0;
	uint8_t laser_lock = 0;
	int i;
	
	*opc_status = 0;
	
	spLogDebug(3,"Laser OPC called. Blade:%u Laser:%u", blade_id, laser_id);

	//first check if this blade and laser are even present
	if (blades[blade_id].present == 0 | blades[blade_id].lasers[laser_id].present == 0){
		spLogDebug(3,"OPC: blade/laser do not exist or do not responde. \n");
		return;
	} 
	
	//We need to find laser satatus and laser lock
	
	b2ss_command_query(blade_id, laser_id, sequence,GET_LASER_STATUS,tx_data, rx_data,&status );
	if(STATUS_QUERY_OK!=status){
		spLogDebug(3,"B2SS Query failed for laser state.");
		return;
	}
	laser_pending = rx_data[0]&4;
	spLogDebug(3,"Laser pending:%u\n", laser_pending);

	b2ss_command_query(blade_id, laser_id, sequence,GET_FREQUENCY_LOCKED,tx_data, rx_data,&status );
	if(STATUS_QUERY_OK!=status){
		spLogDebug(3,"B2SS Query failed for laser LOCK.");
		return;
	}

	laser_lock = rx_data[0]&1;
	spLogDebug(3,"Laser lock:%u\n", rx_data[0]);

	if (blades[blade_id].lasers[laser_id].enable[0]) {
	  *opc_status =   (!laser_pending) && laser_lock;
		spLogDebug(3,"OPC status :%u\n", *opc_status);
	}
	else {
		*opc_status = !laser_pending;
		spLogDebug(3,"OPC status :%u\n", *opc_status);
	}
	
	return;
}//laser_opc command

/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * SCPI interface has different status numbers so we need to map
 * 
 **/
void map_status_code(uint8_t* status){

  switch (*status)
           {
           case STATUS_QUERY_OK:
		   *status = B2S_NO_ERROR;
		   break;
           case STATUS_EXEC_ERROR:
		   *status = B2S_EXEC_ERROR;
		   break;
           case STATUS_COMMAND_PENDING:
		   *status = B2S_NO_ERROR; //This is also considered an OK state.
		   break;
           case STATUS_UNKNOWN_COMMAND:
		   *status = B2S_INVALID_COMMAND;
		   break;
           default:
		   spLogDebug(3,"B2S returned status: %u\n", *status);	     
		   *status = B2S_UNKNOWN_ERROR;
           }    
        
    return;

}
/***********************************************************************************/
