/***********************************************************************************
* Project    : TLSX
************************************************************************************
* File       : 
* Author     : u.malik@coherent-solutions.com
* Company    : Coherent Solutions
* Created    : 
************************************************************************************
* Description: 
* Runs continuosly to update the state of the cache. 
************************************************************************************
* Copyright 2013, Coherent Solutions www.coherent-solutions.com
* All rights reserved.
************************************************************************************/
#include "cache_manager.h"

extern int verbose;
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * This thread runs continuosuly and updates blades data structure regularly. 
 * 
 **/


void state_update_thread(){

	spLogDebug(3,"State update thread started....\n");
	int i=0;

	while(1){		
		sleep(1);
		update_state_parameters();
		i++;
		//cache_print();
	}

}

/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/

void update_state_parameters(){
	
	int i;
	
	//These parameters have a 4 byte value associated with them
	uint8_t dynamic_parameters[] = {
		GET_POWER_ACTUAL,
		GET_GRID,
		GET_FINE_FREQUENCY,
		GET_SBS_RATE,
		GET_SBS_FREQUENCY,
		GET_TEMPERATURE,
		GET_FREQUENCY_REQUESTED,
		GET_FREQUENCY_LOCKED,
		GET_CONTROL_DITHER,
		GET_SBS_DITHER,
		GET_LASER_ON_OFF,
		GET_SAFETY
	};
	uint8_t num_dynamic_parameters = sizeof(dynamic_parameters)/sizeof(uint8_t);;
	//printf("Num dynamic parameters: %u\n",  num_dynamic_parameters);
	//For the time being we use retrive_parameter_pipelined method.
	//todo: check that dynamic data is within ranges. 
	for(i=0;i<num_dynamic_parameters;i++)
		retrieve_parameter_pipelined(dynamic_parameters[i]);
	

}

/***********************************************************************************/
