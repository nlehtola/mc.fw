/***********************************************************************************
* Project    : TLSX
************************************************************************************
* File       : 
* Author     : u.malik@coherent-solutions.com
* Company    : Coherent Solutions
* Created    : 
************************************************************************************
* Description: 
* 
************************************************************************************
* Copyright 2013, Coherent Solutions www.coherent-solutions.com
* All rights reserved.
************************************************************************************/


#include "cache_manager.h"

extern int verbose;
extern int cache_bypass;

/***********************************************************************************/

/**Ideally these variables should be static but cache_manager grew bigger and bigger and 
needed to be split in several files for ease of reading and maintainance. The files 
involved are:
* blade_discovery (scans the bus and finds blades/lasers)
* cache_init (calls blade_discovery, retrieves static parameters from each blade/laser)
* cache_bypass_mode (a debug aid. We can start the server in this mode and it will behave
  like the current B2SServer) 
* cache_normal_mode (contains functions to fulfill client's request via cache and update it.)
* command_to_data_mapper (contains functions to determine how to update/access the cache)
* cache state monitor (a thread that continuously updates the state of the cache for each blade.)
**/

//A bit vector for blades that are present. 
uint32_t blades_present; 
//A bit vector per blade to identify which lasers are present. 
uint32_t lasers_present[MAX_NUMBER_OF_BLADES];
//Main data strcuture to hold the state of each blade. 
blade_t blades[MAX_NUMBER_OF_BLADES];

//Read and write threads on this would require this lock. 
pthread_mutex_t cache_lock;


/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * This will call functions from blade discovery as well as cache_init to populate the 
 * blades data structure. 
 **/

void cache_init(){
	int i;
	int error;
	pthread_t thread_id;
	

	for(i=1;i<MAX_NUMBER_OF_BLADES;i++){
		printf("Scaning for Blade: %u\n", i);
		init_blade(i, &blades[i]); //init_blade function is implemented in blade_discovery.c

	}


	 //Now fill in OPT data for blades and lasers.

	opt_unit_command();
	for(i=1;i<MAX_NUMBER_OF_BLADES;i++)
		opt_blade_command(i);
	
	spLogNotice("OPT blade: %x \n", blades_present);
	for(i=1;i<MAX_NUMBER_OF_BLADES;i++)
		if (1==blades[i].present)
			spLogNotice("Blade %u: OPT laser: %x \n", i, lasers_present[i]);
	//Only retreive these parameters if we are not in bypass mode.
	if(0==cache_bypass){
	
		//We now populate all static parameters		

		spLogDebug(3,"Initialising Cache Contents.....\n");       
		get_static_parameters();
		check_static_parameters();
		spLogDebug(3,"PRINTING CACHE...\n");
		cache_print();
		spLogDebug(3,"Returning from cache init\n");
		return;
	}
	//else we don't do anything
	return;

}

/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/

void cache_print(){
	
	int laser_id; 
	int blade_id; 


	for(laser_id=1;laser_id<MAX_NUMBER_OF_LASERS;laser_id++){
		//	spLogDebug(3,"laser_id: %u\n", laser_id);
		for(blade_id=1;blade_id<MAX_NUMBER_OF_BLADES; blade_id++){
			//spLogDebug(3,"blade_id: %u\n", blade_id);
			if (1==blades[blade_id].present && 1==blades[blade_id].lasers[laser_id].present){
				//spLogDebug(3,"Blade_ID: %u, Laser ID: %u\n", blade_id,blades[blade_id].lasers[laser_id].laser_id);
			print_laser(&(blades[blade_id].lasers[laser_id]));

			}//only run if blade/laser are present
		}//inner for loop to iterate over each blade with the selected laser. 

	}//outer for loop to iterate over all lasers. 

	//spLogDebug(3,"######################CACHE PRINTED############################\n");
	//spLogDebug(3,"Returning from cache_print()\n");
	return;
}

/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * @assumptions
 * Blade, laser and command numbers are within range.  
 **/

void cache_handle_client(uint8_t* from_client, uint8_t* to_client){

	uint8_t blade; 
	uint8_t laser;
	uint8_t command; 

	blade = from_client[0];
	laser = from_client[1];
	command = from_client[3];

	to_client[0]  = from_client[0];
	to_client[1]  = from_client[1];
	to_client[2]  = from_client[2];

	
	//OPT command is always served locally. We assume that cache init is run and has 
	//already set blades and lasers present vectors. 
	switch (command)
	{
	case CHASSIS_OPT:
		uint32_to_uint8a(&blades_present, &to_client[4], 0);
		to_client[3] = B2S_NO_ERROR;
		return;
	case BLADE_OPT:
		uint32_to_uint8a(&lasers_present[blade],&to_client[4],0);
		to_client[3] = B2S_NO_ERROR;
		return;		
	case GET_UNIT_OPC:
		handle_bypass_mode(from_client, to_client); //We don't check for blade and laser range
		return;
	case GET_BLADE_OPC:
		handle_bypass_mode(from_client, to_client); //We don't check for blade and laser range
		return;
	case GET_LASER_OPC:
		handle_bypass_mode(from_client, to_client); //We don't check for blade and laser range
		return;

	default:
		break;
	}
	//We prune the blades and laser that are not present in the system.

//Check if the target blade is present 

	if(blade>=MAX_NUMBER_OF_BLADES){
		spLogDebug(3,"No blade in this slot:%u %s:%d\n",blade, __FILE__, __LINE__);
		to_client[3] = B2S_BLADE_NOT_PRESENT;
		return;

	}

	if(1!=blades[blade].present){
		spLogDebug(3,"No blade in this slot:%u %s:%d\n",blade, __FILE__, __LINE__);
		to_client[3] = B2S_BLADE_NOT_PRESENT;
		return;
	}

	if(0==is_rom_command(command)){
		//For this command laser number must be less than MAX_NUMBER_OF_LASERS
		if(laser>=MAX_NUMBER_OF_LASERS){
			spLogDebug(3,"Laser not found. blade:%u. Laser:%u %s:%d\n",blade,laser, __FILE__, __LINE__);
			to_client[3] = B2S_LASER_NOT_PRESENT;
			return;	  
		}

                //Check if the target laser is present. 	
		if(1!=blades[blade].lasers[laser].present){
			spLogDebug(3,"Laser not found. blade:%u. Laser:%u %s:%d\n",blade,laser, __FILE__, __LINE__);
			to_client[3] = B2S_LASER_NOT_PRESENT;
			return;	  
		}		
		
	}
	
	
	

	if(1==cache_bypass){
		spLogDebug(3,"Running in Cache bypass mode.\n");
		//This is implemented im cache_bypass_mode
		handle_bypass_mode(from_client,to_client);
		return;
	}
	//This is implemented in cache_normal_mode. 
	handle_cached_mode(from_client,to_client);
}
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/
void cache_start_monitoring(){

	int error;
 
	//Initialise the mutex first
	spLogDebug(3,"Initialising mutex for cache lock...\n");
	if (pthread_mutex_init(&cache_lock, NULL) != 0){
		spLogDebug(3,"Error initialising cache mutex.\n");
		return;
	}			
	spLogDebug(3,"Starting update thread....\n");
	state_update_thread();
	return; 

}
/***********************************************************************************/

