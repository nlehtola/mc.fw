/***********************************************************************************
* Project    : TLSX                                                               
************************************************************************************
* File       :                                                                    
* Author     : u.malik@coherent-solutions.com                                     
* Company    : Coherent Solutions                                                 
* Created    : 21/05/2013
************************************************************************************
* Description: 
* Contains main function for the TLSX server which spawns the rest. 
************************************************************************************
* Copyright 2013, Coherent Solutions www.coherent-solutions.com
* All rights reserved.
************************************************************************************/

#include "TLSXServer.h"

/***********************************************************************************/


int verbose      = 0;
int cache_bypass = 0;
int fls_mode     = 0;

pthread_t thread_id[2];

/***********************************************************************************/

/**
 * @param   [in] argc: number of command line parameters.
 *          [in] argv: A array of command line arguments.
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/

int main(int argc , char *argv[])
{
	int error;
	uint option;
 
	while ((option = getopt (argc, argv, "fbv:h")) != -1)
		switch (option)
		{
		case 'f':
			fls_mode = 1;
			spLogDebug(0,"Running in FLS mode.\n");
			break;
		case 'b':
			printf("Running in cache bypass mode.\n");
			cache_bypass = 1;	     
			break;
		case 'v':	    

			verbose = atoi(optarg);	    
			if (verbose<0)
				verbose = 0;
			if (verbose>3)
				verbose = 3;
			break;
		case 'h':
			printf("Usage:\n");
			printf("TLSXServer [-v|h|b]\n");
			printf("v: Increase verbosity.\n");
			printf("b: Puts the server in cache bypass mode. All B2S transactions happen directly.\n");
			return 0;
			
		case '?':
			fprintf (stderr,
				 "Unknown option character `\\x%x'.\n",
				 optopt);
			return 1;
		default:
			return 0;
		}
	printf("Verbosity set to: %d.\n", verbose);	
	spOpenLog("TLSX_TLSX_Server", verbose);

	tlsx_init();

	//Create blade_state_update thread 
	if(0==cache_bypass){
		error = pthread_create(&thread_id[0],NULL,(void *)&cache_start_monitoring,NULL);
		if (0!=error) spLogError("cache monitoring thread could not be created.\n");
	}
	error = pthread_create(&thread_id[1],NULL,(void *)&cm_comms_handler,NULL);
	if (0!=error) spLogError("blade_state_update thread could not be created.\n");
	pthread_join(thread_id[1],NULL);
	//We don't want main to exit hence we wait for the treads (which don't exit under normal conditions.)
	if (0==cache_bypass)
		pthread_join(thread_id[0],NULL);


	return 0;
}


/***********************************************************************************/

/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 1)Establishes a connection with the B2S server. It tries this a few times before giving up.
 * 3)Scans SPI bus and determines which blades are present. It also determines if the blades 
 *   are active in bootloader mode or normal mode. 
 * 4)Fetches state information from each blade and locally stores it. 
 **/

void tlsx_init(){

	int status = 1; 
	int retries = 0;
	int i;
	
	while(0!=status){
		status = b2ss_comms_init();	
		if(retries==SOCKET_RETRIES)
			break;
		//spLogDebug(3,"Retyring connection to the B2S Server: %u\n", retries);
		retries++;
	}
	
	if (0!=status){
		spLogDebug(3,"Could not establish a connection with the B2S Server. Exit..\n");
		exit(0);
	}

//Run an ECHO test to make sure that communication with the B2S Server is OK. 
	b2ss_echo();
	spLogNotice("Connection with B2S server established..\n");
	cache_init(); 	
	spLogNotice("Cache init finished..\n");
}



