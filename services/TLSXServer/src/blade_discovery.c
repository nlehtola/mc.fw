/***********************************************************************************
* Project    : TLSX
************************************************************************************
* File       : blade_discovery
* Author     : u.malik@coherent-solutions.com
* Company    : Coherent Solutions
* Created    : 
************************************************************************************
* Description: 
* Contains routines for finding which blades are present and how many lasers are there. 
************************************************************************************
* Copyright 2013, Coherent Solutions www.coherent-solutions.com
* All rights reserved.
************************************************************************************/

#include "TLSXServer.h"
#include "cache_manager.h"

extern int verbose;
extern blade_t blades[MAX_NUMBER_OF_BLADES];
extern fls_mode;

/***********************************************************************************/

/**
 * @param [in] blade: blade state variable
 * @returns      0 for success, 1 for a failure. 
 * @brief  
 * Initialises the blade data structure.
 **/
int init_blade(uint8_t blade_id, blade_t* blade){

  int i;
  uint8_t status;
  uint8_t rx_data[4];
  uint8_t tx_data[4];

   
  find_blade(blade_id, &blade->present);
   
  if(NOT_PRESENT==blade->present)
	  return; 


  for(i=1;i<MAX_NUMBER_OF_LASERS;i++){//We keep laser index the same as the number in the blade FW
	  find_laser(blade_id, i,&(blade->lasers[i].present));    
  }
  spLogDebug(3,"\n");
  spLogDebug(3,"------------------------------------------------------------------------------------\n");
  
  
  return NO_ERROR;

}/**init_blade**/

/***********************************************************************************/
	
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/


void find_laser(uint8_t blade_id, uint8_t laser_id, uint8_t* present){


	uint8_t tx[16] ={0};
	uint8_t rx[16]={0};

	spLogDebug(3,"Finding blade:%u, laser:%u..",blade_id, laser_id);  
	tx[BLADE_OFFSET] = blade_id;
	tx[LASER_OFFSET] = laser_id;

	frame_txrx(tx,rx,B2SS_LASER_PRESENT);
	if(B2SS_LASER_IS_PRESENT==rx[B2SS_COMMAND_INDEX]){
		*present = 1;
		if(1==fls_mode) //In IQS we have HW disable key. Hence safety is not required on start-up
			turn_safety_on(blade_id, laser_id);
	}
	else
		*present = 0;
	
	return;
}//find_laser

/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * Turns safety ON at start up. 
 * 
 **/

void turn_safety_on(uint8_t blade_id, uint8_t laser_id){


	uint8_t tx_data[4];
	uint8_t rx_data[4];
	uint8_t status = 0; 
	int i; 

	tx_data[0] = 1;

	for(i=0;i<5;i++){//Try a few times
		b2ss_command_query(blade_id, laser_id, 4,SET_SAFETY,tx_data,rx_data,&status );
		if(STATUS_QUERY_OK==status)
			break;
	}
	//log the action
	if(STATUS_QUERY_OK!=status)
		spLogDebug(0,"Safety ON failed for blade:%u, laser:%u\n", blade_id, laser_id);
	else
		spLogDebug(0,"Safety ON for blade:%u, laser:%u\n", blade_id, laser_id);
	
	return;

}


	
/************************************************************************/
/**
 * @param [in] blade_id: 
 * @param [out] present: 
 * @returns      0 for success, 1 for a failure. 
 * @brief  
 * Sends ECHO command and checks if the response is correct. 
 **/

void find_blade(uint8_t slot_id, uint8_t* present){
  

	uint8_t tx[16] ={0};
	uint8_t rx[16]={0};

	spLogDebug(3,"Finding blade:%u..",slot_id);  
	
	tx[BLADE_OFFSET] = slot_id;
	frame_txrx(tx,rx,B2SS_BLADE_PRESENT);
	if(B2SS_BLADE_IS_PRESENT==rx[B2SS_COMMAND_INDEX])
		*present = 1;
	else
		*present = 0;
	
	return;
	
	
}/**find_blade**/
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/


int get_blade_fw_rev(uint8_t slot_id, uint32_t* major, uint32_t* minor){
  
  uint8_t tx_data[4];
  uint8_t rx_data[4];
  uint8_t status; 
  uint8_t laser = 0;
  uint8_t sequence = 1;
  int i;
  
  //We assume that the blade is present. 

  b2ss_command_query(slot_id, laser, sequence,GET_FW_MAJOR_VERSION, tx_data, rx_data,&status );

  if(STATUS_QUERY_OK==status)
	  uint8a_to_uint32(rx_data, major, 0);	  
  else
	  *major = 0;

  b2ss_command_query(slot_id, laser, sequence,GET_FW_MINOR_VERSION, tx_data, rx_data,&status );

  if(STATUS_QUERY_OK==status)
	  uint8a_to_uint32(rx_data, minor, 0);	  
  else
	  *minor = 0;


return NO_ERROR;

}/**get_blade_fw_rev**/


