/***********************************************************************************
* Project    : TLSX                                                               
************************************************************************************
* File       :                                                                    
* Author     : u.malik@coherent-solutions.com                                     
* Company    : Coherent Solutions                                                 
* Created    : 21/05/2013
************************************************************************************
* Description: 
* TLS-X client. This is developed to the test the server. Comments inline.  
************************************************************************************
* Copyright 2013, Coherent Solutions www.coherent-solutions.com
* All rights reserved.
************************************************************************************/

#include "client_manager.h"
/***********************************************************************************/

extern int verbose;
extern int b2s_comms_status;
static int server_socket , client_socket , c ;

/***********************************************************************************/

/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * This functions listens on a socket on which SCPI server connects and send B2S commands. 
 * These commands are either served locally or sent to B2SServer for processing. 
 * This function runs in a separate thread and synchronises with tlsx_blade_state_update for
 * access to B2S socket. 
 **/

uint8_t cm_comms_handler(){

	
	int on,error;
	struct sockaddr_in server_address, client_address;
	int message_size=0;
	uint8_t from_scpi[PAYLOAD_SIZE] = {0};
	uint8_t to_scpi[PAYLOAD_SIZE] = {0};

	spLogDebug(1,"Registering INT, TERM and SEGV signals...");
	signal(SIGINT, signal_handler);
	signal(SIGTERM, signal_handler);
	signal(SIGSEGV, signal_handler);
	signal(SIGPIPE, signal_handler);

	spLogDebug(1,"port number: %d\n", TLSX_SERVER_PORT);
	//Open Socket
	//We use SOCK_STREAM for reliable two-way communication. 
	server_socket = socket(AF_INET , SOCK_STREAM , 0);
	if (server_socket == -1)
	{
		spLogDebug(1,"Error creating socket.");
		return 1;
	}
	on=1;
	error = setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));

	
	spLogDebug(1,"Socket opened.\n");
	
	//Set parameters for server address
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(TLSX_SERVER_PORT);
	server_address.sin_addr.s_addr = inet_addr("127.0.0.1");

	//Bind the server to the specified address
	if( bind(server_socket,(struct sockaddr *)&server_address , sizeof(server_address)) < 0)
	{
		//print the error message
		spLogDebug(1,"Error binding socket to the address.");
		return 1;
	}

	
	//Start listening on the socket. We set the Incoming queue length to 16. 

	listen(server_socket ,16);
	
	c = sizeof(struct sockaddr_in);

	//Accept and incoming connection
	spLogDebug(1,"Server initialised. Waiting for connections...\n");
	c = sizeof(struct sockaddr_in);
	while( (client_socket = accept(server_socket, (struct sockaddr *)&client_address, (socklen_t*)&c)) ){
		spLogDebug(1,"Connection accepted..\n");
		while(1){
			message_size = recv(client_socket , from_scpi , PAYLOAD_SIZE , 0);
			if(PAYLOAD_SIZE !=message_size ){
				fflush(stdout);
				spLogError("Error reading from SCPI..\n");
				break;//We need to accept a new connection at this point. 
			}//something happened with the read. Reconnect.
			spLogDebug(3,"Message received.\n");
			handle_client_transaction(from_scpi, to_scpi); 
			write(client_socket , to_scpi , PAYLOAD_SIZE);
			
		}//for ever while loop
				
		spLogDebug(1,"Closing Client socket.\n");
	//Server socket is not required any more. 
		close(client_socket);

	}//outer while loop
	
	if (client_socket < 0){
		spLogDebug(1,"Connection was not accepted.\n");
		return ERROR;
	}

	return ERROR;
//Note: this procedure should not return

}

/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * Main function to handle commands from SCPI server. A command can be locally served
 * or sent to B2SServer (which communicates with the blades). It is assumed that frame 
 * length is 16 bytes (as per spec).
 **/

void handle_client_transaction(uint8_t* from_client, uint8_t* to_client){

   int i;

   spLogDebug(3,"Data received from Client...\n");
   for (i=0;i<16;i++)
	   spLogDebug(3,"%u ", from_client[i]);

   //Do some sanity checking before passing on the request to the cache manager
   //Client error codes are updated in this function
   if(0!=check_data_ranges(from_client, to_client))
	   return;
   
   //Now call cache manager to handle the request. Note: Even if we are in bypass mode 
   //we still call the cache manager. This is because some commands, such as OPT/OPC, are
   //handled inside it. 
   cache_handle_client(from_client,to_client);

  return;

}
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/

uint8_t check_data_ranges(uint8_t* from_user, uint8_t* to_user){


  uint8_t blade   = from_user[0];
  uint8_t laser   = from_user[1];
  uint8_t command = from_user[3];
  

  if(blade >=MAX_NUMBER_OF_BLADES){
	  spLogDebug(3,"BLADE out of range..\n");
	  to_user[3] = B2S_INVALID_BLADE_ID;
	  return ERROR; 
  }

  //in case of a ROM command the address can be any number
  if (1==is_rom_command(command))
    return NO_ERROR;

  if(laser >= MAX_NUMBER_OF_LASERS){
	  spLogDebug(3,"LASER out of range..\n");
	  to_user[3] = B2S_INVALID_LASER_ID;
	  return ERROR; 
  }
 
  return NO_ERROR;

}
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/

void signal_handler(int signal_no){

	spLogDebug(0,"Received signal %d\n", signal_no);	
	if(SIGPIPE==signal_no){
		b2s_comms_status=B2SSERVER_COMMS_UNCONNECTED;
		spLogDebug(0,"Connection to B2S server lost. Retrying for 250s.");	
		tlsx_init();
		return;
	}

	spLogDebug(0,"Closing socket %d\n", signal_no);	
	close(client_socket);
	exit(0);

}

/***********************************************************************************/


