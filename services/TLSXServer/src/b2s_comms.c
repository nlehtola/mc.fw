/***********************************************************************************
* Project    : TLSX
************************************************************************************
* File       : b2s_comms.c
* Author     : u.malik@coherent-solutions.com
* Company    : Coherent Solutions
* Created    : 
************************************************************************************
* Description: 
* Contains functions to communicate with B2S Server.
************************************************************************************
* Copyright 2013, Coherent Solutions www.coherent-solutions.com
* All rights reserved.
************************************************************************************/

#include "TLSXServer.h"

extern int verbose;

int b2s_server_socket; //this is used for communication with B2SServer
int b2s_comms_status; 
pthread_mutex_t b2s_server_lock;


/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 *  Opens a TCP/IP connection with B2S server. 
 * 
 **/

int b2ss_comms_init(){
	
	int repeat = 0;
	int i;
	srand(time(NULL));	//For rand function used in some functions
	
	for(i=0;i<SOCKET_RETRIES;i++){
		
		if(0!=open_b2s_server_comms(B2S_SERVER_PORT)) {
			spLogDebug(0,"Failed to connect to B2SServer: %u..\n", i);			
			sleep(SLEEP_TIME);
			continue;
		}	
		else{
			spLogDebug(0,"Connected to B2SServer: %u..\n", i);			
			break;

		}//else

	}//for loop

	if(B2SSERVER_COMMS_CONNECTED!=b2s_comms_status){
		spLogDebug(0,"Unable to connect to B2S server for %u seconds. Exit.",SOCKET_RETRIES*SLEEP_TIME );
		exit(1);
	}
	//Initialise mutex for multiple thread access
	if (pthread_mutex_init(&b2s_server_lock, NULL) != 0){
		spLogError("Mutex init failed.\n");
		close_b2s_server_comms(); //close the connection
		return ERROR;
	}
	



	return NO_ERROR; 
}

/***********************************************************************************/
/**
 * @param   [in] port: port number on which to open a TCP/IP connection 
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * Opens a connection to the B2S server. 
 * 
 **/

int open_b2s_server_comms(int i_port){

	struct sockaddr_in server_address;
	int status; 

	//Create socket
	b2s_server_socket = socket(AF_INET , SOCK_STREAM , 0);
	if (-1==b2s_server_socket)
	{
		spLogError("Could not create socket");
		b2s_comms_status = B2SSERVER_SOCKET_ERROR;
		return B2SSERVER_SOCKET_ERROR;
	}

	spLogError("Client socket created");
	
	server_address.sin_addr.s_addr = inet_addr("127.0.0.1");
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(i_port);

	//Establish connection with the server
	status =connect(b2s_server_socket , (struct sockaddr *)&server_address , sizeof(server_address));

	if ( status < 0)
	{
		spLogDebug(0,"connect failed. Error: %d\n", status);
		b2s_comms_status=B2SSERVER_CONNECT_ERROR;
		return B2SSERVER_CONNECT_ERROR;
	}
	
	spLogDebug(0,"Connection established.\n");
	//Update global status
	b2s_comms_status = B2SSERVER_COMMS_CONNECTED;
	return B2SSERVER_COMMS_CONNECTED;

}

/***********************************************************************************/

/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/
int close_b2s_server_comms(){
	
	if (b2s_comms_status == B2SSERVER_COMMS_CONNECTED){
		close(b2s_server_socket);
		b2s_comms_status =B2SSERVER_COMMS_UNCONNECTED;
	}
	pthread_mutex_destroy(&b2s_server_lock);
	return NO_ERROR;
}
/***********************************************************************************/
//Various functions for data transportation
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * A wrapper around b2s_frame_txrx where fields can be individually spcified.
 * 
 **/


int b2ss_command_query(uint8_t i_blade_id, uint8_t i_laser_id, uint8_t i_sequence,
		      uint8_t i_command,  uint8_t* i_data, uint8_t* o_data,
		      uint8_t* o_status){
	uint8_t tx[16];
	uint8_t rx[16];
	
	*o_status = 0;//Put in undefined error in case b2s call fails for some reason.
	packa(tx,4, i_blade_id,i_laser_id,i_sequence, i_command);
	memcpy(&tx[4],i_data,4);
	
	//Perform B2S transaction
	frame_txrx(tx,rx, B2SS_TXRX);
	if(B2SS_NO_ERROR!=rx[B2SS_COMMAND_INDEX]){
		*o_status = STATUS_UNKNOWN;
		spLogDebug(0,"Error from B2S Server.....\n");
		
	}
	
	//Get the required fields from the received B2S frame.
	memcpy(o_data,&rx[4],4);
	*o_status = rx[3];
	return NO_ERROR;
}
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * Only sends a command to B2S and gets the ACK back. 
 * 
 **/

int b2ss_command(uint8_t i_blade_id, uint8_t i_laser_id, uint8_t i_sequence,
		      uint8_t i_command,  uint8_t* i_data,  uint8_t* o_status){
	
	uint8_t tx[16];
	uint8_t rx[16];

	packa(tx, 4, i_blade_id,i_laser_id,i_sequence, i_command);
	memcpy(&tx[4],i_data,4);
	frame_txrx(tx,rx, B2SS_COMMAND);
	if(B2SS_NO_ERROR!=rx[B2SS_COMMAND_INDEX]){
		*o_status = STATUS_UNKNOWN;
		spLogDebug(0,"Error from B2S Server.....\n");
		
	}

	*o_status = rx[3]; //The status resides in byte[3] of the received frame. 
	return NO_ERROR;
}
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * Only perform B2S query.
 * 
 **/

int b2ss_query(uint8_t i_blade_id, uint8_t i_sequence,  uint8_t* o_data,  uint8_t* o_status){

	int j;
	uint8_t tx[16];
	uint8_t rx[16];

	packa(tx, 4, i_blade_id,0,i_sequence,0);
	
	frame_txrx(tx,rx, B2SS_QUERY);
	if(B2SS_NO_ERROR!=rx[B2SS_COMMAND_INDEX]){
		*o_status = STATUS_UNKNOWN;
		spLogDebug(0,"Error from B2S Server.....\n");
		
	}
	//Get the required fields from the frame received from B2S.
	memcpy(o_data,&rx[4],4);
	*o_status = rx[3]; //The status resides in byte[3] of the received frame. 
	return NO_ERROR;
}




/***********************************************************************************/

/**
 * @param    [in] i_b2s: An array of 16 bytes to be send to B2SServer.
 *           [out] o_b2s: An array of 16 bytes sent by B2SServer.
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * Works in the same fashion as b2s_transaction except the communication layer is over TCP/IP instead of
 * direct function calls. 
 * Mutex locks are used in this function so that only one thread can access B2S server at any time. 
 **/
int frame_txrx(uint8_t* i_b2s, uint8_t* o_b2s, int transaction_mode){

	
	//spLogError("Sending message.\n");
	if(B2SSERVER_COMMS_CONNECTED!=b2s_comms_status) {
		spLogDebug(0,"Not connected to B2SServer. \n");
		return b2s_comms_status;
	}
//We use b2s_server_mutex to protect this region

	pthread_mutex_lock(&b2s_server_lock);	
	i_b2s[B2SS_COMMAND_INDEX] = transaction_mode;
	
	if( send(b2s_server_socket , i_b2s , PAYLOAD_SIZE , 0) < 0){
		spLogDebug(0,"Message transmission failed. \n");
		return B2SSERVER_SEND_ERROR;
	}
	//spLogError("Message sent.\n");
	if( recv(b2s_server_socket , o_b2s , PAYLOAD_SIZE , 0) < 0){
		spLogDebug(0,"Reception failed.\n");
		return B2SSERVER_RECEIVE_ERROR;
	  }	
//We use b2s_server_mutex to protect the above region
	pthread_mutex_unlock(&b2s_server_lock);
	//spLogError("Message received from B2SServer.\n");       
	return NO_ERROR;
}



/***********************************************************************************/
//Test functions
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * Sends 16 random bytes to B2SServer which are echoed back. Checks to see
 * if the returned data matches with what was sent.
 **/


int b2ss_echo(){
	
	int j;
	uint8_t to_b2s[16] = {0};
	uint8_t from_b2s[16];

	//Generate 16 random bytes
	gen_random_bytes(16,to_b2s);
	//printa(to_b2s,16);
	//send them to the server and check the result. 
	frame_txrx(to_b2s,from_b2s, B2SS_ECHO);
	//We only check 15 bytes as the byte #16 is for status
        if(NO_ERROR == check_bytes(15,to_b2s,from_b2s))
		return NO_ERROR;
	spLogError("*****B2S ServerEcho failed.******\n");
	//printa(from_b2s,16);

}
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * Random bytes for the ECHO test.
 * 
 **/


void gen_random_bytes(int length, uint8_t* o_data){

	int i;
	for(i=0;i<length;i++){
		o_data[i] = rand();
	}
}
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * Check bytes sent = byts received for ECHO test.
 * 
 **/


int check_bytes(int length, uint8_t* a, uint8_t* b){
	int i;
	for(i=0;i<length;i++){
		if(a[i]!=b[i]){
			spLogError("a[%d]:%d b[%d]:%d\n", i, a[i],i,b[i]);
			return ERROR;
		}
	}
	return NO_ERROR;
}
/***********************************************************************************/

