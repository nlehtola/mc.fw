/***********************************************************************************
* Project    : TLSX
************************************************************************************
* File       : 
* Author     : u.malik@coherent-solutions.com
* Company    : Coherent Solutions
* Created    : 
************************************************************************************
* Description: 
* The functions in this file allow us to set/get data in the blade_t depending 
* on what command has been issued and by what entity. More comments can be found 
* inline. 
************************************************************************************
* Copyright 2013, Coherent Solutions www.coherent-solutions.com
* All rights reserved.
************************************************************************************/


#include "cache_manager.h"

extern int verbose;
extern blade_t blades[MAX_NUMBER_OF_BLADES];
extern pthread_mutex_t cache_lock;
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/

int set_parameter_valid(uint8_t blade_id, uint8_t laser_idx, uint8_t command, uint8_t data){

	uint8_t error;

	if(!(0==data||1==data)){
		spLogDebug(3,"Incorrect value for static_data_valid data: %u\n", data);
		return;
	}
	//spLogDebug(3,"Setting static_data_valid: Blade:%u, Laser id:%u\n", blade_id, laser_idx+1);
	switch(command){

	case GET_POWER_MIN:
	case GET_POWER_MAX:
       		blades[blade_id].lasers[laser_idx].power.static_data_valid = data;
		break;
	case GET_FREQUENCY_MIN:
	case GET_FREQUENCY_MAX:
		blades[blade_id].lasers[laser_idx].frequency.static_data_valid = data; 
	case GET_GRID_MIN: 
	case GET_GRID_MAX: 
      		blades[blade_id].lasers[laser_idx].grid.static_data_valid = data;
		break;
	case GET_FINE_FREQUENCY_MIN:
        case GET_FINE_FREQUENCY_MAX:
      		blades[blade_id].lasers[laser_idx].fine_frequency.static_data_valid=data;
		break;
	case GET_SBS_RATE_MIN:
        case GET_SBS_RATE_MAX: 
      		blades[blade_id].lasers[laser_idx].sbs_dither_rate.static_data_valid=data;
		break;
	case GET_SBS_FREQUENCY_MIN: 
        case GET_SBS_FREQUENCY_MAX:
      		blades[blade_id].lasers[laser_idx].sbs_dither_frequency.static_data_valid=data;
		break;
	//Dynamic parameters
		//For these we need to check whether the values received is within limits or not. 
	case GET_TEMPERATURE:
		error = check_dynamic_parameter_in_range(blade_id, laser_idx, command,
							 blades[blade_id].lasers[laser_idx].temperature.actual);
		if (NO_ERROR==error && 1==data)
			blades[blade_id].lasers[laser_idx].temperature.dynamic_data_valid = 1;
		else
			blades[blade_id].lasers[laser_idx].temperature.dynamic_data_valid = 0;
		break;
	case GET_POWER_ACTUAL:
		error = check_dynamic_parameter_in_range(blade_id, laser_idx, command,
							 blades[blade_id].lasers[laser_idx].power.actual);
		if (NO_ERROR==error && 1==data)
			blades[blade_id].lasers[laser_idx].power.dynamic_data_valid = 1;
		else
			blades[blade_id].lasers[laser_idx].power.dynamic_data_valid = 0;
		break;
	case GET_FREQUENCY_LOCKED:
//We cannot check this so always set/reset
		blades[blade_id].lasers[laser_idx].frequency.dynamic_data_valid = data;
		break;
	case GET_GRID:		
		error = check_dynamic_parameter_in_range(blade_id, laser_idx, command,
							 blades[blade_id].lasers[laser_idx].grid.actual);
		if (NO_ERROR==error && 1==data)
			blades[blade_id].lasers[laser_idx].grid.dynamic_data_valid = 1;
		else
			blades[blade_id].lasers[laser_idx].grid.dynamic_data_valid = 0;
		break;
	case GET_FINE_FREQUENCY:
		error = check_dynamic_parameter_in_range(blade_id, laser_idx, command,
							 blades[blade_id].lasers[laser_idx].fine_frequency.actual);
		if (NO_ERROR==error && 1==data)
			blades[blade_id].lasers[laser_idx].fine_frequency.dynamic_data_valid = 1;
		else
			blades[blade_id].lasers[laser_idx].fine_frequency.dynamic_data_valid = 0;
		break;
        case GET_SBS_RATE:
		error = check_dynamic_parameter_in_range(blade_id, laser_idx, command,
							blades[blade_id].lasers[laser_idx].sbs_dither_rate.actual);
		if (NO_ERROR==error && 1==data)
			blades[blade_id].lasers[laser_idx].sbs_dither_rate.dynamic_data_valid = 1;
		else
			blades[blade_id].lasers[laser_idx].sbs_dither_rate.dynamic_data_valid = 0;
		break;
        case GET_SBS_FREQUENCY:
		error = check_dynamic_parameter_in_range(blade_id, laser_idx, command,
					 blades[blade_id].lasers[laser_idx].sbs_dither_frequency.actual);
		if (NO_ERROR==error && 1==data)
			blades[blade_id].lasers[laser_idx].sbs_dither_frequency.dynamic_data_valid = 1;
		else
			blades[blade_id].lasers[laser_idx].sbs_dither_frequency.dynamic_data_valid = 0;
		break;
	//We don't set valid for these
	case SET_LASER_ON_OFF:
	case GET_LASER_ON_OFF:
	case SET_SAFETY:
        case GET_SAFETY:
        case GET_SBS_DITHER:
        case SET_SBS_DITHER:
        case GET_CONTROL_DITHER:
        case SET_CONTROL_DITHER:
	case GET_POWER_REQUESTED:
        case GET_FREQUENCY_REQUESTED:
		break;
		
		//These set commands originate from the host. We invalidated dynamic data before sending
		//a B2S command
	case SET_POWER:
		blades[blade_id].lasers[laser_idx].power.dynamic_data_valid = data;
		break;
	case SET_FREQUENCY:
		blades[blade_id].lasers[laser_idx].frequency.dynamic_data_valid = data;
		break;
	case SET_GRID:
		blades[blade_id].lasers[laser_idx].grid.dynamic_data_valid = data;
		break;
	case SET_FINE_FREQUENCY:
		blades[blade_id].lasers[laser_idx].fine_frequency.dynamic_data_valid = data;
		break;
	case SET_SBS_RATE:
		blades[blade_id].lasers[laser_idx].sbs_dither_rate.dynamic_data_valid = data;
		break;
	case SET_SBS_FREQUENCY:
		blades[blade_id].lasers[laser_idx].sbs_dither_frequency.dynamic_data_valid = data;
		break;


	default:
		spLogDebug(3,"Unknown paramter:%u, %s\n", command, __func__);
		return ERROR;
	}
	return NO_ERROR;
}
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/

int get_parameter_valid(uint8_t blade_id, uint8_t laser_idx, uint8_t command, uint8_t *data){


	spLogDebug(3,"Getting data_valid: Blade:%u, Laser id:%u, Command:%u\n", blade_id, laser_idx, command);
	switch(command){
		//static parameters
	case GET_POWER_MIN:
	case GET_POWER_MAX:
       		*data = blades[blade_id].lasers[laser_idx].power.static_data_valid;
		break;
	case GET_FREQUENCY_MIN:
	case GET_FREQUENCY_MAX:
		*data = blades[blade_id].lasers[laser_idx].frequency.static_data_valid;
	case GET_GRID_MIN: 
	case GET_GRID_MAX: 
      		*data = blades[blade_id].lasers[laser_idx].grid.static_data_valid;
		break;
	case GET_FINE_FREQUENCY_MIN:
        case GET_FINE_FREQUENCY_MAX:
      		*data = blades[blade_id].lasers[laser_idx].fine_frequency.static_data_valid;
		break;
	case GET_SBS_RATE_MIN:
        case GET_SBS_RATE_MAX: 
      		*data = blades[blade_id].lasers[laser_idx].sbs_dither_rate.static_data_valid;
		break;
	case GET_SBS_FREQUENCY_MIN: 
        case GET_SBS_FREQUENCY_MAX:
      		*data = blades[blade_id].lasers[laser_idx].sbs_dither_frequency.static_data_valid;
		break;
	case GET_TEMPERATURE:
		*data = blades[blade_id].lasers[laser_idx].temperature.dynamic_data_valid;
		break;
	//Dynamic parameters
	case GET_POWER_ACTUAL:
		*data = blades[blade_id].lasers[laser_idx].power.dynamic_data_valid;
		break;
	case GET_FREQUENCY_LOCKED:
		*data = blades[blade_id].lasers[laser_idx].frequency.dynamic_data_valid;
		break;
	case GET_GRID:
		*data = blades[blade_id].lasers[laser_idx].grid.dynamic_data_valid;
		break;
        case GET_FINE_FREQUENCY:
		*data = blades[blade_id].lasers[laser_idx].fine_frequency.dynamic_data_valid;
		break;
        case GET_SBS_RATE:
		*data = blades[blade_id].lasers[laser_idx].sbs_dither_rate.dynamic_data_valid;
		break;
        case GET_SBS_FREQUENCY:
		*data = blades[blade_id].lasers[laser_idx].sbs_dither_frequency.dynamic_data_valid;
		break;
	default:
		spLogDebug(3,"Unknown paramter:%u, %s\n", command, __func__);

	}

}
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * This function can be invoked by cache update thread which will have some 
 * data to write. In this case the function will receive GET commands. 
 * This function can also be invoked by client while setting the value of a 
 * parameter. In this case the function will receive SET commands and it will
 * update 'requested' field.  
 **/



int set_parameter(uint8_t blade_id, uint8_t laser_idx, uint8_t command, uint8_t* data){

	uint8_t zeros[4] = {0};

	pthread_mutex_lock(&cache_lock);	
	switch(command){
		//Static parameters
		//We populate these at start up
	case GET_POWER_MIN:
		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].power.min,4);
		break;
	case GET_POWER_MAX:
      		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].power.max,4);
		break;
	case GET_FREQUENCY_MIN:
       		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].frequency.min,4);
		break;
	case GET_FREQUENCY_MAX: 
       		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].frequency.max,4);
		break;
	case GET_GRID_MIN: 
      		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].grid.min,4);
		break;
	case GET_GRID_MAX: 
      		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].grid.max,4);
		break;
	case GET_FINE_FREQUENCY_MIN:
      		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].fine_frequency.min,4);
		break;
	case GET_FINE_FREQUENCY_MAX: 
      		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].fine_frequency.max,4);
		break;
	case GET_SBS_RATE_MIN: 
      		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].sbs_dither_rate.min,4);
		break;
	case GET_SBS_RATE_MAX: 
      		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].sbs_dither_rate.max,4);
		break;
	case GET_SBS_FREQUENCY_MIN: 
      		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].sbs_dither_frequency.min,4);
		break;
	case GET_SBS_FREQUENCY_MAX: 
      		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].sbs_dither_frequency.min,4);
		break;
		//Dynamic parameters. These are updated by the cache at regular intervals. 
	case GET_TEMPERATURE:		
		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].temperature.actual,4);
		break;
	case GET_POWER_ACTUAL:
       		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].power.actual,4);
		break;
	case GET_LASER_ON_OFF:
       		uint8a_to_uint8a(data, &blades[blade_id].lasers[laser_idx].enable,4);
		break;
	case GET_SAFETY:
       		uint8a_to_uint8a(data, &blades[blade_id].lasers[laser_idx].safety,4);
		break;
	case GET_GRID:
       		uint8a_to_uint8a(data, &blades[blade_id].lasers[laser_idx].grid,4);
		break;	
	case GET_CONTROL_DITHER:
		uint8a_to_uint8a(data, &blades[blade_id].lasers[laser_idx].control_dither, 4);	
		break;
	case GET_FINE_FREQUENCY:
		uint8a_to_uint8a(data, &blades[blade_id].lasers[laser_idx].fine_frequency, 4);	
		break;
	case GET_SBS_DITHER:
		uint8a_to_uint8a(data, &blades[blade_id].lasers[laser_idx].sbs_dither_enable, 4);	
		break;
	case GET_SBS_FREQUENCY:
       		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].sbs_dither_frequency.actual,4);
		break;
	case GET_SBS_RATE:
       		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].sbs_dither_rate.actual,4);
		break;
//Instead of keeping a separate variable we set actural to requested to 
		//indicate that frequency lock has been achieved.
	case GET_FREQUENCY_LOCKED:
		if (1==data[0])
		uint8a_to_uint8a(blades[blade_id].lasers[laser_idx].frequency.requested, 
				 blades[blade_id].lasers[laser_idx].frequency.actual,4);
		else
		uint8a_to_uint8a(zeros,blades[blade_id].lasers[laser_idx].frequency.actual,4);

		break;

		//We need to update the requested value field every time a SET command is invoked by 
		//the client. 
	case SET_POWER:
		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].power.requested, 4);
		break;
		//This will be invoked at start up to initialise power and frequency
	case GET_POWER_REQUESTED:
		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].power.requested, 4);
		break;
	case GET_FREQUENCY_REQUESTED:
		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].frequency.requested, 4);
		break;

        case SET_FREQUENCY:
		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].frequency.requested, 4);
		uint8a_to_uint8a(zeros, blades[blade_id].lasers[laser_idx].frequency.actual, 4);
		break;
        case SET_GRID:
		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].grid.requested, 4);
		break;
        case SET_FINE_FREQUENCY:
		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].fine_frequency.requested, 4);
		break;
        case SET_SBS_RATE:
		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].sbs_dither_rate.requested, 4);
		break;
	case SET_SBS_FREQUENCY:
		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].sbs_dither_frequency.requested, 4);
		break;
		//The cache state update thread will set this when it detects a change.
        case SET_CONTROL_DITHER:
		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].control_dither, 4);	
		break;
	case SET_SBS_DITHER:
		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].sbs_dither_enable, 4);	
		break;
	case SET_LASER_ON_OFF:	
		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].enable, 4);	
		break;
	case SET_SAFETY:	
		uint8a_to_uint8a(data, blades[blade_id].lasers[laser_idx].safety, 4);	
		break;

	default:
		spLogDebug(3,"Unknown paramter:%u, %s\n", command, __func__);
		pthread_mutex_unlock(&cache_lock);
		return ERROR;
	}

	pthread_mutex_unlock(&cache_lock);
	return NO_ERROR;
}
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * These will origin from the client and will be served if the 
 * corresponding entry in the cache is valid. 
 * 
 **/

int get_parameter(uint8_t blade_id, uint8_t laser_idx, uint8_t command, uint8_t* data){


	pthread_mutex_lock(&cache_lock);	
	switch(command){
	//Static parameters. 
	case GET_POWER_MIN:
		uint8a_to_uint8a(blades[blade_id].lasers[laser_idx].power.min,data,4);
		break;
	case GET_POWER_MAX:
      		uint8a_to_uint8a(blades[blade_id].lasers[laser_idx].power.max,data,4);
		break;
	case GET_FREQUENCY_MIN:
       		uint8a_to_uint8a(blades[blade_id].lasers[laser_idx].frequency.min,data,4);
		break;
	case GET_FREQUENCY_MAX: 
       		uint8a_to_uint8a(blades[blade_id].lasers[laser_idx].frequency.max,data,4);
		break;
	case GET_GRID_MIN: 
      		uint8a_to_uint8a(blades[blade_id].lasers[laser_idx].grid.min,data,4);
		break;
	case GET_GRID_MAX: 
      		uint8a_to_uint8a(blades[blade_id].lasers[laser_idx].grid.max,data,4);
		break;
	case GET_FINE_FREQUENCY_MIN:
      		uint8a_to_uint8a(blades[blade_id].lasers[laser_idx].fine_frequency.min,data,4);
		break;
	case GET_FINE_FREQUENCY_MAX: 
      		uint8a_to_uint8a(blades[blade_id].lasers[laser_idx].fine_frequency.max,data,4);
		break;
	case GET_SBS_RATE_MIN: 
      		uint8a_to_uint8a(blades[blade_id].lasers[laser_idx].sbs_dither_rate.min,data,4);
		break;
	case GET_SBS_RATE_MAX: 
      		uint8a_to_uint8a(blades[blade_id].lasers[laser_idx].sbs_dither_rate.max,data,4);
		break;
	case GET_SBS_FREQUENCY_MIN: 
      		uint8a_to_uint8a(blades[blade_id].lasers[laser_idx].sbs_dither_frequency.min,data,4);
		break;
	case GET_SBS_FREQUENCY_MAX: 
      		uint8a_to_uint8a(blades[blade_id].lasers[laser_idx].sbs_dither_frequency.min,data,4);
		break;			   
	//Dynamic parameters. 
	case GET_TEMPERATURE:
		uint8a_to_uint8a(blades[blade_id].lasers[laser_idx].temperature.actual,data,4);
		break;
	case GET_FREQUENCY_LOCKED:
		if (blades[blade_id].lasers[laser_idx].frequency.requested ==
				 blades[blade_id].lasers[laser_idx].frequency.actual) 
			data[0] = 1;
		else
			data[0] = 0;
		break;
	case GET_FREQUENCY_REQUESTED:
		uint8a_to_uint8a(blades[blade_id].lasers[laser_idx].frequency.requested,data, 4);
		break;

	case GET_POWER_ACTUAL:
		uint8a_to_uint8a(blades[blade_id].lasers[laser_idx].power.actual,data, 4);
		break;
	case GET_POWER_REQUESTED:
		uint8a_to_uint8a(blades[blade_id].lasers[laser_idx].power.requested,data, 4);
		break;

        case GET_GRID:
		uint8a_to_uint8a(blades[blade_id].lasers[laser_idx].grid.actual, data,4);
		break;
        case GET_FINE_FREQUENCY:
		uint8a_to_uint8a(blades[blade_id].lasers[laser_idx].fine_frequency.actual, data,4);
		break;
        case GET_SBS_RATE:
		uint8a_to_uint8a(blades[blade_id].lasers[laser_idx].sbs_dither_rate.actual,data, 4);
		break;
	case GET_SBS_FREQUENCY:
		uint8a_to_uint8a(blades[blade_id].lasers[laser_idx].sbs_dither_frequency.actual, data,4);
		break;
		//This will be sent by the client. 
	case GET_CONTROL_DITHER:
		uint8a_to_uint8a(blades[blade_id].lasers[laser_idx].control_dither, data,4);	
		break;
	case GET_SBS_DITHER:
		uint8a_to_uint8a(blades[blade_id].lasers[laser_idx].sbs_dither_enable, data,4);
		break;
	case GET_LASER_ON_OFF:	
		uint8a_to_uint8a(blades[blade_id].lasers[laser_idx].enable, data, 4);	
		break;
	case GET_SAFETY:	
		uint8a_to_uint8a(blades[blade_id].lasers[laser_idx].safety, data, 4);	
		break;



	default:
		spLogDebug(3,"Unknown paramter:%u\n, %s", command, __func__);
		pthread_mutex_unlock(&cache_lock);
		return ERROR;
	}
	pthread_mutex_unlock(&cache_lock);
	return NO_ERROR;
}
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * The classification is done on the commands that are sent by the client. 
 **/

uint8_t classify_command(uint8_t command){

	spLogDebug(3,"Command submitted for classification: %u\n", command);

	switch(command){
		//all min/max/actual commands are locally served if the corresponding entry in the 
		//cache is valid. 
	case GET_POWER_MIN:
	case GET_POWER_MAX:
        case GET_POWER_ACTUAL:
	case GET_FREQUENCY_MIN:
	case GET_FREQUENCY_MAX:
	case GET_FREQUENCY_LOCKED:
        case GET_GRID_MIN:
        case GET_GRID_MAX:
	case GET_GRID:
        case GET_FINE_FREQUENCY_MIN:
        case GET_FINE_FREQUENCY_MAX:
        case GET_FINE_FREQUENCY:
	case GET_SBS_RATE_MIN:
        case GET_SBS_RATE_MAX:
	case GET_SBS_RATE:
        case GET_SBS_FREQUENCY_MIN:
        case GET_SBS_FREQUENCY_MAX:
	case GET_SBS_FREQUENCY:
        case GET_TEMPERATURE:
		return GET_PARAMETER_IF_VALID;
		//all requested parameters are locally served
        case GET_POWER_REQUESTED:
	case GET_FREQUENCY_REQUESTED:
        case GET_LASER_ON_OFF:
        case GET_CONTROL_DITHER:
        case GET_SBS_DITHER:
        case GET_SAFETY:
		return GET_PARAMETER;
        case SET_SAFETY:
	case SET_LASER_ON_OFF:
        case SET_CONTROL_DITHER:
        case SET_SBS_DITHER:
		return ENABLE_COMMAND;
        case SET_GRID:
        case SET_FINE_FREQUENCY:
        case SET_SBS_RATE:
        case SET_SBS_FREQUENCY:
		return SET_PARAMETER;
        case GET_BLADE_OPC:
        case GET_LASER_OPC:
		return OPC_COMMAND;
	case SET_FREQUENCY:
		return SET_FREQ_COMMAND;
	case SET_POWER:
		return SET_POWER_COMMAND;
	
	default:
		return CACHE_BYPASS;

	}

	return CACHE_BYPASS;
}

/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * @assumption
 * We assume that 'actual' data 
 **/

uint8_t check_dynamic_parameter_in_range(uint8_t blade_id, uint8_t laser_id, uint8_t command, uint8_t* value){

	uint32_t min = 0;
	uint32_t max = 0;
	uint32_t actual = 0;

	switch(command){
	case GET_TEMPERATURE:
		//Temperature's static data is always valid
		uint8a_to_uint32(blades[blade_id].lasers[laser_id].temperature.min,&min,0);
		uint8a_to_uint32(blades[blade_id].lasers[laser_id].temperature.max,&max,0);
		uint8a_to_uint32(value,&actual,0);
		break;
	case GET_POWER_ACTUAL:
		if(1==blades[blade_id].lasers[laser_id].power.static_data_valid){
			uint8a_to_uint32(blades[blade_id].lasers[laser_id].power.min,&min,0);
			uint8a_to_uint32(blades[blade_id].lasers[laser_id].power.max,&max,0);
			uint8a_to_uint32(value,&actual,0);
		}
		//If static data is not valid then we have no way of determining if dynamic data
		// is within range. We therefore send NO_ERROR
		break;
	case  GET_GRID:
		if(1==blades[blade_id].lasers[laser_id].grid.static_data_valid){
			uint8a_to_uint32(blades[blade_id].lasers[laser_id].grid.min,&min,0);
			uint8a_to_uint32(blades[blade_id].lasers[laser_id].grid.max,&max,0);
			uint8a_to_uint32(value,&actual,0);
		} 
		break;
	case  GET_FINE_FREQUENCY:
		if(1==blades[blade_id].lasers[laser_id].fine_frequency.static_data_valid){
			uint8a_to_int32(blades[blade_id].lasers[laser_id].fine_frequency.min,&min,0);
			uint8a_to_int32(blades[blade_id].lasers[laser_id].fine_frequency.max,&max,0);
			uint8a_to_int32(value,&actual,0);
		}
		break;
	case  GET_SBS_RATE:
		if(1==blades[blade_id].lasers[laser_id].sbs_dither_rate.static_data_valid){
			uint8a_to_uint32(blades[blade_id].lasers[laser_id].sbs_dither_rate.min,&min,0);
			uint8a_to_uint32(blades[blade_id].lasers[laser_id].sbs_dither_rate.max,&max,0);
			uint8a_to_uint32(value,&actual,0);
		}
		break;
	case  GET_SBS_FREQUENCY:
		if(1==blades[blade_id].lasers[laser_id].sbs_dither_frequency.static_data_valid){
			uint8a_to_uint32(blades[blade_id].lasers[laser_id].sbs_dither_frequency.min,&min,0);
			uint8a_to_uint32(blades[blade_id].lasers[laser_id].sbs_dither_frequency.max,&max,0);
			uint8a_to_uint32(value,&actual,0);
		}

		break;

	default:
		return ERROR; //don't know what to do with this so let's make it not valid. 
		
	}

	if(min<=actual && actual <=max){
		//spLogDebug(3,"In range: B:%u,L:%u,C:%u,value %d, min:%d, max:%d\n ", blade_id, laser_id, command,actual, min, max);	
		return NO_ERROR;

	}
	else{
		//spLogDebug(3,"Out of range: Blade:%u, Laser:%u, Command:%u,value %d.\n ", blade_id, laser_id, command,actual);	
	return ERROR;
	}
	
}



/***********************************************************************************/


