/***********************************************************************************
* Project    : TLSX
************************************************************************************
* File       : cache_manager_init
* Author     : u.malik@coherent-solutions.com
* Company    : Coherent Solutions
* Created    : 
************************************************************************************
* Description: 
* Performs initial scan of the bus and discovers blades/lasers that we present in 
* system. The output of this step is written to a file for SW upgrades.
* It also retrieves all static parameters from each blade/laser and stores them in the
* cache. 
************************************************************************************
* Copyright 2013, Coherent Solutions www.coherent-solutions.com
* All rights reserved.
************************************************************************************/


#include "cache_manager.h"

#include <sys/time.h>

extern int verbose;

/***********************************************************************************/

//Only the functions in this file can access the following variables. All other functions 
//must use proper access methods as defined later in this file. 
//A bit vector for blades that are present. 
extern uint32_t blades_present; 
//A bit vector per blade to identify which lasers are present. 
extern uint32_t lasers_present[MAX_NUMBER_OF_BLADES];
//Main data strcuture to hold the state of each blade. 
blade_t blades[MAX_NUMBER_OF_BLADES];



/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/


void get_static_parameters(){

	int i;
	uint8_t tmp[4];
	uint32_t min, max;
	uint8_t blade_id;
	uint8_t laser_id;

	uint8_t static_parameters[] = {
		GET_POWER_MIN,
		GET_POWER_MAX,
		GET_POWER_REQUESTED,
		GET_FREQUENCY_MIN, 
		GET_FREQUENCY_MAX, 
		GET_FREQUENCY_REQUESTED,
		GET_GRID_MIN, 
		GET_GRID_MAX, 
		GET_FINE_FREQUENCY_MIN, 
		GET_FINE_FREQUENCY_MAX 
	};
	uint8_t num_static_parameters = sizeof(static_parameters)/sizeof(uint8_t);
	spLogDebug(3,"Num static parameters: %u\n",  num_static_parameters);
	
	for (i=0;i<num_static_parameters;i++)
		retrieve_parameter_pipelined(static_parameters[i]);       
	min = TEMPERATURE_MIN;
	max = TEMPERATURE_MAX;

	//For temperature we put min and max by our selves. 
		for(laser_id=1;laser_id<MAX_NUMBER_OF_LASERS;laser_id++){
			for(blade_id=1;blade_id<MAX_NUMBER_OF_BLADES; blade_id++){
				if (1==blades[blade_id].present & 1==blades[blade_id].lasers[laser_id].present){
					//spLogDebug(3,"Setting min/max temperature. blade:%u laser:%u\n", blade_id, laser_id+1);
				uint32_to_uint8a(&min, tmp,0);
				uint8a_to_uint8a(tmp, blades[blade_id].lasers[laser_id].temperature.min,4);
				uint32_to_uint8a(&max, tmp,0);
				uint8a_to_uint8a(tmp, blades[blade_id].lasers[laser_id].temperature.max,4);
				blades[blade_id].lasers[laser_id].temperature.static_data_valid = 1;
			
			}//only run if blade/laser are present
		}//inner for loop to iterate over each blade with the selected laser. 

	}//outer for loop to iterate over all lasers. 

}
/***********************************************************************************/

/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * Sometimes we do get incorrect min and max values from the blade and therefore we invalidate such 
 * results. 
 **/


void check_static_parameters(){

	uint8_t blade_id;
	uint8_t laser_id;

		for(laser_id=1;laser_id<MAX_NUMBER_OF_LASERS;laser_id++){
			for(blade_id=1;blade_id<MAX_NUMBER_OF_BLADES; blade_id++){
				if (1==blades[blade_id].present & 1==blades[blade_id].lasers[laser_id].present){
					spLogDebug(3,"Checking static data:blade:%u laser:%u\n", blade_id, laser_id);
					spLogDebug(3,"Temperature\n");
					check_static_parameter(&blades[blade_id].lasers[laser_id].temperature);
					check_static_parameter(&blades[blade_id].lasers[laser_id].power);
					check_static_parameter(&blades[blade_id].lasers[laser_id].frequency);
					
					check_static_parameter(&blades[blade_id].lasers[laser_id].grid);
					spLogDebug(3,"Fine Frequency.\n");
					check_static_parameter(&blades[blade_id].lasers[laser_id].fine_frequency);
					check_static_parameter(&blades[blade_id].lasers[laser_id].sbs_dither_rate);
					check_static_parameter(&blades[blade_id].lasers[laser_id].sbs_dither_frequency);


				}//only run if blade/laser are present
		}//inner for loop to iterate over each blade with the selected laser. 

	}//outer for loop to iterate over all lasers. 

}
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/

void check_static_parameter(laser_parameter_t* p){

	int min;
	int max;
	
	uint8a_to_uint32(p->min, &min,0);
	uint8a_to_uint32(p->max, &max,0);
	if(max<min){
		//spLogDebug(3,"Static parameter values incorrect.\n");
		p->static_data_valid = 0;
		return;
	}	
	
	//spLogDebug(3,"Static parameter correct.\n");
}



/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * This call will fill in a bit vector for the blades that are identified. 
 * the size of o_id_vector is 4 bytes (i.e. a maximum of 32 blades can be identified)
 * 
 **/


void opt_unit_command(){
  
  int i;
  blades_present =0;
   //laser numbers start 
  for(i=1;i<MAX_NUMBER_OF_BLADES;i++){
	  if (blades[i].present==1){

		  blades_present |= (1<<i);
	  }
  }//for loop  
		  spLogDebug(3,"OPT unit: %x\n",blades_present);
}
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/

void opt_blade_command(uint8_t i_blade_id){

  int i;
  lasers_present[i_blade_id] = 0;
  //laser numbers start 
  for(i=1;i<MAX_NUMBER_OF_LASERS;i++){
    if (blades[i_blade_id].lasers[i].present==1)
      lasers_present[i_blade_id] |= 1<<(i);
  }//for loop  
  
}

/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * Update this particular paramter for all blades and lasers. The procedure is to split command and query 
 * phases. We first send commands to all blades and then follow up with queries. 
 * @assumptions
 * parameter command is within range. No sanity checking is done on it. 
 **/

void retrieve_parameter_pipelined(uint8_t command){

	int laser_id; 
	int blade_id; 
        //We store command status for each blade. A query is skipped if the result for it's command was 
        //not STATUS_FRAME_ACCEPTED
	uint8_t command_status[MAX_NUMBER_OF_BLADES]; 
	uint8_t tx[4] = {0}; //Don't care we are processing GET commands. 
	uint8_t rx[4]; //The result of a B2S command or query
	uint8_t status; 

	//Command phase. Send a command to each present blade for each present laser

	spLogDebugNoCR(3,"Retreiving parameter: %u\n", command);

	for(laser_id=1;laser_id<MAX_NUMBER_OF_LASERS;laser_id++){
		//The following loop is needed to make sure that we only run a query for the command
		//that has passed. 
		int i;
		for(i=1;i<MAX_NUMBER_OF_BLADES;i++)
			command_status[i] = 0; //assume command has failed.
				
		//spLogDebug(3,"************************CMD PHASE*********************************************\n");
		for(blade_id=1;blade_id<MAX_NUMBER_OF_BLADES; blade_id++){
			if (1==blades[blade_id].present & 1==blades[blade_id].lasers[laser_id].present){
				//spLogDebug(3,"Sending command for blade:%u laser:%u\n", blade_id, laser_id+1);
				b2ss_command(blade_id,laser_id, 3,command,tx,&status);
				if(STATUS_FRAME_ACCEPTED!=status){
					spLogDebug(3,"Command failed.: %u\n", status);

				}
				else
					command_status[blade_id] = 1;
			}//only run if blade/laser are present
		}//inner for loop to iterate over each blade with the selected laser. 

		usleep(100*1000);//sleep for 200ms
		//spLogDebug(3,"************************QUERY PHASE*********************************************\n");
		for(blade_id=1;blade_id<MAX_NUMBER_OF_BLADES; blade_id++){
			if (1==blades[blade_id].present & 1==blades[blade_id].lasers[laser_id].present){
				if(1!=command_status[blade_id]){
					//spLogDebug(3,"Command had failed. Not running query.\n");
					set_parameter_valid(blade_id,laser_id, command,0);
					continue;
				}
				//spLogDebug(3,"Sending query for blade:%u laser:%u\n", blade_id, laser_id+1);
				b2ss_query(blade_id,3,rx,&status);
				if(STATUS_QUERY_OK!=status){
					spLogDebug(3,"Query failed: %u\n", status);
				        set_parameter_valid(blade_id,laser_id, command,0);
					continue;
				}
				set_parameter(blade_id, laser_id, command, rx);
				set_parameter_valid(blade_id,laser_id, command,1);
				//print_parameter(command, rx);
			}//only run if blade/laser are present
		}//inner for loop to iterate over each blade with the selected laser. 

	}//outer for loop to iterate over all lasers. 

}
/***********************************************************************************/
