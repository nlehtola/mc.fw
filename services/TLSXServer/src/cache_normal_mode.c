/***********************************************************************************
* Project    : TLSX
************************************************************************************
* File       : 
* Author     : u.malik@coherent-solutions.com
* Company    : Coherent Solutions
* Created    : 
************************************************************************************
* Description: 
* 
************************************************************************************
* Copyright 2013, Coherent Solutions www.coherent-solutions.com
* All rights reserved.
************************************************************************************/
#include "cache_manager.h"

/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
  The procedure for handling a command is as follows:
  1) Classify the command.      
  GET_PARAMETER_IF_VALID:
      We check if the data corresponding to this command is valid in the cache.
      If yes then we retrieve the value and send it back. If data is not valid 
      then we issue a B2SS command and send back the result. 
  GET_PARAMETER:
      For these commands the data in the cache is always valid. Hence we retrieve
      and send it back to the client.
  ENABLE_COMMAND:
      We issue a B2SS command. Upon success we then check if the command has suceeded by 
      checking the actual state from the laser. We then update local enable/disable.
      If 
  SET_PARAMETER:
      We set local 'request' field and issue a B2SS command. 
 **/

extern blade_t blades[MAX_NUMBER_OF_BLADES];
extern int verbose; 

void handle_cached_mode(uint8_t* from_client, uint8_t* to_client){


	switch(classify_command(from_client[3])){
	case GET_PARAMETER_IF_VALID:
		handle_get_parameter_if_valid(from_client,to_client);
		break;
	case GET_PARAMETER:
		handle_get_parameter(from_client,to_client);
		break;
	case ENABLE_COMMAND:
		handle_enable(from_client,to_client);
		break;
        case SET_PARAMETER:
		handle_set_parameter(from_client,to_client);
		break;
        case OPC_COMMAND: //We will cache this later. 
		handle_bypass_mode(from_client, to_client);
		break;
        case SET_FREQ_COMMAND: //this one is special due to lock status
		handle_set_frequency(from_client, to_client);
		break;
        case SET_POWER_COMMAND: //this one is special as it can take a long time.
		handle_set_power(from_client, to_client);
		break;
	case CACHE_BYPASS:
		handle_bypass_mode(from_client, to_client);
		break;
	default:
		handle_bypass_mode(from_client, to_client);
		return;
	}
	return;

}
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * The parameters accessed in this function are all min/max and all actual values. 
 * These values can get stale and hence we need to check.
 * @assumption
 * Blade, laser, command values are within ranges. Blade and laser are both present. 
 * 
 **/

void handle_get_parameter_if_valid(uint8_t* from_client, uint8_t* to_client){

	
	int error;
	uint8_t blade;
	uint8_t laser;
	uint8_t command;
	uint8_t valid; 

	blade = from_client[0];
	laser = from_client[1]; 
	command = from_client[3];
	
	spLogDebug(3,"Exec: get_parameter_if_valid. b:%u,l%u,c:%u\n", blade, laser, command);
	to_client[0]  = blade;
	to_client[1]  = laser;
	to_client[2]  = from_client[2];
	
	error = get_parameter_valid(blade, laser, command, &valid);

	if (NO_ERROR == error && 1==valid){
		error = get_parameter(blade, laser, command, &to_client[4]);
		if (NO_ERROR == error){
			spLogDebug(3,"parameter was valid.\n");
//value is correct. Copy to the returning array
			to_client[3] = B2S_NO_ERROR;
			return; 
		}
	}
	//The data is either invalid or there has been some error. 
        //We therefore need to access the data from the blade itself. 
	spLogDebug(3,"parameter was invalid. Doing direct B2S..\n");
	handle_bypass_mode(from_client,to_client);
	return;

}
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * These values don't get stale and hence we simply retrieve them.
 * 
 **/

void handle_get_parameter(uint8_t* from_client, uint8_t* to_client){

	int error;
	uint8_t blade;
	uint8_t laser;
	uint8_t command;
	uint8_t valid; 

	blade = from_client[0];
	laser = from_client[1]; 
	command = from_client[3];
	
	to_client[0]  = blade;
	to_client[1]  = laser;
	to_client[2]  = from_client[2];
	
	spLogDebug(3,"Exec: get_parameter. b:%u,l index%u,c:%u\n", blade, laser, command);
	error = get_parameter(blade, laser, command, &to_client[4]);
	if (NO_ERROR == error){		
		to_client[3] = B2S_NO_ERROR;
		spLogDebug(3,"get parameter: %u %u %u %u", to_client[4], to_client[5], to_client[6], to_client[7]);
		return; 
	}

	//The data is either invalid or there has been some error. 
        //We therefore need to access the data from the blade itself. 

	handle_bypass_mode(from_client,to_client);
	return;

}
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * The procedure for this is as follows:
  1) Set the requested value of the parameter in question. 
  2) Invalidate the 'dynamic valid' field as it will soon be stale. 
  3) Send a B2SS command and query. Remap the status and return. 
 * 
 **/

void handle_set_parameter(uint8_t* from_client, uint8_t* to_client){

	uint8_t blade; 
	uint8_t laser;
	uint8_t sequence;
	uint8_t command;
	uint8_t to_b2s[4];
	uint8_t from_b2s[4];
	uint8_t status;
	uint8_t crc_status;
	int j;
	uint8_t get_set_command; 

	
	blade = from_client[0];
	laser = from_client[1];
	sequence = 0;
	command = from_client[3];
	
	to_client[0]  = blade;
	to_client[1]  = laser;
	to_client[2]  = from_client[2];
	spLogDebug(3,"Exec: set_parameter. b:%u,l%u,c:%u\n", blade, laser, command);

	//Note: Frequency command is handled separately below. 
	switch(command){

	case SET_POWER:
		get_set_command = GET_POWER_REQUESTED;
		break;
	case SET_GRID:
		get_set_command = GET_GRID;
		break;
	case SET_FINE_FREQUENCY:
		get_set_command = GET_FINE_FREQUENCY;
		break;
        case SET_SBS_RATE:
		get_set_command = GET_SBS_RATE;
		break;
        case SET_SBS_FREQUENCY:
		get_set_command = GET_SBS_FREQUENCY;
		break;
	default:
		spLogDebug(3,"Unknown set parameter: %u\n", command);
	}


//invalidate
	spLogDebug(3,"invalidating parameter.\n");
	set_parameter_valid(blade, laser,command,0);
	
	uint8a_to_uint8a(&from_client[4], to_b2s,4);
	b2ss_command_query(blade, laser, sequence, command, to_b2s, from_b2s, &status);	
	spLogDebug(3,"Updating set value from the blade itself.\n");
	b2ss_command_query(blade, laser, sequence, get_set_command, to_b2s, from_b2s, &status);
	set_parameter(blade, laser, command, from_b2s);	
	
	spLogDebug(3,"Set parameter: B2S status: %u\n", status);
	to_client[3]  = status;
	map_status_code(&to_client[3]);//The status codes between TLSX and Client are different

	uint8a_to_uint8a(from_b2s, &to_client[4],4);



}
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * Here we need to update and immediately update the cache state. 
 **/

void handle_enable(uint8_t* from_client, uint8_t* to_client){
	uint8_t blade; 
	uint8_t laser;
	uint8_t sequence;
	uint8_t command;
	uint8_t to_b2s[4];
	uint8_t from_b2s[4];
	uint8_t status;
	uint8_t get_command = GET_LASER_ON_OFF;
	int j;
	int repeat;

	command = from_client[3];
	switch(command){

	case SET_SAFETY:
	        get_command = GET_SAFETY;
		break;
	case SET_LASER_ON_OFF:
		get_command = GET_LASER_ON_OFF;
		break;
	case SET_CONTROL_DITHER:
		get_command = GET_CONTROL_DITHER;
		break;
        case SET_SBS_DITHER:
		get_command = GET_SBS_DITHER;
		break;
	default:
		spLogDebug(3,"Unknown SET Enable command: %u\n", command);
		break;
	}

	blade = from_client[0];
	laser = from_client[1];
	sequence = 0;
	command = from_client[3];
	
	to_client[0]  = blade;
	to_client[1]  = laser;
	to_client[2]  = from_client[2];
	spLogDebug(3,"Exec: set_enable. b:%u,l%u,c:%u\n", blade, laser, command);
	
	//First check if we are trying to turn laser on or off when safety is on. 
	//If yes then return an error now. 

	if(command==SET_LASER_ON_OFF && blades[blade].lasers[laser].safety[0]){
	    to_client[3] = B2S_EXEC_ERROR;
	    return;
	}

	uint8a_to_uint8a(&from_client[4], to_b2s,4);
	

	for(repeat =0;repeat<5;repeat++){
	//We split here so that we can spin in query until the command has been executed. 
		b2ss_command(blade, laser, sequence, command, to_b2s, &status);
		if(STATUS_FRAME_ACCEPTED==status)
			break;		
	}
	if(STATUS_FRAME_ACCEPTED!=status){ //Command was not accepted even after repeated trials. 
		to_client[3] =B2S_UNKNOWN_ERROR;
		spLogDebug(3,"B2S status for setting parameter: %u\n", status);
		return;
	}
	
	for(repeat =0;repeat<5;repeat++){		
		usleep(100*1000);
		b2ss_query(blade, sequence,from_b2s, &status);
		if(STATUS_QUERY_OK==status)
			break;	
		
	}
	
	if(STATUS_QUERY_OK!=status){ //Query was not completed. 
		to_client[3] =B2S_UNKNOWN_ERROR;
		spLogDebug(3,"B2S status for setting parameter: %u\n", status);
		return;
	}
	
	//The enable command has succeeded. Now we read back the status. 



	spLogDebug(3,"B2S status for setting parameter: %u\n", status);
	to_client[3]  = status;
	map_status_code(&to_client[3]);//The status codes between TLSX and Client are different
	//uint8a_to_uint8a(from_b2s, &to_client[4],4);

	spLogDebug(3,"Now updating cache state. get command: %u\n", get_command);
	//Now we need to update local cache

	b2ss_command_query(blade, laser, sequence, get_command, to_b2s, from_b2s, &status);

	spLogDebug(3,"B2S status:%u, data:%u:%u:%u:%u\n", status, from_b2s[0], from_b2s[1], from_b2s[2], from_b2s[3]);
	spLogDebug(3,"Setting parameter in the cache..\n");
	set_parameter(blade, laser, get_command, from_b2s);

	if (SET_SAFETY==command){
		//In case of safety we will also update laser on/off state immediately. 
		get_command = GET_LASER_ON_OFF;
		
		spLogDebug(3,"Updating cache state for laser on/off command: %u\n", get_command);
		//Now we need to update local cache
		
		b2ss_command_query(blade, laser, sequence, get_command, to_b2s, from_b2s, &status);
		
		spLogDebug(3,"B2S status:%u, data:%u:%u:%u:%u\n", status, from_b2s[0], from_b2s[1], from_b2s[2], from_b2s[3]);
		spLogDebug(3,"Setting parameter in the cache..\n");
		set_parameter(blade, laser, get_command, from_b2s);		
	}


	return;

}

/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/

void handle_set_frequency(uint8_t* from_client, uint8_t* to_client){

	uint8_t blade; 
	uint8_t laser;
	uint8_t sequence;
	uint8_t command;
	uint8_t to_b2s[4];
	uint8_t from_b2s[4];
	uint8_t status;
	int j;
	int repeat;



	blade = from_client[0];
	laser = from_client[1];
	sequence = 0;
	command = from_client[3];
	
	to_client[0]  = blade;
	to_client[1]  = laser;
	to_client[2]  = from_client[2];
	spLogDebug(3,"Exec: set_frequency_command. b:%u,l%u,c:%u\n", blade, laser, command);
	uint8a_to_uint8a(&from_client[4], to_b2s,4);
	

	for(repeat =0;repeat<5;repeat++){
	//We split here so that we can spin in query until the command has been executed. 
		b2ss_command(blade, laser, sequence, command, to_b2s, &status);		
		if(STATUS_FRAME_ACCEPTED==status)
			break;		

	}
	if(STATUS_FRAME_ACCEPTED!=status){ //Command was not accepted even after repeated trials. 
		to_client[3] =B2S_UNKNOWN_ERROR;
		spLogDebug(3,"command frame was not accepted.\n");
		return;
	}
	
	for(repeat =0;repeat<10;repeat++){		
		usleep(100*1000);
		b2ss_query(blade, sequence,from_b2s, &status);
		if(STATUS_QUERY_OK==status)
			break;	
		
	}
	
	if(STATUS_QUERY_OK!=status){ //Query was not completed. 
		to_client[3] =B2S_UNKNOWN_ERROR;
		spLogDebug(3,"Query frame was not OK. %u\n", status);
		return;
	}
	
	spLogDebug(3,"Set frequency command succeeded. Updating requested, actual and lock status\n.");
	spLogDebug(3,"Running get set frequency from the blade.\n.");
	sequence++;
	b2ss_command_query(blade, laser, sequence,GET_FREQUENCY_REQUESTED,to_b2s, from_b2s, &status);

//The enable command has succeeded. Now we update requested, actual and lock status
	set_parameter(blade, laser,SET_FREQUENCY,from_b2s); //This will update what the FW made of the set
	//frequency command. 
	return;


}



/***********************************************************************************/

/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/

void handle_set_power(uint8_t* from_client, uint8_t* to_client){

	uint8_t blade; 
	uint8_t laser;
	uint8_t sequence;
	uint8_t command;
	uint8_t to_b2s[4];
	uint8_t from_b2s[4];
	uint8_t status;
	int j;
	int repeat;



	blade = from_client[0];
	laser = from_client[1];
	sequence = 0;
	command = from_client[3];
	
	to_client[0]  = blade;
	to_client[1]  = laser;
	to_client[2]  = from_client[2];
	spLogDebug(3,"Exec: set_power_command. b:%u,l%u,c:%u\n", blade, laser, command);
	uint8a_to_uint8a(&from_client[4], to_b2s,4);
	

	for(repeat =0;repeat<5;repeat++){
	//We split here so that we can spin in query until the command has been executed. 
		b2ss_command(blade, laser, sequence, command, to_b2s, &status);		
		if(STATUS_FRAME_ACCEPTED==status)
			break;		

	}
	if(STATUS_FRAME_ACCEPTED!=status){ //Command was not accepted even after repeated trials. 
		to_client[3] =B2S_UNKNOWN_ERROR;
		spLogDebug(3,"command frame was not accepted.\n");
		return;
	}
	
	for(repeat =0;repeat<10;repeat++){		
		usleep(100*1000);
		b2ss_query(blade, sequence,from_b2s, &status);
		if(STATUS_QUERY_OK==status)
			break;	
		
	}
	
	if(STATUS_QUERY_OK!=status){ //Query was not completed. 
		to_client[3] =B2S_UNKNOWN_ERROR;
		spLogDebug(3,"Query frame was not OK. %u\n", status);
		return;
	}
	
	spLogDebug(3,"Set power command succeeded. Updating requested, actual and lock status\n.");
	spLogDebug(3,"Running get set frequency from the blade.\n.");
	sequence++;
	b2ss_command_query(blade, laser, sequence,GET_POWER_REQUESTED,to_b2s, from_b2s, &status);

//The enable command has succeeded. Now we update requested
	set_parameter(blade, laser,GET_POWER_REQUESTED,from_b2s); //This will update what the FW made of the set
	//power command. 
	return;


}



/***********************************************************************************/
