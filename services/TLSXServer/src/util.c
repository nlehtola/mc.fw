/***********************************************************************************
* Project    : TLSX
************************************************************************************
* File       : util
* Author     : u.malik@coherent-solutions.com
* Company    : Coherent Solutions
* Created    : 
************************************************************************************
* Description: 
* Contains routines for data processing and printing. 
************************************************************************************
* Copyright 2013, Coherent Solutions www.coherent-solutions.com
* All rights reserved.
************************************************************************************/

#include "cache_manager.h"
extern int verbose;
/***********************************************************************************/

/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/

void print_laser(laser_t* laser){
	
	char separator[12*7];
	memset(separator, '-',12*7);
	separator[12*7-1] = '\0';
	spLogDebugNoCR(3,"Status:%u",laser->status);
	spLogDebugNoCR(3," Present:%u",laser->present);
	spLogDebugNoCR(3," Laser Enable:%u %u %u %u",laser->enable[0], laser->enable[1],laser->enable[2],laser->enable[3]);
	spLogDebugNoCR(3," Control Dither:%u %u %u %u",laser->control_dither[0], laser->control_dither[1], laser->control_dither[2], laser->control_dither[3]);
	spLogDebugNoCR(3," SBS Dither:%u %u %u %u\n",laser->sbs_dither_enable[0], laser->sbs_dither_enable[1],laser->sbs_dither_enable[2],laser->sbs_dither_enable[3]);
	spLogDebugNoCR(3,"\n");
	spLogDebugNoCR(3,"%-12s%-12s%-12s%-12s%-12s%-12s%-12s\n", "Name", "Min", "Max", "Req", "Actual", "Static Vld", "Dynamic Vld");
	spLogDebugNoCR(3,"%s\n", separator);
	
	print_parameter(&(laser->power), "power",1);
	print_parameter(&(laser->frequency), "freq",1);
	print_parameter(&(laser->grid), "grid",1);
	print_parameter(&(laser->fine_frequency), "fine_freq",0);
	print_parameter(&(laser->temperature), "temp",1);

	//print_parameter(&(laser->sbs_dither_rate), "sbs rate",1);
	//print_parameter(&(laser->sbs_dither_frequency), "sbs freq",1);
	
	spLogDebugNoCR(3,"\n");

}

/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/
void print_parameter(laser_parameter_t* p, char* name, uint8_t u){

	spLogDebugNoCR(3,"%-12s", name);
	//spLogDebugNoCR(3,"min:");
	print_value(p->min,u);
//spLogDebugNoCR(3," max:");
	print_value(p->max,u);
	print_value(p->requested,u);
	print_value(p->actual,u);
	spLogDebugNoCR(3,"%-12u", p->static_data_valid);
	spLogDebugNoCR(3,"%-12u", p->dynamic_data_valid);
	spLogDebugNoCR(3,"\n");
}

/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/

void print_value(uint8_t* data, uint8_t u){

	uint32_t value;
	uint8a_to_uint32(data, &value,0);
	if (1==u)
		spLogDebugNoCR(3,"%-12u", value);
	else
		spLogDebugNoCR(3,"%-12d", value);

}
