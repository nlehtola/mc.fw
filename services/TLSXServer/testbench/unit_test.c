/***********************************************************************************
* Project    : TLSX
************************************************************************************
* File       :
* Author     : u.malik@coherent-solutions.com
* Company    : Coherent Solutions
* Created    : 21/05/2013
************************************************************************************
* Description:
* TLS-X client. This is developed to the test the server. Comments inline.
************************************************************************************
* Copyright 2013, Coherent Solutions www.coherent-solutions.com
* All rights reserved.
************************************************************************************/

#include "unit_test.h"
#define TLSXSERVER_PORT 10001
/***********************************************************************************/

void tlsx_init();

command commands[MAX_NUM_COMMANDS];
int commands_parsed = 0;


int client_sock;
struct sockaddr_in server_address;


/***********************************************************************************/

/**
 * @param   [in] argc: number of command line parameters.
 *          [in] argv: A array of command line arguments.
 * @returns      0 for success or an error code for a failure.
 * @brief
 *
 *
 **/

int main(int argc , char *argv[])
{
	uint8_t vflag = 0;
	uint8_t iflag = 0;
	uint8_t bflag = 0;
	uint option; 
	
	if(1==argc){
		printf("ut.exe [-v|h|i|b]\n");
		return 0;
	}


	while ((option = getopt (argc, argv, "ibhv")) != -1)
		switch (option)
		{
		case 'v':
			vflag = 1;	     
			break;
		case 'i':	    
			iflag = 1;	     
			break;
		case 'b':
			bflag = 1;
			break; 
		case 'h':
			printf("Usage:\n");
			printf("ut.exe [-v|h|i|b]\n");
			return 0;
			
		case '?':
			if (optopt == 'p')
				fprintf (stderr, "Option -%p requires an argument.\n", optopt);
			else
				fprintf (stderr,
					 "Unknown option character `\\x%x'.\n",
					 optopt);
			return 1;
		default:
			return 0;	
		}//switch
	
	tlsx_init();

	if(1==bflag){
		handle_batch_mode();
		return 0;			
	}

	if(1==iflag){
		handle_interactive_mode();
		return 0;			
	}
		
}
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * We need to open a single TCP connection with the TLSX server. 
 * 
 **/



void tlsx_init(){

	int status;
  //Create socket
        client_sock = socket(AF_INET , SOCK_STREAM , 0);
        if (client_sock == -1) {
                printf("Could not create socket");
        }
        //printf("Client socket created.\n");

        server_address.sin_addr.s_addr = inet_addr("127.0.0.1");
        server_address.sin_family = AF_INET;
        server_address.sin_port = htons(TLSXSERVER_PORT);


        status =connect(client_sock , (struct sockaddr *)&server_address , sizeof(server_address));
        //Establish connection with the server
        if ( status < 0) {
                printf("connect failed. Error: %d\n", status);
                return;
        }

        //printf("Connection established.\n");


}



/***********************************************************************************/

void handle_interactive_mode(){

	char input[32];
	int data=0;
	int i;
	for(i=0;i<32;i++)
		input[i] = '\0';

	while (1) {
		printf("Enter <blade laser sequence command data>:\n");
		if (fgets(input, sizeof input, stdin)) {
				parse_and_run(input);
		}//if		
	}//do forever
}
/***********************************************************************************/


void parse_and_run(char* input){

	uint8_t blade=0;
	uint8_t laser=0;
	uint8_t sequence=0;
	uint8_t command=0;
	int data=0;
	char* token;
	int i;
	uint8_t to_tlsx_server[16];
	uint8_t from_tlsx_server[16];
	int value=0;
	
	printf("Parsing:%s\n", input);
	
	token = strtok(input, " ");
	if(token!=NULL)
		to_tlsx_server[0] = atoi(token);
	token = strtok(NULL," ");
	if(token!=NULL)
		to_tlsx_server[1] = atoi(token);
	token = strtok(NULL," ");
	if(token!=NULL)
		to_tlsx_server[2] = atoi(token);
	token = strtok(NULL," ");
	if(token!=NULL)
		to_tlsx_server[3] = atoi(token);
	token = strtok(NULL," ");
	if(token!=NULL)
		data = atoi(token);
	int32_to_uint8a(&data,&to_tlsx_server[4]);
	
	printf("Data to TLSX Server..\n");
	for(i=0;i<16;i++)
		printf("%u ", to_tlsx_server[i]);
	printf("\n");
		
	tlsxserver_transaction(to_tlsx_server, from_tlsx_server);
	printf("Data from TLSX Server..\n");
	uint8a_to_int32(&from_tlsx_server[4],&value,0);
	printf("Status:%u: Data:%d\n", from_tlsx_server[3],value );

}





/***********************************************************************************/

void handle_batch_mode(){

	uint8_t to_server[16];
	uint8_t from_server[16];
	uint32_t value;
	int i,j;
	
	read_commands();
	
	for(i=0;i<commands_parsed;i++){
		printf("Command: %u\n", i);
		print_command(i);
		make_frame(i,to_server);
		for(j=0;j<16;j++)
			from_server[j]=0;
		tlsxserver_transaction(to_server,from_server);
		uint8a_to_uint32(&from_server[4], &value);
		if(128==from_server[3])
			printf("Status: OK, Value:%u\n", value);
		else
			printf("Status: %u, Value:%u\n", from_server[3],value);
		printf("-----------------------------------\n");
	}
	
        return;
	
}




void make_frame(int i_command_no, uint8_t* to_tlsxserver){

	int j;

	to_tlsxserver[0] = commands[i_command_no].blade_id;
	to_tlsxserver[1] = commands[i_command_no].laser;
	to_tlsxserver[2] = commands[i_command_no].sequence;
	to_tlsxserver[3] = commands[i_command_no].command;
	for(j=0; j<4; j++)
		to_tlsxserver[4+j] = commands[i_command_no].data[j];

}



int tlsxserver_transaction(uint8_t* to_tlsxserver, uint8_t* from_tlsxserver){

        int status;
      
        //printf("Sending message.\n");
        if( send(client_sock , to_tlsxserver , PAYLOAD_SIZE , 0) < 0) {
                printf("Message transmission failed. \n");
                return 1;
        }
        //printf("Message sent.\n");
        if( recv(client_sock , from_tlsxserver , PAYLOAD_SIZE , 0) < 0) {
                printf("Reception failed.\n");
                return 1;
        }
        //printf("Message received.\n");
        return 0;

}
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/
void print_command(int i)
{
	int j;
	printf("B:%d, L:%d, S:%d, C:%d ", commands[i].blade_id, commands[i].laser,
	       commands[i].sequence, commands[i].command);
	printf("D:");
	for(j=0; j<4; j++)
		printf("%d ", commands[i].data[j]);
	printf("\n");
        
}

/***********************************************************************************/

/**
 * @param
 * @returns      0 for success or an error code for a failure.
 * @brief
 * Iterate and prints the commands array
 *
 **/

void print_commands()
{

        int i,j;
        for(i=0; i<commands_parsed; i++) {
                printf("B:%d, L:%d, S:%d, C:%d ", commands[i].blade_id, commands[i].laser,
                       commands[i].sequence, commands[i].command);
                printf("D:");
                for(j=0; j<4; j++)
                        printf("%d ", commands[i].data[j]);
                printf("\n");
        }//for loop

}

/***********************************************************************************/

/**
 * @param
 * @returns      0 for success or an error code for a failure.
 * @brief
 * Reads a file containing B2S commands to be executed. Only reads MAX_NUM_COMMANDS
 *
 **/

void read_commands()
{

        FILE* file = fopen(COMMANDS_FILE,"r");
        if(file!=NULL) {
                char line [64];
                int command_no = 0;
                while(fgets(line, sizeof (line), file)!=NULL  && command_no<MAX_NUM_COMMANDS) {
			if('#'==line[0]) //skip comments
				continue;
			add_command(command_no,line);
                        command_no++;
                }//iterate until end of file
                commands_parsed = command_no;
        } else
                printf("Error reading command file.\n");
	printf("Num of commands parsed: %u\n", commands_parsed);

}
/***********************************************************************************/
/**
 * @param   [in] i_command_no
 *          [in] i_command_str: String read from commands file.
 * @returns      0 for success or an error code for a failure.
 * @brief
 * Parses the command line and fills in command data structure.
 *
 **/

void add_command(int i_command_no, char* i_command_str)
{

        char* token;
        int i;
//printf("Command:%d string: %s\n", i_command_no, i_command_str);
        token = strtok(i_command_str, " ");
        for(i=0; i<5; i++) {
                if(token!=NULL) {
                        add_value(i_command_no,i,atoi(token));
                        //	printf("Add command: %d %s\n",i, token);
                        token = strtok(NULL," ");
                }

        }//for loop

}//function add_command
/***********************************************************************************/
/**
 * @param   [in] i_command_no:  Index into the commands array.
 *          [in] i_value_index: Index into the command structure.
 *          [in] i_value:       Value to be stored.
 * @returns      0 for success or an error code for a failure.
 * @brief
 *
 *
 **/

void add_value(int i_command_no, int i_value_index, int i_value)
{

        //printf("command_no:%d value:index:%d value:%d\n",i_command_no, i_value_index, i_value);

        switch (i_value_index) {
        case 0:
                commands[i_command_no].blade_id = i_value;
                break;
        case 1:
                commands[i_command_no].laser = i_value;
                break;
        case 2:
                commands[i_command_no].sequence = i_value;
                break;
        case 3:
                commands[i_command_no].command = i_value;
                break;
        case 4:
                uint32_to_uint8a(&i_value, commands[i_command_no].data,0);
                break;
        default:
                printf("Unknown struct index. %d\n", i_value_index);
                break;

        }//switch


}

