/**********************************************************************************
* Project    : TLSX
***********************************************************************************
* File       : 
* Author     : u.malik@coherent-solutions.com
* Company    : Coherent Solutions
* Created    : 
***********************************************************************************
* Description: 
* 
***********************************************************************************
* Copyright 2013, Coherent Solutions www.coherent-solutions.com
* All rights reserved.
***********************************************************************************/

#include "common.h"
#include <string.h>
#include "time.h"
#include <stdint.h>
#include <unistd.h>
/***********************************************************************************/
//Global defs. 
#define PAYLOAD_SIZE 16
#define COMMANDS_FILE "commands.txt"
#define MAX_NUM_COMMANDS 20 

typedef struct {

	uint8_t blade_id;
	uint8_t laser;
	uint8_t sequence; 
	uint8_t command;
	uint8_t data[4];

} command; 

//Function prototypes. 

void read_commands();
void add_command(int i_command_no, char* i_command_str);
void add_value(int i_command_no, int i_value_index, int i_value); 
void print_commands();
void print_command(int);
int  tlsxserver_transaction(uint8_t* to_tlsx, uint8_t* from_tlsx);
void make_frame(int i_command_no, uint8_t* to_b2sserver);

void handle_batch_mode();
void handle_interactive_mode();
void parse_and_run(char* input);
