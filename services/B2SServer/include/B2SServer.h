/***********************************************************************************
* Project    : TLSX
************************************************************************************
* File       : 
* Author     : u.malik@coherent-solutions.com
* Company    : Coherent Solutions
* Created    : 
************************************************************************************
* Description: 
* 
************************************************************************************
* Copyright 2013, Coherent Solutions www.coherent-solutions.com
* All rights reserved.
************************************************************************************/

#ifndef B2SSERVER_H
#define B2SSERVER_H


#include "common.h"
#include <errno.h>
#include "b2s_wrapper.h"


#define B2S_SERVER_PORT 10017
#define PAYLOAD_SIZE 16

void b2ss_handle_command(uint8_t* from_tlsx_server, uint8_t* to_tlsx_server);
void b2ss_echo(uint8_t* from_tlsx_server, uint8_t* to_tlsx_server);
void b2ss_txrx(uint8_t* from_tlsx_server, uint8_t* to_tlsx_server);
void b2ss_command(uint8_t* from_tlsx_server, uint8_t* to_tlsx_server);
void b2ss_query(uint8_t* from_tlsx_server, uint8_t* to_tlsx_server);
void b2ss_init();

void signal_handler(int signal_no);
void blade_present();
void laser_present();
int start_server(int);
void print_blades();
int turn_crc_on(int blade_id);


#define B2SS_COMMAND_INDEX           15

//Commands accepted by B2SServer 
#define B2SS_ECHO                    128
#define B2SS_TXRX                    129
#define B2SS_COMMAND                 130
#define B2SS_QUERY                   131
#define B2SS_BLADE_PRESENT           132 
#define B2SS_LASER_PRESENT           133 


//Errors that can be send back to the TLSX Server
#define B2SS_NO_ERROR                140
#define B2SS_UNKNOWN_COMMAND         141
#define B2SS_ERROR                   142
#define B2SS_COMMAND_CRC_ERROR       143
#define B2SS_QUERY_CRC_ERROR         144
#define B2SS_BLADE_IS_PRESENT           145 
#define B2SS_LASER_IS_PRESENT           146 
#define B2SS_BLADE_IS_NOT_PRESENT           147 
#define B2SS_LASER_IS_NOT_PRESENT           148 




#define MAX_NUM_BLADES               16



#endif
