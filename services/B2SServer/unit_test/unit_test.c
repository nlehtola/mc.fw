/***********************************************************************************
* Project    : TLSX
************************************************************************************
* File       :
* Author     : u.malik@coherent-solutions.com
* Company    : Coherent Solutions
* Created    : 21/05/2013
************************************************************************************
* Description:
* TLS-X client. This is developed to the test the server. Comments inline.
************************************************************************************
* Copyright 2013, Coherent Solutions www.coherent-solutions.com
* All rights reserved.
************************************************************************************/

#include "unit_test.h"
#define B2SSERVER_SERVER_PORT 10017
/***********************************************************************************/

command commands[MAX_NUM_COMMANDS];
int commands_parsed = 0;

void b2sserver_test();

/***********************************************************************************/

/**
 * @param   [in] argc: number of command line parameters.
 *          [in] argv: A array of command line arguments.
 * @returns      0 for success or an error code for a failure.
 * @brief
 *
 *
 **/

int main(int argc , char *argv[])
{
	

	read_commands();
	print_commands();
	
	printf("Running Test.\n");
	b2sserver_test();
        return 0;
}


void b2sserver_test(){

	int i;
	int command_no=0;
	uint8_t to_b2sserver[16];
	uint8_t from_b2sserver[16];

	srand(time(NULL));
	
	
	for(command_no=0;command_no<MAX_NUM_COMMANDS;command_no++){
		make_frame(command_no,to_b2sserver);
		printf("B2SSERVER transaction..\n");
		b2sserver_transaction(to_b2sserver,from_b2sserver);

		if (5!=from_b2sserver[3]){
			printf("Transaction failed.\n");
			continue;
		}
		if(0==commands[command_no].command){
			for(i=4;i<8;i++){
				if (to_b2sserver[i] != from_b2sserver[i]){
					printf("%d %d \n",to_b2sserver[i], from_b2sserver[i]);
					break;
				}
			}
			printf("command_no:%d echo passed.\n", command_no);
		}//check in case of an ECHO command
		
		usleep((rand()%100)*5000);
	}
	

}

void make_frame(int i_command_no, uint8_t* to_b2sserver){

	int j;

	to_b2sserver[0] = commands[i_command_no].blade_id;
	to_b2sserver[1] = commands[i_command_no].laser;
	to_b2sserver[2] = commands[i_command_no].sequence;
	to_b2sserver[3] = commands[i_command_no].command;
	for(j=0; j<4; j++)
		to_b2sserver[4+j] = commands[i_command_no].data[j];
	//We need to instruct the server to perform a B2S transaction
	to_b2sserver[B2SS_COMMAND_INDEX] = B2SS_TXRX;


}



int b2sserver_transaction(uint8_t* to_b2sserver, uint8_t* from_b2sserver){


        int client_sock;
        struct sockaddr_in server_address;
        int status;

        //Create socket
        client_sock = socket(AF_INET , SOCK_STREAM , 0);
        if (client_sock == -1) {
                printf("Could not create socket");
        }
        printf("Client socket created.\n");

        server_address.sin_addr.s_addr = inet_addr("127.0.0.1");
        server_address.sin_family = AF_INET;
        server_address.sin_port = htons(B2SSERVER_SERVER_PORT);

	printf("Opening connection on port: %d\n", B2SSERVER_SERVER_PORT);

        status =connect(client_sock , (struct sockaddr *)&server_address , sizeof(server_address));
        //Establish connection with the server
        if ( status < 0) {
                printf("connect failed. Error: %d\n", status);
                return 1;
        }

        printf("Connection established.\n");

        printf("Sending message.\n");
        if( send(client_sock , to_b2sserver , PAYLOAD_SIZE , 0) < 0) {
                printf("Message transmission failed. \n");
                return 1;
        }
        printf("Message sent.\n");
        if( recv(client_sock , from_b2sserver , PAYLOAD_SIZE , 0) < 0) {
                printf("Reception failed.\n");
                return 1;
        }
        printf("Message received.\n");

        close(client_sock);
        return 0;

}

/**
 * @param
 * @returns      0 for success or an error code for a failure.
 * @brief
 * Iterate and prints the commands array
 *
 **/

void print_commands()
{

        int i,j;
        for(i=0; i<commands_parsed; i++) {
                printf("b:%d, l:%d, s:%d, c:%d ", commands[i].blade_id, commands[i].laser,
                       commands[i].sequence, commands[i].command);
                printf("d:");
                for(j=0; j<4; j++)
                        printf("%d ", commands[i].data[j]);
                printf("\n");
        }//for loop

}

/***********************************************************************************/

/**
 * @param
 * @returns      0 for success or an error code for a failure.
 * @brief
 * Reads a file containing B2S commands to be executed. Only reads MAX_NUM_COMMANDS
 *
 **/

void read_commands()
{

        FILE* file = fopen(COMMANDS_FILE,"r");
	


        if(file!=NULL) {
                char line [64];
                int command_no = 0;
                while(fgets(line, sizeof (line), file)!=NULL  && command_no<MAX_NUM_COMMANDS) {
                        add_command(command_no,line);
                        command_no++;
                }//iterate until end of file
                commands_parsed = command_no;
        } else{
                printf("Error reading command file.\n");
		exit(1);

	}
	

}
/***********************************************************************************/
/**
 * @param   [in] i_command_no
 *          [in] i_command_str: String read from commands file.
 * @returns      0 for success or an error code for a failure.
 * @brief
 * Parses the command line and fills in command data structure.
 *
 **/

void add_command(int i_command_no, char* i_command_str)
{

        char* token;
        int i;
        //printf("Command:%d string: %s\n", i_command_no, i_command_str);
        token = strtok(i_command_str, " ");
        for(i=0; i<5; i++) {
                if(token!=NULL) {
                        add_value(i_command_no,i,atoi(token));
                        //	printf("Add command: %d %s\n",i, token);
                        token = strtok(NULL," ");
                }

        }//for loop

}//function add_command
/***********************************************************************************/
/**
 * @param   [in] i_command_no:  Index into the commands array.
 *          [in] i_value_index: Index into the command structure.
 *          [in] i_value:       Value to be stored.
 * @returns      0 for success or an error code for a failure.
 * @brief
 *
 *
 **/

void add_value(int i_command_no, int i_value_index, int i_value)
{

        //printf("command_no:%d value:index:%d value:%d\n",i_command_no, i_value_index, i_value);

        switch (i_value_index) {
        case 0:
                commands[i_command_no].blade_id = i_value;
                break;
        case 1:
                commands[i_command_no].laser = i_value;
                break;
        case 2:
                commands[i_command_no].sequence = i_value;
                break;
        case 3:
                commands[i_command_no].command = i_value;
                break;
        case 4:
                uint32_to_uint8a(&i_value, commands[i_command_no].data,0);
                break;
        default:
                printf("Unknown struct index. %d\n", i_value_index);
                break;

        }//switch


}

