/**********************************************************************************
* Project    : TLSX
***********************************************************************************
* File       : 
* Author     : u.malik@coherent-solutions.com
* Company    : Coherent Solutions
* Created    : 
***********************************************************************************
* Description: 
* 
***********************************************************************************
* Copyright 2013, Coherent Solutions www.coherent-solutions.com
* All rights reserved.
***********************************************************************************/

#include "common.h"
#include <string.h>
#include "time.h"

/***********************************************************************************/
//Global defs. 
#define PAYLOAD_SIZE 16
#define COMMANDS_FILE "commands.txt"
#define MAX_NUM_COMMANDS 4
#define B2SS_COMMAND_INDEX          15

#define B2SS_ECHO                    128
#define B2SS_TXRX                    129
#define B2SS_COMMAND                 130
#define B2SS_QUERY                   131

//Errors that can be send back to the TLSX Server
#define B2SS_NO_ERROR                132
#define B2SS_UNKNOWN_COMMAND         133
#define B2SS_ERROR                   134

typedef struct {

	uint8_t blade_id;
	uint8_t laser;
	uint8_t sequence; 
	uint8_t command;
	uint8_t data[4];

} command; 

//Function prototypes. 


void read_commands();
void add_command(int i_command_no, char* i_command_str);
void add_value(int i_command_no, int i_value_index, int i_value); 
void print_commands();
int  b2sserver_transaction(uint8_t* to_tlsx, uint8_t* from_tlsx);
void b2sserver_echo_test();
void make_frame(int i_command_no, uint8_t* to_b2sserver);
