/***********************************************************************************
* Project    : TLSX
************************************************************************************
* File       : B2SServer.c
* Author     : u.malik@coherent-solutions.com
* Company    : Coherent Solutions
* Created    : 21 May 2013
************************************************************************************
* Description: 
* A simplified version of the previous B2SServer. This is now just a wrapper over
* B2S wrapper calls. The rest of the functionality is moved to TLSX server.
************************************************************************************
* Copyright 2013, Coherent Solutions www.coherent-solutions.com
* All rights reserved.
************************************************************************************/

#include "B2SServer.h"

#define PAYLOAD_SIZE 16


static int server_socket , client_socket , c ;

struct bade_info_t
  {
    uint8_t present;
    uint8_t crc_on; 
    uint32_t ver_major;
    uint32_t ver_minor;
    uint8_t laser_present[5];
  } blade_info[MAX_NUM_BLADES];

int fls_mode = 0;
int verbose = 1;

int main(int argc , char *argv[])
{
	int error; 
	uint option;
        
	while ((option = getopt (argc, argv, "fv:h")) != -1)
		switch (option)
		{
		case 'v':
			verbose = atoi(optarg);		    
			break;
		case 'f':
			fls_mode = 1;
			break;
			
		case 'h':
			printf("Usage:\n");
			printf("B2SServer [-v|h]\n");
			printf("v: Increase verbosity.\n");
			return 0;
			
		case '?':
			fprintf (stderr,
				 "Unknown option character `\\x%x'.\n",
				 optopt);
			return 1;
		default:
			return 0;
		}
	printf("Verbosity set to %d.\n", verbose);
	spOpenLog("TLSX_B2S_Server", verbose);
	spLogDebug(1,"Verbosity: %d\n", verbose);
	spLogDebug(1,"FLS mode: %d\n", fls_mode);
	spLogDebug(1,"Registering INT, TERM and SEGV signals..");

	signal(SIGINT, signal_handler);
	signal(SIGTERM, signal_handler);
	signal(SIGSEGV, signal_handler);

	b2ss_init();
	print_blades();

	printf("Initial scan done...\n");
	error = start_server(B2S_SERVER_PORT);

        return error;
}
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  This function scans B2S bus, determines which blades are present and whether
 *         to turn CRC on for these. 
 * 
 **/

void b2ss_init(){
	
	int i,j;
        int start_slot = 0;

	b2s_configure();

	//We don't need to scan non-existant slots for FLS
	if(1==fls_mode)
		start_slot = 15;

	for(i=start_slot;i<MAX_NUM_BLADES;i++){
		// check for blade in normal mode, ie responds to b2s commands
		find_blade(i, &blade_info[i].present);
		
		if(1==blade_info[i].present){
			get_blade_fw_rev(i,&(blade_info[i].ver_major), &(blade_info[i].ver_minor));     
			if(blade_info[i].ver_major<1)
				blade_info[i].crc_on = 0;
			else{
				if(SUCCESS==turn_crc_on(i)){
					blade_info[i].crc_on = 1;
					printf("Turned on CRC for blade:%u\n",i);
				}
			}
			for(j=1;j<5;j++)
				find_laser(i,j,blade_info[i].ver_major, &blade_info[i].laser_present[j]);
		}
	}//do for each slot
}
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/

int turn_crc_on(int blade_id){

	uint8_t rx[4];
	uint8_t tx[4];
	uint8_t status;
	
	tx[0] = 1;
	
	run_blade_command(blade_id, 0, 0, SET_CRC_ON_OFF, rx, tx, &status);
	if(STATUS_QUERY_OK==status)
		return  SUCCESS;
	//try one more time 
	run_blade_command(blade_id, 0, 0, SET_CRC_ON_OFF, rx, tx, &status);

	if(STATUS_QUERY_OK==status)
		return  SUCCESS;
	return ERROR;

}



/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/


void print_blades(){

	int i,j;

	for(i=0;i<MAX_NUM_BLADES;i++){
		if (1==blade_info[i].present) {
			// show blades found in normal b2s mode
			spLogDebugNoCR(1,"Slot %u: Blade present. Lasers:  ", i); 
			for(j=1;j<5;j++)			
				if(blade_info[i].laser_present[j])
					spLogDebugNoCR(1,"%u:Yes ", j);
				else
					spLogDebugNoCR(1,"%u:No ", j);
			spLogDebugNoCR(1,"(FW version: %u.%u CRC:%u)",blade_info[i].ver_major, blade_info[i].ver_minor,blade_info[i].crc_on );
			//printf("\n");
			continue;
		}
		spLogDebugNoCR(1,"Slot %u: Blade Not present.\n", i); 

	}//for all blades
}

/************************************************************************/

/**
 * @param   [in] i_port_number: 
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * The main server function which runs continuosly. It accepts connections from TLSX
 * server and passes on the commands to b2s_wrapper directly. At present this service 
 * performs no checks on the incoming and outgoing data and simpl acts as a 
 * communication service on top of b2s_wrapper. Later we will add sanity checks to make 
 * it more robust. 
 * TODO: Output messages to system log instead of printfs
 **/


int start_server(int i_port_number)
{

        int i, on;

        struct sockaddr_in server_address, client_address;
        int message_size=0;
        uint8_t from_tlsx_server[PAYLOAD_SIZE] = {0};
        uint8_t to_tlsx_server[PAYLOAD_SIZE] = {0};

	int error;

	
	spLogDebug(1,"port number: %d\n", i_port_number );
	spLogDebug(1,"Opening socket\n");
        //Open Socket
        //We use SOCK_STREAM for reliable two-way communication.
        server_socket = socket(AF_INET , SOCK_STREAM , 0);
        if (server_socket < 0 ) {
                spLogDebug(1,"Error creating socket.\n");
		spLogError("Error creating socket.\n");
                return 1;
        }
	on=1;
	error = setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));

	spLogDebug(1,"Socket created...\n");


	//Set parameters for server address
        server_address.sin_family = AF_INET;
        server_address.sin_port = htons(i_port_number);
        server_address.sin_addr.s_addr = inet_addr("127.0.0.1");

        //Bind the server to the specified address
	
	error = bind(server_socket,(struct sockaddr *)&server_address , sizeof(server_address));
        if( error < 0) {
                //print the error message
		spLogDebug(1,"Error binding socket to the address: %s\n", strerror(errno));

		return error;
        }

        //Start listening on the socket. We set the Incoming queue length to 3.
        listen(server_socket , 3);

        //Accept and incoming connection
	spLogDebugNoCR(3,"Server initialised. Waiting for connections...\n");
        spLogNotice("Server initialised. Waiting for connections...\n");

        c = sizeof(struct sockaddr_in);

        while( (client_socket = accept(server_socket, (struct sockaddr *)&client_address, (socklen_t*)&c)) ) {
                spLogDebugNoCR(3,"Connection accepted..");
		spLogNotice("Connection accepted..\n");
		//We use only one connection for all communication. 
		while(1) { 

                if( (message_size = recv(client_socket , from_tlsx_server , PAYLOAD_SIZE , 0)) > 0 ) {

                        spLogDebugNoCR(3,"Message received.\n");
			b2ss_handle_command(from_tlsx_server,to_tlsx_server);
                        write(client_socket , to_tlsx_server , PAYLOAD_SIZE);
                }
		else
			break;//there has been some error. Re-establish the connection
		}//internal while loop which exchanges data for ever.

                spLogNotice("Closing Client socket.\n");
                //Server socket is not required any more.
                close(client_socket);

        }//external while loop that accepts connections

        if (client_socket < 0) {
                spLogError("Connection was not accepted.\n");
                return 1;
        }

        return 0;

}

/***********************************************************************************/

/**
 * @param   [in] from_tlsx_server: The frame corresponds to TLSX<->B2SServer protcol spec
 *          [out] to_tlsx_server:   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * Calls local as well as functiosn in b2s_wrapper and b2s. 
 * 
 **/


void b2ss_handle_command(uint8_t* from_tlsx_server, uint8_t* to_tlsx_server)
{

	int i;
	for(i=0;i<PAYLOAD_SIZE;i++)
		spLogDebugNoCR(3,"%i ", from_tlsx_server[i]);
	spLogDebug(3,"");

	//Byte[15] contains the action code for B2S Server

	switch (from_tlsx_server[B2SS_COMMAND_INDEX]) {

	case B2SS_ECHO:
		b2ss_echo(from_tlsx_server, to_tlsx_server);
		break;
	case B2SS_TXRX:
		spLogDebugNoCR(3,"B2SS Transaction..\n");
		b2ss_txrx(from_tlsx_server, to_tlsx_server);
		break;
	case B2SS_COMMAND:
		spLogDebugNoCR(3,"B2SS Command..\n");
		b2ss_command(from_tlsx_server, to_tlsx_server);
		break;
	case B2SS_QUERY:
		spLogDebugNoCR(3,"B2SS query..\n");
		b2ss_query(from_tlsx_server, to_tlsx_server);
		break;
	case B2SS_BLADE_PRESENT:
		spLogDebugNoCR(3,"B2SS blade present..\n");
		blade_present(from_tlsx_server, to_tlsx_server);
		break;
	case B2SS_LASER_PRESENT:
		spLogDebugNoCR(3,"B2SS laser present..\n");
		laser_present(from_tlsx_server, to_tlsx_server);
		break;

	default:
		spLogDebugNoCR(3,"Unknown command \n");
		to_tlsx_server[B2SS_COMMAND_INDEX] = B2SS_UNKNOWN_COMMAND;

	}
	
	spLogDebugNoCR(3,"Data to TLSX Server..\n");
	for (i=0;i<PAYLOAD_SIZE;i++)
		spLogDebugNoCR(3,"%i ", to_tlsx_server[i]);

	spLogDebug(3,"");

}//b2ss_handle_command
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/

void blade_present(uint8_t* from_tlsx_server, uint8_t* to_tlsx_server){

	uint8_t blade = from_tlsx_server[BLADE_OFFSET];

	if(1==blade_info[blade].present)
		to_tlsx_server[B2SS_COMMAND_INDEX]=B2SS_BLADE_IS_PRESENT;
	else
		to_tlsx_server[B2SS_COMMAND_INDEX]=B2SS_BLADE_IS_NOT_PRESENT;

	return;

}
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/

void laser_present(uint8_t* from_tlsx_server, uint8_t* to_tlsx_server){

	uint8_t blade = from_tlsx_server[BLADE_OFFSET];
	uint8_t laser = from_tlsx_server[LASER_OFFSET];

	if(1==blade_info[blade].present && 1==blade_info[blade].laser_present[laser])
		to_tlsx_server[B2SS_COMMAND_INDEX]=B2SS_LASER_IS_PRESENT;
	else
		to_tlsx_server[B2SS_COMMAND_INDEX]=B2SS_LASER_IS_NOT_PRESENT;

	return;
	


}

/***********************************************************************************/

/**
 * @param   [in] from_tlsx_server: The frame corresponds to TLSX<->B2SServer protcol spec
 *          [out] to_tlsx_server:   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * Local ECHO for TLSX server
 * 
 * 
 **/


void b2ss_echo(uint8_t* from_tlsx_server, uint8_t* to_tlsx_server)
{

	int i;
	for(i=0;i<PAYLOAD_SIZE;i++)
		to_tlsx_server[i] = from_tlsx_server[i];

	to_tlsx_server[B2SS_COMMAND_INDEX] = B2SS_NO_ERROR;
	
}//b2ss_echo


/***********************************************************************************/

/**
 * @param   [in] from_tlsx_server: The frame corresponds to TLSX<->B2SServer protcol spec
 *          [out] to_tlsx_server:   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * This directly calls b2s_transaction function. 
 * 
 * 
 **/


void b2ss_txrx(uint8_t* from_tlsx_server, uint8_t* to_tlsx_server)
{

	b2ss_command(from_tlsx_server,to_tlsx_server);
	
	if (B2SS_NO_ERROR!=to_tlsx_server[B2SS_COMMAND_INDEX])
		return; //Command resulted in an error. No point running query

	b2ss_query(from_tlsx_server,to_tlsx_server);

	
}//b2ss_txrx
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * This prepares data and calls b2s_command function.
 * 
 **/

void b2ss_command(uint8_t* from_tlsx_server, uint8_t* to_tlsx_server){

	user_data_in ui;
	user_data_out uo; 
	int i;

	 ui.blade_id = from_tlsx_server[BLADE_OFFSET]; 
	 ui.laser_address = from_tlsx_server[LASER_OFFSET] ;
	 ui.sequence  = from_tlsx_server[SEQUENCE_OFFSET] ;
	 ui.command   = from_tlsx_server[COMMAND_OFFSET];
	 ui.data[0] = from_tlsx_server[DATA_OFFSET];
	 ui.data[1] = from_tlsx_server[DATA_OFFSET+1];
	 ui.data[2] = from_tlsx_server[DATA_OFFSET+2];
	 ui.data[3] = from_tlsx_server[DATA_OFFSET+3];
	 //Execute the command
	 b2s_command(&ui, &uo);

	  //We retry a few times in case MCU is busy or there is 
	 //some other error.
	 for(i=0;i<COMMAND_RETRIES;i++){		 

		 if (STATUS_FRAME_ACCEPTED==uo.status){			 
			 if(0==blade_info[from_tlsx_server[BLADE_OFFSET]].crc_on)
				 break;
			 else if( 1==uo.crc_handshake)
				 break;
			 else
				  spLogDebugNoCR(1,"Retry:%u uo.crc:%u\n",i, uo.crc_handshake);

		 }
		 usleep(37e3);
		 b2s_command(&ui, &uo);
	 }
	 spLogDebug(3,"Final uo.status:%u uo.crc:%u\n", uo.status, uo.crc_handshake);

	 to_tlsx_server[BLADE_OFFSET] = from_tlsx_server[BLADE_OFFSET];
	 to_tlsx_server[LASER_OFFSET] = from_tlsx_server[LASER_OFFSET] ;
	 to_tlsx_server[SEQUENCE_OFFSET] = from_tlsx_server[SEQUENCE_OFFSET];
	 to_tlsx_server[COMMAND_OFFSET] = uo.status;
	 //Commands do not return data
	 to_tlsx_server[DATA_OFFSET] = 0;
	 to_tlsx_server[DATA_OFFSET+1] =0;
	 to_tlsx_server[DATA_OFFSET+2] =0;
	 to_tlsx_server[DATA_OFFSET+3] =0;

	 //Update the B2SS server local status
	 to_tlsx_server[B2SS_COMMAND_INDEX] = B2SS_NO_ERROR;

	 return;
}

/***********************************************************************************/


/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * This prepares data and calls b2s query function. 
 * 
 **/

void b2ss_query(uint8_t* from_tlsx_server, uint8_t* to_tlsx_server){

	user_data_in ui;
	user_data_out uo; 
	int i;

	 ui.blade_id = from_tlsx_server[BLADE_OFFSET];
	 ui.sequence  = from_tlsx_server[SEQUENCE_OFFSET] ;

	 //Execute the command
	 b2s_query(&ui, &uo);

	 //Based on the response we decide what to do. 
	 for(i=0; i<QUERY_RETRIES;i++){
		
		 if (STATUS_QUERY_OK==uo.status){			 
			 if(0==blade_info[from_tlsx_server[BLADE_OFFSET]].crc_on)
				 break;
			 else if( 1==uo.crc_handshake)
				 break;
			 else
				 spLogDebugNoCR(1,"Retry:%u uo.crc:%u\n",i, uo.crc_handshake);

		 }

		 usleep(37e3);
		 b2s_query(&ui, &uo); 
	 }
	
	 spLogDebug(3,"Final uo.status:%u uo.crc:%u\n",uo.status, uo.crc_handshake);
	 //Now send what ever is the outcome of the above. 
	 
	 to_tlsx_server[BLADE_OFFSET] = from_tlsx_server[BLADE_OFFSET];
	 to_tlsx_server[LASER_OFFSET] = from_tlsx_server[LASER_OFFSET] ;
	 to_tlsx_server[SEQUENCE_OFFSET] = from_tlsx_server[SEQUENCE_OFFSET];
	 to_tlsx_server[COMMAND_OFFSET] = uo.status;
	 to_tlsx_server[DATA_OFFSET] =   uo.data[0];
	 to_tlsx_server[DATA_OFFSET+1] = uo.data[1];
	 to_tlsx_server[DATA_OFFSET+2] = uo.data[2];
	 to_tlsx_server[DATA_OFFSET+3] = uo.data[3];

}

/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/

void signal_handler(int signal_no){

	spLogDebugNoCR(1,"Received signal %d\n", signal_no);	
	spLogDebugNoCR(1,"Closing socket %d\n", signal_no);	
	close(client_socket);
	exit(0);

}


/***********************************************************************************/

