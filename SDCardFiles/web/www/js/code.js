function dhcp_enable(enable)
{
  disable = true;
  if(enable == 1)
  {
    disable = false;
  }

  document.getElementById('ip_address').disabled = disable;
  document.getElementById('subnet').disabled = disable;
}
