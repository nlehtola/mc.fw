#!/usr/bin/python2.6
import os

error500Page = """
<html>
	<head>
		<title>EXFO - %s</title>
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
	</head>
	<body>
		<a href="/"><img src="/img/%s/logo.png" border="0"/></a><br/>
		<hr />
		Error 500: Server Error
		<hr />
	</body>
</html>
"""

errorEthernetPage = """
<html>
	<head>
		<title>EXFO - %s</title>
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
	</head>
	<body>
		<a href="/"><img src="/img/%s/logo.png" border="0"/></a><br/>
		<hr />
		Please use the USB connection to change system configuration.
		<hr />
	</body>
</html>
"""

def getError500Page():
	model = getInstrumentModel()
	return error500Page%(model, model)

def getErrorEthernetPage():
	model = getInstrumentModel()
	return errorEthernetPage%(model, model)

def isClientUSB():
	""" Checks if the client is conencting via the usb. Return true for USB 
		false for other/ethernet.
	"""
	return ('192.168.97.201'==os.environ['SERVER_ADDR'])

def getInstrumentModel():
	configFile = "/cs/releases/TLSX_chassis_config.cfg"
	models = ["IQS-636", "FLS-2800","FVA-3800"]
	try:
		config = open(configFile,'r').read()
		for model in models:
			if config.find(model)!=-1:
				return model
	except:
		pass
	return models[0]

