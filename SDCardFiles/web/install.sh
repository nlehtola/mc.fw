#!/bin/bash

if [ $1 == ""] ; then
	echo "No IP address provided"
	exit
fi

tar -zcvf installation.tar ./web ./cgi-bin sites-available

sftp root@$1<<EOF
put installation.tar 
EOF

ssh root@$1<<\EOF
rm -r ~/webinstall
mkdir webinstall

tar -zxvf installation.tar -C ./webinstall/ --touch

rm -r /var/www/*
rm -r /usr/lib/cgi-bin/*
cp -f ~/webinstall/sites-available /etc/apache2/sites-available/default
cp -r ~/webinstall/web/* /var/www/
cp -r ~/webinstall/cgi-bin/* /usr/lib/cgi-bin/

apache2ctl restart

EOF
