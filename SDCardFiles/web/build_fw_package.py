from struct import *
import hashlib
import getopt
import sys

def main(argv):
	sbc_file = False
	sbc_version = False
	blade_file = False
	
	sbc_bin = False
	blade_bin = False
	
	try:
		opts, args = getopt.getopt(argv, "hs:b:v:", ["sfile=", "bfile=", "version="])
		
	except getopt.GetoptError:
		print 'build_fw_package.py -s <sbc firmware> -v <sbc fw version> -b <blade firmware>'
		print "opt err"
		sys.exit(2);
	
	for opt, arg in opts:
		if opt == "-h":
			print 'build_fw_package.py -s <sbc firmware> -v <sbc version> -b <blade firmware>'
			sys.exit()
		elif opt in ("-s", "--sfile"):
			sbc_file = arg
		elif opt in ("-v", "--version"):
			sbc_version = arg
		elif opt in ("-b", "--bfile"):
			blade_file = arg
	
	try:
		fout = open('update.zip', 'wb')
		
		print sbc_file
		m = hashlib.md5()
		print sbc_version
	
		if sbc_file and sbc_version:
			vers = sbc_version.split(".")
			major = int(vers[0])
			minor = int(vers[1])
			
			fbin = open(sbc_file, 'rb')
			sbc_bin = fbin.read()
			fbin.close()
			
			fout.write(pack('BB', major, minor))			
		else:
			fout.write(pack('BB', 0, 0, 0))
			
		if blade_file:			
			fbin = open(blade_file, 'rb')
			blade_bin = fbin.read()
			fbin.close()
			
			fout.write(pack('B', 1))			
		else:
			fout.write(pack('B',0,))
			
		fout.write(pack('BBBBBBBBBBBBB',0,0,0,0,0,0,0,0,0,0,0,0,0))
		
		if(sbc_file):
			m.update(sbc_bin)
		if(blade_file):
			m.update(blade_bin)
			
		calc_hash = m.digest()
		
		print calc_hash
		
		fout.write(calc_hash)
		
		if(sbc_file):
			fout.write(sbc_bin)
		if(blade_file):
			fout.write(blade_bin)
			
	except:
		print "Error"

if __name__ == "__main__":
   main(sys.argv[1:])