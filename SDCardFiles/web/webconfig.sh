#!/bin/bash

if [ $1 == ""] ; then
	echo "No IP address provided"
	exit
fi

ssh root@$1<<\EOF
apt-get update
apt-get install unzip

apt-get install sudo

chmod 666 /etc/network/interfaces
EOF
