/*  
 * File Name     : coding_standards.c
 * Author        : Usama Malik
 * Date Created  : 5/02/2013
 * Description: 
 * This file provides a coding standard to be followed for the TLSX Master 
 * Controller software. 
 *
 * Copyright 2014, Southern Photonics/Coherent Solutions. www.southernphotonics.com
 * All Rights Reserved.
 */

//General: 
//Each source file must contain the above header. 
//Each line in the source file must not span more than 80 characters.
//Tab spacing must be set to 4.

//Syntax:
//Variables must be either all lower case or camel case. Within a file they must not be 
//mixed and matched. 


//Errors:
//error.h has a list of error codes. These must be used for uniform error reporting. 
//errors must be logged via function calls in error.c

//Comments:
//Each defined function must have a prototype defined and a description of what it does 
//must proceed before its definition.

//Directory structure. 
//The must follow the directory structure defined in the tls-x.mc.fw repository. 

//Compiler flags:
//The code must be compiled using Makefiles and with the following compiler options turned ON. 
// -Wall
