/*  
 * File Name     : conversions.c
 * Author        : Usama Malik
 * Date Created  : 13/02/2013
 * Description: 
 * Contains code for changing between different formats
 *
 * Copyright 2013, Southern Photonics/Coherent Solutions. www.southernphotonics.com
 * All Rights Reserved.
 */

#include "conversions.h"

/*******************************************************************/
/**
 * @param [in] a: A 32-bit unsigned number
 * @param [in] swap: Whether to swap byte positions or not. 
 * @param [out] out: An array of 4 bytes. 
 * @returns      0 for success, 1 for a failure. 
 * @brief  
 * Converts a 32-bit unsigned into an array of 4 bytes. 
 **/
int int32_to_uint8a(int32_t* a, uint8_t* out,uint8_t swap){
  int i;

  for(i=0;i<4;i++){
    if(1==swap)
      out[3-i] = (*a&(0xFF<<i*8))>>(i*8);
    else
      out[i]   = (*a&(0xFF<<i*8))>>(i*8);
  }
  return 0;

}

/*******************************************************************/
/**
 * @param [in] a: A 32-bit unsigned number
 * @param [in] swap: Whether to swap byte positions or not. 
 * @param [out] out: An array of 4 bytes. 
 * @returns      0 for success, 1 for a failure. 
 * @brief  
 * Converts a 32-bit unsigned into an array of 4 bytes. 
 **/
int uint32_to_uint8a(uint32_t* a, uint8_t* out,uint8_t swap){
  int i;

  for(i=0;i<4;i++){
    if(1==swap)
      out[3-i] = (*a&(0xFF<<i*8))>>(i*8);
    else
      out[i]   = (*a&(0xFF<<i*8))>>(i*8);
  }
  return 0;

}

/************************************************************************/
/**
 * @param [in] a: An array of 4 bytes. 
 * @param [in] swap: Whether to swap bytes positions or not. 
 * @param [out] out: A 32-bit unsigned number. 
 * @returns      0 for success, 1 for a failure. 
 * @brief  
 * Converts an array of bytes into an unsigned 32-bit unsigned. 
 **/

int uint8a_to_int32(uint8_t* a, int* out,uint8_t swap){
  int i;
  *out = 0;
  for(i=0;i<4;i++){
    if(1==swap)
      *out |= (((int)a[3-i])<<i*8);
    else
      *out |= (((int)a[i])<<i*8);
  }
  return 0;

}

/************************************************************************/
/**
 * @param [in] a: An array of 4 bytes. 
 * @param [in] swap: Whether to swap bytes positions or not. 
 * @param [out] out: A 32-bit unsigned number. 
 * @returns      0 for success, 1 for a failure. 
 * @brief  
 * Converts an array of bytes into an unsigned 32-bit unsigned. 
 **/

int uint8a_to_uint32(uint8_t* a, uint32_t* out,uint8_t swap){
  int i;
  *out = 0;
  for(i=0;i<4;i++){
    if(1==swap)
      *out |= (((uint32_t)a[3-i])<<i*8);
    else
      *out |= (((uint32_t)a[i])<<i*8);
  }
  return 0;

}
/************************************************************************/

/**
 * @param [in] c: An array of 4 uint8_t. 
 * @param [out] out: An array of 4 uint8_t
 * @returns      0 for success, 1 for a failure. 
 * @brief  
 * Function is mainly used to copy one array into another. 
 **/

int uint8a_to_uint8a(uint8_t* c,uint8_t* u,int size){

  int i;
  for(i=0;i<size;i++){
    u[i] = c[i];
  }
  return 0;

}


/**
 * @param   [out] o_array*: An array bytes
 *          [in] i_count: size of the array
 *          [in] ....: variable number of arguments each of type byte 
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * The function takes a variable number of arguments and loads them in the array. Each argument is of 
 * the type uint8_t
 **/


void packa(uint8_t* i_array, int i_count, ...){

	va_list ap;
  int i;
	va_start(ap,i_count);
	for(i=0;i<i_count;i++)
		i_array[i] = va_arg(ap,int);
	va_end(ap);

  }



/**
 * @param   [in] i_array*: An array bytes
 *          [in] i_count: size of the array
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * Prints the input byte array
 **/


void printa(uint8_t* i_array, int i_count){

	int i;

	for(i=0;i<i_count;i++)
	  printf("%u ", i_array[i]);
	printf("\n");

}
/***********************************************************************************/

/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/

  
uint8_t is_zero(uint8_t* data, int size){
  
  int i; 
  for(i=0;i<size;i++)
    if(data[i]!=0)
      return 1;

  return 0;

}

/***********************************************************************************/




