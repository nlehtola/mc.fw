#include "logging.h"

int loggingInit = 0;
int verbosityLevel = 0;

int spOpenLog(const char* identity, int verbosity)
{
	if (!loggingInit) {
		loggingInit = 1;
		openlog(identity, SYSLOG_OPTION, SYSLOG_FACILITY);
	}
	if (verbosity>verbosityLevel) {
		verbosityLevel = verbosity;
	}
	return 1;
}

// at some point these might be changed to add extra info, such as an "Error: " prefix
void spLogError(const char* format, ...)
{
	if (loggingInit) {
		va_list argptr;
		va_start(argptr, format);
		vsyslog(LOG_ERR, format, argptr);
		vfprintf(stdout, format, argptr);
		va_end(argptr);
		fprintf(stdout, "\n");
	}
}

void spLogWarning(const char* format, ...)
{
	if (loggingInit) {
		va_list argptr;
		va_start(argptr, format);
		vsyslog(LOG_WARNING, format, argptr);
		vfprintf(stdout, format, argptr);
		va_end(argptr);
		fprintf(stdout, "\n");
	}
}

void spLogNotice(const char* format, ...)
{
	if (loggingInit) {
		va_list argptr;
		va_start(argptr, format);
		vsyslog(LOG_NOTICE, format, argptr);
		vfprintf(stdout, format, argptr);
		va_end(argptr);
		fprintf(stdout, "\n");
	}
}

void spLogDebug(int verbosity, const char* format, ...)
{
	if (verbosityLevel>0) {
		if ((verbosityLevel>=verbosity)&&loggingInit) {
			va_list argptr;
			va_start(argptr, format);
			vsyslog(LOG_DEBUG, format, argptr);
			vfprintf(stdout, format, argptr);
			va_end(argptr);
			fprintf(stdout, "\n");
		}
	}
}

// Prints a debug message without appending a newline when printing to stdout
void spLogDebugNoCR(int verbosity, const char* format, ...)
{
	if (verbosityLevel>0) {
		if ((verbosityLevel>=verbosity)&&loggingInit) {
			va_list argptr;
			va_start(argptr, format);
			vsyslog(LOG_DEBUG, format, argptr);
			vfprintf(stdout, format, argptr);
			va_end(argptr);		
		}
	}
}


void spCloseLog()
{
	loggingInit = 0;
	verbosityLevel = 0;
	closelog();
}
