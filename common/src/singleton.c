#include "singleton.h"

char* myPidfile = NULL;

// returns -1 if lock could not be aquired (implies another process instance exists)
// returns 0 on successful creation of lock file
int getSingletonLock(const char* pidfile)
{
	FILE *fd = fopen(pidfile, "r");
	const char* myPidfile = pidfile;
	
	if (fd!=NULL) {
		// read pid from file
		int testPid = -1;
		fscanf(fd, "%d", &testPid);
		
		if (testPid>1) {
			if (kill(testPid,0)!=-1) {
				// process does exist
				// this means that another process has created a file with its pid and is still running
				// however there is a very remote chance that a process could die without removing it's lock file and 
				// another process which does not represent an instance of this program could steal its old pid
				fclose(fd);
				return -1;
			}
		}
		// truncate file
		fd = freopen(pidfile, "w+", fd);
	} else {
		// truncate file
		fd = fopen(pidfile, "w+");
	}
	
	// if we got to this point it means that there was no valid pid in the file, which means that no other
	// instance is running. We will now put our pid in the file to create a lock	
	pid_t ourPid = getpid();

	if (fd==NULL) {
		spLogWarning("Warning, pid file may be read only\n");
		return -1;
	}

	// write pid to file
	fprintf(fd, "%d", (int)ourPid);
	
	fclose(fd);
	return 0;
}

void removeSingletonLock()
{
	if (myPidfile!=NULL) {
		remove(myPidfile);
	}
}	

