#ifndef SINGLETON_H
#define SINGLETON_H

#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>

#include "logging.h"

#ifdef __cplusplus
extern "C" {
#endif

// returns -1 if lock could not be aquired (implies another process instance exists)
// returns 0 on successful creation of lock file
int getSingletonLock(const char* pidfile);

// removes a pid file created by getSingletonLock
void removeSingletonLock();

#ifdef __cplusplus
}
#endif

#endif //SINGLETON_H
