/*  
 * File Name     : conversions.h
 * Author        : Usama Malik
 * Date Created  : 13/02/2013
 * Description: 
 * 
 *
 * Copyright 2013, Southern Photonics/Coherent Solutions. www.southernphotonics.com
 * All Rights Reserved.
 */

#ifndef CONVERSION_H
#define CONVERSIONS_H

#include "common.h"

int int32_to_uint8a(int32_t*, uint8_t*, uint8_t);
int uint8a_to_int32(uint8_t* a, int* out,uint8_t swap);
int uint32_to_uint8a(uint32_t*, uint8_t*, uint8_t);
int uint8a_to_uint32(uint8_t*, uint32_t*, uint8_t);
int uint8a_to_uint8a(uint8_t*,uint8_t*,int);
void packa(uint8_t*, int, ...);
void printa(uint8_t*,int);
uint8_t is_zero(uint8_t* data, int size);
void gen_random_bytes(int length, uint8_t* o_data);
int check_bytes(int length, uint8_t* a, uint8_t* b);


#endif
