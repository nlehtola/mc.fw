#ifndef VERSION_H
#define VERSION_H

// Combined version number for Coherent Solutions software
#define TLSX_VERSION "4.9"

// the value returned by the SYStem:VERSion? command
// As specified by SCPI99 Command Reference 21.21
#define SCPI_COMPLIANCE_VERSION "1999.0"

#endif //VERSION_H

