/*
 * File Name     : errors.h
 * Author        : Usama Malik
 * Date Created  : 12/02/2014
 * Description:
 * Defining error codes for uniform reporting.
 *
 * Copyright 2013, Southern Photonics/Coherent Solutions. www.southernphotonics.com
 * All Rights Reserved.
 */

#ifndef ERROR_H
#define ERROR_H

#define SUCCESS 0
#define NO_ERROR 0
#define ERROR     1

#define B2S_NO_ERROR          128
#define B2S_INVALID_COMMAND   129
#define B2S_INVALID_BLADE_ID  130
#define B2S_INVALID_LASER_ID  131
#define B2S_BLADE_BUSY        132
#define B2S_EXEC_ERROR        133
#define B2S_BLADE_NOT_PRESENT 134
#define B2S_LASER_NOT_PRESENT 135
#define B2S_LASER_NO_RESPONSE 136
#define B2S_SPI_ERROR         137
#define B2S_MALFORMED_DATA    138
#define B2S_PARAMETER_ERROR   139
#define B2S_UNKNOWN_ERROR     140

// Removing the above may cause old projects from compiling
// The following is the unified error code table for TLSX>4.0, including VOA
// Negative numbers are errors, positive numbers are reserved for valid return
// values of functions depending on context
// Numbers between -1 and -99 are available for local error codes
// Numbers between -100 and -199 are reserved for VXI11 errors
#define VXI_ERR_MISC							-100
// Numbers between -200 and -299 are reserved for libSCPI errors
#define LIBSCPI_ERR_MISC						-200
#define LIBSCPI_ERR_INVALID_CALLBACK_ID			-201
#define LIBSCPI_ERR_CALLBACK_NOT_REGISTERED		-202
#define LIBSCPI_ERR_INVALID_CHANNEL_ID			-203
// Numbers between -300 and -399 are reserved for SCPIServer errors
#define SCPISERVER_ERR_MISC						-300
#define SCPISERVER_ERR_INVALID_BLADE_ID			-301
#define SCPISERVER_ERR_TASK_UNDEFINED			-302
#define SCPISERVER_ERR_ATTENUATION_NEGATIVE		-303
#define SCPISERVER_ERR_INVALID_WAVELENGTH_SET	-304
// Numbers between -400 and -499 are reserved for kblade errors
#define KBLADE_ERR_MISC							-400
// Numbers between -500 and -599 are reserved for blade/firmware errors
#define BLADE_ERR_MISC							-500
// Numbers between -600 and -700 are reserved for Misc *TST? self test
// error codes which aren't covered by other error codes
#define TST_ERR_MISC							-600
#define TST_ERR_COULD_NOT_INITIALIZE_HARDWARE	-601

#endif
