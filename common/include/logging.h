#ifndef SPLOG_H
#define SPLOG_H
// This is a wrapper for logging errors to the system log file

#include <stdarg.h>
#include <syslog.h>

#define SYSLOG_OPTION LOG_NDELAY
#define SYSLOG_FACILITY LOG_USER

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

// returns 1 on success
int spOpenLog(const char* identity, int verbosity);
void spLogError(const char* format, ...);
void spLogWarning(const char* format, ...);
void spLogNotice(const char* format, ...);
void spLogDebug(int verbosity, const char* format, ...);
void spLogDebugNoCR(int verbosity, const char* format, ...);
void spCloseLog();

#ifdef __cplusplus
}
#endif

#endif //SPLOG_H
