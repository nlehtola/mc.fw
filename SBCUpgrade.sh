#!/bin/bash

#!/bin/bash
# Argument = -t test -r server -p password -v

usage()
{
cat << EOF
usage: $0 <ip> <file>

This script applies the firmware upgrade package in <file> to the IQS-636, FLS-2800 or FVA-3800 unit located at <ip>

OPTIONS:
   <ip>    The ip address of the instrument
   <file>  The file name of the firmware package
EOF
}

if [[ -z $1 ]] || [[ -z $2 ]]
then
    usage
    exit 1
fi

IP=$1
FILE=$2

echo "Upgrading Firmware of $IP:"
echo "Copying file to device: $FILE"
scp $FILE root@$IP:/tmp
if [ $? -ne 0 ]; then
    exit 1
fi
echo "Installing Firmware..."

NAME=$(basename "$FILE")
ssh root@$IP "tlsx_installer -i${NAME%.*};reboot;exit"

echo "Done"

