ifdef TOOL_PREFIX
CC = $(TOOL_PREFIX)gcc
AR = $(TOOL_PREFIX)ar
LD = $(TOOL_PREFIX)ld
CXX = $(TOOL_PREFIX)g++
CFLAGS += -march=armv4 -ffunction-sections -fdata-sections
endif

ARCH ?= noncavium

LIBS += ./common/libcommon.so
LIBS += components/libSCPI/libSCPI.so
ifdef TOOL_PREFIX
LIBS += ./components/libtsctl/noncavium/libtsctl.so
LIBS += ./components/libcrc/libcrc.so
LIBS += ./components/libbcoms/libbcoms.so
LIBS += ./components/kowhai/libkowhai.so
LIBS += ./components/libbl/libbl.so
LIBS += ./components/b2s/libb2s.so
LIBS += ./components/encoder/libencoder.so
LIBS += ./components/lcp/liblcp.so
LIBS += ./components/vxi_local/libvxilocal.so
LIBS += ./components/iface/libiface.so
LIBS += ./components/libkblade/libkblade.so
endif
LIBS += ./services/IQTX2Server/obj/libfakekblade.so

ifdef TOOL_PREFIX
#SERVICES += services/B2SServer/bin/B2SServer
#SERVICES += services/TLSXServer/bin/TLSXServer
#ifdef IQTX2
SERVICES += services/menu_iqtx2/menu
#else
#SERVICES += services/menu_fls/menu
#endif
endif
#SERVICES += services/SCPIServer/bin/SCPIServer
#SERVICES += services/VXI11Server/bin/VXI11Server
SERVICES += services/IQTX2Server/bin/SCPIServer

ifdef TOOL_PREFIX
TOOLS += components/libkblade/kblade.py
TOOLS += tools/tlsx_bootloader/tlsx_bootloader
TOOLS += tools/tlsx_debug/tlsx_debug
TOOLS += tools/tlsx_ls/tlsx_ls
TOOLS += tools/tlsx_ls/ls_test.sh
TOOLS += tools/tlsx_rom/tlsx_rom
TOOLS += tools/tlsx_select_blade/tlsx_select_blade
TOOLS += tools/bls/bls
TOOLS += tools/blade_tmp/btmp
TOOLS += tools/bset/bset
endif

# if this is defined then the SCPIServer will not attempt to talk to
# the TLSXServer, values returned in queries will be placeholders
#TLSXSERVER_BACKEND_DISABLED = 1

#CFLAGS += -Wall

ifdef DEBUG_LEVEL 
CFLAGS += -DDEBUG_LEVEL=$(DEBUG_LEVEL)
endif

ifdef TLSXSERVER_BACKEND_DISABLED
CFLAGS += -DTLSXSERVER_BACKEND_DISABLED=$(TLSXSERVER_BACKEND_DISABLED)
endif

CPPFLAGS += $(CFLAGS)

ifndef INSTALL_PATH
INSTALL_PATH = ./bin/
ECHO_INSTALL_HELP = 1
endif

export TOOL_PREFIX
export CC
export AR
export LD
export CXX
export CFLAGS
export CPPFLAGS

.PHONY: all clean partialclean install package

ifdef TOOL_PREFIX
all:
	$(MAKE) -C components/linux CROSS_COMPILE=$(TOOL_PREFIX) ARCH=arm KBUILD_NOPEDANTIC=1
	#$(MAKE) -C services/B2SServer
	#$(MAKE) -C services/VXI11Server
	#$(MAKE) -C services/SCPIServer
	#$(MAKE) -C services/TLSXServer
	$(MAKE) -C services/IQTX2Server
	$(MAKE) -C services/menu_fls
	$(MAKE) -C services/menu_voa
	$(MAKE) -C services/menu_iqtx2
	$(MAKE) -C tools
else
all:
	#$(MAKE) -C services/VXI11Server
	#$(MAKE) -C services/SCPIServer
	$(MAKE) -C services/IQTX2Server
endif

clean:
	#$(MAKE) -C services/B2SServer clean
	#$(MAKE) -C services/VXI11Server clean
	#$(MAKE) -C services/SCPIServer clean
	#$(MAKE) -C services/TLSXServer clean
	$(MAKE) -C services/IQTX2Server clean
	$(MAKE) -C services/menu_fls clean
	$(MAKE) -C services/menu_voa clean
	$(MAKE) -C services/menu_iqtx2 clean
	$(MAKE) -C tools clean
	
install:
ifdef ECHO_INSTALL_HELP 
	@echo -e "\nDoing local install to $(INSTALL_PATH). To setup remote install:"
	@echo -e "\tfirst mount the device via ssh"
	@echo -e "\t\teg: sudo sshfs -o allow_other root@192.168.1.49:/ /mnt/tlsx_dev/"
	@echo -e "\tthen let make know the location of the device"
	@echo -e "\t\texport INSTALL_PATH=/mnt/tlsx_dev/root/dev\n"
endif
	@mkdir -p $(INSTALL_PATH)
	cp $(LIBS) $(INSTALL_PATH)
	cp $(SERVICES) $(INSTALL_PATH)
	cp components/ifchange $(INSTALL_PATH)
ifdef TOOL_PREFIX
	cp components/linux/arch/arm/boot/zImage $(INSTALL_PATH)
	cp components/lcd_driver/fls_lcd.ko $(INSTALL_PATH)
	cp $(TOOLS) $(INSTALL_PATH)
	cp components/pif_loop.sh components/getPifMessage.py $(INSTALL_PATH)
	cp components/pif/daemon.py components/pif/pif.py $(INSTALL_PATH)
	cp components/splash_msg components/stopped_msg components/resume_msg $(INSTALL_PATH)
	cp components/ServerLoop.sh $(INSTALL_PATH)
endif

PACKAGE_TEMP := package_temp
#makes an upgrade package
package:
ifdef TOOL_PREFIX
	@mkdir -p $(PACKAGE_TEMP)
	cp $(LIBS) $(PACKAGE_TEMP)
	cp $(SERVICES) $(PACKAGE_TEMP)
	cp components/ifchange $(PACKAGE_TEMP)
	cp components/linux/arch/arm/boot/zImage $(PACKAGE_TEMP)
	cp components/lcd_driver/fls_lcd.ko $(PACKAGE_TEMP)
	cp $(TOOLS) $(PACKAGE_TEMP)
	cp components/pif_loop.sh components/getPifMessage.py $(PACKAGE_TEMP)
	cp components/pif/daemon.py components/pif/pif.py $(PACKAGE_TEMP)
	cp components/splash_msg components/stopped_msg components/resume_msg $(PACKAGE_TEMP)
	cp components/ServerLoop.sh $(PACKAGE_TEMP)
	zip -j 9.9.zip package_temp/*
else
	@echo -e "TOOL_PREFIX must be set when making a package"
endif

