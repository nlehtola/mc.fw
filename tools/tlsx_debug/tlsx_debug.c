#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>     

#include "b2s_wrapper.h"

void print_usage();


int verbose = 1 ;

int main(int argc , char *argv[])
{

  ushort hflag = 0;
  ushort tflag = 0;
  ushort bflag = 0;
  ushort lflag = 0;
  ushort sflag = 0;
  ushort cflag = 0;
  ushort dflag = 0;

  uint option; 

  uint8_t blade_id; 
  uint8_t laser_id=0; 
  uint8_t sequence=1; 
  uint8_t command=0; 
  uint32_t user_data=0; 
  uint8_t i_data[4] = {0};
  uint8_t o_data[4] = {0};
  uint8_t o_status = 0;

  user_data_in ui; 
  user_data_out uo;

  int i;

    while ((option = getopt (argc, argv, "htb:l:s:c:d:")) != -1)
         switch (option)
           {

	   case 'h':	    
	     print_usage();
	     exit(0);
	   case 't':	    
	     tflag = 1;
	     break;;
	   case 'b':
	     blade_id = atoi(optarg);

	     bflag = 1;	     
	     break;
	   case 'l':	    
	     laser_id = atoi(optarg);
	     printf("Laser %u\n", laser_id);
	     lflag = 1;	     
	     break;
           case 's':
	     sequence = atoi(optarg);
	     sflag = 1;
	     break; 
           case 'c':
	     command = atoi(optarg);
	     cflag = 1;
	     break; 
           case 'd':
	     user_data = atoi(optarg);
	     dflag = 1;
	     break; 
           case '?':
             if (optopt == 'p')
               fprintf (stderr, "Option -%p requires an argument.\n", optopt);
             else
               fprintf (stderr,
                        "Unknown option character `\\x%x'.\n",
                        optopt);
             return 1;
           default:
             return 0;
           }
    
    if (0==bflag){
      printf("Must supply a blade number. \n");
      exit(1);      
    }

    if(1==tflag)
      printf("Command-Query:YES,");
    else if (1==cflag)
      printf("Command:YES,");
    else
      printf("Query:YES,");

    printf("Blade=%u,Sequence=%u,", blade_id, sequence);
    if(1==cflag | 1==tflag){
      printf("Laser=%u,Command=%u\n", laser_id, command);
    }
    //Convert 32 bit word to 4 bytes
    uint32_to_uint8a(&user_data, i_data,0); 
    printf("User data=0x%08x:u%u\n", user_data, user_data);
    for(i=0;i<4;i++)
      printf("b2s_data[%u]=0x%02x:0u%u\n",i,i_data[i], i_data[i]);


    b2s_configure();

    if(1==tflag){
      float f;
      printf("Running Transaction.....\n\n");
      run_blade_command(blade_id, laser_id, sequence, command, i_data, o_data, &o_status);
      b2s_print_query_status(o_status); 
      printf("\n");
      uint8a_to_uint32(o_data, &user_data,0);
      memcpy(&f, &user_data, sizeof(f));
      printf("Received data=0x%08x:d%d:u%u:f%f\n", user_data, (int)user_data, user_data, f);
      for(i=0;i<4;i++) {
        if (o_data[i] > 31 && o_data[i] < 127)
          printf("b2s_data[%u]=0x%02x:0u%u\t['%c']\n",i, o_data[i], o_data[i], (char)o_data[i]);
        else
          printf("b2s_data[%u]=0x%02x:0u%u\n",i, o_data[i], o_data[i]);
      }
      b2s_unconfigure();
      return;
    }
    //Now we need to run either command or a query

    ui.blade_id       = blade_id;
    ui.laser_address  = laser_id;
    ui.sequence       = sequence;
    ui.command        = command;
    uint8a_to_uint8a(i_data, ui.data);
    
    if(1==cflag){
      printf("\nSending command frame....\n");
      b2s_command(&ui, &uo);    
      print_command_status(uo.status); 
      printf("\n");
    }
    else{
      float f;
      printf("Sending query frame....\n");
      b2s_query(&ui, &uo);
      b2s_print_query_status(uo.status); 
      printf("\n");   
      uint8a_to_uint32(uo.data, &user_data,0);
      memcpy(&f, &user_data, sizeof(f));
      printf("Received data=0x%08x:d%d:u%u:f%f\n", user_data, (int)user_data, user_data, f);
      for(i=0;i<4;i++) {
        if (uo.data[i] > 31 && uo.data[i] < 127)
          printf("b2s_data[%u]=0x%02x:0u%u\t['%c']\n",i,uo.data[i], uo.data[i], (char)uo.data[i]);
        else
          printf("b2s_data[%u]=0x%02x:0u%u\n",i,uo.data[i], uo.data[i]);
      }
    }
    
    b2s_unconfigure();

}


void print_usage(){

  printf("%tlsx_debug -b[options]\n");
  printf("Options:\n");
  printf("\t-h: prints this message.\n");
  printf("\t-b: Mandatory blade number.\n");
  printf("\t-l: Laser number (for commands that require it)\n");
  printf("\t-s: Sequence number. If left it will be set to 1\n");
  printf("\t-c: Command number.\n");
  printf("\t-d: Data. Taken as a 32 bit usigned integer. Only required for set commands\n");
  printf("\t-t: When this flag is present a complete B2S transaction is performed.\n");
  printf("\t    If this flags is absent then if a command is provided only command frame is sent.\n");
  printf("\t    If a command is not provided then a query frame is sent.\n");

}


