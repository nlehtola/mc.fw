#!/bin/bash

for b in {0..15}
do
	# set crc on
	echo ""
	echo "Setting crc on for blade $b"
	./tlsx_debug -b $b -l0 -c 128 -d 1 -t
done
