#include "b2s.h"
#include <stdio.h>
#include <getopt.h>


void help(char *name)
{
  printf("%s [options] <blade_id>\n", name);
  printf("\t -p[=delay]\n\t\tselect the blade for pause number of seconds (then deinit and hence un select)\n");
  printf("\t -k\n\t\twait for a return key until un-selecting the blade\n");
  printf("\t -x\n\t\tnever unselect blade (experimental as it never does a de init !)\n");

}

int main(int argc, char **argv)
{
  // args
  int op, pause = -1, key = 0, unconfigure = 1, blade_id;
  while ((op = getopt(argc, argv, "p:kxh")) != -1)
  {
    switch (op)
    {
      case 'p':
        pause = atoi(optarg);
        break;
      case 'k':
        key = 1;
        break;
      case 'x':
        unconfigure = 0;
        break;
      case 'h':
        help(argv[0]);
        return 0;
      default:
        help(argv[0]);
        return -1;
    }
  }
  if (optind != argc - 1)
  {
    printf("missing blade_id (mandatory arg)\n");
    help(argv[0]);
    return -1;
  }
  blade_id = atoi(argv[optind]);
  printf("selecting blade %d\n", blade_id);


  b2s_configure();	
  b2s_select(blade_id);

  if (pause > 0){
    sleep(pause);
  }

  if (key){
    printf("press enter to continue: ");
    fflush(stdout);
    getchar();
    printf("\n");
  }

  if (unconfigure)
    b2s_unconfigure();
}


