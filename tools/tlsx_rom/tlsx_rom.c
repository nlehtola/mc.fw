#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>     
#include <getopt.h>

#include "b2s_wrapper.h"

#define PARAMETER_SIZE 32

int size_of_string(char *);
void set_parameter(uint8_t,uint8_t, uint8_t*);
void rom_write(uint8_t,uint8_t,uint8_t,uint8_t*);

int verbose = 1 ;

void help(char *name)
{
  printf("%s <blade> [options]\n\n", name);
  printf("\t -b[=blade_id]\n\t\taddress this command to the blade in slot blade_id [required for all commands]\n");
  printf("\t -l[=laser_id]\n\t\taddress this command the laser in position laser_id [required for some commands]\n");
  printf("\t -n[=number_of_lasers]\n\t\tset the number of lasers connected to the blade\n");
  printf("\t -s[=serial_number]\n\t\tset the serial number of the blade\n");
  printf("\t -H[=HW version number]\n\t\tset the hardware version number of the blade\n");
  printf("\t -m[=model_number]\n\t\tset the model number of the blade\n");
  printf("\t --power_coeff[=coeff0,coeff1]\n\t\tset the power correction coefficient vector power_to_laser(mdBm) -= coeff .* [1, freq(MHz)]\n");
  printf("\t --power_min_max_correction[=min_cor,max_cor]\n\t\tset the power corrections for the lasers min/max values min/max -= min/max_cor (mdBm)\n");

  printf("\t -h, --help\n\t\tshow this menu\n");
}

int main(int argc , char *argv[])
{
  ushort sflag = 0;
  ushort hflag = 0;
  ushort mflag = 0;
  ushort bflag = 0;
  ushort nflag = 0;
  ushort pcoeff_flag = 0;
  ushort pminmax_cor_flag = 0;

  uint option; 
  uint8_t serial[PARAMETER_SIZE] ={0}; 
  uint8_t hw_version[PARAMETER_SIZE] = {0};
  uint8_t num_lasers[PARAMETER_SIZE] = {0};
  uint8_t model[PARAMETER_SIZE] = {0};
  uint8_t blade_id; 
  uint8_t laser_id = 0; 
  int input_size; 
  int i;
  uint8_t data[4];
  float power_coeff[2] = {0};
  int32_t power_minmax_cor[2] = {0};
  int index;
  int r;

  static struct option long_options[] = {
    {"power_coeff", 1, NULL, 1},
    {"power_min_max_correction", 1, NULL, 2},
    {"help", 0, NULL, 'h'},
    {NULL, 0, NULL, 0}
  };
 
  while ((option = getopt_long(argc, argv, "b:s:H:m:n:l:h", long_options, &index)) != -1){
    switch (option){
      case 1:
        r = sscanf(optarg, "%f,%f", &power_coeff[0], &power_coeff[1]);
        printf("power_coeff = %f, %f\n", power_coeff[0], power_coeff[1]);
        if (r > 0)
          pcoeff_flag = 1;
        break;
      case 2:
        r = sscanf(optarg, "%d,%d", &power_minmax_cor[0], &power_minmax_cor[1]);
        printf("power_minmax_cor = %d, %d\n", power_minmax_cor[0], power_minmax_cor[1]);
        if (r > 0)
          pminmax_cor_flag = 1;
        break;
      case 'l':
        laser_id = atoi(optarg);
        printf("laser %u\n", blade_id);
        break;
      case 'b':
        blade_id = atoi(optarg);
        printf("Blade %u\n", blade_id);
        bflag = 1;       
        break;
      case 'n':      
        num_lasers[0] = atoi(optarg);
        printf("Number of lasers %u\n", num_lasers[0]);
        nflag = 1;       
        break;
      case 's':
        printf("Serial: %s\n", optarg); 
        printf("%d\n", size_of_string(optarg));
        input_size = size_of_string(optarg);
        for(i=0;i<input_size;i++){
          serial[i] = optarg[i];
          printf("%c\n", serial[i]);
        }
        sflag = 1;
        break; 
      case 'H':
        printf("HW version: %s\n", optarg); 
        printf("%d\n", size_of_string(optarg));
        input_size = size_of_string(optarg);
        for(i=0;i<input_size;i++){
          hw_version[i] = optarg[i];
          printf("%c\n", hw_version[i]);
        }
        hflag = 1;
        break;
      case 'm':
        printf("Model: %s\n", optarg); 
        printf("%d\n", size_of_string(optarg));
        input_size = size_of_string(optarg);
        for(i=0;i<input_size;i++){
          model[i] = optarg[i];
          printf("%c\n", model[i]);
        }
        mflag = 1;
        break;
      case '?':
        if (optopt == 'p')
          fprintf (stderr, "Option -%p requires an argument.\n", optopt);
        else
          fprintf (stderr,
          "Unknown option character `\\x%x'.\n",
          optopt);
        return -1;
      case 'h':	
      default:
        help(argv[0]);
        return 1;
    }
  }
    
  if (0==bflag){
    printf("Must supply a blade number. \n");
    exit(1);      
  }

  b2s_configure();

  if(sflag==1)
    set_parameter(blade_id,SET_SERIAL_NUMBER , serial);

  if(hflag==1)
    set_parameter(blade_id,SET_HW_VERSION , hw_version);

  if(mflag==1)
    set_parameter(blade_id,SET_MODEL_NUMBER , model);

  if(nflag==1)
    set_parameter(blade_id,SET_NUM_LASERS , num_lasers);

  if (pcoeff_flag == 1)
  {
    if (laser_id == 0){
      printf("Must supply laser number for this operation\n");
      exit(1);
    }
    rom_write(blade_id, laser_id, SET_FCOEFF0, (uint8_t *)&power_coeff[0]);
    rom_write(blade_id, laser_id, SET_FCOEFF1, (uint8_t *)&power_coeff[1]);
  }
  
  if (pminmax_cor_flag == 1)
  {
    if (laser_id == 0){
      printf("Must supply laser number for this operation\n");
      exit(1);
    }
    rom_write(blade_id, laser_id, SET_POWER_MIN_CORRECTION, (uint8_t *)&power_minmax_cor[0]);
    rom_write(blade_id, laser_id, SET_POWER_MAX_CORRECTION, (uint8_t *)&power_minmax_cor[1]);
  }

  //Finally issue commit command
  printf("Commiting..\n");
  rom_write(blade_id, 0,COMMIT_ST,data);

  b2s_unconfigure();
  return 0;
}

//We assume that serial is of size 16 bytes
void set_parameter(uint8_t blade_id, uint8_t command,uint8_t* data){

  int i;
  //We will program the serial in 4 words (each 4 byte). 
  
  for(i=0;i<8;i++){    
    rom_write(blade_id, i, command, &data[i*4]);
    printf(" blade: %u, index: %u, command:%u, %c %c %c %c \n", blade_id, i, command, data[i*4+0], data[i*4+1], data[i*4+2], data[i*4+3]);
  }
}//set_parameter


int size_of_string(char *array){

  int i=0;

  while(array[i]!=0){
    i++;
  }
  return i;

}

void rom_write(uint8_t blade_id, uint8_t address, uint8_t command, uint8_t* tx_data){

  int i;     
  
  uint8_t to_blade[16];
  uint8_t from_blade[16];
  
  to_blade[0] = blade_id;
  to_blade[1] = address;
  to_blade[2] = 1;
  to_blade[3] = command;

  for(i=0;i<4;i++)
    to_blade[4+i] = tx_data[i];

  b2s_transaction(to_blade,from_blade);

  printf("Status: %u\n", from_blade[3]);

}
