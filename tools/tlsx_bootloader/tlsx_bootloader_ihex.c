#include <stdlib.h>
#include <ihex.h>
#include <tlsx_bootloader_ihex.h>
#include <crc.h>


static struct crc_h stm32f10x_crc_h =
{
	// this setup mimics the stm32 32bit crc hardware
	// see: https://my.st.com/public/STe2ecommunities/mcu/Lists/cortex_mx_stm32/Flat.aspx?RootFolder=https%3a%2f%2fmy%2est%2ecom%2fpublic%2fSTe2ecommunities%2fmcu%2fLists%2fcortex_mx_stm32%2fCRC%20calculation%20in%20software&FolderCTID=0x01200200770978C69A1141439FE559EB459D7580009C4E14902C3CDE46A77F0FFD06506F5B&currentviews=3822
	{
		32, 			// width
		0x04C11DB7, 	// poly
		0xFFFFFFFF, 	// init
		FALSE, 			// refin
		FALSE, 			// refot
		0, 				// xorot

		0,				// working reg (irgnore this in cmp)
	},

	NULL,
	0,
	
	CRC_METHOD_BEST,
};


void ihex_print_line(struct ihex_line_t *ihex_lines)
{
	printf("%.8X[%.3d]: 0x%.2X 0x%.2X ... 0x%.2X 0x%.2X\n", ihex_lines->addr,
		ihex_lines->len, ihex_lines->data[0], ihex_lines->data[1], 
		ihex_lines->data[ihex_lines->len - 1], ihex_lines->data[ihex_lines->len]);
}


void ihex_print_lines(struct ihex_line_t *ihex_lines, int lines)
{
	int l;

	for (l = 0; l < lines; l++)
	{
		ihex_print_line(&ihex_lines[l]);
	}
}


#define MIN(a,b) ((a < b)? a: b)
int ihex_min_addr(struct ihex_line_t *ihex_lines, int lines)
{
	uint32_t min_addr = ihex_lines[0].addr;
	int l;

	for (l = 1; l < lines; l++)
		min_addr = MIN(min_addr, ihex_lines[l].addr);
	
	return min_addr;
}


#define MAX(a,b) ((a > b)? a: b)
int ihex_max_addr(struct ihex_line_t *ihex_lines, int lines)
{
	uint32_t max_addr = ihex_lines[0].addr;
	int l;

	for (l = 1; l < lines; l++)
		max_addr = MAX(max_addr, ihex_lines[l].addr + ihex_lines[l].len);
	
	return max_addr;
}


struct ihex_map
{
	uint32_t start_addr;
	uint32_t len;
	void* buf __attribute__ ((aligned (4)));
};


int ihex_map_read(struct ihex_map *map, uint32_t addr, void *dst, uint32_t len)
{
	void *src;

	// bounds check
	if (map == NULL)
		return -1;
	if (addr < map->start_addr || addr >= (map->start_addr + map->len))
		return -1;
	if ((addr + len) < map->start_addr || (addr + len) > (map->start_addr + map->len))
		return -1;
	
	src = (uint8_t *)map->buf + (addr - map->start_addr);
	memcpy(dst, src, len);
	return len;
}


int ihex_map_write(struct ihex_map *map, uint32_t addr, void *src, uint32_t len)
{
	void *dst;

	// bounds check
	if (map == NULL)
		return -1;
	if (addr < map->start_addr || addr >= (map->start_addr + map->len))
		return -1;
	if ((addr + len) < map->start_addr || (addr + len) > (map->start_addr + map->len))
		return -1;
	
	dst = (uint8_t *)map->buf + (addr - map->start_addr);
	memcpy(dst, src, len);
	return len;
}


int ihex_map_crc(struct ihex_map *map, uint32_t addr, uint32_t len, uint32_t *crc)
{
	int offset = addr - map->start_addr;

	// bounds check
	if (map == NULL)
		return -1;
	if (addr < map->start_addr || addr > (map->start_addr + map->len))
		return -1;
	if ((addr + len) < map->start_addr || (addr + len) > (map->start_addr + map->len))
		return -1;
	
	crc_init(&stm32f10x_crc_h);
	*crc = crc_buf(&stm32f10x_crc_h, (uint8_t *)map->buf + offset, len, true);
	return len;
}


void ihex_free_map(struct ihex_map *map)
{
	if (map == NULL)
		return;
	if (map->buf != NULL)
		free(map->buf);
	free(map);
}


static struct ihex_map *ihex_init_map(struct ihex_line_t *ihex_lines, int lines)
{
	struct ihex_map *map = malloc(sizeof(struct ihex_map));
	int l;

	// alloc new map
	if (map == NULL)
		return NULL;
	map->start_addr = ihex_min_addr(ihex_lines, lines);
	map->len = ihex_max_addr(ihex_lines, lines) - map->start_addr;

	// alloc map buffer
	map->buf = NULL;
	map->buf = malloc(map->len);
	if (map->buf == NULL)
	{
		ihex_free_map(map);
		return NULL;
	}
	
	// populate map
	memset(map->buf, 0xff, map->len); // flash defaults to 0xff is not written
	for (l = 0; l < lines; l++)
	{
		struct ihex_line_t *line = &ihex_lines[l];
		ihex_map_write(map, line->addr, line->data, line->len);
	}

	// done
	return map;
}


int ihex_load(char *filename, struct ihex_line_t **ihex_lines, struct ihex_map **map)
{
	FILE *fp = NULL;
	uint32_t base_addr = 0;
	int lines = 0;
	IHexRecord irec;
	int l = 0;

	// open the file
	fp = fopen(filename, "r");
	if (fp == NULL)
		return -1;

	// count the number of hex records in the file
	while (Read_IHexRecord(&irec, fp) == IHEX_OK)
		if (irec.type == 0)
			lines++;

	// create all the hex lines
	*ihex_lines = malloc(lines * sizeof(struct ihex_line_t));
	if (*ihex_lines == NULL)
		return -1;

	// load each hex line and keep address updated
	rewind(fp);
	l = 0;
	while (1)
	{
		// read next line
		int r = Read_IHexRecord(&irec, fp);
		
		// return in error unless it is EOF or success
		if (r == IHEX_ERROR_EOF)
			break;
		if (r != IHEX_OK)
			return -1;

		switch (irec.type)
		{
			case 0:
				// normal hex line to load
				(*ihex_lines)[l].addr = base_addr | irec.address;
				memcpy((*ihex_lines)[l].data, irec.data, irec.dataLen);
				(*ihex_lines)[l].len = irec.dataLen;
				(*ihex_lines)[l].type = 0;
				(*ihex_lines)[l].checksum = irec.checksum;
				l++;
				break;
			case 4:
				// extended address (update base address
				base_addr = ((irec.data[0] << 24) | (irec.data[1] << 16)) & 0xffff0000;
				break;
		}
	}

	// optionally load a map
	if (map != NULL)
		*map = ihex_init_map(*ihex_lines, lines);

	return lines;
}

