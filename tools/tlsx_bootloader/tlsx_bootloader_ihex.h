#ifndef __IHEX_H__
#define __IHEX_H__

#include <stdint.h>

struct ihex_line_t
{
	uint32_t addr;
	uint8_t data[256];	// max possible
	uint8_t len;
	uint8_t type;
	uint8_t checksum;
};

void ihex_print_line(struct ihex_line_t *ihex_lines);

void ihex_print_lines(struct ihex_line_t *ihex_lines, int lines);

int ihex_min_addr(struct ihex_line_t *ihex_lines, int lines);

int ihex_max_addr(struct ihex_line_t *ihex_lines, int lines);

struct ihex_map;

int ihex_map_read(struct ihex_map *map, uint32_t addr, void *dst, uint32_t len);

int ihex_map_write(struct ihex_map *map, uint32_t addr, void *src, uint32_t len);

int ihex_map_crc(struct ihex_map *map, uint32_t addr, uint32_t len, uint32_t *crc);

void ihex_free_map(struct ihex_map *map);

int ihex_load(char *filename, struct ihex_line_t **ihex_lines, struct ihex_map **map);

#endif

