#include <unistd.h>    /* for getopt */
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "bl.h"
#include "bcoms.h"
#include "tlsx_bootloader_ihex.h"
#include <b2s_wrapper.h>

#define BOOTLOADER_PID 2


static bool verbose=false;


void usage(void)
{
	printf("tlsx_bootloader [OPTION] <HEXFILE>\n");
	printf("\t-v\t\tverbose\n");
	printf("\t-b\t\tblade id\n");
	printf("\t-k\t\tkey to unlock the firmware (by default use key in hex file)\n");
	printf("\t-h\t\thelp\n");
}


int kinit(int blade)
{
	// init the bootloader and process
	printf(__FILE__": " "%s[%d]: " "bootloader init\n", __func__, __LINE__);
	bcoms_init();
	bcoms_set_blade(blade);
	bcoms_flush();
	printf(__FILE__": " "%s[%d]: " "kinit\n", __func__, __LINE__);
	if (bl_init(bcoms_read, TRANSFER_BUF_SIZE, bcoms_write, TRANSFER_BUF_SIZE) < 0)
	{
		printf("err: unable to init bootloader interface\n");
		return -1;
	}
	if (verbose)
	{
		bl_dump_version();
		bl_dump_syms();
	}

	return 0;
}


void kdeinit(void)
{
	bl_deinit();
	bcoms_deinit();
}


int main(int argc, char *argv[])
{
	int opt, blade, r;
	bool debugging = false;
	bool fw_dbg = false;
	bool force = false;
	uint32_t key = 0, key_from_hexfile = true;
	char *end;
	uint32_t erase_addr, erase_len=0;

	// 
	// process command line options
	//
	
	while ((opt = getopt(argc, argv, "hvfgdb:k:e:")) != -1)
	{
		switch (opt)
		{
			case 'b':
				blade = atoi(optarg);
				break;

			case 'k':
				key = strtoul(optarg, &end, 0);
				if (end == optarg)
					printf("unable to decipher key!\n");
				printf("using key 0x%.8x\n", key);
				key_from_hexfile = false;
				break;

			case 'v':
				verbose = true;
				break;

			case 'f':
				force = true;
				break;

			case 'g':
				fw_dbg = true;

			case 'd':
				debugging = true;
				break;

			case 'e':
				// check we have 2 args
				if (optind >= argc || *argv[optind] == '-')
				{
					printf("erase requires 2 arguments, address and size!\n");
					usage();
					return 0;
				}

				// get erase address
				erase_addr = strtoul(optarg, &end, 0);
				if (end == optarg)
				{
					printf("unable to parse erase address !\n");
					usage();
					return 0;
				}
				printf("found erase address of %.8x\n", erase_addr);

				// get erase length
				erase_len = strtoul(argv[optind], &end, 0);
				if (end == argv[optind])
				{
					printf("unable to parse erase length !\n");
					usage();
					return 0;
				}
				printf("found erase length of %d\n", erase_len);
				optind++;
				break;

			case 'h':
			default:
				usage();
				return 0;
		}
	}

	// only mandatory field is intel hex filename
	char *ihfilename;
	if (optind != argc - 1)
	{
		printf("err: last argument must contain valid hex file to bootload\n");
		usage();
		return -1;
	}
	ihfilename = argv[optind];
	if (verbose)
		printf("loading %s\n", ihfilename);


	//
	// load the ihex file
	//
	
	struct ihex_line_t *ihex_lines = NULL;
	int lines;
	struct ihex_map *map;
	uint32_t start_addr;
	uint32_t size;
	uint32_t crc, fw_crc;
	uint8_t pid;
	uint16_t hw_id;
	lines = ihex_load(ihfilename, &ihex_lines, &map);
	if (lines < 0 || map == NULL)
	{
		printf("err: unable to open intel hex file %s\n", ihfilename);
		return -1;
	}

	// get required info from the program header
	start_addr = ihex_min_addr(ihex_lines, lines);
	size = ihex_max_addr(ihex_lines, lines) - start_addr;
	if (ihex_map_read(map, start_addr + 0, &fw_crc, sizeof(fw_crc)) != sizeof(fw_crc))
	{
		printf("err: unable to read crc from hex file\n");
		return -1;
	}
	if (ihex_map_crc(map, start_addr + sizeof(crc), size - sizeof(crc), &crc) != size - sizeof(crc))
	{
		printf("err: unable to crc hexfile memory map\n");
		return -1;
	}
	if (fw_crc != crc)
	{
		printf("err: crc in %s [0x%.4x] does not match crc of %s [0x%.4x]\n", ihfilename, fw_crc, ihfilename, crc);
		return -1;
	}
	if (key_from_hexfile && ihex_map_read(map, start_addr + 18, &key, sizeof(key)) != sizeof(key))
	{
		printf("err: unable to read key from hex file\n");
		return -1;
	}
	if (ihex_map_read(map, start_addr + 13, &pid, sizeof(pid)) != sizeof(pid))
	{
		printf("err: unable to read pid from hex file\n");
		return -1;
	}
	if (ihex_map_read(map, start_addr + 22, &hw_id, sizeof(hw_id)) != sizeof(hw_id))
	{
		printf("err: unable to read hw_id from hex file\n");
		return -1;
	}
	if (verbose)
		printf("%s loaded [pid = %d, key = 0x%.4x, crc = 0x%.4x, hw_id = %d]\n", ihfilename, pid, key, crc, hw_id);


	// 
	// switch blade to bootloader
	//
	#ifdef OLD
	printf(__FILE__": " "%s[%d]: " "b2s init & switch\n", __func__, __LINE__);
	b2s_configure();
	reboot_blade(blade, BOOTLOADER_PID);
	b2s_unconfigure();
	#else
	printf(__FILE__": " "%s[%d]: " "init & switch\n", __func__, __LINE__);
	r = kinit(blade);
	if (r != 0)
		return r;
	printf(__FILE__": " "%s[%d]: " "reboot\n", __func__, __LINE__);
	if (bl_reboot(2) != true) // pid == 2 is bootloader
	{
		printf("err: unable reboot into bootloader\n");
		return -1;
	}
	kdeinit();
	#endif
	if (fw_dbg)
	{
		printf("press any key to start bootload\n");
		while (1)
		{
			char c = getchar();
			if (c == '\r' || c == '\n')
				break;
		}
	}
	else
		sleep(2);


	//
	// start bootload process
	//
	
	// init the bcom/kowhai coms layer
	r = kinit(blade);
	if (r != 0)
		return r;
	
	// check bootloader version number
	uint16_t major, minor;
	printf(__FILE__": " "%s[%d]: " "check firmware version\n", __func__, __LINE__);
	if (bl_get_firmware_version(&major, &minor) != true)
	{
		printf("err: unable to get the firmware version\n");
		return -1;
	}
	printf("firmware version = %d.%d\n", major, minor);

	// crc the firmware and check firmware and hexfile differ
	// otherwise we will achieve nothing loading the same firmware in
	///@todo do crc of bytes in ihex_lines and compare with firmware crc below
	printf(__FILE__": " "%s[%d]: " "check firmware and hex file crc's differ\n", __func__, __LINE__);
	if (bl_crc(start_addr + sizeof(fw_crc), size - sizeof(fw_crc), &fw_crc) != true)
	{
		printf("err: unable to crc the firmware\n");
		return -1;
	}
	printf("firmware crc = 0x%.4x\n", fw_crc);
	if (crc == fw_crc && !force)
	{
		printf("%s crc matches the device crc (0x%.4x == 0x%.4x), firmware already up to date\n", ihfilename, crc, fw_crc);
		goto start_new_app;
	}
	
	// unlock the firmware
	printf(__FILE__": " "%s[%d]: " "unlock\n", __func__, __LINE__);
	if (bl_unlock(pid, key, start_addr, hw_id) != true)
	{
		// we might have already erased the firmware in this case the pid = 0xff
		if (bl_unlock(pid, 0xffffffff, start_addr, hw_id) != true)
		{
			printf("err: unable to unlock the firmware\n");
			return -1;
		}
		printf("unlock not needed, already erased (warning you may be updating from incorrect of firmware image)\n");
	}
	else
		printf("firmware unlocked\n");
	
	// Check the hardware id (info only, we use the hw id from the hex file for unlocking)
	uint16_t fw_hw_id;
	printf(__FILE__": " "%s[%d]: " "check firmware hw_id\n", __func__, __LINE__);
	if (bl_get_hw_id(&fw_hw_id) != true)
	{
		printf("err: unable to get the firmware hw_id\n");
		return -1;
	}
	printf("firmware hw_id = %d\n", fw_hw_id);

	// erase region from cmd line
	if (erase_len) 
	{
		printf(__FILE__": " "%s[%d]: " "erase region(%x, %d)\n", __func__, __LINE__, erase_addr, erase_len);
		if (bl_erase(erase_addr, erase_len) != true)
		{
			printf("err: unable to erase region\n");
			return -1;
		}
		printf("region erased\n");
	}

	// erase the application space
	printf(__FILE__": " "%s[%d]: " "erase(%x, %d)\n", __func__, __LINE__, start_addr, size);
	if (bl_erase(start_addr, size) != true)
	{
		printf("err: unable to erase the firmware\n");
		return -1;
	}
	printf("firmware erased\n");

	// download the firmware
	printf(__FILE__": " "%s[%d]: " "downloading\n", __func__, __LINE__);
	printf("|");
	fflush(stdout);
	int l;
	for (l = 0; l < lines; l++)
	{
		int r;
		struct ihex_line_t *line = &ihex_lines[l];

		if (l % (lines / 70) == 0)
		{
			printf("=");
			fflush(stdout);
		}

		// write current line
		///@todo checksum
		if ((r = bl_write(line->addr, line->data, line->len)) != true)
		{
			printf("\nerr: unable to write line[%d], returned %d\n", l, r);
			return -1;
		}
		
		// verify current line
		///@todo checksum
		if ((r = bl_verify(line->addr, line->data, line->len)) != true)
		{
			printf("\nerr: unable to verify line[%d], returned %d\n", l, r);
			return -1;
		}
	}
	printf("< done.\n");

	// ensure crc on device matched crc in the file
	printf(__FILE__": " "%s[%d]: " "crc new firmware image\n", __func__, __LINE__);
	if (bl_crc(start_addr + sizeof(fw_crc), size - sizeof(fw_crc), &fw_crc) != true)
	{
		printf("err: unable to crc the firmware\n");
		return -1;
	}
	printf("firmware crc = 0x%.4x\n", fw_crc);
	if (fw_crc != crc)
	{
		printf("err: crc of firmware does not match crc of %s\n", ihfilename);
		return -1;
	}

	// start new application
start_new_app:
	printf(__FILE__": " "%s[%d]: " "reboot\n", __func__, __LINE__);
	if (bl_reboot(pid) != true)
	{
		printf("err: unable reboot into new firmware\n");
		return -1;
	}
	printf("firmware rebooting\n");
	
	// done so tidy up
	bcoms_deinit();
	bl_deinit();
	ihex_free_map(map);
	return 0;
}

