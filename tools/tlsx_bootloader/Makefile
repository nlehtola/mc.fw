ifdef TOOL_PREFIX
CC = $(TOOL_PREFIX)gcc
AR = $(TOOL_PREFIX)ar
LD = $(TOOL_PREFIX)ld
endif

ARCH ?= noncavium

CFLAGS += -march=armv4 -ffunction-sections -fdata-sections -D$(ARCH)=1
CFLAGS += -g
LDFLAGS += -Wl,-gc-sections
LDFLAGS += -Wl,-rpath,.

COMPONENTS_DIR = ../../components
LIBCOMMON_DIR = ../../common
LIBTSCTL_DIR = $(COMPONENTS_DIR)/libtsctl
LIBCRC_DIR = $(COMPONENTS_DIR)/libcrc
LIBB2S_DIR = $(COMPONENTS_DIR)/b2s
LIBKOWHAI_DIR = $(COMPONENTS_DIR)/kowhai
LIBBCOMS_DIR = $(COMPONENTS_DIR)/libbcoms
LIBBL_DIR = $(COMPONENTS_DIR)/libbl
LIBGIS_DIR = $(COMPONENTS_DIR)/libGIS

LIBCOMMON = $(LIBCOMMON_DIR)/libcommon.so
LIBTSCTL = $(LIBTSCTL_DIR)/$(ARCH)/libtsctl.so
LIBCRC = $(LIBCRC_DIR)/libcrc.so
LIBB2S = $(LIBB2S_DIR)/libb2s.so
LIBKOWHAI = $(LIBKOWHAI_DIR)/libkowhai.so
LIBBCOMS = $(LIBBCOMS_DIR)/libbcoms.so
LIBBL = $(LIBBL_DIR)/libbl.so

LIB += $(LIBCOMMON)
LIB += $(LIBTSCTL)
LIB += $(LIBCRC)
LIB += $(LIBB2S)
LIB += $(LIBKOWHAI)
LIB += $(LIBBCOMS)
LIB += $(LIBBL)

LDFLAGS += -L$(LIBCOMMON_DIR) -lcommon
LDFLAGS += -L$(LIBTSCTL_DIR)/$(ARCH) -ltsctl
LDFLAGS += -L$(LIBCRC_DIR) -lcrc
LDFLAGS += -L$(LIBB2S_DIR) -lb2s
LDFLAGS += -L$(LIBKOWHAI_DIR) -lkowhai
LDFLAGS += -L$(LIBBCOMS_DIR) -lbcoms
LDFLAGS += -L$(LIBBL_DIR) -lbl

INC += $(LIBCOMMON_DIR)/include
INC += $(LIBTSCTL_DIR)/ts
INC += $(LIBCRC_DIR)
INC += $(LIBB2S_DIR)/include
INC += $(LIBKOWHAI_DIR)/src
INC += $(LIBBCOMS_DIR)
INC += $(LIBBL_DIR)
INC += $(LIBGIS_DIR)
INCDIR = $(patsubst %,-I%,$(INC))

SRC_DIR = .
SRC += $(SRC_DIR)/tlsx_bootloader_ihex.c
SRC += $(SRC_DIR)/tlsx_bootloader.c
SRC += $(LIBGIS_DIR)/ihex.c
OBJ_DIR = ./obj
OBJ = $(patsubst %.c, $(OBJ_DIR)/%.o,$(notdir $(SRC)))

PRJ = tlsx_bootloader

.PHONY: clean all $(LIB) $(OBJ_DIR)


all: $(PRJ)
	@echo done

$(PRJ): $(LIB) $(OBJ_DIR) $(OBJ)
	$(CC) $(CFLAGS) -I. $(INCDIR) $(OBJ) $(LDFLAGS) -o $@

$(LIBCOMMON):
	make -C $(LIBCOMMON_DIR) $(notdir $(LIBCOMMON)) CC="$(CC)" CFLAGS="$(CFLAGS)" LD="$(LD)"

$(LIBCRC):
	make -C $(LIBCRC_DIR) $(notdir $(LIBCRC)) CC="$(CC)" CPFLAGS="$(CFLAGS)" LD="$(LD)" 

$(LIBTSCTL):
	make -C $(LIBTSCTL_DIR) $(notdir $(LIBTSCTL)) TOOL_PREFIX="$(TOOL_PREFIX)"

$(LIBB2S):
	make -C $(LIBB2S_DIR) $(notdir $(LIBB2S)) CC="$(CC)" CFLAGS="$(CFLAGS)" LD="$(LD)"

$(LIBKOWHAI):
	make -C $(LIBKOWHAI_DIR) $(notdir $(LIBKOWHAI)) CC=$(CC) CFLAGS="$(CFLAGS)" 

$(LIBBCOMS):
	make -C $(LIBBCOMS_DIR) $(notdir $(LIBBCOMS)) CC=$(CC) CFLAGS="$(CFLAGS)" 

$(LIBBL):
	make -C $(LIBBL_DIR) $(notdir $(LIBBL)) CC=$(CC) CFLAGS="$(CFLAGS)" 

$(OBJ_DIR):
	mkdir -p $(OBJ_DIR)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) -c $(CFLAGS) -I. $(INCDIR) $< -o $@

$(OBJ_DIR)/%.o: $(LIBGIS_DIR)/%.c
	$(CC) -c $(CFLAGS) -I. $(INCDIR) $< -o $@

clean:
	-rm -rf $(OBJ_DIR)
	-rm -rf $(OBJ)
	-rm -f $(PRJ)
	make -C $(LIBCOMMON_DIR) clean
	make -C $(LIBTSCTL_DIR) clean
	make -C $(LIBCRC_DIR) clean
	make -C $(LIBB2S_DIR) clean
	make -C $(LIBKOWHAI_DIR) clean
	make -C $(LIBBCOMS_DIR) clean
	make -C $(LIBBL_DIR) clean

