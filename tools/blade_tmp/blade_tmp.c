#include <kblade.h>
#include <stdio.h>
#include <time.h>

void pid_to_str(int pid, char *str, int len)
{
	switch (pid)
	{
		case KBLADE_PID_BOOTSTRAP:
			snprintf(str, len, "BOOTSTRAP");
			return;
		case KBLADE_PID_TLSX:
			snprintf(str, len, "TLSX");
			return;
		case KBLADE_PID_BOOTLOAER:
			snprintf(str, len, "BOOTLOADER");
			return;
		case KBLADE_PID_FVA:
			snprintf(str, len, "FVA");
			return;
		case KBLADE_PID_NOT_PRESENT:
			snprintf(str, len, "NOT PRESENT");
			return;
		default:
			snprintf(str, len, "UNKNOWN");
			return;
	}
}

int main(int argc, char **argv)
{
	int b = 1;
	uint16_t mask = (1 << b); // look for all blades

	printf("blade tmp:\n");
	kblade_init(&mask);
	printf("blade mask = 0x%.4x\n", mask);
	
	if (mask & (1 << b))
	{
		int n;

		if (kblade_commit_settings(b) > 0)
			printf("settings committed\n");

		if (kblade_set_wavelength(b, 0, 1540.0) <= 0)
		{
			printf("error setting wavelength\n");
			return;
		}

		if (kblade_set_ctrl_mode(b, 0, KBLADE_CTRL_MODE_POWER) <= 0)
		{
			printf("error setting control mode to power\n");
			return;
		}

		if (kblade_set_power(b, 0, 10.12345) <= 0)
		{
			printf("error setting power\n");
			return;
		}

		if (kblade_set_attenuation(b, 0, 40.54321) <= 0)
		{
			printf("error setting attenuation\n");
			return;
		}

		for (n = 0; n < 10; n++)
		{
			float set, actual, def, min, max;
			enum kblade_ctrl_mode_t ctrl_mode;
			kblade_get_power(b, 0, &set, &actual, &def, &min, &max);
			printf("power: set=%.5f, actual=%.5f, default=%.5f, min=%.5f, max=%.5f\n", set, actual, def, min, max);
			kblade_get_attenuation(b, 0, &set, &actual, &def, &min, &max);
			printf("attenuation: set=%.5f, actual=%.5f, default=%.5f, min=%.5f, max=%.5f\n", set, actual, def, min, max);
			kblade_get_wavelength(b, 0, &set, &def, &min, &max);
			printf("wavelength: set=%.5f, default=%.5f, min=%.5f, max=%.5f\n", set, def, min, max);
			kblade_get_ctrl_mode(b, 0, &ctrl_mode);
			switch (ctrl_mode)
			{
				case KBLADE_CTRL_MODE_ATTENUATION:
					printf("control mode is attenuation\n");
					break;
				case KBLADE_CTRL_MODE_POWER:
					printf("control mode is power\n");
					break;
				default:
					printf("control mode is unknown !!!\n");
					break;
			}
			usleep(20000); // what is it 10ms later
			printf("\n");
		}

		//-------------

		if (kblade_set_ctrl_mode(b, 0, KBLADE_CTRL_MODE_ATTENUATION) <= 0)
		{
			printf("error setting control mode to attenuation\n");
			return;
		}

		if (kblade_set_power(b, 0, -20) <= 0)
		{
			printf("error setting power\n");
			return;
		}

		if (kblade_set_attenuation(b, 0, 0) <= 0)
		{
			printf("error setting attenuation\n");
			return;
		}

		for (n = 0; n < 10; n++)
		{
			float set, actual, def, min, max;
			enum kblade_ctrl_mode_t ctrl_mode;
			kblade_get_power(b, 0, &set, &actual, &def, &min, &max);
			printf("power: set=%.5f, actual=%.5f, default=%.5f, min=%.5f, max=%.5f\n", set, actual, def, min, max);
			kblade_get_attenuation(b, 0, &set, &actual, &def, &min, &max);
			printf("attenuation: set=%.5f, actual=%.5f, default=%.5f, min=%.5f, max=%.5f\n", set, actual, def, min, max);
			kblade_get_wavelength(b, 0, &set, &def, &min, &max);
			printf("wavelength: set=%.5f, default=%.5f, min=%.5f, max=%.5f\n", set, def, min, max);
			kblade_get_ctrl_mode(b, 0, &ctrl_mode);
			switch (ctrl_mode)
			{
				case KBLADE_CTRL_MODE_ATTENUATION:
					printf("control mode is attenuation\n");
					break;
				case KBLADE_CTRL_MODE_POWER:
					printf("control mode is power\n");
					break;
				default:
					printf("control mode is unknown !!!\n");
					break;
			}
			usleep(20000); // what is it 10ms later
			printf("\n");
		}
	}

	kblade_deinit(mask);
}

