#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>     

#include "b2s.h"
#include <bcoms.h>
#include <bl.h>

#define MAX_NUM_BLADES 16
#define MAX_NUM_LASERS 4

void scan_bus();
void print_file();
void print_screen();


int verbose;
int check_for_bootloader = 0;
int write_file = 0;

 struct bade_info_t
  {
	  uint8_t mode;
	  uint32_t fw_ver_major;
	  uint32_t fw_ver_minor;
	  uint32_t hw_id; //in bootloader mode
	  uint8_t crc_status; //0=off, 1=on, 2=unknown
	  uint8_t model[32];//this is read from the ROM
	  uint8_t hw_version[32]; //this is read from the ROM
	  uint8_t laser_present[5];
  } blade_info[MAX_NUM_BLADES];

int main(int argc , char *argv[])
{




  uint option;
  uint8_t blade_id; 
  uint8_t laser_id;
  
  while ((option = getopt (argc, argv, "fhvc")) != -1)
    switch (option)
    {
    case 'f':
      write_file = 1;	     
      break;
    case 'v':
      verbose = 1;
      break; 
    case 'c':
      check_for_bootloader = 1;
      break;
    case 'h':
      printf("Usage:\n");
      printf("tlsx_ls [-v|h|c|f] \n");
      printf("v: increase verbosity.\n");
      printf("h: print help.\n");
      printf("c: check for boot mode when printing to screen.");
      printf("f: print the result to /root/releases/BLADE_VERSIONS\n");
      return 0;
      break;

    case '?':
      if (optopt == 'p')
        fprintf (stderr, "Option -%p requires an argument.\n", optopt);
      else
        fprintf (stderr,
        "Unknown option character `\\x%x'.\n",
        optopt);
      return 1;
    default:
      return 0;
    }

  scan_bus();
  
  if(write_file){
	  print_file();
	  return 0;
  }
  print_screen();

}//main function


#define NOT_PRESENT 0
#define PRESENT 1
#define BOOTLOADER 2
void scan_bus(){

  int i;
  int j;
   
  // init
  memset(blade_info, 0, sizeof(blade_info[0]) * MAX_NUM_BLADES);
  
  // look for blades in normal b2s mode (most likely state)
  b2s_configure();
  for(i=0;i<MAX_NUM_BLADES;i++){
	  // check for blade in normal mode, ie responds to b2s commands
	  find_blade(i, &blade_info[i].mode); //mode = 1 implies present 
	  if(1==blade_info[i].mode){
		  // we are in b2s mode so check what lasers are present
		  for(j=1;j<5;j++){
			  get_blade_fw_rev(i,&(blade_info[i].fw_ver_major), &(blade_info[i].fw_ver_minor));
			  find_laser(i,j,blade_info[i].fw_ver_major,&blade_info[i].laser_present[j]);
		  }//for
		  //Get model number and hw version
		  get_rom_parameter(i, GET_MODEL_NUMBER, blade_info[i].model);
		  get_rom_parameter(i, GET_HW_VERSION, blade_info[i].hw_version);
		  get_crc_status(i, &blade_info[i].crc_status);
		  continue;
	  }//if
	  blade_info[i].crc_status = CRC_STATUS_UNKNOWN;
  }//for each blade
  b2s_unconfigure();
  
  if (check_for_bootloader|write_file){
	  // check for blade stuck in a bootloader mode (needs to finish updating)
	  bcoms_init();
	  for(i=0;i<MAX_NUM_BLADES;i++){
		  // skip blade found above
		  if (blade_info[i].mode == PRESENT)
			  continue;
		  
		  // look for blades by setting this blade to be addressed, flushing the bcoms
		  // module so the coms are clean and calling bl_init which will try to download
		  // the kowahi interface.
		  bcoms_set_blade(i);
		  bcoms_flush();
		  if (bl_init(bcoms_read, TRANSFER_BUF_SIZE, bcoms_write, TRANSFER_BUF_SIZE) >= 0){
			  // if we passed init there is something that talks kowhai present lets ask for
			  // bootloader specifics to ensure it really is a bootloader
			  uint16_t major, minor, hw_id;
			  if (bl_get_firmware_version(&major, &minor) && bl_get_hw_id(&hw_id)){
				  blade_info[i].fw_ver_major = major;
				  blade_info[i].fw_ver_minor = minor;
				  blade_info[i].hw_id = hw_id;
				  blade_info[i].mode = BOOTLOADER;
			  }
		  }
		  bl_deinit();
	  }
	  bcoms_deinit();
  }
  
  
}
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/

void print_file(){
	
	int i;
	int j;
	FILE* blade_versions; 
	blade_versions = fopen("/root/releases/BLADE_VERSIONS", "w");

	if(NULL==blade_versions){
	  printf("Couldn't open blade_versions file.\n");
	  return;
	}
	
	for(i=0;i<MAX_NUM_BLADES;i++){
		fprintf(blade_versions, "%u,", i);
		if(PRESENT==blade_info[i].mode){
			for(j=0;j<32;j++)
				if(isprint(blade_info[i].model[j]))		  	   
					fprintf(blade_versions, "%c", blade_info[i].model[j]);
			fprintf(blade_versions, ",");
			for(j=0;j<32;j++)
				if(isprint(blade_info[i].hw_version[j]))		   
					fprintf(blade_versions, "%c", blade_info[i].hw_version[j]);
			fprintf(blade_versions, ",");
			fprintf(blade_versions, "%u.%u,",blade_info[i].fw_ver_major,blade_info[i].fw_ver_minor); 			
		}
		else if (BOOTLOADER==blade_info[i].mode)
			fprintf(blade_versions,"0.0,"); //This is indicated to the SW via FW version = 0.0
		else
			fprintf(blade_versions, ",,");
		if (CRC_STATUS_ON==blade_info[i].crc_status)
			fprintf(blade_versions, "on\n");
		else if (CRC_STATUS_OFF==blade_info[i].crc_status)
			fprintf(blade_versions, "off\n");
		else
			fprintf(blade_versions, "unknown\n");

	}//for loop over all blades. 
	
	fclose(blade_versions);
	return;	
	
	
}


/***********************************************************************************/




/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * Prints the output on std out. 
 * 
 **/


void print_screen(){
	
	int i,j;

	for(i=0;i<MAX_NUM_BLADES;i++){
		if (blade_info[i].mode == PRESENT) {
			// show blades found in normal b2s mode
			printf("Slot %u: Blade present. ", i); 
			for(j=1;j<5;j++)
				if(blade_info[i].laser_present[j] & 0x10)
					printf("---[0x%.2x]---- ", blade_info[i].laser_present[j]);
				else
					printf("Laser:%u[0x%.2x] ", j, blade_info[i].laser_present[j]);
		}
		else if (blade_info[i].mode == BOOTLOADER)
			// show blades found in bootloader mode
			printf("Slot %u: bootloader present, version = %u.%u ", i, 
			       blade_info[i].fw_ver_major, blade_info[i].fw_ver_minor, blade_info[i].hw_id);
		else
			// show empty slots
			printf("Slot %u: -------------", i);
		printf("\n");
	}
	
	
}
