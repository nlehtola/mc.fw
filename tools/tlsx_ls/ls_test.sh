#!/bin/sh

# Execute getopt
ARGS=$(getopt -o b:q -l "blades:" -l "qof" -n "ls_test" -- "$@");

#Bad arguments
if [ $? -ne 0 ];
then
	echo "usage ls_test -b|--blades <number of blades> [OPTIONS]"
	echo "	OPTIONS: -q, --qof: quit on [first] fail"
	echo "	NOTE: number of blades field is mandatory"
	exit 1
fi
eval set -- "$ARGS";
while true; do
  case "$1" in
    -q|--qof)
      shift;
      if [ -n "$1" ]; then
	  	qof="1"
      fi
      ;;
    -b|--blades)
      shift;
      if [ -n "$1" ]; then
        blades=$1
        shift;
      fi
      ;;
    --)
      shift;
      break;
      ;;
  esac
done

if [ -z "${blades}" ]; then
	echo "error: number of blades is mandatory"
	exit 1
fi

# settings
i=0
f=0
LOG="ls_test_log"
PATTERN="Slot \([0-9]\|1[0-9]\): Blade present"
EXE="./tlsx_ls -v"

# clean up old log
mv ${LOG} ${LOG}.old

# main loop
while [ 1 ]
do
	echo -n test $i ": "
	echo -n test $i ": " >> ${LOG}
	i=$(($i+1))
	${EXE} >> ${LOG}
	c=$(tail -n 15 ${LOG} | grep -c "${PATTERN}")
	if [ $c != $blades ]; then
		f=$(($f+1))
		echo "failed (fail rate = $f / $i) ... found $c blades, looking for $blades." 
		echo "failed (fail rate = $f / $i) ... found $c blades, looking for $blades." >> ${LOG}
		if [ ! -z $qof ]; then
			break
		fi
	else
		echo "passed (fail rate = $f / $i)"
		echo "passed (fail rate = $f / $i)" >> ${LOG}
	fi
done

