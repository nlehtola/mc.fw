#!/bin/bash

export SUBNETS=`ip -f inet addr | grep -oE "[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/[0-9]+"`
for SUBNET in $SUBNETS
do
	if [ "$SUBNET" == "169.254.6.191/16" ]; then
		continue
	fi
	export SUBNET=`echo $SUBNET | grep -oE "[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+"`/24
	echo Searching subnet: $SUBNET
	export IPS=`nmap -p 111 $SUBNET | grep -oE "[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+"`
	for IP in $IPS
	do
		export R=`rpcinfo -t $IP 395183 1 2>/dev/null | grep "ready and waiting"`
		if [ -n "$R" ]; then
			echo $IP
		fi
	done
done
