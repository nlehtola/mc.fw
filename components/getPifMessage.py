#!/usr/bin/python

import vxi11
from time import sleep

def getMessage():
	idn = None
	for tries in range(5):
		try:
			instr = vxi11.Instrument("127.0.0.1")
			idn = instr.ask("*IDN?")
			break
		except Exception:
			pass
		sleep(3)
	if not idn:
		return "Timeout"

	[company, model, serial, version] = idn.split(',')
	message = idn
	if "IQS-636" in model:
		slotMask = instr.ask("*OPT?")
		message += ";" + slotMask
		i = 1
		for slot in slotMask.split(','):
			message += ";"
			if slot!="":
				channelMask = instr.ask("SLOT%s:OPT?" % (str(i)))
				message += channelMask
			i += 1
	elif ("FVA-3800" in model) or ("FLS-2800" in model):
		slotMask = instr.ask("*OPT?")
		message += ";" + slotMask
		if slotMask!="ERR":
			channelMask = instr.ask("SLOT:OPT?")
			message += ";" + channelMask
	else:
		return "Development"
	return message

try:
	message = getMessage()
except Exception:
	message = ""

print message
