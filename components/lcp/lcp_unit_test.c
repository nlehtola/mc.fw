#include <lcp.h>
#include <stdio.h>

int main(int argc, char **argv)
{
	bool l0_stat = 0;
	bool l1_stat = 0;

	lcp_init();

	// just sit in a loop setting the status to match the button
	while (1)
	{
		bool l0_enabled = lcp_get_enabled(0);
		bool l1_enabled = lcp_get_enabled(1);
		printf("l0_enabled = %d, l1_enabled = %d\n", l0_enabled, l1_enabled);

		lcp_set_status(0, l0_stat);
		lcp_set_status(1, l1_stat);
		l0_stat = !l0_stat;
		l1_stat = !l1_stat;

		usleep(500000);
	}
}
