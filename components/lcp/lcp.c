//
// laser control panel module
//
//

#include <libtsctl.h>
#include <assert.h>
#include <stdbool.h>


static DIO *dio;
static Pin *pin;

#define L0_STATUS (14)
#define L1_STATUS (13)
#define L0_ENABLE (0)
#define L1_ENABLE (12)

static lock_io(int laser)
{
	switch (laser)
	{
		case 0:
			dio->Lock(dio, L0_ENABLE, 0);
			dio->Lock(dio, L0_STATUS, 0);
			break;
		case 1:
			dio->Lock(dio, L1_ENABLE, 0);
			dio->Lock(dio, L1_STATUS, 0);
			break;
		default:
			return;
	}
}


static unlock_io(int laser)
{
	switch (laser)
	{
		case 0:
			dio->Unlock(dio, L0_ENABLE, 0);
			dio->Unlock(dio, L0_STATUS, 0);
			break;
		case 1:
			dio->Unlock(dio, L1_ENABLE, 0);
			dio->Unlock(dio, L1_STATUS, 0);
			break;
		default:
			return;
	}
}


bool lcp_get_enabled(int laser)
{
	DIOState result; 

	switch(laser)
	{
		case 0:
			lock_io(laser);
			result = dio->GetAsync(dio, L0_ENABLE);
			unlock_io(laser);
			return (result == INPUT_HIGH || result == HIGH);
		case 1:
			lock_io(laser);
			result = dio->GetAsync(dio, L1_ENABLE);
			unlock_io(laser);
			return (result == INPUT_HIGH || result == HIGH);
		default:
			return false;
	}
}


void lcp_set_status(int laser, bool enabled)
{
	switch(laser)
	{
		case 0:
			lock_io(laser);
			dio->SetAsync(dio, L0_STATUS, enabled ? HIGH: LOW);
			unlock_io(laser);
			return;
		case 1:
			lock_io(laser);
			dio->SetAsync(dio, L1_STATUS, enabled ? HIGH: LOW);
			unlock_io(laser);
			return;
		default:
			return;
	}
}


void lcp_init(void)
{
	// init libtsctl
	dio = DIOInit(0);
	assert(dio);
	pin = PinInit(0);
	assert(pin);

	// init the lcp pins
	lock_io(0);
	pin->ModeSet(pin, L0_ENABLE, MODE_DIO);
	pin->ModeSet(pin, L0_STATUS, MODE_DIO);
	unlock_io(0);
	lock_io(1);
	pin->ModeSet(pin, L1_ENABLE, MODE_DIO);
	pin->ModeSet(pin, L1_STATUS, MODE_DIO);
	unlock_io(1);
}

