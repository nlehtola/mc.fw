#ifndef __LCP_H__
#define __LCP_H__

#include <stdbool.h>

bool lcp_get_enabled(int laser);
void lcp_set_status(int laser, bool enabled);
void lcp_init(void);


#endif
