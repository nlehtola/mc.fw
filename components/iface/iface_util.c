#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "iface.h"

#define min(a,b) ((a < b)? a: b)


void usage(void)
{
	printf("Usage:\n");
	printf("\tiface_util [options]\n"); 
	printf("\nOptions:\n");
	printf("\t-i\t\tinterface to work on (defaults to eth0)\n");
	printf("\t-f\t\tinterfaces file (defaults to /etc/network/interfaces)\n");
	printf("\t-m\t\tupdate interface to have given method\n");
	printf("\t-a\t\tupdate interface to have given address\n");
	printf("\t-n\t\tupdate interface to have given netmask\n");
	printf("\t-v\t\tverbose\n");
	printf("\t-h\t\thelp\n");
}


int main(int argc, char **argv)
{
	struct iface_t iface;
	int opt;
	int verbose = 0;
	char *ifacesfile = NULL;
	char *method = NULL;
	char *addr = NULL;
	char *netmask = NULL;
	
	// process command line options
	snprintf(iface.iface_name, IFACE_STR_LEN, "eth0");
	while ((opt = getopt(argc, argv, "i:f:n:a:m:vh")) != -1)
	{
		switch (opt)
		{
			case 'i':
				snprintf(iface.iface_name, IFACE_STR_LEN, "%s", optarg);
				break;
			case 'f':
				ifacesfile = optarg;
				break;
			case 'm':
				method = optarg;
				break;
			case 'a':
				addr = optarg;
				break;
			case 'n':
				netmask = optarg;
			case 'v':
				verbose = 1;
				break;
			case 'h':
			default:
				usage();
				return 0;
		}
	}

	if (iface_get_cfg(&iface, ifacesfile) == NULL)
	{
		printf("error parsing interface (check syslog for more info)\n");
		return 1;
	}

	if (method == NULL && addr == NULL && netmask == NULL)
	{
		iface_print(&iface);
	}
	else
	{
		if (method)
		{
			if (strncmp(method, "loopback", METHOD_STR_LEN) == 0)
				iface.method = IFACE_METHOD_LOOPBACK;
			else if (strncmp(method, "static", METHOD_STR_LEN) == 0)
				iface.method = IFACE_METHOD_STATIC;
			else if (strncmp(method, "dhcp", METHOD_STR_LEN) == 0)
				iface.method = IFACE_METHOD_DHCP;
			else
				iface.method = IFACE_METHOD_UNKNOWN;
		}

		if (addr)
			sscanf(addr, "%d.%d.%d.%d", &iface.addr[0], &iface.addr[1], &iface.addr[2], &iface.addr[3]);
		
		if (netmask)
			sscanf(netmask, "%d.%d.%d.%d", &iface.netmask[0], &iface.netmask[1], &iface.netmask[2], &iface.netmask[3]);

		snprintf(iface.addr_family, ADDR_FAMILY_LEN, "inet");

		iface_set_cfg(&iface, ifacesfile);
	}

	return 0;
}

