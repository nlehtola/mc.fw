#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <netdb.h>
#include <regex.h>
#include <syslog.h>

#include "iface.h"

#define IFACE_UNKNOWN "UNKNOWN"
#define min(a,b) ((a < b)? a: b)


/**
 * @brief strip comment, join lines ending in \ etc
 * @param s string to process
 * @note we only strip chars so the s buffer will only shrink
 * @todo support source command 
 */
static char * preprocess_interfaces_file(char *s)
{
	regex_t re;
	int r;
	regmatch_t match;

	// strip whole lines starting with # or join lines ending in "\"
	// (note join is just strip the "\" and line ending)
	if (regcomp(&re, "(^#.*?$)|(\\\\s*?$)", REG_EXTENDED | REG_NEWLINE) != 0)
		return NULL;
	while ((r = regexec(&re, s, 1, &match, 0)) == 0)
		// copy remaining buffer after the match over the match
		memmove(&s[match.rm_so], &s[match.rm_eo + 1], strlen(&s[match.rm_eo + 1]) + 1);

	// did we exit cleanly
	if (r != REG_NOMATCH)
	{
		syslog(LOG_ERR, "regexec failed with error code %d\n", r);
		return NULL;
	}
	regfree(&re);

	return s;
}


/**
 * @brief open and read the contents of the /etc/network/interfaces file
 * @return string with the contents of the file or NULL on error
 * @note caller is responsible for free'ing the returned string
 */
#define INTERFACES_FILE "/etc/network/interfaces"
static char * read_interfaces_file(char **s, int *len, char *filename)
{
	FILE *fd = NULL;
	int l, _len;
	char *_s = NULL;

	// open file
	fd = fopen((filename)?filename:INTERFACES_FILE, "r");
	if (fd == NULL)
		goto error;

	// allocate a buffer for the whole file
	fseek(fd, 0, SEEK_END);
	_len = ftell(fd);
	fseek(fd, 0, SEEK_SET);
	_s = malloc(_len + 1);
	if (_s == NULL)
		goto error;

	// read the whole file
	if ((l = fread(_s, 1, _len, fd)) != _len)
		goto error;
	_s[_len] = 0x00; // ensure we a NULL terminated (most of the searching depends on that)

	// pre-process the file to make it more machine readable
	if (preprocess_interfaces_file(_s) == NULL)
		goto error;
	
	goto done;

error:
	if (_s != NULL)
		free(_s);
	_s = NULL;
	syslog(LOG_ERR, "error reading interfaces file\n");
done:
	if (fd != NULL)
		fclose(fd);
	fd = NULL;
	if (s != NULL)
		*s = _s;
	if (len != NULL)
		*len = _len;

	return _s;
}


static void parse_iface_stanza(char *stanza, struct iface_t *iface)
{
	regex_t re;
	regmatch_t match[5];
	int r;
	char method_str[METHOD_STR_LEN];

	// pull out the address family and method
	if (regcomp(&re, "iface\\s+(\\w+)\\s+(\\w+)\\s+(\\w+)", REG_EXTENDED | REG_NEWLINE) != 0)
		goto error;
	if ((r = regexec(&re, stanza, 4, match, 0)) == 0)
	{
		snprintf(iface->iface_name, IFACE_STR_LEN, "%.*s", match[1].rm_eo - match[1].rm_so, &stanza[match[1].rm_so]);
		snprintf(iface->addr_family, ADDR_FAMILY_LEN , "%.*s", match[2].rm_eo - match[2].rm_so, &stanza[match[2].rm_so]);
		snprintf(method_str, METHOD_STR_LEN, "%.*s", match[3].rm_eo - match[3].rm_so, &stanza[match[3].rm_so]);
		if (strcmp(method_str, "static") == 0)
			iface->method = IFACE_METHOD_STATIC;
		else if (strcmp(method_str, "dhcp") == 0)
			iface->method = IFACE_METHOD_DHCP;
		else if (strcmp(method_str, "loopback") == 0)
			iface->method = IFACE_METHOD_LOOPBACK;
		else
			iface->method = IFACE_METHOD_UNKNOWN;
	}
	else
		goto error;
	regfree(&re);

	// pull out address
	if (regcomp(&re, "address\\s+([0-9]+)\\.([0-9]+)\\.([0-9]+)\\.([0-9]+)", REG_EXTENDED | REG_NEWLINE) != 0)
		goto error;
	if ((r = regexec(&re, stanza, 5, match, 0)) == 0)
	{
		iface->addr[0] = atoi(&stanza[match[1].rm_so]);
		iface->addr[1] = atoi(&stanza[match[2].rm_so]);
		iface->addr[2] = atoi(&stanza[match[3].rm_so]);
		iface->addr[3] = atoi(&stanza[match[4].rm_so]);
	}
	else if (r == REG_NOMATCH) 
		// some iface definitions don't have an address eg lo on loopback or dhcp
		// for this the address 0.0.0.0 is used
		memset(iface->addr, 0, sizeof(int) * 4);
	else
		goto error;
	regfree(&re);
	
	// pull out netmask
	if (regcomp(&re, "netmask\\s+([0-9]+)\\.([0-9]+)\\.([0-9]+)\\.([0-9]+)", REG_EXTENDED | REG_NEWLINE) != 0)
		goto error;
	if ((r = regexec(&re, stanza, 5, match, 0)) == 0)
	{
		iface->netmask[0] = atoi(&stanza[match[1].rm_so]);
		iface->netmask[1] = atoi(&stanza[match[2].rm_so]);
		iface->netmask[2] = atoi(&stanza[match[3].rm_so]);
		iface->netmask[3] = atoi(&stanza[match[4].rm_so]);
	}
	else if (r == REG_NOMATCH) 
		// some iface definitions don't have a netmask eg lo on loopback or dhcp
		// for this the netmask 0.0.0.0 is used
		memset(iface->netmask, 0, sizeof(int) * 4);
	else
		goto error;
	regfree(&re);
	return;

error:
	regfree(&re);
}


/**
 * @brief find the stanza for iface and return the start and end positions
 * @param src, string to search for the stanza in
 * @param iface interface name of the stanza to find
 */
static void find_iface_stanza(char *src, char *iface, int *start, int *end)
{
	char pattern[1024];
	regex_t re;
	regmatch_t match[3];
	int r;
	int offset = 0;
	enum
	{
		STATE_SEARCHING,
		STATE_FOUND_START,
		STATE_FOUND_NEXT,
	} state = STATE_SEARCHING;

	// init to not found
	if (start != NULL)
		*start = -1;
	if (end != NULL)
		*end = strlen(src);
	
	// look through all stanza's (this assumes our file has only stanza's 
	// left in it, if there are other sections (eg source which we have
	// not stripped yet) then those extra lines might get caught up in a 
	// stanza
	snprintf(pattern, sizeof(pattern), "\\(iface\\s\\+%s.*$\\)\\|\\(^\\(iface\\|auto\\|mapping\\|allow-\\w*\\).*$\\)", iface);
	if (regcomp(&re, pattern, REG_NEWLINE) != 0)
		return;
	while ((r = regexec(&re, &src[offset], 3, match, 0)) == 0)
	{
		switch (state)
		{
			case STATE_SEARCHING:
				if (match[1].rm_so > -1)
				{
					state = STATE_FOUND_START;
					if (start != NULL)
						*start = offset + match[0].rm_so;
				}
				break;
			case STATE_FOUND_START:
				state = STATE_FOUND_NEXT;
				if (end != NULL)
					*end = offset + match[0].rm_so - 1;
				goto done;
			default:
				// we should not get here
				goto done;
		}
		offset += match[0].rm_eo + 1;
	}
	if (r != REG_NOMATCH)
		syslog(LOG_ERR, "stanza regexec failed with error code %d\n", r);

done:
	regfree(&re);
}


/**
 * @brief convert the method enum in iface to a string representation
 * @param s if not null s is the string to populate, if null a static string is returned
 * @param len size of s passed (do not write past this number of bytes into s)
 * @note using s = NULL is very convenient for printf etc, however if this function is called again
 * the value of the result will have changed, hence this is also not thread safe). Hence the user can
 * also allocate s, then these problems are mitigated
 */
char *iface_method_to_str(struct iface_t *iface, char *s, int len)
{
	static char __s[16];
	char *_s = s? s: __s;
	len = s? len: sizeof(__s);

	switch (iface->method)
	{
		case IFACE_METHOD_STATIC:
			snprintf(_s, len, "static");
			break;
		case IFACE_METHOD_DHCP:
			snprintf(_s, len, "dhcp");
			break;
		case IFACE_METHOD_LOOPBACK:
			snprintf(_s, len, "loopback");
			break;
		case IFACE_METHOD_UNKNOWN:
		default:
			snprintf(_s, len, "unknown");
			break;
	}
	return _s;
}


/**
 * @brief convert the addr (address) in iface to a string
 * @param s if not null s is the string to populate, if null a static string is returned
 * @param len size of s passed (do not write past this number of bytes into s)
 * @note using s = NULL is very convenient for printf etc, however if this function is called again
 * the value of the result will have changed, hence this is also not thread safe). Hence the user can
 * also allocate s, then these problems are mitigated
 */
char *iface_addr_to_str(struct iface_t *iface, char *s, int len)
{
	static char __s[32];
	char *_s = s? s: __s;
	len = s? len: sizeof(__s);

	snprintf(_s, len, "%.3d.%.3d.%.3d.%.3d", iface->addr[0], iface->addr[1], iface->addr[2], iface->addr[3]);
	return _s;
}


/**
 * @brief convert the netmask in iface to a string
 * @param s if not null s is the string to populate, if null a static string is returned
 * @param len size of s passed (do not write past this number of bytes into s)
 * @note using s = NULL is very convenient for printf etc, however if this function is called again
 * the value of the result will have changed, hence this is also not thread safe). Hence the user can
 * also allocate s, then these problems are mitigated
 */
char *iface_netmask_to_str(struct iface_t *iface, char *s, int len)
{
	static char __s[32];
	char *_s = s? s: __s;
	len = s? len: sizeof(__s);

	snprintf(_s, len, "%.3d.%.3d.%.3d.%.3d", iface->netmask[0], iface->netmask[1], iface->netmask[2], iface->netmask[3]);
	return _s;
}


/**
 * @brief dump iface contents to stdout
 * @param iface to dump to screen
 */
void iface_print(struct iface_t *iface)
{
	char method[METHOD_STR_LEN];
	char addr[ADDR_STR_LEN];
	char netmask[NETMASK_STR_LEN];

	printf("iface: %s\n", iface->iface_name);
	printf("\taddress family: %s\n", iface->addr_family);
	printf("\tmethod: \"%s\" [%d]\n", iface_method_to_str(iface, method, sizeof(method)), iface->method);
	printf("\taddress: \"%s\" [%d.%d.%d.%d]\n", iface_addr_to_str(iface, addr, sizeof(addr)), iface->addr[0], iface->addr[1], iface->addr[2], iface->addr[3]);
	printf("\tnetmask: \"%s\" [%d.%d.%d.%d]\n", iface_netmask_to_str(iface, netmask, sizeof(netmask)), iface->netmask[0], iface->netmask[1], iface->netmask[2], iface->netmask[3]);
}


/**
 * @brief parse the /etc/network/interfaces file and pull the config params 
 * @param dst initially dst->iface_name contains the name of the interface to lookup,
 * the rest of the structure will be populated by this call
 * @return dst is returned populated with iface info
 */
struct iface_t * iface_get_cfg(struct iface_t *dst, char *filename)
{
	char *s, *stanza;
	int len;
	int start, end;

	// init all to unknown
	memcpy(dst->addr_family, IFACE_UNKNOWN, min(ADDR_FAMILY_LEN, sizeof(IFACE_UNKNOWN)));
	dst->method = IFACE_METHOD_UNKNOWN;
	memset(dst->addr, 0, sizeof(dst->addr));
	memset(dst->netmask, 0, sizeof(dst->netmask));

	// open interfaces file, read the lines and format according to man pages
	s = read_interfaces_file(NULL, &len, filename);
	if (s == NULL)
		return dst;
	
	// find the stanza for iface_name
	find_iface_stanza(s, dst->iface_name, &start, &end);
	if (start < 0)
		return dst;
	stanza = malloc(end - start + 2);
	if (stanza == NULL)
		return dst;
	memcpy(stanza, &s[start], end - start + 1);
	stanza[end - start + 1] = 0; // force section to be null terminated

	// parse the iface stanza into dst
	parse_iface_stanza(stanza, dst);

	// cleanup
	free(s);
	return dst;
}


/**
 * @brief ask the operating system for the config (note the OS does not know the method so, method = IFACE_UNKNOWN)
 * @param dst initially dst->iface_name contains the name of the interface to lookup,
 * the rest of the structure will be populated by this call
 * @return dst is returned populated with iface info
 */
struct iface_t * iface_get_active_cfg(struct iface_t *dst)
{
	struct ifaddrs *ifaddr, *ifa;

	// init all to unknown
	memcpy(dst->addr_family, IFACE_UNKNOWN, min(ADDR_FAMILY_LEN, sizeof(IFACE_UNKNOWN)));
	dst->method = IFACE_METHOD_UNKNOWN;
	memset(dst->addr, 0, sizeof(dst->addr));
	memset(dst->netmask, 0, sizeof(dst->netmask));

	// ask the os for a list of all the interfaces
    if (getifaddrs(&ifaddr) == -1) 
		return dst;

	// look through each interface for one matching ours
    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) 
    {
        if (ifa->ifa_addr == NULL)
            continue;  

        if(strncmp(ifa->ifa_name, dst->iface_name, IFACE_STR_LEN) == 0 && ifa->ifa_addr->sa_family == AF_INET)
        {
			char host_addr[NI_MAXHOST], host_netmask[NI_MAXHOST];
			int addr[4];
			int s;

			s = getnameinfo(ifa->ifa_addr, sizeof(struct sockaddr_in), host_addr, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
			if (s != 0)
				continue;

			s = getnameinfo(ifa->ifa_netmask, sizeof(struct sockaddr_in), host_netmask, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
			if (s != 0)
				continue;

			// found !, pull out the address and netmask and convert them to int array and a fixed length string
			///@todo copy name across
			if (sscanf(host_addr, "%d.%d.%d.%d", &dst->addr[0], &dst->addr[1], &dst->addr[2], &dst->addr[3]) != 4)
				memset(dst->addr, 0, sizeof(dst->addr));
			if (sscanf(host_netmask, "%d.%d.%d.%d", &dst->netmask[0], &dst->netmask[1], &dst->netmask[2], &dst->netmask[3]) != 4)
				memset(dst->netmask, 0, sizeof(dst->netmask));
			break;
        }
    }

    freeifaddrs(ifaddr);
	return dst;
}


/**
 * @brief given iface make a string with a iface stanza for it
 * @param iface details for the stanza
 * @return string version of iface
 */
static char * make_iface_stanza_str(struct iface_t *iface)
{
	char *s;

	// alloc s (but how much)
	//	the largest stanza string we support would be less than:
	//		iface <iface->name_str> inet6 samplepoint
	// 
	//		iface <logic name> <longest address type> <longest method>
	//			address xxx.xxx.xxx.xxx
	//			netmask xxx.xxx.xxx.xxx
	//
	//	where
	//		<longest address type> = inet6 [5] (we don't support inet6 yet, but its longest so no harm)
	//		<longest method> = samplepoint [11] (from can which we don't support either)
	//	hence
	//		max_size =
	//			strlen("iface  inet6 samplepoint\n") + \   /* 25 */
	//			strlen("\taddress xxx.xxx.xxx.xxx\n") + \  /* 25 */
	//			strlen("\tnetmask xxx.xxx.xxx.xxx\n") + \  /* 24 */
	//			strlen(iface->iface_name) + 1 = 75 + strlen(iface->iface_name)
	// ok lets just be generous for development etc 1K should be heaps for iface_name
	s = malloc(1024);
	if (!s)
		return NULL;

	if (strncmp(iface->addr_family, "inet", ADDR_FAMILY_LEN) == 0)
	{
		switch (iface->method)
		{
			case IFACE_METHOD_STATIC:
				snprintf(s, 1024, "iface %s inet static\n\taddress %d.%d.%d.%d\n\tnetmask %d.%d.%d.%d\n\n", \
					iface->iface_name, \
					iface->addr[0], iface->addr[1], iface->addr[2], iface->addr[3], \
					iface->netmask[0], iface->netmask[1], iface->netmask[2], iface->netmask[3]);
				break;
			case IFACE_METHOD_DHCP:
				snprintf(s, 1024, "iface %s inet dhcp\n\n", iface->iface_name);
				break;
			case IFACE_METHOD_LOOPBACK:
				snprintf(s, 1024, "iface %s inet loopback\n\n", iface->iface_name);
				break;
			default:
				syslog(LOG_ERR, "unknown iface method, cannot make iface stanza string\n");
				return NULL;
		}
	}
	else
	{
		syslog(LOG_ERR, "unknown address family, cannot make iface stanza string\n");
		return NULL;
	}

	return s;
}


/**
 * @brief update/add iface stanza in the interfaces file with data from iface
 * @param iface this contains the info to add/update to the interfaces file
 */
int iface_set_cfg(struct iface_t *iface, char *filename)
{
	char *s ,*stanza, *ns;
	int len;
	int start, end;
	int N;
	FILE *fd = NULL;

	// open interfaces file, read the lines and format according to man pages
	s = read_interfaces_file(NULL, &len, filename);
	if (!s)
		return -1;
	
	// find the stanza for iface
	find_iface_stanza(s, iface->iface_name, &start, &end);

	// convert the iface into a iface_stanza string
	stanza = make_iface_stanza_str(iface);
	if (!stanza)
		return -2;

	// build new interfaces file string
	N = strlen(s) + strlen(stanza);
	ns = malloc(N); // maximum size if we are just adding a new stanza
	if (!ns)
		return -3;

	if (start == -1)
		// new stanza so we will append it to the end
		snprintf(ns, N, "%s%s", s, stanza);
	else
		// stanza is in the middle of the file
		snprintf(ns, N, "%.*s%s%.*s", start, s, stanza, strlen(s) - end, &s[end+1]);

	// rewrite the interfaces file
	fd = fopen((filename)?filename:INTERFACES_FILE, "w");
	if (fd == NULL)
		return -4;
	fprintf(fd, "%s", ns);
	
	if (fd)
		fclose(fd);
	fd = NULL;
	return 0;
}

