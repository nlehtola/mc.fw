#ifndef IFACE_H_
#define IFACE_H_

#define IFACE_STR_LEN 32
#define ADDR_FAMILY_LEN 32
#define METHOD_STR_LEN 32
#define ADDR_STR_LEN 16
#define NETMASK_STR_LEN 16

struct iface_t
{
	char iface_name[IFACE_STR_LEN];
	char addr_family[ADDR_FAMILY_LEN];
	enum method_t
	{
		IFACE_METHOD_UNKNOWN,
		IFACE_METHOD_STATIC,
		IFACE_METHOD_DHCP,
		IFACE_METHOD_LOOPBACK,
	} method;

	int addr[4];

	int netmask[4];
};

struct iface_t * iface_get_cfg(struct iface_t *dst, char *filename);
struct iface_t * iface_get_active_cfg(struct iface_t *dst);
int iface_set_cfg(struct iface_t *iface, char *filename);
char *iface_method_to_str(struct iface_t *iface, char *s, int len);
char *iface_addr_to_str(struct iface_t *iface, char *s, int len);
char *iface_netmask_to_str(struct iface_t *iface, char *s, int len);
void iface_print(struct iface_t *iface);

#endif
