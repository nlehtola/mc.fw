
#ifndef __BKOW_H__
#define __BKOW_H__

#include "kowhai.h"

typedef struct bkow_t bkow_t;

struct bkow_tree_t
{
	// extend the kowhai_tree_t type using -fms-extensions
	// eventually we should put this all back in kowhai somehow
	struct kowhai_tree_t;
	int desc_len;
	int data_len;
};

typedef int (*bkow_write_t)(void *buf, int len, void *param);

typedef int (*bkow_read_t)(void *buf, int len, void *param);

int bkow_symid(bkow_t *h, const char *sym, int len);

char * bkow_sym(bkow_t *h, uint16_t symid);

int bkow_pull_data(bkow_t *h, uint16_t tree_id, int path_count, union kowhai_symbol_t *path, void **data);

int bkow_push_data(bkow_t *h, uint16_t tree_id, int path_count, union kowhai_symbol_t *path, void *data, int data_size);

int bkow_pull_tree(bkow_t *h, int tree_id, struct bkow_tree_t *tree);

int bkow_push_tree(bkow_t *h, int tree_id, struct bkow_tree_t *tree);

struct bkow_tree_t * bkow_realloc_tree(struct bkow_tree_t *tree, int nodes, int max_node_size);

int bkow_free_tree(struct bkow_tree_t *tree);

int bkow_call_func(bkow_t *h, int func_sym_id, void *params, int params_size, void *ret, int ret_size, double read_timeout, double write_timeout);

void bkow_dump_version(bkow_t *h);

void bkow_dump_tree(bkow_t *h);

bkow_t *bkow_init(bkow_read_t read, int read_pkt_size, void *read_param, bkow_write_t write, int write_pkt_size, void *write_param);

int bkow_deinit(bkow_t *h);

#endif

