/**
 * @brief model of the blades that speak kowhai
 *
 * @author othane
 *
 * @date Feb 2014
 *
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <inttypes.h>
#include <bcoms.h>

#include "kblade.h"
#include "bkow.h"
#include "kowhai.h"
#include "kowhai_serialize.h"

#ifndef packed
#define packed(type) type __attribute__ ((packed))      		// pack this object (ie struct etc)
#endif

#define WTIMEOUT (3.0f)
#define RTIMEOUT (3.0f)

#define INVALID_CH (0xff)

#define SYMID(h, sym) bkow_symid(h, sym, strlen(sym))

struct blade_t
{
	int addr;
	enum kblade_pid pid;
	struct bkow_t *kowh;	 // bkow handle
	struct bkow_tree_t st; 	 // settings tree
	char *st_buf;
	int st_buf_len;
} blades[MAX_BLADES];

union any_type_t
{
    char c;
    int8_t i8;
    int16_t i16;
    int32_t i32;
    uint8_t ui8;
    uint16_t ui16;
    uint32_t ui32;
    float f;
};

// blade functions

int kblade_get_fw_version(int blade_id, uint16_t *major, uint16_t *minor)
{
	int sym_id = SYMID(blades[blade_id].kowh, "version");
	packed(struct)
	{
		uint16_t major;
		uint16_t minor;
	} ret;

	// sanity check
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol (function not support by server)\n",  __func__, __LINE__);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, NULL, 0, &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: func call failed\n", __func__, __LINE__);
		return -1;
	}
	
	if (major != NULL)
		*major = ret.major;
	if (minor != NULL)
		*minor = ret.minor;

	return 0;
}

int kblade_get_product_id(int blade_id, uint32_t *pid)
{
	int sym_id = SYMID(blades[blade_id].kowh, "get_pid");
	uint32_t _pid;

	// sanity check
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol (function not support by server)\n",  __func__, __LINE__);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, NULL, 0, &_pid, sizeof(_pid), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: func call failed\n", __func__, __LINE__);
		return -1;
	}
	
	if (pid != NULL)
		*pid = _pid;

	return 0;
}

int kblade_reboot(int blade_id, enum kblade_pid pid)
{
	uint32_t _pid = pid;
	uint8_t ret = 0;
	int sym_id = SYMID(blades[blade_id].kowh, "reboot");

	// sanity check
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol (function not support by server)\n",  __func__, __LINE__);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, &_pid, sizeof(_pid), &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: func call failed\n", __func__, __LINE__);
		return -1;
	}

	// return negative numbers from kowhai on error
	if (!ret)
		return -1;

	return ret;
}

int kblade_load_settings_from_rom(int blade_id)
{
	uint8_t ret = 0;
	int sym_id = SYMID(blades[blade_id].kowh, "load_settings");

	// sanity check
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol (function not support by server)\n",  __func__, __LINE__);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, NULL, 0, &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: func call failed\n", __func__, __LINE__);
		return -1;
	}

	// return negative numbers from kowhai on error
	if (!ret)
		return -1;

	return ret;
}

int kblade_load_factory_defaults(int blade_id)
{
	uint8_t ret = 0;
	int sym_id = SYMID(blades[blade_id].kowh, "load_factory_defaults");

	// sanity check
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol (function not support by server)\n",  __func__, __LINE__);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, NULL, 0, &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: func call failed\n", __func__, __LINE__);
		return -1;
	}

	// return negative numbers from kowhai on error
	if (!ret)
		return -1;

	return ret;
}

// commits can take a lot longer
#define COMMIT_WTIMEOUT (5.0)
#define COMMIT_RTIMEOUT (5.0)
int kblade_commit_settings(int blade_id)
{
	uint8_t ret = 0;
	int sym_id = SYMID(blades[blade_id].kowh, "commit");

	// sanity check
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol (function not support by server)\n",  __func__, __LINE__);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, NULL, 0, &ret, sizeof(ret), COMMIT_RTIMEOUT, COMMIT_WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: func call failed\n", __func__, __LINE__);
		return -1;
	}

	// return negative numbers from kowhai on error
	if (!ret)
		return -1;

	return ret;
}

int kblade_commit_factory_defaults(int blade_id)
{
	uint8_t ret = 0;
	int sym_id = SYMID(blades[blade_id].kowh, "commit_factory_defaults");

	// sanity check
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol (function not support by server)\n",  __func__, __LINE__);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, NULL, 0, &ret, sizeof(ret), COMMIT_RTIMEOUT, COMMIT_WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: func call failed\n", __func__, __LINE__);
		return -1;
	}

	// return negative numbers from kowhai on error
	if (!ret)
		return -1;

	return ret;
}

// generic get function used by other get methods
static int kblade_get_general(char *sym, int blade_id, int channel, float *set, float *actual, float *def, float *min, float *max)
{
	packed(struct)
	{
		uint8_t channel;
	} cmd = {.channel = channel};
	packed(struct)
	{
		uint8_t channel;
		float set;
		float actual;
		float def;
		float min;
		float max;
	} ret;

	int sym_id = SYMID(blades[blade_id].kowh, sym);
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol [%d:%s] (function not support by server)\n",  __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, &cmd, sizeof(cmd), &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d][%d:%s]: func call failed\n", __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// these calls fail by returning a special "bad" channel
	if (ret.channel == INVALID_CH)
		return -1;
	
	if (set != NULL)
		*set = ret.set;
	if (actual != NULL)
		*actual = ret.actual;
	if (def != NULL)
		*def= ret.def;
	if (min != NULL)
		*min = ret.min;
	if (max != NULL)
		*max = ret.max;

	return 0;
}

// generic set function for simple float only values
static int kblade_set_general(char *sym, int blade_id, int channel, float set)
{
	packed(struct)
	{
		uint8_t channel;
		float set;
	} cmd = {.channel = channel, .set = set};
	uint8_t ret = 0;

	int sym_id = SYMID(blades[blade_id].kowh, sym);
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol [%d:%s] (function not support by server)\n",  __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, &cmd, sizeof(cmd), &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d][%d:%s]: func call failed\n", __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// return negative numbers from kowhai on error
	if (!ret)
		return -1;

	return ret;
}

int kblade_get_power_raw(int blade_id, int channel, float *actual, float *min, float *max)
{
	packed(struct)
	{
		uint8_t channel;
	} cmd = {.channel = channel};
	packed(struct)
	{
		uint8_t channel;
		float actual;
		float min;
		float max;
	} ret;

	char *sym = "get_power_raw";
	int sym_id = SYMID(blades[blade_id].kowh, sym);
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol [%d:%s] (function not support by server)\n",  __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, &cmd, sizeof(cmd), &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d][%d:%s]: func call failed\n", __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// these calls fail by returning a special "bad" channel
	if (ret.channel == INVALID_CH)
		return -1;
	
	if (actual != NULL)
		*actual = ret.actual;
	if (min != NULL)
		*min = ret.min;
	if (max != NULL)
		*max = ret.max;

	return 0;
}

int kblade_get_power(int blade_id, int channel, float *set, float *actual, float *def, float *min, float *max)
{
	return kblade_get_general("get_power", blade_id, channel, set, actual, def, min, max);
}

int kblade_set_power(int blade_id, int channel, float power)
{
	return kblade_set_general("set_power", blade_id, channel, power);
}

int kblade_get_dark_power(int blade_id, int channel, float *set, float *min, float *max, float *raw)
{
	packed(struct)
	{
		uint8_t channel;
	} cmd = {.channel = channel};
	packed(struct)
	{
		uint8_t channel;
		float set;
		float min;
		float max;
		float raw;
	} ret;

	char *sym = "get_dark_power";
	int sym_id = SYMID(blades[blade_id].kowh, sym);
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol [%d:%s] (function not support by server)\n",  __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, &cmd, sizeof(cmd), &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d][%d:%s]: func call failed\n", __func__, __LINE__, sym_id, sym);
		return -1;
	}
	
	// these calls fail by returning a special "bad" channel
	if (ret.channel == INVALID_CH)
		return -1;
	
	if (set != NULL)
		*set = ret.set;
	if (min != NULL)
		*min = ret.min;
	if (max != NULL)
		*max = ret.max;
	if (raw != NULL)
		*raw = ret.raw;

	return 0;
}

int kblade_set_dark_power(int blade_id, int channel, float pdark)
{
	packed(struct)
	{
		uint8_t channel;
		float set;
	} cmd = {.channel = channel, .set = pdark};
	uint8_t ret = 0;

	char *sym = "set_dark_power";
	int sym_id = SYMID(blades[blade_id].kowh, sym);
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol [%d:%s] (function not support by server)\n",  __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, &cmd, sizeof(cmd), &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d][%d:%s]: func call failed\n", __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// return negative numbers from kowhai on error
	if (!ret)
		return -1;

	return ret;
}

int kblade_get_dark_power_time_remaining(int blade_id, int channel, float *time)
{
	packed(struct)
	{
		uint8_t channel;
	} cmd = {.channel = channel};
	packed(struct)
	{
		uint8_t channel;
		float time;
	} ret;

	char *sym = "get_dark_power_time_remaining";
	int sym_id = SYMID(blades[blade_id].kowh, sym);
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol [%d:%s] (function not support by server)\n",  __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, &cmd, sizeof(cmd), &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d][%d:%s]: func call failed\n", __func__, __LINE__, sym_id, sym);
		return -1;
	}
	
	// these calls fail by returning a special "bad" channel
	if (ret.channel == INVALID_CH)
		return -1;
	
	if (time != NULL)
		*time = ret.time;

	return 0;
}

int kblade_start_dark_power_measurement(int blade_id, int channel)
{
	packed(struct)
	{
		uint8_t channel;
	} cmd = {.channel = channel};
	uint8_t ret = 0;

	char *sym = "measure_dark_power";
	int sym_id = SYMID(blades[blade_id].kowh, sym);
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol [%d:%s] (function not support by server)\n",  __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, &cmd, sizeof(cmd), &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d][%d:%s]: func call failed\n", __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// return negative numbers from kowhai on error
	if (!ret)
		return -1;

	return ret;
}

int kblade_get_power_averaging_time(int blade_id, int channel, float *set, float *def, float *min, float *max)
{
	packed(struct)
	{
		uint8_t channel;
	} cmd = {.channel = channel};
	packed(struct)
	{
		uint8_t channel;
		float set;
		float def;
		float min;
		float max;
	} ret;

	char *sym = "get_power_averaging_time";
	int sym_id = SYMID(blades[blade_id].kowh, sym);
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol [%d:%s] (function not support by server)\n",  __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, &cmd, sizeof(cmd), &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d][%d:%s]: func call failed\n", __func__, __LINE__, sym_id, sym);
		return -1;
	}
	
	// these calls fail by returning a special "bad" channel
	if (ret.channel == INVALID_CH)
		return -1;
	
	if (set != NULL)
		*set = ret.set;
	if (def != NULL)
		*def = ret.def;
	if (min != NULL)
		*min = ret.min;
	if (max != NULL)
		*max = ret.max;

	return 0;
}

int kblade_set_power_averaging_time(int blade_id, int channel, float time)
{
	packed(struct)
	{
		uint8_t channel;
		float set;
	} cmd = {.channel = channel, .set = time};
	uint8_t ret = 0;

	char *sym = "set_power_averaging_time";
	int sym_id = SYMID(blades[blade_id].kowh, sym);
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol [%d:%s] (function not support by server)\n",  __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, &cmd, sizeof(cmd), &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d][%d:%s]: func call failed\n", __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// return negative numbers from kowhai on error
	if (!ret)
		return -1;

	return ret;
}

int kblade_get_attenuation_raw(int blade_id, int channel, float *set, float *actual, float *min, float *max)
{
	packed(struct)
	{
		uint8_t channel;
	} cmd = {.channel = channel};
	packed(struct)
	{
		uint8_t channel;
		float set;
		float actual;
		float min;
		float max;
	} ret;

	char *sym = "get_attenuation_raw";
	int sym_id = SYMID(blades[blade_id].kowh, sym);
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol [%d:%s] (function not support by server)\n",  __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, &cmd, sizeof(cmd), &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d][%d:%s]: func call failed\n", __func__, __LINE__, sym_id, sym);
		return -1;
	}
	
	// these calls fail by returning a special "bad" channel
	if (ret.channel == INVALID_CH)
		return -1;
	
	if (set != NULL)
		*set = ret.set;
	if (actual != NULL)
		*actual = ret.actual;
	if (min != NULL)
		*min = ret.min;
	if (max != NULL)
		*max = ret.max;

	return 0;
}

int kblade_get_attenuation(int blade_id, int channel, float *set, float *actual, float *def, float *min, float *max)
{
	return kblade_get_general("get_attenuation", blade_id, channel, set, actual, def, min, max);
}

int kblade_set_attenuation_raw(int blade_id, int channel, float attenuation)
{
	packed(struct)
	{
		uint8_t channel;
		float set;
	} cmd = {.channel = channel, .set = attenuation};
	uint8_t ret = 0;

	char *sym = "set_attenuation_raw";
	int sym_id = SYMID(blades[blade_id].kowh, sym);
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol [%d:%s] (function not support by server)\n",  __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, &cmd, sizeof(cmd), &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d][%d:%s]: func call failed\n", __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// return negative numbers from kowhai on error
	if (!ret)
		return -1;

	return ret;
}

int kblade_set_attenuation(int blade_id, int channel, float attenuation)
{
	return kblade_set_general("set_attenuation", blade_id, channel, attenuation);
}

int kblade_get_attenuation_offset(int blade_id, int channel, float *offset)
{
	packed(struct)
	{
		uint8_t channel;
	} cmd = {.channel = channel};

	packed(struct)
	{
		uint8_t channel;
		float offset;
	} ret;

	char *sym = "get_attenuation_offset";
	int sym_id = SYMID(blades[blade_id].kowh, sym);
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol [%d:%s] (function not support by server)\n",  __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, &cmd, sizeof(cmd), &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d][%d:%s]: func call failed\n", __func__, __LINE__, sym_id, sym);
		return -1;
	}
	
	// these calls fail by returning a special "bad" channel
	if (ret.channel == INVALID_CH)
		return -1;
	
	if (offset != NULL)
		*offset = ret.offset;

	return 0;
}

int kblade_set_attenuation_offset(int blade_id, int channel, float offset)
{
	packed(struct)
	{
		uint8_t channel;
		float offset;
	} cmd = {.channel = channel, .offset = offset};

	uint8_t ret;

	char *sym = "set_attenuation_offset";
	int sym_id = SYMID(blades[blade_id].kowh, sym);
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol [%d:%s] (function not support by server)\n",  __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, &cmd, sizeof(cmd), &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d][%d:%s]: func call failed\n", __func__, __LINE__, sym_id, sym);
		return -1;
	}
	
	// return negative numbers from kowhai on error
	if (!ret)
		return -1;

	return ret;
}

int kblade_get_attenuation_velocity(int blade_id, int channel, float *set, float *actual, float *def, float *min, float *max)
{
	return kblade_get_general("get_attenuation_velocity", blade_id, channel, set, actual, def, min, max);
}

int kblade_set_attenuation_velocity(int blade_id, int channel, float velocity)
{
	return kblade_set_general("set_attenuation_velocity", blade_id, channel, velocity);
}

int kblade_get_attenuator_temperature(int blade_id, int channel, float *actual, float *min, float *max)
{
	packed(struct)
	{
		uint8_t channel;
	} cmd = {.channel = channel};

	packed(struct)
	{
		uint8_t channel;
		float actual;
		float min;
		float max;
	} ret;

	char *sym = "get_attenuator_temperature";
	int sym_id = SYMID(blades[blade_id].kowh, sym);
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol [%d:%s] (function not support by server)\n",  __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, &cmd, sizeof(cmd), &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d][%d:%s]: func call failed\n", __func__, __LINE__, sym_id, sym);
		return -1;
	}
	
	// these calls fail by returning a special "bad" channel
	if (ret.channel == INVALID_CH)
		return -1;
	
	if (actual != NULL)
		*actual = ret.actual;
	if (min != NULL)
		*min = ret.min;
	if (max != NULL)
		*max = ret.max;

	return 0;
}


int kblade_get_wavelength(int blade_id, int channel, float *set, float *def, float *min, float *max)
{
	// dam we dont have a actual so we need to use non-general
	packed(struct)
	{
		uint8_t channel;
	} cmd = {.channel = channel};
	packed(struct)
	{
		uint8_t channel;
		float set;
		float def;
		float min;
		float max;
	} ret;

	char *sym = "get_wavelength";
	int sym_id = SYMID(blades[blade_id].kowh, sym);
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol [%d:%s] (function not support by server)\n",  __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, &cmd, sizeof(cmd), &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d][%d:%s]: func call failed\n", __func__, __LINE__, sym_id, sym);
		return -1;
	}
	
	// these calls fail by returning a special "bad" channel
	if (ret.channel == INVALID_CH)
		return -1;
	
	if (set != NULL)
		*set = ret.set;
	if (def != NULL)
		*def = ret.def;
	if (min != NULL)
		*min = ret.min;
	if (max != NULL)
		*max = ret.max;

	return 0;
}

int kblade_set_wavelength(int blade_id, int channel, float wavelength)
{
	packed(struct)
	{
		uint8_t channel;
		float set;
	} cmd = {.channel = channel, .set = wavelength};
	uint8_t ret = 0;

	char *sym = "set_wavelength";
	int sym_id = SYMID(blades[blade_id].kowh, sym);
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol [%d:%s] (function not support by server)\n",  __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, &cmd, sizeof(cmd), &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d][%d:%s]: func call failed\n", __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// return negative numbers from kowhai on error
	if (!ret)
		return -1;

	return ret;
}

int kblade_get_ctrl_mode(int blade_id, int channel, enum kblade_ctrl_mode_t *mode)
{
	packed(struct)
	{
		uint8_t channel;
	} cmd = {.channel = channel};
	packed(struct)
	{
		uint8_t channel;
		uint8_t mode;
	} ret;

	char *sym = "get_control_mode";
	int sym_id = SYMID(blades[blade_id].kowh, sym);
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol [%d:%s] (function not support by server)\n",  __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, &cmd, sizeof(cmd), &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d][%d:%s]: func call failed\n", __func__, __LINE__, sym_id, sym);
		return -1;
	}
	
	// these calls fail by returning a special "bad" channel
	if (ret.channel == INVALID_CH)
		return -1;
	
	if (mode != NULL)
		*mode = ret.mode;

	return 0;
}

int kblade_set_ctrl_mode(int blade_id, int channel, enum kblade_ctrl_mode_t mode)
{
	packed(struct)
	{
		uint8_t channel;
		uint8_t mode;
	} cmd = {.channel = channel, .mode = mode};
	uint8_t ret = 0;

	char *sym = "set_control_mode";
	int sym_id = SYMID(blades[blade_id].kowh, sym);
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol [%d:%s] (function not support by server)\n",  __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, &cmd, sizeof(cmd), &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d][%d:%s]: func call failed\n", __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// return negative numbers from kowhai on error
	if (!ret)
		return -1;

	return ret;
}

int kblade_get_locked(int blade_id, int channel, uint8_t *attenuation_locked, uint8_t *power_locked)
{
	packed(struct)
	{
		uint8_t channel;
	} cmd = {.channel = channel};
	packed(struct)
	{
		uint8_t channel;
		uint8_t attenuation;
		uint8_t power;
	} ret;

	char *sym = "get_locked";
	int sym_id = SYMID(blades[blade_id].kowh, sym);
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol [%d:%s] (function not support by server)\n",  __func__, __LINE__, sym_id, sym);
		return -1;
	}

	// call function
	if (bkow_call_func(blades[blade_id].kowh, sym_id, &cmd, sizeof(cmd), &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d][%d:%s]: func call failed\n", __func__, __LINE__, sym_id, sym);
		return -1;
	}
	
	// these calls fail by returning a special "bad" channel
	if (ret.channel == INVALID_CH)
		return -1;
	
	if (attenuation_locked != NULL)
		*attenuation_locked = ret.attenuation;
	if (power_locked != NULL)
		*power_locked = ret.power;

	return 0;
}

enum kblade_pid kblade_get_pid(int blade_id)
{
	return (enum kblade_pid)blades[blade_id].pid;
}

// framework

static int kblade_read(void *buf, int len, void *param)
{
	struct blade_t *blade = (struct blade_t *)param;

	bcoms_set_blade(blade->addr);
	return bcoms_read(buf, len);
}

static int kblade_write(void *buf, int len, void *param)
{
	struct blade_t *blade = (struct blade_t *)param;

	bcoms_set_blade(blade->addr);
	return bcoms_write(buf, len);
}

int kblade_push_settings(int blade_id)
{
	int nodes, n;
	struct blade_t *blade = &blades[blade_id];
	bkow_t *kowh = blade->kowh;
	int tree_id;

	// get the settings tree
	tree_id = SYMID(kowh, "settings");
	if (tree_id < 0)
		return -1;

	// set the whole settings tree in the firmware
	///@todo "get_power is clearly wrong, make this settings !!
	if (bkow_push_tree(kowh, tree_id, &blade->st) != 0)
		return -1;

	// note if a commit is not done the new settings will be lost
	// on reset
	return 0;
}

int kblade_pull_settings(int blade_id)
{
	int nodes, n;
	struct blade_t *blade = &blades[blade_id];
	bkow_t *kowh = blade->kowh;
	int tree_id;

	// get the settings tree
	tree_id = SYMID(kowh, "settings");
	if (tree_id < 0)
		return -1;

	// get the settings tree from the firmware
	///@todo "get_power is clearly wrong, make this settings !!
	if ((nodes = bkow_pull_tree(kowh, tree_id, &blade->st)) < 0)
		return -1;

	return 0;
}

static int ignore_missing_nodes(void* param, union kowhai_symbol_t *path, int path_len)
{
	return 0;
}

#define SCRATCH_LEN_INC (20 * 1024)
#define PATH_LEN_INC (16)
int kblade_set_settings(int blade_id, char * settings, int len)
{
	struct blade_t *blade = &blades[blade_id];
	char *scratch = NULL;
	int scratch_len = SCRATCH_LEN_INC;
	int e;
	union kowhai_symbol_t *path = NULL;
	int path_len = PATH_LEN_INC;
	int n = 0;
	
	scratch = (char *)realloc(scratch, scratch_len);
	path = (union kowhai_symbol_t *)realloc(path, path_len * sizeof(union kowhai_symbol_t));

	// de serialize string to tree
	while (n++ < 100)
	{
		if (scratch == NULL || path == NULL)
			goto done;
		e = kowhai_deserialize_nodes(settings, len, (struct kowhai_tree_t *)&blade->st, path, path_len, scratch, scratch_len, blade->kowh, (kowhai_get_symbol_t)bkow_symid, NULL, ignore_missing_nodes);
		switch (e)
		{
			// not enough space issues
			case KOW_STATUS_SCRATCH_TOO_SMALL:
				scratch_len += SCRATCH_LEN_INC;
				scratch = (char *)realloc(scratch, scratch_len);
				break;
			case KOW_STATUS_PATH_TOO_SMALL:
				path_len += PATH_LEN_INC;
				path = (union kowhai_symbol_t *)realloc(path, path_len * sizeof(union kowhai_symbol_t));
				break;
			
			// success
			case KOW_STATUS_OK:
				// push the merged settings to the device
				kblade_push_settings(blade_id);
				goto done;
			
			// other errors
			default:
				goto done;
		}
	}

done:
	if (scratch)
		free(scratch);
	if (path)
		free(path);
	return e;
}

#define ST_BUF_LEN_INC (20 * 1024)
char * kblade_get_settings(int blade_id, const char * filename)
{
	int st_buf_len;
	struct blade_t *blade = &blades[blade_id];
	union kowhai_symbol_t *path = NULL;
	int path_len = PATH_LEN_INC;
	int e, n = 0;

	if (blade->st_buf_len == 0 || blade->st_buf == NULL)
	{
		blade->st_buf_len = ST_BUF_LEN_INC;
		blade->st_buf = (char*)realloc(blade->st_buf, blade->st_buf_len);
	}
	path = (union kowhai_symbol_t *)realloc(path, path_len * sizeof(union kowhai_symbol_t));

	// make a serialised string
	while (n++ < 100)
	{
		if (path == NULL || blade->st_buf == NULL)
			goto done;

		st_buf_len = blade->st_buf_len;
		e = kowhai_serialize_nodes(blade->st_buf, &st_buf_len, (struct kowhai_tree_t *)&blade->st, path, path_len, blade->kowh, (kowhai_get_symbol_name_t)bkow_sym);
		switch (e)
		{
			// not enough space issues
			case KOW_STATUS_PATH_TOO_SMALL:
				path_len += PATH_LEN_INC;
				path = (union kowhai_symbol_t *)realloc(path, path_len * sizeof(union kowhai_symbol_t));
				break;
			case KOW_STATUS_TARGET_BUFFER_TOO_SMALL:
				blade->st_buf_len += ST_BUF_LEN_INC;
				blade->st_buf = (char*)realloc(blade->st_buf, blade->st_buf_len);
				break;

			// success
			case KOW_STATUS_OK:
				// if filename given then save it
				if (filename != NULL)
				{
					FILE *fd = fopen(filename, "w");
					e = -1;
					if (fd == NULL)
						goto done;
					if (fputs(blade->st_buf, fd) < 0)
					{
						fclose(fd);
						goto done;
					}
					fclose(fd);
					e = 0;
				}
				goto done;

			// other errors
			default:
				goto done;
		}
	}

done:
	if (path)
		free(path);
	if (e == KOW_STATUS_OK)
		return blade->st_buf;
	else
		return NULL;
}


static union kowhai_symbol_t * get_kpath(struct blade_t *blade, const char * path, int *path_len)
{
	union kowhai_symbol_t *kpath = NULL;
	int kpath_len = PATH_LEN_INC;
	int e;

	kpath = (union kowhai_symbol_t *)realloc(kpath, kpath_len * sizeof(union kowhai_symbol_t));
	while (1)
	{
		if (kpath == NULL || kpath_len > 100)
			return NULL;
		e = kowhai_str_to_path(path, *path_len, kpath, &kpath_len, blade->kowh, (kowhai_get_symbol_t)bkow_symid);
		switch (e)
		{
			case KOW_STATUS_PATH_TOO_SMALL:
				kpath_len += PATH_LEN_INC;
				kpath = (union kowhai_symbol_t *)realloc(kpath, kpath_len * sizeof(union kowhai_symbol_t));
				break;
			case KOW_STATUS_OK:
				*path_len = kpath_len;
				return kpath;
			default:
				return NULL;
		}
	}
}

int kblade_get_setting(int blade_id, const char * path, int path_len, void *dst, int dst_len, int pull)
{
	struct blade_t *blade = &blades[blade_id];
	union kowhai_symbol_t *kpath = NULL;
	void *data = NULL;
	int e = KOW_STATUS_OK;

	// de-serialise the path
	if ((kpath = get_kpath(blade, path, &path_len)) == NULL)
		return -2;
	
	if (pull)
	{
		// pull the setting out of the firmware and copy them into
		// dst
		e = bkow_pull_data(blade->kowh, kpath[0].parts.name, path_len, kpath, &data);
		if (e < 0)
			goto done;
		memcpy(dst, data, (dst_len < e)? dst_len: e);
		e = KOW_STATUS_OK;
	}
	else
		// get the setting from our local cache
		e = kowhai_read((struct kowhai_tree_t *)&blade->st, path_len, kpath, 0, dst, dst_len);

done:
	if (kpath)
		free(kpath);
	if (data)
		free(data);
	if (e != KOW_STATUS_OK)
		return -3;
	return 0;
}

static char * get_node_type_format(struct kowhai_node_t *node)
{
	switch (node->type)
	{
		case KOW_CHAR:
			return "%c";
		case KOW_INT8:
			return "%"PRIi8;
		case KOW_UINT8:
			return "%"PRIu8;
		case KOW_INT16:
			return "%"PRIi16;
		case KOW_UINT16:
			return "%"PRIu16;
		case KOW_INT32:
			return "%"PRIi32;
		case KOW_UINT32:
			return "%"PRIu32;
		case KOW_FLOAT:
			return "%g";
		defuault:
			return NULL;
	}
}

static int put_node_val(char **dst, int *dst_len, const char * fmt, struct kowhai_node_t *node, void *value)
{
	int e = 0;
	union any_type_t tmp;

	if (value == NULL)
	{
		e = snprintf(*dst, *dst_len, fmt);
		goto done;
	}

	if (node == NULL)
		goto done;

	memcpy(&tmp, value, kowhai_get_node_type_size(node->type));
	switch (node->type)
	{
		case KOW_CHAR:
			e = snprintf(*dst, *dst_len, fmt, tmp.c);
			break;
		case KOW_INT8:
			e = snprintf(*dst, *dst_len, fmt, tmp.i8);
			break;
		case KOW_UINT8:
			e = snprintf(*dst, *dst_len, fmt, tmp.ui8);
			break;
		case KOW_INT16:
			e = snprintf(*dst, *dst_len, fmt, tmp.i16);
			break;
		case KOW_UINT16:
			e = snprintf(*dst, *dst_len, fmt, tmp.ui16);
			break;
		case KOW_INT32:
			e = snprintf(*dst, *dst_len, fmt, tmp.i32);
			break;
		case KOW_UINT32:
			e = snprintf(*dst, *dst_len, fmt, tmp.ui32);
			break;
		case KOW_FLOAT:
			e = snprintf(*dst, *dst_len, fmt, tmp.f);
			break;
		defuault:
			break;
	}

done:
	if (e < 0 || e > *dst_len)
		return -6;
	*dst += e;
	*dst_len -= e;
	return e;
}

int kblade_get_setting_str(int blade_id, const char * path, int path_len, char *dst, int dst_len, int pull)
{
	struct blade_t *blade = &blades[blade_id];
	void *node_data = NULL, *node_data_init = NULL;
	int node_size, e = -1, n = 0, node_type_size;
	struct kowhai_node_t *node;
	const char * fmt;
	union kowhai_symbol_t *kpath = NULL;
	int kpath_len = path_len;

	// de-serialise the path
	if ((kpath = get_kpath(blade, path, &kpath_len)) == NULL)
		return -9;
	
	// get node info
	e = kowhai_get_node((const struct kowhai_node_t *)&blade->st.desc[0], kpath_len, kpath, 0, &node);
	if (e != KOW_STATUS_OK)
	{
		e = -1;
		goto done;
	}
	e = kowhai_get_node_size(node, &node_size);
	if (e != KOW_STATUS_OK)
	{
		e = -2;
		goto done;
	}
	node_type_size = kowhai_get_node_type_size(node->type);
	if (node_type_size < 1)
		return -19;

	// alloc space for the node data
	node_data_init = malloc(node_size);
	node_data = node_data_init;
	if (node_data == NULL)
	{
		e = -3;
		goto done;
	}

	// get the setting(s)
	e = kblade_get_setting(blade_id, path, path_len, node_data, node_size, pull);
	if (e != KOW_STATUS_OK)
	{
		e = -4;
		goto done;
	}

	// serialise the data into the dst buffer
	fmt = get_node_type_format(node);
	if (fmt == NULL)
	{
		e = -5;
		goto done;
	}
	if (node->count > 1)
		if ((e = put_node_val(&dst, &dst_len, "[", node, NULL)) < 0)
			goto done;
	n = kpath[kpath_len - 1].parts.array_index;
	while (n++ < node->count && node_size > 0 && dst_len > 0)
	{
		if ((e = put_node_val(&dst, &dst_len, fmt, node, node_data)) < 0)
			goto done;
		if (n != node->count)
			if ((e = put_node_val(&dst, &dst_len, ", ", node, NULL)) < 0)
				goto done;
		node_data = (uint8_t *)node_data + node_type_size;
		node_size -= node_type_size;
	}
	if (node->count > 1)
		if ((e = put_node_val(&dst, &dst_len, "]", node, NULL)) < 0)
			goto done;
	e = 0;
		
done:
	// clean up
	if (kpath)
		free(kpath);
	if (node_data_init)
		free(node_data_init);
	return e;
}

int kblade_set_setting(int blade_id, const char * path, int path_len, void *src, int src_len, int push)
{
	struct blade_t *blade = &blades[blade_id];
	union kowhai_symbol_t *kpath = NULL;
	int e = KOW_STATUS_OK;

	// de-serialise the path
	if ((kpath = get_kpath(blade, path, &path_len)) == NULL)
		return -2;
	
	if (push)
		// push src to the firmware
		e = bkow_push_data(blade->kowh, kpath[0].parts.name, path_len, kpath, src, src_len);
	else
		// set the setting in our local settings cache
		e = kowhai_write((struct kowhai_tree_t *)&blade->st, path_len, kpath, 0, &src, src_len);

done:
	if (kpath)
		free(kpath);
	if (e != KOW_STATUS_OK)
		return -3;
	return 0;
}

static int get_node_val(char **src, int *src_len, const char *fmt, struct kowhai_node_t *node, void *value)
{
	int __n, e = -1;
	char _fmt[128];
	union any_type_t _val;

	if (value == NULL)
	{
		snprintf(_fmt, 128, " %s%%n", fmt);
		e = sscanf(*src, _fmt, &__n);
	}
	else
	{
		// this jumble of shit should allow for spaces and commas etc
		snprintf(_fmt, 128, " %s%%n,%%n", fmt);
		e = sscanf(*src, _fmt, &_val, &__n, &__n);
		memcpy(value, &_val, kowhai_get_node_type_size(node->type));
	}

	if (e < 0 || __n > *src_len)
		return -6;
	*src += __n;
	*src_len -= __n;
	return __n;
}

int kblade_set_setting_str(int blade_id, const char * path, int path_len, char *src, int src_len, int push)
{
	struct blade_t *blade = &blades[blade_id];
	void *node_data = NULL, *node_data_init = NULL;
	int node_size, node_bytes_to_send = 0, e = -1, n = 0, node_type_size;
	struct kowhai_node_t *node;
	const char * fmt;
	union kowhai_symbol_t *kpath = NULL;
	int kpath_len = path_len;

	// de-serialise the path
	if ((kpath = get_kpath(blade, path, &kpath_len)) == NULL)
		return -9;
	
	// get node info
	e = kowhai_get_node((const struct kowhai_node_t *)&blade->st.desc[0], kpath_len, kpath, 0, &node);
	if (e != KOW_STATUS_OK)
		return -1;
	e = kowhai_get_node_size(node, &node_size);
	if (e != KOW_STATUS_OK)
		return -2;
	node_type_size = kowhai_get_node_type_size(node->type);

	// alloc space for the node data
	node_data_init = malloc(node_size);
	node_data = node_data_init;
	if (node_data == NULL)
		return -3;

	// de-serialise the src buffer
	fmt = get_node_type_format(node);
	get_node_val(&src, &src_len, "[", node, NULL); // dont care if this fails (we can work with poorly formatted stuff)
	n = kpath[kpath_len - 1].parts.array_index;
	while (n++ < node->count && node_size > 0 && src_len > 0)
	{
		if ((e = get_node_val(&src, &src_len, fmt, node, node_data)) < 0)
			goto done;
		node_data = (uint8_t *)node_data + node_type_size;
		node_bytes_to_send += node_type_size;
		node_size -= node_type_size;
	}

	// set the setting(s)
	e = kblade_set_setting(blade_id, path, path_len, node_data_init, node_bytes_to_send, push);

done:
	// free results
	if (kpath)
		free(kpath);
	if (node_data_init)
		free(node_data_init);
	return e;
}

int kblade_flush(int blade_id)
{
	// this should not be needed, it is just an emergency bridge to bcoms for debugging
	bcoms_set_blade(blade_id);
	bcoms_flush();
	return 0;
}

int kblade_init(uint16_t *mask)
{
	int b; 

	// init bcoms module (this is the transfer layer for the kowhia
	// protocol below)
	bcoms_init();

	// init all possible blades
	for (b = 0; b < MAX_BLADES; b++)
	{
		uint32_t pid;

		// zero init buffers
		blades[b].st.desc = NULL;
		blades[b].st.desc_len = 0;
		blades[b].st.data = NULL;
		blades[b].st.data_len = 0;
		blades[b].st_buf = NULL;
		blades[b].st_buf_len = 0;

		// if we were told there is no blade here, then skip it
		if ((*mask & (1 << b)) == 0)
			continue;

		bcoms_set_blade(b);
		bcoms_flush();
		
		// init the kowhai module for this blade
		blades[b].addr = b;
		blades[b].pid = KBLADE_PID_NOT_PRESENT; // prove me wrong
		blades[b].kowh = bkow_init(kblade_read, TRANSFER_BUF_SIZE, &blades[b], kblade_write, TRANSFER_BUF_SIZE, &blades[b]);
		if (blades[b].kowh == NULL)
		{
			*mask &= ~(1 << b); // no kowhai = no blade
			///@todo we could add a hook here for b2s to take over for really old blades (tlsx only)
			continue;
		}

		// figure out what we are talking to
		// older bootloaders do not support the get pid command so if a device is present, but
		// does not have the pid function we will assume it is a older bootloader
		blades[b].pid = KBLADE_PID_BOOTLOAER;
		if (kblade_get_product_id(b, &pid) >= 0)
			blades[b].pid = (enum kblade_pid)pid;

		// do unique devices stuff
		switch (blades[b].pid)
		{
			case KBLADE_PID_TLSX:
				break;
			case KBLADE_PID_FVA:
				kblade_pull_settings(b);
				break;
			case KBLADE_PID_BOOTSTRAP:
			case KBLADE_PID_BOOTLOAER:
			default:
				break;
		}
	}

	return 0;
}

void kblade_deinit(uint16_t mask)
{
	int b; 

	for (b = 0; b < MAX_BLADES; b++)
	{
		if ((mask & (1 << b)) == 0)
			continue;

		bkow_free_tree(&blades[b].st);
		if (blades[b].st_buf != NULL)
			free(blades[b].st_buf);
		blades[b].st_buf_len = 0;

		bkow_deinit(blades[b].kowh);
	}

	bcoms_deinit();
}

