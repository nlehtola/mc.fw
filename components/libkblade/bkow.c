/**
 * Helper functions to manage the kowhai interface
 *
 * we should probably push most of this upstream to kowhai or something
 *
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

#include <kowhai.h>
#include <kowhai_protocol.h>

#include <bkow.h>

#ifndef packed
#define packed(type) type __attribute__ ((packed))      		// pack this object (ie struct etc)
#endif

#define WTIMEOUT (3.0f)
#define RTIMEOUT (3.0f)


// simple handle to hold all our state info
struct bkow_t 
{
	// io
	bkow_read_t read;
	int read_pkt_size;
	void *read_buf;	
	void *read_param;
	bkow_write_t write;
	int write_pkt_size;
	void *write_buf;
	void *write_param;

	// kowhai objs
	struct kowhai_protocol_t prot;

	// cached values
	uint32_t version;
	char *syms_buf;
	char **syms_tbl;
	int syms_tbl_len;
	struct kowhai_protocol_id_list_item_t *func_list;
	int func_list_len;
	struct kowhai_protocol_id_list_item_t *tree_list;
	int tree_list_len;
};


static int bkow_write_pkt(bkow_t *h, void *buf, int len, double timeout)
{
	int r;
	uint8_t *_buf = buf;
	time_t start_time;
	int count = 0;

	// sanity checks
	if (len > h->write_pkt_size)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: write called with invalid len [%d]\n", __func__,  __LINE__, len);
		return -1;
	}
	if (h->write == NULL)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: write called, but NULL\n", __func__,  __LINE__);
		return -1;
	}

	// put a 'k' out the front for kowhai packet
	// note buf was alloced to be write_pkt_size + 1 but index 1 used
	// for the kowhai buffers, so we know there really is a index -1
	_buf[-1] = 'k';

	// try to send
	time(&start_time);
	while ((r = h->write(&_buf[-1], h->read_pkt_size + 1, h->write_param)) < 0)
	{
		count++;
		#ifndef NO_TIMEOUTS
		if (timeout >= 0)
		{
			double dt = difftime(time(NULL), start_time);
			if (dt > timeout)
			{
				printf("ERROR: "__FILE__ ":%s:[%d]: timed out (dt = %lf) after %d retires\n", __func__,  __LINE__, dt, count);
				break;
			}
		}
		#endif
	}

	return r;
}


static int bkow_read_pkt(bkow_t *h, void *buf, int len, double timeout)
{
	int r;
	uint8_t *_buf = buf;
	time_t start_time;
	int count = 0;
	
	// sanity checks
	if (len > h->read_pkt_size)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: read called with invalid len [%d]\n", __func__,  __LINE__, len);
		return -1;
	}
	if (h->read == NULL)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: read called, but NULL\n", __func__,  __LINE__);
		return -1;
	}

	// try to read
	time(&start_time);
	while ((r = h->read(&_buf[-1], h->write_pkt_size + 1, h->read_param)) < 0)
	{
		count++;
		#ifndef NO_TIMEOUTS
		if (timeout >= 0)
		{
			double dt = difftime(time(NULL), start_time);
			if (dt > timeout)
			{
				printf("ERROR: "__FILE__ ":%s:[%d]: timed out (dt = %lf) after %d retries [%d]\n", __func__,  __LINE__, dt, count, r);
				break;
			}
		}
		#endif
		// reads often follow writes, if the firmware fails to respond it is likely busy doing something
		// like responding to a write, this gives the firmware some time to complete the request so this
		// read does not spam it to death
		usleep(100000);
	}

	// check for kowhai pack and strip 'k'
	// note buf was alloced to be read_pkt_size + 1 but index 1 used
	// for the kowhai buffers, so we know there really is a index -1
	if (_buf[-1] != 'k')
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: bcoms_read(...) = %d gave buf[-1] == \\x%.2x != 'k'\n", __func__, __LINE__, r, _buf[-1]);
		return -1;
	}

	return r;
}


static int bkow_get_version(bkow_t *h)
{
	int bytes_required;
	int r;

	memset(h->read_buf, 0, h->read_pkt_size);
	memset(h->write_buf, 0, h->write_pkt_size);

	// request version number
	POPULATE_PROTOCOL_CMD(h->prot, KOW_CMD_GET_VERSION, 0);
	if (kowhai_protocol_create(h->write_buf, h->write_pkt_size, &h->prot, &bytes_required) != KOW_STATUS_OK)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: kowhai_protocol_create() fail\n", __func__,  __LINE__);
		return -1;
	}
	if ((r = bkow_write_pkt(h, h->write_buf, h->write_pkt_size, WTIMEOUT)) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: bkow_write_pkt() fail\n", __func__,  __LINE__);
		return -1;
	}

	// get response (hopefully containing the version number)
	if ((r = bkow_read_pkt(h, h->read_buf, h->read_pkt_size, RTIMEOUT)) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: bkow_read_pkt() fail\n", __func__, __LINE__);
		return -1;
	}
	kowhai_protocol_parse(h->read_buf, r, &h->prot);
	if (h->prot.header.command != KOW_CMD_GET_VERSION_ACK)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: response not KOW_CMD_GET_VERSION_ACK [%d]\n", __func__, __LINE__, h->prot.header.command);
		return -1;
	}
	
	return h->prot.payload.spec.version;
}

static int bkow_get_tree_list(bkow_t *h, struct kowhai_protocol_id_list_item_t **tree_list)
{
	int bytes_required;
	int r;
	size_t tree_list_size = 0;
	int offset = 0;
	int tree_list_count = 0;
	int k;

	// request the tree list
	POPULATE_PROTOCOL_GET_TREE_LIST(h->prot);
	if (kowhai_protocol_create(h->write_buf, h->write_pkt_size, &h->prot, &bytes_required) != KOW_STATUS_OK)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: kowhai_protocol_create() fail\n", __func__, __LINE__);
		return -1;
	}
	if ((r = bkow_write_pkt(h, h->write_buf, h->write_pkt_size, WTIMEOUT)) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: bkow_write_pkt() fail\n", __func__, __LINE__);
		return -1;
	}

	// read trees until we get them all or timeout
	do
	{
		memset(h->read_buf, 0, h->read_pkt_size);
		if ((r = bkow_read_pkt(h, h->read_buf, h->read_pkt_size, RTIMEOUT)) < 0)
		{
			printf("ERROR: "__FILE__ ":%s:[%d]: bkow_read_pkt() fail[%d]\n", __func__, __LINE__, r);
			return -1;
		}
		kowhai_protocol_parse(h->read_buf, r, &h->prot);
		if (h->prot.header.command != KOW_CMD_GET_TREE_LIST_ACK && h->prot.header.command != KOW_CMD_GET_TREE_LIST_ACK_END)
		{
			printf("ERROR: "__FILE__ ":%s:[%d]: h->prot.header.command not right [%d]\n", __func__, __LINE__, h->prot.header.command);
			return -1;
		}

		// copy payload into syms (resizing as needed)
		offset = h->prot.payload.spec.id_list.offset;
		tree_list_size += h->prot.payload.spec.id_list.size;
		*tree_list = realloc(*tree_list, tree_list_size);
		memcpy(((void*)*tree_list) + offset, h->prot.payload.buffer, h->prot.payload.spec.id_list.size);
	}
	while (h->prot.header.command != KOW_CMD_GET_TREE_LIST_ACK_END);

	tree_list_count = h->prot.payload.spec.id_list.list_count;

	return tree_list_count;
}

static int bkow_get_function_list(bkow_t *h, struct kowhai_protocol_id_list_item_t **func_list)
{
	int bytes_required;
	int r;
	size_t func_list_size = 0;
	int offset = 0;
	int func_count = 0;
	int k;

	// request the function list
	POPULATE_PROTOCOL_GET_FUNCTION_LIST(h->prot);
	if (kowhai_protocol_create(h->write_buf, h->write_pkt_size, &h->prot, &bytes_required) != KOW_STATUS_OK)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: kowhai_protocol_create() fail\n", __func__, __LINE__);
		return -1;
	}
	if ((r = bkow_write_pkt(h, h->write_buf, h->write_pkt_size, WTIMEOUT)) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: bkow_write_pkt() fail\n", __func__, __LINE__);
		return -1;
	}

	// read functions until we get them all or timeout
	do
	{
		memset(h->read_buf, 0, h->read_pkt_size);
		if ((r = bkow_read_pkt(h, h->read_buf, h->read_pkt_size, RTIMEOUT)) < 0)
		{
			printf("ERROR: "__FILE__ ":%s:[%d]: bkow_read_pkt() fail[%d]\n", __func__, __LINE__, r);
			return -1;
		}
		kowhai_protocol_parse(h->read_buf, r, &h->prot);
		if (h->prot.header.command != KOW_CMD_GET_FUNCTION_LIST_ACK && h->prot.header.command != KOW_CMD_GET_FUNCTION_LIST_ACK_END)
		{
			printf("ERROR: "__FILE__ ":%s:[%d]: h->prot.header.command not right [%d]\n", __func__, __LINE__, h->prot.header.command);
			return -1;
		}

		// copy payload into syms (resizing as needed)
		offset = h->prot.payload.spec.id_list.offset;
		func_list_size += h->prot.payload.spec.id_list.size;
		*func_list = realloc(*func_list, func_list_size);
		memcpy(((void*)*func_list) + offset, h->prot.payload.buffer, h->prot.payload.spec.id_list.size);
	}
	while (h->prot.header.command != KOW_CMD_GET_FUNCTION_LIST_ACK_END);

	func_count = h->prot.payload.spec.id_list.list_count;
	
	return func_count;
}

int bkow_get_function_details(bkow_t *h, uint16_t function_id, struct kowhai_protocol_function_details_t *function_details)
{
	int bytes_required;
	int r;
	int k;

	// request the function details for function_id
	POPULATE_PROTOCOL_GET_FUNCTION_DETAILS(h->prot, function_id);
	if (kowhai_protocol_create(h->write_buf, h->write_pkt_size, &h->prot, &bytes_required) != KOW_STATUS_OK)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: kowhai_protocol_create() fail\n", __func__, __LINE__);
		return -1;
	}
	if ((r = bkow_write_pkt(h, h->write_buf, h->write_pkt_size, WTIMEOUT)) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: bkow_write_pkt() fail\n", __func__, __LINE__);
		return -1;
	}

	// read the result
	memset(h->read_buf, 0, h->read_pkt_size);
	if ((r = bkow_read_pkt(h, h->read_buf, h->read_pkt_size, RTIMEOUT)) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: bkow_read_pkt() fail[%d]\n", __func__, __LINE__, r);
		return -1;
	}
	kowhai_protocol_parse(h->read_buf, r, &h->prot);
	if (h->prot.header.command != KOW_CMD_GET_FUNCTION_DETAILS_ACK)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: h->prot.header.command not right [%d]\n", __func__, __LINE__, h->prot.header.command);
		return -1;
	}

	// copy payload into function_details
	memcpy(function_details, &(h->prot.payload.spec.function_details), sizeof(struct kowhai_protocol_function_details_t));
	return 0;
}

int bkow_get_descriptor(bkow_t *h, uint16_t tree_id, struct kowhai_node_t **nodes)
{
	int bytes_required;
	int r;
	size_t descriptor_size = 0;
	int offset = 0;
	int k;

	// request the descriptor by tree_id
	POPULATE_PROTOCOL_CMD(h->prot, KOW_CMD_READ_DESCRIPTOR, tree_id);
	if (kowhai_protocol_create(h->write_buf, h->write_pkt_size, &h->prot, &bytes_required) != KOW_STATUS_OK)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: kowhai_protocol_create() fail\n", __func__, __LINE__);
		return -1;
	}
	if ((r = bkow_write_pkt(h, h->write_buf, h->write_pkt_size, WTIMEOUT)) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: bkow_write_pkt() fail\n", __func__, __LINE__);
		return -1;
	}

	// read the descriptor structure until we get all the data or timeout
	do
	{
		memset(h->read_buf, 0, h->read_pkt_size);
		if ((r = bkow_read_pkt(h, h->read_buf, h->read_pkt_size, RTIMEOUT)) < 0)
		{
			printf("ERROR: "__FILE__ ":%s:[%d]: bkow_read_pkt() fail[%d]\n", __func__, __LINE__, r);
			return -1;
		}
		kowhai_protocol_parse(h->read_buf, r, &h->prot);
		if (h->prot.header.command != KOW_CMD_READ_DESCRIPTOR_ACK && h->prot.header.command != KOW_CMD_READ_DESCRIPTOR_ACK_END)
		{
			printf("ERROR: "__FILE__ ":%s:[%d]: h->prot.header.command not right [%d]\n", __func__, __LINE__, h->prot.header.command);
			return -1;
		}

		// copy payload into buffer (resizing as needed)
		offset = h->prot.payload.spec.descriptor.offset;
		descriptor_size += h->prot.payload.spec.descriptor.size;
		*nodes = realloc(*nodes, descriptor_size);
		memcpy(((void*)*nodes) + offset, h->prot.payload.buffer, h->prot.payload.spec.descriptor.size);
	}
	while (h->prot.header.command != KOW_CMD_READ_DESCRIPTOR_ACK_END);
	
	return h->prot.payload.spec.descriptor.node_count;
}

int bkow_pull_data(bkow_t *h, uint16_t tree_id, int path_count, union kowhai_symbol_t *path, void **data)
{
	int bytes_required;
	int r;
	size_t data_size = 0;
	uint16_t offset = 0;
	uint16_t packet_size = 0;
	
	// request the descriptor by tree_id
	POPULATE_PROTOCOL_READ(h->prot, KOW_CMD_READ_DATA, tree_id, path_count, path);
	if (kowhai_protocol_create(h->write_buf, h->write_pkt_size, &h->prot, &bytes_required) != KOW_STATUS_OK)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: kowhai_protocol_create() fail\n", __func__, __LINE__);
		return -1;
	}
	if ((r = bkow_write_pkt(h, h->write_buf, h->write_pkt_size, WTIMEOUT)) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: bkow_write_pkt() fail\n", __func__, __LINE__);
		return -1;
	}

	// read the data structure until we get all the data or timeout
	do
	{
		memset(h->read_buf, 0, h->read_pkt_size);
		if ((r = bkow_read_pkt(h, h->read_buf, h->read_pkt_size, RTIMEOUT)) < 0)
		{
			printf("ERROR: "__FILE__ ":%s:[%d]: bkow_read_pkt() fail[%d]\n", __func__, __LINE__, r);
			return -1;
		}
		kowhai_protocol_parse(h->read_buf, r, &h->prot);
		if (h->prot.header.command != KOW_CMD_READ_DATA_ACK && h->prot.header.command != KOW_CMD_READ_DATA_ACK_END)
		{
			printf("ERROR: "__FILE__ ":%s:[%d]: h->prot.header.command not right [%d]\n", __func__, __LINE__, h->prot.header.command);
			return -1;
		}

		// copy payload into buffer (resizing as needed)
		offset = h->prot.payload.spec.data.memory.offset;
		packet_size = h->prot.payload.spec.data.memory.size;
		data_size += packet_size;
		*data = realloc(*data, data_size);
		memcpy((char *)(*data) + offset, h->prot.payload.buffer, h->prot.payload.spec.data.memory.size);
	}
	while (h->prot.header.command != KOW_CMD_READ_DATA_ACK_END);

	return data_size;
}

int bkow_push_data(bkow_t *h, uint16_t tree_id, int path_count, union kowhai_symbol_t *path, void *data, int data_size)
{
	int bytes_required;
	int r;
	uint16_t offset = 0;
	int overhead;

	while (data_size > 0)
	{
		// build next packet
		POPULATE_PROTOCOL_WRITE(h->prot, KOW_CMD_WRITE_DATA, tree_id, path_count, path, 0, offset, data_size, (char *)data + offset);
		kowhai_protocol_get_overhead(&h->prot, &overhead);
		if (h->prot.payload.spec.data.memory.size > h->write_pkt_size - overhead)
			h->prot.payload.spec.data.memory.size = h->write_pkt_size - overhead;
		else
		{
			POPULATE_PROTOCOL_WRITE(h->prot, KOW_CMD_WRITE_DATA_END, tree_id, path_count, path, 0, offset, data_size, (char *)data + offset);
			kowhai_protocol_get_overhead(&h->prot, &overhead);
		}
		if ((r = kowhai_protocol_create(h->write_buf, h->write_pkt_size, &h->prot, &bytes_required)) != KOW_STATUS_OK)
		{
			printf("ERROR: "__FILE__ ":%s:[%d]: kowhai_protocol_create() fail [%d]\n", __func__, __LINE__, r);
			return -1;
		}

		// send
		if ((r = bkow_write_pkt(h, h->write_buf, h->write_pkt_size, WTIMEOUT)) < 0)
		{
			printf("ERROR: "__FILE__ ":%s:[%d]: bkow_write_pkt() fail [%d]\n", __func__, __LINE__, r);
			return -1;
		}

		// read the result
		memset(h->read_buf, 0, h->read_pkt_size);
		if ((r = bkow_read_pkt(h, h->read_buf, h->read_pkt_size, RTIMEOUT)) < 0)
		{
			printf("ERROR: "__FILE__ ":%s:[%d]: bkow_read_pkt() fail[%d]\n", __func__, __LINE__, r);
			return -1;
		}
		kowhai_protocol_parse(h->read_buf, r, &h->prot);
		if (h->prot.header.command != KOW_CMD_WRITE_DATA_ACK)
		{
			printf("ERROR: "__FILE__ ":%s:[%d]: h->prot.header.command not right [%d]\n", __func__, __LINE__, h->prot.header.command);
			return -1;
		}

		offset += h->prot.payload.spec.data.memory.size;
		data_size -= h->prot.payload.spec.data.memory.size;
	}
	
	return 0;
}

static int bkow_get_symbols(bkow_t *h, char **syms_buf, char ***syms_tbl)
{
	int bytes_required;
	int r;
	size_t syms_buf_size = 0;
	uint16_t offset = 0;
	int syms_count = 0;
	int k;

	// request the symbols table
	POPULATE_PROTOCOL_GET_SYMBOL_LIST(h->prot);
	if (kowhai_protocol_create(h->write_buf, h->write_pkt_size, &h->prot, &bytes_required) != KOW_STATUS_OK)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: kowhai_protocol_create() fail\n", __func__, __LINE__);
		return -1;
	}
	if ((r = bkow_write_pkt(h, h->write_buf, h->write_pkt_size, WTIMEOUT)) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: bkow_write_pkt() fail\n", __func__, __LINE__);
		return -1;
	}

	// read symbols until we get them all or timeout
	do
	{
		memset(h->read_buf, 0, h->read_pkt_size);
		if ((r = bkow_read_pkt(h, h->read_buf, h->read_pkt_size, RTIMEOUT)) < 0)
		{
			printf("ERROR: "__FILE__ ":%s:[%d]: bkow_read_pkt() fail[%d]\n", __func__, __LINE__, r);
			return -1;
		}
		kowhai_protocol_parse(h->read_buf, r, &h->prot);
		if (h->prot.header.command != KOW_CMD_GET_SYMBOL_LIST_ACK && h->prot.header.command != KOW_CMD_GET_SYMBOL_LIST_ACK_END)
		{
			printf("ERROR: "__FILE__ ":%s:[%d]: h->prot.header.command not right [%d]\n", __func__, __LINE__, h->prot.header.command);
			return -1;
		}

		// copy payload into syms (resizing as needed)
		offset = h->prot.payload.spec.string_list.offset;
		syms_buf_size += h->prot.payload.spec.string_list.size;
		*syms_buf = realloc(*syms_buf, syms_buf_size );
		memcpy(*syms_buf + offset, h->prot.payload.buffer, h->prot.payload.spec.string_list.size);
	}
	while (h->prot.header.command != KOW_CMD_GET_SYMBOL_LIST_ACK_END);

	// count the number of symbols we read
	offset = 0;
	while (offset < syms_buf_size)
	{
		syms_count++;
		offset += strlen(*syms_buf + offset) + 1;
	}

	// build the symbols table
	*syms_tbl = realloc(*syms_tbl, syms_count * sizeof(char *));
	offset = 0;
	for (k = 0; k < syms_count; k++)
	{
		(*syms_tbl)[k] = *syms_buf + offset;
		offset += strlen(*syms_buf + offset) + 1;
	}
	
	return syms_count;
}

int bkow_get_cached_functions(bkow_t *h, struct kowhai_protocol_id_list_item_t **func_list)
{
	if (h==NULL)
		return -1;
	if (func_list!=NULL)
		*func_list = h->func_list;
	return h->func_list_len;
}

int bkow_get_cached_trees(bkow_t *h, struct kowhai_protocol_id_list_item_t **tree_list)
{
	if (h==NULL)
		return 0;
	if (h->syms_buf == NULL || h->syms_tbl == NULL || h->syms_tbl_len < 1)
		return 0;
	if (tree_list!=NULL)
		*tree_list = h->tree_list;
	return h->tree_list_len;
}

int bkow_get_cached_symbols(bkow_t *h, char ***symbol_list)
{
	if (h==NULL)
		return 0;
	if (h->syms_buf == NULL || h->syms_tbl == NULL || h->syms_tbl_len < 1)
		return 0;
	if (symbol_list!=NULL)
		*symbol_list = h->syms_tbl;
	
	return h->syms_tbl_len;
}

int bkow_symid(bkow_t *h, const char *sym, int len)
{
	int s;
	for (s = 0; s < h->syms_tbl_len; s++)
	{
		if (strncmp(h->syms_tbl[s], sym, len) == 0 && strlen(h->syms_tbl[s]) == len)
			return s;
	}

	return -1;
}

char * bkow_sym(bkow_t *h, uint16_t symid)
{
	return h->syms_tbl[symid];
}

int bkow_call_func(bkow_t *h, int func_sym_id, void *params, int params_size, void *ret, int ret_size, double read_timeout, double write_timeout)
{
	int bytes_required;
	int r;
	bool res = false; // for now all commands return bool

	memset(h->write_buf, 0, h->write_pkt_size);

	///@todo check params_size will fit in buffer or support multiple packets
	///@todo merge our cmd in to command descriptor from fw so we are future proof

	// call function
	///@todo support multi packet request
	POPULATE_PROTOCOL_CALL_FUNCTION(h->prot, func_sym_id, 0, params_size, params);
	if (kowhai_protocol_create(h->write_buf, h->write_pkt_size, &h->prot, &bytes_required) != KOW_STATUS_OK)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: kowhai_protocol_create() fail\n", __func__, __LINE__);
		return -1;
	}
	if ((r = bkow_write_pkt(h, h->write_buf, h->write_pkt_size, write_timeout) < 0))
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: bkow_write_pkt() fail\n", __func__, __LINE__);
		return -1;
	}

	usleep(100000);

	// get result
	///@todo support multi packet response and merge response into ours
	memset(h->read_buf, 0, h->read_pkt_size);
	if ((r = bkow_read_pkt(h, h->read_buf, h->read_pkt_size, read_timeout)) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: bkow_read_pkt() fail[%d]\n", __func__, __LINE__, r);
		return -1;
	}
	kowhai_protocol_parse(h->read_buf, r, &h->prot);
	if (h->prot.header.command != KOW_CMD_CALL_FUNCTION_RESULT_END)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: prot->header.command not right [0x%.4X]\n", __func__, __LINE__, h->prot.header.command);
		return -1;
	}
	if (h->prot.header.id != func_sym_id)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: prot->header.id not the same as calling function [%d]\n", __func__, __LINE__, h->prot.header.id);
		return -1;
	}
	if (h->prot.payload.spec.function_call.offset != 0 || h->prot.payload.spec.function_call.size != ret_size)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid response (response type may have changed) [o:%d != %d, s:%d != %d]\n", __func__, __LINE__, 
				h->prot.payload.spec.function_call.offset, 0, h->prot.payload.spec.function_call.size, ret_size);
		return -1;
	}
	memcpy(ret, h->prot.payload.buffer, ret_size);

	return 0;
}

int bkow_pull_tree(bkow_t *h, int tree_id, struct bkow_tree_t *tree)
{
	int c;
	union kowhai_symbol_t path[1] = {KOWHAI_SYMBOL(tree_id, 0)};

	// populate the tree (we only need to update the descriptors
	// if this is the first time, ie tree->desc == NULL, otherwise
	// tree is known in advance)
	if (tree->desc == NULL)
	{
		if ((c = bkow_get_descriptor(h, tree_id, &tree->desc)) < 0)
			return c;
		tree->desc_len = c;
	}
	if ((c = bkow_pull_data(h, tree_id, 1, path, &tree->data)) < 0)
		return c;
	tree->data_len = c;

	return 0;
}

int bkow_push_tree(bkow_t *h, int tree_id, struct bkow_tree_t *tree)
{
	int tree_size;
	int tree_nodes;
	int c;
	union kowhai_symbol_t path[1] = {KOWHAI_SYMBOL(tree->desc[0].symbol,0)};

	// send all the data from the root node down
	if ((c = kowhai_get_node_size(tree->desc, &tree_size)) != KOW_STATUS_OK)
		return c;
	if ((c = kowhai_get_node_count(tree->desc, &tree_nodes)) != KOW_STATUS_OK)
		return c;
	if ((c = bkow_push_data(h, tree_id, 1, path, tree->data, tree_size)) < 0)
		return c;

	return 0;
}

struct bkow_tree_t * bkow_realloc_tree(struct bkow_tree_t *tree, int nodes, int max_node_size)
{
	tree->desc_len = nodes * sizeof(struct kowhai_node_t);
	tree->desc = realloc(tree->desc, nodes * sizeof(struct kowhai_node_t));
	if (tree->desc == NULL)
		return NULL;
	tree->data_len = nodes * max_node_size;
	tree->data = realloc(tree->data, nodes * max_node_size);
	if (tree->data == NULL)
		return NULL;

	return tree;
}

int bkow_free_tree(struct bkow_tree_t *tree)
{
	if (tree->desc)
		free(tree->desc);
	tree->desc_len = 0;
	if (tree->data)
		free(tree->data);
	tree->data_len = 0;
}

void bkow_dump_version(bkow_t *h)
{
	printf("server kowhai version = %d\n", h->version);
}


void bkow_dump_tree(bkow_t *h)
{
}


void bkow_dump_syms(bkow_t *h)
{
	int k;
	char **syms_tbl;

	printf("symbol table:\n");
	for (k= 0; k < bkow_get_cached_symbols(h, &syms_tbl); k++)
		printf("\t%.2d: %s\n", k, syms_tbl[k]);
}


bkow_t *bkow_init(bkow_read_t read, int read_pkt_size, void *read_param, bkow_write_t write, int write_pkt_size, void *write_param)
{
	int r;
	struct bkow_t *h;

	h = (struct bkow_t *)malloc(sizeof(*h));
	if (h == NULL)
		return NULL;
	memset(h, 0, sizeof(*h));
	
	// init io callouts, buffers etc
	h->read = read;
	h->read_pkt_size = read_pkt_size - 1;
	h->read_buf = malloc(read_pkt_size);
	if (h->read_buf++ == NULL)
		return NULL;
	h->read_param = read_param;
	h->write = write;
	h->write_pkt_size = write_pkt_size - 1;
	h->write_buf = malloc(write_pkt_size);
	if (h->write_buf++ == NULL)
		return NULL;
	h->write_param = write_param;

	// try for a while to get the version number 
	// this loop is used to poll for the presence of 
	// the kowhai protocol on the target device
	h->version = 0;
	printf(__FILE__": " "%s[%d]: " "get version\n", __func__, __LINE__);
	r = bkow_get_version(h);
	if (r < 0)
		return NULL;
	h->version = r;
		
	// request the symbols (just for humans)
	h->syms_buf = NULL;
	h->syms_tbl = NULL;
	h->syms_tbl_len = 0;
	printf(__FILE__": " "%s[%d]: " "get symbols\n", __func__, __LINE__);
	r = bkow_get_symbols(h, &h->syms_buf, &h->syms_tbl);
	if (r < 0)
		return NULL;
	h->syms_tbl_len = r;

	// request the tree (descriptors and values)
	h->tree_list = NULL;
	h->tree_list_len = 0;
	printf(__FILE__": " "%s[%d]: " "get trees\n", __func__, __LINE__);
	r = bkow_get_tree_list(h, &h->tree_list);
	if (r < 0)
		return NULL;
	h->tree_list_len = r;
	
	// request the functions
	h->func_list = NULL;
	h->func_list_len = 0;
	printf(__FILE__": " "%s[%d]: " "get functions\n", __func__, __LINE__);
	r = bkow_get_function_list(h, &h->func_list);
	if (r < 0)
		return NULL;
	h->func_list_len = r;
	
	return h;
}


int bkow_deinit(bkow_t *h)
{
	if (h == NULL)
		return 0;
	if (h->read_buf != NULL)
		free(h->read_buf - 1);
	h->read_buf = NULL;
	if (h->write_buf != NULL)
		free(h->write_buf - 1);
	h->write_buf = NULL;
	if (h->syms_buf != NULL)
		free(h->syms_buf);
	h->syms_buf = NULL;
	if (h->syms_tbl != NULL)
		free(h->syms_tbl);
	h->syms_tbl = NULL;
	if (h->tree_list != NULL)
		free(h->tree_list);
	h->tree_list = NULL;
	if (h->func_list != NULL)
		free(h->func_list);
	h->func_list = NULL;
	free(h);
	h = NULL;
	
	return 0;
}

