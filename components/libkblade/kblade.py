#!/usr/bin/env python

import sys
import ctypes
import math
import os
import inspect
import atexit

# constant
KBLADE_PID_BOOTSTRAP = 0
KBLADE_PID_TLSX = 1
KBLADE_PID_BOOTLOAER = 2
KBLADE_PID_FVA = 3
KBLADE_PID_NOT_PRESENT = 255

# basic type alias'
cint = ctypes.c_int
cint8_t = ctypes.c_int8
cuint8_t = ctypes.c_uint8
cint16_t = ctypes.c_int16
cuint16_t = ctypes.c_uint16
cint32_t = ctypes.c_int32
cuint32_t = ctypes.c_uint32
cfloat = ctypes.c_float

# load library
libname = "libkblade.so"
thisfile = inspect.getfile(inspect.currentframe())
thisdir = os.path.dirname(os.path.abspath(thisfile))
thisdir = thisdir + '/'
if os.path.exists(thisdir + libname):
	libname = thisdir + libname
libkblade = ctypes.cdll.LoadLibrary(libname)

class kblade(object):
	"""
	Base class for all CS blades supporting the kblade protocol
	(extend this to the appropriate subclass based on pid)
	"""

	def __init__(self, slot = 15):
		self.__slot__ = slot
		self.__get_pid__()
		self.__get_fw_version__()

	@property
	def __ct_slot__(self):
		return cint(self.__slot__)
	
	@property
	def __ct_pid__(self):
		return cuint32_t(self.__pid__)

	def __get_pid__(self):
		self.__pid__ = libkblade.kblade_get_pid(self.__ct_slot__)
	
	def __get_fw_version__(self):
		major = cuint16_t(0)
		minor = cuint16_t(0)
		libkblade.kblade_get_fw_version(self.__ct_slot__, ctypes.byref(major), ctypes.byref(minor))
		self.__major__ = major
		self.__minor__ = minor

	@property
	def slot(self):
		"""
		slot number (offset from case markings by 1 so the first
		slot is 0 and the last is 15 etc)
		"""
		return self.__ct_slot__.value

	@property
	def pid(self):
		"""
		numerical value indicating the type of blade this is
		"""
		return self.__ct_pid__.value

	@property
	def product(self):
		"""
		string indicating the type of blade this is
		"""
		if self.pid == KBLADE_PID_BOOTSTRAP:
			return "BOOTSTRAP"
		elif self.pid == KBLADE_PID_TLSX:
			return "TLSX"
		elif self.pid == KBLADE_PID_BOOTLOAER:
			return "BOOTLOADER"
		elif self.pid == KBLADE_PID_FVA:
			return "FVA"
		elif self.pid == KBLADE_PID_NOT_PRESENT:
			return "UNKNOWN"
		else:
			return "NOT PRESENT"

	@property
	def fw_version(self):
		"""
		return the blade firmware version as a tuple
			(major, minor)
		"""
		return (self.__major__.value, self.__minor__.value)

	def get_power_raw(self, channel):
		_chan_ = cint(channel)
		_act_ = cfloat(0)
		_min_ = cfloat(0)
		_max_ = cfloat(0)
		e = libkblade.kblade_get_power_raw(self.__ct_slot__, _chan_, ctypes.byref(_act_), ctypes.byref(_min_), ctypes.byref(_max_))
		if e < 0:
			raise Exception("kblade_get_power_raw() returned %d" % e)
		return {'actual':_act_.value, 'min':_min_.value, 'max':_max_.value}

	def get_power(self, channel):
		_chan_ = cint(channel)
		_set_ = cfloat(0)
		_act_ = cfloat(0)
		_def_ = cfloat(0)
		_min_ = cfloat(0)
		_max_ = cfloat(0)
		e = libkblade.kblade_get_power(self.__ct_slot__, _chan_, ctypes.byref(_set_), ctypes.byref(_act_), ctypes.byref(_def_), ctypes.byref(_min_), ctypes.byref(_max_))
		if e < 0:
			raise Exception("kblade_get_power() returned %d" % e)
		return {'set':_set_.value, 'actual':_act_.value, 'default':_def_.value, 'min':_min_.value, 'max':_max_.value}

	def get_dark_power(self, channel):
		_chan_ = cint(channel)
		_set_ = cfloat(0)
		_min_ = cfloat(0)
		_max_ = cfloat(0)
		_raw_ = cfloat(0)
		e = libkblade.kblade_get_dark_power(self.__ct_slot__, _chan_, ctypes.byref(_set_), ctypes.byref(_min_), ctypes.byref(_max_), ctypes.byref(_raw_))
		if e < 0:
			raise Exception("kblade_get_dark_power() returned %d" % e)
		return {'set':_set_.value, 'min':_min_.value, 'max':_max_.value, 'raw': _raw_.value}

	def set_dark_power(self, channel, power):
		_chan_ = cint(channel)
		_power_ = cfloat(power)
		e = libkblade.kblade_set_dark_power(self.__ct_slot__, _chan_, _power_)
		if e < 0:
			raise Exception("kblade_set_dark_power() returned %d" % e)

	def get_dark_power_time_remaining(self, channel):
		_chan_ = cint(channel)
		_time_ = cfloat(0)
		e = libkblade.kblade_get_dark_power_time_remaining(self.__ct_slot__, _chan_, ctypes.byref(_time_))
		if e < 0:
			raise Exception("kblade_get_dark_power_time_remaining() returned %d" % e)
		return {'act':_time_.value}

        def start_dark_power_measurement(self, channel):
		_chan_ = cint(channel)
		e = libkblade.kblade_start_dark_power_measurement(self.__ct_slot__, _chan_)
		if e < 0:
			raise Exception("kblade_start_dark_power_measurement() returned %d" % e)
		return
		
	def set_power(self, channel, power):
		_chan_ = cint(channel)
		_power_ = cfloat(power)
		e = libkblade.kblade_set_power(self.__ct_slot__, _chan_, _power_)
		if e < 0:
			raise Exception("kblade_set_power() returned %d" % e)

        def get_power_averaging_time(self, channel):
		_chan_ = cint(channel)
		_set_ = cfloat(0)
		_def_ = cfloat(0)
		_min_ = cfloat(0)
		_max_ = cfloat(0)
		e = libkblade.kblade_get_power_averaging_time(self.__ct_slot__, _chan_, ctypes.byref(_set_), ctypes.byref(_def_), ctypes.byref(_min_), ctypes.byref(_max_))
		if e < 0:
			raise Exception("kblade_get_power_averaging_time() returned %d" % e)
		return {'set':_set_.value, 'default':_def_.value, 'min':_min_.value, 'max':_max_.value}
        
	def set_power_averaging_time(self, channel, time):
		_chan_ = cint(channel)
		_time_ = cfloat(time)
		e = libkblade.kblade_set_power_averaging_time(self.__ct_slot__, _chan_, _time_)
		if e < 0:
			raise Exception("kblade_set_power_averaging_time() returned %d" % e)

	def get_attenuation_raw(self, channel):
		_chan_ = cint(channel)
		_set_ = cfloat(0)
		_act_ = cfloat(0)
		_min_ = cfloat(0)
		_max_ = cfloat(0)
		e = libkblade.kblade_get_attenuation_raw(self.__ct_slot__, _chan_, ctypes.byref(_set_), ctypes.byref(_act_), ctypes.byref(_min_), ctypes.byref(_max_))
		if e < 0:
			raise Exception("kblade_get_attenuation_raw() returned %d" % e)
		return {'set':_set_.value, 'actual':_act_.value, 'min':_min_.value, 'max':_max_.value}
	
	def get_attenuation(self, channel):
		_chan_ = cint(channel)
		_set_ = cfloat(0)
		_act_ = cfloat(0)
		_def_ = cfloat(0)
		_min_ = cfloat(0)
		_max_ = cfloat(0)
		e = libkblade.kblade_get_attenuation(self.__ct_slot__, _chan_, ctypes.byref(_set_), ctypes.byref(_act_), ctypes.byref(_def_), ctypes.byref(_min_), ctypes.byref(_max_))
		if e < 0:
			raise Exception("kblade_get_attenuation() returned %d" % e)
		return {'set':_set_.value, 'actual':_act_.value, 'default':_def_.value, 'min':_min_.value, 'max':_max_.value}
	
	def set_attenuation_raw(self, channel, attenuation):
		_chan_ = cint(channel)
		_attenuation_ = cfloat(attenuation)
		e = libkblade.kblade_set_attenuation_raw(self.__ct_slot__, _chan_, _attenuation_)
		if e < 0:
			raise Exception("kblade_set_attenuation_raw() returned %d" % e)

	def set_attenuation(self, channel, attenuation):
		_chan_ = cint(channel)
		_attenuation_ = cfloat(attenuation)
		e = libkblade.kblade_set_attenuation(self.__ct_slot__, _chan_, _attenuation_)
		if e < 0:
			raise Exception("kblade_set_attenuation() returned %d" % e)

	def get_attenuation_offset(self, channel):
		_chan_ = cint(channel)
		_offset_ = cfloat(0)
		e = libkblade.kblade_get_attenuation_offset(self.__ct_slot__, _chan_, ctypes.byref(_offset_))
		if e < 0:
			raise Exception("kblade_get_attenuation_offset() returned %d" % e)
		return {'offset':_offset_.value}

	def set_attenuation_offset(self, channel, offset):
		_chan_ = cint(channel)
		_offset_ = cfloat(offset)
		e = libkblade.kblade_set_attenuation_offset(self.__ct_slot__, _chan_, _offset_)
		if e < 0:
			raise Exception("kblade_set_attenuation_offset() returned %d" % e)
		pass
	
	def get_attenuator_temperature(self, channel):
		_chan_ = cint(channel)
		_act_ = cfloat(0)
		_min_ = cfloat(0)
		_max_ = cfloat(0)
		e = libkblade.kblade_get_attenuator_temperature(self.__ct_slot__, _chan_, ctypes.byref(_act_), ctypes.byref(_min_), ctypes.byref(_max_))
		if e < 0:
			raise Exception("kblade_get_attenuator_temperature() returned %d" % e)
		return {'actual':_act_.value, 'min':_min_.value, 'max':_max_.value}
	
	def get_attenuation_velocity(self, channel):
		_chan_ = cint(channel)
		_set_ = cfloat(0)
		_act_ = cfloat(0)
		_def_ = cfloat(0)
		_min_ = cfloat(0)
		_max_ = cfloat(0)
		e = libkblade.kblade_get_attenuation_velocity(self.__ct_slot__, _chan_, ctypes.byref(_set_), ctypes.byref(_act_), ctypes.byref(_def_), ctypes.byref(_min_), ctypes.byref(_max_))
		if e < 0:
			raise Exception("kblade_get_attenuation_velocity() returned %d" % e)
		return {'set':_set_.value, 'actual':_act_.value, 'default':_def_.value, 'min':_min_.value, 'max':_max_.value}
	
	def set_attenuation_velocity(self, channel, velocity):
		_chan_ = cint(channel)
		_velocity_ = cfloat(velocity)
		e = libkblade.kblade_set_attenuation_velocity(self.__ct_slot__, _chan_, _velocity_)
		if e < 0:
			raise Exception("kblade_set_attenuation_velocity() returned %d" % e)

	def get_wavelength(self, channel):
		_chan_ = cint(channel)
		_set_ = cfloat(0)
		_def_ = cfloat(0)
		_min_ = cfloat(0)
		_max_ = cfloat(0)
		e = libkblade.kblade_get_wavelength(self.__ct_slot__, _chan_, ctypes.byref(_set_), ctypes.byref(_def_), ctypes.byref(_min_), ctypes.byref(_max_))
		if e < 0:
			raise Exception("kblade_get_attenuation() returned %d" % e)
		return {'set':_set_.value, 'default':_def_.value, 'min':_min_.value, 'max':_max_.value}
	
	def set_wavelength(self, channel, wavelength):
		_chan_ = cint(channel)
		_wavelength_ = cfloat(wavelength)
		e = libkblade.kblade_set_wavelength(self.__ct_slot__, _chan_, _wavelength_)
		if e < 0:
			raise Exception("kblade_set_wavelength() returned %d" % e)
	
	def get_ctrl_mode(self, channel):
		_chan_ = cint(channel)
		_mode_ = cint(0)
		e = libkblade.kblade_get_ctrl_mode(self.__ct_slot__, _chan_, ctypes.byref(_mode_))
		if e < 0:
			raise Exception("kblade_get_mode() returned %d" % e)
		if _mode_.value == 0:
			return "attenuation"
		elif _mode_.value == 1:
			return "power"
		else:
			return "unkown"
	
	def set_ctrl_mode(self, channel, mode):
		_chan_ = cint(channel)
		if mode.lower() == "attenuation":
			_mode_ = cint(0)
		elif mode.lower() == "power":
			_mode_ = cint(1)
		else:
			raise Exception("kblade_set_mode() invalid mode, must be attenuation or power)")
		e = libkblade.kblade_set_ctrl_mode(self.__ct_slot__, _chan_, _mode_)
		if e < 0:
			raise Exception("kblade_set_mode() returned %d" % e)

        def get_locked(self, channel):
            	_chan_ = cint(channel)
		_attenuation_ = cuint8_t(0)
		_power_ = cuint8_t(0)
		e = libkblade.kblade_get_locked(self.__ct_slot__, _chan_, ctypes.byref(_attenuation_), ctypes.byref(_power_))
		if e < 0:
			raise Exception("kblade_get_locked() returned %d" % e)
		return {'attenuation': _attenuation_.value, 'power': _power_.value}
	
	def get_setting(self, path, pull = False):
		dst = ctypes.create_string_buffer(10240) # make a 10K buffer for the string (that should be heaps !)
		_path = ctypes.c_char_p(path)
		if pull:
			_pull = cint(1)
		else:
			_pull = cint(0)
		e = libkblade.kblade_get_setting_str(self.__ct_slot__, _path, len(path), dst, len(dst), _pull)
		if e < 0:
			raise Exception("kblade get setting error %d" % e)
		return dst.value

	def set_setting(self, path, values, push = False):
		_src = ctypes.c_char_p(values)
		_path = ctypes.c_char_p(path)
		if push:
			_push = cint(1)
		else:
			_push = cint(0)
		e = libkblade.kblade_set_setting_str(self.__ct_slot__, _path, len(path), _src, len(values), _push)
		if e < 0:
			raise Exception("kblade set setting error %d" % e)
	
	def load_settings_from_rom(self):
		"""
		ask the blade to load the settings from rom into ram so that we
                can work with the committed settings
		"""
		e = libkblade.kblade_load_settings_from_rom(self.__ct_slot__)
		if e < 0:
			raise Exception("kblade load_settings_from_rom error %d" % e)
	
	def load_factory_defaults(self):
		"""
		ask the blade to load the settings from rom into ram so that we
                can work with the committed settings
		"""
		e = libkblade.kblade_load_factory_defaults(self.__ct_slot__)
		if e < 0:
			raise Exception("kblade load_factory_defaults error %d" % e)
	
	def commit(self):
		"""
		commit any changes in the blades settings permanently to flash 
		so that they will be preserved on power cycles
		"""
		e = libkblade.kblade_commit_settings(self.__ct_slot__)
		if e < 0:
			raise Exception("kblade commit settings error %d" % e)
	
	def commit_factory_defaults(self):
		"""
                commit the current working settings table to the factory defaults
                these are likely to be loaded by the user at a later date if they
                ruin their settings and need to get it back to a good state, see
                load_factory_defaults()
		"""
		e = libkblade.kblade_commit_factory_defaults(self.__ct_slot__)
		if e < 0:
			raise Exception("kblade commit factory defaults error %d" % e)
	
	def reboot(self, pid = 0):
		"""
		reboot the blade (loose any uncommitted settings and reinit hw)
		"""
		_pid_ = cint32_t(pid)
		e = libkblade.kblade_reboot(self.__ct_slot__, _pid_)
		if e < 0:
			raise Exception("kblade reboot error %d" % e)
	
	def _flush_(self):
		"""
		this is a method brought out only for debug and may change
		but is quite useful for checking if buffers get stuck
		"""
		e = libkblade.kblade_flush(self.__ct_slot__)
		if e < 0:
			raise Exception("kblade _flush_ error %d" % e)

	def load_settings(self, src):
		"""
		load settings from a string and push the new settings
		to the blade firmware
		"""
		st = ctypes.c_char_p(src)
		e = libkblade.kblade_set_settings(self.__ct_slot__, st, len(st.value))
		if e != 0:
			raise Exception("kblade load settings error %d" % e)

	def load_settings_file(self, filename):
		"""
		load the settings from a file into this blade, note a 
		commit is still required before the settings will be
		permanently stored in the blade's memory
		"""
		self.load_settings(open(filename).read())

	def save_settings(self, pull=False):
		"""
		return the settings in this blades firmware as a string

		pull is optional, if given the firmware is polled for a copy
		of the latest settings (if not given it defaults to False)
		"""
		if pull:
			libkblade.kblade_pull_settings(self.__ct_slot__)
		st = libkblade.kblade_get_settings(self.__ct_slot__, None)
		if ctypes.c_int(st).value == 0:
			raise Exception("kblade save settings error (st = NULL)")
		return ctypes.c_char_p(st).value
	
	def save_settings_file(self, filename, pull=False):
		"""
		save this blades settings to a file which can be
		loaded back in later
		"""
		open(filename, 'w').write(self.save_settings(pull))

__mask__ = 0
def __kblade_deinit__():
	libkblade.kblade_deinit(__mask__)
	
def find_blades(mask=0xffff):
	"""
	find_blades, returns a list of blades found in the system
	as a list where index 0 is in slot 1, index 1 is slot 2,
	... up to index 15 is slot 16 (this is a benchtop slot)

	if blade is present a blade object is returned in that index
	else None is in that index
	"""
	blades = [None] * 16
	__mask__ = cint(mask)
	libkblade.kblade_init(ctypes.byref(__mask__))
	atexit.register(__kblade_deinit__)
	mask = __mask__.value
	for k in range(0,16):
		if (1 << k) & mask:
			blades[k] = kblade(k)
	return blades

if __name__ == "__main__":
	# simple unit test
	blades = find_blades(mask=0x8000)
	for b in blades:
		if b:
			print "%s, %d.%d" % (b.product, b.fw_version[0], b.fw_version[1])

	blades[15].save_settings_file("settings.js")
