/**
 * @brief interface to the kblade modules
 *
 * @author othane
 *
 * @date Feb 2014
 *
 */


#ifndef __KBLADE_H__
#define __KBLADE_H__

#include <stdint.h>

#define MAX_BLADES (16)

enum kblade_pid
{
	KBLADE_PID_BOOTSTRAP = 0,
	KBLADE_PID_TLSX = 1,
	KBLADE_PID_BOOTLOAER = 2,
	KBLADE_PID_FVA = 3,

	KBLADE_PID_NOT_PRESENT = 255,
};

int kblade_get_fw_version(int blade_id, uint16_t *major, uint16_t *minor);

int kblade_get_product_id(int blade_id, uint32_t *pid);

int kblade_reboot(int blade_id, enum kblade_pid pid);

int kblade_load_settings_from_rom(int blade_id);

int kblade_load_factory_defaults(int blade_id);

int kblade_commit_settings(int blade_id);

int kblade_commit_factory_defaults(int blade_id);

int kblade_get_power_raw(int blade_id, int channel, float *actual, float *min, float *max);

int kblade_get_power(int blade_id, int channel, float *set, float *actual, float *def, float *min, float *max);

int kblade_set_power(int blade_id, int channel, float power);

int kblade_get_dark_power(int blade_id, int channel, float *set, float *min, float *max, float *raw);

int kblade_set_dark_power(int blade_id, int channel, float pdark);

int kblade_get_dark_power_time_remaining(int blade_id, int channel, float *time);

int kblade_start_dark_power_measurement(int blade_id, int channel);

int kblade_get_power_averaging_time(int blade_id, int channel, float *set, float *def, float *min, float *max);

int kblade_set_power_averaging_time(int blade_id, int channel, float time);

int kblade_get_attenuation_raw(int blade_id, int channel, float *set, float *actual, float *min, float *max);

int kblade_get_attenuation(int blade_id, int channel, float *set, float *actual, float *def, float *min, float *max);

int kblade_set_attenuation_raw(int blade_id, int channel, float attenuation);

int kblade_set_attenuation(int blade_id, int channel, float attenuation);

int kblade_get_attenuation_offset(int blade_id, int channel, float *offset);

int kblade_set_attenuation_offset(int blade_id, int channel, float offset);

int kblade_get_attenuation_velocity(int blade_id, int channel, float *set, float *actual, float *def, float *min, float *max);

int kblade_set_attenuation_velocity(int blade_id, int channel, float velocity);

int kblade_get_attenuator_temperature(int blade_id, int channel, float *actual, float *min, float *max);

int kblade_get_wavelength(int blade_id, int channel, float *set, float *def, float *min, float *max);

int kblade_set_wavelength(int blade_id, int channel, float wavelength);

enum kblade_ctrl_mode_t
{
	KBLADE_CTRL_MODE_ATTENUATION = 0,
	KBLADE_CTRL_MODE_POWER = 1,
};

int kblade_get_ctrl_mode(int blade_id, int channel, enum kblade_ctrl_mode_t *mode);

int kblade_set_ctrl_mode(int blade_id, int channel, enum kblade_ctrl_mode_t mode);

int kblade_get_locked(int blade_id, int channel, uint8_t *attenuation_locked, uint8_t *power_locked);

int kblade_push_settings(int blade_id);

int kblade_pull_settings(int blade_id);

int kblade_set_settings(int blade_id, char * settings, int len);

char * kblade_get_settings(int blade_id, const char * filename);

int kblade_init(uint16_t *mask);

void kblade_deinit(uint16_t mask);

int kblade_get_setting(int blade_id, const char * path, int path_len, void *dst, int dst_len, int pull);

int kblade_get_setting_str(int blade_id, const char * path, int path_len, char *dst, int dst_len, int pull);

int kblade_set_setting(int blade_id, const char * path, int path_len, void *src, int src_len, int push);

int kblade_set_setting_str(int blade_id, const char * path, int path_len, char *src, int src_len, int push);

#endif
