/*  
 * File Name     : b2s_unit_test.c
 * Author        : Usama Malik
 * Date Created  : 12/02/2014
 * Description: 
 * Contains test cases for echo, temperature and laser on/off commands. 
 *
 * Copyright 2013, Southern Photonics/Coherent Solutions. www.southernphotonics.com
 * All Rights Reserved.
 */

#include "b2s_wrapper.h"

void run_tests(uint8_t);
void run_function(uint8_t blade_id, uint8_t laser, uint8_t command, uint32_t user_data);

typedef enum {false, true} bool_t;

int  print_status(uint8_t);


/**
 * @param [in] i_status
 *               ACK.
 * @returns:     0 for success, 1 for a failure. 
 * @brief: 
 **/

int print_status(uint8_t i_status){

  switch(i_status){    

    
  case 128:
    printf("Status: No error.\n");
    break;
  case 129:
    printf("Status: Blade not present.\n");
	break;
  case 130:
    printf("Status: Laser not present.\n");
	break;
  case 131:
    printf("Status: Laser stopped responding.\n");
	break;
  case 132:
    printf("Status: SPI bus error.\n");
	break;
  case 133:
    printf("Status: MCU has returned malformed data.\n");
	break;
	/**  
case 8:
    printf("Status: Query header correct. Unknown command.\n");
	break;
  case 9:
    printf("Status: Query header correct. Unknown error.\n");
	break;
  case 10:
    printf("Status: Query header correct. More data.\n");
	break;
  case 11:
    printf("Status: Query incorrect header.\n");
	break;
  case 12:
    printf("Status: Query header correct. Unknown sequence.\n");
	break;
  case 13:
    printf("Status: Query header correct. Incorrect CRC.\n");
	break;

  case 14:
    printf("Status: Frame recognised. MCU RX buffer full..\n");
	break;
    **/
  default:
    printf("Status: Unknown error:%u \n", i_status);
	break;    
}
	
  return 0;

}



void run_tests(uint8_t blade_id){

  
       uint8_t laser =0;
       uint32_t user_data=49;
       

       /*******************************************************************/       
       // Blade Level commands
       /*******************************************************************/       
       laser = 0;
       run_function(blade_id,laser, ECHO, user_data );
	   run_function(blade_id,laser, GET_BLADE_MODEL, user_data );
	   run_function(blade_id,laser, GET_FW_VERSION, user_data );
	   run_function(blade_id,laser, GET_BLADE_STATUS, user_data );
	 
	   /*******************************************************************/       
       // Sensor Level commands
       /*******************************************************************/ 
       // GET Commands------------------------------------------------------	   
       laser = 2;
       run_function(blade_id,laser, GET_POWER_MIN, user_data );
       run_function(blade_id,laser, GET_POWER_MAX, user_data );
       run_function(blade_id,laser, GET_POWER_REQUESTED, user_data );
       run_function(blade_id,laser, GET_POWER_ACTUAL, user_data );
	   run_function(blade_id,laser, GET_FREQUENCY_MIN, user_data );
	   run_function(blade_id,laser, GET_FREQUENCY_MAX, user_data );
	   run_function(blade_id,laser, GET_FREQUENCY_REQUESTED, user_data );
	   run_function(blade_id,laser, GET_FREQUENCY_LOCKED, user_data );
	   run_function(blade_id,laser, GET_GRID_MIN, user_data );
	   run_function(blade_id,laser, GET_GRID_MAX, user_data );
	   run_function(blade_id,laser, GET_GRID, user_data );
	   run_function(blade_id,laser, GET_FINE_FREQUENCY_MIN, user_data );
	   run_function(blade_id,laser, GET_FINE_FREQUENCY_MAX, user_data );
	   run_function(blade_id,laser, GET_FINE_FREQUENCY, user_data );
	   run_function(blade_id,laser, GET_CONTROL_DITHER, user_data );
/**	   run_function(blade_id,laser, GET_TEMPERATURE, user_data );
	   run_function(blade_id,laser, GET_SBS_RATE_MIN, user_data );
	   run_function(blade_id,laser, GET_SBS_RATE_MAX, user_data );
	   run_function(blade_id,laser, GET_SBS_RATE, user_data );
	   run_function(blade_id,laser, GET_SBS_FREQUENCY_MIN, user_data );
	   run_function(blade_id,laser, GET_SBS_FREQUENCY_MAX, user_data );
	   run_function(blade_id,laser, GET_SBS_FREQUENCY, user_data );
	   run_function(blade_id,laser, GET_SBS_AMPLITUDE_MIN, user_data );
	   run_function(blade_id,laser, GET_SBS_AMPLITUDE_MAX, user_data );
	   run_function(blade_id,laser, GET_SBS_AMPLITUDE, user_data );
	   run_function(blade_id,laser, GET_LASER_ON_OFF, user_data );
	   run_function(blade_id,laser, GET_LASER_STATUS, user_data );
	   run_function(blade_id,laser, GET_SBS_DITHER, user_data );
**/
	   // SET Commands------------------------------------------------------
}

void run_function(uint8_t blade_id, uint8_t laser, uint8_t command, uint32_t user_data){

       uint8_t tx_frame[16]= {0};
       uint8_t rx_frame[16] = {0};
       int i,j;
       uint32_to_uint8a(&user_data, &tx_frame[4],false);       
       tx_frame[0] = blade_id;
       tx_frame[1] = laser;
       tx_frame[2] = 0;//sequence
       tx_frame[3] = command;
       
       //printf("Blade: %u, Laser: %u, Command: %u\n", blade_id, laser, command);
       
       //printf("TX user data: ");
       //for(i=4;i<8;i++)
	 //printf("%x ", tx_frame[i]);
       
       b2s_transaction(tx_frame, rx_frame);
       print_status(rx_frame[3]);
       printf("RX user data: ");
       for(j=4;j<8;j++)
	 printf("%x ",rx_frame[j]);
       printf("\n--------------------------\n");
 

}



/*******************************************************************************
* Main: Send frames based on user input
*******************************************************************************/
int main(int argc, char **argv)
{

       if(argc<2){
	 printf("NUM args:%u, Usage: b2s_basic blade_id\n", argc);
	 exit(0);
       }
       
       b2s_configure();
       run_tests(atoi(argv[1]));
       b2s_unconfigure();
return 0;
}

