/*  
 * File Name     : b2s_wrapper.c
 * Author        : Usama Malik
 * Date Created  : 12/03/2014
 * Description: 
 * Contains several functions that wrap around B2S protocol. 
 *
 * Copyright 2013, Southern Photonics/Coherent Solutions. www.southernphotonics.com
 * All Rights Reserved.
 */

#include "b2s_wrapper.h"
#include <unistd.h>


///@todo interface to set this from the app
int verbose; 


uint8_t static_parameters [] = {
  GET_POWER_MIN,
  GET_POWER_MAX,
  GET_FREQUENCY_MIN,
  GET_FREQUENCY_MAX,
  GET_GRID_MIN,
  GET_GRID_MAX,
  GET_FINE_FREQUENCY_MIN,
  GET_FINE_FREQUENCY_MAX,
  GET_SBS_RATE_MIN,
  GET_SBS_RATE_MAX,
  GET_SBS_FREQUENCY_MIN,
  GET_SBS_FREQUENCY_MAX, 
};



int b2s_transaction(uint8_t* frame_in, uint8_t* frame_out){

  user_data_in ui; 
  user_data_out uo; 
  uint8_t transaction_status=133; 

  uint8_t blade_response = 0; //0 is undefined
  int i; 

  ui.blade_id = frame_in[BLADE_OFFSET];
  ui.laser_address = frame_in[LASER_OFFSET] ;
  ui.sequence  = frame_in[SEQUENCE_OFFSET] ;
  ui.command   = frame_in[COMMAND_OFFSET];
  ui.data[0] = frame_in[DATA_OFFSET];
  ui.data[1] = frame_in[DATA_OFFSET+1];
  ui.data[2] = frame_in[DATA_OFFSET+2];
  ui.data[3] = frame_in[DATA_OFFSET+3];

  b2s_command(&ui, &uo);

  //We retry a few times in case MCU is busy or there is 
  //some other error.
  for(i=0;i<COMMAND_RETRIES;i++){
    blade_response = uo.status;   
    if (blade_response==STATUS_FRAME_ACCEPTED)
      break;
    usleep(37e3);
    b2s_command(&ui, &uo);
  }

  if(blade_response!=STATUS_FRAME_ACCEPTED){
    frame_out[COMMAND_OFFSET] = STATUS_UNKNOWN;
    //no point doing a query as the frame was not accepted.
    return;
  }

  b2s_query(&ui, &uo); 

  //Based on the response we decide what to do. 
  for(i=0; i<QUERY_RETRIES;i++){
    blade_response = uo.status;
    if (blade_response == STATUS_QUERY_OK)
      break;//we are done
    //we need to resend the query to clear pending
    usleep(37e3);
    b2s_query(&ui, &uo); 
  }
  //Now send what ever is the outcome of the above. 
  
  frame_out[BLADE_OFFSET]    = frame_in[BLADE_OFFSET];
  frame_out[LASER_OFFSET]    = frame_in[LASER_OFFSET] ;
  frame_out[SEQUENCE_OFFSET] = frame_in[SEQUENCE_OFFSET];
  frame_out[COMMAND_OFFSET]  = uo.status;
  frame_out[DATA_OFFSET]     = uo.data[0];
  frame_out[DATA_OFFSET+1]   = uo.data[1];
  frame_out[DATA_OFFSET+2]   = uo.data[2];
  frame_out[DATA_OFFSET+3]   = uo.data[3];

  return 0;

}


/*********************************************************************************************/


//This is a wrapper around b2_transaction command. 

int run_blade_command(uint8_t blade_id, 
		      uint8_t laser_id, 
                      uint8_t sequence,
		      uint8_t command,
		      uint8_t* i_data, 
		      uint8_t* o_data, 
		      uint8_t* o_status){

  uint8_t tx_frame[PAYLOAD_SIZE];
  uint8_t rx_frame[PAYLOAD_SIZE];
  uint8_t i; 

  tx_frame[0] = blade_id; 
  tx_frame[1] = laser_id;
  tx_frame[2] = sequence; 
  tx_frame[3] = command;
  for(i=0;i<4;i++)
    tx_frame[4+i] = i_data[i];
  
  //  for(i=0;i<16;i++)
  //  printf("%u ", tx_frame[i]);
  b2s_transaction(tx_frame,rx_frame);

  //  for(i=0;i<16;i++)
  //   printf("%u ", rx_frame[i]);
  // printf("\n");
  *o_status = rx_frame[3];
  for(i=0;i<4;i++)
    o_data[i] = rx_frame[4+i];
  //printf("B2S status: %u\n", *o_status);

  return NO_ERROR;


}//run_blade_command

int get_blade_fw_rev(uint8_t slot_id, uint32_t* major, uint32_t* minor){
  
  uint8_t tx_data[4];
  uint8_t rx_data[4];
  uint8_t status; 
  uint8_t laser = 0;
  uint8_t sequence = 1;
  int i;
  
  //We assume that the blade is present. 

  run_blade_command(slot_id, laser, sequence,GET_FW_MAJOR_VERSION, tx_data, rx_data,&status );

  if(STATUS_QUERY_OK==status)
	  uint8a_to_uint32(rx_data, major, 0);	  
  else
	  *major = 0;

  run_blade_command(slot_id, laser, sequence,GET_FW_MINOR_VERSION, tx_data, rx_data,&status );

  if(STATUS_QUERY_OK==status)
	  uint8a_to_uint32(rx_data, minor, 0);	  
  else
	  *minor = 0;


return NO_ERROR;

}/**get_blade_fw_rev**/
/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/

void get_crc_status(uint8_t blade_id, uint8_t* crc_status){

	uint8_t status;
	uint8_t tx[4] = {0};
	uint8_t rx[4] = {0};

	run_blade_command(blade_id,0,0,GET_CRC_ON_OFF,tx,rx ,&status );
	if(STATUS_QUERY_OK!=status){
		*crc_status = CRC_STATUS_UNKNOWN;
		return;
	}

	if(CRC_STATUS_OFF==rx[0] || CRC_STATUS_ON==rx[0] )
		*crc_status = rx[0];
	else
		*crc_status = CRC_STATUS_UNKNOWN;

	return;
}

/***********************************************************************************/
/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * 
 * 
 **/


void get_rom_parameter(uint8_t blade_id, uint8_t command, uint8_t* rom_data){

	int i=0;
	uint8_t tx[4];
	uint8_t status;

	for(i=0;i<8;i++)
		run_blade_command(blade_id,i,0,command,tx,&rom_data[i*4] ,&status );
	//We don't retry in case of status failure. 

}




	
/************************************************************************/
/**
 * @param [in] blade_id: 
 * @param [out] present: 
 * @returns      0 for success, 1 for a failure. 
 * @brief  
 * Tries to send a few commands to the blade specified. Sets
 * present to indicate success. 
 **/

int find_blade(uint8_t slot_id, uint8_t* present){
  
  uint8_t tx_data[4];
  uint8_t rx_data[4];
  uint8_t status; 
  uint8_t laser = 0;
  uint8_t sequence = 1;
  int i;
  

  *present = 1; /**Assume echo test passed**/

  if(1==verbose)
    printf("Trying ECHO test: %u\n", slot_id);
  /**Try ECHO test**/
  for(i=0;i<4;i++)
    tx_data[i] = (i+1)*slot_id; //fill in some data

   run_blade_command(slot_id, laser, sequence,ECHO, tx_data, rx_data,&status );

  
  /** Now check if the data received is the same as data sent. **/

  for(i=0;i<4;i++)
    if(tx_data[i]!=rx_data[i]){
      *present = 0;
      if(1==verbose)
	printf("tx:%u rx:%u\n", tx_data[i], rx_data[i]);
    }

  
  if(0==*present){
      if(1==verbose)
	printf("blade echo test failed..\n");
    return 1;/**This failed, no point running the second command.**/

  }
  if(1==verbose)
    printf("blade echo test passed..\n");


//This is commented out so that B2SServer can work on all versions of the FW. Version 0.8 returns blade 
//status as a number while the later versions return a bit vector. We can accomodate both but ECHO test
//is good enough for blade detection. 

/**  

  call_status  = run_blade_command(slot_id, laser,sequence,GET_BLADE_STATUS,tx_data, rx_data,&status );
  if(call_status!=NO_ERROR) return call_status;  
  
  if(1==verbose)
    for(i=0;i<4;i++)
      printf("%u ", rx_data[i]);

  if(rx_data[0]!=BLADE_READY){
    if (1==verbose)
      printf("blade status test failed\n");
    *present = 0;
  }

  if(0==*present){
    if(1==verbose){
      printf("Blade status test failed.\n");
      for(i=0;i<4;i++)
	printf("%u ", rx_data[i]);
    }
    return 1;

  }
  if(1==verbose)
    printf("Blade status test passed: %u.\n", *present);
**/
  
return NO_ERROR;

}/**find_blade**/
/*************************************************************************************************************/

/**
 * @param       
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * A laser is considered present when it is not in a DEAD state (Bit4).
 * 
 **/



int find_laser(uint8_t blade_id, uint8_t laser_id, uint32_t fw_major_rev, uint8_t* present){

  uint8_t sequence = 1;
  uint8_t tx_data[4];
  uint8_t rx_data[4];
  uint8_t status; 

  run_blade_command(blade_id,laser_id,sequence, GET_LASER_STATUS,tx_data,rx_data ,&status );
 
  //Older FW has a different notion of laser present

  //printf("Find laser: major version:%u\n", fw_major_rev);
  if (fw_major_rev < 1) {

	  if (1==rx_data[0]){ 
		  *present = 1;
		  //printf("[%u: Y(status=%d)] ", laser_id, rx_data[0]);
	  }
	  else{
		  *present = 0;
		  //printf("[%u: N(status=%d)] ", laser_id, rx_data[0]);
	  }
	  return NO_ERROR;
  }


  if ((rx_data[0] & 0x10) == 0){ 
	  *present = 1;
	  //printf("[%u: Y(status=%d)] ", laser_id, rx_data[0]);
  }
  else{
	  *present = 0;
	  //printf("[%u: N(status=%d)] \n", laser_id, rx_data[0]);
  }

  return NO_ERROR;
}//find_laser


void reboot_blade(int blade, uint8_t pid){
  uint8_t i_data[4] = {0};
  uint8_t o_data[4] = {0};
  uint8_t o_status = 0;

  i_data[0] = pid;
  run_blade_command(blade, 0, 0, RESET, i_data, o_data, &o_status);

  // we will not get a response if the blade is rebooting
}






