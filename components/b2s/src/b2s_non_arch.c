/***********************************************************************************
* Project    : TLSX
************************************************************************************
* File       : 
* Author     : u.malik@coherent-solutions.com
* Company    : Coherent Solutions
* Created    : 
************************************************************************************
* Description: 
* This file contains those B2S function that do not depend on any TS4700 libraries
* for their implementation. 
************************************************************************************
* Copyright 2013, Coherent Solutions www.coherent-solutions.com
* All rights reserved.
************************************************************************************/


#include "b2s.h"

/***********************************************************************************/

/**
 * @param   
 * @returns      0 for success or an error code for a failure. 
 * @brief  
 * Checks if the command is meant to be a read/write operation for the ROM
 **/


int is_rom_command(uint8_t command){


	switch (command)
	{

	case GET_FLASH_VERSION:
	case SET_FLASH_VERSION:
	case GET_SERIAL_NUMBER:
	case SET_SERIAL_NUMBER:
	case GET_MODEL_NUMBER:
	case SET_MODEL_NUMBER:
	case GET_FW_MAJOR_VERSION:
	case GET_FW_MINOR_VERSION:
	case GET_HW_VERSION:
	case SET_HW_VERSION:
	case GET_NUM_LASERS:
	case SET_NUM_LASERS:
	case COMMIT_ST:
		return 1;
	default:
		return 0;

	}

}


