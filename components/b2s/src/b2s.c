/*  
 * File Name     : b2s.c
 * Author        : Usama Malik
 * Date Created  : 5/02/2014
 * Description: 
 * Implementation for the backplane bus system protocol. Details of the protocol 
 * can be found in MasterBladesComms.doc. 
 *
 * Copyright 2013, Southern Photonics/Coherent Solutions. www.southernphotonics.com
 * All Rights Reserved.
 */

#include <libtsctl.h>
#include <Array.h>
#include <crc.h>
#include <time.h>
#include "b2s.h"
#include <stddef.h>

#define CRC_HANDSHAKE_ACK (0x5565)
#define CRC_HANDSHAKE_NAK (0xA9AA)

static DIO *dio;
static SPI *spi;


// 8bit crc C2 used
static uint8_t crc_tbl[256];
static struct crc_h crc_h =
{
  // for design see http://hacromatic.com/blog/2012/08/make-your-serial-protocol-robust-with-8-bit-crc-codes/
  {
    8,     // width
    0x2F,  // poly
    0xFF,  // init (not in ref but 0xff makes good default so crc of 0 buf is not 0)
    FALSE, // refin
    FALSE, // refot
    0,     // xorot

    0,     // working reg (irgnore this in cmp)
  },

  crc_tbl,
  sizeof(crc_tbl),
  
  CRC_METHOD_BEST,
};


#ifndef packed
#define packed(type) type __attribute__ ((packed))  // pack this object (ie struct etc)
#endif

union b2s_transfer_t {
  packed(struct) b2s_cmd_out_t{
    uint8_t type;
    uint8_t crc;
    uint8_t seq;
    uint8_t addr;
    uint8_t cmd;
    uint8_t data[11];
  } cmd_out;
  
  packed(struct) b2s_cmd_in_t{
    uint8_t __reserved__[14];
    uint8_t crc;
    uint8_t status;
  } cmd_in;

  packed(struct) b2s_query_out_t{
    uint8_t type;
    uint8_t seq;
    uint8_t crc;
    uint8_t __reserved__[13];
  } query_out;

  packed(struct) b2s_query_in_t{
    uint8_t __reserved__[9];
    uint8_t seq;
    uint8_t crc;
    uint8_t data[4];
    uint8_t status;
  } query_in;
};


union b2s_transfer_t;
static void _printf_transfer(union b2s_transfer_t *t)
{
  uint8_t *p = (uint8_t *)t;
  uint8_t s = sizeof(*t);

  while (s--)
    printf("\\x%.2X", *p++);
}
#define printf_transfer(t, msg, ...) {printf(msg, ##__VA_ARGS__); _printf_transfer((union b2s_transfer_t *)t); printf("\n");}


#define SPI_SELECT_BIT0 30
#define SPI_SELECT_BIT1 29
#define SPI_SELECT_BIT2 28
#define SPI_SELECT_BIT3 27
static void spi_init(void)
{
  dio = DIOInit(0);
  assert(dio);
  dio->Lock(dio, SPI_SELECT_BIT0, 0);
  dio->Lock(dio, SPI_SELECT_BIT1, 0);
  dio->Lock(dio, SPI_SELECT_BIT2, 0);
  dio->Lock(dio, SPI_SELECT_BIT3, 0);

  spi = SPIInit(0);
  assert(spi);
  spi->Lock(spi, 0, 0);
  spi->ClockSet(spi, 1250000); // go slow to make coms easier atm
  spi->EdgeSet(spi, 0);
}


static void spi_deinit(void)
{
  spi->Unlock(spi, 0, 0);

  spi->Unlock(dio, SPI_SELECT_BIT0, 0);
  spi->Unlock(dio, SPI_SELECT_BIT1, 0);
  spi->Unlock(dio, SPI_SELECT_BIT2, 0);
  spi->Unlock(dio, SPI_SELECT_BIT3, 0);
}


#define MAX(a,b) ((a > b)? a: b)
#define MIN(a,b) ((a < b)? a: b)
#define ADR -1 // I think this says de_cs is 0 and dont do any CS but I am not sure
static int spi_transfer(void *buf_rx, int buf_rx_len, const void *buf_tx, int buf_tx_len)
{
  char *tx = NULL, *rx = NULL;
  int len = MAX(buf_rx_len, buf_tx_len);
  int res = -1;
  len = MAX(len, 2); // len needs to be a minimum of 2 bytes for spi using this lib
  
  // alloc array
  rx = ArrayAlloc(len, 1);
  tx = ArrayAlloc(len, 1);

  // populate the tx buffer
  if (buf_tx != NULL)
    memcpy(tx, buf_tx, buf_tx_len);
  
  // send
  spi->ReadWrite(spi, ADR, tx, rx);
  
  // check the rx buffer and if it is valid return it
  if (buf_rx != NULL)
    memcpy(buf_rx, rx, buf_rx_len);
  
  ArrayFree(rx);
  ArrayFree(tx);
  
  return len;
}


// this is unused on purpose it is a backup slower version of above in case we find that the
// firmware cannot keep up
static int spi_transfer_slow(void *buf_rx, int buf_rx_len, const void *buf_tx, int buf_tx_len)
{
  uint8_t *_buf_rx = (uint8_t *)buf_rx, *_buf_tx = (uint8_t *)buf_tx;
  char *tx = NULL, *rx = NULL;
  int l, len = MAX(buf_rx_len, buf_tx_len);
  int res = -1;
  len = MAX(len, 2); // len needs to be a minimum of 2 bytes for spi using this lib
  
  // alloc array
  rx = ArrayAlloc(2, 1);
  tx = ArrayAlloc(2, 1);
  
  // send all but the last word
  for (l = 0; l < len - 2; l += 2)
  {
    // populate the tx buffer
    if (_buf_tx != NULL)
      memcpy(tx, &((uint8_t *)_buf_tx)[l], 2);
    
    // send
    spi->ReadWrite(spi, ADR, tx, rx);
    
    // check the rx buffer and if it is valid return it
    if (_buf_rx != NULL)
      memcpy(&_buf_rx[l], rx, 2);
    
    // delay for slow firmware
    b2s_spin(REG_TO_REG_DELAY);
  }

  // populate the tx buffer
  if (_buf_tx != NULL)
    memcpy(tx, &_buf_tx[l], len - l);
  
  // send
  spi->ReadWrite(spi, ADR, tx, rx);
  
  // check the rx buffer and if it is valid return it
  if (_buf_rx != NULL)
    memcpy(&_buf_rx[l], rx, len - l);
  
  // delay for slow firmware
  b2s_spin(REG_TO_REG_DELAY);
 
  // done
  ArrayFree(rx);
  ArrayFree(tx);
  return len;
}


#define FRAME_HEADER_SIZE 4
static uint16_t b2s_transfer(int blade, union b2s_transfer_t *t, uint8_t icrc){
  int size;
  uint8_t crc, crc_backup;
  uint8_t *tbuf = (uint8_t *)t;
  uint16_t crc_handshake = CRC_HANDSHAKE_NAK;

  // select the blade and delay for the NSS line to fall
  b2s_select(blade);

  // send the header
  size = FRAME_HEADER_SIZE;
  spi_transfer_slow(t, size, t, size);
  b2s_spin(REG_TO_REG_DELAY);

  // send the rest of the transfer
  size = sizeof(*t) - FRAME_HEADER_SIZE;
  spi_transfer_slow((uint8_t *)t + FRAME_HEADER_SIZE, size,
                    (uint8_t *)t + FRAME_HEADER_SIZE, size);

  // check the inbound crc
  crc_backup = tbuf[icrc];
  tbuf[icrc] = 0;  // assume crc byte is zero for actual calc
  crc = crc_buf(&crc_h, t, sizeof(*t), true);
  tbuf[icrc] = crc_backup; // restore crc
  if (crc == crc_backup)
    crc_handshake = CRC_HANDSHAKE_ACK;

  // do crc handshaking phase
  b2s_spin(CRC_HANDSHAKE_DELAY);
  size = sizeof(crc_handshake);
  spi_transfer_slow(&crc_handshake, size, &crc_handshake, size);

  // unselect this blade and wait for the NSS line to rise
  b2s_unselect();
  b2s_spin(REG_TO_REG_DELAY);

  // only tell the caller handshake passed if we got a ACK from the firmware
  // and we send ACK to the firmware
  if (crc != crc_backup)
  	crc_handshake = CRC_HANDSHAKE_NAK;

  return crc_handshake;
}


int b2s_select(uint8_t i_blade_id){

  bool bit3 = (i_blade_id & 0x08)? 1: 0;
  bool bit2 = (i_blade_id & 0x04)? 1: 0;
  bool bit1 = (i_blade_id & 0x02)? 1: 0;
  bool bit0 = (i_blade_id & 0x01)? 1: 0;

  dio->Set(dio, SPI_SELECT_BIT0, bit0);
  dio->Set(dio, SPI_SELECT_BIT1, bit1);
  dio->Set(dio, SPI_SELECT_BIT2, bit2);
  dio->Set(dio, SPI_SELECT_BIT3, bit3);
  dio->Commit(dio, 0);

  b2s_spin(SELECT_DELAY);

  return 0;
}


#define UNSELECT_BLADE_ID (14)
int b2s_unselect(void){

  b2s_select(UNSELECT_BLADE_ID);

  return 0;
}


enum B2S_FRAME_TYPE
{
	B2S_CMD_TYPE = 0x81,
	B2S_QUERY_TYPE = 0x42,
};

#define _OFFSETOFF(base, param) ((uint32_t)&base.param - (uint32_t)&base)
int b2s_command(user_data_in* ui, user_data_out* uo){ 

  union b2s_transfer_t t __attribute__ ((aligned (4)));
  uint16_t crc_handshake;

  // populate the cmd
  memset(&t, 0, sizeof(union b2s_transfer_t));
  t.cmd_out.type = B2S_CMD_TYPE;
  t.cmd_out.seq = ui->sequence;
  //In case of a ROM command we don't convert the input to a bitmask
  if (is_rom_command(ui->command))
	  t.cmd_out.addr = ui->laser_address;
  else
	  t.cmd_out.addr = 1<<ui->laser_address;
  
  t.cmd_out.cmd = ui->command;
  memcpy(t.cmd_out.data, ui->data, 4);
  t.cmd_out.crc = 0;
  t.cmd_out.crc = crc_buf(&crc_h, &t, sizeof(t), true);

  // send the command
  crc_handshake = b2s_transfer(ui->blade_id, &t, _OFFSETOFF(t.cmd_in, crc));

  // unpack the result
  uo->status = t.cmd_in.status;
  uo->crc_handshake = (crc_handshake == CRC_HANDSHAKE_ACK);
}


int b2s_query(user_data_in* ui, user_data_out* uo){     

  union b2s_transfer_t t __attribute__ ((aligned (4)));;
  uint16_t crc_handshake;

  // populate the query
  memset(&t, 0, sizeof(union b2s_transfer_t));
  t.query_out.type = B2S_QUERY_TYPE;
  t.query_out.seq = ui->sequence;
  t.query_out.crc = 0;
  t.query_out.crc = crc_buf(&crc_h, &t, sizeof(t), true);

  // send the query
  crc_handshake = b2s_transfer(ui->blade_id, &t, _OFFSETOFF(t.query_in, crc));

  // unpack the result
  uo->sequence = t.query_in.seq;
  uo->status = t.query_in.status;
  memcpy(uo->data, t.query_in.data, 4);
  uo->crc_handshake = (crc_handshake == CRC_HANDSHAKE_ACK);
}



void b2s_spin(uint32_t s){

  int i,j;
  for(i=0;i<s*3000;i++)
        j +=i;
  return;
}




void print_command_status(uint8_t c){

  switch (c) {
  case 1:
    printf("Frame recognised.");
    break;
  case 2:
    printf("MCU busy.");
    break;
  default:
    printf("Unknown response:%u", c);
  }//switch
}


void b2s_print_query_status(uint8_t q){

  switch (q) {
  case 5:
    printf("Command executed.");
    break;
  case 6:
    printf("Execution error.");
    break;
  case 7:
    printf("Command pending.");
    break;
  case 8:
    printf("Unknown command.");
    break;
  case 12:
    printf("Unknown sequence number.");
    break;
  case 13:
    printf("Incorrect CRC.");
    break;
  case 15:
    printf("Laser communication error.");
    break;

  default:
    printf("Unknown response:%u",q);
  }//switch
}


int b2s_configure(void){

  crc_init(&crc_h);
  spi_init();
  b2s_unselect();
  b2s_spin(REG_TO_REG_DELAY);
}


int b2s_unconfigure(void){

  b2s_unselect();
  b2s_spin(REG_TO_REG_DELAY);
  spi_deinit();
}
/***********************************************************************************/
