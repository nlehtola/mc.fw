/*  
 * File Name     : b2s_wrapper.h
 * Author        : Usama Malik
 * Date Created  : 12/03/2013
 * Description: 
 * Contains several functions that wrap around B2S protocol. 
 *
 * Copyright 2013, Southern Photonics/Coherent Solutions. www.southernphotonics.com
 * All Rights Reserved.
 */

#ifndef B2S_WRAPPER_H
#define B2S_WRAPPER_H

#include "b2s.h"

#define PAYLOAD_SIZE 16


int b2s_transaction(uint8_t*, uint8_t*);
int run_blade_command(uint8_t,uint8_t,uint8_t,uint8_t,uint8_t*,uint8_t*,uint8_t*);
int find_blade(uint8_t, uint8_t*);
void reboot_blade(int blade, uint8_t pid);
void get_rom_parameter(uint8_t blade_id, uint8_t command, uint8_t* rom_data);

#endif
