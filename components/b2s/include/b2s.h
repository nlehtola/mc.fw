/*  
 * File Name     : b2s.h
 * Author        : Usama Malik
 * Date Created  : 5/02/2013
 * Description: 
 * Definitions and function prototypes for the backplane bus system (B2S) protocol. 
 *
 * Copyright 2013, Southern Photonics/Coherent Solutions. www.southernphotonics.com
 * All Rights Reserved.
 */

#ifndef B2S_H
#define B2S_H

#define DEBUG

#include "common.h"

#define SPI_FRAME_SIZE 8 //8 16-bit words
#define SPI_ADDRESS_SIZE 4
#define SPI_NUM_CMDS_DEFINED 39 //defined in the Master-Blade Comms Protocol

#define DIO_OUTPUT_REG_ADDR 0x6a

#define BLADE_OFFSET 0
#define LASER_OFFSET 1
#define SEQUENCE_OFFSET 2
#define COMMAND_OFFSET 3
#define DATA_OFFSET   4

#define LASER_STATE_INIT 0
#define LASER_STATE_READY 1
#define LASER_STATE_PENDING 2
#define LASER_STATE_MALFUNCTION 3
#define LASER_STATE_DEAD 4
#define LASER_STATE_ALIVE 5
#define LASER_STATE_OVER_TEMP 6


#define   ECHO  0
#define   GET_BLADE_MODEL 1 
#define   GET_FW_VERSION 2
#define   GET_BLADE_STATUS 3
#define   GET_POWER_MIN 4
#define   GET_POWER_MAX 5
#define   SET_POWER 6
#define   GET_POWER_REQUESTED 7
#define   GET_POWER_ACTUAL 8
#define   GET_FREQUENCY_MIN 9
#define   GET_FREQUENCY_MAX 10
#define   SET_FREQUENCY 11
#define   GET_FREQUENCY_REQUESTED 12
#define   GET_FREQUENCY_LOCKED 13
#define   GET_GRID_MIN 14
#define   GET_GRID_MAX 15
#define   SET_GRID 16
#define   GET_GRID 17
#define   GET_FINE_FREQUENCY_MIN 18
#define   GET_FINE_FREQUENCY_MAX 19
#define   SET_FINE_FREQUENCY 20
#define   GET_FINE_FREQUENCY 21
#define   SET_CONTROL_DITHER 22
#define   GET_CONTROL_DITHER 23
#define   GET_TEMPERATURE 24
#define   GET_SBS_RATE_MIN 25 
#define   GET_SBS_RATE_MAX 26
#define   SET_SBS_RATE 27
#define   GET_SBS_RATE 28
#define   GET_SBS_FREQUENCY_MIN 29
#define   GET_SBS_FREQUENCY_MAX 30
#define   SET_SBS_FREQUENCY 31
#define   GET_SBS_FREQUENCY 32
#define   SET_LASER_ON_OFF 37
#define   GET_LASER_ON_OFF 38
#define   RESET 39
#define   GET_LASER_STATUS 40
#define   SET_SBS_DITHER 41
#define   GET_SBS_DITHER 42
#define GET_CRC   43
#define SET_CRC   44

//The following commands are meant to read/write from/to the ROM. These
//commands use the laser field to address the ROM location and hence 
//must be treated as such (i.e. no bit mask in the laser field).
#define GET_FLASH_VERSION   45
#define SET_FLASH_VERSION   46
#define GET_SERIAL_NUMBER   47
#define SET_SERIAL_NUMBER   48
#define GET_MODEL_NUMBER   49
#define SET_MODEL_NUMBER   50
#define GET_FW_MAJOR_VERSION   51
#define GET_FW_MINOR_VERSION   52
#define GET_HW_VERSION   53
#define SET_HW_VERSION   54
#define GET_NUM_LASERS   55
#define SET_NUM_LASERS   56
#define COMMIT_ST   57
#define GET_FCOEFF0  58
#define SET_FCOEFF0  59
#define GET_FCOEFF1  60
#define SET_FCOEFF1  61
#define GET_POWER_MIN_CORRECTION  62
#define SET_POWER_MIN_CORRECTION  63
#define GET_POWER_MAX_CORRECTION  64
#define SET_POWER_MAX_CORRECTION  65


#define SET_SAFETY 110
#define GET_SAFETY 111
#define GET_CRC_ON_OFF 127
#define SET_CRC_ON_OFF 128




#define NUM_STATIC_PARAMETERS 14




//Define the various status replies from blade.

#define STATUS_FRAME_ACCEPTED 1
#define STATUS_MCU_BUSY 2
#define STATUS_UNKNOWN 4

#define STATUS_QUERY_OK 5
#define STATUS_EXEC_ERROR 6
#define STATUS_COMMAND_PENDING 7
#define STATUS_UNKNOWN_COMMAND 8 

#define BLADE_INITIALISING 1
#define BLADE_READY 2

#define REG_TO_REG_DELAY 1
#define SELECT_DELAY 1
#define CRC_HANDSHAKE_DELAY 20
#define POST_FRAME_DELAY 20
#define COMMAND_RETRIES 5
#define QUERY_RETRIES 20 //more retries here as the MCU can be busy talking to the lasers.

#define CRC_STATUS_OFF 0
#define CRC_STATUS_ON 1
#define CRC_STATUS_UNKNOWN 2


typedef struct {

  uint8_t blade_id;
  uint8_t laser_address; 
  uint8_t sequence; 
  uint8_t command; 
  uint8_t data[4]; /**Always 4 bytes. **/
} user_data_in; 


typedef struct {

  uint8_t sequence; /**Only relevant for Query frames. **/
  uint8_t status; 
  uint8_t data[4]; /**Always 4 bytes. **/
  uint8_t crc_handshake;
} user_data_out; 


/**
 * @param [in] i_blade_id: Must be a number between 0-15
 * @returns 0 for success and 1 for a failure. 
 * @brief
 * Sets the SE lines to address the given blade
 **/
int b2s_select(uint8_t);


/**
 * @returns: 0 for success and 1 for a failure. 
 * @brief: 
 * This sets the address to 14 (non-existent blade). 
 **/
int b2s_unselect(void);


/**
 * @param [in]  ui: struct containing blade_id, sequence, address and command.       
 * @param [out] uo: struct containing returned data and status.               
 * @returns 0 for success, 1 for a failure. 
 * @brief 
 * This function prepares the frame to be send over B2S and rectreives the status 
 * byte. The length of i_data is assumed be 4 bytes. The rest of the data bytes are filled
 * with zeros. 
 **/
int b2s_command(user_data_in*, user_data_out*);


/**
 * @param [in] ui: Contains blade id and sequence number. Other fields are ignored. 
 * @param [out] uo: Outputs status and data
 * @returns 0 for success, 1 for a failure. 
 * @brief 
 * This function prepares the frame to be send over S2B and rectreives the ACK 
 * byte. The length of data is assumed be 4 bytes. The rest of the data bytes are filled
 * with zeros. 
 **/
int b2s_query(user_data_in*, user_data_out*);


/**
 * @param s makes the spin longer for bigger s
 * @brief  
 * spin for a very short while 
 **/
void b2s_spin(uint32_t);


/**
 * @param [in] command: command to test
 * @returns: 1 yes, 0 no
 * @brief: 
 * checks if the command specified is a set command or not
 **/
uint8_t b2s_is_set_command(uint8_t);


void b2s_print_command_status(uint8_t);
void b2s_print_query_status(uint8_t);


/**
 * @returns 0 for success, 1 for a failure. 
 * @brief  
 * Configures the SPI clock and sets the direction of the DIO pins that are used for  selecting baldes. 
 **/
int b2s_configure();


/**
 * @returns 0 for success, 1 for a failure. 
 * @brief  
 * Frees/Unlocks all the HW reasources used by b2s (spi, dio, etc) and cleans up so other programs can use them
 **/
int b2s_unconfigure(void);


/**
 * @returns 0 for success, 1 for a failure. 
 * @brief  
 * Checks if the command is meant to be a read/write operation for the ROM
 **/
int is_rom_command(uint8_t);



#endif
