#include <libtsctl.h>
#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <unistd.h>

#define SW_A_MFP (106)
#define SW_B_MFP (105)
#define SW_I_MFP (43)
#define SW_A_DIO (SW_A_MFP + 62)
#define SW_B_DIO (SW_B_MFP + 62)
#define SW_I_DIO (SW_I_MFP + 62)
#define MFP_BANK(x) (x / 32)
#define MFP_MASK(x) (1 << (x - 32*MFP_BANK(x)))  
#define MFP_IRQ "/proc/irq/49/irq"

enum ENCODER_POS
{
	POS_0 = 0,
	POS_1 = 1,
	POS_2 = 2,
	POS_3 = 3,
};


enum ENCODER_SIG
{
	SIG_MOV_POS0 = 0,
	SIG_MOV_POS1 = 1,
	SIG_MOV_POS2 = 2,
	SIG_MOV_POS3 = 3,
};


#define ENCODER_POOL_SIZE 1
static struct encoder_handle
{
	// libtsctl hardware resources
	Bus *mfp_bus;
	DIO *dio;
	Pin *pin;

	// used to poll for irq
	int fd_irq;
	fd_set fds;

	// state info
	enum ENCODER_POS pos;
	enum ENCODER_POS last_pos;
	int ticks;
	int dir;
	struct timeval time;
	struct timeval last_time;
	int dt;
	int button;

	int verbose;
} encoder_pool[ENCODER_POOL_SIZE];
static uint8_t encoder_alloc_index = 0;


static void lock_io(struct encoder_handle *h)
{
	h->dio->Lock(h->dio, SW_A_DIO, 0);
	h->dio->Lock(h->dio, SW_B_DIO, 0);
}


static void unlock_io(struct encoder_handle *h)
{
	h->dio->Unlock(h->dio, SW_A_DIO, 0);
	h->dio->Unlock(h->dio, SW_B_DIO, 0);
}


static uint8_t read_sigs(struct encoder_handle *h)
{
	uint8_t ret = 0;
	int r;

	h->dio->Refresh(h->dio);
	r = h->dio->Get(h->dio, SW_A_DIO);
	ret |= (r == INPUT_HIGH || r == HIGH) ? 0x01: 0;
	r = h->dio->Get(h->dio, SW_B_DIO);
	ret |= (r == INPUT_HIGH || r == HIGH) ? 0x02: 0;
	r = h->dio->Get(h->dio, SW_I_DIO);
	ret |= (r == INPUT_HIGH || r == HIGH) ? 0x10: 0;

	return ret;
}


static void transition(struct encoder_handle *h, enum ENCODER_POS new_pos)
{
	// transition
	h->last_pos = h->pos;
	h->pos = new_pos;
}


static void pos0(struct encoder_handle *h, enum ENCODER_SIG sig)
{
	switch (sig)
	{
		case SIG_MOV_POS1:
			// rotate forwards
			h->dir = 1;
			h->ticks++;
			transition(h, POS_1);
			break;
		case SIG_MOV_POS2:
			// rotate backwards 
			h->dir = -1;
			h->ticks--;
			transition(h, POS_2);
			break;
		case SIG_MOV_POS3:
			// rotated twice, could be += 2 or -= 2 so just
			// ignore the ticks and jump to new state
			h->dir = 0;
			transition(h, POS_3);
			break;
		case SIG_MOV_POS0:
		default:
			// stay where we are (nothing happened)
			h->dir = 0;
			break;
	}
}


static void pos1(struct encoder_handle *h, enum ENCODER_SIG sig)
{
	switch (sig)
	{
		case SIG_MOV_POS3:
			// rotate forwards
			h->dir = 1;
			h->ticks++;
			transition(h, POS_3);
			break;
		case SIG_MOV_POS0:
			// rotate backwards 
			h->dir = -1;
			h->ticks--;
			transition(h, POS_0);
			break;
		case SIG_MOV_POS2:
			// rotated twice, could be += 2 or -= 2 so just
			// ignore the ticks and jump to new state
			h->dir = 0;
			transition(h, POS_2);
			break;
		case SIG_MOV_POS1:
		default:
			// stay where we are (nothing happened)
			h->dir = 0;
			break;
	}
}


static void pos2(struct encoder_handle *h, enum ENCODER_SIG sig)
{
	switch (sig)
	{
		case SIG_MOV_POS0:
			// rotate forwards
			h->dir = 1;
			h->ticks++;
			transition(h, POS_0);
			break;
		case SIG_MOV_POS3:
			// rotate backwards 
			h->dir = -1;
			h->ticks--;
			transition(h, POS_3);
			break;
		case SIG_MOV_POS1:
			// rotated twice, could be += 2 or -= 2 so just
			// ignore the ticks and jump to new state
			h->dir = 0;
			transition(h, POS_1);
			break;
		case SIG_MOV_POS2:
		default:
			// stay where we are (nothing happened)
			h->dir = 0;
			break;
	}
}


static void pos3(struct encoder_handle *h, enum ENCODER_SIG sig)
{
	switch (sig)
	{
		case SIG_MOV_POS2:
			// rotate forwards
			h->dir = 1;
			h->ticks++;
			transition(h, POS_2);
			break;
		case SIG_MOV_POS1:
			// rotate backwards 
			h->dir = -1;
			h->ticks--;
			transition(h, POS_1);
			break;
		case SIG_MOV_POS0:
			// rotated twice, could be += 2 or -= 2 so just
			// ignore the ticks and jump to new state
			h->dir = 0;
			transition(h, POS_0);
			break;
		case SIG_MOV_POS3:
		default:
			// stay where we are (nothing happened)
			h->dir = 0;
			break;
	}
}


static void sig(struct encoder_handle *h, enum ENCODER_SIG sig)
{
	switch (h->pos)
	{
		case POS_0:
			pos0(h, sig);
			break;
		case POS_1:
			pos1(h, sig);
			break;
		case POS_2:
			pos2(h, sig);
			break;
		case POS_3:
			pos3(h, sig);
			break;
		default:
			// arg how did we get here !
			h->pos = sig; // just go to current pos
			break;
	}
}


int encoder_dir(struct encoder_handle *h)
{
	///@todo not thread safe !
	return h->dir;
}


int encoder_dt(struct encoder_handle *h)
{
	///@todo not thread safe !
	return h->dt;
}


int encoder_tick(struct encoder_handle *h, int reset)
{
	///@todo not thread safe !
	int ticks = h->ticks;
	if (reset)
		h->ticks = 0;
	return ticks;
}


int encoder_button(struct encoder_handle *h)
{
	return !h->button;
}


// ripped from gnu docs (http://www.gnu.org/software/libc/manual/html_node/Elapsed-Time.html)
// why dont they make me a function to call dam it !
/* Subtract the `struct timeval' values X and Y,
storing the result in RESULT.
Return 1 if the difference is negative, otherwise 0. */
static int timeval_subtract (struct timeval *result, struct timeval *x, struct timeval *y)
{
	/* Perform the carry for the later subtraction by updating y. */
	if (x->tv_usec < y->tv_usec)
	{
		int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
		y->tv_usec -= 1000000 * nsec;
		y->tv_sec += nsec;
	}
	if (x->tv_usec - y->tv_usec > 1000000)
	{
		int nsec = (x->tv_usec - y->tv_usec) / 1000000;
		y->tv_usec += 1000000 * nsec;
		y->tv_sec -= nsec;
	}

	/* Compute the time remaining to wait.
	tv_usec is certainly positive. */
	result->tv_sec = x->tv_sec - y->tv_sec;
	result->tv_usec = x->tv_usec - y->tv_usec;

	/* Return 1 if result is negative. */
	return x->tv_sec < y->tv_sec;
}


int encoder_poll(struct encoder_handle *h, int timeout)
{
	int ret;
	struct timeval tdiff;
	struct timeval _timeout = {.tv_sec = timeout / 1000000, .tv_usec = timeout % 1000000};

	// wait for the next interrupt
	FD_SET(h->fd_irq, &h->fds);

	ret = select(h->fd_irq + 1, &h->fds, NULL, NULL, (timeout < 0)? NULL: &_timeout);
	if (ret == -1)
		return -1;
	gettimeofday(&h->time, NULL);

	// if this is not a timeout and the fd is ready, read it
	if ((ret != 0) && FD_ISSET(h->fd_irq, &h->fds))
	{
		int buf;
		uint8_t sigs;
		enum ENCODER_POS new_pos;
		
		// update the time between ticks
		h->dt = 0;
		if (h->last_time.tv_sec != 0 || h->last_time.tv_usec != 0)
		{
			timeval_subtract(&tdiff, &h->time, &h->last_time);
			if (tdiff.tv_sec > 1000) // handle overflow (someone might not touch it for ages !)
				h->dt = 1000 * 1e6; 
			else
				h->dt = tdiff.tv_sec * 1e6 + tdiff.tv_usec;
		}
		else
			h->dt = 1000 * 1e6; // something happened but we have no idea how fast so just play it safe and say it was slow
		h->last_time = h->time;

		// interrupt occurred, find source
		FD_CLR(h->fd_irq, &h->fds);
		read(h->fd_irq, &buf, sizeof(buf));

		// read the new position
		lock_io(h);
		h->mfp_bus->Poke32(h->mfp_bus, 0x148, MFP_MASK(SW_A_MFP)); // clear pending edge detect events
		h->mfp_bus->Poke32(h->mfp_bus, 0x148, MFP_MASK(SW_B_MFP)); // "
		h->mfp_bus->Poke32(h->mfp_bus, 0x04c, MFP_MASK(SW_I_MFP)); // "
		sigs = read_sigs(h);
		unlock_io(h);
		new_pos = sigs & 0x03;
		h->button = (sigs & 0x10) ? 1: 0;

		// update state machine and ticks
		sig(h, new_pos);
		if (h->verbose)
			printf("last_pos = 0x%.2x; pos = 0x%.2x; sigs = 0x%.2x; ticks = %d; dt = %d\n", h->last_pos, h->pos, sigs, h->ticks, h->dt);

		return 1;
	}

	return 0;
}


static void init_hw(struct encoder_handle *h)
{
	uint32_t d;

	h->mfp_bus = BusInit(3);
	assert(h->mfp_bus);
	h->dio = DIOInit(0);
	assert(h->dio);
	h->pin = PinInit(0);
	assert(h->pin);
	lock_io(h);

	// init the pins as inputs
	h->pin->ModeSet(h->pin, SW_A_DIO, MODE_DIO);
	h->pin->ModeSet(h->pin, SW_B_DIO, MODE_DIO);
	h->pin->ModeSet(h->pin, SW_I_DIO, MODE_DIO);

	// generate rising/falling edge isr
	d = h->mfp_bus->Peek32(h->mfp_bus, 0x184) | MFP_MASK(SW_A_MFP) | MFP_MASK(SW_B_MFP);	// enable falling edge irq
	h->mfp_bus->Poke32(h->mfp_bus, 0x184, d);
	d = h->mfp_bus->Peek32(h->mfp_bus, 0x088) | MFP_MASK(SW_I_MFP);
	h->mfp_bus->Poke32(h->mfp_bus, 0x088, d);

	d = h->mfp_bus->Peek32(h->mfp_bus, 0x16c) | MFP_MASK(SW_A_MFP) | MFP_MASK(SW_B_MFP);	// enable rising edge irq
	h->mfp_bus->Poke32(h->mfp_bus, 0x16c, d);
	d = h->mfp_bus->Peek32(h->mfp_bus, 0x070) | MFP_MASK(SW_I_MFP);
	h->mfp_bus->Poke32(h->mfp_bus, 0x070, d);

	h->mfp_bus->Poke32(h->mfp_bus, 0x148, MFP_MASK(SW_A_MFP)); // clear pending edge detect events
	h->mfp_bus->Poke32(h->mfp_bus, 0x148, MFP_MASK(SW_B_MFP));
	h->mfp_bus->Poke32(h->mfp_bus, 0x04c, MFP_MASK(SW_I_MFP));

	d = h->mfp_bus->Peek32(h->mfp_bus, 0x19c) | MFP_MASK(SW_A_MFP) | MFP_MASK(SW_B_MFP);	// unmask edge isr
	h->mfp_bus->Poke32(h->mfp_bus, 0x19c, d);
	d = h->mfp_bus->Peek32(h->mfp_bus, 0x0a0) | MFP_MASK(SW_I_MFP);
	h->mfp_bus->Poke32(h->mfp_bus, 0x0a0, d);
	
	unlock_io(h);
}


struct encoder_handle *encoder_open(void)
{
	struct encoder_handle *h;
	int sigs;

	// alloc new handle
	if (encoder_alloc_index >= ENCODER_POOL_SIZE)
		return NULL;
	h = &encoder_pool[encoder_alloc_index++];
	memset(h, 0, sizeof(*h));

	// init hardware ports etc
	init_hw(h);

	// open the irq handle
	h->fd_irq = open(MFP_IRQ, O_RDONLY| O_NONBLOCK, S_IREAD);
	if (h->fd_irq == -1)
		return NULL;

	// get initial position
	lock_io(h);
	sigs =  read_sigs(h);
	unlock_io(h);
	h->pos = sigs & 0x03;
	h->button = (sigs & 0x10) ? 1: 0;

	return h;
}

