#include <encoder.h>
#include <stdio.h>
#include <stdlib.h>
#define log(msg, ...) printf(__FILE__ ":%s():[%d]:" msg, __func__, __LINE__, __VA_ARGS__)

int main(int argc, char **argv)
{
	struct encoder_handle *h = encoder_open();

	// poll for encoder events
	while (1)
	{
		int r;
		
		r = encoder_poll(h, -1);
		if (r == 0)
			printf("timeout\n");
		else
		{
			int dt = encoder_dt(h);
			int dtick = encoder_tick(h, 1);
			printf("dt = %d, dtick = %d, velocity = %.4f\n", 
				dt, dtick, (float)dtick * 1e6/(float)dt);
		}
	}

	return 0;
}

