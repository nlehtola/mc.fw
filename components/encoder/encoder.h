#ifndef __ENCODER_H__
#define __ENCODER_H__

#include <stdint.h>
#include <stdio.h>

struct encoder_handle;
int encoder_dir(struct encoder_handle *h);
int encoder_dt(struct encoder_handle *h);
int encoder_tick(struct encoder_handle *h, int reset);
int encoder_button(struct encoder_handle *h);
int encoder_poll(struct encoder_handle *h, int timeout);
struct encoder_handle *encoder_open(void);

#endif
