#!/bin/bash

while [ 1 ]; do
	logger "starting pif"
	MESSAGE=`python getPifMessage.py`
	python pif.py -c -i eth0 -m "$MESSAGE"
	sleep 5
done
