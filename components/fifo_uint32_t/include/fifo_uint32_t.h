/*  
 * File Name     : fifo_uint32_t.h
 * Author        : Usama Malik
 * Date Created  : 5/02/2014
 * Description: 
 * Implementation for a simple thread-safe FIFO for uint32_t type. 
 *
 * Copyright 2013, Southern Photonics/Coherent Solutions. www.southernphotonics.com
 * All Rights Reserved.
 */

#ifndef FIFO_UINT32_T_H
#define FIFO_UINT32_T_H

#include "tlsx_common.h"
#include <unistd.h>
#include <math.h>

typedef struct {

  uint32_t fifo_size;
  uint32_t* data; 
  uint32_t write_pointer; //can support large FIFOs with this
  uint32_t read_pointer;
  uint32_t count;  
  pthread_mutex_t lock; //read/writes are locked to make it safe for multiple threads. 
  
} fifo_uint32_t; 

int fifo_init(fifo_uint32_t*, uint16_t);
int is_full(fifo_uint32_t*, bool_t*);
int is_empty(fifo_uint32_t*, bool_t*);
int fifo_write(fifo_uint32_t*, uint32_t*); 
int fifo_read(fifo_uint32_t*, uint32_t*); 
int print_fifo(fifo_uint32_t*);

//test functions

int test_0();
int test_1();

void* circulate_numbers(void *);

#endif
