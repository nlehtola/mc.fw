/*  
 * File Name     : fifo_uint32_t.c
 * Author        : Usama Malik
 * Date Created  : 5/02/2014
 * Description: 
 * Implementation for the backplane bus system protocol. Details of the protocol 
 * can be found in MasterBladesComms.doc. 
 *
 * Copyright 2013, Southern Photonics/Coherent Solutions. www.southernphotonics.com
 * All Rights Reserved.
 */


#include "fifo_uint32_t.h"

#define NUM_THREAD 50
#define LOOP       1000 

/**
 * @param [in] f: FIFO struct
 * @param [in] size: FIFO size in number of uint32_t words. 
 * @returns 0 for success or an error code as defined in errors.h
 * @brief
 * Initialises an array to hold data. Initialises pointers as well as
 * the mutex variable for multithreaded execution. 
 * 
**/

int fifo_init(fifo_uint32_t* f, uint16_t size){

 
  if (pthread_mutex_init(&f->lock, NULL) != 0)
    {
        printf("\n FIFO mutex init failed\n");
        return 1;
    }
  f->fifo_size = size;
  f->data=(uint32_t *) malloc(size*sizeof(uint32_t));  
  if(f->data==NULL){
    printf("Error in allocating memory.\n");
    return 1;
  }  
  f->write_pointer = 0;
  f->read_pointer = 0;
  f->count = 0;

  return 0;
}


/**
 * @param [in] f FIFO struct
 * @param [in] d A new data value to be loded into the FIFO. 
 * @returns 0 for success or an error code as defined in errors.h
 * @brief
 * Loads a new value into the FIFO if the FIFO is not full. 
 * 
**/

int fifo_write(fifo_uint32_t* f, uint32_t* d){

  pthread_mutex_lock(&f->lock);
  if(f->fifo_size==f->count){    
    pthread_mutex_unlock(&f->lock);
    return 1;
  }
  f->data[f->write_pointer] = *d;
  f->write_pointer++;

  /**The write pointer always point to the position where a new data will be added.**/

   if(f->write_pointer==f->fifo_size)
     f->write_pointer =0; /**always wrap around to avoid overflow.**/
   f->count++;
   pthread_mutex_unlock(&f->lock);

  return 0;
}


/**
 * @param [in] f: FIFO struct
 * @param [out] d: Data value read from the FIFO. 
 * @returns 0 for success or an error code as defined in errors.h
 * @brief
 * d is set to 0 if FIFO is empty. 
 * 
**/


//The read pointer always point to the position where a new data will be read.
int fifo_read(fifo_uint32_t* f, uint32_t* d){

  int status; 
  status= pthread_mutex_lock(&f->lock);
  if(status!=0){
    printf("Error: %u \n", status);
    exit(0);
  }

  if(0==f->count){    
    pthread_mutex_unlock(&f->lock);
    *d = 0;
    return 1;
  }
  *d = f->data[f->read_pointer];
   f->data[f->read_pointer] = 0;
   f->read_pointer++;
   if(f->read_pointer==f->fifo_size)
     f->read_pointer=0; //always wrap around to avoid overflow
   f->count--;
   pthread_mutex_unlock(&f->lock);
  return 0;
}

/**
 * @param [in] f: FIFO struct
 * @param [out] result: True if the FIFO is full else false. 
 * @returns 0 for success or an error code as defined in errors.h
 * @brief
 * 
**/

int is_full(fifo_uint32_t* f, bool_t* result){

   pthread_mutex_lock(&f->lock);
  *result = (f->count==f->fifo_size) ? true : false;
   pthread_mutex_unlock(&f->lock);
  return 0; 
}


/**
 * @param [in] f: FIFO struct
 * @param [out] result: True if the FIFO is empty else false. 
 * @returns 0 for success or an error code as defined in errors.h
 * @brief
 * 
**/


int is_empty(fifo_uint32_t* f, bool_t* result){

  pthread_mutex_lock(&f->lock);
  *result = (0==f->count) ? true : false;
  pthread_mutex_unlock(&f->lock);
  return 0; 
}

/**
 * @param [in] f: FIFO struct
 * @returns 0 for success or an error code as defined in errors.h
 * @brief
 * Auxiallary function to help during development.
**/


int print_fifo(fifo_uint32_t* f){

  int i;
  bool_t full, empty;

  is_full(f, &full);
  is_empty(f, &empty);
  printf("size: %u, w_ptr: %u, r_ptr: %u, count: %u, empty: %u, full: %u \n", f->fifo_size, f->write_pointer, f->read_pointer, f->count, empty, full);
  for(i=0;i<f->fifo_size;i++)
    printf("%u ",*(f->data+i));
  printf("\n");
  return 0;
}


