/*  
 * File Name     : fifo_uint32_t_ut.c
 * Author        : Usama Malik
 * Date Created  : 5/02/2014
 * Description: 
 * Unit test for fifo_uint32_t. Creates threads and circulates numbers 
 * for some time. 
 * 
 * Copyright 2014, Southern Photonics/Coherent Solutions. www.southernphotonics.com
 * All Rights Reserved.
 */


#include "fifo_uint32_t.h"

#define NUM_THREAD 50
#define LOOP       1000 


/**Global variable**/
fifo_uint32_t test_fifo;

/**
 * @returns 0 for success or an error code as defined in errors.h
 * @brief
 * Main program to create threads ans assign tasks. 
 * 
**/


int main(){

  uint32_t i;
  uint32_t fifo_size = 16; 
  pthread_t circulate_thread[NUM_THREAD];
  pthread_attr_t attr;
  int status;

  status = fifo_init(&test_fifo, fifo_size); 
  if(status!=0){
    printf("Init failed.\n");
    return 1;
  }

  for(i=1;i<=fifo_size;i++)
     fifo_write(&test_fifo, (uint32_t*)&i);
  printf("Initial State of the FIFO.\n");
  print_fifo(&test_fifo);

  /**Now start a few threads which wikk randomly circulate 
     numbers through the FIFO**/

  /* Initialize and set thread detached attribute */
   pthread_attr_init(&attr);
   pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

  for(i=0;i<NUM_THREAD;i++){

 
    if( pthread_create( &circulate_thread[i] , &attr ,  circulate_numbers , (void *)i) < 0)
    {
	printf("Error creating thread.");
	return 1;
    }

  }/**for loop**/
  
  pthread_attr_destroy(&attr);
  
   for(i=0;i<NUM_THREAD;i++)
      pthread_join( circulate_thread[i] , NULL);
   
   printf("Final State of the FIFO.\n");
   print_fifo(&test_fifo);
   
   printf("Exit..\n");
	
  return 0;
}

/**
 * @param [in] parameters: Thread ID
 * @returns 0 for success or an error code as defined in errors.h
 * @brief
 * Randomly reads and writes data from the FIFO under test. 
 * 
**/


void* circulate_numbers(void* parameter){

  int i;
  bool_t full, empty;
  srand((uint32_t*)parameter+time(NULL));
  float wait_time; 
  uint32_t result;
  
  /**printf("Thread ID:%u\n", (uint32_t *)parameter);**/
   
  for(i=0;i<LOOP;i++){
    /**pop a number from the FIFO if it is not empty **/

    empty = fifo_read(&test_fifo, &result);
    
    if(0!=empty){
      continue;
    }
    /**This condition is not allowed. **/
    if(result==0){
      printf("ID: %u read    %u\n",(uint32_t *)parameter, result);
      break;
    }
    wait_time = (float)rand()/(float)RAND_MAX;
    sleep(wait_time+0.01);
    full = fifo_write(&test_fifo, &result);
    if(0!=full){
	printf("Write failed.\n");
	/**FIFO can never be full as we read first and then write. **/
	break;
    }/**if **/

 
  }/**repeat LOOP times**/
  
  pthread_exit(NULL);

}/**circulate_numbers**/

