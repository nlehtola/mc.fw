#!/bin/bash

SERVER=$1
shift
ARGS=$@
logger $ARGS
while [ 1 ]; do
	$SERVER $ARGS
	STATUS=$?
	# if the server won't start because another instance is running then don't restart
	if [$STATUS == "2" ]; then
		logger "$SERVER terminated with exit code: 2 Another instance is running. Server will not be restarted"
		break
	fi
	logger "$SERVER terminated with exit code: $STATUS. Server has been restarted"
	sleep 1
done
