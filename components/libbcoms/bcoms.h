#ifndef __BCOMS_H__
#define __BCOMS_H__

#define TRANSFER_SIZE (70)
#define TRANSFER_BUF_SIZE (TRANSFER_SIZE - 8)


int bcoms_read(void *buf, int len);


int bcoms_write(void *buf, int len);


void bcoms_set_blade(int blade);


void bcoms_flush(void);


void bcoms_init(void);


void bcoms_deinit(void);

#endif
