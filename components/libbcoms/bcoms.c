#include <libtsctl.h>
#include <Array.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <crc.h>
#include "bcoms.h"


// packet structure
#define STATUS_D2H_RX_VALID 1
#define STATUS_D2H_TX_VALID 2
#define STATUS_H2D_RX_VALID STATUS_D2H_TX_VALID
#define STATUS_H2D_TX_VALID STATUS_D2H_RX_VALID
#ifndef packed
#define packed(type) type __attribute__ ((packed))      		// pack this object (ie struct etc)
#endif
#define ACK (0x5555)
#define NAK (0xaaaa)
packed(struct) transfer_t
{
	uint8_t buf[TRANSFER_BUF_SIZE];

	// used verify the payload (buf)
	uint16_t status;
	uint32_t crc;

	// pause for DEAD_TIME here for both sides to eval
	// the above part of the packet

	// we let the slave know we understood its packet
	// and it lets us know if it understood ours
	uint16_t ack;
};


static struct crc_h stm32f10x_crc_h =
{
	// this setup mimics the stm32 32bit crc hardware
	// see: https://my.st.com/public/STe2ecommunities/mcu/Lists/cortex_mx_stm32/Flat.aspx?RootFolder=https%3a%2f%2fmy%2est%2ecom%2fpublic%2fSTe2ecommunities%2fmcu%2fLists%2fcortex_mx_stm32%2fCRC%20calculation%20in%20software&FolderCTID=0x01200200770978C69A1141439FE559EB459D7580009C4E14902C3CDE46A77F0FFD06506F5B&currentviews=3822
	{
		32, 			// width
		0x04C11DB7, 	// poly
		0xFFFFFFFF, 	// init
		FALSE, 			// refin
		FALSE, 			// refot
		0, 				// xorot

		0,				// working reg (irgnore this in cmp)
	},

	NULL,
	0,
	
	CRC_METHOD_BEST,
};


static struct _bcoms_t
{
	DIO *dio;
	SPI *spi;

	int blade;
} _bcoms;
static struct _bcoms_t *bcoms = &_bcoms;


// io
#define SPI_SELECT_BIT0 30
#define SPI_SELECT_BIT1 29
#define SPI_SELECT_BIT2 28
#define SPI_SELECT_BIT3 27
static int spi_select(int addr)
{
	bool bit3 = (addr & 0x08)? 1: 0;
	bool bit2 = (addr & 0x04)? 1: 0;
	bool bit1 = (addr & 0x02)? 1: 0;
	bool bit0 = (addr & 0x01)? 1: 0;

	bcoms->dio->Set(bcoms->dio, SPI_SELECT_BIT0, bit0);
	bcoms->dio->Set(bcoms->dio, SPI_SELECT_BIT1, bit1);
	bcoms->dio->Set(bcoms->dio, SPI_SELECT_BIT2, bit2);
	bcoms->dio->Set(bcoms->dio, SPI_SELECT_BIT3, bit3);
	bcoms->dio->Commit(bcoms->dio, 0);
}

// use blade 14 as deselect (we have only 10 slots or on bench top 
// all lines are pulled high [15] so use 14 as deselected).
///@todo use DESELECT_ADDR as 0 for debug then I can short all line together and use 1 wire :)
#define DESELECT_ADDR 0
static int spi_deselect(int addr)
{
	spi_select(DESELECT_ADDR);
}


#define MAX(a,b) ((a > b)? a: b)
#define MIN(a,b) ((a < b)? a: b)
#define ADR -1 // I think this says de_cs is 0 and dont do any CS but I am not sure
static int spi_transfer(void *buf_rx, int buf_rx_len, const void *buf_tx, int buf_tx_len)
{
	char *tx=NULL, *rx=NULL;
	int len = MAX(buf_rx_len, buf_tx_len);
	int res = -1;
	len = MAX(len, 2);	// len needs to be a minimum of 2 bytes for spi using this lib

	// alloc array
	rx = ArrayAlloc(len, 1);
	tx = ArrayAlloc(len, 1);

	// populate the tx buffer
	if (buf_tx != NULL)
		memcpy(tx, buf_tx, buf_tx_len);

	// send
	bcoms->spi->ReadWrite(bcoms->spi, ADR, tx, rx);
	
	// check the rx buffer and if it is valid return it
	if (buf_rx != NULL)
		memcpy(buf_rx, rx, buf_rx_len);

	ArrayFree(rx);
	ArrayFree(tx);

	return len;
}


// this is unused on purpose it is a backup slower version of above in case we find that the
// firmware cannot keep up
static int spi_transfer_slow(void *buf_rx, int buf_rx_len, const void *buf_tx, int buf_tx_len) __attribute__ ((unused));
static int spi_transfer_slow(void *buf_rx, int buf_rx_len, const void *buf_tx, int buf_tx_len)
{
  uint8_t *_buf_rx = (uint8_t *)buf_rx, *_buf_tx = (uint8_t *)buf_tx;
  char *tx = NULL, *rx = NULL;
  int l, len = MAX(buf_rx_len, buf_tx_len);
  int res = -1;
  len = MAX(len, 2); // len needs to be a minimum of 2 bytes for spi using this lib
  
  // alloc array
  rx = ArrayAlloc(2, 1);
  tx = ArrayAlloc(2, 1);
  
  // send all but the last word
  for (l = 0; l < len - 2; l += 2)
  {
    // populate the tx buffer
    if (_buf_tx != NULL)
      memcpy(tx, &((uint8_t *)_buf_tx)[l], 2);
    
    // send
    bcoms->spi->ReadWrite(bcoms->spi, ADR, tx, rx);
    
    // check the rx buffer and if it is valid return it
    if (_buf_rx != NULL)
      memcpy(&_buf_rx[l], rx, 2);
    
    // delay for slow firmware
	usleep(100);
  }

  // populate the tx buffer
  if (_buf_tx != NULL)
    memcpy(tx, &_buf_tx[l], len - l);
  
  // send
  bcoms->spi->ReadWrite(bcoms->spi, ADR, tx, rx);
  
  // check the rx buffer and if it is valid return it
  if (_buf_rx != NULL)
    memcpy(&_buf_rx[l], rx, len - l);
  
  // delay for slow firmware
  usleep(100);
 
  // done
  ArrayFree(rx);
  ArrayFree(tx);
  return len;
}


static void _show_packet(packed(struct) transfer_t *xfer)
{
	int k;

	printf("packet(%p) = ", xfer);
	printf("{buf = ");
	for (k = 0; k < TRANSFER_BUF_SIZE; k++)
	{
		if (xfer->buf[k] >= 20 && xfer->buf[k] <= 127)
			printf("%c, ", xfer->buf[k]);
		else
			printf("\\x%.2X, ", xfer->buf[k]);
	}
	printf("}, ");
	printf("{stat = \\x%.2X}, ", xfer->status);
	printf("{crc = \\x%.8X}, ", xfer->crc);
	printf("{ack = \\x%.2X}\n\n", xfer->ack);
	
}


static uint32_t crc_transfer(packed(struct) transfer_t *t)
{
	uint32_t len = sizeof(*t) - sizeof(t->crc) - sizeof(t->ack); // do not include crc or ack in the crc
	uint32_t crc;

	crc = crc_buf(&stm32f10x_crc_h, t, len, true);

	return crc;
}


#define SELECT_TIME (5000)
#define DEAD_TIME (5000)
#define DESELECT_TIME (5000)
static int bcoms_transfer(packed(struct) transfer_t *rx_buf, packed(struct) transfer_t *tx_buf, uint32_t *crc)
{
	int len = sizeof(*tx_buf);

	// do a transfer using the bcoms protocol
	
	// init to nak and only update this if crc's match
	tx_buf->ack = NAK;
	
	// calc crc on tx packet
	tx_buf->crc = crc_transfer(tx_buf);

	// select and wait for select time (time for NSS to fall and
	// uC to get ready for transfer)
	spi_select(bcoms->blade);
	usleep(SELECT_TIME); 

	// send everything but the ack
	tx_buf->crc = crc_transfer(tx_buf);
	spi_transfer_slow(rx_buf, sizeof(*rx_buf) - sizeof(rx_buf->ack), tx_buf, sizeof(*tx_buf) - sizeof(tx_buf->ack));

	// calculate the crc in the rx buf and if it matches ACK, else NAK
	*crc = crc_transfer(rx_buf);
	if (*crc == rx_buf->crc)
		tx_buf->ack = ACK;

	// insert some dead time to allow the slave to perform ACK test above
	usleep(DEAD_TIME);

	// ack if rx crc match otherwise nak (the slave will have to try again)
	spi_transfer_slow(&rx_buf->ack, sizeof(rx_buf->ack), &tx_buf->ack, sizeof(tx_buf->ack));

	// deselect and wait for deselect time (time for NSS to rise
	// and uC to process transfer)
	spi_deselect(bcoms->blade);
	usleep(DESELECT_TIME);

	// if either the rx or tx ack fails the status bits may be wrong so we must fail the whole
	// transfer and both will need to be done again
	if (rx_buf->ack != ACK || tx_buf->ack != ACK)
		return -1;

	return len;
}


static void spi_init(void)
{
	bcoms->dio = DIOInit(0);
	assert(bcoms->dio);
	bcoms->dio->Lock(bcoms->dio, SPI_SELECT_BIT0, 0);
	bcoms->dio->Lock(bcoms->dio, SPI_SELECT_BIT1, 0);
	bcoms->dio->Lock(bcoms->dio, SPI_SELECT_BIT2, 0);
	bcoms->dio->Lock(bcoms->dio, SPI_SELECT_BIT3, 0);

	bcoms->spi = SPIInit(0);
	assert(bcoms->spi);
	bcoms->spi->Lock(bcoms->spi, 0, 0);
	bcoms->spi->ClockSet(bcoms->spi, 1250000); // go slow to make coms easier atm
	bcoms->spi->EdgeSet(bcoms->spi, 0);
}


static void spi_deinit(void)
{
	bcoms->spi->Unlock(bcoms->spi, 0, 0);

	bcoms->spi->Unlock(bcoms->dio, SPI_SELECT_BIT0, 0);
	bcoms->spi->Unlock(bcoms->dio, SPI_SELECT_BIT1, 0);
	bcoms->spi->Unlock(bcoms->dio, SPI_SELECT_BIT2, 0);
	bcoms->spi->Unlock(bcoms->dio, SPI_SELECT_BIT3, 0);
}


// bcoms interface
#define BCOMS_EBUSY (-1)
#define BCOMS_ESIZE (-2)
#define BCOMS_EACK  (-3)


// this will do a transfer to the device until it reports a successful
// read in the rx packet. The tx packet will indicate no valid data in 
// the tx packet, but indicates the host will process rx packet (read)
#define READ_RETRY_DELAY (50000)
#define MAX_READ_RETRY (7)
int bcoms_read(void *buf, int len)
{
	packed(struct) transfer_t rx_buf __attribute__ ((aligned (4)));
	packed(struct) transfer_t tx_buf __attribute__ ((aligned (4)));
	int retries = 0;
	uint32_t crc;

	if (len > TRANSFER_BUF_SIZE)
		return BCOMS_ESIZE;

	// init the tx buffer for a read
	memset(&tx_buf, 0, sizeof(tx_buf));
	tx_buf.status = STATUS_H2D_RX_VALID;

	while (1)	
	{
		// do a spi transfer
		retries++;
		if (bcoms_transfer(&rx_buf, &tx_buf, &crc) < 0)
		{
			if (retries <= MAX_READ_RETRY)
				goto retry;
			return BCOMS_EACK;
		}
		
		// did the device send us something in this packet
		if (rx_buf.status & STATUS_D2H_TX_VALID)
			break;
		
		// keep waiting or timeout
retry:
		if (retries > MAX_READ_RETRY)
		{
			return BCOMS_EBUSY;
		}
		usleep(READ_RETRY_DELAY);
	}
	
	// done so copy over the buffer
	len = MIN(len, sizeof(rx_buf.buf));
	if (buf != NULL)
		memcpy(buf, rx_buf.buf, len);

	return len;
}


// this will do a transfer to the device until it reports a successful
// write in the rx pack. The tx packet will indicate valid data in the
// tx packet but the read will be unsuccessful (write only no read)
#define WRITE_RETRY_DELAY (50000)
#define MAX_WRITE_RETRY (7)
int bcoms_write(void *buf, int len)
{
	packed(struct) transfer_t rx_buf __attribute__ ((aligned (4)));
	packed(struct) transfer_t tx_buf __attribute__ ((aligned (4)));
	int retries = 0;
	uint32_t crc;

	if (len > TRANSFER_BUF_SIZE)
		return BCOMS_ESIZE;

	// init the tx buffer for a write
	len = MIN(len, sizeof(tx_buf.buf));
	memcpy(tx_buf.buf, buf, len);
	tx_buf.status = STATUS_H2D_TX_VALID;

	while (1)	
	{
		// do a spi transfer
		retries++;
		if (bcoms_transfer(&rx_buf, &tx_buf, &crc) < 0)
		{
			if (retries <= MAX_WRITE_RETRY)
				goto retry;
			return BCOMS_EACK;
		}
		
		// did the device receive this packet
		if (rx_buf.status & STATUS_D2H_RX_VALID)
			break;
		
		// keep waiting or timeout
retry:
		if (retries > MAX_WRITE_RETRY)
		{
			return BCOMS_EBUSY;
		}
		usleep(WRITE_RETRY_DELAY);
	}
	
	return len;
}


// set the blade all
void bcoms_set_blade(int blade)
{
	bcoms->blade = blade;
}


void bcoms_flush(void)
{
	while (bcoms_read(NULL, 0) >= 0)
	{}
}


void bcoms_init(void)
{
	crc_init(&stm32f10x_crc_h);
	spi_init();

	// by default we will talk to a deselected blade (ie nothing)
	// also ensure we are deselected at start-up
	bcoms_set_blade(DESELECT_ADDR);
	spi_deselect(bcoms->blade);
	usleep(DESELECT_TIME);
}


void bcoms_deinit(void)
{
	// ensure we are deselected and clear blade
	bcoms_set_blade(DESELECT_ADDR);
	spi_deselect(bcoms->blade);
	usleep(DESELECT_TIME);

	spi_deinit();
}

