#ifndef INSTRUMENTCONFIG_HPP
#define INSTRUMENTCONFIG_HPP

#define CHASSIS_CONFIG_FILE "/cs/releases/TLSX_chassis_config.cfg"
#define BACKUP_CONFIG_FILE "TLSX_chassis_config.cfg"

#include "logging.h"
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

int loadConfig(char* configFile);

const char* getCompanyNameString();
const char* getInstrumentModelNumber();
const char* getInstrumentSerialNumber();
const char* getHWVersionNumber();		// get hardware version number

bool getIsBenchtop();					// is this a benchtop unit with a single blade

unsigned int getMaxNumberOfBlades();	// get the number of blades expected per chassis
unsigned int getMaxNumberOfChannels();	// get the number of channels expected per blade

bool getIsFakekblade();					// are we using fakekblade
const char* getHardcodedChannelMask();	// this is a quick hack to put the number of channels in the config file

#ifdef __cplusplus
}
#endif

#endif //INSTRUMENTCONFIG_HPP
