#ifndef CLIENTCONNECTIONS_HPP
#define CLIENTCONNECTIONS_HPP

#define CLIENT_IO_ERROR 3		// non fatal
#define CLIENT_IO_TIMEOUT 2		// non fatal
#define CLIENT_NOENDCHAR 1		// expected behaviour
#define CLIENT_SUCCESS 0		// expected behaviour
#define CLIENT_SOCKET_ERROR -1	// link fatal
#define CLIENT_PACKET_ERROR -2	// link fatal
#define CLIENT_PARSE_ERROR -3	// non fatal
#define CLIENT_MISC_ERROR -4	// link fatal

// ############# standard event status register (*ESR?, *ESE)
#define SCPI_SESR_OPC	(1<<0)	// operation complete
#define SCPI_SESR_RQC	(1<<1)	// request control (488.1 only, has no meaning in VXI11)
#define SCPI_SESR_QUERY	(1<<2)	// query error					//IEEE 488.2 11.5.1.1.7
#define SCPI_SESR_DDE	(1<<3)	// device dependant
#define SCPI_SESR_EXEC	(1<<4)	// excecution error
#define SCPI_SESR_CME	(1<<5)	// command error (parse error)	//IEEE 488.2 11.5.1.1.4
#define SCPI_SESR_USR	(1<<6)	// user request
#define SCPI_SESR_PON	(1<<7)	// power on

// ############# status byte (*STB?, *SRE)
#define SCPI_STB_UNUSED0	(1<<0)
#define SCPI_STB_UNUSED1	(1<<1)
#define SCPI_STB_UNUSED2	(1<<2)
#define SCPI_STB_QUES		(1<<3)	// Questionable summary bit (SCPI-99 Figure 9-1)
// My interpretation of MAV based on 488.2 11.5.2.1 and 11.2.1.2 is that MAV
// has no meaning for the *STB? query since this reads the output queue
// (thus it is always empty). In the context of 488.2 the MAV bit must only
// have meaning for "serial read" which is a 488.1 concept. Thus in the context
// of 488.2 over VXI11 MAV only has meaning for the VXI11 read_status call.
#define SCPI_STB_MAV		(1<<4)	//IEEE 488.2 11.5.2.1
// The SESR bit is the OR of all the bits in the bitwise AND of the
// SESR register (*ESR?) and Standard Event Status Enable Register (*ESE)
#define SCPI_STB_SESR		(1<<5)
// The MSS bit is the OR of all the bits in the bitwise and of the Status Byte
// register (*STB?) and Service Request Enable Register (*SRE) excluding bit 6
#define SCPI_STB_MSS		(1<<6)
#define SCPI_STB_OPER		(1<<7)	// Operation summary bit (SCPI-99 Figure 9-1)

// c++ includes
#include <string>
#include <map>
using namespace std;

// c includes
#include <cstdlib>
#include <errno.h>

// network
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/stat.h>

// dependancies
#include "logging.h"

//TODO move to excecute file 
#include "DecodedMessage.hpp"
#include "SCPIParser.hpp"

// ***************************** Client class *****************************
class Client
{
private:
	long linkid;	// linkid -1 indicates an inactive Connection
	string responseBuffer;
	// statusByte is build on the fly, unlike errorByte which is stored
	uint8_t statusByteMask;
	
	uint8_t errorByte;
	uint8_t errorByteMask;
	
	uint8_t getStatusByte();
	uint8_t getErrorByte(bool clear);
public:
	Client(long linkid);
	~Client();
	
	int readBlock(char*& data, unsigned int &length, unsigned long maxLength, char termchar);
	int writeBlock(char* data, unsigned int length, bool endchar);

	int lockDevice(unsigned int timeOut);	// TODO
	int unlockDevice();			// TODO, Note: clients almost never use locking
	uint8_t getStatusByteMAV();
	int deviceClear();

	long getLinkid();
};

// ************************** client managment functions ***********************
Client* findClient(long linkid);
int removeClient(long linkid);
void removeAllClient();
Client* getNewClient(string device);

#endif //CLIENTCONNECTIONS_HPP
