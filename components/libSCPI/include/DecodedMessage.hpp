#ifndef DECODEDMESSAGE_HPP
#define DECODEDMESSAGE_HPP

#include "logging.h"
#include "SCPIServerState.hpp"
#include "Units.h"

#include <stdio.h>
#include <iostream>
#include <stdint.h>
#include <vector>
#include "backendCallbackTable.h"
using namespace std;

/** @class DecodedMessage
 * @brief This class stores information extracted from a command by the parser
 * Calling execute() will execute the command. This will look up the command
 * in the dispatch table and run the callback function associated with it.
**/
class DecodedMessage
{
public:
	BackendCallbackID id;
	TaskID task;
	uint8_t blade;
	uint8_t channel;
	int numValues;
	double* values;
	
	DecodedMessage(BackendCallbackID callbackid, TaskID taskid, uint8_t bladeid, uint8_t channelid, int numValues, double* values);

	~DecodedMessage();
	int execute(string& response);
};

#endif //DECODEDMESSAGE_HPP

