#ifndef UNITS_HPP
#define UNITS_HPP

#include "logging.h"

// This is used for conversions from wavelength to frequency
#define SPEED_OF_LIGHT (299792458.0)	//m/s

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
	UNIT_NONE,
	//temp
	UNIT_C,
	UNIT_CC,
	//frequency
	UNIT_HZ,
	UNIT_KHZ,
	UNIT_MHZ,
	UNIT_GHZ,
	UNIT_THZ,
	//wavelength
	UNIT_M,
	UNIT_MM,
	UNIT_UM,
	UNIT_NM,
	UNIT_PM,
	//power
	UNIT_DB,
	UNIT_MDB,
	UNIT_DBM,
	UNIT_MDBM,
	UNIT_UDBM,
	UNIT_W,
	UNIT_MW,
	UNIT_UW,
	//time
	UNIT_S,
	UNIT_MS,
	UNIT_US,
	UNIT_NS,
	//dB rate
	UNIT_DBPS,
	UNIT_MDBPS
} Unit;

typedef enum {
	UNIT_FREQ,
	UNIT_POWER,
	UNIT_DBPOWER,
	UNIT_DBMPOWER,
	UNIT_WAVE,
	UNIT_TEMP,
	UNIT_TIME,
	UNIT_DBRATE,
	UNIT_TYPE_NONE
} UnitType;

// converts wavelength to frequency
double mToMHzRoundToHundredMHz(double wavelength);
// converts frequency to wavelength
double MHzTom(double frequency);

Unit unitFromString(const char* name);
const char* getUnitName(Unit unit);
int doUnitConvertion(double* result, double value, Unit to, Unit from);

#ifdef __cplusplus
}
#endif

#endif //UNITS_HPP
