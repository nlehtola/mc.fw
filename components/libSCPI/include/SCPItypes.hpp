// These types describe a decoded SCPI or 488.2 command, refered to as a "PROGRAM_MESSAGE_UNIT"
// The ProgramDataValue class encapsulates a single decoded command argment
// ProgramMessageUnit class describes the path of the command, and has a list of decoded arguments

#ifndef SCPITYPES_HPP
#define SCPITYPES_HPP

#include <string>
#include <vector>
#include <algorithm>
#include <stdio.h>		//TODO remove

#include <pthread.h>
#include <queue>

#include "DecodedMessage.hpp"

using namespace std;

//Units, SUFFIX will be an emum for the units
typedef int SUFFIX;

typedef enum {
	NO_TYPE,
	CHARACTER_PROGRAM_DATA,
	DECIMAL_NUMERIC_PROGRAM_DATA,
	NON_DECIMAL_PROGRAM_DATA,
	STRING_PROGRAM_DATA,
	ARBITARY_BLOCK_PROGRAM_DATA,
	EXPRESSION_PROGRAM_DATA,
	SUFFIX_PROGRAM_DATA
} ProgramDataType;

// ***************************** ProgramDataValue *****************************
class ProgramDataValue
{
// This class uses double and int for non decimal and decimal data types respectively
// This may restrict the maximum allowable values for these data types
private:
	void *value;
	ProgramDataType _type;
	string unit;
public:

	ProgramDataValue();
	~ProgramDataValue();
	
	void assignCharacter(string *p);
	void assignDecimal(double f, const string& unit);
	void assignNumeric(int i);
	void assignString(string *p);
	string* retrieveCharacter();
	double retrieveDecimal(string& unit); // returns the value and the units
	int retrieveNumeric();
	string* retrieveString();
	
	ProgramDataType type();
	bool isDecimal();
};

// ***************************** ProgramDataList *****************************
class ProgramDataList
{
public:
	vector<ProgramDataValue*> list;
	~ProgramDataList();
};

// **************************** ProgramHeader ****************************
class ProgramHeader
{
public:
	ProgramHeader(bool query);
	
	bool query;		// if this is false, the message is a command
	bool absolute;
	vector<string> headerPath;
};

class ProgramMessageUnit
{
public:
	ProgramMessageUnit(ProgramHeader* programHeader, ProgramDataList* programDataList);
	~ProgramMessageUnit();
	
	ProgramHeader* programHeader;
	ProgramDataList* programDataList;
};

#endif // SCPITYPES_HPP
