#ifndef BACKENDCALLBACKTABLE_H
#define BACKENDCALLBACKTABLE_H

#include <stdint.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

#define COMMAND_SUCCESS 0
#define COMMAND_ERROR   -1

// the meaning of TaskID varies depending on the command id
// each command function interprets this differently, some commands support
// multiple tasks, some support only the default
typedef enum {
//Commands:
	TASK_NONE = 0,		// encodes unset task in arguments
	TASK_COMMAND,
	TASK_SET,
	STATE_ON,
	STATE_OFF,
	SET_ATT,
	SET_POW,
	SET_OFFSET,
	SET_REL,
	SET_ABS,
	SET_XB,
	SET_REF,
	SET_MIN,
	SET_MAX,
	SET_DEFAULT,
//Queries:
	TASK_QUERY,		// all queries must be larger than TASK_QUERY, it is used to define command/query in the parser (commandTable.cpp)
	TASK_MIN,
	TASK_MAX,
	TASK_DEFAULT,
	TASK_SET_POINT,
	TASK_LIST,
	TASK_ALL
} TaskID;

// The unit suffix on the ID tells which unit numbers in the double* 
// values parameter are in. These are also the default unit for return 
// values of that command
typedef enum {
	ID_NONE = 0, // encodes end of command table
// VOA
	ID_READDC,
	ID_COLZERO,
	ID_ACLSTATE,
	ID_LOCK,
	ID_CALZERO,
	ID_ATTEN_MODE,
	ID_CONTMODE,
	ID_ARESOLUTION,
	ID_INSERTION_LOSS_DB,
	ID_ATTENUATION_DB,
	ID_ATTENUATION_RAW,
	ID_ATTENUATOR_TEMP_C,
	ID_INOFFSET,
	ID_ATTEN_VEL_DBPS,
	ID_RATTENUATION,
	ID_INREFERENCE,
	ID_WAVELENGTH_NM,
	ID_APMODE,
	ID_DTOLERANCE,
	ID_OUTOFFSET,
	ID_POWER_DBM,
	ID_POWER_RAW,
	ID_DARK_POWER_DBM,
	ID_START_DARK_NULLING,
	ID_AVERAGING_TIME_S,
	ID_OUTREFERENCE,
	ID_RPOWER,
	ID_STATE,
// RF Clock
	ID_RFCL_POWER,
	ID_RFCL_FREQUENCY,
	ID_RFCL_OUTPUT,
// PPG
	ID_PPG,
	ID_PPG_CHANNEL,
	ID_PPG_BITRATE,
	ID_PPG_VPKPK,
	ID_PPG_BIT_PATTERN,
	ID_PPG_SKEW,
	ID_PPG_TAP,
// System
	ID_ERROR,
	ID_VERS,
	ID_DATE,
	ID_TIME,
// Status
	ID_PRES,
// GPIB:
	ID_CLS,
	ID_ESE,
	ID_ESR,
	ID_IDN,
	ID_IDN_SLOT,
	ID_OPC,
	ID_OPC_SLOT,
	ID_OPC_CHAN,
	ID_OPT,
	ID_OPT_SLOT,
	ID_RST,
	ID_WAI,
	ID_SRE,
	ID_STB,
	ID_TST
} BackendCallbackID;

int runCallback(size_t max, char* response, BackendCallbackID id, TaskID task, uint8_t blade, uint8_t channel, int numValues, const double* values);
int registerCallback(BackendCallbackID id, int (*funcPtr) (size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values));

#ifdef __cplusplus
}
#endif

#endif //BACKENDCALLBACKTABLE_H
