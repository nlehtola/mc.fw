#ifndef SCPIPARSER_H
#define SCPIPARSER_H

#include "DecodedMessage.hpp"
#include "logging.h"

#include <iostream>
#include <string>
using namespace std;

#define PARSER_SUCCESS		 0
#define PARSER_PARSE_ERROR	-1
#define PARSER_ERROR		-2
#define PARSER_FATAL_ERROR	-3 		//link fatal

int parseSCPI(char* data, unsigned int length, vector<DecodedMessage*>* decodedMessages);

#endif //SCPIPARSER_H
