#ifndef DEFAULTBACKENDCALLBACKS_H
#define DEFAULTBACKENDCALLBACKS_H

#include "backendCallbackTable.h"
#include "Units.h"
#include "stdint.h"
#include <stdio.h>

// Operation

// Questionable

// System
//int execError(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values);
int execVersion(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values);

// registers all the callbacks that are implimented by this file
// These are the default callbacks which are needed for all instrument types
void registerDefaultCallbacks();

#endif //DEFAULTBACKENDCALLBACKS_H
