#ifndef LIBSCPI_H
#define LIBSCPI_H
/**
 * @file libSCPI.h
 * @brief This is the interface of the libSCPI library. Register callbacks
 * for hardware backend using the <register callback function name> function.
 * Then start the SCPI server by calling the startSCPI() function
 */

/** @brief Functions for unit conversions and Unit symbol names */
#include "Units.h"

/** @brief Functions for registering callback handler functions for the 
 * backend. */
#include "backendCallbackTable.h"

/** @brief Functions for loading and querying the settings file */
#include "InstrumentConfig.h"

/** 
 * @brief Registers an RPC service acting as a VXI11 server.
 * SCPI commands will be parsed and the appropriate hardware backend callbacks
 * will be called when a command is sent to the server.
 * @param configFile If this is not "" or NULL SCPI will attempt to open this 
 * file as the config file.
 * @return Does not return unless a fatal error has occurred
 */
int startSCPI();

#endif //LIBSCPI_H
