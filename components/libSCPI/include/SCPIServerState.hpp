#ifndef SCPISSERVERSTATE_HPP
#define SCPISSERVERSTATE_HPP


#include <stdint.h>
#include <stdlib.h>
#include <string>
#include <version.h>
#include <vector>
#include "logging.h"
using namespace std;

class SCPIServerState
{
	vector<string> errors;

public:
	SCPIServerState();

	void recordError(int errorCode, string extraInfo);
};


#endif //SCPISSERVERSTATE_HPP
