#include "SCPIParser.hpp"
#include "SCPItypes.hpp"
#include "logging.h"
#include <string.h>

int parseSCPIWithBison(char* data, unsigned int length, vector<ProgramMessageUnit*>& programMessage);

int main()
{
	spOpenLog("test_SCPIParser", 0);
	
	//char* data = (char*)"SOURCE:CHANNEL? HI, 1 Hznm ;SOURCE:CHANNEL? 2";
	//char* data = (char*)"SOURCE:CHANNEL? 1.0 Hz";
	
	char* data[10];
	//MANTISSA WHITE_SPACE EXPONENT WHITE_SPACE PROGRAM_MNEMONIC
	data[0] = (char*)"SOURCE:CHANNEL? 1.0 e+3 Hz";
	//MANTISSA             EXPONENT WHITE_SPACE PROGRAM_MNEMONIC
	data[1] = (char*)"SOURCE:CHANNEL? 1.0e+3 Hz";
	//MANTISSA WHITE_SPACE EXPONENT             PROGRAM_MNEMONIC
	data[2] = (char*)"SOURCE:CHANNEL? 1.0 e+3Hz";
	//MANTISSA             EXPONENT             PROGRAM_MNEMONIC
	data[3] = (char*)"SOURCE:CHANNEL? 1.0e+3Hz";

	//MANTISSA WHITE_SPACE EXPONENT
	data[4] = (char*)"SOURCE:CHANNEL? 1.0 e+3";
	//MANTISSA             EXPONENT
	data[5] = (char*)"SOURCE:CHANNEL? 1.0e+3";
	
	//MANTISSA                      WHITE_SPACE PROGRAM_MNEMONIC *
	data[6] = (char*)"SOURCE:CHANNEL? 1000.0 Hz";
	//MANTISSA                                  PROGRAM_MNEMONIC
	data[7] = (char*)"SOURCE:CHANNEL? 1000.0Hz";
	//MANTISSA
	data[8] = (char*)"SOURCE:CHANNEL? 1000.0";
	int size = 9;
	
	for(int m=0; m<size; ++m) {
		unsigned int length = strlen(data[m]);
		printf("%s\n", data[m]);

		vector<ProgramMessageUnit*> programMessage;
		if (parseSCPIWithBison(data[m], length, programMessage)!=PARSER_SUCCESS) {
			printf("################## Failed\n");
			continue;
		}
		
		for (vector<ProgramMessageUnit*>::iterator i=programMessage.begin(); i!=programMessage.end(); ++i) {
			printf("Start\n");
			if ((*i)->programDataList==NULL) {
				continue;
			}
			for(vector<ProgramDataValue*>::iterator j=(*i)->programDataList->list.begin(); j!=(*i)->programDataList->list.end(); ++j) {
				switch((*j)->type()) {	
				case NO_TYPE:
					printf("\tNo Type\n");
					break;
				case CHARACTER_PROGRAM_DATA:
					printf("\tCharacter: %s\n", (*j)->retrieveCharacter()->c_str());
					break;
				case DECIMAL_NUMERIC_PROGRAM_DATA:
					{
						string unit;
						double num = (*j)->retrieveDecimal(unit);
						printf("\tDecimal: %f Unit: \"%s\"\n", num, unit.c_str());
					}
					break;
				case NON_DECIMAL_PROGRAM_DATA:
					printf("\tNumeric: %d\n", (*j)->retrieveNumeric());
					break;
				case STRING_PROGRAM_DATA:
					printf("\tString: %s\n", (*j)->retrieveString()->c_str());
					break;
				case ARBITARY_BLOCK_PROGRAM_DATA:
					printf("\tCan't display arbitary block data\n");
					break;
				case EXPRESSION_PROGRAM_DATA:
					printf("\tCan't display expression data\n");
					break;
				case SUFFIX_PROGRAM_DATA:
					printf("\tSuffix data type is the same as CHARACTER_PROGRAM_DATA\n");
					break;
				default:
					printf("\tType unknown\n");
				}
			}
		}
	
	
		for (vector<ProgramMessageUnit*>::iterator i=programMessage.begin(); i!=programMessage.end(); ++i) {
			delete(*i);
		}
		printf("################## Succeeded\n");
	}
	return 0;
}
