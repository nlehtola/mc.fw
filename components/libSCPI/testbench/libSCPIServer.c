/**
 * @file libSCPIServer.c
 * @brief Example usage of the libSCPI library
**/

#include "libSCPI.h"
#include "common.h"
#include "logging.h"
#include "singleton.h"
#include <string.h>
#include <stdlib.h>
#include "backendCallbackTable.h"

#define SCPI_DAEMON_NAME "TLSX_libSCPIServer"
#define SCPI_PID_FILE "/tmp/libSCPIServer.pid"

int execReset(size_t max, char* response, TaskID task, uint8_t blade, uint8_t laser, int numValues, const double* values)
{
	spLogDebug(1, "executing SOUR%d:CHAN%d:RESET", blade, laser);
	spLogWarning("Warning: executing SOUR%d:CHAN%d:RESET, this is a testing command ONLY", blade, laser);
	return 0;
}

int main(int argc, char **argv)
{
	int verbosity = 0;
	if ((argc>=3)&&(strcmp("-v", argv[1])==0)) {
		verbosity = atoi(argv[2]);
		if(verbosity<0) {
			verbosity = 0;
		}
	}
	spOpenLog(SCPI_DAEMON_NAME, verbosity);
	if (getSingletonLock(SCPI_PID_FILE)<0) {
		spLogError("Another instance of %s is already running, aborting\n", SCPI_DAEMON_NAME);
		fprintf(stdout, "Another instance of %s is already running, aborting\n", SCPI_DAEMON_NAME);
		exit(1);
	}
	

	// register callback functions
	registerCallback(ID_RESET, execReset);
	
	// starts the RPC server to recieve SCPI commands over VXI11
	startSCPI();
	// Only returns on fatal errors	
	return 1;
}
