/* **************************************************************** */
/*								    */
/*			Testing Readline			    */
/*								    */
/* **************************************************************** */

/* Copyright (C) 1987-2009 Free Software Foundation, Inc.

   This file is part of the GNU Readline Library (Readline), a library for
   reading lines of text with interactive input and history editing.

   Readline is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Readline is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Readline.  If not, see <http://www.gnu.org/licenses/>.
*/

#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif

#include <stdio.h>
#include <sys/types.h>

#ifdef HAVE_STDLIB_H
#  include <stdlib.h>
#else 
extern void exit();
#endif

#include <readline/readline.h>
#include <readline/history.h>

#include <getopt.h>

extern HIST_ENTRY **history_list ();

#include "vxi11_user.h"
#define BUF_LEN 1024

static struct option long_options[] = {
	{"file", required_argument, 0, 'f'},
	{"command", required_argument, 0, 'c'},
	{"device", required_argument, 0, 'd'},
	{"help", no_argument, 0, 'h'},
	{0, 0, 0, 0},
};

void usage(void)
{
	printf("vxi11_console [OPTIONS] YOUR.INST.IP.ADDR [DEVICE_NAME]\n");
	printf("communicate with device via vxi11 protocol\n\n");
	printf("OPTIONS:\n");
	printf("\t-f, --file\t\trun commands line by line in the given file until eof while printing results\n");
	printf("\t-c, --command\t\trun command given as argument, print results and disconnect\n");
	printf("\t-d, --device\t\tset the device name\n");
	printf("\t-h, --help\t\tshow this screen\n");
	printf("\n");
}

static FILE *commands_file = NULL;
static char *command_arg = NULL;
static bool command_arg_done = false;
static int readline_mode = 0;

char * next_cmd()
{
	char *cmd;

	/* if we have a command file send that */
	if (commands_file) {
		cmd = (char *)malloc(256);
		memset(cmd, 0, 256); /* initialize command string */
		if (!fgets(cmd, 256, commands_file))
			return NULL;
		cmd[strlen(cmd)-1] = 0;	/* just gets rid of the \n */
		return cmd;
	}
	
	/* else if we have a command line arg just send that */
	if (command_arg) {
		if (command_arg_done)
			return NULL;
		command_arg_done = true;
		return command_arg;
	}

	/* finally if neither, we are in interactive mode */
	readline_mode = 1;
	return readline("vxi11$ ");
}

main (int argc, char *argv[])
{
	static char *device_ip;
	static char *device_name = NULL;
	char buf[BUF_LEN];
	int ret;
	long bytes_returned;
	CLINK *clink;   
	int done;
	int option_index;
	char *cmd = NULL;
	int c;
	done = 0;

	clink = new CLINK;

	/* process options */
	while ((c = getopt_long (argc, argv, "f:c:h", long_options, &option_index)) != -1) {
		switch (c) {
			case 'f':
				commands_file = fopen(optarg, "r");
				if (!commands_file) {
					printf("error: unable to open commands file\n");
					exit(1);
				}
				break;
			case 'c':
				command_arg = (char *)malloc(strlen(optarg) + 1);
				if (!command_arg) {
					printf("error: memory error\n");
					exit(1);
				}
				memcpy(command_arg, optarg, strlen(optarg) + 1);
				break;
			case 'd':
				device_name = optarg;
				break;
			case 'h':
				usage();
				exit(0);
			case '?':
			default:
				printf("error: invalid option\n\n");
				usage();
				exit(1);
		}
	}

	/* process mandatory arguments */
	if (optind >= argc) {
		printf("parameter YOUR.INST.IP.ADDR is mandatory !\n");
		usage();
		exit(1);
	}
	device_ip = argv[optind++];

	/* open connection */
	if (device_name == NULL) {
		ret = vxi11_open_device(device_ip,clink);
	} else {
		device_name = argv[2];
		ret = vxi11_open_device(device_ip,clink,device_name);
	}
	if (ret != 0) {
		printf("Error: could not open device %s, quitting\n",device_ip);
		exit(2);
	}

	while (!done)
	{
		/* pop next command from what ever input method were using */
		cmd = next_cmd();
		if (!cmd)
			break;

		/* If there is anything on the line, print it and remember it. */
		if (readline_mode && *cmd)
			add_history (cmd);

		/* Check for `command' that we handle. */
		if (strcmp (cmd, "quit") == 0 || strcmp (cmd, "q") == 0 )
			done = 1;

		if (readline_mode && strcmp (cmd, "list") == 0) {
			HIST_ENTRY **list;
			register int i;

			list = history_list();
			if (list) {
				for (i = 0; list[i]; i++)
					fprintf (stderr, "%d: %s\r\n", i, list[i]->line);
			}
		}

		if (vxi11_send(clink, cmd) < 0) {
			printf("Error sending command..\n");
			break;
		}

		if (strstr(cmd, "?") != 0) {
			memset(buf, 0, BUF_LEN);
			bytes_returned = vxi11_receive(clink, buf, BUF_LEN);
			if (bytes_returned > 0) {
				printf("%s\n",buf);
			}
			else if (bytes_returned == -15) {
				printf("*** [ NOTHING RECEIVED ] ***\n");
			}
			else
				break;
		}

		free(cmd);
	}

	/* cleanup */
	printf("Closing connection.\n");
	ret=vxi11_close_device(device_ip,clink);
	exit (0);
}


