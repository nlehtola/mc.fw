#include "defaultBackendCallbacks.h"
#include "Units.h"
#include "version.h"
#include <string.h>

// Operation

// Questionable

// System
//int execError(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)
int execVersion(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)
{
	size_t n = 0;
	spLogDebug(1, "executing Version: %s", SCPI_COMPLIANCE_VERSION);
	n += snprintf(response+n, max-n, SCPI_COMPLIANCE_VERSION);
	return n;
}

int execIDN(size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)
{
	spLogDebug(1, "executing IDN");
	// companyName,ModelNumber,SerialNumber,HW1.0FW3.5
	size_t n=0;
	n += snprintf(response+n, max-n, "%s,", getCompanyNameString());
	n += snprintf(response+n, max-n, "%s,", getInstrumentModelNumber());
	n += snprintf(response+n, max-n, "%s,", getInstrumentSerialNumber());
	n += snprintf(response+n, max-n, "%s", TLSX_VERSION);
	return n;
}

void registerDefaultCallbacks()
{
	registerCallback(ID_VERS, execVersion);
	registerCallback(ID_IDN, execIDN);
}

