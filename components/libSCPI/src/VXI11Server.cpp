/**
 * @file VXI11Server.cpp
 * @brief RPC handlers for the VXI11 server
 * Refer to the VXI-11 Revision 1.0 July 17, 1995 specification at
 * http://www.vxibus.org/specifications.html for more details on the VXI-11
 * protocol
 */

#include "vxi11.h"
#include "logging.h"
#include "ClientConnections.hpp"
#include <arpa/inet.h>
#include <rpc/pmap_clnt.h>
#include <signal.h>

using namespace std;

#define MAX_RECV_SIZE 2048
#define READ_BUFFER_SIZE 2048

// Device_ReadResp reason flags
#define END (1<<2)
#define CHR (1<<1)
#define REQCNT (1<<0)

// Device_Flags
#define TERMCHRSET (1<<7)
#define DEV_END (1<<3)
#define WAITLOCK (1<<0)

// VXI-11: B.5.2
// Function error codes for Device_Error
#define VXI_SUCCESS							0
#define VXI_SYNTAX_ERROR					1
#define VXI_DEVICE_NOT_ACCESSIBLE			3
#define VXI_INVALID_LINK_IDENTIFIER			4
#define VXI_PARAMETER_ERROR					5
#define VXI_CHANNEL_NOT_ESTABLISHED			6
#define VXI_OPERATION_NOT_SUPPORTED			8
#define VXI_OUT_OF_RESOURCES				9
#define VXI_DEVICE_ALREADY_LOCKED			11
#define VXI_NO_LOCK_HELD					12
#define VXI_IO_TIMEOUT						15
#define VXI_IO_ERROR						17
#define VXI_INVALID_ADDRESS					21
#define VXI_ABORT							23
#define VXI_CHANNEL_ALREADY_ESTABLISHED		29

// Timeout (times in ms)
// overide the clients request for a timeout if it is longer than this
#define MAX_IO_TIMEOUT 2000
#define MAX_LOCK_TIMEOUT 1000

bool_t
device_abort_1_svc(Device_Link* argp, Device_Error* result, struct svc_req* rqstp)
{
	spLogError("Error, device_abort not implimented");
	result->error = VXI_OPERATION_NOT_SUPPORTED;
	return 1;
}

int
device_async_1_freeresult (SVCXPRT* transp, xdrproc_t xdr_result, caddr_t result)
{
	xdr_free (xdr_result, result);
	spLogDebug(2, "device_async: freeresult");
	return 1;
}

bool_t
create_link_1_svc(Create_LinkParms* argp, Create_LinkResp* result, struct svc_req* rqstp)
{
	result->error = VXI_SUCCESS;
	result->lid = -1;
	spLogDebug(2, "create_link start");
	
	// out
	Client* cli = getNewClient(argp->device);
	if (cli==NULL) {
		// TODO distinguish between error types VXI_INVALID_ADDRESS, VXI_OUT_OF_RESOURCES (RULE B.6.5)
		result->error = VXI_DEVICE_NOT_ACCESSIBLE;
		spLogError("Error, Cound not create new linkid");
		spLogDebug(2, "create_link error: device not accessible");
		return 1;
	}
	
	// attempt to aquire device lock
	if (argp->lockDevice==1) {
		spLogDebug(2, "create_link error: locking not implimented");
		spLogWarning("Warning, Locking not implimented");
		result->error = VXI_DEVICE_ALREADY_LOCKED;
		return 1;
				
		/*// attempt to aquire lock
		if (lockDevice(linkid, 5)!=CLIENT_SUCCESS) {
			result->error = VXI_DEVICE_ALREADY_LOCKED;
			// and we want to free the deviceHandle
			spLogDebug(2, "create_link error: device already locked");
			return 1;
		}*/
	}

	// tell the client some things about us
	result->maxRecvSize = MAX_RECV_SIZE;
	
	// find the port of the RPC service which handles aborts (DEVICE_ASYNC)
	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(0);
	inet_pton(AF_INET, "127.0.0.1", &addr.sin_addr);
	unsigned short port = pmap_getport(&addr, DEVICE_ASYNC, DEVICE_ASYNC_VERSION, IPPROTO_TCP);
	spLogDebug(3, "abort port: %d", port);
	
	result->lid = cli->getLinkid();
	result->abortPort = port;	//means nothing yet TODO
	
	spLogDebug(2, "create_link end");
	return 1;
}

bool_t
device_write_1_svc(Device_WriteParms* argp, Device_WriteResp* result, struct svc_req* rqstp)
{
	// TODO RULE B.6.17
	// TODO RULE B.6.18

	result->error = VXI_SUCCESS;
	spLogDebug(2, "device_write start: linkid: %ld", argp->lid);
	
	// check lid
	Client* cli = findClient(argp->lid);
	if (cli==NULL) {
		//TODO, there are possibly other reasons
		result->error = VXI_INVALID_LINK_IDENTIFIER;
		spLogDebug(2, "device_write error: client sent invalid linkid: %ld", argp->lid);
		return 1;
	}
	
	// check that buffer size has not been exceeded
	if ((argp->data.data_len)>MAX_RECV_SIZE) {
		result->error = VXI_PARAMETER_ERROR;
		spLogDebug(2, "device_write error: client parameter error: data_len = %u"
				" is > maximum buffer size: %u", argp->data.data_len, MAX_RECV_SIZE);
		return 1;
	}

	// The client indicates whether this is an end packet or not
	// non end packets must be buffered
	bool endchar = false;
	if ((argp->flags)&DEV_END) {
		endchar = true;
		spLogDebug(3, "device_write: ^END indicator set by client");
	}

	int numWritten = 0;
	if (argp->data.data_len>0) {
		// TODO, figure out who is protecting us from buffer overflows if data_len is wrong
		// send command to SCPI server
		numWritten = cli->writeBlock(argp->data.data_val, argp->data.data_len, endchar);
		if (numWritten!=((int)argp->data.data_len)) {
			if (numWritten==CLIENT_IO_TIMEOUT) {
				// B.6.19
				spLogDebug(1, "device_write: IO timeout");
				result->error = VXI_IO_TIMEOUT;
			} else if (numWritten==CLIENT_IO_ERROR) {
				spLogDebug(1, "device_write: IO error");
				result->error = VXI_IO_ERROR;
			} else {
				// assume IO error
				spLogDebug(1, "device_write: unknown write error, reported IO error");
				result->error = VXI_IO_ERROR;
			}
		}
	} else {
		// argp->data.data_len==0 is not an error
	}
	
	if (numWritten<0) {
		numWritten=0;
	}
	result->size = numWritten;

	spLogDebug(2, "device_write end");
	return 1;
}

// TODO: make this compliant:
bool_t
device_read_1_svc(Device_ReadParms* argp, Device_ReadResp* result, struct svc_req* rqstp)
{
	result->error = VXI_SUCCESS;
	
	spLogDebug(2, "device_read start: linkid: %ld", argp->lid);

	// to indicate to device_core_1_freeresult that this memory does not need to be freed
	result->data.data_val = NULL;
	result->data.data_len = 0;
	
	// check lid
	Client* cli = findClient(argp->lid);
	if (cli==NULL) {
		//TODO, there are possibly other reasons
		result->error = VXI_INVALID_LINK_IDENTIFIER;
		spLogDebug(2, "device_read error: client sent invalid linkid: %ld", argp->lid);
		return 1;
	}
	
	// termination condidtions
	int termChar = EOF;
	result->reason = 0;
	
	if(argp->flags&TERMCHRSET) {
		termChar = (char)argp->termChar;
	}
	
	unsigned long maxsize = argp->requestSize;
	if (maxsize>READ_BUFFER_SIZE) {
		maxsize = READ_BUFFER_SIZE;
	}
	
	int e;
	if ((e=cli->readBlock(result->data.data_val, result->data.data_len, maxsize, termChar))!=CLIENT_NOENDCHAR) {
		if (e==CLIENT_SUCCESS) {
			spLogDebug(3, "device_read: endchar found");
			result->reason |= END;
		} else if (e==CLIENT_IO_TIMEOUT) {
			result->error = VXI_IO_TIMEOUT;
			spLogDebug(2, "device_read error, io timeout");
			return 1;
		} else {
			result->error = VXI_IO_ERROR;
			spLogDebug(2, "device_read error, io error");
			return 1;
		}
	}
	
#if DEBUG_LEVEL >= 3
	stringstream ss;
	ss << "device_read data read sucessfully: ";
	for(unsigned int i=0; i<result->data.data_len; ++i) {
		ss << ((int)(unsigned char)(result->data.data_val[i])) << " ";
	}
	spLogDebug(3, ss.str().c_str());
#endif

	// set reason bits

	// Is the terminating char a termchar?
	if (result->data.data_val[result->data.data_len-1]==termChar) {
		spLogDebug(3, "device_read: termchar found");
		result->reason |= CHR;
	}
	// was the number of returned bytes the maximum requested?
	if (result->data.data_len==argp->requestSize) {
		spLogDebug(3, "device_read: REQCNT found");
		result->reason |= REQCNT;
	}
	// if none of the reasons are set then the buffer must be full, this will implicitly leave all reason bits 0
	// This indicates to the client that bytes are still on the queue and that another read should be done

	spLogDebug(2, "device_read end");
	return 1;
}

bool_t
device_readstb_1_svc(Device_GenericParms* argp, Device_ReadStbResp* result, struct svc_req* rqstp)
{
	result->error = VXI_SUCCESS;
	
	spLogDebug(2, "device_readstb start: linkid: %ld", argp->lid);
	
	// check lid
	Client* cli = findClient(argp->lid);
	if (cli==NULL) {
		//TODO, there are possibly other reasons
		result->error = VXI_INVALID_LINK_IDENTIFIER;
		spLogDebug(2, "device_readstb error: client sent invalid linkid: %ld", argp->lid);
		return 1;
	}
	
	uint8_t ret = cli->getStatusByteMAV();
	if (ret<0) {
		result->error = VXI_IO_ERROR;
		spLogDebug(2, "device_readstb error: io error");
		return 1;
	}
	result->stb = ret;
	
	spLogDebug(2, "device_readstb end");
	return 1;
}

bool_t
device_trigger_1_svc(Device_GenericParms* argp, Device_Error* result, struct svc_req* rqstp)
{
	spLogError("Error, device_trigger not implimented");
	result->error = VXI_OPERATION_NOT_SUPPORTED;
	return 1;
}

bool_t
device_clear_1_svc(Device_GenericParms* argp, Device_Error* result, struct svc_req* rqstp)
{
	result->error = VXI_SUCCESS;
	
	spLogDebug(2, "device_clear start: linkid: %ld", argp->lid);
	
	// check lid
	Client* cli = findClient(argp->lid);
	if (cli==NULL) {
		//TODO, there are possibly other reasons
		result->error = VXI_INVALID_LINK_IDENTIFIER;
		spLogDebug(2, "device_clear error: client sent invalid linkid: %ld", argp->lid);
		return 1;
	}
	
	int ret = cli->deviceClear();
	if (ret<0) {
		result->error = VXI_IO_ERROR;
		spLogDebug(2, "device_clear error: io error");
		return 1;
	}
	
	spLogDebug(2, "device_clear end");
	return 1;
}

bool_t
device_remote_1_svc(Device_GenericParms* argp, Device_Error* result, struct svc_req* rqstp)
{
	spLogError("Error, device_remote not implimented");
	result->error = VXI_OPERATION_NOT_SUPPORTED;
	return 1;
}

bool_t
device_local_1_svc(Device_GenericParms* argp, Device_Error* result, struct svc_req* rqstp)
{
	spLogError("Error, device_local not implimented");
	result->error = VXI_OPERATION_NOT_SUPPORTED;
	return 1;
}

bool_t
device_lock_1_svc(Device_LockParms* argp, Device_Error* result, struct svc_req* rqstp)
{
	spLogError("Error, device_lock not implimented");
	result->error = VXI_OPERATION_NOT_SUPPORTED;
	return 1;
}

bool_t
device_unlock_1_svc(Device_Link* argp, Device_Error* result, struct svc_req* rqstp)
{
	spLogError("Error, device_unlock not implimented");
	result->error = VXI_OPERATION_NOT_SUPPORTED;
	return 1;
}

bool_t
device_enable_srq_1_svc(Device_EnableSrqParms* argp, Device_Error* result, struct svc_req* rqstp)
{
	spLogError("Error, device_enable not implimented");
	result->error = VXI_OPERATION_NOT_SUPPORTED;
	return 1;
}

bool_t
device_docmd_1_svc(Device_DocmdParms* argp, Device_DocmdResp* result, struct svc_req* rqstp)
{
	spLogError("Error, device_docmd not implimented");
	result->error = VXI_OPERATION_NOT_SUPPORTED;
	return 1;
}

bool_t
destroy_link_1_svc(Device_Link* argp, Device_Error* result, struct svc_req* rqstp)
{
	spLogDebug(2, "destroy_link start: linkid: %ld", *argp);
	result->error = VXI_SUCCESS;
	
	// TODO, if locked, unlock
	// TODO remove from using interupt mechanism
	if (!removeClient(*argp)) {
		spLogDebug(2, "destroy_link error");
		result->error = VXI_INVALID_LINK_IDENTIFIER;
		return 1;
	}

	spLogDebug(2, "destroy_link end");
	return 1;
}

bool_t
create_intr_chan_1_svc(Device_RemoteFunc* argp, Device_Error* result, struct svc_req* rqstp)
{
	spLogError("Error, create interupt channel not implimented");
	result->error = VXI_OPERATION_NOT_SUPPORTED;
	return 1;
}

bool_t
destroy_intr_chan_1_svc(void* argp, Device_Error* result, struct svc_req* rqstp)
{
	spLogError("Error, destroy interupt channel not implimented");
	result->error = VXI_OPERATION_NOT_SUPPORTED;
	return 1;
}

int
device_core_1_freeresult (SVCXPRT* transp, xdrproc_t xdr_result, caddr_t result)
{
	spLogDebug(2, "Freeing device core result");
	xdr_free (xdr_result, result);
	return 1;
}

bool_t
device_intr_srq_1_svc(Device_SrqParms* argp, void* result, struct svc_req* rqstp)
{
	spLogError("Error, device interupt service request not implimented (because this is not a client)");
	return 1;
}

int
device_intr_1_freeresult (SVCXPRT* transp, xdrproc_t xdr_result, caddr_t result)
{
	xdr_free (xdr_result, result);
	return 1;
}
