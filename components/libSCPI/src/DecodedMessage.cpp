#include "DecodedMessage.hpp"

DecodedMessage::DecodedMessage(BackendCallbackID callbackid, TaskID taskid, uint8_t bladeid, uint8_t channelid, int numValues, double* values)
{
	this->id = callbackid;
	this->task = taskid;
	this->blade = bladeid;
	this->channel = channelid;
	this->numValues = numValues;
	this->values = values;
}

DecodedMessage::~DecodedMessage()
{	
	delete(values);
}

int DecodedMessage::execute(string& response)
{
	//run function
	char buffer[1024];
	int n = runCallback(1024, buffer, id, task, blade, channel, numValues, values);
	if(n>0) {
		response = string(buffer, n);
	} else {
		response = "";
	}
	return n;
}

