#include "backendCallbackTable.h"
#include "logging.h"
#include "error_codes.h"
#include "InstrumentConfig.h"

typedef struct {
	BackendCallbackID id;
	int (*funcPtr) (size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values);
} TableEntry;

static TableEntry callbackTable[] = {
// VOA
	{ID_READDC, NULL},
	{ID_COLZERO, NULL},
	{ID_ACLSTATE, NULL},
	{ID_LOCK, NULL},
	{ID_CALZERO, NULL},
	{ID_ATTEN_MODE, NULL},
	{ID_CONTMODE, NULL},
	{ID_ARESOLUTION, NULL},
	{ID_INSERTION_LOSS_DB, NULL},
	{ID_ATTENUATION_DB, NULL},
	{ID_ATTENUATION_RAW, NULL},
	{ID_ATTENUATOR_TEMP_C, NULL},
	{ID_INOFFSET, NULL},
	{ID_ATTEN_VEL_DBPS, NULL},
	{ID_RATTENUATION, NULL},
	{ID_INREFERENCE, NULL},
	{ID_WAVELENGTH_NM, NULL},
	{ID_APMODE, NULL},
	{ID_DTOLERANCE, NULL},
	{ID_OUTOFFSET, NULL},
	{ID_POWER_DBM, NULL},
	{ID_POWER_RAW, NULL},
	{ID_DARK_POWER_DBM, NULL},
	{ID_START_DARK_NULLING, NULL},
	{ID_AVERAGING_TIME_S, NULL},
	{ID_OUTREFERENCE, NULL},
	{ID_RPOWER, NULL},
	{ID_STATE, NULL},
// RF Clock
	{ ID_RFCL_POWER, NULL },
	{ ID_RFCL_FREQUENCY, NULL },
	{ ID_RFCL_OUTPUT, NULL },
// PPG
	{ ID_PPG, NULL },
	{ ID_PPG_CHANNEL, NULL },
	{ ID_PPG_BITRATE, NULL },
	{ ID_PPG_VPKPK, NULL },
	{ ID_PPG_BIT_PATTERN, NULL },
	{ ID_PPG_SKEW, NULL },
	{ ID_PPG_TAP, NULL },
// System
	{ID_ERROR, NULL},	//	execError},
	{ID_VERS, NULL},
	{ID_DATE, NULL},
	{ID_TIME, NULL},
// Status
	{ID_PRES, NULL},
// GPIB:
	{ID_CLS, NULL},	// handled as special case
	{ID_ESE, NULL},	// handled as special case
	{ID_ESR, NULL},	// handled as special case
	{ID_IDN, NULL},
	{ID_IDN_SLOT, NULL},
	{ID_OPC, NULL},
	{ID_OPC_SLOT, NULL},
	{ID_OPC_CHAN, NULL},
	{ID_OPT, NULL},
	{ID_OPT_SLOT, NULL},
	{ID_RST, NULL},	//	execRST}, // TODO, handled as special case but also called here
	{ID_WAI, NULL},	// handled as special case, NOP
	{ID_STB, NULL},	// handled as special case
	{ID_TST, NULL},
	{ID_NONE, NULL}
};

int runCallback(size_t max, char* response, BackendCallbackID id, TaskID task, uint8_t blade, uint8_t channel, int numValues, const double* values)
{
	int (*funcPtr) (size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values)=NULL;
	int i;
	//lookup command
	for (i=0; callbackTable[i].id!=ID_NONE; ++i) {
		if (callbackTable[i].id==id) {
			funcPtr = callbackTable[i].funcPtr;
			break;
		}
	}
	if (funcPtr==NULL) {
		spLogError("backendCallbackTable.c runCallback: no callback has been registered for id %d", id);
		return LIBSCPI_ERR_CALLBACK_NOT_REGISTERED;
	}

	if (channel<0||channel>getMaxNumberOfChannels()) {
		spLogDebug(2,"backendCallbackTable.c runCallback: channel id was incorrect: %d", channel);
		return LIBSCPI_ERR_INVALID_CHANNEL_ID;
	}
	int e = funcPtr(max, response, task, blade, channel, numValues, values);
	if (e<0)
		spLogError("runCallback: callback with id %d returned error code: %d", id, e);
	return e;
}

int registerCallback(BackendCallbackID id, int (*funcPtr) (size_t max, char* response, TaskID task, int blade, int channel, int numValues, const double* values))
{
	int i;
	for (i=0; callbackTable[i].id!=ID_NONE; ++i) {
		if (callbackTable[i].id==id) {
			callbackTable[i].funcPtr = funcPtr;
			spLogDebug(4, "backendCallbackTable.c registerCallback: callback registered, id %d", id);
			return 0;
		}
	}
	spLogError("backendCallbackTable.c registerCallback: id not found: %d", id);
	return LIBSCPI_ERR_INVALID_CALLBACK_ID;
}

