#include "InstrumentConfig.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static char* companyName = NULL;
static char* chassisModelNumber = NULL;
static char* chassisSerialNumber = NULL;
static char* HWVersion = NULL;
static bool isBenchtop = false;
static unsigned int maxNumBlades = 9;
static unsigned int maxNumChannels = 4;
static bool isFakekblade = false;
static char* hardcodedChannelMask = NULL;

int loadConfig(char* configFile)
{
	FILE* cfgFilePtr = NULL;
	
	// if an override has been provided by the command line try to open it
	if (configFile)
		cfgFilePtr = fopen(configFile, "r");
	if (cfgFilePtr==NULL) {
		// if opening it failed attempt to open the default
		cfgFilePtr = fopen(CHASSIS_CONFIG_FILE, "r");
		if (cfgFilePtr==NULL) {
			// if opening the default failed attempt to open the fallback
			cfgFilePtr = fopen(BACKUP_CONFIG_FILE, "r");
			if (cfgFilePtr==NULL) {
				spLogError("Error, Could not open a config file for reading");
				return -1;
			} else {
				spLogNotice("Warning, default config file not found. Using \"%s\"", BACKUP_CONFIG_FILE);
			}
		}
		spLogDebug(1, "Default config file loaded: %s", CHASSIS_CONFIG_FILE);
	} else {
		spLogNotice("Non-default config file loaded from command line: %s", configFile);
	}
	
	char header[128];
	char data[128];
	while(fscanf(cfgFilePtr, "%128s\n%128s\n", (char*)&header, (char*)&data)==2) {
		if (strncmp(header, "companyName", 128)==0) {
			companyName = strndup(data, 128);
		} else if (strncmp(header, "chassisModelNumber", 128)==0) {
			chassisModelNumber = strndup(data, 128);
		} else if (strncmp(header, "chassisSerialNumber", 128)==0) {
			chassisSerialNumber = strndup(data, 128);
		} else if (strncmp(header, "HWVersion", 128)==0) {
			HWVersion = strndup(data, 128);
		} else if (strncmp(header, "maxNumBlades", 128)==0) {
			maxNumBlades = atoi(data);
		} else if (strncmp(header, "maxNumChannels", 128)==0) {
			maxNumChannels = atoi(data);
		} else if (strncmp(header, "isBenchtop", 128)==0) {
			if (strncmp(data, "true", 128)==0) {
				isBenchtop = 1;
			}
		} else if (strncmp(header, "fakekblade", 128)==0) {	
			isFakekblade = true;
		} else if (strncmp(header, "hardcodedChannelMask", 128)==0) {
			hardcodedChannelMask = strndup(data, 128);
		}
	}
	fclose(cfgFilePtr);
	return 0;
}

const char* getCompanyNameString()
{
	return companyName;
}
const char* getInstrumentModelNumber()
{
	return chassisModelNumber;
}
const char* getInstrumentSerialNumber()
{
	return chassisSerialNumber;
}
const char* getHWVersionNumber()
{
	return HWVersion;
}

bool getIsBenchtop()
{
	return isBenchtop;
}
		
unsigned int getMaxNumberOfBlades()
{
	return maxNumBlades;
}
	
unsigned int getMaxNumberOfChannels()
{
	return maxNumChannels;
}

bool getIsFakekblade()
{
	return isFakekblade;
}

const char* getHardcodedChannelMask()
{
	return hardcodedChannelMask;
}

