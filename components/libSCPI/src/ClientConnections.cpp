/**
 * @file ClientConnections.cpp
 * @brief
**/

#include "ClientConnections.hpp"
#include "error_codes.h"

#include <iostream>
#include <sstream>
#include <vector>
#include <math.h>
#include <algorithm>
#include <strings.h>
using namespace std;

//global state of exisiting connection objects
map<long, Client*> connections;

// MRU list of open linkid's
// when a linkid is used it is touched in this list by removing it's entry
// then readding it at the back of the list
// thus the top of this list is always the least recently used
vector<long> linkidMRU;
#define MAX_OPEN_LINKS 32

// TODO This might be better if it wasn't a global, or if it is, put it somewhere better
SCPIServerState SCPIstate;

Client::Client(long linkid)
{
	spLogDebug(4, "Client()");
	this->linkid = linkid;

	// Power On as defined by IEEE 488.2
	responseBuffer = "";
	statusByteMask = 0;	// IEEE 488.2 Table 11-2 page 153
	errorByte = 0;
	errorByteMask = 0;	// IEEE 488.2 11.5.1.3.4
}

Client::~Client()
{
	spLogDebug(2, "~Client(): linkid: %ld", linkid);
}

/** @brief reads a block of data from the SCPI server
 *  @param[out] data Pointer to where the read data should be stored. This must
 * be freed using free()
 *  @param[out] length The length of data will be stored here
 *  @param[in] maxLength The maximum amount of data to be read
 *  @param[in] termchar Read until this character is found TODO not implemented
 *  @return CLIENT_SUCCESS when the entire buffer has been returned
 *  @return CLIENT_NOENDCHAR when maxLength is less than the reponse buffer.
 *  The remaining data is left in the buffer.
 *  @return CLIENT_IO_TIMEOUT when there is no data to be read. In a
 *  multithreaded server this function would block until all commands have
 *  successfully executed.
 */
int Client::readBlock(char*& data, unsigned int &length, unsigned long maxLength, char termchar)
{
	data = NULL;
	length = 0;

	spLogDebug(3, "linkid: %ld, Client::readBlock responseBuffer: \"%s\" length: %d", linkid, responseBuffer.c_str(), responseBuffer.length());
	if (responseBuffer!="") {
		length = responseBuffer.length();
		if (length>maxLength) {
			data = (char*)malloc(maxLength);
			memcpy(data, responseBuffer.c_str(), maxLength);
			responseBuffer = responseBuffer.substr(maxLength);
			length = maxLength;
			spLogDebug(2, "linkid: %ld, Client::readBlock returned: \"%.*s\"...", linkid, length, data);
			// This means that more data is available to read and the client
			// should perform another read to get the rest of the data
			return CLIENT_NOENDCHAR;
		} else {
			data = (char*)malloc(length);
			memcpy(data, responseBuffer.c_str(), length);
			responseBuffer = "";
			spLogDebug(2, "linkid: %ld, Client::readBlock returned: \"%.*s\"", linkid, length, data);
			return CLIENT_SUCCESS;
		}
	} else {
		// This means that there was no data to read, which would cause a
		// "timeout" when blocking for data.
		// In a multithreaded server this may need to block until there are
		// no commands waiting to be excecuted. In practice we do not need to
		// worry about this unless write commands can return without completing
		// excecution. In theory 2 clients could use the same linkid at the
		// same time (1 of them would have to be spoofing, or have a bug),
		// which would cause them to steal each others reponses anyway
		return CLIENT_IO_TIMEOUT;
	}
}

/** @brief writes a block of data to the SCPI server
 *  @param[in] data Pointer to the data to be written
 *  @param[in] length The length of data to be written
 *  @param[in] endchar If endchar is true the END flag shall be ascociated with
 *  the last byte of data. If endchar is false writeBlock will buffer data until
 *  the next write.
 *  @return length The number of characters written is returned
 *  @return CLIENT_IO_ERROR when an unrecoverable error occures and write
 *  cannot be completed. The input buffer has been flushed. The command will not
 *  be excecuted. Note: command excecution errors will not return an error here.
 *  These will be reported on the statusByte
 */
int Client::writeBlock(char* data, unsigned int length, bool endchar)
{
	spLogDebug(2, "linkid: %ld, Client::writeBlock recieved: \"%.*s\"", linkid, length, data);

	//TODO put me in excecute
	vector<DecodedMessage*> decodedMessages;
	int e = parseSCPI(data, length, &decodedMessages);
	if (e<0) {
		if (e==PARSER_PARSE_ERROR) {
			// we need to add the parse error in the status byte
			errorByte |= SCPI_SESR_CME;	//IEEE 488.2 11.5.1.1.4

			for (vector<DecodedMessage*>::iterator i=decodedMessages.begin(); i!=decodedMessages.end(); ++i) {
				delete(*i);
				*i = NULL;
			}
			// The VXI11 write was a success
			// but don't execute the DecodedMessage
			// and don't do anything to the reponse buffer
			return length;
		} else if (e==PARSER_ERROR) {
			// this write failed because the parser couldn't parse the message
			return CLIENT_IO_ERROR;
		}
	}

	string response = "";
	for (vector<DecodedMessage*>::iterator i=decodedMessages.begin(); i!=decodedMessages.end(); ++i) {
		// append the ";" delimiter between multiple reponses
		if (i!=decodedMessages.begin()) {
			response += ";";
		}
		int e = COMMAND_SUCCESS;
		if ((*i)->id==ID_CLS) {
			// *CLS is a special command which clears the reponse buffer of the current stream
			deviceClear();
			response = "";
			// excecuting *CLS in the same line as other commands is offically
			// undefined. What will actually happen is that the results of
			// anything following the CLS will be added to the response buffer
		} else if ((*i)->id==ID_SRE) {
			if((*i)->task==TASK_COMMAND) {
				if ((*i)->numValues==1) {
					//rounded to int according to IEEE 488.2 10.10.3
					statusByteMask = 0xFF&int(round((*i)->values[0]));
					spLogDebug(1, "executing *SRE: statusByteMask %d", statusByteMask);
				} else {
					spLogDebug(1, "executing *SRE: error, invalid number of values from SCPI arguments");
					e = -1;
				}
			} else {
				spLogDebug(1, "executing *SRE?: returning: %d", statusByteMask);
				stringstream ss;
				ss << (unsigned int)statusByteMask;
				response += ss.str();
			}
		} else if ((*i)->id==ID_STB) {
			// *STB? is a special command which returns the status byte.
			stringstream ss;
			unsigned int b = getStatusByte();
			ss << b;
			response += ss.str();
			spLogDebug(1, "executing *STB?: returning STB: %d", b);
		} else if ((*i)->id==ID_ESE) {
			if((*i)->task==TASK_COMMAND) {
				if ((*i)->numValues==1) {
					//rounded to int according to IEEE 488.2 10.10.3
					errorByteMask = 0xFF&int(round((*i)->values[0]));
					spLogDebug(1, "executing *ESE: statusByteMask %d", errorByteMask);
				} else {
					spLogDebug(1, "executing *ESE: error, invalid number of values from SCPI arguments");
					e = -1;
				}
			} else {
				spLogDebug(1, "executing *ESE?: returning: %d", errorByteMask);
				stringstream ss;
				ss << (unsigned int)errorByteMask;
				response += ss.str();
			}
		} else if ((*i)->id==ID_ESR) {
			stringstream ss;
			int b = getErrorByte(true);
			ss << b;
			response += ss.str();
			spLogDebug(1, "executing *ERS?: returning SESR: %d", b);
		} else if ((*i)->id==ID_WAI) {
			spLogDebug(1, "executing *WAI: NOP");
			// Since all commands are sequential, OPC always returns 1 and WAI is always a NOP
		} else if ((*i)->id==ID_OPC && (*i)->task==TASK_COMMAND) {
			spLogDebug(1, "executing *OPC");
			errorByte |= SCPI_SESR_OPC;
		} else {
			string tempResponse = "";
			e = (*i)->execute(tempResponse);
			response += tempResponse;
		}

		// This is important since DecodedMessage's are allocated by new inside
		// the bison parser code
		delete(*i);
		*i = NULL;
		if (e<0) {
			if (e==LIBSCPI_ERR_CALLBACK_NOT_REGISTERED||e==LIBSCPI_ERR_INVALID_CHANNEL_ID) {
				// if a callback hasn't been registered for a valid command
				// or if an invalid channel id was supplied then we treat it
				// as if it was a parse error (IEEE 488.2 11.5.1.1.4)
				errorByte |= SCPI_SESR_CME;
			} else {
				//TODO: distinguish between timeout error and other errors and
				// set questionable/operation bits
				errorByte |= SCPI_SESR_DDE; //IEEE 488.2 11.5.1.1.6
			}
		}
	}
	if (response!="") {
		// This throws away any previous data on the buffer
		responseBuffer = response+"\n";
	}

	for (vector<DecodedMessage*>::iterator i=decodedMessages.begin(); i!=decodedMessages.end(); ++i) {
		delete(*i);
		*i = NULL;
	}
	return length;
}

int Client::lockDevice(unsigned int timeOut)
{
	//TODO
	return CLIENT_SUCCESS;
}

int Client::unlockDevice()
{
	//TODO
	return CLIENT_SUCCESS;
}

/** @brief Returns the linkid of this client
 *  @return the linkid
 */
long Client::getLinkid()
{
	return linkid;
}

/** @brief Returns the status byte (STB) for this client
 *  @return the status byte
 */
uint8_t Client::getStatusByte()
{
	// TODO or with global status byte from instrumentState
	uint8_t statusByte = 0;

	/*if(getOperationRegister() & getOperationRegisterMask())
		statusByte |= SCPI_STB_OPER;

	if(getQuestionableRegister() & getQuestionableRegisterMask())
		statusByte |= SCPI_STB_QUES;*/

	if(getErrorByte(false) & errorByteMask)
		statusByte |= SCPI_STB_SESR;

	if(statusByte & statusByteMask)
		statusByte |= SCPI_STB_MSS;

	return (statusByte&0xFF);
}

/** @brief Returns the status byte (STB) for this client including the MAV bit
 *  @return the status byte
 */
uint8_t Client::getStatusByteMAV()
{
	uint8_t statusByte = getStatusByte();

	// If the response buffer is not empty, set the message available bit
	if(responseBuffer!="")
		statusByte |= SCPI_STB_MAV;

	return (statusByte&0xFF);
}

/** @brief Returns the error byte (SESR) for this client, othewise called the
 * "standard event status register"
 *  @return the status byte
 */
uint8_t Client::getErrorByte(bool clear)
{
	// TODO or with global error byte from instrumentState

	uint8_t temp = errorByte;
	// reading errorByte is destructive
	if (clear)
		errorByte = 0;
	return (temp);
}

/** @brief Performs a device clear on information related to this linkid/client
 *  This is equivilent to a *CLS command.
 *  @return 0
 */
int Client::deviceClear()
{
	// clear the local message queue
	responseBuffer = "";
	// clear the status byte and error queue
	errorByte = 0;
	// we must not clear errorByteMask or statusByteMask: SCPI-99 page 502
	spLogDebug(1, "executing *CLS: Clearing output buffer");
	return 0;
}

// ************************** client managment functions ***********************
Client* findClient(long linkid)
{
	// looks up in the connections map object to find the link id
	// checks that the linkid matches the object
	// returns the connection if it is found, NULL otherwise
	if (linkid==-1) {
		spLogError("Error, VXI11 server tried to find connection with invalid linkid -1");
		return NULL;
	}
	map<long, Client*>::iterator i = connections.find(linkid);
	if (i==connections.end()) {
		spLogDebug(2, "Client for requested linkid %ld could not be found", linkid);
		return NULL;	// connection not found
	}
	if (i->second->getLinkid()==-1) {
		spLogError("Client to SCPI server for linkid %ld has been closed, connection removed", linkid);
		spLogDebug(1, "Removing connection #2, linkid: %d, connections.size(): %d", i->second->getLinkid(), connections.size()-1);
		delete(i->second);
		connections.erase(i);
		return NULL;
	}
	if (linkid!=i->second->getLinkid()) {
		spLogError("Error, linkid in map and linkid of Client differ, connection terminated");
		spLogDebug(1, "Removing connection #3, linkid: %d, connections.size(): %d", i->second->getLinkid(), connections.size()-1);
		linkidMRU.erase(find(linkidMRU.begin(), linkidMRU.end(), i->second->getLinkid()));
		delete(i->second);
		connections.erase(i);
		return NULL;
	}
	// touch the linkid in the MRU list to make it the most recently used linkid
	linkidMRU.erase(find(linkidMRU.begin(), linkidMRU.end(), linkid));
	linkidMRU.push_back(linkid);
	return i->second;
}

// same as findClient except connection is removed from the map
// 0 on failure
// 1 on success
int removeClient(long linkid)
{
	// looks up in the connections map object to find the link id
	// checks that the linkid matches the object
	// returns the connection if it is found, NULL otherwise
	if (linkid==-1) {
		spLogError("Warning, VXI11 server tried to remove connection with invalid linkid -1");
		return 0;
	}
	map<long, Client*>::iterator i = connections.find(linkid);
	if (i==connections.end()) {
		spLogWarning("Warning, linkid: %ld not found, VXI11 server could not remove connection", linkid);
		return 0;
	}

	spLogDebug(1, "Removing connection #1, linkid: %d, connections.size(): %d", i->second->getLinkid(), connections.size()-1);
	linkidMRU.erase(find(linkidMRU.begin(), linkidMRU.end(), i->second->getLinkid()));
	delete(i->second);
	connections.erase(i);

	return 1;		//TODO, this doesn't follow return value convention
}

void removeAllClients()
{
	spLogNotice("Closing all active connections to device");
	for (map<long, Client*>::iterator i=connections.begin(); i!=connections.end(); ++i)
	{
		delete(i->second);
	}
	connections.clear();
	linkidMRU.clear();
}

Client* getNewClient(string device) {
	// in theory each device has its own map of connections
	// with unique linkid's or something
	// since we are only dealing with 1 we ignore it
	if (device!="inst0") {
		spLogError("Error, a VXI11 client tried to create new connection to device %s, only inst0 supported", device.c_str());
		return NULL;
	}

	static long nextLinkid = 0;
	long newLinkid = nextLinkid;
	nextLinkid++;
	Client *temp = new Client(newLinkid);

	map<long, Client*>::iterator i = connections.find(newLinkid);
	if (i!=connections.end()) {
		spLogWarning("Warning reusing existing linkid");
		delete(i->second);
		connections.erase(i);
	}

	// if there are already too many open links
	if (linkidMRU.size()>=MAX_OPEN_LINKS) {
		// remove the linkid of the oldest TCPWorker from the MRU list
		long culledLinkid = linkidMRU.front();
		linkidMRU.erase(linkidMRU.begin());

		// find any clients with the corresponding linkid and remove them (should be exactly 1)
		int i=0;
		while(1) {
			map<long, Client*>::iterator c = connections.find(culledLinkid);
			if (c==connections.end())
				break;
			delete(c->second);
			connections.erase(c);
			i++;
		}
		spLogWarning("While adding new link: %ld, number of open"
			"links exceeds %ld, Culling stale link: %ld", newLinkid,
			MAX_OPEN_LINKS, culledLinkid);
		if (i!=1) {
			spLogError("Warning, number of stale clients removed with linkid %ld "
				"was %d instead of 1. This could be a symptom of memory corruption", culledLinkid, i);
		}
	}
	// add the newly added linkid to the back of the MRU
	linkidMRU.push_back(newLinkid);

	pair<map<long, Client*>::iterator,bool> p = connections.insert(make_pair(newLinkid, temp));
	if (p.second==false) {
		// when p.second is false this indicates that insert failed due to an existing entry
		// We shouldn't ever get here since this should be handled by the previous check
		// If we do get here then the previous connection object will memoryleak
		spLogError("Warning, check for existing linkid failed. Reusing existing linkid. "
					"The original Client object has memory leaked");
	}
	spLogDebug(1, "New connection, linkid: %d, connections.size(): %d", newLinkid, connections.size());

	return temp;
}

