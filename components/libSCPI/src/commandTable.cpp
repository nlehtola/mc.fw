#include "commandTable.hpp"
#include "Units.h"
#include <algorithm>

string convertToUpper(string in)
{
	std::transform(in.begin(), in.end(), in.begin(), ::toupper);
	return in;
}

// checks if a SCPI mnemonic matches its long or short form, then returns the
// trailing digit if it is present. Returns -1 if there is no match, returns 0
// if there is no number, otherwise returns the trailing digit. A trailing 0 is
// illegal and will return -1
// longForm and shortForm are compared to the mnemonic after any trailing digits
// are removed, they must be upper case
int SCPIMnemonicDecode(string& mnemonic, string longForm, string shortForm)
{
	int number;
	string name;
	
	// look for numbers
	unsigned found = mnemonic.find_last_not_of("0123456789");
	if (found==string::npos) {
		// the mnemonic was all number
		return -1;
	}
	if (found+1==mnemonic.length()) {
		// there is no trailing number
		name = mnemonic;
		number = 0;
	} else {
		// there is a trailing number
		name = mnemonic.substr(0, found+1);
		number = atoi(mnemonic.substr(found+1).c_str());
		if (number==0) {
			// the trailing number is a 0
			return -1;
		}
	}
	
	//compare the name part of the header to the candidate and see if it matches
	name = convertToUpper(name);
	if (name==longForm) {
		return number;
	}
	if (name==shortForm) {
		return number;
	}
	return -1;
}

// ############################## ArgumentNodes ##############################

// TODO Boolean Program Data for 0,1 instead of ON/OFF
const ArgumentNode stateArgs[] = {
{NODE_CMD,		STATE_ON,		1, {{CHARACTER_PROGRAM_DATA, "ON"}}			},
{NODE_CMD,		STATE_OFF,		1, {{CHARACTER_PROGRAM_DATA, "OFF"}}		},
{NODE_QUERY,	TASK_QUERY,		0, {}										},
{NODE_QUERY,	TASK_NONE,		0, {}}										};

const ArgumentNode maxMinSetArgs[] = {
{NODE_CMD,		TASK_SET,		1, {{DECIMAL_NUMERIC_PROGRAM_DATA, ""}}		},
{NODE_CMD,		SET_MIN,		1, {{CHARACTER_PROGRAM_DATA, "MIN"}}		},
{NODE_CMD,		SET_MIN,		1, {{CHARACTER_PROGRAM_DATA, "MINIMUM"}}	},
{NODE_CMD,		SET_MAX,		1, {{CHARACTER_PROGRAM_DATA, "MAX"}}		},
{NODE_CMD,		SET_MAX,		1, {{CHARACTER_PROGRAM_DATA, "MAXIMUM"}}	},
{NODE_QUERY,	TASK_MIN, 		1, {{CHARACTER_PROGRAM_DATA, "MIN"}}		},
{NODE_QUERY,	TASK_MIN, 		1, {{CHARACTER_PROGRAM_DATA, "MINIMUM"}} 	},
{NODE_QUERY,	TASK_MAX,		1, {{CHARACTER_PROGRAM_DATA, "MAX"}}		},
{NODE_QUERY,	TASK_MAX, 		1, {{CHARACTER_PROGRAM_DATA, "MAXIMUM"}}	},
{NODE_QUERY,	TASK_SET_POINT, 1, {{CHARACTER_PROGRAM_DATA, "SET"}}		},
{NODE_QUERY,	TASK_QUERY, 	0, {}										},	// returns the current value
{NODE_QUERY,	TASK_ALL, 		1, {{CHARACTER_PROGRAM_DATA, "ALL"}}		},
{NODE_QUERY,	TASK_NONE,		0, {}}										};

const ArgumentNode getMinMaxArgs[] = {
{NODE_QUERY,	TASK_MIN, 		1, {{CHARACTER_PROGRAM_DATA, "MIN"}}		},
{NODE_QUERY,	TASK_MIN, 		1, {{CHARACTER_PROGRAM_DATA, "MINIMUM"}} 	},
{NODE_QUERY,	TASK_MAX,		1, {{CHARACTER_PROGRAM_DATA, "MAX"}}		},
{NODE_QUERY,	TASK_MAX, 		1, {{CHARACTER_PROGRAM_DATA, "MAXIMUM"}}	},
{NODE_QUERY,	TASK_QUERY, 	0, {}										},	// returns the current value
{NODE_QUERY,	TASK_ALL, 		1, {{CHARACTER_PROGRAM_DATA, "ALL"}}		},
{NODE_QUERY,	TASK_NONE,		0, {}}										};

const ArgumentNode maxMinDefaultArgs[] = {
{NODE_CMD,		TASK_SET,		1, {{DECIMAL_NUMERIC_PROGRAM_DATA, ""}} 	},
{NODE_CMD,		SET_MIN,		1, {{CHARACTER_PROGRAM_DATA, "MIN"}}		},
{NODE_CMD,		SET_MIN,		1, {{CHARACTER_PROGRAM_DATA, "MINIMUM"}}	},
{NODE_CMD,		SET_MAX,		1, {{CHARACTER_PROGRAM_DATA, "MAX"}}		},
{NODE_CMD,		SET_MAX,		1, {{CHARACTER_PROGRAM_DATA, "MAXIMUM"}}	},
{NODE_CMD,		SET_DEFAULT,	1, {{CHARACTER_PROGRAM_DATA, "DEF"}}		},
{NODE_CMD,		SET_DEFAULT,	1, {{CHARACTER_PROGRAM_DATA, "DEFAULT"}}	},
{NODE_QUERY,	TASK_QUERY, 	0, {}										},	// returns the current value
{NODE_QUERY,	TASK_MIN, 		1, {{CHARACTER_PROGRAM_DATA, "MIN"}}		},
{NODE_QUERY,	TASK_MIN, 		1, {{CHARACTER_PROGRAM_DATA, "MINIMUM"}} 	},
{NODE_QUERY,	TASK_MAX,		1, {{CHARACTER_PROGRAM_DATA, "MAX"}}		},
{NODE_QUERY,	TASK_MAX, 		1, {{CHARACTER_PROGRAM_DATA, "MAXIMUM"}}	},
{NODE_QUERY,	TASK_DEFAULT, 	1, {{CHARACTER_PROGRAM_DATA, "DEF"}}		},
{NODE_QUERY,	TASK_DEFAULT, 	1, {{CHARACTER_PROGRAM_DATA, "DEFAULT"}}	},
{NODE_QUERY,	TASK_ALL, 		1, {{CHARACTER_PROGRAM_DATA, "ALL"}}		},
{NODE_QUERY,	TASK_NONE,		0, {}} 										};

const ArgumentNode maxMinDefaultSetActArgs[] = {
{NODE_CMD,		TASK_SET,		1, {{DECIMAL_NUMERIC_PROGRAM_DATA, ""}} 	},
{NODE_CMD,		SET_MIN,		1, {{CHARACTER_PROGRAM_DATA, "MIN"}}		},
{NODE_CMD,		SET_MIN,		1, {{CHARACTER_PROGRAM_DATA, "MINIMUM"}}	},
{NODE_CMD,		SET_MAX,		1, {{CHARACTER_PROGRAM_DATA, "MAX"}}		},
{NODE_CMD,		SET_MAX,		1, {{CHARACTER_PROGRAM_DATA, "MAXIMUM"}}	},
{NODE_CMD,		SET_DEFAULT,	1, {{CHARACTER_PROGRAM_DATA, "DEF"}}		},
{NODE_CMD,		SET_DEFAULT,	1, {{CHARACTER_PROGRAM_DATA, "DEFAULT"}}	},
{NODE_QUERY,	TASK_QUERY,		0, {} 										},	// returns the current value
{NODE_QUERY,	TASK_MIN,		1, {{CHARACTER_PROGRAM_DATA, "MIN"}}		},
{NODE_QUERY,	TASK_MIN, 		1, {{CHARACTER_PROGRAM_DATA, "MINIMUM"}} 	},
{NODE_QUERY,	TASK_MAX, 		1, {{CHARACTER_PROGRAM_DATA, "MAX"}}		},
{NODE_QUERY,	TASK_MAX, 		1, {{CHARACTER_PROGRAM_DATA, "MAXIMUM"}}	},
{NODE_QUERY,	TASK_DEFAULT, 	1, {{CHARACTER_PROGRAM_DATA, "DEF"}}		},
{NODE_QUERY,	TASK_DEFAULT,	1, {{CHARACTER_PROGRAM_DATA, "DEFAULT"}}	},
{NODE_QUERY,	TASK_SET_POINT, 1, {{CHARACTER_PROGRAM_DATA, "SET"}}		},
{NODE_QUERY,	TASK_ALL, 		1, {{CHARACTER_PROGRAM_DATA, "ALL"}}		},
{NODE_QUERY,	TASK_NONE,		0, {}} 										};

const ArgumentNode maxMinDefaultSetArgs[] = {
{NODE_CMD,		TASK_SET,		1, {{DECIMAL_NUMERIC_PROGRAM_DATA, ""}} 	},
{NODE_CMD,		SET_MIN,		1, {{CHARACTER_PROGRAM_DATA, "MIN"}}		},
{NODE_CMD,		SET_MIN,		1, {{CHARACTER_PROGRAM_DATA, "MINIMUM"}}	},
{NODE_CMD,		SET_MAX,		1, {{CHARACTER_PROGRAM_DATA, "MAX"}}		},
{NODE_CMD,		SET_MAX,		1, {{CHARACTER_PROGRAM_DATA, "MAXIMUM"}}	},
{NODE_CMD,		SET_DEFAULT,	1, {{CHARACTER_PROGRAM_DATA, "DEF"}}		},
{NODE_CMD,		SET_DEFAULT,	1, {{CHARACTER_PROGRAM_DATA, "DEFAULT"}}	},
{NODE_QUERY,	TASK_QUERY,		0, {} 										},	// returns the set point
{NODE_QUERY,	TASK_MIN,		1, {{CHARACTER_PROGRAM_DATA, "MIN"}}		},
{NODE_QUERY,	TASK_MIN, 		1, {{CHARACTER_PROGRAM_DATA, "MINIMUM"}} 	},
{NODE_QUERY,	TASK_MAX, 		1, {{CHARACTER_PROGRAM_DATA, "MAX"}}		},
{NODE_QUERY,	TASK_MAX, 		1, {{CHARACTER_PROGRAM_DATA, "MAXIMUM"}}	},
{NODE_QUERY,	TASK_DEFAULT, 	1, {{CHARACTER_PROGRAM_DATA, "DEF"}}		},
{NODE_QUERY,	TASK_DEFAULT,	1, {{CHARACTER_PROGRAM_DATA, "DEFAULT"}}	},
{NODE_QUERY,	TASK_ALL, 		1, {{CHARACTER_PROGRAM_DATA, "ALL"}}		},
{NODE_QUERY,	TASK_NONE,		0, {}}										};

const ArgumentNode wavelengthArgs[] = {
{NODE_CMD,		TASK_SET,		1, {{DECIMAL_NUMERIC_PROGRAM_DATA, ""}} 	},	// set the value
{NODE_QUERY,	TASK_LIST,		1, {{CHARACTER_PROGRAM_DATA, "LIST"}}		},	// returns a list
{NODE_QUERY,	TASK_QUERY, 	0, {}										},	// returns the current value
{NODE_QUERY,	TASK_NONE,		0, {}} 										};

const ArgumentNode bitpatternArgs[] = {
{NODE_CMD,		TASK_SET,		1, {{DECIMAL_NUMERIC_PROGRAM_DATA, ""}}		},	// set the value
{NODE_QUERY,	TASK_LIST,		1, {{CHARACTER_PROGRAM_DATA, "LIST"}}		},	// returns a list
{NODE_QUERY,	TASK_QUERY,		0, {}										},	// returns the current value
{NODE_QUERY,	TASK_NONE,		0, {}}										};

const ArgumentNode apmodeArgs[] = {
{NODE_CMD,		SET_ABS,		1, {{CHARACTER_PROGRAM_DATA, "ABS"}}		},
{NODE_CMD,		SET_ABS,		1, {{CHARACTER_PROGRAM_DATA, "ABSOLUTE"}}	},
{NODE_CMD,		SET_XB,			1, {{CHARACTER_PROGRAM_DATA, "XB"}}			},
{NODE_CMD,		SET_REF,		1, {{CHARACTER_PROGRAM_DATA, "REF"}}		},
{NODE_CMD,		SET_REF,		1, {{CHARACTER_PROGRAM_DATA, "REFERENCE"}}	},
{NODE_QUERY,	TASK_QUERY,		0, {} 										},
{NODE_QUERY,	TASK_NONE,		0, {}} 										};

const ArgumentNode attenModeArgs[] = {
{NODE_CMD,		SET_OFFSET,		1, {{CHARACTER_PROGRAM_DATA, "OFFSET"}}		},
{NODE_CMD,		SET_REL,		1, {{CHARACTER_PROGRAM_DATA, "REL"}}		},
{NODE_CMD,		SET_REL,		1, {{CHARACTER_PROGRAM_DATA, "RELATIVE"}}	},
{NODE_CMD,		SET_ABS,		1, {{CHARACTER_PROGRAM_DATA, "ABS"}}		},
{NODE_CMD,		SET_ABS,		1, {{CHARACTER_PROGRAM_DATA, "ABSOLUTE"}}	},
{NODE_QUERY,	TASK_QUERY,		0, {} 										},
{NODE_QUERY,	TASK_NONE,		0, {}}	 									};

const ArgumentNode controlModeArgs[] = {
{NODE_CMD,		SET_ATT,		1, {{CHARACTER_PROGRAM_DATA, "ATT"}}		},
{NODE_CMD,		SET_ATT,		1, {{CHARACTER_PROGRAM_DATA, "ATTENUATION"}}},
{NODE_CMD,		SET_POW,		1, {{CHARACTER_PROGRAM_DATA, "POW"}}		},
{NODE_CMD,		SET_POW,		1, {{CHARACTER_PROGRAM_DATA, "POWER"}}		},
{NODE_QUERY,	TASK_QUERY,		0, {}										},
{NODE_QUERY,	TASK_NONE,		0, {}} 										};

// aliases:
const ArgumentNode* attenuationArgs 	= maxMinDefaultSetActArgs;
const ArgumentNode* offsetArgs 			= maxMinDefaultArgs;
const ArgumentNode* referenceArgs 		= maxMinDefaultArgs;
const ArgumentNode* dtoleranceArgs 		= maxMinDefaultArgs;
const ArgumentNode* powerArgs			= maxMinDefaultSetActArgs;
const ArgumentNode* frequencyArgs		= maxMinDefaultSetActArgs;
const ArgumentNode* rawAttenuationArgs	= maxMinSetArgs;
const ArgumentNode* ppgChannelArgs		= maxMinDefaultSetActArgs;
const ArgumentNode* ppgBitPatternArgs	= bitpatternArgs;

// no arguments just command:
const ArgumentNode noCommandArgs[] = {
{NODE_CMD,		TASK_COMMAND,	0, {}	},
{NODE_QUERY,	TASK_NONE,		0, {} 	}};

// no arguments just query:
const ArgumentNode noQueryArgs[] = {
{NODE_QUERY,	TASK_QUERY,		0, {}	},
{NODE_QUERY,	TASK_NONE,		0, {}	}};

// something that has both command and query forms with no arguments
const ArgumentNode noCMDQueryArgs[] = {
{NODE_CMD,		TASK_COMMAND,	0, {}	},
{NODE_QUERY,	TASK_QUERY,		0, {}	},
{NODE_QUERY,	TASK_NONE,		0, {}	}};

const ArgumentNode setBitmaskArgs[] = {
{NODE_CMD,		TASK_COMMAND,	1, {{DECIMAL_NUMERIC_PROGRAM_DATA, ""}}},
{NODE_QUERY,	TASK_QUERY,		0, {}	},
{NODE_QUERY, 	TASK_NONE, 		0, {} 	}};

// ############################### HeaderNodes ###############################

// level 5
const HeaderNode readPowerChilds[] = {
{ID_READDC,	DIGIT_NONE, "DC",	"DC",	NULL,	noQueryArgs,	UNIT_NONE},
{ID_NONE,	DIGIT_NONE, NULL,	NULL,	NULL,	NULL,			UNIT_NONE} };

// level 4
const HeaderNode collectChilds[] = {
{ID_COLZERO,	DIGIT_NONE,	"ZERO", "ZERO",	NULL,	noCommandArgs,	UNIT_NONE},
{ID_NONE,		DIGIT_NONE,	NULL,	NULL,	NULL,	NULL,			UNIT_NONE} };

// level 3
const HeaderNode aclChilds[] = {
{ID_ACLSTATE,	DIGIT_NONE,	"STAT",	"STATE",	NULL,	stateArgs,	UNIT_NONE},
{ID_NONE,		DIGIT_NONE,	NULL,	NULL,		NULL,	NULL,		UNIT_NONE} };

const HeaderNode lockChilds[] = {
{ID_LOCK,	DIGIT_NONE,	"STAT",	"STATE",	NULL,	noQueryArgs,	UNIT_NONE},
{ID_NONE,	DIGIT_NONE,	NULL,	NULL,		NULL,	NULL,			UNIT_NONE} };

const HeaderNode readScalarChilds[] = {
{ID_NONE,	DIGIT_NONE,	"POW",	"POWER",	readPowerChilds,	NULL,	UNIT_NONE},
{ID_NONE,	DIGIT_NONE,	NULL,	NULL,		NULL,				NULL,	UNIT_NONE} };

const HeaderNode correctionChilds[] = {
{ID_NONE,	DIGIT_NONE,	"COLL",	"COLLECT",	collectChilds,	NULL,	UNIT_NONE},
{ID_NONE,	DIGIT_NONE,	NULL,	NULL,		NULL,			NULL,	UNIT_NONE} };

const HeaderNode operationChilds[] = {
{ID_NONE,	DIGIT_NONE,	NULL,	NULL,	NULL,	NULL,	UNIT_NONE} };

const HeaderNode questionableChilds[] = {
{ID_NONE,	DIGIT_NONE,	NULL,	NULL,	NULL,	NULL,	UNIT_NONE} };

const HeaderNode attenChilds[] = {
{ID_ATTEN_VEL_DBPS,	DIGIT_NONE,	"VEL",	"VELOCITY",	NULL,	maxMinDefaultSetActArgs,	UNIT_DBPS},
{ID_NONE,			DIGIT_NONE, NULL,	NULL,		NULL,	NULL,						UNIT_NONE} };

const HeaderNode powerChilds[] = {
{ID_AVERAGING_TIME_S,	DIGIT_NONE,	"AVER",	"AVERAGINGTIME",	NULL,	maxMinDefaultSetArgs,	UNIT_S},
{ID_START_DARK_NULLING,	DIGIT_NONE, "NULL",	"NULLING",			NULL,	noCommandArgs, 			UNIT_NONE},
{ID_NONE,				DIGIT_NONE,	NULL,	NULL,				NULL,	NULL,					UNIT_NONE} };

// PPG
const HeaderNode ppgChannelChilds[] = {
{ID_PPG_BITRATE,		DIGIT_NONE,	"BITR",		"BITRATE",		NULL,	ppgChannelArgs,		UNIT_NONE},
{ID_PPG_VPKPK,			DIGIT_NONE, "VPKPK",	"VPKPK",		NULL,	ppgChannelArgs,		UNIT_NONE},
{ID_PPG_BIT_PATTERN,	DIGIT_NONE, "BITP",		"BITPATTERN",	NULL,	ppgBitPatternArgs,	UNIT_NONE},
{ID_PPG_SKEW,			DIGIT_NONE, "SKEW",		"SKEW",			NULL,	ppgChannelArgs,		UNIT_NONE},
{ID_PPG_TAP,			DIGIT_CHAN, "TAP",		"TAP",			NULL,	ppgChannelArgs,		UNIT_NONE},
{ID_NONE,				DIGIT_NONE, NULL,		NULL,			NULL,	NULL,				UNIT_NONE} };

// level 2
const HeaderNode systemChilds[] = {
{ID_VERS,	DIGIT_NONE,	"VERS",	"VERSION",	NULL,	noQueryArgs,	UNIT_NONE},
{ID_NONE,	DIGIT_NONE,	NULL,	NULL,		NULL,	NULL,			UNIT_NONE} };

const HeaderNode slotChilds[] = {
{ID_OPC_SLOT,	DIGIT_NONE,	"OPC",	"OPC",		NULL, noQueryArgs,	UNIT_NONE},
{ID_OPT_SLOT,	DIGIT_NONE, "OPT",	"OPTIONS",	NULL, noQueryArgs,	UNIT_NONE},
{ID_IDN_SLOT,	DIGIT_NONE, "IDN",	"IDN",		NULL, noQueryArgs,	UNIT_NONE},
{ID_NONE,		DIGIT_NONE, NULL,	NULL,		NULL, NULL,			UNIT_NONE} };

const HeaderNode calibrationChilds[] = {
{ID_CALZERO,	DIGIT_NONE, "ZERO",	"ZERO",	NULL,	noCommandArgs,	UNIT_NONE},
{ID_NONE,		DIGIT_NONE, NULL,	NULL,	NULL,	NULL,			UNIT_NONE} };

const HeaderNode controlChilds[] = {
{ID_CONTMODE,	DIGIT_NONE,	"MODE",	"MODE",	NULL,	controlModeArgs,	UNIT_NONE},
{ID_NONE,		DIGIT_NONE,	NULL,	NULL,	NULL,	NULL,				UNIT_NONE} };

const HeaderNode inputChilds[] = {
{ID_ATTEN_MODE,		DIGIT_NONE,	"AMODE",	"AMODE",		NULL,			attenModeArgs,		UNIT_NONE},
{ID_ARESOLUTION,	DIGIT_NONE, "ARES",		"ARESOLUTION", 	NULL, 			noQueryArgs,	 	UNIT_NONE},
{ID_ATTENUATION_DB,	DIGIT_NONE, "ATT",		"ATTENUATION", 	attenChilds, 	attenuationArgs, 	UNIT_DB},
{ID_INOFFSET,		DIGIT_NONE, "OFFS",		"OFFSET", 		NULL, 			offsetArgs, 		UNIT_DB},
{ID_RATTENUATION,	DIGIT_NONE, "RATT",		"RATTENUATION",	NULL, 			attenuationArgs, 	UNIT_DB},
{ID_INREFERENCE,	DIGIT_NONE, "REF",		"REFERENCE",	NULL, 			referenceArgs, 		UNIT_DB},
{ID_WAVELENGTH_NM,	DIGIT_NONE, "WAV",		"WAVELENGTH",	NULL, 			wavelengthArgs,		UNIT_NM},
{ID_NONE,			DIGIT_NONE, NULL,		NULL,			NULL,			NULL,				UNIT_NONE} };

const HeaderNode rawChilds[] = {
{ID_ATTENUATION_RAW,	DIGIT_NONE, "ATT", "ATTENUATION", 	NULL,	rawAttenuationArgs,	UNIT_NONE},
{ID_POWER_RAW,			DIGIT_NONE, "POW", "POWER",			NULL,	getMinMaxArgs,		UNIT_NONE},
{ID_ATTENUATOR_TEMP_C,	DIGIT_NONE, "TEMP", "TEMPERATURE",	NULL,	getMinMaxArgs,		UNIT_C},
{ID_INSERTION_LOSS_DB,	DIGIT_NONE, "LOSS", "LOSS",			NULL,	noQueryArgs, 		UNIT_DB},
{ID_DARK_POWER_DBM,		DIGIT_NONE, "DARK", "DARKPOWER", 	NULL,	maxMinSetArgs, 		UNIT_DBM},
{ID_START_DARK_NULLING,	DIGIT_NONE, "NULL", "NULLING",		NULL,	noCommandArgs, 		UNIT_NONE},
{ID_NONE,				DIGIT_NONE,	NULL,	NULL,			NULL,	NULL,				UNIT_NONE} };

const HeaderNode outputChilds[] = {
{ID_NONE,			DIGIT_NONE, "ALC",	"ALC", 			aclChilds, 	NULL, 			UNIT_NONE},
{ID_APMODE,			DIGIT_NONE, "APM",	"APMODE", 		NULL, 		apmodeArgs,		UNIT_NONE},
{ID_DTOLERANCE,		DIGIT_NONE, "DTO",	"DTOLERANCE", 	NULL, 		dtoleranceArgs, UNIT_DB},
{ID_LOCK,			DIGIT_NONE, "LOCK",	"LOCK", 		lockChilds, noQueryArgs, 	UNIT_NONE},
{ID_OUTOFFSET,		DIGIT_NONE, "OFFS", "OFFSET", 		NULL, 		offsetArgs,		UNIT_DB},
{ID_POWER_DBM,		DIGIT_NONE, "POW",	"POWER",		powerChilds,powerArgs, 		UNIT_DBM},
{ID_OUTREFERENCE,	DIGIT_NONE, "REF",	"REFERENCE",	NULL, 		referenceArgs,	UNIT_DBM},
{ID_RPOWER,			DIGIT_NONE, "RPOW",	"RPOWER",		NULL, 		powerArgs,		UNIT_DBM},
{ID_STATE,			DIGIT_NONE, "STAT", "STATE",		NULL, 		stateArgs, 		UNIT_NONE},
{ID_NONE,			DIGIT_NONE, NULL,	NULL,			NULL,		NULL,			UNIT_NONE} };

// RF Clock
const HeaderNode rfclockChilds[] = {
{ID_RFCL_POWER,		DIGIT_NONE, "POW",	"POWER",		NULL,	powerArgs,		UNIT_DBM},
{ID_RFCL_FREQUENCY,	DIGIT_NONE, "FREQ",	"FREQUENCY",	NULL,	frequencyArgs,	UNIT_NONE},
{ID_RFCL_OUTPUT,	DIGIT_NONE, "OUTP", "OUTPUT",		NULL,	stateArgs,		UNIT_NONE},
{ID_NONE,			DIGIT_NONE, NULL,	NULL,			NULL,	NULL,			UNIT_NONE} };

// PPG
const HeaderNode ppgChilds[] = {
{ID_PPG_CHANNEL,	DIGIT_CHAN,	"CHAN",	"CHANNEL",	ppgChannelChilds,	NULL,	UNIT_NONE},
{ID_NONE,			DIGIT_NONE,	NULL,	NULL,		NULL,				NULL,	UNIT_NONE} };

const HeaderNode readChilds[] = {
{ID_NONE,	DIGIT_NONE,	"SCAL",	"SCALAR",	readScalarChilds,	NULL,	UNIT_NONE},
// since scalar is optional we have a different (non-scalar) power here
{ID_NONE,	DIGIT_NONE, "POW",	"POWER",	readPowerChilds,	NULL,	UNIT_NONE},
{ID_NONE,	DIGIT_NONE, NULL,	NULL,		NULL,				NULL,	UNIT_NONE} };

const HeaderNode senseChilds[] = {
{ID_NONE,	DIGIT_NONE,	"CORR",	"CORRECTION",	correctionChilds,	NULL,	UNIT_NONE},
{ID_NONE,	DIGIT_NONE, NULL,	NULL,			NULL,				NULL,	UNIT_NONE} };

const HeaderNode statusChilds[] = {
{ID_PRES,	DIGIT_NONE,	"PRES",	"PRESET",		NULL,				noCommandArgs,	UNIT_NONE},
{ID_NONE,	DIGIT_NONE, "OPER", "OPERATION",	operationChilds,	NULL,			UNIT_NONE},
{ID_NONE,	DIGIT_NONE, "QUES", "QUESTIONABLE",	questionableChilds,	NULL,			UNIT_NONE},
{ID_NONE,	DIGIT_NONE, NULL,	NULL,			NULL,				NULL,			UNIT_NONE} };

// level 1
const HeaderNode root[] = {
// GPIB:
{ID_CLS,	DIGIT_NONE,	"*CLS",	"*CLS",			NULL,				noCommandArgs,	UNIT_NONE},
{ID_ESE,	DIGIT_NONE, "*ESE", "*ESE",			NULL,				setBitmaskArgs, UNIT_NONE},
{ID_ESR,	DIGIT_NONE, "*ESR", "*ESR",			NULL,				noQueryArgs,	UNIT_NONE},
{ID_IDN,	DIGIT_NONE, "*IDN", "*IDN",			NULL,				noQueryArgs,	UNIT_NONE},
{ID_OPC,	DIGIT_NONE, "*OPC", "*OPC",			NULL,				noCMDQueryArgs,	UNIT_NONE},
{ID_OPT,	DIGIT_NONE, "*OPT", "*OPT",			NULL,				noQueryArgs,	UNIT_NONE},
{ID_RST,	DIGIT_NONE, "*RST", "*RST",			NULL,				noCommandArgs,	UNIT_NONE},
{ID_WAI,	DIGIT_NONE, "*WAI", "*WAI",			NULL,				noCommandArgs,	UNIT_NONE},
{ID_STB,	DIGIT_NONE, "*STB", "*STB",			NULL,				noQueryArgs,	UNIT_NONE},
{ID_SRE,	DIGIT_NONE, "*SRE", "*SRE",			NULL,				setBitmaskArgs, UNIT_NONE},
{ID_TST,	DIGIT_NONE, "*TST",	"*TST",			NULL,				noQueryArgs,	UNIT_NONE},
// SCPI:
{ID_NONE,	DIGIT_NONE, "SYST",	"SYSTEM",		systemChilds, 		NULL,			UNIT_NONE},
{ID_NONE,	DIGIT_NONE, "STAT", "STATUS", 		statusChilds, 		NULL,			UNIT_NONE},
{ID_NONE,	DIGIT_SLOT, "SLOT", "SLOT",			slotChilds, 		NULL,			UNIT_NONE},
{ID_NONE,	DIGIT_CHAN, "CAL", 	"CALIBRATION", 	calibrationChilds,	NULL,			UNIT_NONE},
{ID_NONE,	DIGIT_CHAN, "CONT", "CONTROL", 		controlChilds, 		NULL,			UNIT_NONE},
{ID_NONE,	DIGIT_CHAN, "INP",	"INPUT", 		inputChilds, 		NULL,			UNIT_NONE},
{ID_NONE,	DIGIT_CHAN, "OUTP", "OUTPUT", 		outputChilds,		stateArgs,		UNIT_NONE},
{ID_NONE,	DIGIT_CHAN, "READ", "READ", 		readChilds, 		NULL,			UNIT_NONE},
{ID_NONE,	DIGIT_CHAN, "SENS",	"SENSE", 		senseChilds,		NULL,			UNIT_NONE},
{ID_NONE,	DIGIT_CHAN, "RAW",	"RAW",			rawChilds, 			NULL,			UNIT_NONE},
// RF Clock
{ID_NONE,	DIGIT_CHAN, "RFCL", "RFCLOCK",		rfclockChilds,		NULL,			UNIT_NONE},
// PPG
{ID_NONE,	DIGIT_CHAN,	"PPG", "PPG",			ppgChilds,			NULL,			UNIT_NONE},
{ID_NONE,	DIGIT_NONE, NULL,	NULL,			NULL,				NULL,			UNIT_NONE} };

// matches dataList, which is the user input, against a list of candidate 
// arangments for arguments provided by args (hard coded constants)
// defaultUnit is the default Units for the header
DecodedMessage* decodeArgs(Unit defaultUnit, bool query, vector<ProgramDataValue*>& dataList, BackendCallbackID messageid, const ArgumentNode* args, uint8_t slot, uint8_t channel)
{
	spLogDebug(1, "decodeArgs: DecodedMessageID: %d query?: %d", messageid, query);
	
	// values will need to be at most dataList.size() long
	double* values = new double[dataList.size()];
	int numValues = 0;
	for (int i=0;; ++i) {
		// this is the marker for the end of the table
		if (args[i].task==TASK_NONE) {
			spLogDebug(4, "parse error: argument arangment did not match any in table");
			return NULL;
		}
		
		// if the candidate's query-ness isn't the same as the input, 
		// try the next candidate
		if (args[i].query != query) {
			spLogDebug(4, "parse error: arguments candidate failed, query-ness differs");
			continue;
		}
		
		numValues = 0;
		unsigned int j=0;
		
		if (args[i].argsLength!=dataList.size()) {
			// this candidate did not match
			spLogDebug(4, "parse error: arguments candidate failed, incorrect number of arguments");
			continue;
		}
		
		// try each of the arguments in turn, 
		// if the arguments match the candidate then make a decoded message and return
		for (; (j<args[i].argsLength); ++j) {
			if (j>=dataList.size()) {
				spLogDebug(4, "parse error: arguments candidate failed, not enough arguments");
				break;
			}
			if (args[i].args[j].dataType!=dataList[j]->type()) {
				spLogDebug(4, "parse error: arguments candidate failed, types mismatch");
				break;
			}
			
			if (args[i].args[j].dataType==DECIMAL_NUMERIC_PROGRAM_DATA) {
				// Extract the unitString and value from the argument
				string unitString;
				double value = dataList[j]->retrieveDecimal(unitString);
				// We need to check if it is possible to convert 
				// into defaultUnits
				Unit from = defaultUnit;
				if (unitString!="") {
					spLogDebug(4, "creating unit from string: \"%s\"", unitString.c_str());
					from = unitFromString(unitString.c_str());
				}
				Unit to = defaultUnit;
				double result;
				if (doUnitConvertion(&result, value, to, from)) {
					values[numValues] = result;
					++numValues;
				} else {
					spLogDebug(4, "parse error: arguments candidate failed, invalid units: %s", unitString.c_str());
					break;
				}
			} else if (args[i].args[j].dataType==CHARACTER_PROGRAM_DATA) {
				if (dataList[j]->retrieveCharacter()==NULL) {
					spLogDebug(4, "parse error: arguments candidate failed, argument of type CHARACTER_PROGRAM_DATA is (null)");
					break;
				}
				if (convertToUpper(*(dataList[j]->retrieveCharacter()))!=args[i].args[j].taskString) {
					spLogDebug(4, "parse error: arguments candidate failed, argument of type CHARACTER_PROGRAM_DATA does not match");
					break;
				}
			} else {
				spLogDebug(4, "parse error: arguments candidate failed, argument of type: %d is currently unsupported", args[i].args[j].dataType);
				break;
			}
		}
		if (j!=args[i].argsLength)
			continue;
		
		// this candidate matches
		return new DecodedMessage(messageid, args[i].task, slot, channel, numValues, values);
	}

	// this would normally be taken care of by the destructor of DecodedMessage
	// however, since we have failed we must clean up here
	delete(values);
	return NULL;
}

DecodedMessage* decodeHeader(bool query, vector<string>& headerPath, vector<ProgramDataValue*>& dataList)
{
	// this prevents matches when nothing is provided
	if (headerPath.size()<1) {
		return NULL;	
	}
	
	unsigned int depth=0;
	int slot = 0;
	int channel = 0;
	const HeaderNode* node = root;
	while(depth<10) {		// this is while 1, but with a sanity check. loop should break before depth = 5
		spLogDebug(2, "header path depth: %d out of %d", 
					depth, headerPath.size());
		for (int i=0;; ++i) {
			// this is the marker for the end of the table
			if (node[i].shortForm==NULL) {
				spLogDebug(4, "parse error: header did not match any in table");
				return NULL;
			}
			// for commands allowing trailing digits explicit 0 is invalid,
			// but no digits decodes to 0
			int digit = SCPIMnemonicDecode(headerPath[depth], 
									node[i].shortForm, node[i].longForm);
			// if the header doesn't match then move to the next candidate 
			if (digit<0) {
				continue; // move to node[i+1]
			}
			if (node[i].digit==DIGIT_NONE) {
				if (digit!=0) {
					spLogDebug(4, "parse error: plain node has digit supplied");
					return NULL;
				}
			} else if (node[i].digit==DIGIT_SLOT) {
				//if (SCPIstate.isBenchtop) {
					if (digit==0) {
						slot = 0;
					} else {
						spLogDebug(4, "parse error: slot specified for benchtop unit");
						return NULL;
					}
				/*} else {
					if (digit==0) {
						spLogDebug(4, "parse error: required trailing digit (slot) ommited");
						return NULL;
					} else {
						slot = digit;
					}
				}*/
			} else if (node[i].digit==DIGIT_CHAN) {
				if (digit==0) {
					spLogDebug(4, "parse error: required trailing digit (channel) ommited");
					return NULL;
				} else {
					channel = digit;
				}
			} else {
				spLogDebug(4, "parse error: command table has unknown value for Digit_t: %d", node[i].digit);
				return NULL;
			}

			if (headerPath.size()==depth+1) {
				if (node[i].messageid!=ID_NONE) {
					if (node[i].args==NULL) {
						// not a terminal node, but no more header to parse
						spLogDebug(4, "parse error: not a terminal node");
						return NULL;
					}
					else {
						//terminal node
						Unit unit = node[i].defaultUnit;
						//if (SCPIstate.isBenchtop) {
							slot = 15;
						//}
						return decodeArgs(unit, query, dataList, node[i].messageid, node[i].args, slot, channel);
					}
				}
				return NULL;
			} else {
				if (node[i].children==NULL) {
					if (node[i].args==NULL) {
						spLogWarning("Warning, Internal error, node[i].args and "
									"node[i].children are both NULL, command is "
									"not implimented: %s", node[i].longForm);
					}
					return NULL;
				}
				node = node[i].children;
				++depth;
				break;	// This is a matching non terminal node, increase depth
			}
		}
	}
	return NULL;
}
