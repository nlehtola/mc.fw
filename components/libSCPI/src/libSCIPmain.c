/**
 * @file libSCPImain.c
 * @brief This is the interface of the libSCPI library. Register callbacks
 * for hardware backend using the <register callback function name> function.
 * Then start the SCPI server by calling the startSCPI() function
 */

#include "vxi11_svc.h"
#include "stdlib.h"
#include <signal.h>
#include "InstrumentConfig.h"
#include "defaultBackendCallbacks.h"

/** @brief Registers an RPC service acting as a VXI11 server.
 * SCPI commands will be parsed and the appropriate hardware backend callbacks
 * will be called when a command is sent to the server.
 *  @return Does not return unless a fatal error has occured
 */
int startSCPI()
{
	// if SIGINT is not ignored, then calling write on a broken socket will 
	// result in proccess termination due to unhandled signal
	signal(SIGPIPE, SIG_IGN);
	
	registerDefaultCallbacks();
	
	startVXI11();
	// Does not return
	return 0;
}
