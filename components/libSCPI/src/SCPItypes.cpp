#include "SCPItypes.hpp"

// ***************************** ProgramDataValue *****************************
ProgramDataValue::ProgramDataValue()
{
	value = NULL;
	_type = NO_TYPE;
	unit = "";
}

ProgramDataValue::~ProgramDataValue()
{
	if(value!=NULL) {
		// different data types must be freed in different ways
		switch (_type) {
		case CHARACTER_PROGRAM_DATA:
		case STRING_PROGRAM_DATA:
			delete((string*)value);
			break;
		case DECIMAL_NUMERIC_PROGRAM_DATA:
			delete((double*)value);
			break;
		case NON_DECIMAL_PROGRAM_DATA:
			delete((int*)value);
			break;
		default:
			// potential memory leak
			spLogError("Error, ProgramDataValue contained an unknown data type, this may not have been freed properly by the destructor");
			break;
		}
	}
}

void ProgramDataValue::assignCharacter(string *p) 
{
	value = (void*)p;
	_type = CHARACTER_PROGRAM_DATA;
}

void ProgramDataValue::assignDecimal(double f, const string& unit)
{
	value = new double(f);
	this->unit = unit;
	_type = DECIMAL_NUMERIC_PROGRAM_DATA;
}

void ProgramDataValue::assignNumeric(int i)
{
	value = new int(i);
	_type = NON_DECIMAL_PROGRAM_DATA;
}

void ProgramDataValue::assignString(string *p)
{
	value = (void*)p;
	_type = STRING_PROGRAM_DATA;
}

string* ProgramDataValue::retrieveCharacter() 
{
	if (_type != CHARACTER_PROGRAM_DATA) {
		return NULL;
	}
	return (string*)value;
}

// returns the value and the units
double ProgramDataValue::retrieveDecimal(string& unit)
{
	unit = this->unit;
	if (_type != DECIMAL_NUMERIC_PROGRAM_DATA) {
		return 0;	//warning
	}
	return *(double*)value;
}

int ProgramDataValue::retrieveNumeric()
{
	if (_type != NON_DECIMAL_PROGRAM_DATA) {
		return 0;	//warning
	}
	return *(int*)value;
}

string* ProgramDataValue::retrieveString()
{
	if (_type != STRING_PROGRAM_DATA) {
		return NULL;	//warning
	}
	return (string*)value;
}

ProgramDataType ProgramDataValue::type()
{
	return _type;
}

bool ProgramDataValue::isDecimal()
{
	if (_type==DECIMAL_NUMERIC_PROGRAM_DATA) {
		return true;
	}
	return false;
}

// ***************************** ProgramDataList *****************************
ProgramDataList::~ProgramDataList()
{
	// free any dynamic memory ascociated with the command arguments
	for (vector<ProgramDataValue*>::iterator i=list.begin(); i!=list.end(); ++i) {
		delete(*i);
	}
}

ProgramHeader::ProgramHeader(bool query)
{
	this->query = query;
	absolute = true;
}

ProgramMessageUnit::ProgramMessageUnit(ProgramHeader* programHeader, ProgramDataList* programDataList)
{
	this->programHeader = programHeader;
	this->programDataList = programDataList;
}

ProgramMessageUnit::~ProgramMessageUnit()
{
	delete(programHeader);
	delete(programDataList);
}
