#include "Units.h"

#include <ctype.h>
#include <math.h>

typedef struct {
	Unit unit;
	double factor;
	UnitType type;
	const char* name;
} UnitTableEntry;

const UnitTableEntry unitsTable[] = {
	//temp
	{UNIT_C,1,UNIT_TEMP,"C"},
	{UNIT_CC,1e-2,UNIT_TEMP,"cC"},	// centi celcius is 1/100 of a degee celcius (this is what b2s blades returned)
	//frequency
	{UNIT_HZ,1,UNIT_FREQ,"Hz"},
	{UNIT_KHZ,1e3,UNIT_FREQ,"kHz"},
	{UNIT_MHZ,1e6,UNIT_FREQ,"MHz"},
	{UNIT_GHZ,1e9,UNIT_FREQ,"GHz"},
	{UNIT_THZ,1e12,UNIT_FREQ,"THz"},
	//wavelength
	{UNIT_M,1,UNIT_WAVE,"m"},
	{UNIT_MM,1e-3,UNIT_WAVE,"mm"},
	{UNIT_UM,1e-6,UNIT_WAVE,"um"},
	{UNIT_NM,1e-9,UNIT_WAVE,"nm"},
	{UNIT_PM,1e-12,UNIT_WAVE,"pm"},
	// DB
	{UNIT_DB,1,UNIT_DBPOWER,"dB"},
	{UNIT_MDB,1e-3,UNIT_DBPOWER,"mdB"},
	//power
	{UNIT_DBM,1,UNIT_DBMPOWER,"dBm"},
	{UNIT_MDBM,1e-3,UNIT_DBMPOWER,"mdBm"},
	{UNIT_UDBM,1e-6,UNIT_DBMPOWER,"udBm"},
	{UNIT_W,1,UNIT_POWER,"W"},
	{UNIT_MW,1e-3,UNIT_POWER,"mW"},
	{UNIT_UW,1e-6,UNIT_POWER,"uW"},
	//time
	{UNIT_S,1,UNIT_TIME,"s"},
	{UNIT_MS,1e-3,UNIT_TIME,"ms"},
	{UNIT_US,1e-6,UNIT_TIME,"us"},
	{UNIT_NS,1e-9,UNIT_TIME,"ns"},
	//dB rate
	{UNIT_DBPS,1,UNIT_DBRATE,"dBps"},
	{UNIT_MDBPS,1e-3,UNIT_DBRATE,"mdBps"},

	{UNIT_NONE,1,UNIT_TYPE_NONE,""}
};

// converts wavelength to frequency
double mToMHzRoundToHundredMHz(double wavelength)
{
	return round((SPEED_OF_LIGHT)/wavelength/((double)100e6))*100;
}

// converts frequency to wavelength
double MHzTom(double frequency)
{
	return (SPEED_OF_LIGHT)/(frequency*((double)1e6));
}

// converts between dBm and W
inline double dBmtoW(double power)
{
	return pow(10, power/10.0)/1000.0;
}
inline double WtodBm(double power)
{
	return 10*log10(power*1000.0);
}

const UnitTableEntry* findUnit(Unit unit) {
	int i=0; 
	for (; unitsTable[i].unit!=UNIT_NONE; ++i) {
		if (unitsTable[i].unit==unit) {
			break;
		}
	}
	return &(unitsTable[i]);
}

Unit unitFromString(const char* name)
{
	int i;
	for (i=0; unitsTable[i].unit!=UNIT_NONE; ++i) {
		// compare the 2 strings in upper case
		// if they match, break
		const char* a = name;
		const char* b = unitsTable[i].name;
		for (; *a!='\0'&&*b!='\0'&&(toupper(*a)==toupper(*b)); ++b, ++a);
		if (*a==*b&&*a=='\0') {
			break;
		}
	}	
	return unitsTable[i].unit;
}

// for printing the name
const char* getUnitName(Unit unit)
{
	const UnitTableEntry* unitPtr = findUnit(unit);
	return unitPtr->name; // this will be "" if we can't find the unit
}

// convert input from units from into units to
// 0 on failure
// 1 on success
int doUnitConvertion(double* result, double value, Unit to, Unit from)
{
	if (from==to) {
		*result = value;
		return 1;
	}
	
	*result = nan("");
	
	const UnitTableEntry* inUnit = findUnit(from);
	const UnitTableEntry* outUnit = findUnit(to);
	spLogDebug(3, "doUnitConvertion converting value: %f, "
				"from units: %s into units: %s", value, 
				inUnit->name, outUnit->name);
	
	if (inUnit->type==outUnit->type) {
		// This is the easy case where there is an easy conversion
		// the rest require more complicated conversions
		*result = (value*inUnit->factor)/outUnit->factor;
		spLogDebug(4, "doUnitConvertion converting %f to %f", value, *result);
	} else if ((inUnit->type==UNIT_DBMPOWER)&&(outUnit->type==UNIT_POWER)) {
		*result = dBmtoW(value*inUnit->factor)/outUnit->factor;
		spLogDebug(4, "doUnitConvertion converting dBm to W");
	} else if ((inUnit->type==UNIT_POWER)&&(outUnit->type==UNIT_DBMPOWER)) {
		if ((value*inUnit->factor)<=0) {
			spLogDebug(4, "doUnitConvertion converting invalid value for power: %f", value);
			return 0;
		}
		*result = WtodBm(value*inUnit->factor)/outUnit->factor;
		spLogDebug(4, "doUnitConvertion converting W to dBm");
	} else {
		spLogDebug(4, "doUnitConvertion converting between invalid units: "
			"\"%s\" to \"%s\"", inUnit->name, outUnit->name);
		return 0;
	}
	spLogDebug(4, "doUnitConvertion outputing %f, in units: %d", *result, to);
	return 1;
}

