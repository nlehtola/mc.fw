#ifndef VXI11_SVC_H
#define VXI11_SVC_H
/** @file vxi11_svc.h
 *  @brief Header file for prototype of entry point for the VXI11 RPC server
 */
 
/** @brief Registers an RPC service acting as a VXI11 server
 *  @return Does not return unless a fatal error has occured
 */
int startVXI11 ();

#endif //VXI11_SVC_H
