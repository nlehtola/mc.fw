#include "SCPIParser.hpp"
#include "parser/SCPI.tab.hpp"
#include "parser/SCPI.yy.hpp"
#include "commandTable.hpp"
#include "SCPItypes.hpp"
#include "logging.h"

int parseSCPIWithBison(char* data, unsigned int length, vector<ProgramMessageUnit*>& programMessage)
{
	// The state of the lexer and parser
	YYSTYPE type;
	yypstate *ps;
	yyscan_t scanner;
	
	// a pair of connected FILE descriptors
	int inputfdWrite;		//input (we push into here)
	FILE *inputfdRead;		//output (lexer pulls out of here)
	int pipes[2];

	if (pipe(pipes)!=0) {
		// This means that we have too many streams open currently
		// This results in unsuccessful parsing
		// We can only hope that this is transient issue
		spLogError("Error, could not create internal pipe for parser: %s", strerror(errno));
		return PARSER_ERROR;
	}
	inputfdWrite = pipes[1];
	inputfdRead = fdopen(pipes[0], "r");
	
	if (inputfdRead==NULL) {
		// not sure why this would happen, assumption is that it is temporary
		spLogError("Error, could not create internal pipe for parser #2: %s", strerror(errno));
		close(pipes[0]);
		close(pipes[1]);	// is this correct procedure for closing the failed pipe?
		return PARSER_ERROR;
	}
	
	ps = yypstate_new();
	yylex_init(&scanner);
	yyset_in(inputfdRead, scanner);

	unsigned int written = 0;
	// variables for select(2) call, and timeouts
	fd_set fdset;
	struct timeval timeoutStruct;
	long timeout = 100;	// ms
	timeoutStruct.tv_sec = (long)((timeout*1000)/1000000);
	timeoutStruct.tv_usec = (long)((timeout*1000)%1000000);
	FD_ZERO(&fdset); // clear the set
	FD_SET(inputfdWrite, &fdset); // add our file descriptor to the set

	while(written<length) {
		int e;
		e = select(inputfdWrite+1, NULL, &fdset, NULL, &timeoutStruct);
		if (e<0) {
			spLogError("Error, inputfdWrite: select(2) errored: %s", strerror(errno));
			// shut down the parser/scanner
			close(inputfdWrite);
			fclose(inputfdRead);
			yypstate_delete(ps);
			yylex_destroy(scanner);
			return PARSER_ERROR;
		}
		if (e==0) {
			//IO timeout
			spLogDebug(1, "Error, SCPIParser push: write to flex scanner inputfdWrite: IO Timeout occured");
			// shut down the parser/scanner
			close(inputfdWrite);
			fclose(inputfdRead);
			yypstate_delete(ps);
			yylex_destroy(scanner);
			return PARSER_ERROR;	//TODO return write timeout
		}
		if ((e = write(inputfdWrite, &(data[written]), length-written))<0) {
			spLogError("Error, SCPIParser, write to flex scanner inputfdWrite failed: %s", strerror(errno));
			// shut down the parser/scanner
			close(inputfdWrite);
			fclose(inputfdRead);
			yypstate_delete(ps);
			yylex_destroy(scanner);
			return PARSER_ERROR;
		}
		written += e;
	}

	// ###### decode tokens using bison/flex ######
	// send an EOF to the parser, this seems to be the only way to trigger
	// it to halt at the end of the input
	fsync(inputfdWrite);	//is fsync necessary?
	close(inputfdWrite);
	inputfdWrite = -1;

	int status;
	do {
		status = yypush_parse(ps, yylex(&type, scanner), &type, &programMessage);
	} while ((status==YYPUSH_MORE));
	// shut down the parser/scanner
	fclose(inputfdRead);
	yypstate_delete(ps);
	yylex_destroy(scanner);
	
	if (status==1) {
		// clean up the list of ProgramMessageUnit's
		for (vector<ProgramMessageUnit*>::iterator i=programMessage.begin(); i!=programMessage.end(); ++i) {
			delete(*i);
			programMessage.erase(i);
			--i;
		}
		spLogDebug(1, "##################### Parse Error: Syntax #####################");
		return PARSER_PARSE_ERROR;
	}
	spLogDebug(2, "Done bison parsing, bison status: %d", status);
	return PARSER_SUCCESS;
}

int parseSCPI(char* data, unsigned int length, vector<DecodedMessage*>* decodedMessages)
{
	// trim the optional trailing \n if there is one, 
	// note that only a single trailing \n is valid
	if (data[length-1]=='\n')
		--length;
		
	// print out data to be parsed for debug purposes
	spLogDebug(1, "parsing: \"%.*s\"", length, data);

	// trim any remaining whitespace, additional newlines are invalid
	for(int i=length-1; i>=0; --i) {
		if (data[i]=='\n') {
			spLogDebug(1, "######## Parse Error: Syntax, Illegal trailing newlines #######");
			return PARSER_PARSE_ERROR;
		} else if ((0x00<=data[i]&&data[i]<=0x09)||(0x0B<=data[i]&&data[i]<=0x20)) {
			// IEEE 488.2 definition of whitespace 7.4.1.2
			length = i;
		} else {
			break;
		}
	}

	vector<ProgramMessageUnit*> programMessage;
	int r;
	if((r=parseSCPIWithBison(data, length, programMessage))!=PARSER_SUCCESS) {
		for (vector<ProgramMessageUnit*>::iterator i=programMessage.begin(); i!=programMessage.end(); ++i) {
			delete(*i);
			programMessage.erase(i);
			--i;
		}
		return r;
	}
	
	// ###### decode headers and arguments ######
	bool e = false;
	bool first = true;
	vector<string> prevHeaderPath;
	for (vector<ProgramMessageUnit*>::iterator i=programMessage.begin(); i!=programMessage.end(); ++i) {
		DecodedMessage* d = NULL;
		vector<string> headerPath;
		
		// relative headers are handled according to SCPI-99 6.2.4:
		// Absolute paths set prevHeaderPath
		// Relative paths (missing leading ':') are appended with prevHeaderPath
		// The first headers is implicitly absolute
		if ((*i)->programHeader->absolute||first) {
			headerPath = (*i)->programHeader->headerPath;
			prevHeaderPath = (*i)->programHeader->headerPath;
			prevHeaderPath.erase(prevHeaderPath.end()-1);
		} else if (!first) {
			// concatenate prevHeaderPath with the new relative path
			headerPath = prevHeaderPath;
			headerPath.insert(headerPath.end(), 
					(*i)->programHeader->headerPath.begin(),
					(*i)->programHeader->headerPath.end());
		}
		
		if ((*i)->programDataList == NULL) {
			vector<ProgramDataValue*> v;	//dummy for no args
			d = decodeHeader((*i)->programHeader->query, headerPath, v);
		} else {
			d = decodeHeader((*i)->programHeader->query, headerPath, (*i)->programDataList->list);
		}
		if (d==NULL) {
			spLogDebug(1, "Parse error: Command could not be found in command table");
			e = true;
			break;
		}
		first = false;
		decodedMessages->push_back(d);
	}
	
	for (vector<ProgramMessageUnit*>::iterator i=programMessage.begin(); i!=programMessage.end(); ++i) {
		delete(*i);
		programMessage.erase(i);
		--i;
	}
	if (e) {
		spLogDebug(1, "################ Parse Error: Command not found ###############");
		return PARSER_PARSE_ERROR;
	}
	return PARSER_SUCCESS;
}
