
%{
#include "SCPI.tab.hpp"
#include <stdlib.h>
#include <ctype.h>

int line_num = 1;

string *decodeText(char *text) {
	char quote = text[0];

	string *out = new string(text);
	for (std::string::iterator i=out->begin(); i!=out->end(); ++i) {
		if(*i == quote) {
			out->erase(i);
			--i;
		}
	}
	
	return out;	
}

%}

%option reentrant
%option bison-bridge

WS		[\x00-\x09\x0B-\x20]+

%%	

(\"[^\"]*\")+			{ yylval->sval = decodeText(yytext); return DOUBLE_QUOTE_STRING; }
(\'[^\']*\')+			{ yylval->sval = decodeText(yytext); return SINGLE_QUOTE_STRING; }

\*						{ return STAR; }
\?						{ return QUERY; }
:						{ return COLON; }
{WS}?,{WS}?				{ return PROGRAM_DATA_SEPARATOR; }
{WS}?;					{ return PROGRAM_MESSAGE_UNIT_SEPARATOR; }

#(h|H)[[:xdigit:]]+		{ sscanf(yytext+2, "%x", &yylval->uival); return HEXIDECIMAL; }
#(q|Q)[0-7]+			{ sscanf(yytext+2, "%o", &yylval->uival); return OCTAL; }
#(b|B)[01]+				{ yylval->uival = (unsigned int)strtol(yytext+2, NULL, 2);  return BINARY; }

(\+|-)?(([0-9]*\.[0-9]+)|([0-9]+\.?))	{ sscanf(yytext, "%lf", &yylval->fval); return MANTISSA; }
{WS}?((E|e){WS}?(\+|-)?[[:digit:]]+)	{ int start=0; while(toupper(yytext[start++])!='E');
											sscanf(yytext+start, "%d", &yylval->ival); return EXPONENT; }

[[:alpha:]][[:alnum:]_]*				{ yylval->sval = new string(yytext); return PROGRAM_MNEMONIC; }

{WS}					{ return WHITE_SPACE; }
\n						{} //{ return PMT; }

.|\n					{ yyterminate(); }

