#ifndef __VXI_LOCAL_H__
#define __VXI_LOCAL_H__

#include "vxi11.h"

#define SERVER_IP "localhost"
#define COMMAND_BUFFER_SIZE 1000
#define READ_BUFFER_SIZE 1000

typedef enum 
{
	VXI_SUCCESS = 0,
	VXI_CONNECT_ERROR =1,
	VXI_READ_ERROR = 2,
	VXI_WRITE_ERROR = 3,
	VXI_SANITY_ERROR = 4,
} vxi_stat;

int vxi_rpc_connect();
int vxi_write(char *command, size_t len);
int vxi_read(char *buffer, size_t len, int *bytesRead);

vxi_stat vxi_disconnect();
#endif
