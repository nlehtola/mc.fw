#ifndef __FLS_COMMANDS_H__
#define __FLS_COMMANDS_H__

#include "vxi_local.h"

vxi_stat vxi_get_grid(uint8_t blade_id, uint8_t laser_id, double *min, double *max, double *act);
vxi_stat vxi_get_power(uint8_t blade_id, uint8_t laser_id, double *min, double *max, double *set, double *act);
vxi_stat vxi_get_frequency(uint8_t blade_id, uint8_t laser_id, double *min, double *max, double *set, int *lock);
vxi_stat vxi_get_state(uint8_t blade_id, uint8_t laser_id, int *state);
vxi_stat vxi_get_laser_positions(uint8_t blade_id, uint8_t *mask);
vxi_stat vxi_get_laser_temp(uint8_t blade_id, uint8_t laser_id, double *temp);

vxi_stat vxi_set_grid(uint8_t blade_id, uint8_t laser_id, double frequency);
vxi_stat vxi_set_power(uint8_t blade_id, uint8_t laser_id, double power);
vxi_stat vxi_set_frequency(uint8_t blade_id, uint8_t laser_id, double frequency);
vxi_stat vxi_set_safety(uint8_t blade_id, uint8_t laser_id, int safety);
vxi_stat vxi_set_state(uint8_t blade_id, uint8_t laser_id, int state);

#endif
