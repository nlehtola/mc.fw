#ifndef __VOA_COMMANDS_H__
#define __VOA_COMMANDS_H__

#include "vxi_local.h"

vxi_stat vxi_get_opt(char* modelNumber);
vxi_stat vxi_get_voa_insertion_loss(uint8_t blade_id, uint8_t channel_id, double* insertion_loss);
vxi_stat vxi_get_voa_attenuation_offset(uint8_t blade_id, uint8_t channel_id, double* set);
vxi_stat vxi_get_voa_attenuation(uint8_t blade_id, uint8_t channel_id, double* min, double* max, double* def, double* set, double *act);
vxi_stat vxi_get_voa_power(uint8_t blade_id, uint8_t channel_id, double* min, double* max, double* def, double* set, double* act);
vxi_stat vxi_get_voa_wavelength(uint8_t blade_id, uint8_t channel_id, unsigned int* set, unsigned int* list, unsigned int* list_len);
vxi_stat vxi_get_voa_attenuation_mode(uint8_t blade_id, uint8_t channel_id, int* mode);
vxi_stat vxi_get_voa_control_mode(uint8_t blade_id, uint8_t channel_id, int* mode);
vxi_stat vxi_get_channel_mask(uint8_t blade_id, uint8_t* channel_mask);

vxi_stat vxi_set_voa_attenuation_offset(uint8_t blade_id, uint8_t channel_id, double value);
vxi_stat vxi_set_voa_attenuation(uint8_t blade_id, uint8_t channel_id, double value);
vxi_stat vxi_set_voa_power(uint8_t blade_id, uint8_t channel_id, double value);
vxi_stat vxi_set_voa_wavelength(uint8_t blade_id, uint8_t channel_id, unsigned int value);
vxi_stat vxi_set_voa_attenuation_mode(uint8_t blade_id, uint8_t channel_id, int mode);	// 0=offset 1=absolute 2=relative
vxi_stat vxi_set_voa_control_mode(uint8_t blade_id, uint8_t channel_id, int mode);	// 0=attenuation 1=tracking

#endif
