#include <stdio.h>
#include <stdlib.h>
#include "vxi11.h"
#include <string.h>
#include <ctype.h>
#include "vxi_local.h"

static char _command_buffer[COMMAND_BUFFER_SIZE];
static char _read_buffer[READ_BUFFER_SIZE];

vxi_stat vxi_get_opt(char* modelNumber)
{
	int length;

	if(vxi_rpc_connect() != 0) {
		return VXI_CONNECT_ERROR;
	}
	length = snprintf(_command_buffer, COMMAND_BUFFER_SIZE, "*OPT?");

	if(vxi_write(_command_buffer, length) != 0) {
		return VXI_WRITE_ERROR;
	}
	if(vxi_read(_read_buffer, READ_BUFFER_SIZE, &length) != 0) {
		return VXI_READ_ERROR;
	}

	if(length >= READ_BUFFER_SIZE) {
		_read_buffer[READ_BUFFER_SIZE - 1] = '\0';
	} else {
		_read_buffer[length] = '\0';
	}

	snprintf(modelNumber, 128, "%s", _read_buffer);
	return VXI_SUCCESS;
}

vxi_stat vxi_get_voa_insertion_loss(uint8_t blade_id, uint8_t channel_id, double* insertion_loss)
{
	int length;
	double _insertion_loss;

	if(vxi_rpc_connect() != 0) {
		return VXI_CONNECT_ERROR;
	}
	length = snprintf(_command_buffer, COMMAND_BUFFER_SIZE, ":RAW%u:LOSS?", channel_id);

	if(vxi_write(_command_buffer, length) != 0) {
		return VXI_WRITE_ERROR;
	}
	if(vxi_read(_read_buffer, READ_BUFFER_SIZE, &length) != 0) {
		return VXI_READ_ERROR;
	}

	if(length >= READ_BUFFER_SIZE) {
		_read_buffer[READ_BUFFER_SIZE - 1] = '\0';
	} else {
		_read_buffer[length] = '\0';
	}

	if(sscanf(_read_buffer, "%le", &_insertion_loss) == 1) {
		if (insertion_loss != NULL)
			*insertion_loss = _insertion_loss;
		return VXI_SUCCESS;
	} else {
		return VXI_READ_ERROR;
	}
}

vxi_stat vxi_get_voa_attenuation_offset(uint8_t blade_id, uint8_t channel_id, double* set)
{
	int length;
	double _min, _max, _set;

	if(vxi_rpc_connect() != 0) {
		return VXI_CONNECT_ERROR;
	}
	length = snprintf(_command_buffer, COMMAND_BUFFER_SIZE, ":INPUT%u:OFFSET?", channel_id);

	if(vxi_write(_command_buffer, length) != 0) {
		return VXI_WRITE_ERROR;
	}
	if(vxi_read(_read_buffer, READ_BUFFER_SIZE, &length) != 0) {
		return VXI_READ_ERROR;
	}

	if(length >= READ_BUFFER_SIZE) {
		_read_buffer[READ_BUFFER_SIZE - 1] = '\0';
	} else {
		_read_buffer[length] = '\0';
	}

	if(sscanf(_read_buffer, "%le", &_set) == 1) {
		if (set != NULL)
			*set = _set;
		return VXI_SUCCESS;
	} else {
		return VXI_READ_ERROR;
	}
}

vxi_stat vxi_get_voa_attenuation(uint8_t blade_id, uint8_t channel_id, double* min, double* max, double* def, double* set, double* act)
{
	int length;
	double _min, _max, _def, _set, _act;

	if(vxi_rpc_connect() != 0) {
		return VXI_CONNECT_ERROR;
	}
	length = snprintf(_command_buffer, COMMAND_BUFFER_SIZE, ":INPUT%u:ATTENUATION? ALL", channel_id);

	if(vxi_write(_command_buffer, length) != 0) {
		return VXI_WRITE_ERROR;
	}
	if(vxi_read(_read_buffer, READ_BUFFER_SIZE, &length) != 0) {
		return VXI_READ_ERROR;
	}

	if(length >= READ_BUFFER_SIZE) {
		_read_buffer[READ_BUFFER_SIZE - 1] = '\0';
	} else {
		_read_buffer[length] = '\0';
	}

	if(sscanf(_read_buffer, "%le,%le,%le,%le,%le", &_min, &_max, &_def, &_set, &_act) == 5) {
		if (min != NULL)
			*min = _min;
		if (max != NULL)
			*max = _max;
		if (def != NULL)
			*def = _def;
		if (set != NULL)
			*set = _set;
		if (act != NULL)
			*act = _act;
		return VXI_SUCCESS;
	} else {
		return VXI_READ_ERROR;
	}
}

vxi_stat vxi_get_voa_power(uint8_t blade_id, uint8_t channel_id, double* min, double* max, double* def, double* set, double* act)
{
	int length;
	double _min, _max, _def, _set, _act;

	if(vxi_rpc_connect() != 0) {
		return VXI_CONNECT_ERROR;
	}
	length = snprintf(_command_buffer, COMMAND_BUFFER_SIZE, ":OUTPUT%u:POWER? ALL", channel_id);

	if(vxi_write(_command_buffer, length) != 0) {
		return VXI_WRITE_ERROR;
	}
	if(vxi_read(_read_buffer, READ_BUFFER_SIZE, &length) != 0) {
		return VXI_READ_ERROR;
	}

	if(length >= READ_BUFFER_SIZE) {
		_read_buffer[READ_BUFFER_SIZE - 1] = '\0';
	} else {
		_read_buffer[length] = '\0';
	}

	if(sscanf(_read_buffer, "%le,%le,%le,%le,%le", &_min, &_max, &_def, &_set, &_act) == 5) {
		if (min != NULL)
			*min = _min;
		if (max != NULL)
			*max = _max;
		if (def != NULL)
			*def = _def;
		if (set != NULL)
			*set = _set;
		if (act != NULL)
			*act = _act;
		return VXI_SUCCESS;
	} else {
		return VXI_READ_ERROR;
	}
}

vxi_stat vxi_get_voa_wavelength(uint8_t blade_id, uint8_t channel_id, unsigned int* set, unsigned int* list, unsigned int* list_len)
{
	int length;
	unsigned int _set, _list_len;
	*list_len = 0;
	char *token;
	char *buffer;

	if(vxi_rpc_connect() != 0) {
		return VXI_CONNECT_ERROR;
	}
	
	// ###### Read the set point ######
	length = snprintf(_command_buffer, COMMAND_BUFFER_SIZE, ":INPUT%u:WAVELENGTH?", channel_id);

	if(vxi_write(_command_buffer, length) != 0) {
		return VXI_WRITE_ERROR;
	}
	if(vxi_read(_read_buffer, READ_BUFFER_SIZE, &length) != 0) {
		return VXI_READ_ERROR;
	}

	if(length >= READ_BUFFER_SIZE) {
		_read_buffer[READ_BUFFER_SIZE - 1] = '\0';
	} else {
		_read_buffer[length] = '\0';
	}

	if(sscanf(_read_buffer, "%u", &_set) == 1) {
		if (set != NULL)
			*set = _set;
	} else {
		return VXI_READ_ERROR;
	}
	
	// ###### Read the list of values ######
	length = snprintf(_command_buffer, COMMAND_BUFFER_SIZE, ":INPUT%u:WAVELENGTH? LIST", channel_id);

	if(vxi_write(_command_buffer, length) != 0) {
		return VXI_WRITE_ERROR;
	}
	if(vxi_read(_read_buffer, READ_BUFFER_SIZE, &length) != 0) {
		return VXI_READ_ERROR;
	}

	if(length >= READ_BUFFER_SIZE) {
		_read_buffer[READ_BUFFER_SIZE - 1] = '\0';
	} else {
		_read_buffer[length] = '\0';
	}

	_list_len = 0;
	buffer = _read_buffer;
	unsigned int _value = 0;
	if (list != NULL) {
		while((token = strsep(&buffer, ",\n")) != NULL)
		{
			if(token != NULL && strnlen(token, READ_BUFFER_SIZE) > 0)
			{
				if(sscanf(token, "%u", &_value) == 1) {
					list[_list_len] = _value;
					_list_len++;
				} else {
					return VXI_READ_ERROR;
				}
			}
		}
	}
	*list_len = _list_len;
	
	return VXI_SUCCESS;
}

vxi_stat vxi_get_voa_attenuation_mode(uint8_t blade_id, uint8_t channel_id, int* mode)
{
	int length;
	int _mode;

	if(vxi_rpc_connect() != 0) {
		return VXI_CONNECT_ERROR;
	}
	length = snprintf(_command_buffer, COMMAND_BUFFER_SIZE, ":INPUT%u:AMODE?", channel_id);

	if(vxi_write(_command_buffer, length) != 0) {
		return VXI_WRITE_ERROR;
	}
	if(vxi_read(_read_buffer, READ_BUFFER_SIZE, &length) != 0) {
		return VXI_READ_ERROR;
	}

	if(length >= READ_BUFFER_SIZE) {
		_read_buffer[READ_BUFFER_SIZE - 1] = '\0';
	} else {
		_read_buffer[length] = '\0';
	}
	
	if (strncasecmp("offset",_read_buffer, 6) == 0) {
		_mode = 0;
	} else if(strncasecmp("absolute",_read_buffer, 8) == 0) {
		_mode = 1;
	} else if(strncasecmp("relative",_read_buffer, 8) == 0) {
		_mode = 2;
	} else {
		return VXI_SANITY_ERROR;
	}
	if (mode != NULL)
		*mode = _mode;
		
	return VXI_SUCCESS;
}
	
vxi_stat vxi_get_voa_control_mode(uint8_t blade_id, uint8_t channel_id, int* mode)
{
	int length;
	int _mode;

	if(vxi_rpc_connect() != 0) {
		return VXI_CONNECT_ERROR;
	}
	length = snprintf(_command_buffer, COMMAND_BUFFER_SIZE, ":CONTROL%u:MODE?", channel_id);

	if(vxi_write(_command_buffer, length) != 0) {
		return VXI_WRITE_ERROR;
	}
	if(vxi_read(_read_buffer, READ_BUFFER_SIZE, &length) != 0) {
		return VXI_READ_ERROR;
	}

	if(length >= READ_BUFFER_SIZE) {
		_read_buffer[READ_BUFFER_SIZE - 1] = '\0';
	} else {
		_read_buffer[length] = '\0';
	}
	
	if(strncasecmp("power",_read_buffer, 5) == 0) {
		_mode= 1; //power, tracking
	} else if(strncasecmp("attenuation",_read_buffer, 11) == 0) {
		_mode= 0; //attenuation not tracking
	} else {
		return VXI_SANITY_ERROR;
	}	
	if (mode != NULL)
		*mode = _mode;
		
	return VXI_SUCCESS;
}

vxi_stat vxi_get_channel_mask(uint8_t blade_id, uint8_t* mask)
{
	int length;
	char *token;
	char *buffer;
	int i;

	if(vxi_rpc_connect() != 0)
	{
		return VXI_CONNECT_ERROR;
	}

	length = snprintf(_command_buffer,COMMAND_BUFFER_SIZE,":SLOT:OPT?");

	if(vxi_write(_command_buffer, length) != 0)
	{
		return VXI_WRITE_ERROR;
	}
	if(vxi_read(_read_buffer, READ_BUFFER_SIZE, &length) != 0)
	{
		return VXI_READ_ERROR;
	}

	if(length >= READ_BUFFER_SIZE)
	{
		_read_buffer[READ_BUFFER_SIZE - 1] = '\0';
	}
	else
	{
		_read_buffer[length] = '\0';
	}

	if(strlen(_read_buffer) < 3) return VXI_READ_ERROR;

	buffer = _read_buffer;
	*mask = 0;
	i = 0;

	while((token = strsep(&buffer, ",\n")) != NULL && i < 4)
	{
		if(token != NULL && strnlen(token, READ_BUFFER_SIZE) > 0)
		{
			*mask |= (1 << i);
		}
		i++;
	}

	return VXI_SUCCESS;
}

vxi_stat vxi_set_voa_attenuation_offset(uint8_t blade_id, uint8_t channel_id, double value)
{
	int length;

	if(vxi_rpc_connect() != 0)
	{
		return VXI_CONNECT_ERROR;
	}

	length = snprintf(_command_buffer,COMMAND_BUFFER_SIZE,":INPUT%u:OFFSET %f", channel_id, value);

	if(vxi_write(_command_buffer, length) != 0)
	{
		return VXI_WRITE_ERROR;
	}
	
	return VXI_SUCCESS;
}

vxi_stat vxi_set_voa_attenuation(uint8_t blade_id, uint8_t channel_id, double value)
{
	int length;

	if(vxi_rpc_connect() != 0)
	{
		return VXI_CONNECT_ERROR;
	}

	length = snprintf(_command_buffer,COMMAND_BUFFER_SIZE,":INPUT%u:ATTENUATION %f", channel_id, value);

	if(vxi_write(_command_buffer, length) != 0)
	{
		return VXI_WRITE_ERROR;
	}
	
	return VXI_SUCCESS;
}

vxi_stat vxi_set_voa_power(uint8_t blade_id, uint8_t channel_id, double value)
{
	int length;

	if(vxi_rpc_connect() != 0)
	{
		return VXI_CONNECT_ERROR;
	}

	length = snprintf(_command_buffer,COMMAND_BUFFER_SIZE,":OUTPUT%u:POWER %f", channel_id, value);

	if(vxi_write(_command_buffer, length) != 0)
	{
		return VXI_WRITE_ERROR;
	}
	
	return VXI_SUCCESS;
}

vxi_stat vxi_set_voa_wavelength(uint8_t blade_id, uint8_t channel_id, unsigned int value)
{
	int length;

	if(vxi_rpc_connect() != 0)
	{
		return VXI_CONNECT_ERROR;
	}

	length = snprintf(_command_buffer,COMMAND_BUFFER_SIZE,":INPUT%u:WAVELENGTH %u", channel_id, (unsigned int)value);

	if(vxi_write(_command_buffer, length) != 0)
	{
		return VXI_WRITE_ERROR;
	}
	
	return VXI_SUCCESS;
}

// 0=offset 1=relative 2=absolute
vxi_stat vxi_set_voa_attenuation_mode(uint8_t blade_id, uint8_t channel_id, int mode)
{
	int length;

	if(vxi_rpc_connect() != 0)
	{
		return VXI_CONNECT_ERROR;
	}

	if (mode==0) {
		length = snprintf(_command_buffer, COMMAND_BUFFER_SIZE, ":INPUT%u:AMODE OFFSET", channel_id);
	} else if (mode==1) {
		length = snprintf(_command_buffer, COMMAND_BUFFER_SIZE, ":INPUT%u:AMODE ABSOLUTE", channel_id);
	} else {
		length = snprintf(_command_buffer, COMMAND_BUFFER_SIZE, ":INPUT%u:AMODE RELATIVE", channel_id);
	}
	
	if(vxi_write(_command_buffer, length) != 0)
	{
		return VXI_WRITE_ERROR;
	}
	
	return VXI_SUCCESS;
}

vxi_stat vxi_set_voa_control_mode(uint8_t blade_id, uint8_t channel_id, int mode)	// 0=attenuation 1=power tracking
{
	int length;

	if(vxi_rpc_connect() != 0)
	{
		return VXI_CONNECT_ERROR;
	}

	if (mode) {
		length = snprintf(_command_buffer, COMMAND_BUFFER_SIZE, ":CONTROL%u:MODE POWER", channel_id);
	} else {
		length = snprintf(_command_buffer, COMMAND_BUFFER_SIZE, ":CONTROL%u:MODE ATTENUATION", channel_id);
	}
	
	if(vxi_write(_command_buffer, length) != 0)
	{
		return VXI_WRITE_ERROR;
	}
	
	return VXI_SUCCESS;
}


