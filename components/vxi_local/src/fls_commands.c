#include <stdio.h>
#include <stdlib.h>
#include "vxi11.h"
#include <string.h>
#include <ctype.h>
#include "vxi_local.h"

static char _command_buffer[COMMAND_BUFFER_SIZE];
static char _read_buffer[READ_BUFFER_SIZE];

vxi_stat vxi_get_grid
(uint8_t blade_id, uint8_t laser_id, double *min, double *max, double *act)
{
	int length;
	double _min, _max, _act;

	if(vxi_rpc_connect() != 0)
	{
		return VXI_CONNECT_ERROR;
	}


	length = snprintf(_command_buffer,COMMAND_BUFFER_SIZE,":SOURCE%u:CHANNEL%u:GRID? ALL", blade_id, laser_id);

	if(vxi_write(_command_buffer, length) != 0)
	{
		return VXI_WRITE_ERROR;
	}
	if(vxi_read(_read_buffer, READ_BUFFER_SIZE, &length) != 0)
	{
		return VXI_READ_ERROR;
	}

	if(length >= READ_BUFFER_SIZE)
	{
		_read_buffer[READ_BUFFER_SIZE - 1] = '\0';
	}
	else
	{
		_read_buffer[length] = '\0';
	}

	if(sscanf(_read_buffer, "%le,%le,%le", &_min, &_max, &_act) == 3)
	{
		if (min != NULL)
			*min = _min;
		if (max != NULL)
			*max = _max;
		if (act != NULL)
			*act = _act;
		return VXI_SUCCESS;
	}
	else
	{
		return VXI_READ_ERROR;	
	}
}

vxi_stat vxi_get_power(uint8_t blade_id, uint8_t laser_id, double *min, double *max, double *set, double *act)
{
	int length;
	char *sep = ",";
	char *token;
	double _min, _max, _set, _act;

	if(vxi_rpc_connect() != 0)
	{
		return VXI_CONNECT_ERROR;
	}

	length = snprintf(_command_buffer,COMMAND_BUFFER_SIZE,":SOURCE%u:CHANNEL%u:POWER? ALL", blade_id, laser_id);

	if(vxi_write(_command_buffer, length) != 0)
	{
		return VXI_WRITE_ERROR;
	}
	if(vxi_read(_read_buffer, READ_BUFFER_SIZE, &length) != 0)
	{
		return VXI_READ_ERROR;
	}

	if(length >= READ_BUFFER_SIZE)
	{
		_read_buffer[READ_BUFFER_SIZE - 1] = '\0';
	}
	else
	{
		_read_buffer[length] = '\0';
	}

	token = strtok(_read_buffer, sep); 

	if(token != NULL)
	{
		_min = atof(token);
	}
	token = strtok(NULL, sep);
	if(token != NULL)
	{
		_max = atof(token);
	}

	token = strtok(NULL, sep);
	if(token != NULL)
	{
		_set = atof(token);
	}

	token = strtok(NULL, sep);
	if(token != NULL)
	{
		_act = atof(token);
	}

	// ok all good so update pointers
	if (min)
	{
		*min = _min;
	}
	if (max)
	{
		*max = _max;
	}
	if (set)
	{
		*set = _set;
	}
	if (act)
	{
		*act = _act;
	}

	return VXI_SUCCESS;
}

vxi_stat vxi_get_frequency(uint8_t blade_id, uint8_t laser_id, double *min, double *max, double *set, int *lock)
{
	int length;
	char *sep = ",";
	char *token;
	double _min, _max, _set, _lock;

	if(vxi_rpc_connect() != 0)
	{
		return VXI_CONNECT_ERROR;
	}

	length = snprintf(_command_buffer,COMMAND_BUFFER_SIZE,":SOURCE%u:CHANNEL%u:FREQUENCY? ALL", blade_id, laser_id);

	if(vxi_write(_command_buffer, length) != 0)
	{
		return VXI_WRITE_ERROR;
	}
	if(vxi_read(_read_buffer, READ_BUFFER_SIZE, &length) != 0)
	{
		return VXI_READ_ERROR;
	}

	if(length >= READ_BUFFER_SIZE)
	{
		_read_buffer[READ_BUFFER_SIZE - 1] = '\0';
	}
	else
	{
		_read_buffer[length] = '\0';
	}

	token = strtok(_read_buffer, sep); 

	if(token != NULL)
	{
		_min = atof(token);
	}
	token = strtok(NULL, sep);
	if(token != NULL)
	{
		_max = atof(token);
	}

	token = strtok(NULL, sep);
	if(token != NULL)
	{
		_set = atof(token);
	}

	token = strtok(NULL, sep);
	if(token != NULL)
	{
		if(strncasecmp("true",token, 2) == 0)
		{
			_lock= 1; //on
		}
		else if(strncasecmp("false",token, 3) == 0)
		{
			_lock= 0; //off
		}
		else
		{
			return VXI_SANITY_ERROR;
		}
	}

	// some sanity checks
	if (_set < _min || _set > _max)
	{
		return VXI_SANITY_ERROR;
	}

	// ok all good so update pointers
	if (min)
	{
		*min = _min;
	}
	if (max)
	{
		*max = _max;
	}
	if (set)
	{
		*set = _set;
	}
	if (lock)
	{
		*lock = _lock;
	}

	return VXI_SUCCESS;
}

vxi_stat vxi_get_state(uint8_t blade_id, uint8_t laser_id, int *state)
{
	int length;

	if(vxi_rpc_connect() != 0)
	{
		return VXI_CONNECT_ERROR;
	}

	length = snprintf(_command_buffer,COMMAND_BUFFER_SIZE,":OUTPUT%u:CHANNEL%u:STATE?", blade_id, laser_id);

	if(vxi_write(_command_buffer, length) != 0)
	{
		return VXI_WRITE_ERROR;
	}
	if(vxi_read(_read_buffer, READ_BUFFER_SIZE, &length) != 0)
	{
		return VXI_READ_ERROR;
	}

	if(length >= READ_BUFFER_SIZE)
	{
		_read_buffer[READ_BUFFER_SIZE - 1] = '\0';
	}
	else
	{
		_read_buffer[length] = '\0';
	}

	if(strncasecmp("on",_read_buffer, 2) == 0)
	{
		*state = 1; //on
	}
	else if(strncasecmp("off",_read_buffer, 3) == 0)
	{
		*state = 0; //off
	}
	else
	{
		return VXI_SANITY_ERROR;
	}

	return VXI_SUCCESS;
}

vxi_stat vxi_get_laser_positions(uint8_t blade_id, uint8_t *mask)
{
	int length;
	char *token;
	char *buffer;
	int i;

	if(vxi_rpc_connect() != 0)
	{
		return VXI_CONNECT_ERROR;
	}

	length = snprintf(_command_buffer,COMMAND_BUFFER_SIZE,":SLOT%u:OPT?", blade_id);

	if(vxi_write(_command_buffer, length) != 0)
	{
		return VXI_WRITE_ERROR;
	}
	if(vxi_read(_read_buffer, READ_BUFFER_SIZE, &length) != 0)
	{
		return VXI_READ_ERROR;
	}

	if(length >= READ_BUFFER_SIZE)
	{
		_read_buffer[READ_BUFFER_SIZE - 1] = '\0';
	}
	else
	{
		_read_buffer[length] = '\0';
	}

	if(strlen(_read_buffer) < 3) return VXI_READ_ERROR;

	if((buffer = strndup(_read_buffer,READ_BUFFER_SIZE)) == NULL)
	{
		return VXI_READ_ERROR;
	}

	*mask = 0;
	i = 0;

	while((token = strsep(&buffer, ",\n")) != NULL && i < 4)
	{

		if(token != NULL && strnlen(token, READ_BUFFER_SIZE) > 0)
		{
			*mask |= (1 << i);
		}
		i++;
	}

	free(buffer);

	return VXI_SUCCESS;
}

vxi_stat vxi_get_laser_temp(uint8_t blade_id, uint8_t laser_id, double *temp)
{
	int length;
	char *buffer;

	if(vxi_rpc_connect() != 0)
	{
		return VXI_CONNECT_ERROR;
	}

	length = snprintf(_command_buffer,COMMAND_BUFFER_SIZE,":SOUR%u:CHAN%u:TEMP?", blade_id, laser_id);

	if(vxi_write(_command_buffer, length) != 0)
	{
		return VXI_WRITE_ERROR;
	}
	if(vxi_read(_read_buffer, READ_BUFFER_SIZE, &length) != 0)
	{
		return VXI_READ_ERROR;
	}

	if(length >= READ_BUFFER_SIZE)
	{
		_read_buffer[READ_BUFFER_SIZE - 1] = '\0';
	}
	else
	{
		_read_buffer[length] = '\0';
	}

	if(strlen(_read_buffer) < 3) return VXI_READ_ERROR;

	if((buffer = strndup(_read_buffer,READ_BUFFER_SIZE)) == NULL)
	{
		return VXI_READ_ERROR;
	}
	
	*temp = atof(buffer);

	free(buffer);

	return VXI_SUCCESS;
}

vxi_stat vxi_set_grid(uint8_t blade_id, uint8_t laser_id, double frequency)
{
	int length;

	if(vxi_rpc_connect() != 0)
	{
		return VXI_CONNECT_ERROR;
	}

	length = snprintf(_command_buffer,COMMAND_BUFFER_SIZE,":SOURCE%u:CHANNEL%u:GRID %f", blade_id, laser_id, frequency);

	if(vxi_write(_command_buffer, length) != 0)
	{
		return VXI_WRITE_ERROR;
	}
	
	return VXI_SUCCESS;
}

vxi_stat vxi_set_power(uint8_t blade_id, uint8_t laser_id, double power)
{
	int length;

	if(vxi_rpc_connect() != 0)
	{
		return VXI_CONNECT_ERROR;
	}

	length = snprintf(_command_buffer,COMMAND_BUFFER_SIZE,":SOURCE%u:CHANNEL%u:POWER %f", blade_id, laser_id, power);

	if(vxi_write(_command_buffer, length) != 0)
	{
		return VXI_WRITE_ERROR;
	}
	
	return VXI_SUCCESS;
}

vxi_stat vxi_set_frequency(uint8_t blade_id, uint8_t laser_id, double frequency)
{
	int length;

	if(vxi_rpc_connect() != 0)
	{
		return VXI_CONNECT_ERROR;
	}

	length = snprintf(_command_buffer,COMMAND_BUFFER_SIZE,":SOURCE%u:CHANNEL%u:FREQUENCY %f", blade_id, laser_id, frequency);

	if(vxi_write(_command_buffer, length) != 0)
	{
		return VXI_WRITE_ERROR;
	}
	
	return VXI_SUCCESS;
}

vxi_stat vxi_set_safety(uint8_t blade_id, uint8_t laser_id, int safety)
{
	int length;

	if(vxi_rpc_connect() != 0)
	{
		return VXI_CONNECT_ERROR;
	}

	if(safety == 1)
	{
		length = snprintf(_command_buffer,COMMAND_BUFFER_SIZE,":OUTPUT%u:CHANNEL%u:SAFETY ON", blade_id, laser_id);
	}
	else
	{
		length = snprintf(_command_buffer,COMMAND_BUFFER_SIZE,":OUTPUT%u:CHANNEL%u:SAFETY OFF", blade_id, laser_id);
	}
	
	if(vxi_write(_command_buffer, length) != 0)
	{
		return VXI_WRITE_ERROR;
	}
	
	return VXI_SUCCESS;
}

vxi_stat vxi_set_state(uint8_t blade_id, uint8_t laser_id, int state)
{
	int length;

	if(vxi_rpc_connect() != 0)
	{
		return VXI_CONNECT_ERROR;
	}

	if(state == 1)
	{
		length = snprintf(_command_buffer,COMMAND_BUFFER_SIZE,":OUTPUT%u:CHANNEL%u:STATE ON", blade_id, laser_id);
	}
	else
	{

		length = snprintf(_command_buffer,COMMAND_BUFFER_SIZE,":OUTPUT%u:CHANNEL%u:STATE OFF", blade_id, laser_id);
	}

	if(vxi_write(_command_buffer, length) != 0)
	{
		return VXI_WRITE_ERROR;
	}
	
	return VXI_SUCCESS;
}

