#include <stdio.h>
#include <stdlib.h>
#include "vxi11.h"
#include <string.h>
#include <ctype.h>
#include "vxi_local.h"

static Device_Link _link_id;
static int _connected = 0;
static CLIENT *_rpc_client;

int vxi_rpc_connect()
{
	Create_LinkResp linkResp;
	Create_LinkParms linkParms;
	enum clnt_stat status;

	if(_connected == 1) return 0;

	_connected = 0;

	_rpc_client = clnt_create(SERVER_IP, DEVICE_CORE, DEVICE_CORE_VERSION, "tcp");

	if(_rpc_client == NULL)
	{
		_connected = 0;
		return 1;
	}

	linkParms.clientId = 100;
	linkParms.lockDevice = 0;
	linkParms.lock_timeout = 20000;
	linkParms.device = "inst0";

	if((status = create_link_1(&linkParms, &linkResp, _rpc_client)) != RPC_SUCCESS)
	{
		return 1;
	}

	if(linkResp.error != 0)
	{
		return 1;
	}

	_link_id = linkResp.lid;

	_connected = 1;
	return 0;
}

int vxi_write(char *command, size_t len)
{
	Device_WriteParms writeParms;
	Device_WriteResp writeResp;
	enum clnt_stat status;

	if(vxi_rpc_connect() != 0)
	{
		return 1;
	}

	writeParms.lid = _link_id;
	writeParms.io_timeout = 20000;
	writeParms.lock_timeout = 20000;
	writeParms.flags = 8;
	writeParms.data.data_len = strlen(command);
	writeParms.data.data_val = command;

	if((status = device_write_1(&writeParms, &writeResp, _rpc_client)) != RPC_SUCCESS)
	{
		_connected = 0;
		return -1;
	}

	return 0;
}

int vxi_read(char *buffer, size_t len, int *bytesRead)
{
	*bytesRead = 0;
	Device_ReadParms readParms;
	Device_ReadResp readResp;
	enum clnt_stat status;

	readResp.data.data_len = len;
	readResp.data.data_val = buffer;

	readParms.lid = _link_id;
	readParms.requestSize = len;
	readParms.io_timeout = 20000;
	readParms.lock_timeout = 20000;
	readParms.flags = 0;
	readParms.termChar = 0;

	if((status = device_read_1(&readParms, &readResp, _rpc_client)) != RPC_SUCCESS)
	{
		//vxi_disconnect();
		_connected = 0;
		return 1;
	}

	if(readResp.error != 0)
	{
		// For any minor errors do not break the connection
		// These are errors in vxi11 explicitly, such as reading when there 
		// is no data to read
		
		// But for errors which invalidate the current connection we must 
		// disconnect and then reconnect
		if (readResp.error==4) {	// invalid linkid
			vxi_disconnect();
			_connected = 0;
		}
		return 1;
	}

	*bytesRead = readResp.data.data_len;
	return 0;
}

vxi_stat vxi_disconnect()
{
	Device_Error err;

	if(_rpc_client != NULL)
	{
		destroy_link_1(&(_link_id), &err, _rpc_client);
	}
	_connected = 0;
	return VXI_SUCCESS;
}
