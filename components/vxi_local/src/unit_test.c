#include <stdio.h>
#include "vxi_local.h"

int main(int argc, char *argv[])
{
	double min_power, max_power, set_power, act_power;
	double min, max, act;
	int laser_state;
	uint8_t laser_mask;
	int blade_id, laser_id;

	if(argc != 3)
	{
		printf("USAGE: %s [blade_id] [laser_id]\n", argv[0]);
		return 1;
	}

	blade_id = atoi(argv[1]);
	laser_id = atoi(argv[2]);

	if(vxi_get_power(blade_id, laser_id, &min_power, &max_power, 
					 &set_power, &act_power) == VXI_SUCCESS)
	{
		printf("Minimum power %f\n", min_power);
		printf("Maximum power %f\n", max_power);
		printf("Set power %f\n", set_power);
		printf("Actual power %f\n", act_power);
	}
	else
	{
		printf("Unable to get power\n");
	}

	if(vxi_set_grid(blade_id, laser_id, 1e8) == VXI_SUCCESS)
	{
		printf("Laser grid set\n");
	}
	else
	{
		printf("Unable to set laser grid\n");
	}

	if(vxi_get_grid(blade_id, laser_id, &min, &max, &act) == VXI_SUCCESS)
	{
		printf("Minimum grid %f\n", min);
		printf("Maximum grid %f\n", max);
		printf("Actual grid %f\n", act);
	}
	else
	{
		printf("Unable to get grid\n");
	}

	if(vxi_set_state(blade_id, laser_id, 1) == VXI_SUCCESS)
	{
		printf("Laser enabled\n");
	}
	else
	{
		printf("Unable to enable laser\n");
	}

	if(vxi_get_state(blade_id, laser_id, &laser_state) == VXI_SUCCESS)
	{
		printf("Laser state %d\n", laser_state);
	}
	else
	{
		printf("Unable to get laser state\n");
	}

	if(vxi_get_laser_positions(blade_id, &laser_mask) == VXI_SUCCESS)
	{
		printf("Laser mask %x\n", laser_mask);
	}
	else
	{
		printf("Unable to get laser mask\n");
	}

	vxi_disconnect();	
	return 0;
}

