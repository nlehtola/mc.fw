#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

#include <kowhai.h>
#include <kowhai_protocol.h>

#include <bl.h>

#ifndef packed
#define packed(type) type __attribute__ ((packed))      		// pack this object (ie struct etc)
#endif

#define WTIMEOUT (0.5)
#define RTIMEOUT (0.5)


// simple handle to hold all our state info
static struct handle_t
{
	// io
	bl_read_t read;
	int read_pkt_size;
	void *read_buf;	
	bl_write_t write;
	int write_pkt_size;
	void *write_buf;

	// kowhai objs
	struct kowhai_protocol_t prot;

	// cached values
	uint32_t version;
	char *syms_buf;
	char **syms_tbl;
	int syms_tbl_len;

} _h = {0,};
static struct handle_t *h = &_h;


static int bl_write_pkt(void *buf, int len, double timeout)
{
	int r;
	uint8_t *_buf = buf;
	time_t start_time;

	// sanity checks
	if (len > h->write_pkt_size)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: write called with invalid len [%d]\n", __func__,  __LINE__, len);
		return -1;
	}
	if (h->write == NULL)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: write called, but NULL\n", __func__,  __LINE__);
		return -1;
	}

	// put a 'k' out the front for kowhai packet
	// note buf was alloced to be write_pkt_size + 1 but index 1 used
	// for the kowhai buffers, so we know there really is a index -1
	_buf[-1] = 'k';

	// try to send
	///@todo write with timeout
	time(&start_time);
	while ((r = h->write(&_buf[-1], h->read_pkt_size + 1)) < 0)
	{
		#ifndef NO_TIMEOUTS
		if (timeout >= 0)
		{
			if (difftime(time(NULL), start_time) > timeout)
			{
				double dt = difftime(time(NULL), start_time);
				printf("ERROR: "__FILE__ ":%s:[%d]: timed out (dt = %lf)\n", __func__,  __LINE__, dt);
				break;
			}
		}
		#endif
	}

	return r;
}


static int bl_read_pkt(void *buf, int len, double timeout)
{
	int r;
	uint8_t *_buf = buf;
	time_t start_time;
	
	// sanity checks
	if (len > h->read_pkt_size)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: read called with invalid len [%d]\n", __func__,  __LINE__, len);
		return -1;
	}
	if (h->read == NULL)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: read called, but NULL\n", __func__,  __LINE__);
		return -1;
	}

	// try to read
	///@todo read with timeout
	time(&start_time);
	while ((r = h->read(&_buf[-1], h->write_pkt_size + 1)) < 0)
	{
		#ifndef NO_TIMEOUTS
		if (timeout >= 0)
		{
			if (difftime(time(NULL), start_time) > timeout)
			{
				double dt = difftime(time(NULL), start_time);
				printf("ERROR: "__FILE__ ":%s:[%d]: timed out (dt = %lf)\n", __func__,  __LINE__, dt);
				break;
			}
		}
		#endif
	}

	// check for kowhai pack and strip 'k'
	// note buf was alloced to be read_pkt_size + 1 but index 1 used
	// for the kowhai buffers, so we know there really is a index -1
	if (_buf[-1] != 'k')
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: bcoms_read(...) = %d gave buf[-1] == \\x%.2x != 'k'\n", __func__, __LINE__, r, _buf[-1]);
		return -1;
	}

	return r;
}


static int bl_get_version(struct kowhai_protocol_t *prot)
{
	int bytes_required;
	int r;

	memset(h->read_buf, 0, h->read_pkt_size);
	memset(h->write_buf, 0, h->write_pkt_size);

	// request version number
	POPULATE_PROTOCOL_CMD((*prot), KOW_CMD_GET_VERSION, 0);
	if (kowhai_protocol_create(h->write_buf, h->write_pkt_size, prot, &bytes_required) != KOW_STATUS_OK)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: kowhai_protocol_create() fail\n", __func__,  __LINE__);
		return -1;
	}
	if ((r = bl_write_pkt(h->write_buf, h->write_pkt_size, WTIMEOUT)) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: bl_write_pkt() fail\n", __func__,  __LINE__);
		return -1;
	}

	// get response (hopefully containing the version number)
	if ((r = bl_read_pkt(h->read_buf, h->read_pkt_size, RTIMEOUT)) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: bl_read_pkt() fail\n", __func__, __LINE__);
		return -1;
	}
	kowhai_protocol_parse(h->read_buf, r, prot);
	if (prot->header.command != KOW_CMD_GET_VERSION_ACK)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: response not KOW_CMD_GET_VERSION_ACK\n", __func__, __LINE__);
		return -1;
	}
	
	return prot->payload.spec.version;
}


static int bl_get_descriptor(struct kowhai_protocol_t *prot)
{
}


static int bl_get_function_list(struct kowhai_protocol_t *prot)
{
}


static int bl_get_descriptor_list(struct kowhai_protocol_t *prot)
{
}


static int bl_get_descriptors(struct kowhai_protocol_t *prot)
{
}


static int bl_symid(char *sym)
{
	int s;
	for (s = 0; s < h->syms_tbl_len; s++)
	{
		if (!strcmp(sym, h->syms_tbl[s]))
			return s;
	}

	return -1;
}


static int bl_get_symbols(struct kowhai_protocol_t *prot, char **syms_buf, char ***syms_tbl)
{
	int bytes_required;
	int r;
	size_t syms_buf_size = 0;
	int offset = 0;
	int syms_count = 0;
	int k;

	// request the symbols table
	POPULATE_PROTOCOL_GET_SYMBOL_LIST((*prot));
	if (kowhai_protocol_create(h->write_buf, h->write_pkt_size, prot, &bytes_required) != KOW_STATUS_OK)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: kowhai_protocol_create() fail\n", __func__, __LINE__);
		return -1;
	}
	if ((r = bl_write_pkt(h->write_buf, h->write_pkt_size, WTIMEOUT)) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: bl_write_pkt() fail\n", __func__, __LINE__);
		return -1;
	}

	// read symbols until we get them all or timeout
	do
	{
		memset(h->read_buf, 0, h->read_pkt_size);
		if ((r = bl_read_pkt(h->read_buf, h->read_pkt_size, RTIMEOUT)) < 0)
		{
			printf("ERROR: "__FILE__ ":%s:[%d]: bl_read_pkt() fail[%d]\n", __func__, __LINE__, r);
			return -1;
		}
		kowhai_protocol_parse(h->read_buf, r, prot);
		if (prot->header.command != KOW_CMD_GET_SYMBOL_LIST_ACK && prot->header.command != KOW_CMD_GET_SYMBOL_LIST_ACK_END)
		{
			printf("ERROR: "__FILE__ ":%s:[%d]: prot->header.command not right [%d]\n", __func__, __LINE__, prot->header.command);
			return -1;
		}

		// copy payload into syms (resizing as needed)
		offset = prot->payload.spec.string_list.offset;
		syms_buf_size += prot->payload.spec.string_list.size;
		*syms_buf = realloc(*syms_buf, syms_buf_size );
		memcpy(*syms_buf + offset, prot->payload.buffer, prot->payload.spec.string_list.size);
	}
	while (prot->header.command != KOW_CMD_GET_SYMBOL_LIST_ACK_END);

	// count the number of symbols we read
	offset = 0;
	while (offset < syms_buf_size)
	{
		syms_count++;	
		offset += strlen(*syms_buf + offset) + 1;
	}

	// build the symbols table
	*syms_tbl = realloc(*syms_tbl, syms_count * sizeof(char *));
	offset = 0;
	for (k = 0; k < syms_count; k++)
	{
		(*syms_tbl)[k] = *syms_buf + offset;
		offset += strlen(*syms_buf + offset) + 1;
	}
	
	return syms_count;
}


static int bl_call_func(int func_sym_id, void *params, int params_size, void *ret, int ret_size, double read_timeout, double write_timeout)
{
	int bytes_required;
	int r;
	bool res = false; // for now all commands return bool

	memset(h->write_buf, 0, h->write_pkt_size);

	///@todo check params_size will fit in buffer or support multiple packets
	///@todo merge our cmd in to command descriptor from fw so we are future proof

	// call function
	///@todo support multi packet request
	POPULATE_PROTOCOL_CALL_FUNCTION(h->prot, func_sym_id, 0, params_size, params);
	if (kowhai_protocol_create(h->write_buf, h->write_pkt_size, &h->prot, &bytes_required) != KOW_STATUS_OK)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: kowhai_protocol_create() fail\n", __func__, __LINE__);
		return -1;
	}
	if ((r = bl_write_pkt(h->write_buf, h->write_pkt_size, write_timeout) < 0))
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: bl_write_pkt() fail\n", __func__, __LINE__);
		return -1;
	}

	// get result
	///@todo support multi packet response and merge response into ours
	memset(h->read_buf, 0, h->read_pkt_size);
	if ((r = bl_read_pkt(h->read_buf, h->read_pkt_size, read_timeout)) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: bl_read_pkt() fail[%d]\n", __func__, __LINE__, r);
		return -1;
	}
	kowhai_protocol_parse(h->read_buf, r, &h->prot);
	if (h->prot.header.command != KOW_CMD_CALL_FUNCTION_RESULT_END)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: prot->header.command not right [0x%.4X]\n", __func__, __LINE__, h->prot.header.command);
		return -1;
	}
	if (h->prot.header.id != func_sym_id)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: prot->header.id not the same as calling function [%d != %d]\n", __func__, __LINE__, func_sym_id, h->prot.header.id);
		return -1;
	}
	if (h->prot.payload.spec.function_call.offset != 0 || h->prot.payload.spec.function_call.size != ret_size)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid response (response type may have changed) [o:%d, s:%d]\n", __func__, __LINE__, h->prot.payload.spec.function_call.offset, h->prot.payload.spec.function_call.size);
		return -1;
	}
	memcpy(ret, h->prot.payload.buffer, ret_size);

	return 0;
}


int bl_get_firmware_version(uint16_t *major, uint16_t *minor)
{
	int sym_id = bl_symid("version");
	packed(struct)
	{
		uint16_t major;
		uint16_t minor;
	} ret;

	// sanity check
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol (function not support by server)\n",  __func__, __LINE__);
		return -1;
	}

	// call function
	if (bl_call_func(sym_id, NULL, 0, &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: func call failed\n", __func__, __LINE__);
		return -1;
	}
	
	if (major != NULL)
		*major = ret.major;
	if (minor != NULL)
		*minor = ret.minor;

	return 1;
}


int bl_get_hw_id(uint16_t *hw_id)
{
	int sym_id = bl_symid("get_hw_id");
	packed(struct)
	{
		uint16_t hw_id;
	} ret;

	// sanity check
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol (function not support by server)\n",  __func__, __LINE__);
		return -1;
	}

	// call function
	if (bl_call_func(sym_id, NULL, 0, &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: func call failed\n", __func__, __LINE__);
		return -1;
	}
	
	if (hw_id != NULL)
		*hw_id = ret.hw_id;

	return 1;
}


int bl_unlock(uint8_t pid, uint32_t key, uint32_t addr, uint16_t hw_id)
{
	int sym_id = bl_symid("unlock");
	packed(struct)
	{
		uint8_t pid;
		uint32_t key;
		uint32_t addr;
		uint16_t hw_id;
	} cmd = {pid, key, addr, hw_id};
	bool ret = false;

	// sanity check
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol (function not support by server)\n",  __func__, __LINE__);
		return -1;
	}

	// call function
	if (bl_call_func(sym_id, &cmd, sizeof(cmd), &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: func call failed\n", __func__, __LINE__);
		return -1;
	}

	return ret;
}

#define ERASE_RTIMEOUT (30) // this will take much longer as the erase op takes a long time and is blocking
int bl_erase(uint32_t addr, uint32_t len)
{
	int sym_id = bl_symid("erase");
	packed(struct)
	{
		uint32_t addr;
		uint32_t len;
	} cmd = {addr, len};
	bool ret = false;

	// sanity check
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol (function not support by server)\n", __func__, __LINE__);
		return -1;
	}

	// call function
	if (bl_call_func(sym_id, &cmd, sizeof(cmd), &ret, sizeof(ret), ERASE_RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: func call failed\n", __func__, __LINE__);
		return -1;
	}

	return ret;
}


#define MAX_WV_BUF_LEN 48
int bl_verify(uint32_t addr, void *data, uint8_t len)
{
	int sym_id = bl_symid("verify");
	packed(struct)
	{
		uint32_t addr;
		uint8_t len;
		uint8_t data[MAX_WV_BUF_LEN];
	} cmd = {addr, len, {0,}};
	bool ret = false;

	// sanity checks
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol (function not support by server)\n", __func__, __LINE__);
		return -1;
	}
	if (len > MAX_WV_BUF_LEN)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid buffer length\n", __func__, __LINE__);
		return -1;
	}

	// call function
	memcpy(cmd.data, data, len);
	if (bl_call_func(sym_id, &cmd, sizeof(cmd), &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: func call failed\n", __func__, __LINE__);
		return -1;
	}

	return ret;
}


int bl_write(uint32_t addr, void *data, uint8_t len)
{
	int sym_id = bl_symid("write");
	packed(struct)
	{
		uint32_t addr;
		uint8_t len;
		uint8_t data[MAX_WV_BUF_LEN];
	} cmd = {addr, len, {0,}};
	bool ret = false;

	// sanity checks
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol (function not support by server)\n", __func__, __LINE__);
		return -1;
	}
	if (len > MAX_WV_BUF_LEN)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid buffer length\n", __func__, __LINE__);
		return -1;
	}

	// call function
	memcpy(cmd.data, data, len);
	if (bl_call_func(sym_id, &cmd, sizeof(cmd), &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: func call failed\n", __func__, __LINE__);
		return -1;
	}

	return ret;
}


int bl_crc(uint32_t addr, int32_t len, uint32_t *crc)
{
	int sym_id = bl_symid("crc");
	packed(struct)
	{
		uint32_t addr;
		uint32_t len;
	} cmd = {addr, len};

	// sanity check
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol (function not support by server)\n", __func__, __LINE__);
		return -1;
	}

	// call function
	if (bl_call_func(sym_id, &cmd, sizeof(cmd), crc, sizeof(*crc), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: func call failed\n", __func__, __LINE__);
		return -1;
	}

	return 1;
}


int bl_reboot(uint8_t pid)
{
	int sym_id = bl_symid("reboot");
	packed(struct)
	{
		uint32_t pid;
	} cmd = {pid};
	bool ret = false;

	// sanity check
	if (sym_id < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: invalid symbol (function not support by server)\n", __func__, __LINE__);
		return -1;
	}

	// call function
	if (bl_call_func(sym_id, &cmd, sizeof(cmd), &ret, sizeof(ret), RTIMEOUT, WTIMEOUT) < 0)
	{
		printf("ERROR: "__FILE__ ":%s:[%d]: func call failed\n", __func__, __LINE__);
		return -1;
	}

	return ret;
}


void bl_dump_version(void)
{
	printf("server kowhai version = %d\n", h->version);
}


void bl_dump_tree(void)
{
}


void bl_dump_syms(void)
{
	int k;

	if (h->syms_buf == NULL || h->syms_tbl == NULL || h->syms_tbl_len < 1)
		return;
	
	printf("symbol table:\n");
	for (k = 0; k < h->syms_tbl_len; k++)
		printf("\t%.2d: %s\n", k, h->syms_tbl[k]);
}


int bl_init(bl_read_t read, int read_pkt_size, bl_write_t write, int write_pkt_size)
{
	int r; 

	memset(h, 0, sizeof(*h));
	
	// init io callouts, buffers etc
	h->read = read;
	h->read_pkt_size = read_pkt_size - 1;
	h->read_buf = malloc(read_pkt_size);
	if (h->read_buf++ == NULL)
		return -1;
	h->write = write;
	h->write_pkt_size = write_pkt_size - 1;
	h->write_buf = malloc(write_pkt_size);
	if (h->write_buf++ == NULL)
		return -1;

	// try for a while to get the version number 
	// this loop is used to poll for the presence of 
	// the bootloader program
	h->version = 0;
	printf(__FILE__": " "%s[%d]: " "get version\n", __func__, __LINE__);
	r = bl_get_version(&h->prot);
	if (r < 0)
		return r;
	h->version = r;
		
	// request the symbols (just for humans)
	h->syms_buf = NULL;
	h->syms_tbl = NULL;
	h->syms_tbl_len = 0;
	printf(__FILE__": " "%s[%d]: " "get symbols\n", __func__, __LINE__);
	r = bl_get_symbols(&h->prot, &h->syms_buf, &h->syms_tbl);
	if (r < 0)
		return r;
	h->syms_tbl_len = r;

	///@todo request the tree (desciptors and values)
	
	return 0;
}


int bl_deinit(void)
{
	if (h->read_buf != NULL)
		free(h->read_buf - 1);
	if (h->write_buf != NULL)
		free(h->write_buf - 1);
	if (h->syms_buf != NULL)
		free(h->syms_buf);
	if (h->syms_tbl != NULL)
		free(h->syms_tbl);
}

