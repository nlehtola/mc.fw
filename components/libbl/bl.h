#ifndef __BL_H__
#define __BL_H__

typedef int (*bl_write_t)(void *buf, int len);

typedef int (*bl_read_t)(void *buf, int len);

int bl_get_firmware_version(uint16_t *major, uint16_t *minor);

int bl_get_hw_id(uint16_t *hw_id);

int bl_unlock(uint8_t pid, uint32_t key, uint32_t addr, uint16_t hw_id);

int bl_erase(uint32_t addr, uint32_t len);

int bl_verify(uint32_t addr, void *data, uint8_t len);

int bl_write(uint32_t addr, void *data, uint8_t len);

int bl_crc(uint32_t addr, int32_t len, uint32_t *crc);

int bl_reboot(uint8_t pid);

void bl_dump_version(void);

void bl_dump_tree(void);

int bl_init(bl_read_t read, int read_pkt_size, bl_write_t write, int write_pkt_size);

int bl_deinit(void);

#endif
