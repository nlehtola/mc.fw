# Synopsys, Inc. constraint file
# Y:/Documents/tslogic/ts4700/ts4700_syn.sdc
# Written on Wed Jun 15 17:04:20 2011
# by Synplify Pro for Lattice, D-2010.03L-SP1 Scope Editor

#
# Collections
#

#
# Clocks
#
define_clock   {n:ts4700pll.CLKOP} -name {pll_100mhz}  -freq 110 -clockgroup default_clkgroup_0
define_clock   {sysconcore.rndosc} -name {random_clk}  -freq 5 -clockgroup default_clkgroup_1
define_clock   {n:ts4700pll2.CLKOP} -name {pll_75mhz}  -freq 83 -clockgroup default_clkgroup_2

#
# Clock to Clock
#

#
# Inputs/Outputs
#

#
# Registers
#

#
# Delay Paths
#

#
# Attributes
#

#
# I/O Standards
#

#
# Compile Points
#

#
# Other
#
