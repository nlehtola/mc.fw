module wb_readbuf(
  wb_clk_i,
  wb_rst_i,

  wbs_cyc_i,
  wbs_stb_i,
  wbs_we_i,
  wbs_dat_i,
  wbs_dat_o,
  wbs_ack_o,

  wbm_cyc_o,
  wbm_stb_o,
  wbm_we_o,
  wbm_dat_o,
  wbm_dat_i,
  wbm_ack_i
);

input wb_clk_i, wb_rst_i, wbs_cyc_i, wbs_stb_i, wbs_we_i, wbm_ack_i;
output [31:0] wbs_dat_o, wbm_dat_o;
input [31:0] wbs_dat_i, wbm_dat_i;
output wbs_ack_o, wbm_cyc_o, wbm_stb_o, wbm_we_o;

reg ack /* synthesis syn_maxfan=1 */;
reg [31:0] dat;
assign wbm_cyc_o = wbs_cyc_i && !ack;
assign wbm_stb_o = wbs_stb_i && !ack;
assign wbm_we_o = wbs_we_i;
assign wbm_dat_o = wbs_dat_i;
assign wbs_ack_o = wbs_we_i ? wbm_ack_i : ack;
assign wbs_dat_o = dat;
always @(posedge wb_clk_i or posedge wb_rst_i) begin
  if (wb_rst_i) begin
    ack <= 1'b0;
    dat <= 32'd0;
  end else begin
    ack <= 1'b0;
    if (wbm_cyc_o && wbm_stb_o && wbm_ack_i && !wbm_we_o) begin
      dat <= wbm_dat_i;
      ack <= 1'b1;
    end
  end
end

endmodule
