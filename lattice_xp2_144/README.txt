
INSTRUCTIONS ON BUILDING A RELOADABLE BITSTREAM FOR THE TS-4700

1) Make any desired changes to the verilog files.  The simplest changes
are to enable and disable optional cores using the parameters in ts4700_top.v.
Modifying the 'custom' parameter in syscon.v is also recommended.  This is a
16 bit hexadecimal "sub-model" ID code that will allow easy software
confirmation that your changes took effect.

2) Open the project in Lattice Diamond.  In the "File List" tab, note that
the LFXP2-5E-5TN144C is selected.  If your TS-4700 uses this part, you are 
ready to build.  If your TS-4700 uses the LFXP2-8E-5TN144C, right-click on
the part number and select edit to change this.

3) On the "Process" tab, right-click on "Place and Route Trace" and select
"Run."  It will take several minutes to synthesize, map, and place and route
the design.  When it finishes, make sure the report indicates 0 timing errors.

4) Right-click on "JEDEC File" and select "Run."  This should create the file
ts4700_default.jed

5) Create a VME file:
./jed2mve.x86 ts4700_default.jed > ts4700_bitstream.vme

6) Compress your bitstream:
gzip -9 ts4700_bitstream.vme

7) Copy your bitstream to the root directory in the fastboot environment on
the TS-4700.  Save the initrd to the boot media with the 'save' command.  Your
new bitstream will now be automatically loaded on each boot.

8) Run 'ts4700ctl --info' and confirm that the submodel value is what you set
it to in verilog.  You are now running a reloaded custom bitstream.

NOTES

- The default linuxrc script on the TS-4700 automatically checks for a file
called /ts4700_bitstream.vme.gz and loads it if it exists.  You must use this
exact name or edit linuxrc accordingly.

- The jed2vme utility source is also provided, and jed2vme is also available
on the TS-4700.  This utility creates an optimized bitstream that loads in
about 3 seconds.  As of this writing, jed2vme only works with the 5K LUT 
(5E part number) FPGA.  It is also possible to use standard Lattice tools that
are part of the Lattice Diamond package to create a .vme file.  If you do this,
it is important to make sure you create a file that only loads the FPGA SRAM,
not the FPGA flash.  If you reload the FPGA flash, you will erase the
TS-BOOTROM and brick the TS-4700.  At this point the board will have to be
returned to Technologic Systems for reprogramming at your expense.

- If your application needs to dynamically reload with different bitstreams
without rebooting, please note that this is not supported on the TS-4700.  It
is possible, but any bus cycles emitted by the CPU to FPGA address space
during the reload could hang the board or return invalid data.  If your
application has a touchscreen, then the touchscreen driver will query the
FPGA at 100Hz any time it is installed.  In summary, the FPGA can only be
reloaded when the touchscreen driver, ts4700ctl, and any other processes that
try to talk to the FPGA, are not running.

