/* Copyright 2007-2011, Unpublished Work of Technologic Systems
 * All Rights Reserved.
 *
 * THIS WORK IS AN UNPUBLISHED WORK AND CONTAINS CONFIDENTIAL,
 * PROPRIETARY AND TRADE SECRET INFORMATION OF TECHNOLOGIC SYSTEMS.
 * ACCESS TO THIS WORK IS RESTRICTED TO (I) TECHNOLOGIC SYSTEMS 
 * EMPLOYEES WHO HAVE A NEED TO KNOW TO PERFORM TASKS WITHIN THE SCOPE
 * OF THEIR ASSIGNMENTS AND (II) ENTITIES OTHER THAN TECHNOLOGIC
 * SYSTEMS WHO HAVE ENTERED INTO APPROPRIATE LICENSE AGREEMENTS.  NO
 * PART OF THIS WORK MAY BE USED, PRACTICED, PERFORMED, COPIED, 
 * DISTRIBUTED, REVISED, MODIFIED, TRANSLATED, ABRIDGED, CONDENSED, 
 * EXPANDED, COLLECTED, COMPILED, LINKED, RECAST, TRANSFORMED, ADAPTED
 * IN ANY FORM OR BY ANY MEANS, MANUAL, MECHANICAL, CHEMICAL, 
 * ELECTRICAL, ELECTRONIC, OPTICAL, BIOLOGICAL, OR OTHERWISE WITHOUT
 * THE PRIOR WRITTEN PERMISSION AND CONSENT OF TECHNOLOGIC SYSTEMS.
 * ANY USE OR EXPLOITATION OF THIS WORK WITHOUT THE PRIOR WRITTEN
 * CONSENT OF TECHNOLOGIC SYSTEMS COULD SUBJECT THE PERPETRATOR TO
 * CRIMINAL AND CIVIL LIABILITY.
 */

/* Syscon memory map:
 * 0x00: reads 0x4700
 * 0x02: Control register:
     bits 3-0:   FPGA revision (RO)
     bits 5-4:   Mode2, Mode1
     bits 7-6:   Scratch register
     bit 8:      Enable CAN (override DIO 15,16)
     bit 9:      Enable 2nd CAN (override DIO 10,11)
     bit 10:     Enable SPI (override DIO 17-20)
     bit 11:     Enable 12.5MHz base board clock (override DIO 3)
     bit 12:     Enable UART0 TXEN (override DIO 12)
     bit 13:     Enable UART4 TXEN (override DIO 14)
     bit 14:     Enable touchscreen (override DIO 30-35)
     bit 15:     Reset switch enable
 * 0x04: external bus config register
 * 0x06: watchdog feed register
 * 0x08: free running 1MHz counter LSB
 * 0x0a: free running 1MHz counter MSB
 * 0x0c: RNG LSB
 * 0x0e: RNG MSB
 * 0x10: DIO group 1 DIO[14:0] output data (bits 14-0)
 * 0x12: DIO group 2 DIO[26:22] DIO[20:15] output data (bits 10-0) 11: Green LED. 12: Red LED.
 * 0x14: DIO group 3 DIO[42:27] output data (bits 15-0)
 * 0x16: DIO group 4 DIO[59:48] output data (bits 11-0)
     bit 12:     Enable UART3 TXEN (override DIO 13)
     bit 13:     Enable UART5 TXEN (override DIO 7)
     bit 14:     Enable UART1 TXEN (override DIO 8)
     bit 15:     Enable UART2 TXEN (override DIO 10)
 * 0x18: DIO group 1 DIO[14:0] data direction (bits 14-0)
 * 0x1a: DIO group 2 DIO[26:22] DIO[20:15] data direction (bits 10-0)
 * 0x1c: DIO group 3 DIO[42:27] data direction (bits 15-0)
 * 0x1e: DIO group 4 DIO[59:48] data direction (bits 11-0)
 * 0x20: DIO group 1 DIO[14:0] input data (bits 14-0)
 * 0x22: DIO group 2 DIO[26:22] DIO[20:15] input data (bits 10-0)
 * 0x24: DIO group 3 DIO[42:27] input data (bits 15-0)
 * 0x26: DIO group 4 DIO[59:48] input data (bits 11-0)
 * 0x28: TAG memory access
 * 0x2a: Custom load ID register (reads 0 on standard load)
 * 0x2c: IRQ register (smc_irq line indicates one of the following is active)
 *   bit 0: XUART IRQ
 *   bit 1: CAN IRQ
 *   bit 2: CAN2 IRQ
 *   bit 3: PC/104 IRQ 5
 *   bit 4: PC/104 IRQ 6
 *   bit 5: PC/104 IRQ 7
 *   bits 6-15: reserved
 * 0x2e: IRQ mask register.  Setting a bit in this register disables the IRQ
 *       for the corresponding bit in the IRQ register.
 */
 
/* DIO pin numbering conventions:
 * DIO 0-8:    CN1 odd pins 93 to 77
 * DIO 9-14:   CN1 odd pins 73 to 63
 * DIO 15-16:  CN2 pins 97 and 99
 * DIO 17-20:  CN2 odd pins 65 to 71 (SPI)
 * DIO 22-26:  CN1 pins 97, 99, 100, 98, 96 (bus control signals)
 * DIO 27-34:  CN1 even pins 78 to 64 (AD bus 8 to 15)
 * DIO 35-42:  CN1 even pins 94 to 80 (AD bus 0 to 7)
 * DIO 48-59:  CN2 even pins 78 to 100 (UARTS)
 *
 * A total of 54 FPGA DIO pins are available, divided into 4 groups.
 * group 1: 0-14
 * group 2: 15-20, 22-26
 * group 3: 27-42
 * group 4: 48-59
 * Each group has an output data register, a data direction register, 
 * and a pin data register.  The PDR reports the actual status of the pin.
 * The ODR is ignored if the DDR is 0.
 */

/* Notes:
 * - 2nd CAN port will not be implemented unless a bigger FPGA is used.
 * - DIO DDR/ODR registers have no effect if other functionality is enabled
 *   for the pin in question.  Muxing is done in top module.
 */
 
/*
 *  0 - Initial revision
 *  1 - Enabled XUARTs
 *  2 - ADC override of GPIO
 *		- Syscon bit 11, control 12.5MHz baseboard clock on GPIO3
 *		- Temporarily disabled MUXBUS for timing constraints
 *		- 4 TX_EN lines, 2 edge counters
 *  3 - Corrected SMC bridge, bus cycles are working 100% for correct bus width
 *    - Modified reset logic 
 *  4 - CAN always returns ACK even when disabled
 *  5 - DIO pin muxing refactored and working
 *		- Optional TX_EN brought out for all 6 XUARTs as advertised
 *		- Changed behavior of SMC_IRQ so it honors mask register
 *    - Added CAN resync fixes
 */

module syscon(
  wb_clk_i,
  wb_rst_i,

  wb_cyc_i,
  wb_stb_i,
  wb_we_i,
  wb_adr_i,
  wb_dat_i,
  wb_sel_i,
  wb_dat_o,
  wb_ack_o,

  dio_i,
  dio_oe_o,
  dio_o,
  usec_i,
  mode1_i,
  mode2_i,

  bus_config_o,
  en_can_o,
  en_can2_o,
  en_spi_o,
  en_bbclk_o,
  en_txen_o,
  en_touch_o,
  red_led_o,
  green_led_o,
  reboot_o,
  irqs_i,
  irq_o

);

input wb_clk_i, wb_rst_i, wb_cyc_i, wb_stb_i, wb_we_i;
input [31:0] wb_adr_i;
input [15:0] wb_dat_i;
input [1:0] wb_sel_i;
output [15:0] wb_dat_o;
output wb_ack_o;

input [59:0] dio_i;
output [59:0] dio_oe_o;
output [59:0] dio_o;

input [31:0] usec_i;
input mode1_i;
input mode2_i;

output [15:0] bus_config_o;
output en_can_o;
output en_can2_o;
output en_spi_o;
output en_bbclk_o;
output [5:0] en_txen_o;
output en_touch_o;
output red_led_o;
output green_led_o;
output reboot_o;
input [15:0] irqs_i;
output irq_o;

localparam [15:0] model = 16'h4700;
localparam [3:0] revision = 4'h5;
localparam [15:0] custom = 16'h0000;

reg tag_csn, tag_clk, tag_si;
wire tag_so;
reg tag_so_q;
tagmem tagmemcore(
  .CLK(tag_clk),
  .SI(tag_si),
  .SO(tag_so),
  .CS(tag_csn)
);

always @(posedge wb_clk_i or posedge wb_rst_i) begin
  if (wb_rst_i) tag_so_q <= 1'b0;
  else tag_so_q <= tag_so;
end

wire random_clk;
OSCE #(.NOM_FREQ("3.1")) rndosc (random_clk);
reg [31:0] random;
reg [15:0] rndcnt;
reg [1:0] wdogctl;
reg [9:0] wdogcnt;
reg feed_en, reboot;
wire sed_err;
wire feed_bsy, feed_rdreq;
wire [15:0] wdog_dat;
reg resetsw_en;
assign reboot_o = reboot | (resetsw_en & !dio_i[9]);
always @(posedge random_clk or posedge wb_rst_i) begin
  if (wb_rst_i) begin
    rndcnt <= 16'd0;
    random <= 32'd0;
    wdogctl <= 2'd1;   
    wdogcnt <= 10'd0;
  end else begin
    rndcnt <= rndcnt + 1'b1;
    if (rndcnt == 16'd0) begin
      random[31:0] <= {random[30:0], wb_clk_i};
      wdogcnt <= wdogcnt + 1'b1;
    end
    if (feed_rdreq) begin
      wdogctl <= wdog_dat;
      wdogcnt <= 10'd0;
    end
  end
end
always @(*) begin
  case (wdogctl)
  2'd0: reboot = wdogcnt[4]; /* approx .338 seconds */
  2'd1: reboot = wdogcnt[7]; /* approx 2.706 seconds */
  2'd2: reboot = wdogcnt[9]; /* approx 10.824 seconds */
  2'd3: reboot = 1'b0;
  endcase
end

resync_fifo watchdogfifo(
  .rst_i(wb_rst_i),
  .wrclk_i(wb_clk_i),
  .wrreq_i(feed_en),
  .data_i(wb_dat_i),
  .full_wrclk_o(feed_bsy),
  .rdclk_i(random_clk),
  .rdreq_i(feed_rdreq),
  .q_o(wdog_dat),
  .full_rdclk_o(feed_rdreq)
);

reg [59:0] dio_oe;
assign dio_oe_o = dio_oe;
reg [59:0] dio;
assign dio_o = dio;
reg [1:0] scratch;

reg [15:0] bus_config;
reg en_can, en_can2, en_spi, en_bbclk, en_touch;
reg [5:0] en_txen; 
reg red_led, green_led;
reg [15:0] irq_mask;

assign bus_config_o = bus_config;
assign en_can_o = en_can;
assign en_can2_o = en_can2;
assign en_spi_o = en_spi;
assign en_bbclk_o = en_bbclk;
assign en_txen_o = en_txen;
assign en_touch_o = en_touch;
assign red_led_o = red_led;
assign green_led_o = green_led;

assign irq_o = ((irqs_i & ~irq_mask) != 16'h0000);

// custom resettable falling edge counters for poweroasis:
reg [15:0] count0;
reg [15:0] count1;
reg [3:0] countfifo;
always @(posedge wb_clk_i or posedge wb_rst_i) begin
  if (wb_rst_i) begin
    count0 <= 16'd0;
    count1 <= 16'd0;
    countfifo <= 4'h0;
  end else begin
    countfifo <= {countfifo[1:0], dio_i[6], dio_i[4]};
    if (countfifo[3] & !countfifo[1]) count0 <= count0 + 1'b1;
    if (countfifo[2] & !countfifo[0]) count1 <= count1 + 1'b1;
    if (wb_cyc_i && wb_stb_i && wb_we_i) case (wb_adr_i[5:0])
      6'h30: count0 <= 16'd0;
      6'h32: count1 <= 16'd0;
    endcase
  end
end

always @(posedge wb_clk_i or posedge wb_rst_i) begin
  if (wb_rst_i) begin
    en_can <= 1'b0;
    en_can2 <= 1'b0;
    en_spi <= 1'b0;
    tag_clk <= 1'b0;
    tag_si <= 1'b0;
    tag_csn <= 1'b0;
    en_bbclk <= 1'b0;
    en_txen <= 6'd0;
    en_touch <= 1'b0;
    dio_oe <= 60'd0;
    dio <= 60'd0;
    bus_config <= 16'h0000;
    scratch <= 2'b00;
    red_led <= 1'b1;
    green_led <= 1'b1;
    resetsw_en <= 1'b0;
    irq_mask <= 16'h0000;
  end else begin
    if (wb_cyc_i && wb_stb_i && wb_we_i) case (wb_adr_i[5:0])
      6'h2: {resetsw_en, en_touch, en_txen[4], en_txen[0], en_bbclk, en_spi,
             en_can2, en_can, scratch} <= wb_dat_i[15:6];
      6'h4: bus_config <= wb_dat_i;
      6'h10: dio[14:0] <= wb_dat_i[14:0];
      6'h12: {red_led, green_led, dio[26:22], dio[20:15]} <= wb_dat_i[12:0];
      6'h14: dio[42:27] <= wb_dat_i;
      6'h16: {en_txen[2:1], en_txen[5], en_txen[3], dio[59:48]} <= wb_dat_i;
      6'h18: dio_oe[14:0] <= wb_dat_i[14:0];
      6'h1a: {dio_oe[26:22], dio_oe[20:15]} <= wb_dat_i[10:0];
      6'h1c: dio_oe[42:27] <= wb_dat_i;
      6'h1e: dio_oe[59:48] <= wb_dat_i[11:0];
      6'h28: {tag_clk, tag_si, tag_csn} <= wb_dat_i[3:1];
      6'h2e: irq_mask <= wb_dat_i;
    endcase
  end
end

reg [15:0] wb_dat;
assign wb_dat_o = wb_dat;
reg wb_ack;
assign wb_ack_o = wb_ack;
always @(*) begin
  wb_ack = wb_cyc_i && wb_stb_i;
  wb_dat = 16'hxxxx;

  case (wb_adr_i[5:0]) 
    6'h0: wb_dat = model[15:0];
    6'h2: wb_dat = {resetsw_en, en_touch, en_txen[4], en_txen[0], en_bbclk,
          en_spi, en_can2, en_can, scratch, mode2_i, mode1_i, revision};
    6'h4: wb_dat = bus_config;
    6'h6: begin
      feed_en = wb_cyc_i & wb_stb_i & wb_we_i & !feed_bsy;
      wb_ack = wb_cyc_i & wb_stb_i & ((wb_we_i & !feed_bsy ) | !wb_we_i);
    end
    6'h8: wb_dat = usec_i[15:0];
    6'ha: wb_dat = usec_i[31:16];
    6'hc: wb_dat = random[15:0];
    6'he: wb_dat = random[31:16];
    6'h10: wb_dat = {1'bx, dio[14:0]};
    6'h12: wb_dat = {3'bxxx, red_led, green_led, dio[26:22], dio[20:15]};
    6'h14: wb_dat = dio[42:27];
    6'h16: wb_dat = {en_txen[2:1], en_txen[5], en_txen[3], dio[59:48]};
    6'h18: wb_dat = {1'bx, dio_oe[14:0]};
    6'h1a: wb_dat = {5'bxxxxx, dio_oe[26:22], dio_oe[20:15]};
    6'h1c: wb_dat = dio_oe[42:27];
    6'h1e: wb_dat = {4'bxxxx, dio_oe[59:48]};
    6'h20: wb_dat = {1'bx, dio_i[14:0]};
    6'h22: wb_dat = {5'bxxxxx, dio_i[26:22], dio_i[20:15]};
    6'h24: wb_dat = dio_i[42:27];
    6'h26: wb_dat = {4'bxxxx, dio_i[59:48]};
    6'h28: wb_dat = {8'hxx, 2'b00, wdogctl, tag_clk, tag_si, tag_csn, tag_so_q};
    6'h2a: wb_dat = custom;
    6'h2c: wb_dat = irqs_i & ~irq_mask;
    6'h2e: wb_dat = irq_mask;
    6'h30: wb_dat = count0;
    6'h32: wb_dat = count1;
    //6'h2c: wb_dat = wb_sel_i[1:0] == 2'b11 ? 16'h1616 : 16'h8888; //width test
    //default: wb_dat = 16'hxxxx;
  endcase
end

endmodule

