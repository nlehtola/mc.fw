/* Verilog netlist generated by SCUBA Diamond_1.1_Production (517) */
/* Module Version: 6.1 */
/* C:\lscc\diamond\1.1\ispfpga\bin\nt\scuba.exe -w -lang verilog -synth synplify -bus_exp 7 -bb -arch mg5a00 -type bram -wp 10 -rp 0011 -rdata_width 8 -data_width 8 -num_rows 4096 -resetmode SYNC -cascade -1 -e  */
/* Thu Jan 27 12:08:32 2011 */


`timescale 1 ns / 1 ps
module lattice_ram (WrAddress, RdAddress, Data, WE, RdClock, RdClockEn, 
    Reset, WrClock, WrClockEn, Q);
    input wire [11:0] WrAddress;
    input wire [11:0] RdAddress;
    input wire [7:0] Data;
    input wire WE;
    input wire RdClock;
    input wire RdClockEn;
    input wire Reset;
    input wire WrClock;
    input wire WrClockEn;
    output wire [7:0] Q;

    wire scuba_vhi;
    wire scuba_vlo;

    VHI scuba_vhi_inst (.Z(scuba_vhi));

    // synopsys translate_off
    defparam lattice_ram_0_0_1.CSDECODE_B =  3'b000 ;
    defparam lattice_ram_0_0_1.CSDECODE_A =  3'b000 ;
    defparam lattice_ram_0_0_1.WRITEMODE_B = "NORMAL" ;
    defparam lattice_ram_0_0_1.WRITEMODE_A = "NORMAL" ;
    defparam lattice_ram_0_0_1.GSR = "DISABLED" ;
    defparam lattice_ram_0_0_1.RESETMODE = "SYNC" ;
    defparam lattice_ram_0_0_1.REGMODE_B = "NOREG" ;
    defparam lattice_ram_0_0_1.REGMODE_A = "NOREG" ;
    defparam lattice_ram_0_0_1.DATA_WIDTH_B = 4 ;
    defparam lattice_ram_0_0_1.DATA_WIDTH_A = 4 ;
    // synopsys translate_on
    DP16KB lattice_ram_0_0_1 (.DIA0(Data[0]), .DIA1(Data[1]), .DIA2(Data[2]), 
        .DIA3(Data[3]), .DIA4(scuba_vlo), .DIA5(scuba_vlo), .DIA6(scuba_vlo), 
        .DIA7(scuba_vlo), .DIA8(scuba_vlo), .DIA9(scuba_vlo), .DIA10(scuba_vlo), 
        .DIA11(scuba_vlo), .DIA12(scuba_vlo), .DIA13(scuba_vlo), .DIA14(scuba_vlo), 
        .DIA15(scuba_vlo), .DIA16(scuba_vlo), .DIA17(scuba_vlo), .ADA0(scuba_vlo), 
        .ADA1(scuba_vlo), .ADA2(WrAddress[0]), .ADA3(WrAddress[1]), .ADA4(WrAddress[2]), 
        .ADA5(WrAddress[3]), .ADA6(WrAddress[4]), .ADA7(WrAddress[5]), .ADA8(WrAddress[6]), 
        .ADA9(WrAddress[7]), .ADA10(WrAddress[8]), .ADA11(WrAddress[9]), 
        .ADA12(WrAddress[10]), .ADA13(WrAddress[11]), .CEA(WrClockEn), .CLKA(WrClock), 
        .WEA(WE), .CSA0(scuba_vlo), .CSA1(scuba_vlo), .CSA2(scuba_vlo), 
        .RSTA(Reset), .DIB0(scuba_vlo), .DIB1(scuba_vlo), .DIB2(scuba_vlo), 
        .DIB3(scuba_vlo), .DIB4(scuba_vlo), .DIB5(scuba_vlo), .DIB6(scuba_vlo), 
        .DIB7(scuba_vlo), .DIB8(scuba_vlo), .DIB9(scuba_vlo), .DIB10(scuba_vlo), 
        .DIB11(scuba_vlo), .DIB12(scuba_vlo), .DIB13(scuba_vlo), .DIB14(scuba_vlo), 
        .DIB15(scuba_vlo), .DIB16(scuba_vlo), .DIB17(scuba_vlo), .ADB0(scuba_vlo), 
        .ADB1(scuba_vlo), .ADB2(RdAddress[0]), .ADB3(RdAddress[1]), .ADB4(RdAddress[2]), 
        .ADB5(RdAddress[3]), .ADB6(RdAddress[4]), .ADB7(RdAddress[5]), .ADB8(RdAddress[6]), 
        .ADB9(RdAddress[7]), .ADB10(RdAddress[8]), .ADB11(RdAddress[9]), 
        .ADB12(RdAddress[10]), .ADB13(RdAddress[11]), .CEB(RdClockEn), .CLKB(RdClock), 
        .WEB(scuba_vlo), .CSB0(scuba_vlo), .CSB1(scuba_vlo), .CSB2(scuba_vlo), 
        .RSTB(Reset), .DOA0(), .DOA1(), .DOA2(), .DOA3(), .DOA4(), .DOA5(), 
        .DOA6(), .DOA7(), .DOA8(), .DOA9(), .DOA10(), .DOA11(), .DOA12(), 
        .DOA13(), .DOA14(), .DOA15(), .DOA16(), .DOA17(), .DOB0(Q[0]), .DOB1(Q[1]), 
        .DOB2(Q[2]), .DOB3(Q[3]), .DOB4(), .DOB5(), .DOB6(), .DOB7(), .DOB8(), 
        .DOB9(), .DOB10(), .DOB11(), .DOB12(), .DOB13(), .DOB14(), .DOB15(), 
        .DOB16(), .DOB17())
             /* synthesis MEM_LPC_FILE="lattice_ram.lpc" */
             /* synthesis MEM_INIT_FILE="" */
             /* synthesis CSDECODE_B="0b000" */
             /* synthesis CSDECODE_A="0b000" */
             /* synthesis WRITEMODE_B="NORMAL" */
             /* synthesis WRITEMODE_A="NORMAL" */
             /* synthesis GSR="DISABLED" */
             /* synthesis RESETMODE="SYNC" */
             /* synthesis REGMODE_B="NOREG" */
             /* synthesis REGMODE_A="NOREG" */
             /* synthesis DATA_WIDTH_B="4" */
             /* synthesis DATA_WIDTH_A="4" */;

    VLO scuba_vlo_inst (.Z(scuba_vlo));

    // synopsys translate_off
    defparam lattice_ram_0_1_0.CSDECODE_B =  3'b000 ;
    defparam lattice_ram_0_1_0.CSDECODE_A =  3'b000 ;
    defparam lattice_ram_0_1_0.WRITEMODE_B = "NORMAL" ;
    defparam lattice_ram_0_1_0.WRITEMODE_A = "NORMAL" ;
    defparam lattice_ram_0_1_0.GSR = "DISABLED" ;
    defparam lattice_ram_0_1_0.RESETMODE = "SYNC" ;
    defparam lattice_ram_0_1_0.REGMODE_B = "NOREG" ;
    defparam lattice_ram_0_1_0.REGMODE_A = "NOREG" ;
    defparam lattice_ram_0_1_0.DATA_WIDTH_B = 4 ;
    defparam lattice_ram_0_1_0.DATA_WIDTH_A = 4 ;
    // synopsys translate_on
    DP16KB lattice_ram_0_1_0 (.DIA0(Data[4]), .DIA1(Data[5]), .DIA2(Data[6]), 
        .DIA3(Data[7]), .DIA4(scuba_vlo), .DIA5(scuba_vlo), .DIA6(scuba_vlo), 
        .DIA7(scuba_vlo), .DIA8(scuba_vlo), .DIA9(scuba_vlo), .DIA10(scuba_vlo), 
        .DIA11(scuba_vlo), .DIA12(scuba_vlo), .DIA13(scuba_vlo), .DIA14(scuba_vlo), 
        .DIA15(scuba_vlo), .DIA16(scuba_vlo), .DIA17(scuba_vlo), .ADA0(scuba_vlo), 
        .ADA1(scuba_vlo), .ADA2(WrAddress[0]), .ADA3(WrAddress[1]), .ADA4(WrAddress[2]), 
        .ADA5(WrAddress[3]), .ADA6(WrAddress[4]), .ADA7(WrAddress[5]), .ADA8(WrAddress[6]), 
        .ADA9(WrAddress[7]), .ADA10(WrAddress[8]), .ADA11(WrAddress[9]), 
        .ADA12(WrAddress[10]), .ADA13(WrAddress[11]), .CEA(WrClockEn), .CLKA(WrClock), 
        .WEA(WE), .CSA0(scuba_vlo), .CSA1(scuba_vlo), .CSA2(scuba_vlo), 
        .RSTA(Reset), .DIB0(scuba_vlo), .DIB1(scuba_vlo), .DIB2(scuba_vlo), 
        .DIB3(scuba_vlo), .DIB4(scuba_vlo), .DIB5(scuba_vlo), .DIB6(scuba_vlo), 
        .DIB7(scuba_vlo), .DIB8(scuba_vlo), .DIB9(scuba_vlo), .DIB10(scuba_vlo), 
        .DIB11(scuba_vlo), .DIB12(scuba_vlo), .DIB13(scuba_vlo), .DIB14(scuba_vlo), 
        .DIB15(scuba_vlo), .DIB16(scuba_vlo), .DIB17(scuba_vlo), .ADB0(scuba_vlo), 
        .ADB1(scuba_vlo), .ADB2(RdAddress[0]), .ADB3(RdAddress[1]), .ADB4(RdAddress[2]), 
        .ADB5(RdAddress[3]), .ADB6(RdAddress[4]), .ADB7(RdAddress[5]), .ADB8(RdAddress[6]), 
        .ADB9(RdAddress[7]), .ADB10(RdAddress[8]), .ADB11(RdAddress[9]), 
        .ADB12(RdAddress[10]), .ADB13(RdAddress[11]), .CEB(RdClockEn), .CLKB(RdClock), 
        .WEB(scuba_vlo), .CSB0(scuba_vlo), .CSB1(scuba_vlo), .CSB2(scuba_vlo), 
        .RSTB(Reset), .DOA0(), .DOA1(), .DOA2(), .DOA3(), .DOA4(), .DOA5(), 
        .DOA6(), .DOA7(), .DOA8(), .DOA9(), .DOA10(), .DOA11(), .DOA12(), 
        .DOA13(), .DOA14(), .DOA15(), .DOA16(), .DOA17(), .DOB0(Q[4]), .DOB1(Q[5]), 
        .DOB2(Q[6]), .DOB3(Q[7]), .DOB4(), .DOB5(), .DOB6(), .DOB7(), .DOB8(), 
        .DOB9(), .DOB10(), .DOB11(), .DOB12(), .DOB13(), .DOB14(), .DOB15(), 
        .DOB16(), .DOB17())
             /* synthesis MEM_LPC_FILE="lattice_ram.lpc" */
             /* synthesis MEM_INIT_FILE="" */
             /* synthesis CSDECODE_B="0b000" */
             /* synthesis CSDECODE_A="0b000" */
             /* synthesis WRITEMODE_B="NORMAL" */
             /* synthesis WRITEMODE_A="NORMAL" */
             /* synthesis GSR="DISABLED" */
             /* synthesis RESETMODE="SYNC" */
             /* synthesis REGMODE_B="NOREG" */
             /* synthesis REGMODE_A="NOREG" */
             /* synthesis DATA_WIDTH_B="4" */
             /* synthesis DATA_WIDTH_A="4" */;



    // exemplar begin
    // exemplar attribute lattice_ram_0_0_1 MEM_LPC_FILE lattice_ram.lpc
    // exemplar attribute lattice_ram_0_0_1 MEM_INIT_FILE 
    // exemplar attribute lattice_ram_0_0_1 CSDECODE_B 0b000
    // exemplar attribute lattice_ram_0_0_1 CSDECODE_A 0b000
    // exemplar attribute lattice_ram_0_0_1 WRITEMODE_B NORMAL
    // exemplar attribute lattice_ram_0_0_1 WRITEMODE_A NORMAL
    // exemplar attribute lattice_ram_0_0_1 GSR DISABLED
    // exemplar attribute lattice_ram_0_0_1 RESETMODE SYNC
    // exemplar attribute lattice_ram_0_0_1 REGMODE_B NOREG
    // exemplar attribute lattice_ram_0_0_1 REGMODE_A NOREG
    // exemplar attribute lattice_ram_0_0_1 DATA_WIDTH_B 4
    // exemplar attribute lattice_ram_0_0_1 DATA_WIDTH_A 4
    // exemplar attribute lattice_ram_0_1_0 MEM_LPC_FILE lattice_ram.lpc
    // exemplar attribute lattice_ram_0_1_0 MEM_INIT_FILE 
    // exemplar attribute lattice_ram_0_1_0 CSDECODE_B 0b000
    // exemplar attribute lattice_ram_0_1_0 CSDECODE_A 0b000
    // exemplar attribute lattice_ram_0_1_0 WRITEMODE_B NORMAL
    // exemplar attribute lattice_ram_0_1_0 WRITEMODE_A NORMAL
    // exemplar attribute lattice_ram_0_1_0 GSR DISABLED
    // exemplar attribute lattice_ram_0_1_0 RESETMODE SYNC
    // exemplar attribute lattice_ram_0_1_0 REGMODE_B NOREG
    // exemplar attribute lattice_ram_0_1_0 REGMODE_A NOREG
    // exemplar attribute lattice_ram_0_1_0 DATA_WIDTH_B 4
    // exemplar attribute lattice_ram_0_1_0 DATA_WIDTH_A 4
    // exemplar end

endmodule
