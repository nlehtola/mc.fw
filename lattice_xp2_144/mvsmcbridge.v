/* Copyright 2007-2008, Unpublished Work of Technologic Systems
 * All Rights Reserved.
 *
 * THIS WORK IS AN UNPUBLISHED WORK AND CONTAINS CONFIDENTIAL,
 * PROPRIETARY AND TRADE SECRET INFORMATION OF TECHNOLOGIC SYSTEMS.
 * ACCESS TO THIS WORK IS RESTRICTED TO (I) TECHNOLOGIC SYSTEMS 
 * EMPLOYEES WHO HAVE A NEED TO KNOW TO PERFORM TASKS WITHIN THE SCOPE
 * OF THEIR ASSIGNMENTS AND (II) ENTITIES OTHER THAN TECHNOLOGIC
 * SYSTEMS WHO HAVE ENTERED INTO APPROPRIATE LICENSE AGREEMENTS.  NO
 * PART OF THIS WORK MAY BE USED, PRACTICED, PERFORMED, COPIED, 
 * DISTRIBUTED, REVISED, MODIFIED, TRANSLATED, ABRIDGED, CONDENSED, 
 * EXPANDED, COLLECTED, COMPILED, LINKED, RECAST, TRANSFORMED, ADAPTED
 * IN ANY FORM OR BY ANY MEANS, MANUAL, MECHANICAL, CHEMICAL, 
 * ELECTRICAL, ELECTRONIC, OPTICAL, BIOLOGICAL, OR OTHERWISE WITHOUT
 * THE PRIOR WRITTEN PERMISSION AND CONSENT OF TECHNOLOGIC SYSTEMS.
 * ANY USE OR EXPLOITATION OF THIS WORK WITHOUT THE PRIOR WRITTEN
 * CONSENT OF TECHNOLOGIC SYSTEMS COULD SUBJECT THE PERPETRATOR TO
 * CRIMINAL AND CIVIL LIABILITY.
 */

/* Marvell SMC bridge has 16 muxed A/D lines, no separate address lines.
 * Thus this version of our SMC bridge module will just add a latch for
 * the address.
 */

/* CS0 is a 16 bit bus although byte lane selects should work.
 * CS1 is a dedicated 8 bit bus for 8 bit cores.
 */

module mvsmcbridge(
  wb_clk_i,
  wb_rst_i,
  wbm_cyc_o,
  wbm_stb_o,
  wbm_dat_o,
  wbm_adr_o,
  wbm_we_o,
  wbm_dat_i,
  wbm_ack_i,
  wbm_err_o,
  wbm_sel_o,
  
  //smc_add_i,
  smc_dat_i,
  smc_dat_o,
  smc_dat_oe_o,
  smc_rdn_i,
  smc_wrn_i,
  smc_cs0n_i,
  smc_cs1n_i,
  smc_cs2n_i,
  smc_cs3n_i,
  smc_cs6n_i,
  smc_cs7n_i,
  smc_be1n_i,
  smc_be2n_i,
  smc_advn_i,
  smc_waitn_o,

  /* Board specific */
  ts4700_decodes_o
);

input wb_clk_i;
input wb_rst_i;
output wbm_cyc_o;
output wbm_stb_o;
output [15:0] wbm_dat_o;
output [31:0] wbm_adr_o;
output wbm_we_o, wbm_err_o;
input [15:0] wbm_dat_i;
input wbm_ack_i;
output [1:0] wbm_sel_o;
  
//input [25:0] smc_add_i;
input [15:0] smc_dat_i;
output [15:0] smc_dat_o;
output smc_dat_oe_o;
input smc_rdn_i /* synthesis syn_maxfan=1000 syn_useioff=1 */;
input smc_wrn_i /* synthesis syn_maxfan=1000 syn_useioff=1 */;
input smc_cs0n_i;
input smc_cs1n_i;
input smc_cs2n_i;
input smc_cs3n_i;
input smc_cs6n_i;
input smc_cs7n_i;
input smc_be1n_i;
input smc_be2n_i;
input smc_advn_i;
output smc_waitn_o;

output [11:0] ts4700_decodes_o;

reg [5:0] csadr;
reg anycs;
always @(smc_cs1n_i or smc_cs2n_i or smc_cs3n_i or smc_cs6n_i 
  or smc_cs7n_i or smc_cs0n_i) begin
  anycs = 1'b1;
  if (!smc_cs0n_i) csadr = {4'h0, 2'b00};
  else if (!smc_cs1n_i) csadr = {4'h1, 2'b00};
  else if (!smc_cs2n_i) csadr = {4'h2, 2'b00};
  else if (!smc_cs3n_i) csadr = {4'h3, 2'b00};
  else if (!smc_cs6n_i) csadr = {4'h6, 2'b00};
  else if (!smc_cs7n_i) csadr = {4'h7, 2'b00};
  else begin
    csadr = 6'bxxxxxx;
    anycs = 1'b0;
  end
end

reg [2:0] state /* synthesis syn_encoding="original" */;
reg [2:0] pause;
reg [1:0] wbm_sel;
assign wbm_sel_o = wbm_sel;
reg smc_rd, smc_wr;
reg smc_wait, wbm_we, wbm_cyc, wbm_stb, wbm_err, smc_dat_oe;
reg [15:0] smc_dat;
assign wbm_err_o = wbm_err;
assign smc_dat_o = smc_dat;
assign smc_dat_oe_o = smc_dat_oe;
assign smc_waitn_o = !(anycs && smc_wait);
reg [15:0] wbm_dat;
reg [15:0] smc_add /* synthesis syn_maxfan=4 */;
/* 16 bit bus uses 16 bit addressing, 8 bit bus uses 8 bit: */
reg [15:0] wbm_adr /* synthesis syn_maxfan=20 */;
assign wbm_adr_o = wbm_adr;

always @(*) wbm_adr = smc_cs1n_i ? {9'd0, csadr, smc_add, 1'b0} : 
  {9'd0, csadr, 1'b0, smc_add[15:1], 1'b0} ;
assign wbm_dat_o = wbm_dat;
assign wbm_we_o = wbm_we;
assign wbm_cyc_o = wbm_cyc;
assign wbm_stb_o = wbm_stb;
reg anycs_q;
always @(posedge wb_clk_i or posedge wb_rst_i) begin
  if (wb_rst_i) begin
    wbm_sel <= 2'b00;
    wbm_err <= 1'b0;
    smc_rd <= 1'b0;
    smc_wr <= 1'b0;
    smc_dat_oe <= 1'b0;
    wbm_we <= 1'b0;
    wbm_dat <= 32'd0;
    wbm_cyc <= 1'b0;
    wbm_stb <= 1'b0;
    smc_dat <= 16'h0000;
    state <= 3'd0;
    anycs_q <= 1'b0;
    smc_add <= 16'd0;
    //smc_wait <= 1'b0;
    //pause <= 3'd0;
  end else begin
    smc_rd <= !smc_rdn_i;
    smc_wr <= !smc_wrn_i;
    anycs_q <= anycs;
    wbm_err <= 1'b0;
    if (!smc_advn_i) smc_add <= smc_dat_i; // marvell address latch
    case (state)
      3'd0: begin // waiting for address phase
        if (!smc_advn_i) begin
          pause <= 3'd3; // XXX need the right value here
          state <= 3'd1;
        end
      end
      3'd1: begin // waiting until WAIT can be asserted
        pause <= pause - 1'b1;
        if (pause == 3'd0) begin
          state <= 3'd2;
          smc_wait <= 1'b1;
          if (!anycs_q) begin
            state <= 3'd0;
            smc_wait <= 1'b0;
          end
        end
      end
      3'd2: begin // waiting for RD or WR signal
        wbm_dat <= smc_dat_i;
        if (!smc_cs1n_i) wbm_dat[15:8] <= smc_dat_i[7:0];
        if (smc_cs1n_i) wbm_sel <= {!smc_be2n_i, !smc_be1n_i};
        else wbm_sel <= smc_add[0] ? 2'b10 : 2'b01;
        if (anycs_q && smc_rd) begin
          wbm_cyc <= 1'b1;
          wbm_stb <= 1'b1;
          wbm_we <= 1'b0;
          state <= 3'd3;
          smc_dat_oe <= 1'b1;
        end else if (anycs_q && smc_wr) begin
          wbm_cyc <= 1'b1;
          wbm_stb <= 1'b1;
          wbm_we <= 1'b1;
          state <= 3'd3;
          smc_dat_oe <= 1'b0;
        end
      end
      3'd3: begin // waiting for wishbone ACK
        smc_dat <= wbm_dat_i;
        if (wbm_ack_i) begin
          wbm_stb <= 1'b0;
          wbm_cyc <= 1'b0;
          smc_wait <= 1'b0;
          state <= 3'd4;
        end
      end
      3'd4: begin // 1 tick for data hold
        state <= 3'd5;
      end
      3'd5: begin // waiting for end of cycle
        smc_dat_oe <= 1'b0;
        if (!anycs_q) state <= 3'd0;
      end
      default: state <= 3'd0;
    endcase
  end
end

/*

64 KB FPGA address space:
start   stop    usage
-----   ----    -----
0x8000  0xFFFF  external bus access
0x5400          XUART IO registers
0x5000          touchscreen core
0x4D00  0x4DFF  2nd CAN core
0x4C00  0x4CFF  CAN core
0x4800          SPI core
0x4400          ADC core
0x4000          syscon registers
0x0000  0x3FFF  16KB blockram access

*/

/* The SMC bus has 16 address lines that address 16 bit quantities, so it 
 * is actually 128KB of space.  But I am only use 64KB at this time.
 */

// wire [15:0] add = {smc_add[14:0], 1'b0}; XXX WRONG
wire [15:0] add = wbm_adr_o[15:0];

reg [11:0] ts4700_decodes;
assign ts4700_decodes_o = ts4700_decodes;
always @(posedge wb_clk_i or posedge wb_rst_i) begin
  if (wb_rst_i) begin
    ts4700_decodes <= 12'h800; 
  end else if (state == 2'd0 || state == 2'd2) begin
    if (add < 16'h4000) begin
      ts4700_decodes <= 12'h001; // blockram

    end else if (add >= 16'h8000) begin
      ts4700_decodes <= 12'h002; // external bus

    end else if (add >= 16'h5400) begin
      ts4700_decodes <= 12'h004; // XUART IO registers

    end else if (add >= 16'h5000) begin
      ts4700_decodes <= 12'h008; // touchscreen

    end else if (add >= 16'h4D00) begin
      ts4700_decodes <= 12'h100; // CAN2 core

    end else if (add >= 16'h4C00) begin
      ts4700_decodes <= 12'h010; // CAN core

    end else if (add >= 16'h4800) begin
      ts4700_decodes <= 12'h020; // SPI core

    end else if (add >= 16'h4400) begin
      ts4700_decodes <= 12'h040; // ADC core

    end else begin
      ts4700_decodes <= 12'h080; // Syscon
    end
  end
end


endmodule
