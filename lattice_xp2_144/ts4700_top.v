/* Copyright 2007-2010, Unpublished Work of Technologic Systems
 * All Rights Reserved.
 *
 * THIS WORK IS AN UNPUBLISHED WORK AND CONTAINS CONFIDENTIAL,
 * PROPRIETARY AND TRADE SECRET INFORMATION OF TECHNOLOGIC SYSTEMS.
 * ACCESS TO THIS WORK IS RESTRICTED TO (I) TECHNOLOGIC SYSTEMS 
 * EMPLOYEES WHO HAVE A NEED TO KNOW TO PERFORM TASKS WITHIN THE SCOPE
 * OF THEIR ASSIGNMENTS AND (II) ENTITIES OTHER THAN TECHNOLOGIC
 * SYSTEMS WHO HAVE ENTERED INTO APPROPRIATE LICENSE AGREEMENTS.  NO
 * PART OF THIS WORK MAY BE USED, PRACTICED, PERFORMED, COPIED, 
 * DISTRIBUTED, REVISED, MODIFIED, TRANSLATED, ABRIDGED, CONDENSED, 
 * EXPANDED, COLLECTED, COMPILED, LINKED, RECAST, TRANSFORMED, ADAPTED
 * IN ANY FORM OR BY ANY MEANS, MANUAL, MECHANICAL, CHEMICAL, 
 * ELECTRICAL, ELECTRONIC, OPTICAL, BIOLOGICAL, OR OTHERWISE WITHOUT
 * THE PRIOR WRITTEN PERMISSION AND CONSENT OF TECHNOLOGIC SYSTEMS.
 * ANY USE OR EXPLOITATION OF THIS WORK WITHOUT THE PRIOR WRITTEN
 * CONSENT OF TECHNOLOGIC SYSTEMS COULD SUBJECT THE PERPETRATOR TO
 * CRIMINAL AND CIVIL LIABILITY.
 */
/* Modules:
 * Syscon -- DIO, LED, etc
 * CAN
 * XUARTs
 * touchscreen controller
 * external bus interface
 * SPI core
 * ADC core
 */
module ts4700_top(

  smc_d_pad,
  smc_clk_pad,
  smc_cs0n_pad,
  smc_cs1n_pad,
  smc_llan_pad,
  smc_luan_pad,
  smc_be1n_pad,
  smc_be2n_pad,
  smc_wen_pad,
  smc_rdn_pad,
  smc_irq_pad,
  nand_rdn_pad,
  nand_wrn_pad,
  smc_rdy_pad,

  fpga_25mhz_pad,
  //off_bd_resetn_pad, // no longer connected on rev A
  reboot_pad,
  cpu_wakeup_pad,
  en_usb_5v_pad,
  en_lcd_3v_pad,
  //cpu_jtag_resetn_pad, // no longer connected on rev A
  en_avdd_otgn_pad,
  sd_powern_pad,
  un_reset_pad,
  fpga_resetn_pad, // new global reset
  clk_512hz_pad,

  mux_ad_pad,
  bus_alen_pad,
  bus_dir_pad,
  bus_csn_pad,
  bus_bhen_pad,
  bus_rdy_pad,

  uart1_txd_pad,
  uart1_rxd_pad,
  uart2_txd_pad,
  uart2_rxd_pad,
  uart3_txd_pad,
  uart3_rxd_pad,
  uart4_txd_pad,
  uart4_rxd_pad,
  uart5_txd_pad,
  uart5_rxd_pad,
  uart6_txd_pad,
  uart6_rxd_pad,

  debug_txd_pad,
  debug_rxd_pad,
  spi_clk_pad,
  spi_miso_pad,
  spi_mosi_pad,
  spi_frm_pad,

  dio_pad,
  red_led_pad,
  green_led_pad,
  mfp_18_pad,
  mfp_34_pad,
  mfp_51_pad,
  //tp_141_pad
  //nc_58_pad,
  //nc_66_pad,
  //nc_10_pad
 
);

inout [15:0] smc_d_pad /* synthesis syn_useioff=0 */;
input smc_clk_pad;
input smc_cs0n_pad;
input smc_cs1n_pad;
input smc_llan_pad;
input smc_luan_pad;
input smc_be1n_pad;
input smc_be2n_pad;
input smc_wen_pad;
input smc_rdn_pad;
output smc_irq_pad;
input nand_rdn_pad;
input nand_wrn_pad;
inout smc_rdy_pad;

input fpga_25mhz_pad;
//input off_bd_resetn_pad;
output reboot_pad;
output cpu_wakeup_pad;
output en_usb_5v_pad;
output en_lcd_3v_pad;
//output cpu_jtag_resetn_pad;
output en_avdd_otgn_pad;
output sd_powern_pad;
output un_reset_pad;
input fpga_resetn_pad;
input clk_512hz_pad;

inout [15:0] mux_ad_pad;
inout bus_alen_pad;
inout bus_dir_pad;
inout bus_csn_pad;
inout bus_bhen_pad;
inout bus_rdy_pad;

// Starting with Rev A schematic, these uarts are numbered 0-5.
// Changing the top level pad names is probably not worth the effort.
inout uart1_txd_pad;
inout uart1_rxd_pad;
inout uart2_txd_pad;
inout uart2_rxd_pad;
inout uart3_txd_pad;
inout uart3_rxd_pad;
inout uart4_txd_pad;
inout uart4_rxd_pad;
inout uart5_txd_pad;
inout uart5_rxd_pad;
inout uart6_txd_pad;
inout uart6_rxd_pad;

input debug_txd_pad;
input debug_rxd_pad;
inout spi_clk_pad;
inout spi_miso_pad;
inout spi_mosi_pad;
inout spi_frm_pad;

inout [16:0] dio_pad;
output red_led_pad;
output green_led_pad;
inout mfp_18_pad;
inout mfp_34_pad;
inout mfp_51_pad;

//output tp_141_pad;
//input nc_58_pad;
//input nc_66_pad;
//input nc_10_pad;

/*XXX: Do not modify, redefined by ts4700-8k project!*/
parameter FPGA_8K = 1'b0;

parameter xuart_opt = 1'b1;
parameter touchscreen_opt = 1'b1;
parameter spi_opt = 1'b1;
parameter can_opt = 1'b1;
parameter can2_opt = FPGA_8K;

/****************************************************************************
 * TS-4700 boilerplate
 ****************************************************************************/

wire pll_100mhz, pll_locked, pll_75mhz, pll2_locked;
wire wb_clk = pll_75mhz;
reg [15:0] rst_counter = 16'd0;
reg rstn = 1'b0;
reg lock_q;
always @(posedge wb_clk) begin
  lock_q <= pll_locked && pll2_locked && fpga_resetn_pad;
  if (lock_q) rst_counter <= rst_counter + 1'b1;
  else begin
    rst_counter <= 16'd0;
    rstn <= 1'b0;
  end
  if (rst_counter[15]) rstn <= 1'b1;
end
wire wb_rst = !rstn;
pll ts4700pll (
  .CLK(fpga_25mhz_pad),
  .CLKOP(pll_100mhz),
  .LOCK(pll_locked)
);
pll2 ts4700pll2 (
  .CLK(fpga_25mhz_pad),
  .CLKOP(pll_75mhz),
  .LOCK(pll2_locked)
);
/*
wire internal_osc;
reg [15:0] rst_counter = 16'd0;
reg rstn = 1'b0;
always @(posedge internal_osc) begin
  if (pll_locked && ) rst_counter <= rst_counter + 1'b1;
  else begin
    rst_counter <= 16'd0;
    rstn <= 1'b0;
  end
  if (rst_counter[15]) rstn <= 1'b1;
end
wire wb_rst = !rstn;
*/
wire reboot;
assign en_usb_5v_pad = 1'b1;
assign en_lcd_3v_pad = 1'b1;
assign reboot_pad = reboot;
assign sd_powern_pad = 1'b0;

assign cpu_wakeup_pad = 1'b0;

reg en_avdd;
assign en_avdd_otgn_pad = !en_avdd;
reg un_reset;
assign un_reset_pad = un_reset;

reg [31:0] usecs;
reg mode1, mode2;
always @(posedge wb_clk or posedge wb_rst) begin
  if (wb_rst) begin
    en_avdd <= 1'b0;
    un_reset <= 1'b0;
    mode1 <= 1'b0;
    mode2 <= 1'b0;
  end else begin
    en_avdd <= 1'b1;
    if (!un_reset) begin
      mode1 <= bus_alen_pad;
      mode2 <= bus_dir_pad;
    end
    if (usecs[15]) un_reset <= 1'b1; // ~32 ms (16ms was not working)
  end
end

reg [6:0] count_1mhz;
always @(posedge wb_clk or posedge wb_rst) begin
  if (wb_rst) begin
    count_1mhz <= 7'd0;
    usecs <= 32'd0;
  end else begin
    count_1mhz <= count_1mhz + 1;
    //if (count_1mhz == 7'd99) begin
    if (count_1mhz == 7'd74) begin // change for 75MHz wb_clk
      count_1mhz <= 7'd0;
      usecs <= usecs + 1;
    end
  end
end

reg [2:0] count_12mhz; // actually 12.5 MHz
assign mfp_51_pad = count_12mhz[2];
always @(posedge pll_100mhz or posedge wb_rst) begin
  if (wb_rst) count_12mhz <= 3'd0;
  else count_12mhz <= count_12mhz + 1'b1;
end

// pulling all SMC pins high during reset
//assign smc_d_pad[15:0] = un_reset ? {16{1'bz}} : {16{1'b1}};
//assign smc_d_pad[15:0] = {16{1'bz}}; 

/****************************************************************************
 * CPU bridge 
 ****************************************************************************/

wire smc_irq;
assign smc_irq_pad = smc_irq;

wire cpuwbm_cyc_o, cpuwbm_stb_o, cpuwbm_we_o, cpuwbm_err;
wire [15:0] cpuwbm_dat_o;
wire [31:0] cpuwbm_adr_o;
reg [15:0] cpuwbm_dat_i;
wire [1:0] cpuwbm_sel_o;
reg cpuwbm_ack_i;

wire [11:0] ts4700_decodes;

wire [15:0] data_o;
wire data_oe;
assign smc_d_pad = data_oe ? data_o : 16'hzzzz;

wire smc_waitn;
assign smc_rdy_pad = smc_waitn;

/* Corrected notes on SMC_ADV# signal.  The built-in bootrom uses LLA#.  Once
 * we have control, we turn LLA# into a GPIO so that it is not used, and our
 * bus settings use #LUA.
 */

mvsmcbridge cpu(
  .wb_clk_i(wb_clk),
  .wb_rst_i(wb_rst),
  .wbm_cyc_o(cpuwbm_cyc_o),
  .wbm_stb_o(cpuwbm_stb_o),
  .wbm_dat_o(cpuwbm_dat_o),
  .wbm_adr_o(cpuwbm_adr_o),
  .wbm_we_o(cpuwbm_we_o),
  .wbm_dat_i(cpuwbm_dat_i),
  .wbm_ack_i(cpuwbm_ack_i),
  .wbm_err_o(cpuwbm_err),
  .wbm_sel_o(cpuwbm_sel_o),  

  .smc_be1n_i(smc_be1n_pad),
  .smc_be2n_i(smc_be2n_pad),
  .smc_dat_i(smc_d_pad),
  .smc_dat_o(data_o),
  .smc_dat_oe_o(data_oe),
  .smc_rdn_i(smc_rdn_pad),
  .smc_wrn_i(smc_wen_pad),
  .smc_cs0n_i(smc_cs0n_pad),
  .smc_cs1n_i(smc_cs1n_pad),
  .smc_cs2n_i(mfp_18_pad),
  .smc_cs3n_i(1'b1),
  .smc_cs6n_i(1'b1),
  .smc_cs7n_i(1'b1),
  .smc_advn_i(smc_luan_pad & smc_llan_pad),
  .smc_waitn_o(smc_waitn),

  .ts4700_decodes_o(ts4700_decodes)
);

/****************************************************************************
 * SYSCON
 ****************************************************************************/

wire en_can, en_can2, en_spi, en_bbclk, en_adc, en_touch;
wire [5:0] en_txen;
wire [15:0] bus_config;

wire [59:0] dio;
wire [59:0] dio_oe;
wire [59:0] dio_in;

reg [15:0] fpga_irqs;

wire green_led, red_led;
assign green_led_pad = green_led ? 1'b0 : 1'b1;
assign red_led_pad = red_led ? 1'b0 : 1'b1;

reg scwbs_en;
wire scwbs_ack_o;
wire [15:0] scwbs_dat_o;
syscon sysconcore(
  .wb_clk_i(wb_clk),
  .wb_rst_i(wb_rst),

  .wb_cyc_i(cpuwbm_cyc_o && scwbs_en),
  .wb_stb_i(cpuwbm_stb_o && scwbs_en),
  .wb_we_i(cpuwbm_we_o),
  .wb_adr_i(cpuwbm_adr_o),
  .wb_dat_i(cpuwbm_dat_o),
  .wb_sel_i(cpuwbm_sel_o),
  .wb_dat_o(scwbs_dat_o),
  .wb_ack_o(scwbs_ack_o),

  .dio_o(dio),
  .dio_oe_o(dio_oe),
  .dio_i(dio_in),
  .usec_i(usecs),
  .mode1_i(mode1),
  .mode2_i(mode2),

  .bus_config_o(bus_config),
  .en_can_o(en_can),
  .en_can2_o(en_can2),
  .en_spi_o(en_spi),
  .en_bbclk_o(en_bbclk),
  .en_txen_o(en_txen),
  .en_touch_o(en_touch),
  .red_led_o(red_led),
  .green_led_o(green_led),
  .reboot_o(reboot),
  .irqs_i(fpga_irqs),
  .irq_o(smc_irq)

);

/****************************************************************************
 * 16 KByte WISHBONE blockram 
 ****************************************************************************/

wire [31:0] sramwbs1_dat;
wire [15:0] sramwbs1_dat_o = 
  cpuwbm_adr_o[1] ? sramwbs1_dat[31:16] : sramwbs1_dat[15:0];
wire [3:0] cpuwbm32_sel_o =
  cpuwbm_adr_o[1] ? {cpuwbm_sel_o, 2'b00} : {2'b00, cpuwbm_sel_o};
wire sramwbs1_ack_o;
reg sramwbs1_en;
wire xuartwbm_cyc_o, xuartwbm_stb_o, xuartwbm_we_o, xuartwbm_ack_i;
wire [15:0] xuartwbm_adr_o, xuartwbm_dat_o, xuartwbm_dat_i;
wire [31:0] xuartwbm_dat32_i;
wire [3:0] xuartwbm_sel_o;
assign xuartwbm_dat_i = xuartwbm_adr_o[1] ?
  xuartwbm_dat32_i[31:16] : xuartwbm_dat32_i[15:0];
wire sramwbm1_cyc, sramwbm1_stb, sramwbm1_ack;
wire [31:0] sramwbm1_dat;
wb_readbuf srambuf(
  .wb_clk_i(wb_clk),
  .wb_rst_i(wb_rst),

  .wbs_cyc_i(cpuwbm_cyc_o && sramwbs1_en),
  .wbs_stb_i(cpuwbm_stb_o && sramwbs1_en),
  .wbs_we_i(cpuwbm_we_o),
  .wbs_dat_o(sramwbs1_dat),
  .wbs_ack_o(sramwbs1_ack_o),

  .wbm_cyc_o(sramwbm1_cyc),
  .wbm_stb_o(sramwbm1_stb),
  .wbm_ack_i(sramwbm1_ack),
  .wbm_dat_i(sramwbm1_dat)
);
blockram_8kbyte sramcore(
  .wb_clk_i(wb_clk),
  .wb_rst_i(wb_rst),

  .wb1_adr_i(cpuwbm_adr_o),
  .wb1_cyc_i(sramwbm1_cyc),
  .wb1_stb_i(sramwbm1_stb),
  .wb1_we_i(cpuwbm_we_o), 
  .wb1_sel_i(cpuwbm32_sel_o),
  .wb1_dat_i({cpuwbm_dat_o, cpuwbm_dat_o}),
  .wb1_dat_o(sramwbm1_dat),
  .wb1_ack_o(sramwbm1_ack),

  .wb2_cyc_i(xuartwbm_cyc_o),
  .wb2_stb_i(xuartwbm_stb_o),
  .wb2_adr_i(xuartwbm_adr_o),
  .wb2_dat_i({xuartwbm_dat_o, xuartwbm_dat_o}),
  .wb2_sel_i(xuartwbm_sel_o),
  .wb2_we_i(xuartwbm_we_o),
  .wb2_ack_o(xuartwbm_ack_i),
  .wb2_dat_o(xuartwbm_dat32_i)

);

/****************************************************************************
 * XUART core
 ****************************************************************************/

wire xuartwbs_ack;
wire xuartwbs_ack_o = xuartwbs_ack | !xuart_opt;
reg xuwbs_en;
wire [15:0] xuartwbs_dat_o;
reg [7:0] xuart_rx;
wire [7:0] xuart_active;
wire [7:0] xuart_cts = 8'hff;
wire [7:0] xuart_txen, xuart_tx;
reg [7:0] xuart_txen_q, xuart_tx_q;
reg [7:0] xuart_rx_q, xuart_cts_q;
wire xuart_irq;
wire xwbm_cyc_o, xwbm_stb_o, xwbm_we_o, xwbm_ack_i;
wire [15:0] xwbm_adr_o, xwbm_dat_o, xwbm_dat_i;
wire [3:0] xwbm_sel32_o = xwbm_adr_o[1] ? 4'b1100 : 4'b0011;
wire [31:0] xwbm_dat32_i;
wire [3:0] xwbm_sel_o;
always @(posedge wb_clk or posedge wb_rst) begin
  if (wb_rst) begin
    xuart_rx_q <= 8'hff;
    xuart_cts_q <= 8'hff;
    xuart_tx_q <= 8'hff;
    xuart_txen_q <= 8'hff;
  end else begin
    xuart_rx_q <= xuart_rx;
    xuart_cts_q <= xuart_cts;
    xuart_tx_q <= xuart_tx;
    xuart_txen_q <= xuart_txen;
  end
end
wb_buf xubuf(
  .wb_clk_i(wb_clk),
  .wb_rst_i(wb_rst),

  .wbs_cyc_i(xwbm_cyc_o),
  .wbs_stb_i(xwbm_stb_o),
  .wbs_we_i(xwbm_we_o),
  .wbs_dat_o(xwbm_dat_i),
  .wbs_dat_i(xwbm_dat_o),
  .wbs_ack_o(xwbm_ack_i),
  .wbs_adr_i(xwbm_adr_o),
  .wbs_sel_i(xwbm_sel32_o),

  .wbm_adr_o(xuartwbm_adr_o),
  .wbm_cyc_o(xuartwbm_cyc_o),
  .wbm_stb_o(xuartwbm_stb_o),
  .wbm_we_o(xuartwbm_we_o),
  .wbm_dat_o(xuartwbm_dat_o),
  .wbm_ack_i(xuartwbm_ack_i),
  .wbm_sel_o(xuartwbm_sel_o),
  .wbm_dat_i(xuartwbm_dat_i)
);
ts_xuart tsxuartcore(
  .wb_clk_i(wb_clk & xuart_opt),
  .wb_rst_i(wb_rst | !xuart_opt),

  .wbm_cyc_o(xwbm_cyc_o),
  .wbm_stb_o(xwbm_stb_o),
  .wbm_we_o(xwbm_we_o),
  .wbm_ack_i(xwbm_ack_i),
  .wbm_adr_o(xwbm_adr_o),
  .wbm_dat_i(xwbm_dat_i),
  .wbm_dat_o(xwbm_dat_o),

  .wbs_cyc_i(cpuwbm_cyc_o && xuwbs_en),
  .wbs_stb_i(cpuwbm_stb_o && xuwbs_en),
  .wbs_we_i(cpuwbm_we_o),
  .wbs_adr_i(cpuwbm_adr_o),
  .wbs_dat_i(cpuwbm_dat_o),
  .wbs_dat_o(xuartwbs_dat_o),
  .wbs_ack_o(xuartwbs_ack),

  .baudclk_i(pll_100mhz),
  .cts_i(xuart_cts_q),
  .rx_i(xuart_rx_q),
  .tx_o(xuart_tx),
  .txen_o(xuart_txen),
  .active_o(xuart_active),
  .irq_o(xuart_irq),
  .uart_disable_i(FPGA_8K ? 8'h80 : (can_opt ? 8'hf0 : 8'h80))
);

/****************************************************************************
 * Touchscreen core
 ****************************************************************************/

reg tswbs_en;
wire [15:0] tswbs_dat_o;
wire tswbs_ack;
wire tswbs_ack_o = tswbs_ack | !touchscreen_opt;
wire touch_mosi, touch_clk, touch_csn; // CS# is open drain for some reason
touchscreen ts(
  .wb_clk_i(wb_clk & touchscreen_opt),
  .wb_rst_i(wb_rst | !touchscreen_opt),
  .wb_cyc_i(cpuwbm_cyc_o && tswbs_en),
  .wb_stb_i(cpuwbm_stb_o && tswbs_en),
  .wb_we_i(cpuwbm_we_o),
  .wb_adr_i(cpuwbm_adr_o),
  .wb_dat_i(cpuwbm_dat_o),
  .wb_dat_o(tswbs_dat_o),
  .wb_ack_o(tswbs_ack),

  .spi_miso_i(mux_ad_pad[13]),
  .spi_mosi_o(touch_mosi),
  .spi_clk_o(touch_clk),
  .ad7843_csn_o(touch_csn),
  .ad7843_penirqn_i(mux_ad_pad[11])
);

/****************************************************************************
 * CAN core
 ****************************************************************************/

reg [4:0] seq_24mhz;
reg clk_24mhz;
always @(posedge pll_100mhz or posedge wb_rst) begin
  if (wb_rst) begin
    clk_24mhz <= 1'b1;
    seq_24mhz <= 5'd0;
  end else begin
    seq_24mhz <= seq_24mhz + 1'b1;
    case (seq_24mhz)
    1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21: clk_24mhz <= !clk_24mhz;
    24: begin
      clk_24mhz <= !clk_24mhz;
      seq_24mhz <= 5'd0;
    end
    endcase
  end
end

reg canwbs_en;
wire canwbs_ack;
wire canwbs_ack_o = canwbs_ack | !can_opt;
wire can_tx, can_irqn;
wire [7:0] canwbs_dat_i;
assign canwbs_dat_i = cpuwbm_sel_o[0] ? cpuwbm_dat_o[7:0] : cpuwbm_dat_o[15:8];
wire [7:0] canwbs_dat8_o;
wire [15:0] canwbs_dat_o = {canwbs_dat8_o, canwbs_dat8_o};
wire [7:0] canwbs_adr_i;
assign canwbs_adr_i[7:1] = cpuwbm_adr_o[7:1];
assign canwbs_adr_i[0] = !cpuwbm_sel_o[0];

can_top cancore(
  .wb_clk_i(wb_clk && can_opt),
  .wb_rst_i(wb_rst | !can_opt),
  .wb_cyc_i(cpuwbm_cyc_o && canwbs_en),
  .wb_stb_i(cpuwbm_stb_o && canwbs_en),
  .wb_we_i(cpuwbm_we_o),
  .wb_dat_i(canwbs_dat_i),
  .wb_dat_o(canwbs_dat8_o),
  .wb_adr_i(canwbs_adr_i),
  .wb_ack_o(canwbs_ack),

  .clk_i(clk_24mhz && can_opt),
  .rx_i(dio_pad[16]),
  .tx_o(can_tx),
  .irq_on(can_irqn)
  //.bus_off_on()
);

reg can2wbs_en;
wire can2wbs_ack;
wire can2wbs_ack_o = can2wbs_ack | !can2_opt;
wire can2_tx, can2_irqn;
wire [7:0] can2wbs_dat8_o;
wire [15:0] can2wbs_dat_o = {can2wbs_dat8_o, can2wbs_dat8_o};

can_top can2core(
  .wb_clk_i(wb_clk && can2_opt),
  .wb_rst_i(wb_rst | !can2_opt),
  .wb_cyc_i(cpuwbm_cyc_o && can2wbs_en),
  .wb_stb_i(cpuwbm_stb_o && can2wbs_en),
  .wb_we_i(cpuwbm_we_o),
  .wb_dat_i(canwbs_dat_i),
  .wb_dat_o(can2wbs_dat8_o),
  .wb_adr_i(canwbs_adr_i),
  .wb_ack_o(can2wbs_ack),

  .clk_i(clk_24mhz && can2_opt),
  .rx_i(dio_pad[11]),
  .tx_o(can2_tx),
  .irq_on(can2_irqn)
  //.bus_off_on()
);


/****************************************************************************
 * SPI core
 ****************************************************************************/

reg spiwbs_en;
wire [15:0] spiwbs_dat_o;
wire spiwbs_ack;
wire spiwbs_ack_o = spiwbs_ack | !spi_opt;
wire scken;
wire [3:0] spi_csn;
reg [3:0] spi_csn_q;
always @(posedge wb_clk or posedge wb_rst) begin
  if (wb_rst) spi_csn_q <= 4'hf;
  else spi_csn_q <= spi_csn;
end
wire [3:0] spi_si = {4{spi_miso_pad}};
wire spi_sck, hispeed, spi_clk;
assign spi_clk = hispeed ? (pll_75mhz | !scken) : spi_sck;
wire spi_so;
wb_spi spicore(
  .wb_rst_i(wb_rst | !spi_opt),
  .wb_clk_i(wb_clk & spi_opt),
  .wb_cyc_i(cpuwbm_cyc_o && spiwbs_en),
  .wb_stb_i(cpuwbm_stb_o && spiwbs_en),
  .wb_dat_o(spiwbs_dat_o),
  .wb_dat_i(cpuwbm_dat_o),
  .wb_adr_i(cpuwbm_adr_o),
  .wb_we_i(cpuwbm_we_o),
  .wb_ack_o(spiwbs_ack),
  .wb_sel_i(cpuwbm_sel_o),

  .scken_o(scken),
  .so_o(spi_so),
  .si_i(spi_si),
  .csn_o(spi_csn),
  .sck_o(spi_sck),
  .hispeed_o(hispeed)
);

/****************************************************************************
 * ADC core
 ****************************************************************************/

reg adcwbs_en;
wire [15:0] adcwbs_dat_o;
wire adcwbs_ack_o;
wire adc_scl, adc_sda, an_sel;
wire [1:0] an_sel_sel;

wb_mcp3428 adc(
  .wb_rst_i(wb_rst),
  .wb_clk_i(wb_clk),
  .wb_cyc_i(cpuwbm_cyc_o && adcwbs_en),
  .wb_stb_i(cpuwbm_stb_o && adcwbs_en),
  .wb_dat_o(adcwbs_dat_o),
  .wb_dat_i(cpuwbm_dat_o),
  .wb_adr_i(cpuwbm_adr_o),
  .wb_we_i(cpuwbm_we_o),
  .wb_ack_o(adcwbs_ack_o),
  .wb_sel_i(cpuwbm_sel_o),

  .scl_o(adc_scl),
  .sda_o(adc_sda),
  .sda_i(dio_pad[6]),
  .an_sel_o(an_sel),
  .an_sel_sel_o(an_sel_sel),
  .active_o(en_adc)
);


/****************************************************************************
 * External address-data bus
 ****************************************************************************/

reg buswbs_en;
wire [15:0] buswbs_dat_o;
wire buswbs_ack_o;

wire [15:0] mux_dat;
wire bus_oe, bus_csn, bus_dir, bus_ale, bus_bhen;

isamuxbus_bridge muxbus(
  .wb_clk_i(wb_clk),
  .wb_rst_i(wb_rst),
  .wb_cyc_i(cpuwbm_cyc_o && buswbs_en),
  .wb_stb_i(cpuwbm_stb_o && buswbs_en),
  .wb_dat_o(buswbs_dat_o),
  .wb_dat_i(cpuwbm_dat_o),
  .wb_adr_i({1'b0, cpuwbm_adr_o[14:0]}),
  .wb_we_i(cpuwbm_we_o),
  .wb_ack_o(buswbs_ack_o),
  .wb_sel_i(cpuwbm_sel_o),

  .bus_config_i(bus_config),
  .cs_8bit_i(!smc_cs1n_pad),

  .csn_o(bus_csn),
  .dir_o(bus_dir),
  .ale_o(bus_ale),
  .bhen_o(bus_bhen),
  .waitn_i(bus_rdy_pad),
  .mux_i(mux_ad_pad[15:0]),
  .mux_o(mux_dat),
  .mux_oe_o(bus_oe)
);

/****************************************************************************
 * DIO multiplexing
 ****************************************************************************/

reg [59:0] dio_reg, pad_o, pad_oe;

assign dio_pad[16:0] = dio_reg[16:0];
assign spi_frm_pad = dio_reg[17];
assign spi_mosi_pad = dio_reg[18];
assign spi_miso_pad = dio_reg[19];
assign spi_clk_pad = dio_reg[20];
assign bus_rdy_pad = dio_reg[22];
assign bus_bhen_pad = dio_reg[23];
assign bus_csn_pad = dio_reg[24];
assign bus_dir_pad = dio_reg[25];
assign bus_alen_pad = dio_reg[26];
assign mux_ad_pad[7:0] = dio_reg[42:35];
assign mux_ad_pad[15:8] = dio_reg[34:27];
assign uart1_txd_pad = dio_reg[48];
assign uart1_rxd_pad = dio_reg[49];
assign uart2_txd_pad = dio_reg[50];
assign uart2_rxd_pad = dio_reg[51];
assign uart3_txd_pad = dio_reg[52];
assign uart3_rxd_pad = dio_reg[53];
assign uart4_txd_pad = dio_reg[54];
assign uart4_rxd_pad = dio_reg[55];
assign uart5_txd_pad = dio_reg[56];
assign uart5_rxd_pad = dio_reg[57];
assign uart6_txd_pad = dio_reg[58];
assign uart6_rxd_pad = dio_reg[59];

assign dio_in[59:48] = {
  uart6_rxd_pad,
  uart6_txd_pad,
  uart5_rxd_pad,
  uart5_txd_pad,
  uart4_rxd_pad,
  uart4_txd_pad,
  uart3_rxd_pad,
  uart3_txd_pad,
  uart2_rxd_pad,
  uart2_txd_pad,
  uart1_rxd_pad,
  uart1_txd_pad
};
assign dio_in[42:27] = {mux_ad_pad[7:0], mux_ad_pad[15:8]};
assign dio_in[26:22] = {
  bus_alen_pad,
  bus_dir_pad,
  bus_csn_pad,
  bus_bhen_pad,
  bus_rdy_pad
};
assign dio_in[20:17] = {
  spi_clk_pad,
  spi_miso_pad,
  spi_mosi_pad,
  spi_frm_pad
};
assign dio_in[16:0] = dio_pad[16:0];

integer i;

always @(*) begin
  for (i = 0; i <= 59; i = i + 1) begin
    dio_reg[i] = pad_oe[i] ? pad_o[i] : 1'bz;
  end
end

always @(*) begin
  pad_o = dio;
  pad_oe = dio_oe;

  fpga_irqs = 16'h0000;
  /* irq0: XUART
   * irq1: CAN
   * irq2: 2nd CAN
   * irq3: PC/104 irq 5
   * irq4: PC/104 irq 6
   * irq5: PC/104 irq 7
   * irq6-15 reserved
   * These 16 irqs will be remapped to Linux irqs by the kernel.
   */

  /* Primary CAN port overrides dio 15 and 16 */
  if (en_can) begin
    pad_o[15] = can_tx;
    pad_oe[16:15] = 2'b01;
    fpga_irqs[1] = !can_irqn;
  end

  /* Secondary CAN port overrides dio 10 and 11 */
  if (en_can2) begin
    pad_o[10] = can2_tx;
    pad_oe[11:10] = 2'b01;
    fpga_irqs[2] = !can2_irqn;
  end

  /* SPI core overrides DIO 17-20 */
  if (en_spi) begin
    pad_o[17] = spi_csn[0];
    pad_o[18] = spi_so;
    pad_o[20] = spi_clk;
    pad_oe[20:17] = 4'b1011;
  end

  /* ADC core overrides DIO 6 and 7.  It may also override DIO 8 or DIO 29 */
  if (en_adc) begin
    pad_o[7:6] = 2'b00;
    pad_oe[7] = !adc_scl;
    pad_oe[6] = !adc_sda;
    if (an_sel_sel == 2'b01) begin
      pad_o[8] = an_sel;
      pad_oe[8] = 1'b1;
    end else if (an_sel_sel == 2'b10) begin
      pad_o[29] = an_sel;
      pad_oe[29] = 1'b1;
    end
  end

  /* touchscreen overrides DIO 30 to 34 */
  if (en_touch) begin
    pad_o[34] = touch_clk;
    pad_o[33] = touch_mosi;
    pad_o[31] = 1'b0;
    pad_oe[31] = !touch_csn;
    pad_oe[30] = 1'b0;
    pad_oe[34:32] = 3'b110;
  end

  /* external bus overrides DIO 22-42 */
  if (bus_config[0]) begin // config bit 0 = bus enable
    pad_o[23] = bus_bhen;
    pad_o[24] = bus_csn;
    pad_o[25] = bus_dir;
    pad_o[26] = !bus_ale;
    pad_oe[26:22] = 5'b11110;
    pad_o[42:35] = mux_dat[7:0];
    pad_o[34:27] = mux_dat[15:8];
    pad_oe[42:27] = bus_oe ? 16'hffff : 16'h0000;
    fpga_irqs[5:3] = dio_pad[2:0];
    pad_oe[2:0] = 3'b000;
  end

  /* DIO#26 and DIO#27 are are latched bootup MODE1/MODE2 pins */
  // I think we could discard this line since DIO default to inputs anyway.
  //if (!un_reset) pad_oe[26:25] = 2'b00;

  /* WARNING: UART numbering is all off by one. */
  if (xuart_active[7:0] != 8'h00) fpga_irqs[0] = xuart_irq;
  xuart_rx = 8'hff;

  xuart_rx[0] = uart1_rxd_pad;
  if (xuart_active[0]) begin
    pad_o[48] = xuart_tx_q[0];
    pad_oe[49:48] = 2'b01;
    if (en_txen[0]) begin
      pad_o[12] = xuart_txen_q[0];
      pad_oe[12] = 1'b1;
    end
  end

  xuart_rx[1] = uart2_rxd_pad;
  if (xuart_active[1]) begin
    pad_o[50] = xuart_tx_q[1];
    pad_oe[51:50] = 2'b01;
    if (en_txen[1]) begin
      pad_o[8] = xuart_txen_q[1];
      pad_oe[8] = 1'b1;
    end
  end

  xuart_rx[2] = uart3_rxd_pad;
  if (xuart_active[2]) begin
    pad_o[52] = xuart_tx_q[2];
    pad_oe[53:52] = 2'b01;
    if (en_txen[2]) begin
      pad_o[10] = xuart_txen_q[2];
      pad_oe[10] = 1'b1;
    end
  end

  xuart_rx[3] = uart4_rxd_pad;
  if (xuart_active[3]) begin
    pad_o[54] = xuart_tx_q[3];
    pad_oe[55:54] = 2'b01;
    if (en_txen[3]) begin
      pad_o[13] = xuart_txen_q[3];
      pad_oe[13] = 1'b1;
    end
  end

  xuart_rx[4] = uart5_rxd_pad;
  if (xuart_active[4]) begin
    pad_o[56] = xuart_tx_q[4];
    pad_oe[57:56] = 2'b01;
    if (en_txen[4]) begin
      pad_o[14] = xuart_txen_q[4];
      pad_oe[14] = 1'b1;
    end
  end

  xuart_rx[5] = uart6_rxd_pad;
  if (xuart_active[5]) begin
    pad_o[58] = xuart_tx_q[5];
    pad_oe[59:58] = 2'b01;
    if (en_txen[5]) begin
      pad_o[7] = xuart_txen_q[5];
      pad_oe[7] = 1'b1;
    end
  end

  xuart_rx[6] = dio_pad[13];

  if (en_bbclk) begin
    pad_o[3] = count_12mhz[2];
    pad_oe[3] = 1'b1;
  end

end

/****************************************************************************
 * Master address decode
 ****************************************************************************/

/*

64 KB FPGA address space:
start   stop    usage
-----   ----    -----
0x8000  0xFFFF  external bus access
0x5400          XUART IO registers
0x5000          touchscreen core
0x4D00  0x4DFF  2nd CAN core
0x4C00  0x4CFF  CAN core
0x4800          SPI core
0x4400          ADC core
0x4000          syscon registers
0x0000  0x3FFF  16KB blockram access

*/
always @(*) begin
  
  sramwbs1_en = ts4700_decodes[0];
  buswbs_en = ts4700_decodes[1];
  xuwbs_en = ts4700_decodes[2];
  tswbs_en = ts4700_decodes[3];
  canwbs_en = ts4700_decodes[4];
  spiwbs_en = ts4700_decodes[5];
  adcwbs_en = ts4700_decodes[6];
  scwbs_en = ts4700_decodes[7];
  can2wbs_en = ts4700_decodes[8];
  cpuwbm_dat_i = 16'hxxxx;
  cpuwbm_ack_i = 1'b1;

  if (ts4700_decodes[0]) begin
    cpuwbm_dat_i = sramwbs1_dat_o;
    cpuwbm_ack_i = sramwbs1_ack_o;
  end else if (ts4700_decodes[1]) begin
    cpuwbm_dat_i = buswbs_dat_o;
    cpuwbm_ack_i = buswbs_ack_o;
  end else if (ts4700_decodes[2]) begin
    cpuwbm_dat_i = xuartwbs_dat_o;
    cpuwbm_ack_i = xuartwbs_ack_o;
  end else if (ts4700_decodes[3]) begin
    cpuwbm_dat_i = tswbs_dat_o;
    cpuwbm_ack_i = tswbs_ack_o;
  end else if (ts4700_decodes[4]) begin
    cpuwbm_dat_i = canwbs_dat_o;
    cpuwbm_ack_i = canwbs_ack_o;
  end else if (ts4700_decodes[8]) begin
    cpuwbm_dat_i = can2wbs_dat_o;
    cpuwbm_ack_i = can2wbs_ack_o;
  end else if (ts4700_decodes[5]) begin
    cpuwbm_dat_i = spiwbs_dat_o;
    cpuwbm_ack_i = spiwbs_ack_o;
  end else if (ts4700_decodes[6]) begin
    cpuwbm_dat_i = adcwbs_dat_o;
    cpuwbm_ack_i = adcwbs_ack_o;
  end else if (ts4700_decodes[7]) begin
    cpuwbm_dat_i = scwbs_dat_o;
    cpuwbm_ack_i = scwbs_ack_o;
  end
end

endmodule




