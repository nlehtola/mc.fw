#A simple start/stop script for TLSX servers. This is to test that 
#a sequence of start/stop doesnot lock the machine.

iterations=100

for i in `seq $iterations`
do
	/etc/init.d/run_cs_servers stop > /dev/null
	#On an IQS remove menu from the list
	ps -A | grep -E "SCPI|VXI|Serve|TLSX|B2S|menu" > /dev/null
	if [ $? -eq 0 ]
        then 
                printf "Fail:%d stop all servers." $i
	else
		printf "Pass:%d stop all servers.\n" $i
        fi 

	/etc/init.d/run_cs_servers start > /dev/null

	declare -a servers=("SCPI" "VXI" "ServerLoop" "TLSX" "B2S" "menu")
	for j in ${servers[@]}
	do	
		ps -A | grep -E $j > /dev/null

		if [ $? -ne 0 ] 
		then 
			printf "Fail:%d %s start.\n" $i $j
		else
			printf "Pass:%d %s start.\n" $i $j
		fi
	done 
done


