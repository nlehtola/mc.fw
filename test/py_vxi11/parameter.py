#########################################################################################################
# Class:     TLSX
# Author:    Usama Malik
# Date:      24 September 2013
# Company:   Coherent Solutions. 
#            @All rights reserved 2013. 
# Description:
#            Contains methods for manipulating ITLS parameters
#########################################################################################################        


#In this class min and max are read from the device. The other values like
#set, act and lock are desired points. Once these points are set then we
#perform a read back from the device and check if that value is what is 
#expected


class PARAMETER:


    def __init__(self, name):
        self.name = name
        self.values = {}
    
    def __str__(self):
        string = "["+self.name+"]"
        for key, value in self.values.items():
            string += "  "+key+":" + str(value)
        return string

    def set_value(self, name, value):
        self.values[name] = value
        return
        
    def get_value(self, name):
        return self.values[name]
    #The value read from the FW will differ due to two reasons:
    # 1) Rounding
    # 2) The ACT for power is a measured value

    #In addition to this, frequencey and wavelength have no numeric ACT value. 
    #Instead they have a lock attribute. All this information is encoded in this 
    #method
    def is_read_equal_expected(self, name, value):
        "Returns true if |a-b| is less than range_percentage of max(a,b)"     
        error_margin = 0.0
        if name=="set":
            error_margin = 0.1 #0.1% to cater for rounding 
        else:
            error_margin = 1.0 #1.0% for measured values
        expected = self.values['expected'] 
        #print error_margin
        #print (abs(expected-value)/max(expected,value))*100
        if ((abs(expected-value)/max(expected,value))*100> error_margin):
            return False
        else:
            return True
                
def main():

    p = PARAMETER("power")
    print p


if __name__=="__main__":
    main()
