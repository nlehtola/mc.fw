#########################################################################################################
# Class:     TLSX
# Author:    Usama Malik
# Date:      24 September 2013
# Company:   Coherent Solutions. 
#            @All rights reserved 2013. 
# Description:
#            Contains methods for running tests on a TLSX unit via VXI-11/SCPI interface
#########################################################################################################        


import sys
import socket
import select
import time
import vxi11
import argparse 
import random
import re
from parameter import *

class TLSX:

    PIF_PORT = 50007
    PIF_TIMEOUT = 3
    
    def __init__(self, ip):
        if(ip):
            self.instrument = vxi11.Instrument(ip)
        else:
            None

#########################################################################################################
# Function to search via pif. Copied from Oliver's pif.py 
#########################################################################################################           
    def search(self):
        # look for pif messages and print out the devices found
        print("searching for pif devices..")
        end_time = time.time() + self.PIF_TIMEOUT
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind(('', self.PIF_PORT))
        s.setblocking(0)
        while 1:
                try:
                        if select.select([s], [], [], 1)[0]:
                                #conn, addr = s.accept()
                                #msg = s.recv(8192)
                                msg, addr = s.recvfrom(8192)
                                if msg == 'pif':
                                        print "pif found: " + str(addr[0])
                        now = time.time()
                        if now > end_time:
                                break
                except KeyboardInterrupt:
                        break
#########################################################################################################
# Function to make VXI-11 calls
#########################################################################################################        
    def run(self, command):
        
        try:
            if ("?" in command):
                vxi11_data= self.instrument.ask(command);
                if(len(vxi11_data.strip())==0):
                    print "Instrument retuened zero length data."
                    return None
                else:
                    return vxi11_data
            else:
                self.instrument.write(command)
        except vxi11.vxi11.Vxi11Error as e:
            print e
            return None


#########################################################################################################
# executes opt command and determines which blades are present in the system
#########################################################################################################        


    def find_blades(self):

        blades = []
        vxi11_data = self.run("*opt?");
        if vxi11_data is None:
            print "[FAIL] VXI-11 did not return data."
            return
        if (vxi11_data.startswith("XX")):
            return ["15"]
        a =  vxi11_data.split(',')   
        if len(a)!=9:
            print "[FAIL]: "+ vxi11_data
            return []
            
        for i in range(9):
            if len(a[i])!=0:
                blades.append(str(i+1))

        return blades
#########################################################################################################
# executes opt command and determines which blades are present in the system
#########################################################################################################        
    def find_lasers(self, blade):

        lasers = []
        vxi11_data = self.run("slot"+blade+":opt?");
        if vxi11_data is None:
            print "[FAIL] VXI-11 did not return data."
            return
        a =  vxi11_data.split(',')   
        if len(a)!=4:
            print "[FAIL]: "+ vxi11_data
            return []
            
        for i in range(4):
            if len(a[i])!=0:
                lasers.append(str(i+1))

        return lasers

#########################################################################################################
# Test if there are any blades/lasers present. 
#########################################################################################################        
    def blades_lasers_test(self):

         #tlsx_unit.run_idn_test(opt_args.blades)
        blades = self.find_blades()
        if not blades:
            print "[FAIL] No blades found in the system"
            return
        for i in blades:
            lasers = self.find_lasers(i)
            if not lasers:
                print "[FAIL] Blade:"+str(i)+"has no lasers."
            else:
                print "[PASS] Blade:"+str(i)+" Lasers:",
                print lasers
            


#########################################################################################################
# Test if IDN commands returns properly formatted strings. 
#########################################################################################################        
    def run_idn_test(self, blades):
        #The following define regexs for parts of the idn strings
        company = "EXFO"
        instrument_model = "(IQS-636-OLS)|(FLS-2800-[\w][\w]-[\w]-[\w]C)"
        blade_model = "(IQS-2800-1-[\w]-[\w]-[\w]-[\w]-[\w]C)|(FLS-2800-[\w][\w]-[\w]-[\w]C)"
        serial = "CSL-[\w]{6}"
        version = "HW[\d].[\d]FW[\d].[\d]"

        instrument_idn = [company, instrument_model, serial, version]
        blade_idn  = [company, blade_model, serial, version]
        print "Test: Instrument IDN."
        #Get instrument level idn 
        vxi11_data = self.run("*idn?");
        if vxi11_data is None:
            print "[FAIL] VXI-11 did not return data."
            return
        #Tokenise and make sure there are exactly four tokens. 

        tokens = vxi11_data.split(',')

        if len(tokens)!=4:
            print "[FAIL] " + vxi11_data
        else:
            for i in range(4):
                if not self.regex_match(instrument_idn[i], tokens[i].strip()):
                    print "[FAIL] " + tokens[i]
        #Test blade_idns now             
        print "Test: Blades IDN:"
        
        for i in blades:
            print "Blade " + str(i)
            vxi11_data = self.run("slot"+i+":idn?");
            if vxi11_data is None:
                print "[FAIL] Instrument returned no data: "+ str(i)
                continue
            #Tokenise and make sure there are exactly four tokens.                     
            tokens = vxi11_data.split(',')
                    
            if len(tokens)!=4:
                print "[FAIL] " + vxi11_data
            else:
                for i in range(4):
                    if not self.regex_match(blade_idn[i], tokens[i].strip()):
                        print "[FAIL] " + tokens[i]
                        result = False

        if result:
            print "Test passed."
        else:
            print "Test failed."
        print "---------------------------------------------"


#########################################################################################################
# Test : Testing if common commands work
#########################################################################################################        
    def run_common_commands_test(self):
        "Runs common commands"
        result = True #assume that the test pass
        common_commands = ["*opc?", "*opt?"]
        print "Test 0. Common commands. Criterion for passing"
        print "is any non-empty string from the instrument"
        print "---------------------------------------------"
        for i in common_commands:
            print "->"+i
            try:
                return_data = self.run(i);
                if(len(return_data.strip())==0):
                    print "FAIL: zero length returned."
                    result = False
                else:
                    print "PASS: "+return_data
                
            except vxi11.vxi11.Vxi11Error as e:
                print e
        print "---------------------------------------------"
        if result:
            print "Test passed."
        else:
            print "Test failed."
        print "---------------------------------------------"
#########################################################################################################
# Test : Testing if laser can be turned on/off in a sequence
#########################################################################################################        

    def run_safety_test(self, blade, laser):
        "Laser on/off/on/off test."
        print "Test: Safety test." 
        print "---------------------------------------------"

        result = True
        command_prefix = "outp"+blade+":chan"+laser

        laser_on     = command_prefix+":state on"
        laser_off    = command_prefix+":state off"
        laser_state  = command_prefix+":state?"
        safety_on    = command_prefix+":safety on"
        safety_off   = command_prefix+":safety off"
        safety_state = command_prefix+":safety?"

        # We now define a sequence of operation tuples. For each tuple, [0] defines the operation 
        # and [1] defines the expected return value from the SCPI call. 
        # We first turn laser on. We then turn safety on and check that 
        # it turns off the laser and laser cannot be turned on. We then 
        # turn safety off and make sure that laser state can be toggled.

        seq = [(safety_on, ""),(safety_state,"on"),(laser_state,"off"), \
               (laser_on, ""), (laser_state, "off"),\
               (safety_off, ""), (safety_state,"off"),\
               (laser_on, ""), (laser_state, "on"),\
               (laser_off, ""), (laser_state, "off"),\
               (laser_on, ""), (laser_state, "on"),\
               (safety_on, ""),(safety_state,"on"), (laser_state,"off"), \
               (safety_off, ""), (safety_state,"off"),\
               ]
       
        for i in seq:
            print i[0]
            vxi11_data = self.run(i[0]);
            if (0!=len(i[1])): #there is an expected value
                if vxi11_data is None:
                    print "[FAIL] blade:" +str(blade) + " Laser:"+ str(laser)
                    return
                #for some reason state changing commands take a while to settle
                for j in range(10):
                    time.sleep(2) 
                    if (vxi11_data.lower() !=i[1]):
                        vxi11_data = self.run(i[0]);
                    else:
                        break;
                if (vxi11_data.lower() !=i[1]): #check for the final iteration
                    print "[FAIL]" + vxi11_data
                    result = False
                else:
                    print "[PASS]"

        print "---------------------------------------------"
        if result:
            print "[PASS]"
        else:
            print "[FAIL]"
        print "---------------------------------------------"

#########################################################################################################
# Test : Testing if laser can be turned on/off in a sequence
#########################################################################################################        

    def run_on_off_test(self, run_length, blade, laser):
        "Laser on/off/on/off test."
        print "Test: Switching laser into on,off,on,off sequence." 
        print "---------------------------------------------"

        result = True
        states = []
        scpi_command = "outp"+blade+":chan"+laser+":state"

        for i in range(run_length):
            if random.random()>0.5:
                states.append("on")
            else:
                states.append("off")
         
        for i in states:            
            self.run(scpi_command+ " " + i);
            time.sleep(0.2)
            return_data = self.run(scpi_command+ "?" );
            if return_data is None:
                print "[FAIL] blade:" +str(blade) + " Laser:"+ str(laser)
                result = False     
                continue
            if(return_data.strip().lower()!=i):
                print "[FAIL] "+ return_data
                result = False
            else:
                None
                
        print "---------------------------------------------"
        if result:
            print "[PASS]"
        else:
            print "[FAIL]"
        print "---------------------------------------------"
#########################################################################################################
# Function to test temperature values. Two tests are performed:
# 1) Temperature value is within min and max
# 2) Successive temperature values are not that far off from the running average
#########################################################################################################        
    def run_temperature_test(self,run_length, blade, laser):
        "Testing temperature values.."
        print "Testing temperature values."
        result = True
        temperature_min = 5.0 #If below this in lab enviornment we have an issue
        temperature_max = 50.0 #If above this in lab enviornment we have an issue
        running_average = 0.0
        running_sum = 0.0
        accepted_flucuation = 2.0
        scpi_command = "sour"+blade+":chan"+laser+":temperature?"
        for i in range(run_length):
            return_data = self.run(scpi_command);
            if(return_data is None):
                print "[FAIL]"
                return 
            print return_data
            if (i>2 and float(return_data)-running_average > accepted_flucuation):
                    print "FAIL: Large fluctuation in temperature."
                    result = False
                    break
            running_sum += float(return_data)
            running_average = running_sum/(i+1)            
            if(float(return_data) < temperature_min or float(return_data)>temperature_max):
                print "FAIL:"
                result = False
        #print result
        return result

#########################################################################################################
# Poll the laser power mixed with other cmd's to shake the lasers state up a bit and ensure the power is 
# always returned correctly (tlsx-209)
# We:
# 1) Set power actual to a known value
# 2) Loop polling power actual mixed with other random commands
# NB: I think tlsx-209 fails because the laser gets in a bad state after a execution error. Currently I
# can reliably generate this by setting dither I think, but when this is fixed in the blade firmware this
# test will need to be updated
#########################################################################################################        
    def run_poll_power_act(self, run_length, blade, laser):
        "Testing poll of power actual"
        print "Testing poll of power actual"
        power_target = 10.0
        power_tolerance = 1.0 # +/- 1dBm is pretty generous !!

        scpi_command_prefix = "sour"+blade+":chan"+laser+":"
        scpi_command_power_act = "power? act"
        scpi_random_cmd_list = ["dith?", "grid? all", "freq? all", "", ""]
        
        # set the laser on and initial power
        self.run(scpi_command_prefix + "power " + str(power_target))
        time.sleep(0.5)
        self.run("output" + blade + ":chan" + laser + ":state on")
        time.sleep(0.5)

        for run in range(run_length):
            # shuffle the cmd list to intersperse the power polling
            random.shuffle(scpi_random_cmd_list)

            for c in scpi_random_cmd_list:
                # send a random cmd
                if c != "":
                    cmd = scpi_command_prefix + c
                    print "sending: " + cmd
                    r = self.run(scpi_command_prefix + c)

                # check the actual power is still in spec
                cmd = scpi_command_prefix + scpi_command_power_act
                print "sending: " + cmd
                r = self.run(cmd)
                if r is None:
                    print "[FAIL: comms error !]"
                    return False
                pa = float(r)
                print "power_act[run:" + str(run) + "] = " + str(pa)
                pa_min = (power_target - power_tolerance) 
                pa_max = (power_target + power_tolerance) 
                if pa < pa_min or pa > pa_max:
                    print "[FAIL: power outside tolerance range (failed " + str(pa_min) + " < " + str(pa) + " < " + str(pa_max) + " requirement) !]"
                    return False
        return True

#########################################################################################################
# Auxillary function to turn laser on or off
#########################################################################################################        
    def switch_laser(self, on_off, blade, laser):
        "Turning laser on."
        print "Turning laser " + on_off 
        result = True
        scpi_command = "outp"+blade+":chan"+laser+":state"
        self.run(scpi_command + " "+on_off);
        time.sleep(0.5)
        return_data = self.run(scpi_command+ "?" );
        if(return_data is None):
            print "[FAIL]"
            return 
        if return_data.strip().lower()!=on_off:
            print "failed to turn on laser: "+ return_data
            result = False

        return result
#########################################################################################################
# Auxillary functions 
#########################################################################################################        

    def report(self, parameter, value_name, value_read):
        
        if value_read is None:
            print "coms error."
            return
        
        expected = parameter.get_value('expected')
        outcome = True
        print "expected: " + str(expected) + " value:" + str(value_read) 
        if parameter.is_read_equal_expected(value_name, value_read):
            print "[PASS]\t", 
        else:
            print "[FAIL]\t",
            outcome = False
        print "expected: "+ str(expected)+ " Read: "+ str(value_read)
        return outcome


        #returns True if tha target match the regex else False
    def regex_match(self, regex, target):
        r = re.compile(regex)
        match = r.match(target)
        if match is None or len(match.group())!=len(target):
            return False
        else:
            return True
#########################################################################################################
# Sometimes changing frequency set point and then reading power leads to absurd values.
#########################################################################################################        
      
    def run_large_values_test(self, blade, laser):
        print "Test:  Large values:"
        print "---------------------------------------------"
        
        sleep_time = 1
        result = True
        scpi_freq_command = "sour"+blade+":chan"+laser+":freq"
        scpi_power_command = "sour"+blade+":chan"+laser+":power"
        
        parameter = PARAMETER("freq")
        #min and max are always defined. 
        try:
            min_power = float(self.run(scpi_power_command+ "? min"))
            max_power = float(self.run(scpi_power_command+ "? max"))
            min_freq = float(self.run(scpi_freq_command+ "? min"))
            max_freq = float(self.run(scpi_freq_command+ "? max"))
        except (vxi11.vxi11.Vxi11Error, ValueError) as e:
            print e
            return;
        #min and max has been found. Now find a random value in between these two
        parameter.set_value("min", min_freq);
        parameter.set_value("min", max_freq);
        num = random.randrange(int(min_freq), int(max_freq), 1)
        fraction = random.random()
        set_point = float(num)+fraction

        self.run(scpi_freq_command+ " "+str(set_point))

        #Now check power set point and act
        power_set =  self.run(scpi_power_command+"? set")
        power_act =  self.run(scpi_power_command+"? act")
        pow_parameter = PARAMETER("pow")
        pow_parameter.set_value("expected",float(power_set))
        print str(power_set) + " " + str(power_act)
        if pow_parameter.is_read_equal_expected("act", float(power_act)):
            print "PASS: "
            return True
        else:
            print "FAIL: "
            return False

  
#########################################################################################################
# OPC should go low as soon as we issue power or frequency command and should come high after some time
# The test works by trying to change the current value to a random value between min and max
#########################################################################################################        
      
    def run_opc_test(self, blade, laser, parameter):
        print "Test:  OPC:"+ parameter
        print "---------------------------------------------"
        
        sleep_time = 1
        threshold  = 20
        result = True
        scpi_command = "sour"+blade+":chan"+laser+":power"
        opc_command = "slot"+blade+":opc?"
        parameter = PARAMETER(parameter)
        #min and max are always defined. 
        try:
            min = float(self.run(scpi_command+ "? min"))
            max = float(self.run(scpi_command+ "? max"))
        except (vxi11.vxi11.Vxi11Error, ValueError) as e:
            print e
            return;
        if (max < min):
            print "Error: max is less than min. min:" + min + " max:"+max
            print "Automated test cannot run with these values."
            return 
        #min and max has been found. Now find a random value in between these two
        parameter.set_value("min", min);
        parameter.set_value("min", max);
        num = random.randrange(int(min), int(max), 1)
        fraction = random.random()
        set_point = float(num)+fraction

        parameter.set_value("expected",set_point)
        print "Changing " + str(parameter)
        self.run(scpi_command+ " "+str(set_point))
        vxi11_data =  self.run(opc_command)
        iterations = 0
        while (vxi11_data =='0' and iterations < threshold):
            vxi11_data =  self.run(opc_command)
            iterations  += 1
            time.sleep(sleep_time)


        if (vxi11_data =='1'):
            print "PASS: iterations: "+str(iterations)
        else:
            print "FAIL: iterations: "+str(iterations)

        return

#########################################################################################################
# Test 2: Checking a given parameter's range test
#########################################################################################################        
    def run_exceed_range_test(self, range_direction, parameter_name, blade, laser):
        "Parameter min/max"
        print "Test:  Switching "+parameter_name+ " parameter outside min/max range." 
        print "---------------------------------------------"
        
        sleep_time = 5
        result = True
        scpi_command = "sour"+blade+":chan"+laser+":"+parameter_name
        parameter = PARAMETER(parameter_name)
        #min and max are always defined. 
        try:
            min = float(self.run(scpi_command+ "? min"))
            max = float(self.run(scpi_command+ "? max"))
        except (vxi11.vxi11.Vxi11Error, ValueError) as e:
            print e
            return;
        if (max < min):
            print "Error: max is less than min. min:" + min + " max:"+max
            print "Automated test cannot run with these values."
            return 
        #min and max has been found. Add to the parameter
        parameter.set_value("min", min);
        parameter.set_value("min", max);

        exceeded_value = 0.0        
        if (range_direction=="min"):
            exceeded_value = min-(min/10);
            parameter.set_value("expected", min)
        else: 
            if (range_direction=="max"):
                exceeded_value = max+(max/10);
                parameter.set_value("expected", max)
                
        self.run(scpi_command+ " "+str(exceeded_value))
                 
                
        value_read = None
        #power has set and act 
        if parameter_name == "power" or parameter_name == "frequency" or parameter_name=="wavelength":            
            try:
                value_read = float(self.run(scpi_command+ "? set" ));
            except (vxi11.vxi11.Vxi11Error, ValueError) as e:
                print e
            print "Testing set value."
            self.report(parameter, "set", value_read)
        
        #sleep so that the new value can be settled
        #Frequency has lock 
        if parameter_name == "frequency":
            try:
                for i in range(10):
                    print "Sleeping for "+ str(sleep_time) + "s"
                    time.sleep(sleep_time)        
                    value_read = self.run(scpi_command+ "? lock" );
                    if value_read.lower() =="true":
                        break;                    
            except (vxi11.vxi11.Vxi11Error, ValueError) as e:
                print e
            print "Testing lock."
            if value_read.lower() !="true":
                print "[FAIL]\texpected: true Read: "+ value_read.lower()
            else:
                print "[PASS]\texpected: true Read: "+ value_read.lower()

        #Apart from frequency all other parameters have act 
        if parameter_name != "frequency" and parameter_name!="wavelength":            
            print "Sleeping for "+ str(sleep_time*2) + "s"
            time.sleep(sleep_time*2)        
            value_read = float(self.run(scpi_command+ "? act"));
            print "Testing act value."
            self.report(parameter, "act", value_read)
#########################################################################################################
# Test : Checking a given parameter can be changed within it's range
#########################################################################################################        
    def run_within_range_test(self, parameter_name, run_length, blade, laser):
        "Parameter min/max"
        print "Test:  Switching "+parameter_name+ " parameter inside min/max range." 
        print "---------------------------------------------"
        
        sleep_time = 5
        result = True
        scpi_command = "sour"+blade+":chan"+laser+":"+parameter_name
        parameter = PARAMETER(parameter_name)
        #min and max are always defined. 
        try:
            min = float(self.run(scpi_command+ "? min"))
            max = float(self.run(scpi_command+ "? max"))
        except (vxi11.vxi11.Vxi11Error, ValueError) as e:
            print e
            return;
        if (max < min):
            print "Error: max is less than min. min:" + min + " max:"+max
            print "Automated test cannot run with these values."
            return 
        #min and max has been found. Add to the parameter
        parameter.set_value("min", min);
        parameter.set_value("min", max);

        #Now generate a set of random values within min and max inclusive
        set_points = []
        set_points.append(min)
        set_points.append(max)
        for i in range(run_length):
            num = random.randrange(int(min), int(max), 1)
            fraction = random.random()
            set_points.append(float(num)+fraction)
        print "Testing set for the following points:"        
        print set_points

        for i in set_points:
            parameter.set_value("expected",i)                
            print "Running SCPI command."
            self.run(scpi_command+ " "+str(i))
            value_read = None
            #power has set and act 
            if parameter_name == "power" or parameter_name == "frequency" or parameter_name=="wavelength":            
                try:
                    value_read = float(self.run(scpi_command+ "? set" ));
                except (vxi11.vxi11.Vxi11Error, ValueError) as e:
                    print e
                print "Testing set value."
                self.report(parameter, "set", value_read)

            #Frequency has lock 
            if parameter_name == "frequency":
                try:
                    for i in range(10):
                        print "Sleeping for "+ str(sleep_time) + "s"
                        time.sleep(sleep_time)        
                        value_read = self.run(scpi_command+ "? lock" );
                        if value_read.lower() =="true":
                            break;                    
                except (vxi11.vxi11.Vxi11Error, ValueError) as e:
                    print e
                print "Testing lock."
                if value_read.lower() !="true":
                    print "[FAIL]\texpected: true Read: "+ value_read.lower()
                else:
                    print "[PASS]\texpected: true Read: "+ value_read.lower()
        
            if parameter_name != "frequency" and parameter_name!="wavelength":            
                print "Sleeping for "+ str(sleep_time*2) + "s"
                time.sleep(sleep_time*2)        
                value_read = float(self.run(scpi_command+ "? act"));
                print "Testing act value."
                self.report(parameter, "act", value_read)

#########################################################################################################
# Main method. Uses arg parse for command line options. 
#########################################################################################################        

def main():
    opt_parser = argparse.ArgumentParser()


    opt_parser.add_argument("-v", "--verbose", help="increase verbosity", 
                            action="store_true")
    opt_parser.add_argument("-i", "--ip", help="ip address of the machine",
                            action="store")
    opt_parser.add_argument("-c", "--command", help="SCPI command to execute",
                            action="store")
    opt_parser.add_argument("-s", "--search", help="search for TLSX machines",
                            action="store_true")
    opt_parser.add_argument("-b", "--blades", nargs='*', help="List of blades",
                            action="store")
    opt_parser.add_argument("-l", "--lasers", nargs='*', help="List of lasers",
                            action="store")
    
    opt_args = opt_parser.parse_args()

    #Directly run a command if asked
    if opt_args.command:
        tlsx_unit = TLSX(None)
        tlsx_unit.run(opt_args.command)
        return 
    

    if opt_args.ip:
        print "Connecting:"+opt_args.ip
    else:
        print "Requires the IP address of the machine. Please run with -s option to discover."

    tlsx_unit  = TLSX(opt_args.ip)

    #todo implement verbosity levels
    if opt_args.verbose:
        print "Output set to verbose."
    if opt_args.search:
        tlsx_unit.search()
        return
    #print "Running tests..."
    #print "------------------------------"

    

    if opt_args.blades:
        blades = opt_args.blades
    else:
        blades = tlsx_unit.find_blades()
    
    if not blades:
        print "[FAIL] Could not find blades."
        return
    


    for b in blades:        
        if opt_args.lasers:
            lasers = opt_args.lasers
        else:
            lasers = tlsx_unit.find_lasers(b)
        if not lasers:
            print "[FAIL] Could not find lasers."
            return
        for l in lasers:
            print "Blade:" + b + " Laser:"+ l
            #tlsx_unit.run_safety_test(b,l)
            #tlsx_unit.switch_laser("on", b,l)
            #tlsx_unit.run_temperature_test(100,b,l)
            tlsx_unit.run_poll_power_act(20,b,l)
            #tlsx_unit.run_opc_test(b,l, "frequency")
            #parameters = ["power", "frequency", "grid"]
            #parameters = ["frequency:fine"]
            #for p in parameters:
            #    tlsx_unit.run_exceed_range_test("min",p,b,l)
            #    tlsx_unit.run_exceed_range_test("max",p,b,l)
            #    tlsx_unit.run_within_range_test(p,10,b,l)
#    tlsx_unit.run_common_commands_test()
#    
#    parameters = ["power", "frequency", "grid"]
#    for b in opt_args.blades:
#        for l in opt_args.lasers:
#            #tlsx_unit.run_test_1(5,i, j)
#            for p in parameters:
#                tlsx_unit.run_on_off_test(2,b,l)
#                #Turn on the laser so that we can switch values
#                tlsx_unit.switch_laser("on", b,l)
#                tlsx_unit.run_exceed_range_test("min",p,b,l)
#                tlsx_unit.run_exceed_range_test("max",p,b,l)
#                tlsx_unit.run_within_range_test(p,10,b,l)
#            
 

if __name__=="__main__":
    main()
