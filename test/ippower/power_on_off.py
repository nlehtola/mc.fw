#!/usr/bin/python3

import http.cookiejar
import urllib.request, urllib.error, urllib.parse
import re
import base64
import time
import sys
from random import randint

ipPowerAddress = "10.10.10.100"
	
def sendCommand(url):
	opener = urllib.request.build_opener()

	username = "admin"
	password = "12345678"
	auth = base64.b64encode((username+":"+password).encode())
	headers = {"Authorization": "Basic "+auth.decode()}
	
	reqinitial = urllib.request.Request(url, headers=headers)

	fptr = opener.open(reqinitial)
	data = fptr.read()

	return data.decode()

#command = "http://"+ipPowerAddress+"/Set.cmd?CMD=GetPower"
#command = "http://"+ipPowerAddress+"/Set.cmd?CMD=SetPower+P61=0"
#command = "http://"+ipPowerAddress+"/Set.cmd?CMD=GetSetup"

def turnPowerOn(channel=1):
	if (channel<1 or channel>4):
		print ("Please set the channel to set between 1 and 4")
		return
	command = "http://"+ipPowerAddress+"/Set.cmd?CMD=SetPower+P6"+str(channel)+"=1"
	sendCommand(command)

def turnPowerOff(channel=1):
	if (channel<1 or channel>4):
		print ("Please set the channel to set between 1 and 4")
		return

	command = "http://"+ipPowerAddress+"/Set.cmd?CMD=SetPower+P6"+str(channel)+"=0"
	sendCommand(command)

channel = 1

for x in range(0,1000):
	print('Iteration: %d' %x)
	print('Turning channel %d ON' %channel)
	sys.stdout.flush()
	turnPowerOn(channel)
	s=randint(5,50)
	print('Sleeping for %d secs..' %s)
	sys.stdout.flush()
	time.sleep(s)
	print('Turning channel %d OFF' %channel)
	sys.stdout.flush()
	turnPowerOff(channel)
	print('Sleeping for 3s..')
	sys.stdout.flush()
	time.sleep(3.0)
