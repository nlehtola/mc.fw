sleep 10

cd /root
rm -rf tmp*

for i in `seq 100`
do
    
    echo $i
    dd if=/dev/urandom of=/root/random_file bs=1M count=1
    sync
    mv /root/random_file /root/tmp_$i
    ls
done
